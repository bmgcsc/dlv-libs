
OBJDIR = ../obj
SRCDIR = .
DLVSRC = $(SRCDIR)/dlv
WLFSRC = $(SRCDIR)/wulff
HDRDIR = $(SRCDIR)/graphics
LIBDIR = ../lib64
PYDIR = ../python
TOOLDIR =

TOOLSRC = $(SRCDIR)/dummy
TOOLLIB =

CRYSRC = $(SRCDIR)/crystal

include make/$(ARCH).mk

clean:
	rm $(OBJDIR)/dlv/*.o $(PYDIR)/dlv/*.so # $(OBJDIR)/crystal/*.o

libs: $(TOOLLIB) $(LIBDIR)/libDLVbase_types.$(SO) \
	$(LIBDIR)/libDLVwulff.$(SO) $(LIBDIR)/libDLVsymmetry.$(SO) \
	$(LIBDIR)/libDLVmodels.$(SO) $(LIBDIR)/libDLVdata.$(SO) \
	$(LIBDIR)/libDLVdata_model.$(SO) $(LIBDIR)/libDLVcrystal_model.$(SO)

pylibs: $(PYDIR)/dlv/main/base.$(SO) \
	$(PYDIR)/dlv/main/model.$(SO) \
	$(PYDIR)/dlv/main/data.$(SO) \
	$(PYDIR)/dlv/main/data_model.$(SO) \
	$(PYDIR)/dlv/crystal/gui.$(SO)

DLVOBJS = $(OBJDIR)/dlv/math_fns.o $(OBJDIR)/dlv/utils.o \
	$(OBJDIR)/dlv/atom_prefs.o $(OBJDIR)/dlv/data_objs.o \
	$(OBJDIR)/dlv/model.o $(OBJDIR)/dlv/file.o \
	$(OBJDIR)/dlv/operation.o $(OBJDIR)/dlv/project.o \
	$(OBJDIR)/dlv/job_setup.o $(OBJDIR)/dlv/job.o \
	$(OBJDIR)/dlv/op_admin.o $(OBJDIR)/dlv/calculation.o \
	$(OBJDIR)/dlv/op_extra.o $(OBJDIR)/dlv/extern_base.o

SYMMOBJS = $(OBJDIR)/dlv/symmetry.o $(OBJDIR)/dlv/settings.o

MODELOBJS = $(OBJDIR)/dlv/outline.o $(OBJDIR)/dlv/shells.o \
	$(OBJDIR)/dlv/atom_model.o $(OBJDIR)/dlv/model_atoms.o \
	$(OBJDIR)/dlv/molecule.o $(OBJDIR)/dlv/edits.o $(OBJDIR)/dlv/polymer.o \
	$(OBJDIR)/dlv/slab.o $(OBJDIR)/dlv/surface.o $(OBJDIR)/dlv/crystal.o \
	$(OBJDIR)/dlv/op_model.o $(OBJDIR)/dlv/save_model.o \
	$(OBJDIR)/dlv/load_model.o $(OBJDIR)/dlv/op_atoms.o \
	$(OBJDIR)/dlv/extern_model.o

DATAOBJS = $(OBJDIR)/dlv/data_simple.o $(OBJDIR)/dlv/data_plots.o \
	$(OBJDIR)/dlv/data_surf.o $(OBJDIR)/dlv/data_vol.o \
	$(OBJDIR)/dlv/op_objs.o $(OBJDIR)/dlv/extern_data.o

DATAATOMS = $(OBJDIR)/dlv/data_atoms.o $(OBJDIR)/dlv/op_data.o \
	$(OBJDIR)/dlv/extern_data_model.o

WULFFOBJS = $(OBJDIR)/wulff/output_OOGL.o $(OBJDIR)/wulff/FD_preferences.o \
	$(OBJDIR)/wulff/make_shape.o $(OBJDIR)/wulff/vertex.o \
	$(OBJDIR)/wulff/write_shape.o

DLVTOOLS = $(OBJDIR)/$(TOOLDIR)/render_base.o \
	$(OBJDIR)/$(TOOLDIR)/model_render.o \
	$(OBJDIR)/$(TOOLDIR)/drawable.o $(OBJDIR)/$(TOOLDIR)/display_objs.o \
	$(OBJDIR)/$(TOOLDIR)/calculations.o $(OBJDIR)/$(TOOLDIR)/edit_objs.o \
	$(OBJDIR)/$(TOOLDIR)/events.o

CRYSTALMODEL = $(OBJDIR)/crystal/model.o $(OBJDIR)/crystal/load.o \
	$(OBJDIR)/crystal/save.o $(OBJDIR)/crystal/extern_gui.o

CRYSTALTOOLS = $(OBJDIR)/$(TOOLDIR)/crystal.o

$(LIBDIR)/libDLVgraphics_$(TOOLDIR).$(SO): $(DLVTOOLS)
	$(LD) $(LDFLAGS) -o $@ $(DLVTOOLS) $(GRAPHICSLIBS)

# Dummy modules
$(OBJDIR)/$(TOOLDIR)/calculations.o: $(TOOLSRC)/calculations.cxx \
	$(DLVSRC)/types.hxx $(HDRDIR)/calculations.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/calculations.cxx

$(OBJDIR)/$(TOOLDIR)/crystal.o: $(TOOLSRC)/crystal.cxx $(HDRDIR)/crystal.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/crystal.cxx

$(OBJDIR)/$(TOOLDIR)/display_objs.o: $(TOOLSRC)/display_objs.cxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(HDRDIR)/drawable.hxx \
	$(HDRDIR)/display_objs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/display_objs.cxx

$(OBJDIR)/$(TOOLDIR)/drawable.o: $(TOOLSRC)/drawable.cxx $(DLVSRC)/types.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/constants.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/drawable.cxx

$(OBJDIR)/$(TOOLDIR)/edit_objs.o: $(TOOLSRC)/edit_objs.cxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(HDRDIR)/drawable.hxx \
	$(HDRDIR)/edit_objs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/edit_objs.cxx

$(OBJDIR)/$(TOOLDIR)/events.o: $(TOOLSRC)/events.cxx $(HDRDIR)/events.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/events.cxx

$(OBJDIR)/$(TOOLDIR)/excurv.o: $(TOOLSRC)/excurv.cxx $(HDRDIR)/excurv.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/excurv.cxx

$(OBJDIR)/$(TOOLDIR)/model_render.o: $(TOOLSRC)/model_render.cxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(HDRDIR)/model_render.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/model_render.cxx

$(OBJDIR)/$(TOOLDIR)/render_base.o: $(TOOLSRC)/render_base.cxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(TOOLSRC)/render_base.cxx

$(LIBDIR)/libDLVbase_types.$(SO): $(DLVOBJS)
	$(LD) $(LDFLAGS) -o $@ $(DLVOBJS) -L$(LIBDIR) $(BASETYPELIBS)

$(OBJDIR)/dlv/operation.o: $(DLVSRC)/operation.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/job.hxx \
	$(DLVSRC)/file.hxx $(DLVSRC)/project.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/calculations.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/operation.cxx

$(OBJDIR)/dlv/op_admin.o: $(DLVSRC)/op_admin.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/op_admin.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/op_admin.cxx

$(OBJDIR)/dlv/calculation.o: $(DLVSRC)/calculation.cxx $(DLVSRC)/types.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/calculation.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/job.hxx $(DLVSRC)/file.hxx \
	$(DLVSRC)/job_setup.hxx $(DLVSRC)/project.hxx \
	$(HDRDIR)/calculations.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/calculation.cxx

$(OBJDIR)/dlv/data_objs.o: $(DLVSRC)/data_objs.cxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/math_fns.hxx $(DLVSRC)/model.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(HDRDIR)/display_objs.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/edit_objs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/data_objs.cxx

$(OBJDIR)/dlv/model.o: $(DLVSRC)/model.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/data_objs.hxx $(DLVSRC)/math_fns.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/model_render.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/model.cxx

$(OBJDIR)/dlv/atom_prefs.o: $(DLVSRC)/atom_prefs.cxx $(DLVSRC)/atom_prefs.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/atom_prefs_impl.hxx \
	$(DLVSRC)/boost_lib.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/atom_prefs.cxx

$(OBJDIR)/dlv/math_fns.o: $(DLVSRC)/math_fns.cxx $(DLVSRC)/math_fns.hxx \
	$(DLVSRC)/types.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/math_fns.cxx

$(OBJDIR)/dlv/utils.o: $(DLVSRC)/utils.cxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/utils.cxx

$(OBJDIR)/dlv/file.o: $(DLVSRC)/file.cxx $(DLVSRC)/file.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/file_impl.hxx $(DLVSRC)/boost_lib.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/file.cxx

$(OBJDIR)/dlv/job.o: $(DLVSRC)/job.cxx $(DLVSRC)/job.hxx $(DLVSRC)/types.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/job_impl.hxx \
	$(DLVSRC)/job_setup.hxx $(DLVSRC)/file.hxx \
	$(DLVSRC)/utils.hxx $(HDRDIR)/events.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/operation.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/job.cxx

$(OBJDIR)/dlv/job_setup.o: $(DLVSRC)/job_setup.cxx $(DLVSRC)/job_setup.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx  $(DLVSRC)/boost_lib.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/job_setup.cxx

$(OBJDIR)/dlv/project.o: $(DLVSRC)/project.cxx $(DLVSRC)/project.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/operation.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/atom_prefs.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/project.cxx

$(OBJDIR)/dlv/extern_base.o: $(DLVSRC)/extern_base.cxx \
	$(DLVSRC)/extern_base.hxx $(DLVSRC)/types.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/atom_prefs.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/project.hxx $(DLVSRC)/atom_prefs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/extern_base.cxx

$(LIBDIR)/libDLVwulff.$(SO): $(WULFFOBJS)
	$(LD) $(LDFLAGS) -o $@ $(WULFFOBJS) $(WULFFLIBS)

$(OBJDIR)/wulff/output_OOGL.o: $(WLFSRC)/output_OOGL.C $(WLFSRC)/vertex.h
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(WLFSRC)/output_OOGL.C

$(OBJDIR)/wulff/vertex.o: $(WLFSRC)/vertex.C $(WLFSRC)/vertex.h
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(WLFSRC)/vertex.C

$(OBJDIR)/wulff/write_shape.o: $(WLFSRC)/write_shape.C $(WLFSRC)/vertex.h
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(WLFSRC)/write_shape.C

$(OBJDIR)/wulff/make_shape.o: $(WLFSRC)/make_shape.C $(WLFSRC)/vertex.h
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(WLFSRC)/make_shape.C

$(OBJDIR)/wulff/FD_preferences.o: $(WLFSRC)/FD_preferences.C
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(WLFSRC)/FD_preferences.C

$(LIBDIR)/libDLVsymmetry.$(SO): $(SYMMOBJS)
	$(LD) $(LDFLAGS) -o $@ $(SYMMOBJS) $(SYMMETRYLIBS)

$(OBJDIR)/dlv/symmetry.o: $(DLVSRC)/symmetry.cxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/symmetry_impl.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/math_fns.hxx $(DLVSRC)/constants.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/symmetry.cxx

$(OBJDIR)/dlv/settings.o: $(DLVSRC)/settings.cxx $(DLVSRC)/settings.hxx \
	$(DLVSRC)/types.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/settings.cxx

$(LIBDIR)/libDLVmodels.$(SO): $(MODELOBJS)
	$(LD) $(LDFLAGS) -o $@ $(MODELOBJS) -L$(LIBDIR) -lDLVsymmetry -lDLVbase_types -lDLVwulff $(MODELLIBS)

$(OBJDIR)/dlv/outline.o: $(DLVSRC)/outline.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/outline.hxx $(DLVSRC)/boost_lib.hxx \
	$(DLVSRC)/utils.hxx $(HDRDIR)/model_render.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/outline.cxx

$(OBJDIR)/dlv/shells.o: $(DLVSRC)/shells.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/shells.hxx $(DLVSRC)/boost_lib.hxx \
	$(DLVSRC)/utils.hxx $(DLVSRC)/atom_prefs.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/shells.cxx

$(OBJDIR)/dlv/atom_model.o: $(DLVSRC)/atom_model.cxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/atom_model_impl.hxx $(DLVSRC)/math_fns.hxx \
	$(DLVSRC)/atom_pairs.hxx $(DLVSRC)/constants.hxx \
	$(DLVSRC)/atom_prefs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/atom_model.cxx

$(OBJDIR)/dlv/model_atoms.o: $(DLVSRC)/model_atoms.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/math_fns.hxx $(DLVSRC)/constants.hxx \
	$(DLVSRC)/atom_pairs.hxx $(DLVSRC)/atom_prefs.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/extern_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/model_atoms.cxx

$(OBJDIR)/dlv/molecule.o: $(DLVSRC)/molecule.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/molecule.hxx \
	$(DLVSRC)/atom_pairs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/molecule.cxx

$(OBJDIR)/dlv/polymer.o: $(DLVSRC)/polymer.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/polymer.hxx \
	$(DLVSRC)/atom_pairs.hxx $(DLVSRC)/math_fns.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/polymer.cxx

$(OBJDIR)/dlv/slab.o: $(DLVSRC)/slab.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/slab.hxx $(DLVSRC)/constants.hxx \
	$(DLVSRC)/atom_pairs.hxx $(DLVSRC)/math_fns.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/slab.cxx

$(OBJDIR)/dlv/crystal.o: $(DLVSRC)/crystal.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/crystal.hxx \
	$(DLVSRC)/constants.hxx $(DLVSRC)/atom_pairs.hxx \
	$(DLVSRC)/math_fns.hxx $(DLVSRC)/data_objs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/crystal.cxx

$(OBJDIR)/dlv/surface.o: $(DLVSRC)/surface.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/model_render.hxx $(HDRDIR)/toolkit_obj.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/slab.hxx $(DLVSRC)/surface.hxx \
	$(DLVSRC)/atom_pairs.hxx $(DLVSRC)/constants.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/surface.cxx

$(OBJDIR)/dlv/edits.o: $(DLVSRC)/edits.cxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/model_atoms.hxx $(DLVSRC)/symmetry.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/atom_pairs.hxx $(HDRDIR)/model_render.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/math_fns.hxx $(DLVSRC)/atom_prefs.hxx \
	$(HDRDIR)/display_objs.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/outline.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/edits.cxx

$(OBJDIR)/dlv/op_model.o: $(DLVSRC)/op_model.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/op_model.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/settings.hxx \
	$(DLVSRC)/symmetry.hxx $(DLVSRC)/constants.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/extern_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/op_model.cxx

$(OBJDIR)/dlv/op_atoms.o: $(DLVSRC)/op_atoms.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/op_model.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/op_atoms.hxx \
	$(DLVSRC)/math_fns.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/extern_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/op_atoms.cxx

$(OBJDIR)/dlv/op_extra.o: $(DLVSRC)/op_extra.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/math_fns.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/op_extra.cxx

$(OBJDIR)/dlv/load_model.o: $(DLVSRC)/load_model.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/op_model.hxx $(DLVSRC)/atom_prefs.hxx \
	$(DLVSRC)/constants.hxx $(DLVSRC)/load_model.hxx \
	$(DLVSRC)/settings.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/extern_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/load_model.cxx

$(OBJDIR)/dlv/save_model.o: $(DLVSRC)/save_model.cxx $(DLVSRC)/save_model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/op_admin.hxx \
	$(DLVSRC)/atom_prefs.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/save_model.cxx

$(OBJDIR)/dlv/extern_model.o: $(DLVSRC)/extern_model.cxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/extern_model.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/op_model.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/op_atoms.hxx \
	$(DLVSRC)/math_fns.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/model_atoms.hxx $(DLVSRC)/outline.hxx $(DLVSRC)/shells.hxx \
	$(DLVSRC)/symmetry.hxx $(DLVSRC)/atom_pairs.hxx \
	$(HDRDIR)/model_render.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/extern_model.cxx

$(LIBDIR)/libDLVdata.$(SO): $(DATAOBJS)
	$(LD) $(LDFLAGS) -o $@ $(DATAOBJS) -L$(LIBDIR) -lDLVbase_types $(DATALIBS)

$(OBJDIR)/dlv/data_simple.o: $(DLVSRC)/data_simple.cxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/data_simple.hxx \
	$(DLVSRC)/utils.hxx $(DLVSRC)/math_fns.hxx $(DLVSRC)/model.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/display_objs.hxx \
	$(HDRDIR)/edit_objs.hxx $(DLVSRC)/operation.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/data_simple.cxx

$(OBJDIR)/dlv/data_plots.o: $(DLVSRC)/data_plots.cxx $(DLVSRC)/data_plots.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/display_objs.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/model.hxx $(DLVSRC)/constants.hxx $(DLVSRC)/data_simple.hxx \
	$(DLVSRC)/math_fns.hxx $(HDRDIR)/edit_objs.hxx $(DLVSRC)/operation.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/data_plots.cxx

$(OBJDIR)/dlv/data_surf.o: $(DLVSRC)/data_surf.cxx $(DLVSRC)/data_surf.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/display_objs.hxx $(DLVSRC)/utils.hxx \
	$(HDRDIR)/edit_objs.hxx $(DLVSRC)/model.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/data_simple.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/data_surf.cxx

$(OBJDIR)/dlv/data_vol.o: $(DLVSRC)/data_vol.cxx $(DLVSRC)/data_vol.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/display_objs.hxx \
	$(HDRDIR)/edit_objs.hxx $(DLVSRC)/model.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/constants.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/data_simple.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(GRINCS) $(PIC) -c -o $@ $(DLVSRC)/data_vol.cxx

$(OBJDIR)/dlv/op_objs.o: $(DLVSRC)/op_objs.cxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/op_objs.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/data_vol.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/data_surf.hxx \
	$(DLVSRC)/data_plots.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/utils.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/op_objs.cxx

$(OBJDIR)/dlv/extern_data.o: $(DLVSRC)/extern_data.cxx \
	$(DLVSRC)/extern_data.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/op_objs.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/extern_data.cxx

$(LIBDIR)/libDLVdata_model.$(SO): $(DATAATOMS)
	$(LD) $(LDFLAGS) -o $@ $(DATAATOMS) -L$(LIBDIR) -lDLVdata -lDLVmodels $(DATAMODELLIBS)

$(OBJDIR)/dlv/data_atoms.o: $(DLVSRC)/data_atoms.cxx $(DLVSRC)/data_atoms.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/toolkit_obj.hxx $(HDRDIR)/render_base.hxx \
	$(HDRDIR)/drawable.hxx $(HDRDIR)/display_objs.hxx $(DLVSRC)/utils.hxx \
	$(DLVSRC)/data_simple.hxx $(DLVSRC)/model.hxx $(HDRDIR)/edit_objs.hxx \
	$(DLVSRC)/math_fns.hxx $(DLVSRC)/constants.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/model_atoms.hxx \
	$(DLVSRC)/symmetry.hxx $(DLVSRC)/atom_pairs.hxx \
	$(HDRDIR)/model_render.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/data_atoms.cxx

$(OBJDIR)/dlv/op_data.o: $(DLVSRC)/op_data.cxx $(DLVSRC)/op_data.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(DLVSRC)/utils.hxx $(DLVSRC)/op_admin.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/data_vol.hxx $(DLVSRC)/constants.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/data_simple.hxx $(DLVSRC)/atom_model.hxx \
	$(DLVSRC)/op_model.hxx $(DLVSRC)/math_fns.hxx $(DLVSRC)/data_surf.hxx \
	$(DLVSRC)/data_atoms.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/extern_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/op_data.cxx

$(OBJDIR)/dlv/extern_data_model.o: $(DLVSRC)/extern_data_model.cxx \
	$(DLVSRC)/extern_data_model.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/render_base.hxx \
	$(DLVSRC)/op_atoms.hxx $(DLVSRC)/op_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(DLVSRC)/extern_data_model.cxx

###CRYSTAL
$(LIBDIR)/libDLVcrystal_model.$(SO): $(CRYSTALMODEL)
	$(LD) $(LDFLAGS) -o $@ $(CRYSTALMODEL) -L$(LIBDIR) -lDLVmodels

$(OBJDIR)/crystal/model.o: $(CRYSRC)/model.cxx $(CRYSRC)/model.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/calculation.hxx $(DLVSRC)/types.hxx \
	$(DLVSRC)/boost_lib.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/data_objs.hxx $(DLVSRC)/file.hxx \
	$(HDRDIR)/calculations.hxx $(DLVSRC)/atom_prefs.hxx \
	$(HDRDIR)/render_base.hxx $(DLVSRC)/extern_model.hxx $(CRYSRC)/base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(CRYSRC)/model.cxx

$(OBJDIR)/crystal/load.o: $(CRYSRC)/load.cxx $(CRYSRC)/load.hxx \
	$(CRYSRC)/base.hxx $(DLVSRC)/operation.hxx $(DLVSRC)/calculation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/op_model.hxx \
	$(HDRDIR)/render_base.hxx $(CRYSRC)/model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(CRYSRC)/load.cxx

$(OBJDIR)/crystal/save.o: $(CRYSRC)/save.cxx $(CRYSRC)/save.hxx \
	$(CRYSRC)/base.hxx $(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx \
	$(DLVSRC)/calculation.hxx $(DLVSRC)/boost_lib.hxx $(DLVSRC)/model.hxx \
	$(DLVSRC)/atom_model.hxx $(DLVSRC)/utils.hxx $(DLVSRC)/op_admin.hxx \
	$(HDRDIR)/render_base.hxx $(CRYSRC)/model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(CRYSRC)/save.cxx

$(OBJDIR)/crystal/extern_gui.o: $(CRYSRC)/extern_gui.cxx $(CRYSRC)/base.hxx \
	$(CRYSRC)/extern_gui.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/render_base.hxx $(HDRDIR)/model_render.hxx \
	$(CRYSRC)/model.hxx $(CRYSRC)/load.hxx $(CRYSRC)/save.hxx \
	$(DLVSRC)/model.hxx $(DLVSRC)/operation.hxx $(DLVSRC)/data_objs.hxx \
	$(DLVSRC)/op_admin.hxx $(DLVSRC)/op_model.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) $(PIC) -c -o $@ $(CRYSRC)/extern_gui.cxx

###PYTHON-DLV
$(PYDIR)/dlv/main/base.$(SO): $(OBJDIR)/dlv/py_base.o
	$(LD) $(LDFLAGS) -o $@ $(OBJDIR)/dlv/py_base.o -L$(LIBDIR) -lDLVbase_types -lboost_python3

$(OBJDIR)/dlv/py_base.o: $(DLVSRC)/py_base.cxx $(DLVSRC)/extern_base.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) -I$(PYHDR) $(PIC) -c -o $@ $(DLVSRC)/py_base.cxx

$(PYDIR)/dlv/main/model.$(SO): $(OBJDIR)/dlv/py_model.o
	$(LD) $(LDFLAGS) -o $@ $(OBJDIR)/dlv/py_model.o -L$(LIBDIR) -lDLVmodels -lboost_python3

$(OBJDIR)/dlv/py_model.o: $(DLVSRC)/py_model.cxx $(DLVSRC)/extern_model.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) -I$(PYHDR) $(PIC) -c -o $@ $(DLVSRC)/py_model.cxx

$(PYDIR)/dlv/main/data.$(SO): $(OBJDIR)/dlv/py_data.o
	$(LD) $(LDFLAGS) -o $@ $(OBJDIR)/dlv/py_data.o -L$(LIBDIR) -lDLVdata -lboost_python3

$(OBJDIR)/dlv/py_data.o: $(DLVSRC)/py_data.cxx $(DLVSRC)/extern_data.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) -I$(PYHDR) $(PIC) -c -o $@ $(DLVSRC)/py_data.cxx

$(PYDIR)/dlv/main/data_model.$(SO): $(OBJDIR)/dlv/py_data_model.o
	$(LD) $(LDFLAGS) -o $@ $(OBJDIR)/dlv/py_data_model.o -L$(LIBDIR) -lDLVdata_model -lboost_python3

$(OBJDIR)/dlv/py_data_model.o: $(DLVSRC)/py_data_model.cxx \
	$(DLVSRC)/extern_data_model.hxx $(DLVSRC)/operation.hxx \
	$(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx $(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) -I$(PYHDR) $(PIC) -c -o $@ $(DLVSRC)/py_data_model.cxx

$(PYDIR)/dlv/crystal/gui.$(SO): $(OBJDIR)/crystal/py_gui.o
	$(LD) $(LDFLAGS) -o $@ $(OBJDIR)/crystal/py_gui.o -L$(LIBDIR) -lDLVcrystal_model -lboost_python3

$(OBJDIR)/crystal/py_gui.o: $(CRYSRC)/py_gui.cxx $(CRYSRC)/extern_gui.hxx \
	$(DLVSRC)/operation.hxx $(DLVSRC)/types.hxx $(DLVSRC)/boost_lib.hxx \
	$(HDRDIR)/render_base.hxx
	$(CXX) $(CXXFLAGS) $(INCDIRS) -I$(PYHDR) $(PIC) -c -o $@ $(CRYSRC)/py_gui.cxx
