# DLV-libs

DL Visualize library files.

These are the core C++ libraries used by DL Visualize. The symmetry handling
uses the core cctbx libraries from the
[Computational Crystallography Toolbox](https://github.com/cctbx/cctbx_project).
Depending on compile options there
will also be dependencies on some of the [Boost](https://www.boost.org)
libraries (filesystem, threads, and serialization)

The DL Visualize application that uses these libraries can be found [here](https://gitlab.com/bmgcsc/dl-visualize-v3).
