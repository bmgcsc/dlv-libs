
#include "avs/omx.hxx"
#include "../graphics/crystal.hxx"

void CRYSTAL::trigger_dos_atom_selections()
{
  static OMobj_id obj = OMnull_obj;
  if (OMis_null_obj(obj))
    obj = OMfind_str_subobj(OMinst_obj, "DLV.calculations.CRYSTAL"
			    ".Properties.DOS.density.trigger", OM_OBJ_RW);
  OMset_int_val(obj, 1);
}

void CRYSTAL::trigger_bdos_atom_selections()
{
  static OMobj_id obj = OMnull_obj;
  if (OMis_null_obj(obj))
    obj = OMfind_str_subobj(OMinst_obj, "DLV.calculations.CRYSTAL"
			    ".Properties.DOSbands.bands_and_dos.trigger",
			    OM_OBJ_RW);
  OMset_int_val(obj, 1);
}
