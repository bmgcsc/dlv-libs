
#ifndef DLV_DISPLAY_OBJECT
#define DLV_DISPLAY_OBJECT

namespace DLV {

  enum display_object_types {
    DISP_isosurface = 0, DISP_cells = 1, DISP_spheres = 2, DISP_volume = 3,
    DISP_hedgehog = 4, DISP_streamlines = 5, DISP_AGisolines = 6,
    DISP_plot = 7, DISP_bond_text = 8, DISP_isolines = 9, DISP_PNisolines = 10,
    DISP_solid = 11, DISP_shaded = 12, DISP_surface = 13, DISP_text = 14,
    DISP_real = 15, DISP_file = 16, DISP_box = 17, DISP_plane = 18,
    DISP_line = 19, DISP_point = 20, DISP_iso_wavefn = 21,
    DISP_atom_vectors = 22, DISP_traj = 23, DISP_leed = 24,
    DISP_sphere_obj = 25, DISP_wulff = 26
  };

  class display_obj {
  public:
    display_obj(const int i, const string n, const bool sp);
    virtual ~display_obj();

    static display_obj *create_isosurface(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const bool is_periodic,
					  const bool kspace,
					  const int component,
					  const string name, const int index,
					  char message[], const int mlen);
    static display_obj *create_iso_wavefn(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const bool is_periodic,
					  const bool kspace,
					  const int component, const int ndata,
					  const string name, const int index,
					  const int sa, const int sb,
					  const int sc, char message[],
					  const int mlen);
    static display_obj *create_cells(const class render_parent *parent,
				     const class drawable_obj *draw,
				     const bool is_periodic,
				     const bool kspace, const int component,
				     const string name, const int index,
				     char message[], const int mlen);
    static display_obj *create_spheres(const class render_parent *parent,
				       const class drawable_obj *draw,
				       const bool is_periodic,
				       const bool kspace, const int component,
				       const string name, const int index,
				       char message[], const int mlen);
    static display_obj *create_volume(const class render_parent *parent,
				       const class drawable_obj *draw,
				       const bool is_periodic,
				       const bool kspace, const int component,
				       const string name, const int index,
				       char message[], const int mlen);
    static display_obj *create_hedgehog(const class render_parent *parent,
					const class drawable_obj *draw,
					const bool is_periodic,
					const bool kspace, const int component,
					const string name, const int index,
					char message[], const int mlen);
    static display_obj *create_streamlines(const class render_parent *parent,
					   const class drawable_obj *draw,
					   const bool is_periodic,
					   const bool kspace,
					   const int component,
					   const string name, const int index,
					   char message[], const int mlen);
    static display_obj *create_ag_contour(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const bool is_periodic,
					  const bool kspace,
					  const int component,
					  const string name, const int index,
					  char message[], const int mlen);
    static display_obj *create_contour(const class render_parent *parent,
				       const class drawable_obj *draw,
				       const bool is_periodic,
				       const bool kspace,
				       const int component,
				       const string name, const int index,
				       char message[], const int mlen);
    static display_obj *create_pn_contour(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const bool is_periodic,
					  const bool kspace,
					  const int component,
					  const string name, const int index,
					  char message[], const int mlen);
    static display_obj *create_solid_ct(const class render_parent *parent,
					const class drawable_obj *draw,
					const bool is_periodic,
					const bool kspace,
					const int component,
					const string name, const int index,
					char message[], const int mlen);
    static display_obj *create_shaded_ct(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const bool is_periodic,
					 const bool kspace,
					 const int component,
					 const string name, const int index,
					 char message[], const int mlen);
    static display_obj *create_surface_plot(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const bool is_periodic,
					    const bool kspace,
					    const int component,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_dos_plot(const class render_parent *parent,
					const class drawable_obj *draw,
					const string name, const int index,
					const string xaxis, const string yaxis,
					const int ngraphs, const int type,
					char message[], const int mlen);
    static display_obj *create_panel_plot(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const string name, const int index,
					  const string xaxis,
					  const string yaxis,
					  const int ngraphs,
					  char message[], const int mlen,
					  const bool split_spins = false);
    static display_obj *create_bond_text(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 char message[], const int mlen);
    static display_obj *create_text_display(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_real_display(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_file_view(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 char message[], const int mlen);
    static display_obj *create_text_view(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 char message[], const int mlen);
    static display_obj *create_atom_text(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const int component,
					 const string name, const int index,
					 char message[], const int mlen);
    static display_obj *create_atom_vectors(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const int component,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_3D_region(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 const bool kspace, char message[],
					 const int mlen);
    static display_obj *create_sphere_obj(const class render_parent *parent,
					  const class drawable_obj *draw,
					  const string name, const int index,
					  const bool kspace, char message[],
					  const int mlen);
    static display_obj *create_plane_obj(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 const bool kspace, char message[],
					 const int mlen);
    static display_obj *create_line_obj(const class render_parent *parent,
					const class drawable_obj *draw,
					const string name, const int index,
					const bool kspace, char message[],
					const int mlen);
    static display_obj *create_point_obj(const class render_parent *parent,
					 const class drawable_obj *draw,
					 const string name, const int index,
					 const bool kspace, char message[],
					 const int mlen);
    static display_obj *create_wulff_region(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_leed_pattern(const class render_parent *parent,
					    const class drawable_obj *draw,
					    const string name, const int index,
					    char message[], const int mlen);
    static display_obj *create_phonon_anim(const class render_parent *parent,
					   const class drawable_obj *draw,
					   const string name, const int index,
					   char message[], const int mlen);

    virtual void display_3D();
    virtual void undisplay_3D();
    void update_display_list(const render_parent *parent);
    void update_display_list(const render_parent *parent, const string name);
    virtual void update_streamlines(const class drawable_obj *draw,
				    char message[], const int mlen);
    virtual void attach_params() const = 0;
    virtual bool attach_editor(const bool v, const class toolkit_obj &t,
			       float matrix[4][4], float translate[3],
			       float centre[3], char message[],
			       const int mlen) const;
    void detach_from_viewer();
    virtual void reattach_data(const class drawable_obj *draw,
			       const class render_parent *parent) = 0;
    virtual void update_data(const class render_parent *parent,
			     const class drawable_obj *draw,
			     const string name,
			     const string xaxis, const string yaxis,
			     const int ngraphs, const bool visible,
			     char message[], const int mlen);
    int get_index() const;
    bool is_k_space() const;
    void turn_off_visible();

    static void transform_point(float p[3], const float mat[4][4],
				const float xlate[3], const float centre[3]);

  protected:
    virtual int get_type() const = 0;
    string get_name() const;
    bool is_rendered() const;
    void set_rendered();

  private:
    int index;
    string name;
    bool reciprocal_space; // simplify serialization;
    bool rendered;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const
    {
      ar & name;
    }

    template <class Archive>
    void load(Archive &ar, const unsigned int version)
    {
      ar & name;
      rendered = false;
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::display_obj)
#endif // DLV_USES_SERIALIZE

inline DLV::display_obj::display_obj(const int i, const string n,
				     const bool sp)
  : index(i), name(n), reciprocal_space(sp), rendered(true)
{
}

inline void DLV::display_obj::detach_from_viewer()
{
  rendered = false;
}

inline int DLV::display_obj::get_index() const
{
  return index;
}

inline bool DLV::display_obj::is_k_space() const
{
  return reciprocal_space;
}

inline DLV::string DLV::display_obj::get_name() const
{
  return name;
}

inline bool DLV::display_obj::is_rendered() const
{
  return rendered;
}

inline void DLV::display_obj::set_rendered()
{
  rendered = true;
}

#endif // DLV_DISPLAY_OBJECT
