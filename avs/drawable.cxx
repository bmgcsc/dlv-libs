
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../dlv/constants.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"
#include "fld/Xfld.h"
#include "draw_impl.hxx"
#include "avs/src/express/points.hxx"
#include "avs/src/express/lines.hxx"
#include "avs/src/express/plots.hxx"
#include "avs/src/express/volumes.hxx"

#ifdef WIN32
#  define snprintf _snprintf
#endif // WIN32

DLV::drawable_obj::~drawable_obj()
{
}

DLV::drawable_obj *
DLV::drawable_obj::create_lengths_angles(const render_parent *parent,
					 const string formula,
					 char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  drawable_obj *draw = new atoms_bonds_text_obj(id, formula);
  if (draw == 0)
    strncpy(message, "Unable to create bond lengths and angles object", mlen);
    //else {
    // Todo ?
    //}
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_value(const render_parent *parent,
				     const string value, char message[],
				     const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  text_obj *draw = new text_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create text object", mlen);
  else {
    bool ok;
    ok = draw->set_value(value, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_value(const render_parent *parent,
				     const double value, char message[],
				     const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  real_obj *draw = new real_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create real scalar object", mlen);
  else {
    bool ok;
    ok = draw->set_value(value, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_file(const render_parent *parent,
				    const string name, char message[],
				    const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  text_view_obj *draw = new text_view_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create text_file object", mlen);
  else {
    bool ok;
    ok = draw->set_name(name, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_buff(const render_parent *parent,
				    char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  buffer_view_obj *draw = new buffer_view_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create buffer object", mlen);
  else {
    bool ok = true;
    //ok = draw->set_name(name, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_field(const render_parent *parent,
				     float coords[][3], int **data,
				     const string labels[], const int ndata,
				     const int natoms, char message[],
				     const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  atom_text_obj *draw = new atom_text_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create text object", mlen);
  else {
    bool ok;
    ok = draw->field(coords, data, labels, ndata, natoms, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_field(const render_parent *parent,
				     float coords[][3], float **data,
				     const string labels[], const int ndata,
				     const int natoms, char message[],
				     const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  atom_text_obj *draw = new atom_text_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create text object", mlen);
  else {
    bool ok;
    ok = draw->field(coords, data, labels, ndata, natoms, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_vector_field(const render_parent *parent,
				       float coords[][3], float data[][3],
				       const int natoms, const int ncmpts,
				       char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  atom_vector_obj *draw = new atom_vector_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create vector object", mlen);
  else {
    bool ok;
    ok = draw->field(coords, data, natoms, ncmpts, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_volume(const render_parent *parent,
				 const float origin[3], const float astep[3],
				 const float bstep[3], const float cstep[3],
				 float *data[], const int nx, const int ny,
				 const int nz, const int dims[],
				 const string labels[], const int ndata,
				 const bool kspace, const bool use_ignore,
				 const float ignore, char message[],
				 const int mlen)
{
  const float tol = 1e-5f;
  OMobj_id id = OMnull_obj;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  // Decide on the type of field data - we could attempt a smarter
  // version that rearranges the data if the coord order i.e. y,z,x.
  drawable_obj *draw = 0;
  if (std::abs(astep[1]) < tol and std::abs(astep[2]) < tol and
      std::abs(bstep[0]) < tol and std::abs(bstep[2]) < tol and
      std::abs(cstep[0]) < tol and std::abs(cstep[1]) < tol)
    draw = new rect_obj(id, kspace);
  else
    draw = new struct_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create volume object", mlen);
  else {
    bool ok;
    ok = draw->volume(origin, astep, bstep, cstep, data, nx, ny, nz, dims,
		      labels, ndata, use_ignore, ignore, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_slice(const render_parent *parent,
				     const float origin[3],
				     const float astep[3],
				     const float bstep[3],
				     float *data[], const int nx, const int ny,
				     const string labels[], const int ndata,
				     const bool use_ignore, const float ignore,
				     char message[], const int mlen)
{
  const float tol = 1e-5f;
  OMobj_id id = parent->get_rspace().get_id();
  // Decide on the type of field data - we could attempt a smarter
  // version that rearranges the data if the coord order i.e. y,z,x.
  drawable_obj *draw = 0;
  if (std::abs(astep[1]) < tol and std::abs(astep[2]) < tol and
      std::abs(bstep[0]) < tol and std::abs(bstep[2]) < tol)
    draw = new rect_obj(id, false);
  else
    draw = new struct_obj(id, false);
  if (draw == 0)
    strncpy(message, "Unable to create slice object", mlen);
  else {
    bool ok;
    ok = draw->slice(origin, astep, bstep, data, nx, ny,
		     labels, ndata, use_ignore, ignore, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_dos_data(const render_parent *parent,
				   float grid[], const int npoints,
				   float *data[], const string labels[],
				   const int ngraphs, const bool spin,
				   const float xpoints[],
				   const string xlabels[], const int nx,
				   const float yrange[],
				   char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  drawable_obj *draw = new rect_dos(id);
  if (draw == 0)
    strncpy(message, "Unable to create plot object", mlen);
  else {
    bool ok;
    ok = draw->plot(grid, npoints, data, labels, ngraphs, spin,
		    xpoints, xlabels, nx, yrange, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_multi_grid_data(const render_parent *parent,
					  float *multi_grids[], 
					  const int ngrids, int *select_grid,
					  int *grid_sizes,
					  const int npoints,
					  float *data[], const string labels[],
					  const int ngraphs, const bool spin,
					  const float xpoints[],
					  const string xlabels[], const int nx,
					  const float yrange[],
					  char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  drawable_obj *draw = new rect_multi(id);
  if (draw == 0)
    strncpy(message, "Unable to create plot object", mlen);
  else {
    bool ok;
    ok = draw->plot(multi_grids, ngrids, select_grid, grid_sizes,
		    npoints, data, labels, ngraphs, spin,
		    xpoints, xlabels, nx, yrange, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_panel_data(const render_parent *parent,
				     float grid[], const int npoints,
				     float *data[], const string labels[],
				     const int ngraphs, const bool spin,
				     const float xpoints[],
				     const string xlabels[], const int nx,
				     const float ypoints[],
				     const string ylabels[], const int ny,
				     const float xrange[],
				     const float yrange[],
				     char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  drawable_obj *draw = new rect_band(id);
  if (draw == 0)
    strncpy(message, "Unable to create plot object", mlen);
  else {
    bool ok;
    ok = draw->plot(grid, npoints, data, labels, ngraphs, spin, xpoints,
		    xlabels, nx, ypoints, ylabels, ny, xrange, yrange,
		    message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_bdos_data(const render_parent *parent,
				    float grid[], const int npoints,
				    float *data[], const string labels[],
				    const int ngraphs, const bool spin,
				    const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    float dgrid[], const int dnpoints,
				    float *ddata[], const int dngraphs,
				    char message[], const int mlen)
{
  OMobj_id id = parent->get_rspace().get_id();
  rect_band *draw = new rect_band(id);
  if (draw == 0)
    strncpy(message, "Unable to create plot object", mlen);
  else {
    bool ok;
    ok = draw->plot(grid, npoints, data, labels, ngraphs, spin, xpoints,
		    xlabels, nx, ypoints, ylabels, ny, xrange, yrange,
		    dgrid, dnpoints, ddata, dngraphs, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_wavefn(const render_parent *parent,
				      const float origin[3],
				      const float astep[3],
				      const float bstep[3],
				      const float cstep[3],
				      float *data[], float *phases[],
				      const int nx, const int ny,
				      const int nz, const string labels[],
				      const int ndata, const bool use_ignore,
				      const float ignore,
				      char message[], const int mlen)
{
  const float tol = 1e-5f;
  OMobj_id id = parent->get_rspace().get_id();
  // Decide on the type of field data - we could attempt a smarter
  // version that rearranges the data if the coord order i.e. y,z,x.
  drawable_obj *draw = 0;
  if (std::abs(astep[1]) < tol and std::abs(astep[2]) < tol and
      std::abs(bstep[0]) < tol and std::abs(bstep[2]) < tol and
      std::abs(cstep[0]) < tol and std::abs(cstep[1]) < tol)
    draw = new rect_obj(id, false);
  else
    draw = new struct_obj(id, false);
  if (draw == 0)
    strncpy(message, "Unable to create wavefunction object", mlen);
  else {
    bool ok;
    ok = draw->volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		      labels, ndata, use_ignore, ignore, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_bloch_wavefn(const render_parent *parent,
				       const float origin[3],
				       const float astep[3],
				       const float bstep[3],
				       const float cstep[3],
				       float *data[], float *phases[],
				       const int nx, const int ny,
				       const int nz, const string labels[],
				       const int ndata,
				       const int knum[], const int kden[],
				       const bool use_ignore,
				       const float ignore,
				       char message[], const int mlen)
{
  const float tol = 1e-5f;
  OMobj_id id = parent->get_rspace().get_id();
  // Decide on the type of field data - we could attempt a smarter
  // version that rearranges the data if the coord order i.e. y,z,x.
  drawable_obj *draw = 0;
  if (std::abs(astep[1]) < tol and std::abs(astep[2]) < tol and
      std::abs(bstep[0]) < tol and std::abs(bstep[2]) < tol and
      std::abs(cstep[0]) < tol and std::abs(cstep[1]) < tol)
    draw = new rect_obj(id, false);
  else
    draw = new struct_obj(id, false);
  if (draw == 0)
    strncpy(message, "Unable to create bloch object", mlen);
  else {
    bool ok;
    ok = draw->volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		      labels, ndata, knum, kden, use_ignore, ignore,
		      message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_box_data(const render_parent *parent,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  box_obj *draw = new box_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create volume roi object", mlen);
  else {
    float origin[3] = {0.0, 0.0, 0.0};
    float a[3] = {1.0, 0.0, 0.0};
    float b[3] = {0.0, 1.0, 0.0};
    float c[3] = {0.0, 0.0, 1.0};
    draw->update_box(origin, a, b, c);
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_sphere_data(const render_parent *parent,
				      const bool kspace, char message[],
				      const int mlen)
{
  OMobj_id id;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  sphere_obj *draw = new sphere_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create sphere object", mlen);
  else {
    float origin[3] = {0.0, 0.0, 0.0};
    float radius = 1.0;
    draw->update_sphere(origin, radius);
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_plane_data(const render_parent *parent,
				     const bool kspace, char message[],
				     const int mlen)
{
  OMobj_id id;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  plane_obj *draw = new plane_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create plane object", mlen);
  else {
    float origin[3] = {0.0, 0.0, 0.0};
    float a[3] = {1.0, 0.0, 0.0};
    float b[3] = {0.0, 1.0, 0.0};
    draw->update_plane(origin, a, b);
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_line_data(const render_parent *parent,
				    const bool kspace, char message[],
				    const int mlen)
{
  OMobj_id id;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  line_obj *draw = new line_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create line object", mlen);
  else {
    float origin[3] = {0.0, 0.0, 0.0};
    float a[3] = {1.0, 0.0, 0.0};
    draw->update_line(origin, a);
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_point_data(const render_parent *parent,
				     const bool kspace, char message[],
				     const int mlen)
{
  OMobj_id id;
  if (kspace)
    id = parent->get_kspace().get_id();
  else
    id = parent->get_rspace().get_id();
  point_obj *draw = new point_obj(id, kspace);
  if (draw == 0)
    strncpy(message, "Unable to create point object", mlen);
  else {
    float origin[3] = {0.0, 0.0, 0.0};
    draw->update_point(origin);
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_wulff_data(const render_parent *parent,
				     float (*vertices)[3], const int nverts,
				     float (*colours)[3], float (*normals)[3],
				     long connects[], const long nconnects,
				     float (*pos)[3], string labels[],
				     int nodes[], const int nnodes,
				     float (*lverts)[3], int nlverts,
				     long lines[], const long nlines,
				     char message[], const int mlen)
{
  OMobj_id id;
  id = parent->get_rspace().get_id();
  wulff_obj *draw = new wulff_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create wulff object", mlen);
  else {
    bool ok = draw->wulff(vertices, nverts, colours, normals, connects,
			  nconnects, pos, labels, nodes, nnodes,
			  lverts, nlverts, lines, nlines, message, mlen);
    if (!ok) {
      delete draw;
      draw = 0;
    }
  }
  return draw;
}

DLV::drawable_obj *
DLV::drawable_obj::create_leed_data(const render_parent *parent,
				    float (*points)[2], const int npoints,
				    float (*domains)[4], int ndomains,
				    int *colours, char message[],
				    const int mlen)
{
  OMobj_id id;
  id = parent->get_rspace().get_id();
  leed_obj *draw = new leed_obj(id);
  if (draw == 0)
    strncpy(message, "Unable to create leed object", mlen);
  else
    draw->update_leed(points, npoints, domains, ndomains, colours);
  return draw;
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float yrange[], char message[],
				    const int mlen)
{
  // dummy
}

void DLV::rect_dos::reload_data(const render_parent *parent, float grid[],
				const int npoints, float *data[],
				const string labels[], const int ngraphs,
				const bool spin, const float xpoints[],
				const string xlabels[], const int nx,
				const float yrange[], char message[],
				const int mlen)
{
  (void) plot(grid, npoints, data, labels, ngraphs, spin,
	      xpoints, xlabels, nx, yrange, message, mlen);
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float *multi_grids[],
				    const int ngrids, int *grid_sizes,
				    const int npoints, float *data[],
				    int *select_grid,
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float yrange[], char message[],
				    const int mlen)
{
  // dummy
}

void DLV::rect_multi::reload_data(const render_parent *parent, float *multi_grids[],
				  const int ngrids, int *grid_sizes,
				const int npoints, float *data[],
				int *select_grid,
				const string labels[], const int ngraphs,
				const bool spin, const float xpoints[],
				const string xlabels[], const int nx,
				const float yrange[], char message[],
				const int mlen)
{
  (void) plot(multi_grids, ngrids, select_grid, grid_sizes, 
	      npoints, data, labels, ngraphs, spin,
	      xpoints, xlabels, nx, yrange, message, mlen);
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    char message[], const int mlen)
{
  // Dummy
}

void DLV::rect_band::reload_data(const render_parent *parent, float grid[],
				 const int npoints, float *data[],
				 const string labels[], const int ngraphs,
				 const bool spin, const float xpoints[],
				 const string xlabels[], const int nx,
				 const float ypoints[],
				 const string ylabels[], const int ny,
				 const float xrange[], const float yrange[],
				 char message[], const int mlen)
{
  (void) plot(grid, npoints, data, labels, ngraphs, spin, xpoints, xlabels,
	      nx, ypoints, ylabels, ny, xrange, yrange, message, mlen);
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    float dgrid[], const int dnpoints,
				    float *ddata[], const int dngraphs,
				    char message[], const int mlen)
{
  // dummy
}

void DLV::rect_band::reload_data(const render_parent *parent, float grid[],
				 const int npoints, float *data[],
				 const string labels[], const int ngraphs,
				 const bool spin, const float xpoints[],
				 const string xlabels[], const int nx,
				 const float ypoints[],
				 const string ylabels[], const int ny,
				 const float xrange[], const float yrange[],
				 float dgrid[], const int dnpoints,
				 float *ddata[], const int dngraphs,
				 char message[], const int mlen)
{
  (void) plot(grid, npoints, data, labels, ngraphs, spin, xpoints,
	      xlabels, nx, ypoints, ylabels, ny, xrange, yrange,
	      dgrid, dnpoints, ddata, dngraphs, message, mlen);
}

void DLV::drawable_obj::reload_data(const render_parent *parent,
				    const float origin[3],
				    const float astep[3], const float bstep[3],
				    float *data[], const int nx, const int ny,
				    const string labels[], const int ndata,
				    const bool use_ignore, const float ignore,
				    char message[], const int mlen)
{
  // dummy
}

void DLV::rect_obj::reload_data(const render_parent *parent,
				const float origin[3],
				const float astep[3], const float bstep[3],
				float *data[], const int nx, const int ny,
				const string labels[], const int ndata,
				const bool use_ignore, const float ignore,
				char message[], const int mlen)
{
  (void) slice(origin, astep, bstep, data, nx, ny,
	       labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::struct_obj::reload_data(const render_parent *parent,
				  const float origin[3],
				  const float astep[3], const float bstep[3],
				  float *data[], const int nx, const int ny,
				  const string labels[], const int ndata,
				  const bool use_ignore, const float ignore,
				  char message[], const int mlen)
{
  (void) slice(origin, astep, bstep, data, nx, ny,
	       labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::drawable_obj::reload_data(const render_parent *parent,
				    const float origin[3],
				    const float astep[3], const float bstep[3],
				    const float cstep[3], float *data[],
				    const int nx, const int ny,
				    const int nz, const int dims[],
				    const string labels[], const int ndata,
				    const bool kspace, const bool use_ignore,
				    const float ignore, char message[],
				    const int mlen)
{
  // dummy
}

void DLV::rect_obj::reload_data(const render_parent *parent,
				const float origin[3],
				const float astep[3], const float bstep[3],
				const float cstep[3], float *data[],
				const int nx, const int ny,
				const int nz, const int dims[],
				const string labels[], const int ndata,
				const bool kspace, const bool use_ignore,
				const float ignore, char message[],
				const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, nx, ny, nz, dims,
		labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::struct_obj::reload_data(const render_parent *parent,
				  const float origin[3],
				  const float astep[3], const float bstep[3],
				  const float cstep[3], float *data[],
				  const int nx, const int ny,
				  const int nz, const int dims[],
				  const string labels[], const int ndata,
				  const bool kspace, const bool use_ignore,
				  const float ignore, char message[],
				  const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, nx, ny, nz, dims,
		labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::drawable_obj::reload_wavefn(const render_parent *parent,
				      const float origin[3],
				      const float astep[3],
				      const float bstep[3],
				      const float cstep[3], float *data[],
				      float *phases[], const int nx,
				      const int ny, const int nz,
				      const string labels[], const int ndata,
				      const bool use_ignore, const float ignore,
				      char message[], const int mlen)
{
}

void DLV::rect_obj::reload_wavefn(const render_parent *parent,
				  const float origin[3],
				  const float astep[3],
				  const float bstep[3],
				  const float cstep[3], float *data[],
				  float *phases[], const int nx,
				  const int ny, const int nz,
				  const string labels[], const int ndata,
				  const bool use_ignore, const float ignore,
				  char message[], const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::struct_obj::reload_wavefn(const render_parent *parent,
				    const float origin[3],
				    const float astep[3],
				    const float bstep[3],
				    const float cstep[3], float *data[],
				    float *phases[], const int nx,
				    const int ny, const int nz,
				    const string labels[], const int ndata,
				    const bool use_ignore, const float ignore,
				    char message[], const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		labels, ndata, use_ignore, ignore, message, mlen);
}

void DLV::drawable_obj::reload_bloch(const render_parent *parent,
				     const float origin[3],
				     const float astep[3], const float bstep[3],
				     const float cstep[3], float *data[],
				     float *phases[], const int nx,
				     const int ny, const int nz,
				     const string labels[], const int ndata,
				     const int knum[], const int kden[],
				     const bool use_ignore, const float ignore,
				     char message[], const int mlen)
{
  // dummy
}

void DLV::rect_obj::reload_bloch(const render_parent *parent,
				 const float origin[3],
				 const float astep[3], const float bstep[3],
				 const float cstep[3], float *data[],
				 float *phases[], const int nx,
				 const int ny, const int nz,
				 const string labels[], const int ndata,
				 const int knum[], const int kden[],
				 const bool use_ignore, const float ignore,
				 char message[], const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		labels, ndata, knum, kden, use_ignore, ignore,
		message, mlen);
}

void DLV::struct_obj::reload_bloch(const render_parent *parent,
				   const float origin[3],
				   const float astep[3], const float bstep[3],
				   const float cstep[3], float *data[],
				   float *phases[], const int nx,
				   const int ny, const int nz,
				   const string labels[], const int ndata,
				   const int knum[], const int kden[],
				   const bool use_ignore, const float ignore,
				   char message[], const int mlen)
{
  (void) volume(origin, astep, bstep, cstep, data, phases, nx, ny, nz,
		labels, ndata, knum, kden, use_ignore, ignore,
		message, mlen);
}

DLV::drawable_obj *DLV::drawable_obj::create_edit(class toolkit_obj &id)
{
  return new drawable_edit(id.get_id());
}

DLV::drawable_obj *DLV::drawable_obj::duplicate(const toolkit_obj &id) const
{
  // BUG
  return 0;
}

DLV::drawable_obj *DLV::rect_obj::duplicate(const toolkit_obj &id) const
{
  rect_obj *ptr = new rect_obj(id.get_id(), is_k_space());
  OMset_obj_val(ptr->data->obj_id(), data->obj_id(), 0);
  return ptr;
}

DLV::drawable_obj *DLV::struct_obj::duplicate(const toolkit_obj &id) const
{
  struct_obj *ptr = new struct_obj(id.get_id(), is_k_space());
  OMset_obj_val(ptr->data->obj_id(), data->obj_id(), 0);
  return ptr;
}

DLV::text_obj::text_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Points_text_data(parent))
{
}

DLV::real_obj::real_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Points_real_data(parent))
{
}

DLV::text_view_obj::text_view_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Points_file_view_data(parent))
{
}

DLV::buffer_view_obj::buffer_view_obj(const OMobj_id parent)
  : drawable_obj(false),
    data(new CCP3_Renderers_Points_buffer_view_data(parent))
{
}

DLV::atoms_bonds_text_obj::atoms_bonds_text_obj(const OMobj_id parent,
						const string formula)
  : drawable_obj(false),
    data(new CCP3_Renderers_Points_atom_bond_text_data(parent))
{
  data->formula = formula.c_str();
}

DLV::atom_text_obj::atom_text_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Points_label_field(parent))
{
}

DLV::atom_vector_obj::atom_vector_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Points_vector_field(parent))
{
}

DLV::box_obj::box_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new CCP3_Renderers_Volumes_box_data(parent))
{
}

DLV::sphere_obj::sphere_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new CCP3_Renderers_Volumes_sphere_data(parent))
{
}

DLV::plane_obj::plane_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new CCP3_Renderers_Planes_plane_data(parent))
{
}

DLV::line_obj::line_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new CCP3_Renderers_Lines_line_data(parent))
{
}

DLV::point_obj::point_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new CCP3_Renderers_Points_point_data(parent))
{
}

DLV::struct_obj::struct_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new FLD_Field_Struct(parent))
{
}

DLV::rect_obj::rect_obj(const OMobj_id parent, const bool sp)
  : drawable_obj(sp), data(new FLD_Field_Rect(parent))
{
}

DLV::rect_dos::rect_dos(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Plots_dos_data(parent))
{
}

DLV::rect_multi::rect_multi(const OMobj_id parent)
  : rect_dos(false, new CCP3_Renderers_Plots_multi_grid_data(parent))
{
}

DLV::rect_dos::rect_dos(bool const sp, CCP3_Renderers_Plots_dos_data *d)
  : drawable_obj(sp), data(d)
{
}

DLV::rect_band::rect_band(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Plots_band_data(parent))
{
}

DLV::wulff_obj::wulff_obj(const OMobj_id parent)
  : drawable_obj(false),
    data(new CCP3_Renderers_Volumes_wulff_plot_data(parent))
{
}

DLV::leed_obj::leed_obj(const OMobj_id parent)
  : drawable_obj(false), data(new CCP3_Renderers_Plots_leed_data(parent))
{
}

DLV::drawable_edit::drawable_edit(const OMobj_id id)
  : drawable_obj(false), obj_id(id)
{
}

DLV::text_obj::~text_obj()
{
  delete data;
}

DLV::real_obj::~real_obj()
{
  delete data;
}

DLV::text_view_obj::~text_view_obj()
{
  delete data;
}

DLV::buffer_view_obj::~buffer_view_obj()
{
  delete data;
}

DLV::atoms_bonds_text_obj::~atoms_bonds_text_obj()
{
  delete data;
}

DLV::atom_text_obj::~atom_text_obj()
{
  delete data;
}

DLV::atom_vector_obj::~atom_vector_obj()
{
  delete data;
}

DLV::struct_obj::~struct_obj()
{
  delete data;
}

DLV::rect_obj::~rect_obj()
{
  delete data;
}

DLV::rect_dos::~rect_dos()
{
  delete data;
}

DLV::rect_band::~rect_band()
{
  delete data;
}

DLV::box_obj::~box_obj()
{
  delete data;
}

DLV::sphere_obj::~sphere_obj()
{
  delete data;
}

DLV::plane_obj::~plane_obj()
{
  delete data;
}

DLV::line_obj::~line_obj()
{
  delete data;
}

DLV::point_obj::~point_obj()
{
  delete data;
}

DLV::wulff_obj::~wulff_obj()
{
  delete data;
}

DLV::leed_obj::~leed_obj()
{
  delete data;
}

DLV::toolkit_obj DLV::text_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::real_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::text_view_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::buffer_view_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::atoms_bonds_text_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::atom_text_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::atom_vector_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::struct_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::rect_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::rect_dos::get_id() const
{
  return toolkit_obj(data->obj_id());
  }

DLV::toolkit_obj DLV::rect_band::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::box_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::sphere_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::wulff_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::leed_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::plane_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::line_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::point_obj::get_id() const
{
  return toolkit_obj(data->obj_id());
}

DLV::toolkit_obj DLV::drawable_edit::get_id() const
{
  return toolkit_obj(obj_id);
}

bool DLV::text_obj::set_value(const string value, char message[],
			      const int mlen)
{
  OMset_str_val(data->data.obj_id(), value.c_str());
  return true;
}

bool DLV::real_obj::set_value(const double value, char message[],
			      const int mlen)
{
  data->data = value;
  return true;
}

bool DLV::text_view_obj::set_name(const string name, char message[],
				  const int mlen)
{
  data->filename = name.c_str();
  return true;
}

bool DLV::atom_text_obj::field(float coords[][3], int **array,
			       const string labels[], const int ndata,
			       const int natoms, char message[],
			       const int mlen)
{
  data->n = ndata;
  data->natoms = natoms;
  xp_long s = natoms * 3;
  int ok = data->coords.set_array(OM_TYPE_FLOAT, coords, s,
				  OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set text coordinates array", mlen);
    return false;
  }
  char buff[128];
  for (int i = 0; i < ndata; i++) {
    OMset_str_array_val(data->names.obj_id(), i, labels[i].c_str());
    for (int j = 0; j < natoms; j++) {
      snprintf(buff, 128, "%1d", array[i][j]);
      OMset_str_array_val(data->data[i].labels.obj_id(), j, buff);
    }
  }
  return true;
}

bool DLV::atom_text_obj::field(float coords[][3], float **array,
			       const string labels[], const int ndata,
			       const int natoms, char message[],
			       const int mlen)
{
  data->n = ndata;
  data->natoms = natoms;
  xp_long s = natoms * 3;
  int ok = data->coords.set_array(OM_TYPE_FLOAT, coords, s,
				  OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set text coordinates array", mlen);
    return false;
  }
  char buff[128];
  for (int i = 0; i < ndata; i++) {
    OMset_str_array_val(data->names.obj_id(), i, labels[i].c_str());
    for (int j = 0; j < natoms; j++) {
      snprintf(buff, 128, "%8.3f", array[i][j]);
      OMset_str_array_val(data->data[i].labels.obj_id(), j, buff);
    }
  }
  return true;
}

bool DLV::atom_vector_obj::field(float coords[][3], float array[][3],
				 const int natoms, const int ncmpts,
				 char message[], const int mlen)
{
  data->nnodes = natoms;
  data->nnode_data = ncmpts;
  data->nspace = 3;
  data->coordinates.veclen = 3;
  xp_long s = natoms * 3;
  int ok = data->coordinates.values.set_array(OM_TYPE_FLOAT, coords, s,
					      OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set vector coordinates array", mlen);
    return false;
  }
  data->node_data[0].veclen = 3;
  ok = data->node_data[0].values.set_array(OM_TYPE_FLOAT, array, s,
					   OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set vector data array", mlen);
    return false;
  }
  return true;
}

bool DLV::wulff_obj::wulff(float (*vertices)[3], const int nverts,
			   float (*colours)[3], float (*normals)[3],
			   long connects[], const long nconnects,
			   float (*pos)[3], string labels[], int nodes[],
			   const int nnodes, float (*lverts)[3], int nlverts,
			   long lines[], const long nlines,
			   char message[], const int mlen)
{
  data->nvertices = nverts;
  xp_long size = 3 * nverts;
  int ok = data->vertices.set_array(OM_TYPE_FLOAT, vertices,
				    size, OM_SET_ARRAY_COPY);
  ok = data->colours.set_array(OM_TYPE_FLOAT, colours, size,
			       OM_SET_ARRAY_COPY);
  ok = data->normals.set_array(OM_TYPE_FLOAT, normals, size,
			       OM_SET_ARRAY_COPY);
  size = nconnects;
  // Todo - possibly dangerous assumption about long vs xp_long?
  ok = data->connects.set_array(OM_TYPE_LONG, connects, size,
				OM_SET_ARRAY_COPY);
  data->nlabels = nnodes;
  size = nnodes;
  ok = data->nodes.set_array(OM_TYPE_INT, nodes, size, OM_SET_ARRAY_COPY);
  size =  3 * nnodes;
  ok = data->label_pos.set_array(OM_TYPE_FLOAT, pos, size, OM_SET_ARRAY_COPY);
  OMset_array_size(data->labels.obj_id(), nnodes);
  for (int i = 0; i < nnodes; i++)
    OMset_str_array_val(data->labels.obj_id(), i, labels[i].c_str());
  data->nline_verts = nlverts;
  size = 3 * nlverts;
  ok = data->line_vertices.set_array(OM_TYPE_FLOAT, lverts,
				     size, OM_SET_ARRAY_COPY);
  size = nlines;
  ok = data->lines.set_array(OM_TYPE_LONG, lines, size, OM_SET_ARRAY_COPY);
  //OMset_array_size(data->info.obj_id(), nnodes + nverts);
  int k = 0;
  int l = 0;
  char buffer[10240];
  buffer[0] = '\0';
  for (int i = 0; i < nnodes; i++) {
    if (k > 0)
      strcat(buffer, "\n");
    //OMset_str_array_val(data->info.obj_id(), k, labels[i].c_str());
    strcat(buffer, labels[i].c_str());
    k++;
    for (int j = 0; j < nodes[i]; j++) {
      char buff[64];
      snprintf(buff, 64, "%12.6f %12.6f %12.6f", vertices[l][0],
	       vertices[l][1], vertices[l][2]);
      if (k > 0)
	strcat(buffer, "\n");
      strcat(buffer, buff);
      //OMset_str_array_val(data->info.obj_id(), k, buff);
      k++;
      l++;
    }
  }
  OMset_str_val(data->info.obj_id(), buffer);
  return true;
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *datavals[], const int nx, const int ny,
			       const int nz, const int vdims[],
			       const string labels[], const int ndata,
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  strncpy(message, "BUG: not a volume object", mlen);
  return false;
}

bool DLV::struct_obj::volume(const float origin[3], const float astep[3],
			     const float bstep[3], const float cstep[3],
			     float *datavals[], const int nx, const int ny,
			     const int nz, const int vdims[],
			     const string labels[], const int ndata,
			     const bool use_ignore, const float ignore,
			     char message[], const int mlen)
{
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  dims[2] = nz;
  data->dims.free_array(dims);
  xp_long s = nx * ny * nz;
  data->nnodes = s;
  data->nspace = 3;
  float (*coords)[3] = new float[s][3];
    //  (float (*)[3])data->coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  if (coords == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  // Build the x, y, z node values. Check order.
  float c[3];
  for (int k = 0; k < nz; k++) {
    c[0] = origin[0] + ((float) k) * cstep[0];
    c[1] = origin[1] + ((float) k) * cstep[1];
    c[2] = origin[2] + ((float) k) * cstep[2];
    int kk = k * ny;
    float b[3];
    for (int j = 0; j < ny; j++) {
      b[0] = c[0] + ((float) j) * bstep[0];
      b[1] = c[1] + ((float) j) * bstep[1];
      b[2] = c[2] + ((float) j) * bstep[2];
      int jj = (kk + j) * nx;
      for (int i = 0; i < nx; i++) {
        coords[jj + i][0] = b[0] + ((float) i) * astep[0];
        coords[jj + i][1] = b[1] + ((float) i) * astep[1];
        coords[jj + i][2] = b[2] + ((float) i) * astep[2];
      }
    }
  }
  //data->coordinates.values.free_array(coords);
  s = 3 * nx * ny * nz;
  data->coordinates.values.set_array(OM_TYPE_FLOAT, coords, s,
				     OM_SET_ARRAY_COPY);
  delete [] coords;
  data->nnode_data = ndata;
  int array_handler = OM_SET_ARRAY_STATIC;
  if (labels == 0)
    array_handler = OM_SET_ARRAY_COPY;
  for (int i = 0; i < ndata; i++) {
    if (labels != 0) {
      data->node_data[i].veclen = vdims[i];
      string t = labels[i];
      data->node_data[i].labels = t.c_str();
    }
    // Try to avoid having AVS copy the object.
    s = nx * ny * nz * vdims[i];
    int ok = data->node_data[i].values.set_array(OM_TYPE_FLOAT, datavals[i],
						 s, array_handler);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume data array", mlen);
      return false;
    }
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    if (use_ignore) {
      data->node_data[i].null_value = ignore;
      data->node_data[i].null_flag = 1;
      for (xp_long j = 0; j < s; j++) {
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd and datavals[i][j] < (ignore - tol))
          maxd = datavals[i][j];
      }
    } else {
      for (xp_long j = 0; j < s; j++) {
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd)
          maxd = datavals[i][j];
      }
    }
    data->node_data[i].min = mind;
    data->node_data[i].max = maxd;
  }
  return true;
}

bool DLV::rect_obj::volume(const float origin[3], const float astep[3],
			   const float bstep[3], const float cstep[3],
			   float *datavals[], const int nx, const int ny,
			   const int nz, const int vdims[],
			   const string labels[], const int ndata,
			   const bool use_ignore, const float ignore,
			   char message[], const int mlen)
{
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  dims[2] = nz;
  data->dims.free_array(dims);
  xp_long s = nx * ny * nz;
  data->nnodes = s;
  data->nspace = 3;
  // set the points.
  float (*points)[3] = new float[nx + ny + nz][3];
  //  (float (*)[3])data->points.ret_array_ptr(OM_GET_ARRAY_WR);
  if (points == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  for (int i = 0; i < nx; i++) {
    points[i][0] = origin[0] + ((float) i) * astep[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2];
  }
  for (int i = nx; i < nx + ny; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1] + ((float) (i - nx)) * bstep[1];
    points[i][2] = origin[2];
  }
  for (int i = nx + ny; i < nx + ny + nz; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2] + ((float) (i - nx - ny)) * cstep[2];
  }
  //data->points.free_array(points);
  data->npoints = (nx + ny + nz);
  s = 3 * (nx + ny + nz);
  data->points.set_array(OM_TYPE_FLOAT, points, s, OM_SET_ARRAY_COPY);
  delete [] points;
  data->nnode_data = ndata;
  int array_handler = OM_SET_ARRAY_STATIC;
  if (labels == 0)
    array_handler = OM_SET_ARRAY_COPY;
  for (int i = 0; i < ndata; i++) {
    if (labels != 0) {
      data->node_data[i].veclen = vdims[i];
      string t = labels[i];
      data->node_data[i].labels = t.c_str();
    }
    // Try to avoid having AVS copy the object.
    s = nx * ny * nz * vdims[i];
    int ok = data->node_data[i].values.set_array(OM_TYPE_FLOAT, datavals[i],
						 s, array_handler);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    if (use_ignore) {
      data->node_data[i].null_value = ignore;
      data->node_data[i].null_flag = 1;
      for (xp_long j = 0; j < s; j++) {
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd and datavals[i][j] < (ignore - tol))
          maxd = datavals[i][j];
      }
    } else {
      for (xp_long j = 0; j < s; j++) {
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd)
          maxd = datavals[i][j];
      }
    }
    data->node_data[i].min = mind;
    data->node_data[i].max = maxd;
  }
  return true;
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *mag[], float *phase[], const int nx,
			       const int ny, const int nz,
			       const string labels[], const int ndata,
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  strncpy(message, "BUG: not a wavefunction object", mlen);
  return false;
}

bool DLV::struct_obj::volume(const float origin[3], const float astep[3],
			     const float bstep[3], const float cstep[3],
			     float *mag[], float *phase[], const int nx,
			     const int ny, const int nz, const string labels[],
			     const int ndata, const bool use_ignore,
			     const float ignore, char message[],
			     const int mlen)
{
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  dims[2] = nz;
  data->dims.free_array(dims);
  data->nspace = 3;
  float (*coords)[3] =
    (float (*)[3])data->coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  if (coords == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  // Build the x, y, z node values. Check order.
  float c[3];
  for (int k = 0; k < nz; k++) {
    c[0] = origin[0] + ((float) k) * cstep[0];
    c[1] = origin[1] + ((float) k) * cstep[1];
    c[2] = origin[2] + ((float) k) * cstep[2];
    int kk = k * ny;
    float b[3];
    for (int j = 0; j < ny; j++) {
      b[0] = c[0] + ((float) j) * bstep[0];
      b[1] = c[1] + ((float) j) * bstep[1];
      b[2] = c[2] + ((float) j) * bstep[2];
      int jj = (kk + j) * nx;
      for (int i = 0; i < nx; i++) {
        coords[jj + i][0] = b[0] + ((float) i) * astep[0];
        coords[jj + i][1] = b[1] + ((float) i) * astep[1];
        coords[jj + i][2] = b[2] + ((float) i) * astep[2];
      }
    }
  }
  data->coordinates.values.free_array(coords);
  data->nnode_data = 2 * ndata;
  xp_long s = nx * ny * nz;
  for (int l = 0; l < ndata; l++) {
    data->node_data[l].veclen = 1;
    data->node_data[l].labels = labels[l].c_str();
    // Try to avoid having AVS copy the object.
    int ok = data->node_data[l].values.set_array(OM_TYPE_FLOAT, mag[l],
						 s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume data array", mlen);
      return false;
    }
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    if (use_ignore) {
      data->node_data[l].null_value = ignore;
      data->node_data[l].null_flag = 1;
      for (xp_long j = 0; j < s; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd and mag[l][j] < (ignore - tol))
	  maxd = mag[l][j];
      }
    } else {
      for (xp_long j = 0; j < s; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd)
	  maxd = mag[l][j];
      }
    }
    data->node_data[l].min = mind;
    data->node_data[l].max = maxd;
    data->node_data[ndata + l].veclen = 1;
    data->node_data[ndata + l].labels = "phases";
    // Try to avoid having AVS copy the object.
    ok = data->node_data[ndata + l].values.set_array(OM_TYPE_FLOAT, phase[l],
						     s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume data array", mlen);
      return false;
    }
    data->node_data[ndata + l].min = 0.0;
    data->node_data[ndata + l].max = 2.0 * pi;
  }
  return true;
}

bool DLV::rect_obj::volume(const float origin[3], const float astep[3],
			   const float bstep[3], const float cstep[3],
			   float *mag[], float *phase[], const int nx,
			   const int ny, const int nz, const string labels[],
			   const int ndata, const bool use_ignore,
			   const float ignore, char message[], const int mlen)
{
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  dims[2] = nz;
  data->dims.free_array(dims);
  data->nspace = 3;
  // set the points.
  float (*points)[3] =
    (float (*)[3])data->points.ret_array_ptr(OM_GET_ARRAY_WR);
  if (points == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  for (int i = 0; i < nx; i++) {
    points[i][0] = origin[0] + ((float) i) * astep[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2];
  }
  for (int i = nx; i < nx + ny; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1] + ((float) (i - nx)) * bstep[1];
    points[i][2] = origin[2];
  }
  for (int i = nx + ny; i < nx + ny + nz; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2] + ((float) (i - nx - ny)) * cstep[2];
  }
  data->points.free_array(points);
  data->nnode_data = 2 * ndata;
  xp_long s = nx * ny * nz;
  for (int l = 0; l < ndata; l++) {
    data->node_data[l].veclen = 1;
    data->node_data[l].labels = labels[l].c_str();
    // Try to avoid having AVS copy the object.
    int ok = data->node_data[l].values.set_array(OM_TYPE_FLOAT, mag[l],
					       s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    if (use_ignore) {
      data->node_data[l].null_value = ignore;
      data->node_data[l].null_flag = 1;
      for (xp_long j = 0; j < s; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd and mag[l][j] < (ignore - tol))
	  maxd = mag[l][j];
      }
    } else {
      for (xp_long j = 0; j < s; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd)
	  maxd = mag[l][j];
      }
    }
    data->node_data[l].min = mind;
    data->node_data[l].max = maxd;
    data->node_data[ndata + l].veclen = 1;
    data->node_data[ndata + l].labels = "phases";
    // Try to avoid having AVS copy the object.
    ok = data->node_data[ndata + l].values.set_array(OM_TYPE_FLOAT, phase[l],
						     s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
    data->node_data[ndata + l].min = 0.0;
    data->node_data[ndata + l].max = 2.0 * pi;
  }
  return true;
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *mag[], float *phase[], const int nx,
			       const int ny, const int nz,
			       const string labels[], const int ndata,
			       const int knum[], const int kden[],
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  strncpy(message, "BUG: not a bloch object", mlen);
  return false;
}

bool DLV::struct_obj::volume(const float origin[3], const float astep[3],
			     const float bstep[3], const float cstep[3],
			     float *mag[], float *phase[], const int nx,
			     const int ny, const int nz, const string labels[],
			     const int ndata, const int knum[],
			     const int kden[], const bool use_ignore,
			     const float ignore, char message[],
			     const int mlen)
{
  int ncells = kden[0] * kden[1] * kden[2];
  int na1 = nx - 1;
  int nb1 = ny - 1;
  int nc1 = nz - 1;
  int na = na1 * kden[0] + 1;
  int nb = nb1 * kden[1] + 1;
  int nc = nc1 * kden[2] + 1;
  xp_long size = na * nb * nc;
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = na;
  dims[1] = nb;
  dims[2] = nc;
  data->dims.free_array(dims);
  data->nspace = 3;
  float (*coords)[3] =
    (float (*)[3])data->coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  if (coords == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  // Build the x, y, z node values. Check order.
  int i, j, k;
  for (k = 0; k < nc; k++) {
    float c[3];
    c[0] = origin[0] + ((float) k) * cstep[0];
    c[1] = origin[1] + ((float) k) * cstep[1];
    c[2] = origin[2] + ((float) k) * cstep[2];
    int kk = k * nb;
    for (j = 0; j < nb; j++) {
      float b[3];
      b[0] = c[0] + ((float) j) * bstep[0];
      b[1] = c[1] + ((float) j) * bstep[1];
      b[2] = c[2] + ((float) j) * bstep[2];
      int jk = (kk + j) * na;
      for (i = 0; i < na; i++) {
	coords[jk + i][0] = b[0] + ((float) i) * astep[0];
	coords[jk + i][1] = b[1] + ((float) i) * astep[1];
	coords[jk + i][2] = b[2] + ((float) i) * astep[2];
      }
    }
  }
  ARRfree(coords);
  data->nnode_data = 2 * ndata;
  for (int l = 0; l < ndata; l++) {
    // magnitudes
    data->node_data[l].veclen = 1;
    data->node_data[l].labels = labels[l].c_str();
    int type = OM_TYPE_FLOAT;
    float *values = 0;
    int ndims = 1;
    int ok = data->node_data[l].values.get_array(&type, (void **) &values,
						 &ndims, &size,
						 OM_GET_ARRAY_WR);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume magnitudes array", mlen);
      return false;
    }
    if (ncells == 1) {
      for (j = 0; j < size; j++)
	values[j] = mag[l][j];
    } else {
      int ii, ij, ik, ji, jj, jk, ki, kj, kk, jx, kx;
      // This is based on extend_volume
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  for (i = 0; i < kden[0]; i++) {
	    ii = i * na1;
	    // Inner loops on base data
	    for (kk = 0; kk < nc1; kk++) {
	      kj = (ki + kk) * nb;
	      kx = kk * ny;
	      for (jk = 0; jk < nb1; jk++) {
		jj = (kj + ji + jk) * na;
		jx = (kx + jk) * nx;
		for (ik = 0; ik < na1; ik++) {
		  ij = jj + ii;
		  values[ij + ik] = mag[l][jx + ik];
		}
	      }
	    }
	  }
	}
      }
      // z face
      ki = kden[2] * nc1;
      for (j = 0; j < kden[1]; j++) {
	ji = j * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
	// y, z
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  for (ik = 0; ik < na1; ik++) {
	    ij = jj + ii;
	    values[ij + ik] = mag[l][jx + ik];
	  }
	}
	// x, z
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    ik = 0;
	    ij = jj + ii;
	    values[ij + ik] = mag[l][jx + ik];
	  }
	}
	// x, y, z
	ji = kden[1] * nb1;
	ii = kden[0] * na1;
	kk = 0;
	kj = (ki + kk) * nb;
	kx = kk * ny;
	jk = 0;
	jj = (kj + ji + jk) * na;
	jx = (kx + jk) * nx;
	ik = 0;
	ij = jj + ii;
	values[ij + ik] = mag[l][jx + ik];
      }
      // y face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    jk = 0;
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
	// x, y face
	ii = kden[0] * na1;
	// Inner loops on base data
	for (kk = 0; kk < nc1; kk++) {
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  ik = 0;
	  ij = jj + ii;
	  values[ij + ik] = mag[l][jx + ik];
	}
      }
      // x face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    for (jk = 0; jk < nb1; jk++) {
	      jj = (kj + ji + jk) * na;
	      jx = (kx + jk) * nx;
	      ik = 0;
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
      }
    }
    ARRfree(values);
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    xp_long sz = nx * ny * nz;
    if (use_ignore) {
      data->node_data[l].null_value = ignore;
      data->node_data[l].null_flag = 1;
      for (xp_long j = 0; j < sz; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd and mag[l][j] < (ignore - tol))
	  maxd = mag[l][j];
      }
    } else {
      for (xp_long j = 0; j < sz; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd)
	  maxd = mag[l][j];
      }
    }
    data->node_data[l].min = mind;
    data->node_data[l].max = maxd;
    // Phases
    data->node_data[ndata + l].veclen = 1;
    data->node_data[ndata + l].labels = "phases";
    values = 0;
    ok = data->node_data[ndata + l].values.get_array(&type, (void **) &values,
						     &ndims, &size,
						     OM_GET_ARRAY_WR);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume phases array", mlen);
      return false;
    }
    if (ncells == 1) {
      for (j = 0; j < size; j++)
	values[j] = phase[l][j];
    } else {
      float phx, phy, phz;
      int ii, ij, ik, ji, jj, jk, ki, kj, kk, jx, kx;
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1])
			+ phz);
	  for (i = 0; i < kden[0]; i++) {
	    ii = i * na1;
	    phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			  + phy);
	    // Inner loops on base data
	    for (kk = 0; kk < nc1; kk++) {
	      kj = (ki + kk) * nb;
	      kx = kk * ny;
	      for (jk = 0; jk < nb1; jk++) {
		jj = (kj + ji + jk) * na;
		jx = (kx + jk) * nx;
		for (ik = 0; ik < na1; ik++) {
		  ij = jj + ii;
		  values[ij + ik] = phase[l][jx + ik] + phx;
		}
	      }
	    }
	  }
	}
      }
      // z face
      ki = kden[2] * nc1;
      for (j = 0; j < kden[1]; j++) {
	ji = j * nb1;
	phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1]));
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			+ phy);
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phx;
	    }
	  }
	}
	// y, z
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0]));
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  for (ik = 0; ik < na1; ik++) {
	    ij = jj + ii;
	    values[ij + ik] = phase[l][jx + ik] + phx;
	  }
	}
	// x, z
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1]));
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    ik = 0;
	    ij = jj + ii;
	    values[ij + ik] = phase[l][jx + ik] + phy;
	  }
	}
	// x, y, z
	ji = kden[1] * nb1;
	ii = kden[0] * na1;
	kk = 0;
	kj = (ki + kk) * nb;
	kx = kk * ny;
	jk = 0;
	jj = (kj + ji + jk) * na;
	jx = (kx + jk) * nx;
	ik = 0;
	ij = jj + ii;
	values[ij + ik] = phase[l][jx + ik];
      }
      // y face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			+ phz);
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    jk = 0;
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phx;
	    }
	  }
	}
	// x, y face
	ii = kden[0] * na1;
	// Inner loops on base data
	for (kk = 0; kk < nc1; kk++) {
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  ik = 0;
	  ij = jj + ii;
	  values[ij + ik] = phase[l][jx + ik] + phz;
	}
      }
      // x face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1])
			+ phz);
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    for (jk = 0; jk < nb1; jk++) {
	      jj = (kj + ji + jk) * na;
	      jx = (kx + jk) * nx;
	      ik = 0;
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phy;
	    }
	  }
	}
      }
      const float twopi = (float)(2.0 * pi);
      for (j = 0; j < size; j++) {
	if (values[j] < 0.0)
	  values[j] += twopi;
	else if (values[j] > twopi) {
	  while (values[j] > twopi)
	    values[j] -= twopi;
	}
      }
    }
    ARRfree(values);
    data->node_data[ndata + l].min = 0.0;
    data->node_data[ndata + l].max = 2.0 * pi;
  }
  return true;
}

bool DLV::rect_obj::volume(const float origin[3], const float astep[3],
			   const float bstep[3], const float cstep[3],
			   float *mag[], float *phase[], const int nx,
			   const int ny, const int nz, const string labels[],
			   const int ndata, const int knum[], const int kden[],
			   const bool use_ignore, const float ignore,
			   char message[], const int mlen)
{
  int ncells = kden[0] * kden[1] * kden[2];
  int na1 = nx - 1;
  int nb1 = ny - 1;
  int nc1 = nz - 1;
  int na = na1 * kden[0] + 1;
  int nb = nb1 * kden[1] + 1;
  int nc = nc1 * kden[2] + 1;
  xp_long size = na * nb * nc;
  data->ndim = 3;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = na;
  dims[1] = nb;
  dims[2] = nc;
  data->dims.free_array(dims);
  data->nspace = 3;
  // set the points.
  float (*points)[3] =
    (float (*)[3])data->points.ret_array_ptr(OM_GET_ARRAY_WR);
  if (points == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  // Todo - not sure if this is correct.
  int i, j, k;
  for (i = 0; i < na; i++) {
    points[i][0] = origin[0] + ((float) i) * astep[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2];
  }
  for (i = na; i < na + nb; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1] + ((float) (i - na)) * bstep[1];
    points[i][2] = origin[2];
  }
  for (i = na + nb; i < na + nb + nc; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2] + ((float) (i - na - nb)) * cstep[2];
  }
  ARRfree(points);
  // Magnitudes
  data->nnode_data = 2 * ndata;
  for (int l = 0; l < ndata; l++) {
    data->node_data[l].veclen = 1;
    data->node_data[l].labels = labels[l].c_str();
    float *values = 0;
    int type = OM_TYPE_FLOAT;
    int ndims = 1;
    int ok = data->node_data[l].values.get_array(&type, (void **) &values,
						 &ndims, &size,
						 OM_GET_ARRAY_WR);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume magnitudes array", mlen);
      return false;
    }
    if (ncells == 1) {
      for (j = 0; j < size; j++)
	values[j] = mag[l][j];
    } else {
      int ii, ij, ik, ji, jj, jk, ki, kj, kk, jx, kx;
      // This is based on extend_volume
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  for (i = 0; i < kden[0]; i++) {
	    ii = i * na1;
	    // Inner loops on base data
	    for (kk = 0; kk < nc1; kk++) {
	      kj = (ki + kk) * nb;
	      kx = kk * ny;
	      for (jk = 0; jk < nb1; jk++) {
		jj = (kj + ji + jk) * na;
		jx = (kx + jk) * nx;
		for (ik = 0; ik < na1; ik++) {
		  ij = jj + ii;
		  values[ij + ik] = mag[l][jx + ik];
		}
	      }
	    }
	  }
	}
      }
      // z face
      ki = kden[2] * nc1;
      for (j = 0; j < kden[1]; j++) {
	ji = j * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
	// y, z
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  for (ik = 0; ik < na1; ik++) {
	    ij = jj + ii;
	    values[ij + ik] = mag[l][jx + ik];
	  }
	}
	// x, z
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    ik = 0;
	    ij = jj + ii;
	    values[ij + ik] = mag[l][jx + ik];
	  }
	}
	// x, y, z
	ji = kden[1] * nb1;
	ii = kden[0] * na1;
	kk = 0;
	kj = (ki + kk) * nb;
	kx = kk * ny;
	jk = 0;
	jj = (kj + ji + jk) * na;
	jx = (kx + jk) * nx;
	ik = 0;
	ij = jj + ii;
	values[ij + ik] = mag[l][jx + ik];
      }
      // y face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    jk = 0;
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
	// x, y face
	ii = kden[0] * na1;
	// Inner loops on base data
	for (kk = 0; kk < nc1; kk++) {
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  ik = 0;
	  ij = jj + ii;
	  values[ij + ik] = mag[l][jx + ik];
	}
      }
      // x face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    for (jk = 0; jk < nb1; jk++) {
	      jj = (kj + ji + jk) * na;
	      jx = (kx + jk) * nx;
	      ik = 0;
	      ij = jj + ii;
	      values[ij + ik] = mag[l][jx + ik];
	    }
	  }
	}
      }
    }
    ARRfree(values);
    const float tol = 0.01f;
    float mind = 1e10f, maxd = -1e10f;
    xp_long sz = nx * ny * nz;
    if (use_ignore) {
      data->node_data[l].null_value = ignore;
      data->node_data[l].null_flag = 1;
      for (xp_long j = 0; j < sz; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd and mag[l][j] < (ignore - tol))
	  maxd = mag[l][j];
      }
    } else {
      for (xp_long j = 0; j < sz; j++) {
	if (mag[l][j] < mind)
	  mind = mag[l][j];
	if (mag[l][j] > maxd)
	  maxd = mag[l][j];
      }
    }
    data->node_data[l].min = mind;
    data->node_data[l].max = maxd;
    // Phases
    data->node_data[ndata + l].veclen = 1;
    data->node_data[ndata + l].labels = "phases";
    values = 0;
    ok = data->node_data[ndata + l].values.get_array(&type, (void **) &values,
						     &ndims, &size,
						     OM_GET_ARRAY_WR);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate volume phases array", mlen);
      return false;
    }
    if (ncells == 1) {
      for (j = 0; j < size; j++)
	values[j] = phase[l][j];
    } else {
      int ii, ij, ik, ji, jj, jk, ki, kj, kk, jx, kx;
      float phx, phy, phz;
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1])
			+ phz);
	  for (i = 0; i < kden[0]; i++) {
	    ii = i * na1;
	    phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			  + phy);
	    // Inner loops on base data
	    for (kk = 0; kk < nc1; kk++) {
	      kj = (ki + kk) * nb;
	      kx = kk * ny;
	      for (jk = 0; jk < nb1; jk++) {
		jj = (kj + ji + jk) * na;
		jx = (kx + jk) * nx;
		for (ik = 0; ik < na1; ik++) {
		  ij = jj + ii;
		  values[ij + ik] = phase[l][jx + ik] + phx;
		}
	      }
	    }
	  }
	}
      }
      // z face
      ki = kden[2] * nc1;
      for (j = 0; j < kden[1]; j++) {
	ji = j * nb1;
	phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1]));
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			+ phy);
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phx;
	    }
	  }
	}
	// y, z
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0]));
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  for (ik = 0; ik < na1; ik++) {
	    ij = jj + ii;
	    values[ij + ik] = phase[l][jx + ik] + phx;
	  }
	}
	// x, z
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1]));
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  kk = 0;
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  for (jk = 0; jk < nb1; jk++) {
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    ik = 0;
	    ij = jj + ii;
	    values[ij + ik] = phase[l][jx + ik] + phy;
	  }
	}
	// x, y, z
	ji = kden[1] * nb1;
	ii = kden[0] * na1;
	kk = 0;
	kj = (ki + kk) * nb;
	kx = kk * ny;
	jk = 0;
	jj = (kj + ji + jk) * na;
	jx = (kx + jk) * nx;
	ik = 0;
	ij = jj + ii;
	values[ij + ik] = phase[l][jx + ik];
      }
      // y face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	ji = kden[1] * nb1;
	for (i = 0; i < kden[0]; i++) {
	  ii = i * na1;
	  phx = (float)(2.0 * pi * ((float)(i * knum[0])) / ((float)kden[0])
			+ phz);
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    jk = 0;
	    jj = (kj + ji + jk) * na;
	    jx = (kx + jk) * nx;
	    for (ik = 0; ik < na1; ik++) {
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phx;
	    }
	  }
	}
	// x, y face
	ii = kden[0] * na1;
	// Inner loops on base data
	for (kk = 0; kk < nc1; kk++) {
	  kj = (ki + kk) * nb;
	  kx = kk * ny;
	  jk = 0;
	  jj = (kj + ji + jk) * na;
	  jx = (kx + jk) * nx;
	  ik = 0;
	  ij = jj + ii;
	  values[ij + ik] = phase[l][jx + ik] + phz;
	}
      }
      // x face
      for (k = 0; k < kden[2]; k++) {
	ki = k * nc1;
	phz = (float)(2.0 * pi * ((float)(k * knum[2])) / ((float)kden[2]));
	for (j = 0; j < kden[1]; j++) {
	  ji = j * nb1;
	  phy = (float)(2.0 * pi * ((float)(j * knum[1])) / ((float)kden[1])
			+ phz);
	  ii = kden[0] * na1;
	  // Inner loops on base data
	  for (kk = 0; kk < nc1; kk++) {
	    kj = (ki + kk) * nb;
	    kx = kk * ny;
	    for (jk = 0; jk < nb1; jk++) {
	      jj = (kj + ji + jk) * na;
	      jx = (kx + jk) * nx;
	      ik = 0;
	      ij = jj + ii;
	      values[ij + ik] = phase[l][jx + ik] + phy;
	    }
	  }
	}
      }
      const float twopi = (float)(2.0 * pi);
      for (j = 0; j < size; j++) {
	if (values[j] < 0.0)
	  values[j] += twopi;
	else if (values[j] > twopi) {
	  while (values[j] > twopi)
	    values[j] -= twopi;
	}
      }
    }
    ARRfree(values);
    data->node_data[ndata + l].min = 0.0;
    data->node_data[ndata + l].max = 2.0 * pi;
  }
  return true;
}

bool DLV::drawable_obj::slice(const float origin[3], const float astep[3],
			      const float bstep[3], float *datavals[],
			      const int nx, const int ny,
			      const string labels[], const int ndata,
			      const bool use_ignore, const float ignore,
			      char message[], const int mlen)
{
  strncpy(message, "BUG: not a slice object", mlen);
  return false;
}

bool DLV::struct_obj::slice(const float origin[3], const float astep[3],
			    const float bstep[3], float *datavals[],
			    const int nx, const int ny,
			    const string labels[], const int ndata,
			    const bool use_ignore, const float ignore,
			    char message[], const int mlen)
{
  data->ndim = 2;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate slice array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  data->dims.free_array(dims);
  data->nspace = 3;
  float (*coords)[3] =
    (float (*)[3])data->coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  if (coords == 0) {
    strncpy(message, "Unable to allocate slice coordinate array", mlen);
    return false;
  }
  // Build the x, y, z node values. Check order.
  for (int i = 0; i < ny; i++) {
    float t[3];
    t[0] = origin[0] + i * bstep[0];
    t[1] = origin[1] + i * bstep[1];
    t[2] = origin[2] + i * bstep[2];
    for (int j = 0; j < nx; j++) {
      coords[i * nx + j][0] = t[0] + j * astep[0];
      coords[i * nx + j][1] = t[1] + j * astep[1];
      coords[i * nx + j][2] = t[2] + j * astep[2];
    }
  }
  data->coordinates.values.free_array(coords);
  data->nnode_data = ndata;
  for (int i = 0; i < ndata; i++) {
    data->node_data[i].veclen = 1;
    string t = labels[i];
    data->node_data[i].labels = t.c_str();
    xp_long s = nx * ny;
    // Try to avoid having AVS copy the object.
    int ok = data->node_data[i].values.set_array(OM_TYPE_FLOAT, datavals[i],
						 s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate slice data array", mlen);
      return false;
    }
    if (use_ignore) {
      const float tol = 0.01f;
      float mind = 1e10f, maxd = -1e10f;
      for (int j = 0; j < nx * ny; j++) {
        // Todo - we assume ignore is set at the top
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd and datavals[i][j] < (ignore - tol))
          maxd = datavals[i][j];
      }
      data->node_data[i].min = mind;
      data->node_data[i].max = maxd;
      data->node_data[i].null_value = ignore;
      data->node_data[i].null_flag = 1;
    }
  }
  return true;
}

bool DLV::rect_obj::slice(const float origin[3], const float astep[3],
			  const float bstep[3], float *datavals[],
			  const int nx, const int ny,
			  const string labels[], const int ndata,
			  const bool use_ignore, const float ignore,
			  char message[], const int mlen)
{
  data->ndim = 2;
  xp_long *dims = (xp_long *) data->dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  data->dims.free_array(dims);
  data->nspace = 3;
  // set the points.
  float (*points)[3] =
    (float (*)[3])data->points.ret_array_ptr(OM_GET_ARRAY_WR);
  if (points == 0) {
    strncpy(message, "Unable to allocate volume coordinate array", mlen);
    return false;
  }
  // Todo - not sure if this is correct.
  for (int i = 0; i < nx; i++) {
    points[i][0] = origin[0] + ((float) i) * astep[0];
    points[i][1] = origin[1];
    points[i][2] = origin[2];
  }
  for (int i = nx; i < nx + ny; i++) {
    points[i][0] = origin[0];
    points[i][1] = origin[1] + ((float) (i - nx)) * bstep[1];
    points[i][2] = origin[2];
  }
  data->points.free_array(points);
  data->nnode_data = ndata;
  for (int i = 0; i < ndata; i++) {
    data->node_data[i].veclen = 1;
    string t = labels[i];
    data->node_data[i].labels = t.c_str();
    xp_long s = nx * ny;
    // Try to avoid having AVS copy the object.
    int ok = data->node_data[i].values.set_array(OM_TYPE_FLOAT, datavals[i],
						 s, OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
    if (use_ignore) {
      const float tol = 0.01f;
      float mind = 1e10f, maxd = -1e10f;
      for (int j = 0; j < nx * ny; j++) {
        // Todo - we assume ignore is set at the top
        if (datavals[i][j] < mind)
          mind = datavals[i][j];
        if (datavals[i][j] > maxd and datavals[i][j] < (ignore - tol))
          maxd = datavals[i][j];
      }
      data->node_data[i].min = mind;
      data->node_data[i].max = maxd;
      data->node_data[i].null_value = ignore;
      data->node_data[i].null_flag = 1;
    }
  }
  return true;
}

bool DLV::drawable_obj::plot(float grid[], const int npoints,
			     float *plots[], const string labels[],
			     const int ngraphs, const bool spin,
			     const float xpoints[], const string xlabels[],
			     const int nx, const float yrange[],
			     char message[], const int mlen)
{
  strncpy(message, "BUG: not a plot object", mlen);
  return false;
}

bool DLV::drawable_obj::plot(float *multi_grids[], const int ngrids,
			     int *select_grid, int *grid_sizes,
			     const int npoints, float *plots[],
			     const string labels[], const int ngraphs,
			     const bool spin, const float xpoints[],
			     const string xlabels[], const int nx,
			     const float yrange[], char message[],
			     const int mlen)
{
  strncpy(message, "BUG: not a plot object", mlen);
  return false;
}

bool DLV::rect_dos::plot(float grid[], const int npoints,
			 float *plots[], const string labels[],
			 const int ngraphs, const bool spin,
			 const float xpoints[], const string xlabels[],
			 const int nx, const float yrange[],
			 char message[], const int mlen)
{
  data->field.ndim = 1;
  xp_long *dims = (xp_long *) data->field.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate array dimensions", mlen);
    return false;
  }
  dims[0] = npoints;
  data->field.dims.free_array(dims);
  data->field.nspace = 1;
  data->field.points.set_array(OM_TYPE_FLOAT, grid,
			       (xp_long)npoints, OM_SET_ARRAY_STATIC);
  data->field.nnode_data = ngraphs;
  for (int i = 0; i < ngraphs; i++) {
    data->field.node_data[i].veclen = 1;
    if (labels != 0)
      data->field.node_data[i].labels = labels[i].c_str();
    else
      data->field.node_data[i].labels = "";
    if (spin and i >= ngraphs / 2)
      data->field.myid[i].id = i - (ngraphs / 2);
    else
      data->field.myid[i].id = i;
    // Try to avoid having AVS copy the object.
    int ok = data->field.node_data[i].values.set_array(OM_TYPE_FLOAT, plots[i],
						       (xp_long) npoints,
						       OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
  }
  if (nx > 0) {
    data->x.ndim = 1;
    dims = (xp_long *) data->x.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate x array dimensions", mlen);
      return false;
    }
    dims[0] = 2;
    data->x.dims.free_array(dims);
    data->x.nspace = 1;
    float range[2];
    range[0] = yrange[0];
    range[1] = yrange[1];
    data->x.points.set_array(OM_TYPE_FLOAT, range, 2, OM_SET_ARRAY_COPY);
    data->x.nnode_data = nx;
    for (int i = 0; i < nx; i++) {
      data->x.node_data[i].veclen = 1;
      data->x.node_data[i].labels = xlabels[i].c_str();
      float xval[2];
      xval[0] = xpoints[i];
      xval[1] = xpoints[i];
      data->x.myid[i].id = 18;
      int ok = data->x.node_data[i].values.set_array(OM_TYPE_FLOAT, xval,
						     2, OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate x data array", mlen);
	return false;
      }
    }
  }
  return true;
}

bool DLV::rect_multi::plot(float *multi_grids[], const int ngrids,
			   int *select_grid, int *grid_sizes,
			   const int npoints,
			   float *plots[], const string labels[],
			   const int ngraphs, const bool spin,
			   const float xpoints[], const string xlabels[],
			   const int nx, const float yrange[],
			   char message[], const int mlen)
{
  if (ngrids > 2 || ngrids < 1) {
    strncpy(message, "Multi grids only supports one or two grids", mlen);
    return false;
  }
  CCP3_Renderers_Plots_dos_data *data1 = get_data();
  CCP3_Renderers_Plots_multi_grid_data* data 
    = dynamic_cast<CCP3_Renderers_Plots_multi_grid_data*> (data1); 
  data->select_grid.set_array(OM_TYPE_INT, select_grid,
			      (xp_long)ngraphs, OM_SET_ARRAY_STATIC);
  int *select_index = new int[ngraphs];
  int ngraphs1 = 0;
  int ngraphs2 = 0;    
  for (int i=0; i<ngraphs; i++){
    if (select_grid[i] ==0){
      select_index[i] = ngraphs1;
      ngraphs1++;
    }
    if (select_grid[i] ==1){
      select_index[i] = ngraphs2;
      ngraphs2++;
    }
  }
  data->select_index.set_array(OM_TYPE_INT, select_index,
			       (xp_long)ngraphs, OM_SET_ARRAY_STATIC);
  // Grid 1
  data->field.ndim = 1;
  xp_long *dims = (xp_long *) data->field.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate array dimensions", mlen);
    return false;
  }
  int npts =  grid_sizes[0];
  dims[0] = npts;
  data->field.dims.free_array(dims);
  data->field.nspace = 1;
  data->field.points.set_array(OM_TYPE_FLOAT, multi_grids[0],
			       (xp_long)npts, OM_SET_ARRAY_STATIC);
  data->field.nnode_data = ngraphs1;
  for (int i = 0; i < ngraphs; i++) {
    if(select_grid[i] == 0) {
      data->field.node_data[i].veclen = 1;
      if (labels != 0)
	data->field.node_data[i].labels = labels[i].c_str();
      else
	data->field.node_data[i].labels = "";
      if (spin and i >= ngraphs1 / 2)
	data->field.myid[i].id = i - (ngraphs1 / 2);
      else
	data->field.myid[i].id = i;
      // Try to avoid having AVS copy the object.
      int ok = data->field.node_data[i].values.set_array(OM_TYPE_FLOAT,
							 plots[i],
							 (xp_long) npts,
							 OM_SET_ARRAY_STATIC);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate data array", mlen);
	return false;
      }
    }
  }
  //Grid 2
  data->field2.nnode_data = ngraphs2;
  if(ngraphs2 > 0){
    data->field2.ndim = 1;
    dims = (xp_long *) data->field2.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate array dimensions", mlen);
      return false;
    }
    npts =  grid_sizes[1];
    dims[0] = npts;
    data->field2.dims.free_array(dims);
    data->field2.nspace = 1;
    data->field2.points.set_array(OM_TYPE_FLOAT, multi_grids[1],
				  (xp_long)npts, OM_SET_ARRAY_STATIC);
    int j = -1;
    for (int i = 0; i < ngraphs; i++) {
      if(select_grid[i] == 1) {
	j++;
	data->field2.node_data[j].veclen = 1;
	if (labels != 0)
	  data->field2.node_data[j].labels = labels[i].c_str();
	else
	  data->field2.node_data[j].labels = "";
	if (spin and j >= ngraphs2 / 2)
	  data->field2.myid[j].id = j - (ngraphs2 / 2);
	else
	  data->field2.myid[j].id = j;
	// Try to avoid having AVS copy the object.
	int ok = data->field2.node_data[j].values.set_array(OM_TYPE_FLOAT,
							    plots[i],
							    (xp_long) npts,
							    OM_SET_ARRAY_STATIC);
	if (ok != OM_STAT_SUCCESS) {
	  strncpy(message, "Unable to allocate data array", mlen);
	  return false;
	}
      }
    }
  }
  if (nx > 0) {
    data->x.ndim = 1;
    dims = (xp_long *) data->x.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate x array dimensions", mlen);
      return false;
    }
    dims[0] = 2;
    data->x.dims.free_array(dims);
    data->x.nspace = 1;
    float range[2];
    range[0] = yrange[0];
    range[1] = yrange[1];
    data->x.points.set_array(OM_TYPE_FLOAT, range, 2, OM_SET_ARRAY_COPY);
    data->x.nnode_data = nx;
    for (int i = 0; i < nx; i++) {
      data->x.node_data[i].veclen = 1;
      data->x.node_data[i].labels = xlabels[i].c_str();
      float xval[2];
      xval[0] = xpoints[i];
      xval[1] = xpoints[i];
      data->x.myid[i].id = 18;
      int ok = data->x.node_data[i].values.set_array(OM_TYPE_FLOAT, xval,
						     2, OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate x data array", mlen);
	return false;
      }
    }
  }
  return true;
}


bool DLV::drawable_obj::plot(float grid[], const int npoints,
			     float *plots[], const string labels[],
			     const int ngraphs, const bool spin,
			     const float xpoints[], const string xlabels[],
			     const int nx, const float ypoints[],
			     const string ylabels[], const int ny,
			     const float xrange[], const float yrange[],
			     char message[], const int mlen)
{
  strncpy(message, "BUG: not a plot object", mlen);
  return false;
}

bool DLV::rect_band::plot(float grid[], const int npoints,
			  float *plots[], const string labels[],
			  const int ngraphs, const bool spin,
			  const float xpoints[], const string xlabels[],
			  const int nx, const float ypoints[],
			  const string ylabels[], const int ny,
			  const float xrange[], const float yrange[],
			  char message[], const int mlen)
{
  data->field.ndim = 1;
  xp_long *dims = (xp_long *) data->field.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate array dimensions", mlen);
    return false;
  }
  dims[0] = npoints;
  data->field.dims.free_array(dims);
  data->field.nspace = 1;
  data->field.points.set_array(OM_TYPE_FLOAT, grid,
			       (xp_long)npoints, OM_SET_ARRAY_STATIC);
  data->field.nnode_data = ngraphs;
  for (int i = 0; i < ngraphs; i++) {
    data->field.node_data[i].veclen = 1;
    if (labels != 0)
      data->field.node_data[i].labels = labels[i].c_str();
    else
      data->field.node_data[i].labels = "";
    if (spin and i >= ngraphs / 2)
      data->field.myid[i].id = 1;
    else
      data->field.myid[i].id = 0;      
    // Try to avoid having AVS copy the object.
    int ok = data->field.node_data[i].values.set_array(OM_TYPE_FLOAT, plots[i],
						       (xp_long) npoints,
						       OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
  }
  {
    //if (ny > 0) {
    data->y.ndim = 1;
    dims = (xp_long *) data->y.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate y array dimensions", mlen);
      return false;
    }
    dims[0] = 2;
    data->y.dims.free_array(dims);
    data->y.nspace = 1;
    float range[2];
    range[0] = xrange[0];
    range[1] = xrange[1];
    data->y.points.set_array(OM_TYPE_FLOAT, range, 2, OM_SET_ARRAY_COPY);
    data->y.nnode_data = ny;
    for (int i = 0; i < ny; i++) {
      data->y.node_data[i].veclen = 1;
      data->y.node_data[i].labels = ylabels[i].c_str();
      float yval[2];
      yval[0] = ypoints[i];
      yval[1] = ypoints[i];
      int ok = data->y.node_data[i].values.set_array(OM_TYPE_FLOAT, yval,
						     2, OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate y data array", mlen);
	return false;
      }
    }
  }
  if (nx > 0) {
    data->x.ndim = 1;
    dims = (xp_long *) data->x.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate x array dimensions", mlen);
      return false;
    }
    dims[0] = 2;
    data->x.dims.free_array(dims);
    data->x.nspace = 1;
    float range[2];
    range[0] = yrange[0];
    range[1] = yrange[1];
    data->x.points.set_array(OM_TYPE_FLOAT, range, 2, OM_SET_ARRAY_COPY);
    data->x.nnode_data = nx;
    for (int i = 0; i < nx; i++) {
      data->x.node_data[i].veclen = 1;
      data->x.node_data[i].labels = xlabels[i].c_str();
      float xval[2];
      xval[0] = xpoints[i];
      xval[1] = xpoints[i];
      data->x.myid[i].id = 18;
      int ok = data->x.node_data[i].values.set_array(OM_TYPE_FLOAT, xval,
						     2, OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate x data array", mlen);
	return false;
      }
    }
    data->xmin = xpoints[0];
    data->xmax = xpoints[nx - 1];
  }
  return true;
}

bool DLV::rect_band::plot(float grid[], const int npoints,
			  float *plots[], const string labels[],
			  const int ngraphs, const bool spin,
			  const float xpoints[], const string xlabels[],
			  const int nx, const float ypoints[],
			  const string ylabels[], const int ny,
			  const float xrange[], const float yrange[],
			  float dgrid[], const int dnpoints,
			  float *ddata[], const int dngraphs,
			  char message[], const int mlen)
{
  data->field.ndim = 1;
  xp_long *dims = (xp_long *) data->field.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate array dimensions", mlen);
    return false;
  }
  dims[0] = npoints;
  data->field.dims.free_array(dims);
  data->field.nspace = 1;
  data->field.points.set_array(OM_TYPE_FLOAT, grid,
			       (xp_long)npoints, OM_SET_ARRAY_STATIC);
  data->field.nnode_data = ngraphs;
  for (int i = 0; i < ngraphs; i++) {
    data->field.node_data[i].veclen = 1;
    if (labels != 0)
      data->field.node_data[i].labels = labels[i].c_str();
    else
      data->field.node_data[i].labels = "";
    if (spin and i >= ngraphs / 2)
      data->field.myid[i].id = 1;
    else
      data->field.myid[i].id = 0;      
    // Try to avoid having AVS copy the object.
    int ok = data->field.node_data[i].values.set_array(OM_TYPE_FLOAT, plots[i],
						       (xp_long) npoints,
						       OM_SET_ARRAY_STATIC);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to allocate data array", mlen);
      return false;
    }
  }
  {
    //if (ny > 0) {
    data->y.ndim = 1;
    dims = (xp_long *) data->y.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate y array dimensions", mlen);
      return false;
    }
    dims[0] = 2;
    data->y.dims.free_array(dims);
    data->y.nspace = 1;
    float range[2];
    if (spin)
      // assuming xrange[0] = 0
      range[0] = -0.25f * xrange[1];
    else
      range[0] = xrange[0];
    range[1] = 1.25f * xrange[1];
    data->y.points.set_array(OM_TYPE_FLOAT, range, 2, OM_SET_ARRAY_COPY);
    data->y.nnode_data = ny;
    for (int i = 0; i < ny; i++) {
      data->y.node_data[i].veclen = 1;
      data->y.node_data[i].labels = ylabels[0].c_str();
      float yval[2];
      yval[0] = ypoints[i];
      yval[1] = ypoints[i];
      int ok = data->y.node_data[i].values.set_array(OM_TYPE_FLOAT, yval,
						     2, OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate y data array", mlen);
	return false;
      }
    }
  }
  if (nx > 0 or dngraphs > 0) {
    data->x.ndim = 1;
    dims = (xp_long *) data->x.dims.ret_array_ptr(OM_GET_ARRAY_WR);
    if (dims == 0) {
      strncpy(message, "Unable to allocate x array dimensions", mlen);
      return false;
    }
    dims[0] = dnpoints;
    data->x.dims.free_array(dims);
    data->x.nspace = 1;
    if (nx > 0) {
      data->xmin = xpoints[0];
      data->xmax = xpoints[nx - 1];
    }
    //float range[2];
    //range[0] = yrange[0];
    //range[1] = yrange[1];
    data->x.points.set_array(OM_TYPE_FLOAT, dgrid,
			       (xp_long)dnpoints, OM_SET_ARRAY_STATIC);
    data->x.nnode_data = nx + dngraphs;
    float *xval = new_local_array1(float, dnpoints);
    for (int i = 0; i < nx; i++) {
      data->x.node_data[i].veclen = 1;
      data->x.node_data[i].labels = xlabels[i].c_str();
      data->x.myid[i].id = 18; // antibackground
      for (int j = 0; j < dnpoints; j++)
	xval[j] = xpoints[i];
      int ok = data->x.node_data[i].values.set_array(OM_TYPE_FLOAT, xval,
						     (xp_long)dnpoints,
						     OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate x data array", mlen);
	return false;
      }
    }
    // find limits to scale dos
    float xmax = 0.0;
    for (int i = 0; i < dngraphs; i++) {
      for (int j = 0; j < dnpoints; j++) {
	if (std::abs(ddata[i][j]) > xmax)
	  xmax = std::abs(ddata[i][j]);
      }
    }
    float shift = grid[npoints - 1];
    // 1/6 either side of bands or 1/5 on right
    float scale = shift * 0.25f / xmax;
    float mymin = 1e10;
    float mymax = -1e10;
    for (int i = 0; i < dngraphs; i++) {
      // Todo
      data->x.node_data[nx + i].veclen = 1;
      data->x.node_data[nx + i].labels = "";
      if (spin and i >= dngraphs / 2) {
	data->x.myid[nx + i].id = i - (dngraphs / 2);
	// bands start at 0 so put it on the left
	for (int j = 0; j < dnpoints; j++) {
	  xval[j] = ddata[i][j] * scale;
	  if (mymin > xval[j])
	    mymin = xval[j];
	}
      } else {
	data->x.myid[nx + i].id = i;
	// shift it to the right of the bands
	for (int j = 0; j < dnpoints; j++) {
	  xval[j] = shift + ddata[i][j] * scale;
	  if (mymax < xval[j])
	    mymax = xval[j];
	}
      }
      // Try to avoid having AVS copy the object.
      int ok =
	data->x.node_data[nx + i].values.set_array(OM_TYPE_FLOAT,
						   xval, (xp_long)dnpoints,
						   OM_SET_ARRAY_COPY);
      if (ok != OM_STAT_SUCCESS) {
	strncpy(message, "Unable to allocate x dos array", mlen);
	return false;
      }
    }
    if (dngraphs > 0) {
      if (spin)
	data->xmin = mymin;
      data->xmax = mymax;
    }
    delete_local_array(xval);
  }
  return true;
}

void DLV::drawable_obj::update_atoms_and_bonds(const int natoms,
					       const double x, const double y,
					       const double z,
					       const double length,
					       const double x2,
					       const double y2,
					       const double z2,
					       const double angle,
					       const char sym1[],
					       const char sym2[],
					       const char sym3[],
					       const float r1, const float r2,
					       const float r3, const char [])
{
  // BUG
}

void DLV::atoms_bonds_text_obj::update_atoms_and_bonds(const int natoms,
						       const double x,
						       const double y,
						       const double z,
						       const double length,
						       const double x2,
						       const double y2,
						       const double z2,
						       const double angle,
						       const char sym1[],
						       const char sym2[],
						       const char sym3[],
						       const float r1,
						       const float r2,
						       const float r3,
						       const char group[])
{
  data->natoms = natoms;
  data->x = x;
  data->y = y;
  data->z = z;
  data->length = length;
  data->x2 = x2;
  data->y2 = y2;
  data->z2 = z2;
  data->angle = angle;
  data->sym1 = sym1;
  data->sym2 = sym2;
  data->sym3 = sym3;
  data->rad1 = r1;
  data->rad2 = r2;
  data->rad3 = r3;
  data->groupname = group;
}

void DLV::drawable_obj::update_formula(const string f)
{
  // BUG
}

void DLV::atoms_bonds_text_obj::update_formula(const string f)
{
  data->formula = f.c_str();
}

void DLV::drawable_obj::update_selected_atom(const int index)
{
}

void DLV::atom_text_obj::update_selected_atom(const int index)
{
  if (index < 0)
    OMset_obj_val(data->index.obj_id(), OMnull_obj, 0);
  else
    data->index = index;
}

void DLV::drawable_obj::update_text_value(const string value)
{
}

void DLV::text_obj::update_text_value(const string value)
{
  char message[128];
  (void) set_value(value, message, 128);
}

void DLV::text_view_obj::update_text_value(const string value)
{
  char message[128];
  (void) set_name(value, message, 128);
}

void DLV::drawable_obj::update_text_field(float coords[][3], int **array,
					  const string labels[],
					  const int ndata, const int natoms,
					  char message[], const int mlen)
{
}

void DLV::atom_text_obj::update_text_field(float coords[][3], int **array,
					   const string labels[],
					   const int ndata, const int natoms,
					   char message[], const int mlen)
{
  data->n = ndata;
  data->natoms = natoms;
  xp_long s = natoms * 3;
  int ok = data->coords.set_array(OM_TYPE_FLOAT, coords, s,
				  OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set text coordinates array", mlen);
    return;
  }
  char buff[128];
  for (int i = 0; i < ndata; i++) {
    OMset_str_array_val(data->names.obj_id(), i, labels[i].c_str());
    for (int j = 0; j < natoms; j++) {
      snprintf(buff, 128, "%1d", array[i][j]);
      OMset_str_array_val(data->data[i].labels.obj_id(), j, buff);
    }
  }
}

void DLV::drawable_obj::update_text_field(float coords[][3], float **array,
					  const string labels[],
					  const int ndata, const int natoms,
					  char message[], const int mlen)
{
}

void DLV::atom_text_obj::update_text_field(float coords[][3], float **array,
					   const string labels[],
					   const int ndata, const int natoms,
					   char message[], const int mlen)
{
  data->n = ndata;
  data->natoms = natoms;
  xp_long s = natoms * 3;
  int ok = data->coords.set_array(OM_TYPE_FLOAT, coords, s,
				  OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set text coordinates array", mlen);
    return;
  }
  char buff[128];
  for (int i = 0; i < ndata; i++) {
    OMset_str_array_val(data->names.obj_id(), i, labels[i].c_str());
    for (int j = 0; j < natoms; j++) {
      snprintf(buff, 128, "%8.3f", array[i][j]);
      OMset_str_array_val(data->data[i].labels.obj_id(), j, buff);
    }
  }
}

void DLV::drawable_obj::update_vector_field(float coords[][3],
					    float array[][3], const int natoms,
					    const int cmpt, char message[],
					    const int mlen)
{
}

void DLV::atom_vector_obj::update_vector_field(float coords[][3],
					       float array[][3],
					       const int natoms,
					       const int cmpt,
					       char message[], const int mlen)
{
  xp_long s = natoms * 3;
  //data->field.nnode_data = 1;
  int ok;
  if (cmpt == 0) {
    data->nnodes = natoms;
    ok = data->coordinates.values.set_array(OM_TYPE_FLOAT, coords, s,
					    OM_SET_ARRAY_COPY);
    if (ok != OM_STAT_SUCCESS) {
      strncpy(message, "Unable to set vector coordinates array", mlen);
      return;
    }
  }
  data->node_data[cmpt].veclen = 3;
  ok = data->node_data[cmpt].values.set_array(OM_TYPE_FLOAT, array, s,
					      OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set vector data array", mlen);
    return;
  }
}

void DLV::drawable_obj::update_leed(float (*points)[2], const int npoints,
				    float (*domains)[4], const int ndomains,
				    int *colours)
{
  // BUG
}

void DLV::leed_obj::update_leed(float (*points)[2], const int npoints,
				float (*domains)[4], const int ndomains,
				int *colours)
{
  xp_long size = 2 * npoints;
  int ok = data->base_points.set_array(OM_TYPE_FLOAT, points,
				       size, OM_SET_ARRAY_COPY);
  data->ndomains = ndomains;
  size = 4 * ndomains;
  ok = data->domain_points.set_array(OM_TYPE_FLOAT, domains, size,
				     OM_SET_ARRAY_COPY);
  size = ndomains;
  ok = data->cindex.set_array(OM_TYPE_INT, colours, size, OM_SET_ARRAY_COPY);
}

void DLV::drawable_obj::update_box(const float o[3], const float a[3],
				   const float b[3], const float c[3])
{
  // BUG
}

void DLV::box_obj::update_box(const float o[3], const float a[3],
			      const float b[3], const float c[3])
{
  data->ox = o[0];
  data->oy = o[1];
  data->oz = o[2];
  data->ax = a[0];
  data->ay = a[1];
  data->az = a[2];
  data->bx = b[0];
  data->by = b[1];
  data->bz = b[2];
  data->cx = c[0];
  data->cy = c[1];
  data->cz = c[2];
}

void DLV::drawable_obj::update_sphere(const float o[3], const float radius)
{
  // BUG
}

void DLV::sphere_obj::update_sphere(const float o[3], const float radius)
{
  data->ox = o[0];
  data->oy = o[1];
  data->oz = o[2];
  data->radius = radius;
}

void DLV::drawable_obj::update_plane(const float o[3], const float a[3],
				     const float b[3])
{
  // BUG
}

void DLV::plane_obj::update_plane(const float o[3], const float a[3],
				  const float b[3])
{
  data->ox = o[0];
  data->oy = o[1];
  data->oz = o[2];
  data->ax = a[0];
  data->ay = a[1];
  data->az = a[2];
  data->bx = b[0];
  data->by = b[1];
  data->bz = b[2];
  float m[3][3];
  generate_transform_matrix(o, a, b, m);
  float (*ptr)[4] = (float (*)[4]) data->matrix.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0][0] = m[0][0];
  ptr[0][1] = m[0][1];
  ptr[0][2] = m[0][2];
  ptr[0][3] = 0.0;
  ptr[1][0] = m[1][0];
  ptr[1][1] = m[1][1];
  ptr[1][2] = m[1][2];
  ptr[2][3] = 0.0;
  ptr[2][0] = m[2][0];
  ptr[2][1] = m[2][1];
  ptr[2][2] = m[2][2];
  ptr[2][3] = 0.0;
  ptr[3][0] = 0.0;
  ptr[3][1] = 0.0;
  ptr[3][2] = 0.0;
  ptr[3][3] = 1.0;
  data->matrix.free_array(ptr);
}

void DLV::plane_obj::generate_transform_matrix(const float o[3],
					       const float a[3],
					       const float x[3],
					       float r[3][3])
{
  float lena = 0.0;
  float lenx = 0.0;
  float oa[3];
  float ox[3];
  for (int i = 0; i < 3; i++) {
    oa[i] = a[i] - o[i];
    lena += oa[i] * oa[i];
    ox[i] = x[i] - o[i];
    lenx += ox[i] * ox[i];
  }
  lena = sqrt(lena);
  lenx = sqrt(lenx);
  for (int i = 0; i < 3; i++) {
    oa[i] = oa[i] / lena;
    ox[i] = ox[i] / lenx;
  }
  //sa = lena;
  // We want a plane that is rectangular, so if we map (1, 0, 0) into OA
  // (since we have a translation from (0, 0, 0) to O). Then we want a
  // vector OB in the direction of OX but normal to OA. (We make all these
  // unit vectors and use sa/sb as scales).
  // So if n is the normal of the plane defined by OA and OX. Then OB is
  // normal to n and OA (and the angle between OB and OX < 90).
  float n[3];
  n[0] = oa[1] * ox[2] - oa[2] * ox[1];
  n[1] = oa[2] * ox[0] - oa[0] * ox[2];
  n[2] = oa[0] * ox[1] - oa[1] * ox[0];
  float ob[3];
  ob[0] = n[1] * oa[2] - n[2] * oa[1];
  ob[1] = n[2] * oa[0] - n[0] * oa[2];
  ob[2] = n[0] * oa[1] - n[1] * oa[0];
  float l = sqrt(ob[0] * ob[0] + ob[1] * ob[1] + ob[2] * ob[2]);
  ob[0] = ob[0] / l;
  ob[1] = ob[1] / l;
  ob[2] = ob[2] / l;
  // Make sure we got the angles right (use fact that they are unit vectors)
  float c = ob[0] * ox[0] + ob[1] * ox[1] + ob[2] * ox[2];
  const float tol = 0.001;
  if (c < -tol) {
    ob[0] = - ob[0];
    ob[1] = - ob[1];
    ob[2] = - ob[2];
    c = -c;
  }
  // Adjust length to just contain X in the rectangle.
  //sb = lenx * c;
  // Make sure the plane contains X.
  float ctheta = oa[0] * ox[0] + oa[1] * ox[1] + oa[2] * ox[2];
  float stheta;
  if (ctheta < -tol) {
    // Shift origin along -OA by lenx sin(OX.OB) to contain X then extend
    // sa by same amount to get back to OA.
    stheta = sqrt(1.0 - ctheta * ctheta);
    lenx = stheta * lenx;
    // Todo - was this needed?
    //for (i = 0; i < 3; i++)
    //  o[i] -= stheta * oa[i];
    //sa += lenx;
  }
  // Equations for generalised rotation from Kim(1999), Cam UP, section 4.3
  // Theta is the angle between the 2 normalised vectors, s the normal to the
  // plane that contains them (via a cross product).
  // Rotate (1, 0, 0) into OA, then rotate the rotated (0, 1, 0) about
  // OA into OB.
  float diff1 = fabs(1.0 - oa[0]) + fabs(oa[1]) + fabs(oa[2]);
  float diff2 = fabs(oa[0] + 1.0) + fabs(oa[1]) + fabs(oa[2]);
  float m1[3][3];
  if (diff1 < tol) {
    m1[0][0] = 1.0;
    m1[0][1] = 0.0;
    m1[0][2] = 0.0;
    m1[1][0] = 0.0;
    m1[1][1] = 1.0;
    m1[1][2] = 0.0;
    m1[2][0] = 0.0;
    m1[2][1] = 0.0;
    m1[2][2] = 1.0;
  } else if (diff2 < tol) {
    m1[0][0] = -1.0;
    m1[0][1] = 0.0;
    m1[0][2] = 0.0;
    m1[1][0] = 0.0;
    m1[1][1] = 1.0;
    m1[1][2] = 0.0;
    m1[2][0] = 0.0;
    m1[2][1] = 0.0;
    m1[2][2] = -1.0;
  } else {
    float xyl = oa[1] * oa[1] + oa[2] * oa[2];
    l = sqrt(xyl + oa[0] * oa[0]);
    xyl = sqrt(xyl);
    ctheta = oa[0] / l;
    stheta = xyl / l;
    float s[3];
    s[0] = 0.0;
    s[1] = -oa[2] / xyl;
    s[2] = oa[1] / xyl;
    m1[0][0] = ctheta;
    m1[0][1] = - stheta * s[2];
    m1[0][2] = stheta * s[1];
    m1[1][0] = stheta * s[2];
    m1[1][1] = (s[1] * s[1]) + ctheta * (1.0 - s[1] * s[1]);
    m1[1][2] = (1.0 - ctheta) * s[1] * s[2];
    m1[2][0] = - stheta * s[1];
    m1[2][1] = (1.0 - ctheta) * s[2] * s[1];
    m1[2][2] = s[2] * s[2] + ctheta * (1.0 - s[2] * s[2]);
    n[0] = 1.0;
    n[1] = 0.0;
    n[2] = 0.0;
    //DLV_MVmultn(v, m1, n, 3, true);
    float v[3];
    for (int i = 0; i < 3; i++)
      v[i] = m1[i][0] * n[0] + m1[i][1] * n[1] + m1[i][2] * n[2];
    diff2 = fabs(oa[0] - v[0]) + fabs(oa[1] - v[1]) + fabs(oa[2] - v[2]);
    if (diff2 > tol) {
      // negate stheta
      stheta = - stheta;
      m1[0][0] = ctheta;
      m1[0][1] = - stheta * s[2];
      m1[0][2] = stheta * s[1];
      m1[1][0] = stheta * s[2];
      m1[1][1] = (s[1] * s[1]) + ctheta * (1.0 - s[1] * s[1]);
      m1[1][2] = (1.0 - ctheta) * s[1] * s[2];
      m1[2][0] = - stheta * s[1];
      m1[2][1] = (1.0 - ctheta) * s[2] * s[1];
      m1[2][2] = s[2] * s[2] + ctheta * (1.0 - s[2] * s[2]);
    }
  }
  n[0] = 0.0;
  n[1] = 1.0;
  n[2] = 0.0;
  //DLV_MVmultn(v, m1, n, 3, true);
  float v[3];
  for (int i = 0; i < 3; i++)
    v[i] = m1[i][0] * n[0] + m1[i][1] * n[1] + m1[i][2] * n[2];
  diff2 = fabs(ob[0] - v[0]) + fabs(ob[1] - v[1]) + fabs(ob[2] - v[2]);
  float m2[3][3];
  if (diff2 > tol) {
    l = 1.0;
    float xyl = v[0] * ob[0] + v[1] * ob[1] + v[2] * ob[2];
    ctheta = xyl / l;
    stheta = sqrt(1.0 - ctheta * ctheta);
    //s[0] = v[1] * ob[2] - v[2] * ob[1];
    //s[1] = v[2] * ob[0] - v[0] * ob[2];
    //s[2] = v[0] * ob[1] - v[1] * ob[0];
    float s[3];
    s[0] = oa[0];
    s[1] = oa[1];
    s[2] = oa[2];
    m2[0][0] = (s[0] * s[0]) + ctheta * (1.0 - s[0] * s[0]);
    m2[0][1] = (1.0 - ctheta) * s[0] * s[1] - stheta * s[2];
    m2[0][2] = (1.0 - ctheta) * s[0] * s[2] + stheta * s[1];
    m2[1][0] = (1.0 - ctheta) * s[1] * s[0] + stheta * s[2];
    m2[1][1] = (s[1] * s[1]) + ctheta * (1.0 - s[1] * s[1]);
    m2[1][2] = (1.0 - ctheta) * s[1] * s[2] - stheta * s[0];
    m2[2][0] = (1.0 - ctheta) * s[2] * s[0] - stheta * s[1];
    m2[2][1] = (1.0 - ctheta) * s[2] * s[1] + stheta * s[0];
    m2[2][2] = s[2] * s[2] + ctheta * (1.0 - s[2] * s[2]);
    //DLV_MVmultn(n, m2, v, 3, true);
    for (int i = 0; i < 3; i++)
      n[i] = m2[i][0] * v[0] + m2[i][1] * v[1] + m2[i][2] * v[2];
    diff2 = fabs(ob[0] - n[0]) + fabs(ob[1] - n[1]) + fabs(ob[2] - n[2]);
    if (diff2 > tol) {
      // negate stheta
      stheta = - stheta;
      m2[0][0] = (s[0] * s[0]) + ctheta * (1.0 - s[0] * s[0]);
      m2[0][1] = (1.0 - ctheta) * s[0] * s[1] - stheta * s[2];
      m2[0][2] = (1.0 - ctheta) * s[0] * s[2] + stheta * s[1];
      m2[1][0] = (1.0 - ctheta) * s[1] * s[0] + stheta * s[2];
      m2[1][1] = (s[1] * s[1]) + ctheta * (1.0 - s[1] * s[1]);
      m2[1][2] = (1.0 - ctheta) * s[1] * s[2] - stheta * s[0];
      m2[2][0] = (1.0 - ctheta) * s[2] * s[0] - stheta * s[1];
      m2[2][1] = (1.0 - ctheta) * s[2] * s[1] + stheta * s[0];
      m2[2][2] = s[2] * s[2] + ctheta * (1.0 - s[2] * s[2]);
    }
  } else {
    m2[0][0] = 1.0;
    m2[0][1] = 0.0;
    m2[0][2] = 0.0;
    m2[1][0] = 0.0;
    m2[1][1] = 1.0;
    m2[1][2] = 0.0;
    m2[2][0] = 0.0;
    m2[2][1] = 0.0;
    m2[2][2] = 1.0;
  }
  //DLV_MMmultnn(t, m2, m1, 3, true);
  float t[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      t[i][j] = 0.0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 3; k++)
	t[i][j] += m2[i][k] * m1[k][j];
  // AVS wants the transpose - it seems
  //DLV_Mtranspose(t, r, 3);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      r[i][j] = t[j][i];
}

void DLV::drawable_obj::update_line(const float a[3], const float b[3])
{
  // BUG
}

void DLV::line_obj::update_line(const float a[3], const float b[3])
{
  data->ax = a[0];
  data->ay = a[1];
  data->az = a[2];
  data->bx = b[0];
  data->by = b[1];
  data->bz = b[2];
}

void DLV::drawable_obj::update_point(const float point[3])
{
  // BUG
}

void DLV::point_obj::update_point(const float point[3])
{
  data->x = point[0];
  data->y = point[1];
  data->z = point[2];
}

void DLV::drawable_obj::expand_text_list(const int n)
{
  // BUG
}

void DLV::buffer_view_obj::expand_text_list(const int n)
{
  OMset_array_size(data->buffer, n);
}

void DLV::drawable_obj::add_text_line(const char line[], const int index,
				      const bool append)
{
  // BUG
}

void DLV::buffer_view_obj::add_text_line(const char line[], const int index,
					 const bool append)
{
  if (append) {
    char buff[256];
    char *old_line = buff;
    OMget_str_array_val(data->buffer, index, &old_line, 256);
    string new_line = old_line;
    new_line += line;
    OMset_str_array_val(data->buffer, index, new_line.c_str());
  } else
    OMset_str_array_val(data->buffer, index, line);
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::text_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::text_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::real_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::text_view_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::text_view_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::buffer_view_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::buffer_view_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atoms_bonds_text_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::atoms_bonds_text_obj(p->get_rspace().get_id(), "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_text_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::atom_text_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_vector_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::atom_vector_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::struct_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::struct_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::struct_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::rect_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rect_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::rect_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rect_dos *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::rect_dos(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rect_multi *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::rect_multi(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rect_band *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::rect_band(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::drawable_edit *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::drawable_edit(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::box_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::box_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::box_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::sphere_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::sphere_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::sphere_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::plane_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::plane_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::plane_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::line_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::line_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::line_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::point_obj *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::point_obj *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::point_obj(p->get_rspace().get_id(), sp);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::wulff_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::wulff_obj(p->get_rspace().get_id());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::leed_obj *t,
				    const unsigned int file_version)
    {
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::leed_obj(p->get_rspace().get_id());
    }

  }
}

template <class Archive>
void DLV::drawable_obj::serialize(Archive &ar, const unsigned int version)
{
}

template <class Archive>
void DLV::real_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  double d;
  d = (double)data->data;
  ar & d;
}

template <class Archive>
void DLV::real_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  double d;
  ar & d;
  data->data = d;
}

template <class Archive> void
DLV::atoms_bonds_text_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  string text;
  text = (char *)data->formula;
  ar & text;
}

template <class Archive> void
DLV::atoms_bonds_text_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  string text;
  ar & text;
  data->formula = text.c_str();
}

template <class Archive>
void DLV::atom_vector_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i = data->nnode_data;
  ar & i;
}

template <class Archive>
void DLV::atom_vector_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i;
  ar & i;
  data->nnode_data = i;
  data->nspace = 3;
  data->coordinates.veclen = 3;
}

template <class Archive>
void DLV::atom_text_obj::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  // Todo
}

template <class Archive> void
DLV::struct_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i = (int)data->nnode_data;
  ar & i;
  for (int j = 0; j < i; j++) {
    string text;
    text = (char *)data->node_data[j].labels;
    ar & text;
  }
}

template <class Archive> void
DLV::struct_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i;
  ar & i;
  data->nnode_data = i;
  for (int j = 0; j < i; j++) {
    string text;
    ar & text;
    data->node_data[j].labels = text.c_str();
  }
}

template <class Archive> void
DLV::rect_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i = (int)data->nnode_data;
  ar & i;
  for (int j = 0; j < i; j++) {
    string text;
    text = (char *)data->node_data[j].labels;
    ar & text;
  }
}

template <class Archive> void
DLV::rect_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<drawable_obj>(*this);
  int i;
  ar & i;
  data->nnode_data = i;
  for (int j = 0; j < i; j++) {
    string text;
    ar & text;
    data->node_data[j].labels = text.c_str();
  }
}

//BOOST_CLASS_EXPORT_IMPLEMENTATION(DLV::drawable_obj)
BOOST_CLASS_EXPORT_GUID(DLV::text_obj, "DLV::text_obj")
BOOST_CLASS_EXPORT_GUID(DLV::real_obj, "DLV::real_obj")
BOOST_CLASS_EXPORT_GUID(DLV::text_view_obj, "DLV::text_view_obj")
BOOST_CLASS_EXPORT_GUID(DLV::buffer_view_obj, "DLV::buffer_view_obj")
BOOST_CLASS_EXPORT_GUID(DLV::atoms_bonds_text_obj, "DLV::atoms_bonds_text_obj")
BOOST_CLASS_EXPORT_GUID(DLV::atom_text_obj, "DLV::atom_text_obj")
BOOST_CLASS_EXPORT_GUID(DLV::atom_vector_obj, "DLV::atom_vector_obj")
BOOST_CLASS_EXPORT_GUID(DLV::struct_obj, "DLV::struct_obj")
BOOST_CLASS_EXPORT_GUID(DLV::rect_obj, "DLV::rect_obj")
BOOST_CLASS_EXPORT_GUID(DLV::rect_dos, "DLV::rect_dos")
BOOST_CLASS_EXPORT_GUID(DLV::rect_multi, "DLV::rect_multi")
BOOST_CLASS_EXPORT_GUID(DLV::rect_band, "DLV::rect_band")
BOOST_CLASS_EXPORT_GUID(DLV::drawable_edit, "DLV::drawable_edit")
BOOST_CLASS_EXPORT_GUID(DLV::box_obj, "DLV::box_obj")
BOOST_CLASS_EXPORT_GUID(DLV::sphere_obj, "DLV::sphere_obj")
BOOST_CLASS_EXPORT_GUID(DLV::plane_obj, "DLV::plane_obj")
BOOST_CLASS_EXPORT_GUID(DLV::line_obj, "DLV::line_obj")
BOOST_CLASS_EXPORT_GUID(DLV::point_obj, "DLV::point_obj")
BOOST_CLASS_EXPORT_GUID(DLV::wulff_obj, "DLV::wulff_obj")
BOOST_CLASS_EXPORT_GUID(DLV::leed_obj, "DLV::leed_obj")

#endif // DLV_USES_SERIALIZE
