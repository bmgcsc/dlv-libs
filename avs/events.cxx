
#include <avs/event.h>
#include "../graphics/events.hxx"

void DLV::attach_event(const int socket, void (*fn)(char *), void *ptr)
{
  EVadd_select(EV_SELECT0, socket, fn, 0, (char *)ptr, EV_SEL_IN);
}

void DLV::detach_event(const int socket, void (*fn)(char *), void *ptr)
{
  EVdel_select(EV_SELECT0, socket, fn, 0, (char *)ptr, EV_SEL_IN);
}
