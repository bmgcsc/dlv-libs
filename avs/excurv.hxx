
#ifndef DLV_EXCURV_IFACE
#define DLV_EXCURV_IFACE

namespace EXCURV {

  void set_parameters(const char data[]);
  void set_error(const char message[], const bool expt, const bool pot);
  void clear_error(const bool expt, const bool pot);

}

#endif // DLV_EXCURV_IFACE
