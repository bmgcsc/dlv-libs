
#ifndef DLV_RENDER_BASE
#define DLV_RENDER_BASE

// Forward declare to avoid namespace issues.
class CCP3_Renderers_Base_parent;
class CCP3_Renderers_Base_atoms;
class CCP3_Renderers_Base_outline;
class CCP3_Renderers_Base_shells;
class CCP3_Viewers_Scenes_DLV3Dscene;

namespace DLV {

  class render_parent {
  public:
    render_parent();
    virtual ~render_parent();

    virtual DLVreturn_type
    attach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view) = 0;
    virtual DLVreturn_type
    attach_k3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    virtual DLVreturn_type
    detach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view) = 0;
    virtual DLVreturn_type
    detach_k3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    virtual class toolkit_obj get_rspace() const = 0;
    virtual class toolkit_obj get_kspace() const = 0;
    virtual class toolkit_obj get_redit() const = 0;
    virtual class toolkit_obj get_kedit() const = 0;
    char *get_name() const;
    virtual DLVreturn_type attach_rui(class toolkit_obj &r_ui) const = 0;
    virtual DLVreturn_type attach_kui(class toolkit_obj &k_ui) const;

    virtual void copy_settings(render_parent *p, const bool copy_cell = true,
			       const bool copy_wavefn = false) = 0;
    void set_data_size(const int n);
    void select_data_object(const int n);
    void set_edit_size(const int n);
    void set_data_label(const string label, const int n);
    void set_edit_label(const string label, const int n);
    void set_sub_data_size(const int s, const int n);
    void set_sub_edit_size(const int s, const int n);
    void set_sub_data_label(const string label, const int s, const int n);
    void set_sub_edit_label(const string label, const int s, const int n);
    void set_display_type(const int v, const int n);
    void set_edit_type(const int v, const int n);
    void set_sub_data_vector(const bool v, const int s, const int n);
    void set_sub_edit_vector(const bool v, const int s, const int n);

    void add_display_list(const string name, const int id,
			  const int index) const;
    void reset_display_list(const int object) const;
    void reset_display_list(const int first, const int last) const;
    void empty_display_list() const;
    void add_edit_list(const string name, const int id, const int index) const;
    void reset_edit_list(const int object) const;
    void empty_edit_list() const;

    virtual void set_atom_group_size(const int size);
    virtual void set_atom_group_name(const string name, const int index);
    virtual void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
			       int &cell_type, double &tol) const;
    virtual void set_transforms(const int n, const float transforms[][3]);

    virtual void set_CRYSTAL_wavefn(const int valence_min,
				    const int valence_max, const int nbands,
				    const int lattice, const int centre,
				    const bool spin, const int kpoints[][3],
				    const int nkpoints, const int sa,
				    const int sb, const int sc);
    virtual void set_CRYSTAL_scf();
    virtual void set_CRYSTAL_tddft();
    virtual void set_ONETEP_scf();

    void get_background(float c[3]) const;
    void get_inverted_background(float c[3]) const;
    static void show_data_panel();
    static void stop_animation();
    static void show_animate_panel();
    static render_parent *get_serialize_obj();

    // public for serialize
    void set_serialize_obj();

  protected:
    class toolkit_obj *get_parent() const;
    virtual class CCP3_Renderers_Base_parent *get_object() const = 0;

  private:
    static class toolkit_obj *parent;
    static render_parent *serialize_obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
    }
#endif // DLV_USES_SERIALIZE
  };

  class render_atoms : public render_parent {
  public:
    render_atoms(const char name[]);
    ~render_atoms();

    DLVreturn_type attach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    DLVreturn_type attach_k3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    DLVreturn_type detach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    DLVreturn_type detach_k3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;
    DLVreturn_type attach_kui(class toolkit_obj &r_ui) const;

    void copy_settings(render_parent *p, const bool copy_cell,
		       const bool copy_wavefn);

    void set_atom_group_size(const int size);
    void set_atom_group_name(const string name, const int index);
    void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
		       int &cell_type, double &tol) const;
    void set_transforms(const int n, const float transforms[][3]);

    void set_CRYSTAL_wavefn(const int valence_min, const int valence_max,
			    const int nbands, const int lattice,
			    const int centre, const bool spin,
			    const int kpoints[][3], const int nkpoints,
			    const int sa, const int sb, const int sc);
    void set_CRYSTAL_scf();
    void set_CRYSTAL_tddft();
    void set_ONETEP_scf();

  protected:
    class CCP3_Renderers_Base_parent *get_object() const;

  private:
    class CCP3_Renderers_Base_atoms *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    // Todo - these may need to be moved to inherited ones.
    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class render_outline : public render_parent {
  public:
    render_outline(const char name[]);
    ~render_outline();

    DLVreturn_type attach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    DLVreturn_type detach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;
    void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
		       int &cell_type, double &tol) const;
    void set_transforms(const int n, const float transforms[][3]);

    void copy_settings(render_parent *p, const bool copy_cell,
		       const bool copy_wavefn);

  protected:
    class CCP3_Renderers_Base_parent *get_object() const;

  private:
    class CCP3_Renderers_Base_outline *obj;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class render_shells : public render_parent {
  public:
    render_shells(const char name[]);
    ~render_shells();

    DLVreturn_type attach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    DLVreturn_type detach_r3D(class CCP3_Viewers_Scenes_DLV3Dscene *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;

    void copy_settings(render_parent *p, const bool copy_cell,
		       const bool copy_wavefn);

  protected:
    class CCP3_Renderers_Base_parent *get_object() const;

  private:
    class CCP3_Renderers_Base_shells *obj;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

}

inline DLV::render_parent *DLV::render_parent::get_serialize_obj()
{
  return serialize_obj;
}

inline void DLV::render_parent::set_serialize_obj()
{
  serialize_obj = this;
}

#endif // DLV_RENDER_BASE
