
#ifndef DLV_TOOLKIT_OBJ
#define DLV_TOOLKIT_OBJ

#include <avs/omx.hxx>

namespace DLV {

  class toolkit_obj {
  public:
    toolkit_obj();
    toolkit_obj(const OMobj_id id);

    OMobj_id get_id() const;

  private:
    OMobj_id obj;
  };

}

inline DLV::toolkit_obj::toolkit_obj() : obj(OMnull_obj)
{
}

inline DLV::toolkit_obj::toolkit_obj(const OMobj_id id) : obj(id)
{
}

inline OMobj_id DLV::toolkit_obj::get_id() const
{
  return obj;
}

#endif // DLV_TOOLKIT_OBJ
