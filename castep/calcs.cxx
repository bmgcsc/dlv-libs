
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"

CASTEP::real_l CASTEP::cell_file::get_units(const char buff[], char message[],
					    const int_g mlen)
{
  if (strcmp(buff, "ang") == 0 || strcmp(buff, "ANG") == 0)
    return 1.0;
  else if (strcmp(buff, "bohr") == 0 || strcmp(buff, "BOHR") == 0)
    return DLV::bohr_to_angstrom;
  else {
    strncpy(message, "Units not recognised", mlen);
    return 0.0;
  }
}

DLV::model *CASTEP::cell_file::read(const DLV::string name,
				    const char filename[], char message[],
				    const int_g mlen)
{
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      structure = DLV::model::create_atoms(name, 3);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	int_g natoms = 0;
	char line[256], text[256];
	bool read_data = false;
	real_l scale = 1.0;
	input.getline(line, 256);
	while (!input.eof()) {
	  if (strncmp(line, "%block", 6) == 0 or
	      strncmp(line, "%BLOCK", 6) == 0) {
	    scale = 1.0;
	    sscanf(&line[6], "%s", text);
	    if (strcmp(text, "LATTICE_CART") == 0 or
		strcmp(text, "lattice_cart") == 0) {
	      read_data = true;
	      input.getline(line, 256);
	      sscanf(line, "%s", text);
	      if (isalpha(text[0])) {
		scale = get_units(text, message, mlen);
		if (scale == 0.0) {
		  ok = DLV_ERROR;
		  break;
		}
		input.getline(line, 256);
	      }
	      double aa[3];
	      if (sscanf(line, "%lf %lf %lf", &aa[0], &aa[1], &aa[2]) != 3) {
		strncpy(message, "Error reading a lattice vector", mlen - 1);
		ok = DLV_ERROR;
	      } else {
		DLV::coord_type a[3];
		a[0] = scale * aa[0];
		a[1] = scale * aa[1];
		a[2] = scale * aa[2];
		input.getline(line, 256);
		double bb[3];
		if (sscanf(line, "%lf %lf %lf", &bb[0], &bb[1], &bb[2]) != 3) {
		  strncpy(message, "Error reading b lattice vector", mlen - 1);
		  ok = DLV_ERROR;
		} else {
		  DLV::coord_type b[3];
		  b[0] = scale * bb[0];
		  b[1] = scale * bb[1];
		  b[2] = scale * bb[2];
		  input.getline(line, 256);
		  double cc[3];
		  if (sscanf(line, "%lf %lf %lf", &cc[0], &cc[1],
			     &cc[2]) != 3) {
		    strncpy(message, "Error reading c lattice vector",
			    mlen - 1);
		    ok = DLV_ERROR;
		  } else {
		    DLV::coord_type c[3];
		    c[0] = scale * cc[0];
		    c[1] = scale * cc[1];
		    c[2] = scale * cc[2];
		    if (!structure->set_primitive_lattice(a, b, c)) {
		      strncpy(message, "Error setting lattice vectors",
			      mlen - 1);
		      ok = DLV_ERROR;
		    }
		  }
		}
	      }
	      // skip %endblock
	      input.getline(line, 256);
	    } else if (strcmp(text, "LATTICE_ABC") == 0 or
		       strcmp(text, "lattice_abc") == 0) {
	      read_data = true;
	      input.getline(line, 256);
	      sscanf(line, "%s", text);
	      if (isalpha(text[0])) {
		scale = get_units(text, message, mlen);
		if (scale == 0.0) {
		  ok = DLV_ERROR;
		  break;
		}
		input.getline(line, 256);
	      }
	      double aa, bb, cc;
	      if (sscanf(line, "%lf %lf %lf", &aa, &bb, &cc) != 3) {
		strncpy(message, "Error reading lattice length parameters",
			mlen - 1);
		ok = DLV_ERROR;
	      } else {
		DLV::coord_type a, b, c;
		a = scale * aa;
		b = scale * bb;
		c = scale * cc;
		input.getline(line, 256);
		double alpha, beta, gamma;
		if (sscanf(line, "%lf %lf %lf", &alpha, &beta, &gamma) != 3) {
		  strncpy(message, "Error reading lattice angle parameters",
			  mlen - 1);
		  ok = DLV_ERROR;
		} else {
		  if (!structure->set_lattice_parameters(a, b, c, real_l(alpha),
							 real_l(beta),
							 real_l(gamma))) {
		    strncpy(message, "Error setting lattice vectors",
			    mlen - 1);
		    ok = DLV_ERROR;
		  }
		}
		// skip %endblock
		input.getline(line, 256);
	      }
	    } else if (strcmp(text, "POSITIONS_FRAC") == 0 or
		       strcmp(text, "positions_frac") == 0) {
	      // complete structure to make sure we have lattice vectors
	      structure->complete(false);
	      read_data = true;
	      input.getline(line, 256);
	      while (strncmp(line, "%endblock", 9) != 0 and
		     strncmp(line, "%ENDBLOCK", 9) != 0) {
		if (strlen(line) > 0) {
		  double cx[3];
		  if (sscanf(line, "%s %lf %lf %lf", text, &cx[0],
			     &cx[1], &cx[2]) != 4) {
		    strncpy(message, "Error reading atoms", mlen - 1);
		    ok = DLV_ERROR;
		    break;
		  } else {
		    DLV::coord_type coords[3];
		    coords[0] = cx[0];
		    coords[1] = cx[1];
		    coords[2] = cx[2];
		    DLV::atom my_atom;
		    if (isdigit(text[0])) {
		      int_g id = atoi(text);
		      my_atom = structure->add_fractional_atom(id, coords);
		    } else
		      my_atom = structure->add_fractional_atom(text, coords);
		    if (my_atom == 0) {
		      strncpy(message, "Error adding atom to model", mlen - 1);
		      ok = DLV_ERROR;
		      break;
		    } else
		      natoms++;
		  }
		}
		input.getline(line, 256);
	      }
	    } else if (strcmp(text, "POSITIONS_ABS") == 0 or
		       strcmp(text, "positions_abs") == 0) {
	      read_data = true;
	      input.getline(line, 256);
	      sscanf(line, "%s", text);
	      if (isalpha(text[0])) {
		if (DLV::atom_type::check_atomic_symbol(text) == 0) {
		  scale = get_units(text, message, mlen);
		  if (scale == 0.0) {
		    ok = DLV_ERROR;
		    break;
		  }
		  input.getline(line, 256);
		}
	      }
	      while (strncmp(line, "%endblock", 9) != 0 and
		     strncmp(line, "%ENDBLOCK", 9) != 0) {
		if (strlen(line) > 0) {
		  double cx[3];
		  if (sscanf(line, "%s %lf %lf %lf", text, &cx[0],
			     &cx[1], &cx[2]) != 4) {
		    strncpy(message, "Error reading atoms", mlen - 1);
		    ok = DLV_ERROR;
		    break;
		  } else {
		    DLV::coord_type coords[3];
		    coords[0] = cx[0];
		    coords[1] = cx[1];
		    coords[2] = cx[2];
		    DLV::atom my_atom;
		    if (isdigit(text[0])) {
		      int_g id = atoi(text);
		      my_atom = structure->add_cartesian_atom(id, coords);
		    } else
		      my_atom = structure->add_cartesian_atom(text, coords);
		    if (my_atom == 0) {
		      strncpy(message, "Error adding atom to model", mlen - 1);
		      ok = DLV_ERROR;
		      break;
		    } else
		      natoms++;
		  }
		}
		input.getline(line, 256);
	      }
	    } else if (strncmp(text, "SYMMETRY_OPS", 12) == 0 or
		       strncmp(text, "symmetry_ops", 12) == 0) {
	      read_data = true;
	      real_l rotations[48][3][3];
	      real_l translations[48][3];
	      int_g nops = 0;
	      input.getline(line, 256);
	      while (strncmp(line, "%endblock", 9) != 0 and
		     strncmp(line, "%ENDBLOCK", 9) != 0) {
		if (strlen(line) != 0 and line[0] != '#') {
		  for (int_g j = 0; j < 3; j++) {
		    double r[3];
		    if (sscanf(line, "%lf %lf %lf", &r[0], &r[1], &r[2]) != 3) {
		      ok = DLV_ERROR;
		      break;
		    } else {
		      rotations[nops][j][0] = r[0];
		      rotations[nops][j][1] = r[1];
		      rotations[nops][j][2] = r[2];
		    }
		    input.getline(line, 256);
		  }
		  if (!ok)
		    break;
		  //input.getline(line, 256);
		  double t[3];
		  if (sscanf(line, "%lf %lf %lf", &t[0], &t[1], &t[2]) != 3) {
		    ok = DLV_ERROR;
		    break;
		  } else {
		    translations[nops][0] = t[0];
		    translations[nops][1] = t[1];
		    translations[nops][2] = t[2];
		  }
		  nops++;
		}
		input.getline(line, 256);
	      }
	      if (ok == DLV_OK) {
		if (!structure->set_fractional_trans_ops(rotations,
							 translations, nops)) {
		  strncpy(message, "Error setting symmetry operators",
			  mlen - 1);
		}
	      } else
		strncpy(message, "Error reading symmetry operators",
			mlen - 1);
	    } else {
	      // Something else, just skip it.
	      do {
		input.getline(line, 256);
	      } while (strncmp(line, "%endblock", 9) != 0 and
		       strncmp(line, "%ENDBLOCK", 9) != 0);
	    }
	  }
	  input.getline(line, 256);
	}
	if (natoms == 0) {
	  strncpy(message, "No atoms found in file", 256);
	  ok = DLV_ERROR;
	}
	if (ok != DLV_OK or (!read_data)) {
	  delete structure;
	  structure = 0;
	} else
	  structure->complete();
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}

void CASTEP::cell_file::write(const char filename[],
			      const DLV::model *const structure,
			      const bool fractional,
			      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename, message, mlen)) {
    DLV::coord_type a[3];
    DLV::coord_type b[3];
    DLV::coord_type c[3];
    structure->get_primitive_lattice(a, b, c);
    output << "%BLOCK LATTICE_CART" << '\n';
    output.width(18);
    output.precision(8);
    output << a[0];
    output.precision(8);
    output.width(18);
    output << a[1];
    output.precision(8);
    output.width(18);
    output << a[2];
    output << '\n';
    output.precision(8);
    output.width(18);
    output << b[0];
    output.precision(8);
    output.width(18);
    output << b[1];
    output.precision(8);
    output.width(18);
    output << b[2];
    output << '\n';
    output.precision(8);
    output.width(18);
    output << c[0];
    output.precision(8);
    output.width(18);
    output << c[1];
    output.precision(8);
    output.width(18);
    output << c[2];
    output << '\n';
    output << "%ENDBLOCK LATTICE_CART" << '\n';
    int_g natoms = structure->get_number_of_primitive_atoms();
    int_g *atom_types = new_local_array1(int_g, natoms);
    DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,natoms,3);
    structure->get_primitive_atom_types(atom_types, natoms);
    if (fractional) {
      structure->get_primitive_atom_frac_coords(coords, natoms);
      output << "%BLOCK POSITIONS_FRAC" << '\n';
    } else {
      structure->get_primitive_atom_cart_coords(coords, natoms);
      output << "%BLOCK POSITIONS_ABS" << '\n';
    }
    for (int_g i = 0; i < natoms; i++) {
      output << DLV::atom_type::get_atomic_symbol(atom_types[i]) << ' ';
      output.precision(8);
      output.width(18);
      output << coords[i][0];
      output.precision(8);
      output.width(18);
      output << coords[i][1];
      output.precision(8);
      output.width(18);
      output << coords[i][2];
      output << '\n';
    }
    if (fractional)
      output << "%ENDBLOCK POSITIONS_FRAC" << '\n';
    else
      output << "%ENDBLOCK POSITIONS_ABS" << '\n';
    delete_local_array(coords);
    delete_local_array(atom_types);
    int_g nops = structure->get_number_of_sym_ops();
    if (nops > 1) {
      // Now for the symmetry ops
      real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
      real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
      structure->get_cart_rotation_operators(rotations, nops);
      structure->get_frac_translation_operators(translations, nops);
      output << "%BLOCK SYMMETRY_OPS" << '\n';
      // I think this is right - it seems to be a F90 column major matrix.
      for (int_g i = 0; i < nops; i++) {
	for (int_g j = 0; j < 3; j++) {
	  for (int_g k = 0; k < 3; k++) {
	    output.precision(8);
	    output.width(18);
	    output << rotations[i][k][j];
	  }
	  output << '\n';
	}
	for (int_g j = 0; j < 3; j++) {
	  output.precision(8);
	  output.width(18);
	  output << translations[i][j];
	}
	output << '\n';
      }
      output << "%ENDBLOCK SYMMETRY_OPS" << '\n';
      delete_local_array(translations);
      delete_local_array(rotations);
    }
    output.close();
  }
}
