
#ifndef CASTEP_CALCS
#define CASTEP_CALCS

namespace CASTEP {

  using DLV::int_g;
  using DLV::real_g;
  using DLV::real_l;

  // Interface to CASTEP specific stuff
  class cell_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);
    void write(const char filename[], const DLV::model *const structure,
	       const bool fractional, char message[], const int_g mlen);

  private:
    static real_l get_units(const char buff[], char message[],
			    const int_g mlen);
  };

}

#endif // CASTEP_CALCS
