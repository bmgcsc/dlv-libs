
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/atom_prefs.hxx" // ghosts
#include "calcs.hxx"

DLV::model *CHEMSHELL::pun_file::read(const DLV::string name,
				      const char filename[], char message[],
				      const int_g mlen)
{
  const DLV::string group = "Chemshell regions";
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      structure = DLV::model::create_atoms(name, 0);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	structure->add_atom_group(group);
	int_g natoms = 0;
	char line[256], text[256];
	bool in_atoms = false;
	bool in_charge = false;
	int_g index = 0;
	input.getline(line, 256);
	while (!input.eof()) {
	  if (strncmp(line, "block ", 6) == 0) {
	    in_atoms = (strncmp(line, "block = coordinates", 19) == 0);
	    in_charge = (strncmp(line, "block = atom_charges", 20) == 0);
	    index = 0;
	  } else if (in_atoms) {
	    double cx[3];
	    if (sscanf(line, "%s %lf %lf %lf", text, &cx[0],
		       &cx[1], &cx[2]) != 4) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      ok = DLV_ERROR;
	      break;
	    } else {
	      DLV::coord_type coords[3];
	      for (int_g c = 0; c < 3; c++)
		coords[c] = DLV::bohr_to_angstrom * cx[c];
	      if (isalpha(text[0])) {
		DLV::atom my_atom;
		int_g region = 0;
		if (islower(text[0]))
		  text[0] = toupper(text[0]);
		if (strcmp(text, "F5") == 0) {
		  region = 5;
		  my_atom = structure->add_cartesian_point_charge(coords);
		} else {
		  if (isdigit(text[1])) {
		    region = atoi(&text[1]);
		    text[1] = 0;
		  } else {
		    region = atoi(&text[2]);
		    text[2] = 0;
		  }
		  my_atom = structure->add_cartesian_atom(text, coords);
		}
		if (my_atom == 0) {
		  strncpy(message, "Error adding atom to model", mlen - 1);
		  ok = DLV_ERROR;
		  break;
		} else {
		  char rbuff[16];
		  snprintf(rbuff, 16, "region %d", region);
		  my_atom->set_group(group, rbuff);
		  natoms++;
		}
	      } else {
		strncpy(message,
			"Atom entry doesn't start with symbol", mlen - 1);
		ok = DLV_ERROR;
		break;
	      }
	    }
	  } else if (in_charge) {
	    double charge;
	    if (sscanf(line, "%lf", &charge) == 1)
	      structure->set_point_charge(index, real_l(charge), message, mlen);
	    index++;
	  } // else skip
	  input.getline(line, 256);
	}
	if (natoms == 0) {
	  strncpy(message, "No atoms found in file", 256);
	  ok = DLV_ERROR;
	}
	if (ok != DLV_OK) {
	  delete structure;
	  structure = 0;
	} else
	  structure->complete();
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}

void CHEMSHELL::pun_file::write(const char filename[], const char group[],
				const DLV::model *const structure,
				char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename, message, mlen)) {
    int_g natoms = structure->get_number_of_primitive_atoms();
    int_g *atom_types = new_local_array1(int_g, natoms);
    DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,natoms,3);
    structure->get_primitive_atom_types(atom_types, natoms);
    structure->get_primitive_atom_cart_coords(coords, natoms);
    int_g *regions = new_local_array1(int_g, natoms);
    structure->get_primitive_atom_regions(group, regions, natoms);
    output << "block = coordinates records = " << natoms << '\n';
    for (int_g i = 0; i < natoms; i++) {
      if (atom_types[i] == DLV::point_charge_index)
	output << "F5 ";
      else {
	output << DLV::atom_type::get_atomic_symbol(atom_types[i]);
	output << regions[i] << ' ';
      }
      output.precision(10);
      output.width(18);
      output << (coords[i][0] / DLV::bohr_to_angstrom);
      output.precision(10);
      output.width(18);
      output << (coords[i][1] / DLV::bohr_to_angstrom);
      output.precision(10);
      output.width(18);
      output << (coords[i][2] / DLV::bohr_to_angstrom);
      output << '\n';
    }
    delete_local_array(regions);
    delete_local_array(coords);
    delete_local_array(atom_types);
    output.close();
  }
}
