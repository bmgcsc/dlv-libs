
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "struct.hxx"

DLV::operation *CHEMSHELL::load_structure::create(const char name[],
					       const char filename[],
					       const bool set_bonds,
					       char message[], const int_g mlen)
{
  DLV::string model_name = name_from_file(name, filename);
  DLV::model *structure = read(model_name, filename, message, mlen);
  load_structure *op = 0;
  if (structure != 0) {
    op = new load_structure(structure, filename);
    attach_base(op);
    if (set_bonds)
      op->set_bond_all();
  }
  return op;
}

DLV::string CHEMSHELL::load_structure::get_name() const
{
  return (get_model_name() + " - Load CHEMSHELL pun file");
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CHEMSHELL::load_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)CHEMSHELL::load_structure(0, "recover");
    }

  }
}

template <class Archive>
void CHEMSHELL::load_structure::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CHEMSHELL::load_structure)

DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)

DLV_NORMAL_EXPLICIT(CHEMSHELL::load_structure)

#endif // DLV_USES_SERIALIZE
