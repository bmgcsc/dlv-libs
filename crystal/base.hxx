
#ifndef CRYSTAL_BASE
#define CRYSTAL_BASE

namespace CRYSTAL {

  using DLV::int_g;
  using DLV::int_l;
  using DLV::nat_g;
  using DLV::real_g;
  using DLV::real_l;

  enum version_type {
    CRYSTAL98 = 0, CRYSTAL03 = 1, CRYSTAL06 = 2, CRYSTAL09 = 3,
    CRYSTAL14 = 4, CRYSTAL17 = 5, CRYSTAL23 = 6, CRYSTAL_DEV = 15
  };

  const DLV::string program_name = "CRYSTAL";
  const DLV::string wavefn_label = "Wavefunction";
  const DLV::string fermi_label = "Fermi/HOMO Energy (eV)";
  const DLV::string invalid_fermi_label = "Invalid Fermi Energy";
  const DLV::string prim_atom_label = "Primitive Atoms";
  const DLV::string tddft_label = "TD-DFT_data";

  // make angstrom the consistent DLV unit
  const real_l C09_bohr_to_angstrom = 0.5291772083;

}

#endif // CRYSTAL_BASE
