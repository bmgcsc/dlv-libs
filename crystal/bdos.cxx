
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "plots.hxx"
#include "band.hxx"
#include "dos.hxx"
#include "wavefn.hxx"
#include "bdos.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  ifdef DLV_USES_AVS_GRAPHICS
#  endif // DLV_USES_AVS_GRAPHICS
#  include "../graphics/crystal.hxx"
#endif // ENABLE_DLV_GRAPHICS

DLV::operation *CRYSTAL::band_and_dos::create(const int_g version,
					      char message[], const int_g mlen)
{
  band_and_dos *op = 0;
  message[0] = '\0';
  Density_Matrix dm(0, 0, 0, 0, 0);
  NewK nk(0, 0, 0, 0, false, false, false, false);
  switch (version) {
  case CRYSTAL98:
    strncpy(message, "Band and DOS not supported in CRYSTAL98", mlen);
    break;
  case CRYSTAL03:
    op = new band_and_dos_v5(dm, nk);
    break;
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
    op = new band_and_dos_v6(dm, nk);
    break;
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    op = new band_and_dos_v7(dm, nk);
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::band_and_dos operation",
	      mlen);
  } else {
    message[0] = '\0';
    op->attach_pending();
  }
  return op;
}

bool CRYSTAL::band_and_dos::calculate(const int_g ndp, const int_g npoly,
				      const int_g bmin, const int_g bmax,
				      const int_g nbp, const char path[],
				      const Density_Matrix &dm, const NewK &nk,
				      const int_g version,
				      const DLV::job_setup_data &job,
				      const bool extern_job,
				      const char extern_dir[],
				      char message[], const int_g mlen)
{
  // I could do this via a virtual in DLV::operation, but I don't want to
  // have to alter CCP3core to add CCP3calc options.
  DLV::operation *base = get_editing();
  band_and_dos *op = dynamic_cast<band_and_dos *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not a Band/DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  bool parallel = false;
  if (version == CRYSTAL_DEV)
    parallel = job.is_parallel();
  op->set_params(ndp, npoly, bmin, bmax, nbp, dm, nk);
  (void) accept_pending();
  if (op->create_files(parallel, job.is_local(), message, mlen))
    if (op->make_directory(message, mlen))
      op->write(path, op, op->get_model(), message, mlen);
  if (strlen(message) == 0) {
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(), "", job, parallel,
		extern_job, extern_dir, message, mlen);
    return true;
  } else
    return false;
}

DLV::string CRYSTAL::band_and_dos::get_name() const
{
  return ("CRYSTAL BAND and Density of States calculation");
}

void CRYSTAL::band_and_dos_v5::set_params(const int_g np, const int_g npoly,
					  const int_g bmin, const int_g bmax,
					  const int_g nbp,
					  const Density_Matrix &dm,
					  const NewK &nk)
{
  dos_data_v5::set_params(np, npoly, bmin, bmax, 0.0, 0.0, false, dm, nk);
  band_data::set_params(nbp, bmin, bmax);
}

void CRYSTAL::band_and_dos_v6::set_params(const int_g np, const int_g npoly,
					  const int_g bmin, const int_g bmax,
					  const int_g nbp,
					  const Density_Matrix &dm,
					  const NewK &nk)
{
  dos_data_v6::set_params(np, npoly, bmin, bmax, 0.0, 0.0, false, dm, nk);
  band_data::set_params(nbp, bmin, bmax);
}

void CRYSTAL::band_and_dos_v7::set_params(const int_g np, const int_g npoly,
					  const int_g bmin, const int_g bmax,
					  const int_g nbp,
					  const Density_Matrix &dm,
					  const NewK &nk)
{
  dos_data_v6::set_params(np, npoly, bmin, bmax, 0.0, 0.0, false, dm, nk);
  band_data::set_params(nbp, bmin, bmax);
}

void CRYSTAL::band_and_dos_v5::add_dos_file(const char tag[],
					    const bool is_local)
{
  add_output_file(1, tag, "dos", "fort.24", is_local, true);
}

void CRYSTAL::band_and_dos_v6::add_dos_file(const char tag[],
					    const bool is_local)
{
  add_output_file(1, tag, "dos", "DOSS.DAT", is_local, true);
}

void CRYSTAL::band_and_dos_v7::add_dos_file(const char tag[],
					    const bool is_local)
{
  add_output_file(1, tag, "dos", "DOSS.DAT", is_local, true);
}

void CRYSTAL::band_and_dos_v6::add_calc_error_file(const DLV::string tag,
						   const bool is_parallel,
						   const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::band_and_dos_v7::add_calc_error_file(const DLV::string tag,
						   const bool is_parallel,
						   const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::band_and_dos::create_files(const bool is_parallel,
					 const bool is_local, char message[],
					 const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "bdos";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "bnd", "BAND.DAT", is_local, true);
    add_dos_file(tag, is_local);
    return true;
  } else
    return false;
}

void CRYSTAL::band_and_dos_v5::write(const char path[],
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  if (set_kpath(path, message, mlen)) {
    std::ofstream output;
    if (DLV::open_file_write(output, get_infile_name(0).c_str(),
			     message, mlen)) {
      write_header(output, op, structure, is_binary(),
		   true, !has_fermi_energy());
      write_dos(output, op, structure);
      write_band(output, op, structure);
      output << "END\n";
      output.close();
    }
  }
}

void CRYSTAL::band_and_dos_v6::write(const char path[],
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  if (set_kpath(path, message, mlen)) {
    std::ofstream output;
    if (DLV::open_file_write(output, get_infile_name(0).c_str(),
			     message, mlen)) {
      write_header(output, op, structure, is_binary(),
		   true, !has_fermi_energy());
      write_dos(output, op, structure);
      write_band(output, op, structure);
      output << "END\n";
      output.close();
    }
  }
}

void CRYSTAL::band_and_dos_v7::write(const char path[],
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  if (set_kpath(path, message, mlen)) {
    std::ofstream output;
    if (DLV::open_file_write(output, get_infile_name(0).c_str(),
			     message, mlen)) {
      write_header(output, op, structure, is_binary(),
		   true, !has_fermi_energy());
      write_dos(output, op, structure);
      write_band(output, op, structure);
      output << "END\n";
      output.close();
    }
  }
}

bool CRYSTAL::band_and_dos_v5::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool no_fermi = true;
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi()) {
      read_logfile(name, id);
      no_fermi = false;
    }
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::dos_plot *data = dos_data_v5::read_data(this, 0,
						 get_outfile_name(1).c_str(),
						 id, get_labels(), false, 0.0,
						 message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      bool use_fermi;
      real_g value = 0.0;
      DLV::string fermi;
      bool insulator = false;
      if ((use_fermi = find_fermi_energy(ef, message, len))) {
	value = real_g(ef);
	data->set_xpoints(&value, 1);
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      real_l shift = 0.0;
      if (no_fermi)
	if (find_invalid_fermi(shift, message, len))
	  value = real_g(shift);
      //attach_data(data);
      int_g nk = get_nkpoints();
      DLV::string *labels = 0;
      if (nk > 0) {
	labels = new DLV::string[nk];
	for (int_g i = 0; i < nk; i++)
	  labels[i] = get_klabel(i);
      }
      DLV::panel_plot *data2 =
	band_file_v5::read_data(this, 0, get_outfile_name(0).c_str(), id,
				get_min_band(), use_fermi, value, nk, labels,
				message, len, data);
      if (nk > 0)
	delete [] labels;
      if (data2 == 0)
	return false;
      else {
	if (use_fermi) {
	  value = real_g(ef);
	  data2->set_ypoints(&value, 1);
	  data2->set_ylabel(fermi.c_str(), 0);
	}
	attach_data(data2);
      }
    }
  }
  return true;
}

bool CRYSTAL::band_and_dos_v6::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool no_fermi = true;
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi()) {
      read_logfile(name, id);
      no_fermi = false;
    }
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::dos_plot *data = dos_data_v6::read_data(this, 0,
						 get_outfile_name(1).c_str(),
						 id, get_labels(), false, 0.0,
						 message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      bool use_fermi;
      real_g value = 0.0;
      DLV::string fermi;
      bool insulator = false;
      if ((use_fermi = find_fermi_energy(ef, message, len))) {
	value = real_g(ef);
	data->set_xpoints(&value, 1);
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      real_l shift = 0.0;
      if (no_fermi)
	if (find_invalid_fermi(shift, message, len))
	  value = real_g(shift);
      //attach_data(data);
      int_g nk = get_nkpoints();
      DLV::string *labels = 0;
      if (nk > 0) {
	labels = new DLV::string[nk];
	for (int_g i = 0; i < nk; i++)
	  labels[i] = get_klabel(i);
      }
      DLV::panel_plot *data2 =
	band_file_v5::read_data(this, 0, get_outfile_name(0).c_str(), id,
				get_min_band(), use_fermi, value, nk, labels,
				message, len, data);
      if (nk > 0)
	delete [] labels;
      if (data2 == 0)
	return false;
      else {
	if (use_fermi) {
	  value = real_g(ef);
	  data2->set_ypoints(&value, 1);
	  data2->set_ylabel(fermi.c_str(), 0);
	}
	attach_data(data2);
      }
    }
  }
  return true;
}

bool CRYSTAL::band_and_dos_v7::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool no_fermi = true;
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi()) {
      read_logfile(name, id);
      no_fermi = false;
    }
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band+DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    real_l ef;
    bool use_fermi;
    real_g value = 0.0;
    if ((use_fermi = find_fermi_energy(ef, message, len)))
      value = real_g(ef);
    DLV::dos_plot *data = dos_data_v6::read_data(this, 0,
						 get_outfile_name(1).c_str(),
						 id, get_labels(), use_fermi,
						 value, message, len);
    if (data == 0)
      return false;
    else {
      DLV::string fermi;
      bool insulator = false;
      if (use_fermi) {
	data->set_xpoints(&value, 1);
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      real_l shift = 0.0;
      if (no_fermi)
	if (find_invalid_fermi(shift, message, len))
	  value = real_g(shift);
      //attach_data(data);
      int_g nk = get_nkpoints();
      DLV::string *labels = 0;
      if (nk > 0) {
	labels = new DLV::string[nk];
	for (int_g i = 0; i < nk; i++)
	  labels[i] = get_klabel(i);
      }
      DLV::panel_plot *data2 =
	band_file_v5::read_data(this, 0, get_outfile_name(0).c_str(), id,
				get_min_band(), use_fermi, value, nk, labels,
				message, len, data);
      if (nk > 0)
	delete [] labels;
      if (data2 == 0)
	return false;
      else {
	if (use_fermi) {
	  value = real_g(ef);
	  data2->set_ypoints(&value, 1);
	  data2->set_ylabel(fermi.c_str(), 0);
	}
	attach_data(data2);
      }
    }
  }
  return true;
}

bool CRYSTAL::band_and_dos_v5::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::electron_bdos *v = dynamic_cast<DLV::electron_bdos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands/DOS", mlen);
    return false;
  } else {
    DLV::electron_dos *d = dynamic_cast<DLV::electron_dos *>(v->get_dos());
    if (d != 0) {
      if (dos_data_v5::read_data(this, d, get_outfile_name(1).c_str(),
				 "", get_labels(), false, 0.0, message, mlen)) {
	int_g nk = get_nkpoints();
	DLV::string *labels = 0;
	if (nk > 0) {
	  labels = new DLV::string[nk];
	  for (int_g i = 0; i < nk; i++)
	    labels[i] = get_klabel(i);
	}
	bool ok = (band_file_v5::read_data(this, v, get_outfile_name(0).c_str(),
					   "", get_min_band(), true,
					   get_shift(), nk, labels,
					   message, mlen) != 0);
	if (nk > 0)
	  delete [] labels;
	return ok;
      }
    }
  }
  return false;
}

bool CRYSTAL::band_and_dos_v6::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::electron_bdos *v = dynamic_cast<DLV::electron_bdos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands/DOS", mlen);
    return false;
  } else {
    DLV::electron_dos *d = dynamic_cast<DLV::electron_dos *>(v->get_dos());
    if (d != 0) {
      if (dos_data_v6::read_data(this, d, get_outfile_name(1).c_str(),
				 "", get_labels(), false, 0.0, message, mlen)) {
	int_g nk = get_nkpoints();
	DLV::string *labels = 0;
	if (nk > 0) {
	  labels = new DLV::string[nk];
	  for (int_g i = 0; i < nk; i++)
	    labels[i] = get_klabel(i);
	}
	bool ok = (band_file_v5::read_data(this, v, get_outfile_name(0).c_str(),
					   "", get_min_band(), true,
					   get_shift(), nk, labels,
					   message, mlen) != 0);
	if (nk > 0)
	  delete [] labels;
	return ok;
      }
    }
  }
  return false;
}

bool CRYSTAL::band_and_dos_v7::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::electron_bdos *v = dynamic_cast<DLV::electron_bdos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands/DOS", mlen);
    return false;
  } else {
    DLV::electron_dos *d = dynamic_cast<DLV::electron_dos *>(v->get_dos());
    if (d != 0) {
      if (dos_data_v6::read_data(this, d, get_outfile_name(1).c_str(),
				 "", get_labels(), true, get_shift(),
				 message, mlen)) {
	int_g nk = get_nkpoints();
	DLV::string *labels = 0;
	if (nk > 0) {
	  labels = new DLV::string[nk];
	  for (int_g i = 0; i < nk; i++)
	    labels[i] = get_klabel(i);
	}
	bool ok = (band_file_v5::read_data(this, v, get_outfile_name(0).c_str(),
					   "", get_min_band(), true,
					   get_shift(), nk, labels,
					   message, mlen) != 0);
	if (nk > 0)
	  delete [] labels;
	return ok;
      }
    }
  }
  return false;
}

CRYSTAL::int_g CRYSTAL::band_and_dos_v5::get_proj_type() const
{
  return dos_data::get_proj_type();
}

CRYSTAL::int_g CRYSTAL::band_and_dos_v6::get_proj_type() const
{
  return dos_data::get_proj_type();
}

CRYSTAL::int_g CRYSTAL::band_and_dos_v7::get_proj_type() const
{
  return dos_data::get_proj_type();
}

void CRYSTAL::band_and_dos_v5::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("Band/DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::band_and_dos_v6::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("Bamd/DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::band_and_dos_v7::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("Bamd/DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

bool CRYSTAL::band_and_dos::reset_projections(const int_g ptype,
					      char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  band_and_dos *op = dynamic_cast<band_and_dos *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not Band/DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  op->reset_projections(ptype);
#ifdef ENABLE_DLV_GRAPHICS
  if (ptype == 2)
    CRYSTAL::trigger_bdos_atom_selections();
#endif // ENABLE_DLV_GRAPHICS
  return true;
}

bool CRYSTAL::band_and_dos_v5::add_projection(wavefn_calc *wavefn,
					      const DLV::model *const m,
					      int_g &nproj, DLV::string &pname,
					      const int_g orbitals[],
					      const int_g norbitals,
					      const bool all_atoms,
					      const bool orb_atoms,
					      const bool orb_states,
					      const DLV::atom_integers *data,
					      int_g &ptype, char message[],
					      const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::band_and_dos_v6::add_projection(wavefn_calc *wavefn,
					      const DLV::model *const m,
					      int_g &nproj, DLV::string &pname,
					      const int_g orbitals[],
					      const int_g norbitals,
					      const bool all_atoms,
					      const bool orb_atoms,
					      const bool orb_states,
					      const DLV::atom_integers *data,
					      int_g &ptype, char message[],
					      const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::band_and_dos_v7::add_projection(wavefn_calc *wavefn,
					      const DLV::model *const m,
					      int_g &nproj, DLV::string &pname,
					      const int_g orbitals[],
					      const int_g norbitals,
					      const bool all_atoms,
					      const bool orb_atoms,
					      const bool orb_states,
					      const DLV::atom_integers *data,
					      int_g &ptype, char message[],
					      const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::band_and_dos::add_projection(int_g &nproj, DLV::string &pname,
					   const int_g orbitals[],
					   const int_g norbitals,
					   const bool all_atoms,
					   const bool orb_atoms,
					   const bool orb_states,
					   char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  band_and_dos *op = dynamic_cast<band_and_dos *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not Band/DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  // Find primitive atom labels object
  const DLV::data_object *data = op->find_data(prim_atom_label, program_name);
  if (data == 0) {
    strncpy(message, "Unable to locate primitive atom indices", mlen);
    return false;
  }
  const DLV::atom_integers *s = dynamic_cast<const DLV::atom_integers *>(data);
  if (s == 0) {
    strncpy(message, "Error locating primitive atom indices", mlen);
    return false;
  }
  // The op isn't yet attached, pending is where it would be
  DLV::operation *calc = find_pending_parent(prim_atom_label, program_name);
  if (calc == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
  if (wfn == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  int_g ptype = 0;
  bool ok = op->add_projection(wfn, op->get_model(), nproj, pname, orbitals,
			       norbitals, all_atoms, orb_atoms, orb_states,
			       s, ptype, message, mlen);
  if (ok) {
#ifdef ENABLE_DLV_GRAPHICS
    if (ptype == 1)
      op->set_selected_atom_flags(true);
    if (ptype > 0)
      (void) op->deselect_atoms(message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  } else {
    if (strlen(message) == 0)
      strncpy(message, "Error adding projection", mlen);
  }
  return ok;
}

bool CRYSTAL::band_and_dos::list_orbital_data(int_g &nproj,
					      DLV::string * &pname,
					      const bool all_atoms,
					      const bool orb_atoms,
					      const bool orb_states,
					      char message[], const int_g mlen)
{
  message[0] = '\0';
  const DLV::data_object *data = find_data(prim_atom_label, program_name);
  if (data == 0) {
    strncpy(message, "Unable to locate primitive atom indices", mlen);
    return false;
  }
  const DLV::atom_integers *s = dynamic_cast<const DLV::atom_integers *>(data);
  if (s == 0) {
    strncpy(message, "Error locating primitive atom indices", mlen);
    return false;
  }
  // The op isn't yet attached, pending is where it would be
  DLV::operation *calc = find_pending_parent(prim_atom_label, program_name);
  if (calc == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
  if (wfn == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  return dos_calc::list_orbitals(wfn, get_model(), s, nproj, pname,
				 all_atoms, orb_atoms, orb_states);
}

bool CRYSTAL::band_and_dos::list_orbitals(int_g &nproj, DLV::string * &pname,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  band_and_dos *op = dynamic_cast<band_and_dos *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not Band/DOS calculation", mlen);
    return false;
  }
  return op->list_orbital_data(nproj, pname, all_atoms, orb_atoms, orb_states,
			       message, mlen);
}

void CRYSTAL::band_and_dos::atom_selections_changed()
{
#ifdef ENABLE_DLV_GRAPHICS
  if (get_proj_type() == 2)
    CRYSTAL::trigger_bdos_atom_selections();
#endif // ENABLE_DLV_GRAPHICS
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_and_dos_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_and_dos_v5(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_and_dos_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_and_dos_v6(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_and_dos_v7 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_and_dos_v7(d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::band_and_dos::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::band_and_dos_v5::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_and_dos>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v5>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::band_and_dos_v6::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_and_dos>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v6>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::band_and_dos_v7::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_and_dos>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v6>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v6>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_and_dos_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_and_dos_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_and_dos_v7)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::band_file_v5)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::band_data_v4)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::dos_data_v5)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::band_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::dos_data_v6)

DLV_NORMAL_EXPLICIT(CRYSTAL::band_and_dos_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_and_dos_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_and_dos_v7)

#endif // DLV_USES_SERIALIZE
