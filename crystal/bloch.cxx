
#include <vector>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"
#include "bloch.hxx"

DLV::operation *CRYSTAL::bloch_calc::create(const int_g np, const int_g grid,
					    const int_g minb, const int_g maxb,
					    const int_g nkp, const int_g *kp,
					    const Density_Matrix &dm,
					    const NewK &nk, const int_g version,
					    const DLV::job_setup_data &job,
					    const bool extern_job,
					    const char extern_dir[],
					    char message[], const int_g mlen)
{
  bloch_calc *op = 0;
  message[0] = '\0';
  DLV::data_object *data = DLV::operation::find_3D_region(grid);
  if (data == 0)
    strncpy(message, "Failed to find 3D region", mlen);
  else {
    DLV::box *volume = dynamic_cast<DLV::box *>(data);
    if (volume == 0)
      strncpy(message, "BUG: selected grid is not 3D region", mlen);
    else {
      bool parallel = false;
      switch (version) {
      case CRYSTAL06:
      case CRYSTAL09:
      case CRYSTAL14:
      case CRYSTAL17:
      case CRYSTAL23:
	op = new bloch_calc_v6(np, volume, minb, maxb, nkp, kp, dm, nk);
	break;
      case CRYSTAL_DEV:
	op = new bloch_calc_v6(np, volume, minb, maxb, nkp, kp, dm, nk);
	parallel = job.is_parallel();
	break;
      default:
	strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
	break;
      }
      if (op == 0) {
	if (strlen(message) == 0)
	  strncpy(message, "Failed to allocate CRYSTAL::bloch_calc operation",
		  mlen);
      } else {
	op->attach(); // attach to find wavefunction.
	if (op->create_files(parallel, job.is_local(), message, mlen))
	  if (op->make_directory(message, mlen))
	    op->write(op->get_infile_name(0), op, op->get_model(),
		      message, mlen);
	if (strlen(message) > 0) {
	  // Don't delete once attached
	  //delete op;
	  //op = 0;
	} else {
	  op->execute(op->get_path(), op->get_serial_executable(),
		      op->get_parallel_executable(), "", job, parallel,
		      extern_job, extern_dir, message, mlen);
	}
      }
    }
  }
  return op;
}

DLV::string CRYSTAL::bloch_calc::get_name() const
{
  return ("CRYSTAL Bloch function calculation");
}

DLV::operation *CRYSTAL::load_bloch_data::create(const char filename[],
						 const int_g version,
						 char message[],
						 const int_g mlen)
{
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_bloch_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_bloch_data::get_name() const
{
  return ("Load CRYSTAL Bloch data - " + get_filename());
}

DLV::operation *CRYSTAL::load_bloch_data_v6::create(const char filename[],
						    char message[],
						    const int_g mlen)
{
  load_bloch_data_v6 *op = new load_bloch_data_v6(filename);
  // create_volume needs the model type, so attach first, pending allows detach
  op->attach_pending();
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    op->cancel_pending();
    delete op;
    op = 0;
  } else {
    op->accept_pending();
    // read_data attachs it
    // op->attach_data(data);
  }
  return op;
}

void CRYSTAL::bloch_calc_v6::add_calc_error_file(const DLV::string tag,
						 const bool is_parallel,
						 const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::bloch_calc::create_files(const bool is_parallel,
				       const bool is_local, char message[],
				       const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "bloch";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.31", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::bloch_data::write_input(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      const bool is_bin, char message[],
				      const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, false, false);
    write_command(output);
    output << "NPOINTS\n";
    output << npoints;
    output << '\n';
    write_grid(output, structure);
    write_data_sets(output);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::bloch_data::write_grid(std::ofstream &output,
				     const DLV::model *const structure) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  grid->get_points(o, a, b, c);
  int_g dims = structure->get_number_of_periodic_dims();
  if (dims > 0) {
    o[2] = 0.0;
    if (dims > 1) {
      o[1] = 0.0;
      if (dims > 2)
	o[0] = 0.0;
    }
  }
  if (dims < 3) {
    output << "ORIGIN\n";
    output.width(10);
    output.precision(5);
    output << o[0];
    output.width(10);
    output.precision(5);
    output << o[1];
    output.width(10);
    output.precision(5);
    output << o[2] << '\n';
  }
  if (dims < 1) {
    output << "A_AXIS\n";
    output.width(10);
    output.precision(5);
    output << (a[0] - o[0]);
    output.width(10);
    output.precision(5);
    output << (a[1] - o[1]);
    output.width(10);
    output.precision(5);
    output<< (a[2] - o[2]) << '\n';
  }
  if (dims < 2) {
    output << "B_AXIS\n";
    output.width(10);
    output.precision(5);
    output << (b[0] - o[0]);
    output.width(10);
    output.precision(5);
    output << (b[1] - o[1]);
    output.width(10);
    output.precision(5);
    output << (b[2] - o[2]) << '\n';
  }
  if (dims < 3) {
    output << "C_AXIS\n";
    output.width(10);
    output.precision(5);
    output << (c[0] - o[0]);
    output.width(10);
    output.precision(5);
    output << (c[1] - o[1]);
    output.width(10);
    output.precision(5);
    output << (c[2] - o[2]) << '\n';
  }
}

void CRYSTAL::bloch_data::write_command(std::ofstream &output) const
{
  output << "DLV_3D\n";
}

void CRYSTAL::bloch_data::write_data_sets(std::ofstream &output) const
{
  output << "AMPLITUDE\n";
  output << "MINBAND\n";
  output.precision(5);
  output << min_band << '\n';
  output << "MAXBAND\n";
  output.precision(5);
  output << max_band << '\n';
  output << "NKPOINTS\n";
  output << nkpoints << '\n';
  for (int_g i = 0; i < nkpoints; i++)
    output << (kpoints[i] + 1) << '\n';
  output << "END\n";
}

void CRYSTAL::bloch_calc_v6::write(const DLV::string filename,
				   const DLV::operation *op,
				   const DLV::model *const structure,
				   char message[], const int_g mlen)
{
  write_input(filename, op, structure, is_binary(), message, mlen);
}

DLV::volume_data *CRYSTAL::bloch_file::create_volume(const char code[],
						     const DLV::string id,
						     DLV::operation *op,
						     const int_g nx,
						     const int_g ny,
						     const int_g nz,
						     const real_g origin[3],
						     const real_g astep[3],
						     const real_g bstep[3],
						   const real_g cstep[3]) const
{
  if (op->get_model_type() == 0)
    return new DLV::rspace_wavefunction("CRYSTAL", id, op, nx, ny, nz,
					origin, astep, bstep, cstep);
  else
    return new DLV::bloch_wavefunction("CRYSTAL", id, op, nx, ny, nz,
				       origin, astep, bstep, cstep);
}

void CRYSTAL::bloch_file::get_label(const char line[], char buff[], int_g &k1,
				    int_g &k2, int_g &k3, int_g &i1, int_g &i2,
				    int_g &i3, const bool periodic,
				    bool &spin) const
{
  int_g spin_label = -1;
  int_g band = 0;
  int_g x1 = 0;
  int_g x2 = 0;
  int_g x3 = 0;
  int_g y1 = 1;
  int_g y2 = 1;
  int_g y3 = 1;
  sscanf(line, "%d %d %d %d %d %d %d %d",
	 &spin_label, &x1, &x2, &x3, &y1, &y2, &y3, &band);
  // Setup label
  if (periodic) {
    if (spin_label < 0)
      snprintf(buff, 128, "Band %d", band);
    else if (spin_label == 0)
      snprintf(buff, 128, "Alpha band %d", band);
    else
      snprintf(buff, 128, "Beta band %d", band);
  } else {
    if (spin_label < 0)
      snprintf(buff, 128, "State %d", band);
    else if (spin_label == 0)
      snprintf(buff, 128, "Alpha state %d", band);
    else
      snprintf(buff, 128, "Beta state %d", band);
  }
  if (!spin)
    spin = (spin_label == 1);
  // common denominators
  int_g j = 1;
  if (x1 == 0)
    j = y1;
  else {
    for (int_g i = 2; i <= x1; i++)
      if ((x1 % i) == 0 and (y1 % i) == 0)
	j = i;
  }
  k1 = x1 / j;
  i1 = y1 / j;
  j = 1;
  if (x2 == 0)
    j = y2;
  else {
    for (int_g i = 2; i <= x2; i++)
      if ((x2 % i) == 0 and (y2 % i) == 0)
	j = i;
  }
  k2 = x2 / j;
  i2 = y2 / j;
  j = 1;
  if (x3 == 0)
    j = y3;
  else {
    for (int_g i = 2; i <= x3; i++)
      if ((x3 % i) == 0 and (y3 % i) == 0)
	j = i;
  }
  k3 = x3 / j;
  i3 = y3 / j;
}

bool CRYSTAL::bloch_file::get_data(std::ifstream &input,
				   DLV::volume_data *data,
				   DLV::operation *op, const int_g nx,
				   const int_g ny, const int_g nz,
				   const char label[], char message[],
				   const int_g mlen)
{
  if (nkpoints == 0) {
    size = 1000;
    data_items = new DLV::volume_data *[size];
    for (int_g i = 0; i < size; i++)
      data_items[i] = 0;
    data_items[0] = data;
  }
  char line[128];
  input.getline(line, 128);
  char buff[128];
  bool spin = false;
  int_g k1, k2, k3, s1, s2, s3;
  get_label(line, buff, k1, k2, k3, s1, s2, s3, data->is_periodic(), spin);
  DLV::string id = data_items[0]->get_source();
  DLV::volume_data *cur_data = data_items[0];
  cur_data->set_k_point(k1, k2, k3, s1, s2, s3);
  bool get_data = true;
  bool ok = true;
  bool use_list = false;
  int_g counter = 1;
  while (get_data) {
    if ((ok = read_wvfn(input, cur_data, nx, ny, nz, buff, k1, k2, k3,
			s1, s2, s3, message, mlen))) {
      // Check for next data set - This will be the label
      int_g ok1 = k1;
      int_g ok2 = k2;
      int_g ok3 = k3;
      int_g sk1 = s1;
      int_g sk2 = s2;
      int_g sk3 = s3;
      input.getline(line, 128);
      if (input.eof())
	get_data = false;
      else {
	get_label(line, buff, k1, k2, k3, s1, s2, s3,
		  data->is_periodic(), spin);
	if (spin and !use_list) {
	  counter = 0;
	  use_list = true;
	}
	if (ok1 != k1 or ok2 != k2 or ok3 != k3 or
	    sk1 != s1 or sk2 != s2 or sk3 != s3) {
	  if (use_list)
	    cur_data = data_items[counter];
	  else {
	    if (data_items[counter] == 0) {
	      attach_volume(cur_data);
	      cur_data = data->create("CRYSTAL", id, op);
	      data_items[counter] = cur_data;
	    } else
	      cur_data = data_items[counter];
	    cur_data->set_k_point(k1, k2, k3, s1, s2, s3);
	  }
	  counter++;
	}
      }
    } else
      get_data = false;
  }
  if (nkpoints == 0) {
    attach_volume(cur_data);
    nkpoints = counter;
  }
  return ok;
}

void CRYSTAL::bloch_calc_v6::attach_volume(DLV::volume_data *data)
{
  attach_data(data);
}

bool CRYSTAL::bloch_calc_v6::recover(const bool no_err, const bool log_ok,
				     char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Bloch output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Bloch(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else {
      // Done by read_data
      //attach_data(data);
    }
  }
  return true;
}

void CRYSTAL::load_bloch_data_v6::attach_volume(DLV::volume_data *data)
{
  attach_data(data);
}

bool CRYSTAL::load_bloch_data_v6::reload_data(DLV::data_object *data,
					      char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bloch function", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::bloch_calc_v6::reload_data(DLV::data_object *data,
					 char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bloch function", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_bloch_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_bloch_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::bloch_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::bloch_calc_v6(0, 0, 0, 0, 0, 0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::bloch_file::save(Archive &ar, const unsigned int version) const
{
  //ar & boost::serialization::base_object<CRYSTAL::grid3D_v6>(*this);
  ar & nkpoints;
  for (int_g i = 0; i < nkpoints; i++)
    ar & data_items[i];
}

template <class Archive>
void CRYSTAL::bloch_file::load(Archive &ar, const unsigned int version)
{
  //ar & boost::serialization::base_object<CRYSTAL::grid3D_v6>(*this);
  ar & nkpoints;
  data_items = new DLV::volume_data *[nkpoints];
  for (int_g i = 0; i < nkpoints; i++)
    ar & data_items[i];
  size = nkpoints;
}

template <class Archive>
void CRYSTAL::bloch_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & npoints;
  ar & grid;
  ar & min_band;
  ar & max_band;
  ar & nkpoints;
  for (int i = 0; i < nkpoints; i++)
    ar & kpoints[i];
}

template <class Archive>
void CRYSTAL::bloch_data::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
  else
    ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & npoints;
  ar & grid;
  ar & min_band;
  ar & max_band;
  ar & nkpoints;
  kpoints = new int[nkpoints];
  for (int i = 0; i < nkpoints; i++)
    ar & kpoints[i];
}

template <class Archive>
void CRYSTAL::bloch_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::bloch_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::bloch_file>(*this);
}

template <class Archive>
void CRYSTAL::load_bloch_data::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_bloch_data_v6::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<load_bloch_data>(*this);
  ar & boost::serialization::base_object<bloch_file>(*this);
}

template <class Archive>
void CRYSTAL::bloch_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::bloch_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::bloch_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::bloch_data_v6>(*this);
}

BOOST_CLASS_VERSION(CRYSTAL::bloch_data, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_bloch_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::bloch_calc_v6)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::box)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_bloch_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::bloch_calc_v6)

#endif // DLV_USES_SERIALIZE
