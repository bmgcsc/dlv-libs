
#include <cmath>
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/file.hxx"
#include "../dlv/atom_prefs.hxx" // ghosts
#include "../dlv/extern_model.hxx"
#include "base.hxx"
#include "calcs.hxx"

#ifndef NO_DLV_CALCS

#  ifdef ENABLE_DLV_GRAPHICS
#    include "../graphics/calculations.hxx"

DLV::string CRYSTAL::calculate::get_path() const
{
  return DLV::get_code_path("CRYSTAL");
}
#  else

DLV::string CRYSTAL::calculate::get_path() const
{
  return "";
}

#  endif // ENABLE_DLV_GRAPHICS

#endif // !NO_DLV_CALCS

#ifndef NO_DLV_CALCS

void CRYSTAL::calculate::add_calc_error_file(const DLV::string tag,
					     const bool is_parallel,
					     const bool is_local)
{
  if (is_parallel)
    add_job_error_file(tag, "err", "ERROR", is_local, true);
  else
    add_job_error_file(tag, "err", "fort.7", is_local, true);
}

bool CRYSTAL::calculate::find_wavefunction(DLV::string &filename,
					   bool &is_binary,
					   char message[],
					   const int_g mlen) const
{
  const DLV::data_object *data = find_data(wavefn_label, program_name);
  if (data == 0) {
    strncpy(message, "Couldn't find CRYSTAL wavefunction for calculation",
	    mlen);
    return false;
  }
  const DLV::file_data *file = dynamic_cast<const DLV::file_data *>(data);
  if (file == 0) {
    strncpy(message, "BUG in CRYSTAL wavefunction data", mlen);
    return false;
  }
  filename = file->get_filename();
  is_binary = file->is_binary();
  return true;
}

bool CRYSTAL::calculate::find_tddft_wavefn(DLV::string &filename,
					   char message[],
					   const int_g mlen) const
{
  const DLV::data_object *data = find_data(tddft_label, program_name);
  if (data == 0) {
    strncpy(message, "Couldn't find CRYSTAL TD-DFT file for calculation",
	    mlen);
    return false;
  }
  const DLV::file_data *file = dynamic_cast<const DLV::file_data *>(data);
  if (file == 0) {
    strncpy(message, "BUG in CRYSTAL TD-DFT data", mlen);
    return false;
  }
  filename = file->get_filename();
  return true;
}

bool CRYSTAL::calculate::find_current_wavefn(DLV::string &filename,
					     bool &is_binary,
					     char message[],
					     const int_g mlen) const
{
  // Use when analysis is not yet attached and user can't have changed current
  const DLV::data_object *data = get_current()->find_data(wavefn_label,
							  program_name);
  if (data == 0) {
    strncpy(message, "Couldn't find CRYSTAL wavefunction for calculation",
	    mlen);
    return false;
  }
  const DLV::file_data *file = dynamic_cast<const DLV::file_data *>(data);
  if (file == 0) {
    strncpy(message, "BUG in CRYSTAL wavefunction data", mlen);
    return false;
  }
  filename = file->get_filename();
  is_binary = file->is_binary();
  return true;
}

bool CRYSTAL::calculate::no_errors(char message[], const int_g len)
{
  if (strlen(message) > 0)
    strcat(message, "\n");
  DLV::string name = get_job_error_file();
  if (DLV::check_filename(name.c_str(), message, len)) {
    std::ifstream error_file;
    if (DLV::open_file_read(error_file, name.c_str(), message, len)) {
      bool ok = true;
      char buff[256];
      error_file.getline(buff, 256);
      int_g nerrors = 0;
      int_g nwarnings = 0;
      while (!error_file.eof()) {
        if (strncmp(buff, " ERROR", 6) == 0) {
          ok = false;
          nerrors++;
	  if (strlen(message) + strlen(buff) + 2 < (nat_g)len) {
	    // If message is too big - DLVwarning fails
	    if (nerrors < 5) {
	      strcat(message, buff);
	      strcat(message, "\n");
	    } else if (nerrors == 5)
	      strcat(message, "Further errors/warnings truncated\n");
	  }
	} else
          nwarnings++;
        error_file.getline(buff, 256);
      }
      error_file.close();
      return ok;
    }
    return true; // empty file -> no errors
  }
  return false;
}

bool CRYSTAL::calc_geom::is_geometry() const
{
  return true;
}

void CRYSTAL::calc_geom::inherit_model()
{
  // Null op, we have a new structure
}

void CRYSTAL::calc_geom::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}

#endif // !NO_DLV_CALCS


#if defined(DLV_USES_SERIALIZE) && !defined(NO_DLV_CALCS)

template <class Archive>
void CRYSTAL::calculate::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::batch_calc>(*this);
}

template <class Archive>
void CRYSTAL::calc_geom::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<calculate>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::calculate)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::calc_geom)

DLV_SUPPRESS_TEMPLATES(DLV::batch_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::calculate)
DLV_NORMAL_EXPLICIT(CRYSTAL::calc_geom)
DLV_EXPORT_EXPLICIT(CRYSTAL::calc_geom)

#endif // DLV_USES_SERIALIZE
