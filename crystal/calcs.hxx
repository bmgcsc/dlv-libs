
#ifndef CRYSTAL_CALCS
#define CRYSTAL_CALCS

namespace CRYSTAL {

#ifndef NO_DLV_CALCS

  class calculate : public DLV::batch_calc {
  protected:
    bool find_wavefunction(DLV::string &filename, bool &is_binary,
			   char message[], const int_g mlen) const;
    bool find_tddft_wavefn(DLV::string &filename, char message[],
			   const int_g mlen) const;
    bool find_current_wavefn(DLV::string &filename, bool &is_binary,
			     char message[], const int_g mlen) const;
    DLV::string get_path() const;
    bool no_errors(char message[], const int_g len);
    virtual void add_calc_error_file(const DLV::string tag,
				     const bool is_parallel,
				     const bool is_local);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class calc_geom : public calculate {
  public:
    bool is_geometry() const;

  protected:
    void inherit_model();
    void inherit_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#endif // !NO_DLV_CALCS

}

#if defined(DLV_USES_SERIALIZE) && !defined(NO_DLV_CALCS)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(CRYSTAL::calculate)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(CRYSTAL::calc_geom)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::calc_geom)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::calculate)
#endif // DLV_USES_SERIALIZE

#endif // CRYSTAL_CALCS
