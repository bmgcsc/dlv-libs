
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"
#include "density.hxx"

DLV::operation *CRYSTAL::dlv_3d_calc::create(const bool do_cd,
					     const bool do_spin,
					     const bool do_pot,
					     const bool do_efield,
					     const bool do_density,
					     const int_g np, const int_g grid,
					     const int_g pol, const int_g pot,
					     const int_g minb, const int_g maxb,
					     const real_g spacing,
					     const bool use_np,
					     const bool do_ldr,
					     const bool do_part,
					     const bool do_hole,
					     const bool do_ov,
					     const int_g mine,
					     const int_g maxe,
					     const Density_Matrix &dm,
					     const NewK &nk,
					     const int_g version,
					     const DLV::job_setup_data &job,
					     const bool extern_job,
					     const char extern_dir[],
					     char message[], const int_g mlen)
{
  dlv_3d_calc *op = 0;
  message[0] = '\0';
  DLV::data_object *data = DLV::operation::find_3D_region(grid);
  if (data == 0)
    strncpy(message, "Failed to find 3D region", mlen);
  else {
    DLV::box *volume = dynamic_cast<DLV::box *>(data);
    if (volume == 0)
      strncpy(message, "BUG: selected grid is not 3D region", mlen);
    else {
      bool parallel = false;
      switch (version) {
      case CRYSTAL06:
      case CRYSTAL09:
	op = new dlv_3d_calc_v6(do_cd, do_spin, do_pot, do_efield, do_density,
				np, volume, pol, pot, minb, maxb, dm, nk);
	break;
      case CRYSTAL14:
      case CRYSTAL17:
      case CRYSTAL23:
	op = new dlv_3d_calc_v8(do_cd, do_spin, do_pot, do_efield, do_density,
				np, volume, pol, pot, minb, maxb, spacing,
				use_np, do_ldr, do_part, do_hole, do_ov,
				mine, maxe, dm, nk);
	break;
      case CRYSTAL_DEV:
	op = new dlv_3d_calc_v8(do_cd, do_spin, do_pot, do_efield, do_density,
				np, volume, pol, pot, minb, maxb, spacing,
				use_np, do_ldr, do_part, do_hole, do_ov,
				mine, maxe, dm, nk);
	parallel = job.is_parallel();
	break;
      default:
	strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
	break;
      }
      if (op == 0) {
	if (strlen(message) == 0)
	  strncpy(message, "Failed to allocate CRYSTAL::dlv_3d_calc operation",
		  mlen);
      } else {
	op->attach(); // attach to find wavefunction.
	if (op->create_files(parallel, job.is_local(), message, mlen))
	  if (op->make_directory(message, mlen))
	    op->write(op->get_infile_name(0), op, op->get_model(),
		      message, mlen);
	if (strlen(message) > 0) {
	  // Don't delete once attached
	  //delete op;
	  //op = 0;
	} else {
	  op->execute(op->get_path(), op->get_serial_executable(),
		      op->get_parallel_executable(), "", job, parallel,
		      extern_job, extern_dir, message, mlen);
	}
      }
    }
  }
  return op;
}

bool CRYSTAL::dlv_3d_calc::is_tddft() const
{
  return false;
}

bool CRYSTAL::dlv_3d_calc_v8::is_tddft() const
{
  return check_tddft();
}

DLV::string CRYSTAL::dlv_3d_calc::get_name() const
{
  return ("CRYSTAL DLV3D Grid calculation");
}

DLV::operation *CRYSTAL::load_dlv3d_data::create(const char filename[],
						 const int_g version,
						 char message[],
						 const int_g mlen)
{
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_dlv3d_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_dlv3d_data::get_name() const
{
  return ("Load CRYSTAL DLV3D data - " + get_filename());
}

DLV::operation *CRYSTAL::load_dlv3d_data_v6::create(const char filename[],
						    char message[],
						    const int_g mlen)
{
  load_dlv3d_data_v6 *op = new load_dlv3d_data_v6(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::dlv_3d_calc_v6::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::dlv_3d_calc_v8::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::dlv_3d_calc::create_files(const bool is_parallel,
					const bool is_local, char message[],
					const int_g mlen)
{
  DLV::string filename;
  DLV::string tddftname;
  bool ok = find_wavefunction(filename, binary_wvfn, message, mlen);
  if (ok and is_tddft())
    ok = find_tddft_wavefn(tddftname, message, mlen);
  if (ok) {
    static char tag[] = "d3d";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    if (is_tddft())
      add_data_file(2, tddftname, "TDDFT.RESTART", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.31", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::dlv_3d_data::write_input(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       const bool is_bin, char message[],
				       const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, false, false, false);
    write_command(output);
    output << "NPOINTS\n";
    output << npoints;
    output << '\n';
    write_grid(output);
    write_data_sets(output);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::dlv_3d_data::write_input(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       const real_g spacing,
				       const bool set_points, const bool is_bin,
				       char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, false, false, false);
    write_command(output);
    if (set_points) {
      output << "NPOINTS\n";
      output << npoints;
    } else {
      output << "STEPSIZE\n";
      output << spacing;
    }
    output << '\n';
    write_grid(output);
    write_data_sets(output);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::dlv_3d_data_v8::write_input(const DLV::string filename,
					  const DLV::operation *op,
					  const DLV::model *const structure,
					  const bool is_bin, char message[],
					  const int_g mlen)
{
  dlv_3d_data::write_input(filename, op, structure, grid_spacing, set_points,
			   is_bin, message, mlen);
}

void CRYSTAL::dlv_3d_data::write_command(std::ofstream &output) const
{
  output << "DLV_3D\n";
}

void CRYSTAL::dlv_3d_data::write_grid(std::ofstream &output) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  grid->get_points(o, a, b, c);
  output << "ORIGIN\n";
  output.width(10);
  output.precision(5);
  output << o[0];
  output << ' ';
  output.width(10);
  output.precision(5);
  output << o[1];
  output << ' ';
  output.width(10);
  output.precision(5);
  output << o[2] << '\n';
  output << "A_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (a[0] - o[0]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output << (a[1] - o[1]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output<< (a[2] - o[2]) << '\n';
  output << "B_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (b[0] - o[0]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output << (b[1] - o[1]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output << (b[2] - o[2]) << '\n';
  output << "C_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (c[0] - o[0]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output << (c[1] - o[1]);
  output << ' ';
  output.width(10);
  output.precision(5);
  output << (c[2] - o[2]) << '\n';
}

void CRYSTAL::dlv_3d_data::write_data_sets(std::ofstream &output) const
{
  if (calc_charge)
    output << "CHARGE\n";
  if (calc_spin)
    output << "SPIN\n";
  if (calc_potential) {
    output << "POTENTIAL\n";
    output.precision(5);
    output << pol_tol << ' ' << pot_tol << '\n';
  }
  if (calc_efield) {
    output << "EFIELD\n";
    output.precision(5);
    output << pol_tol << ' ' << pot_tol << '\n';
  }
  if (calc_density) {
    output << "DENSITY\n";
    output << "MINBAND\n";
    output.precision(5);
    output << min_band << '\n';
    output << "MAXBAND\n";
    output.precision(5);
    output << max_band << '\n';
  }
  // A_AXIS, B_AXIS, C_AXIS, ORIGIN
  write_tddft(output);
  output << "END\n";
}

void CRYSTAL::dlv_3d_data::write_tddft(std::ofstream &output) const
{
}

void CRYSTAL::dlv_3d_data_v8::write_tddft(std::ofstream &output) const
{
  if (check_tddft()) {
    if (calc_response)
      output << "RESPONSE\n";
    if (calc_particle)
      output << "PARTICLE\n";
    if (calc_hole)
      output << "HOLE\n";
    if (calc_overlap)
      output << "OVERLAP\n";
    output << "MINEXCITE\n";
    output.precision(5);
    output << min_excitation << '\n';
    output << "MAXEXCITE\n";
    output.precision(5);
    output << max_excitation << '\n';
  }
}

void CRYSTAL::dlv_3d_calc_v6::write(const DLV::string filename,
				    const DLV::operation *op,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  write_input(filename, op, structure, is_binary(), message, mlen);
}

void CRYSTAL::dlv_3d_calc_v8::write(const DLV::string filename,
				    const DLV::operation *op,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  write_input(filename, op, structure, is_binary(), message, mlen);
}

bool CRYSTAL::dlv_3d_calc_v6::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DLV3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DLV3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(), id,
				       message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::dlv_3d_calc_v8::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DLV3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DLV3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(), id,
				       message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_dlv3d_data_v6::reload_data(DLV::data_object *data,
					      char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::dlv_3d_calc_v6::reload_data(DLV::data_object *data,
					  char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

bool CRYSTAL::dlv_3d_calc_v8::reload_data(DLV::data_object *data,
					  char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    /*
    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_dlv3d_data *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_dlv3d_data("recover");
    }
    */

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_dlv3d_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_dlv3d_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dlv_3d_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dlv_3d_calc_v6(false, false, false, false, false,
				      0, 0, 0, 0, 0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dlv_3d_calc_v8 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dlv_3d_calc_v8(false, false, false, false, false,
				      0, 0, 0, 0, 0, 0, 0.0, true, false,
				      false, false, false, 0, 0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::dlv_3d_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & calc_charge;
  ar & calc_spin;
  ar & calc_potential;
  ar & calc_efield;
  ar & calc_density;
  ar & npoints;
  ar & grid;
  ar & pol_tol;
  ar & pot_tol;
  ar & min_band;
  ar & max_band;
}

template <class Archive>
void CRYSTAL::dlv_3d_data::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
  else
    ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & calc_charge;
  ar & calc_spin;
  ar & calc_potential;
  ar & calc_efield;
  ar & calc_density;
  ar & npoints;
  ar & grid;
  ar & pol_tol;
  ar & pot_tol;
  ar & min_band;
  ar & max_band;
}

template <class Archive>
void CRYSTAL::dlv_3d_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::dlv_3d_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_data>(*this);
  ar & grid_spacing;
  ar & set_points;
  ar & calc_response;
  ar & calc_particle;
  ar & calc_hole;
  ar & calc_overlap;
  ar & min_excitation;
  ar & max_excitation;
}

template <class Archive>
void CRYSTAL::load_dlv3d_data::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_dlv3d_data_v6::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<load_dlv3d_data>(*this);
}

template <class Archive>
void CRYSTAL::dlv_3d_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::dlv_3d_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::dlv_3d_calc_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dlv_3d_data_v8>(*this);
}

BOOST_CLASS_VERSION(CRYSTAL::dlv_3d_data, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_dlv3d_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dlv_3d_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dlv_3d_calc_v8)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::box)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_dlv3d_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::dlv_3d_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::dlv_3d_calc_v8)

#endif // DLV_USES_SERIALIZE
