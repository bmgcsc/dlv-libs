
#ifndef CRYSTAL_3D_DLVDATA
#define CRYSTAL_3D_DLVDATA

namespace CRYSTAL {

  class dlv_3d_data : public property_data_v6 {
  protected:
    dlv_3d_data(const bool do_cd, const bool do_spin, const bool do_pot,
		const bool do_efield, const bool do_density, const int_g np,
		DLV::box *v, const int_g pol, const int_g pot,
		const int_g minb, const int_g maxb, const Density_Matrix &dm,
		const NewK &nk);

  protected:
    virtual void write_input(const DLV::string filename,
			     const DLV::operation *op,
			     const DLV::model *const structure,
			     const bool is_bin, char message[],
			     const int_g mlen);
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const real_g spacing,
		     const bool set_points, const bool is_bin,
		     char message[], const int_g mlen);
    virtual void write_grid(std::ofstream &output) const;
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;
    virtual void write_tddft(std::ofstream &output) const;

  private:
    bool calc_charge;
    bool calc_spin;
    bool calc_potential;
    bool calc_efield;
    bool calc_density;
    int_g npoints;
    DLV::box *grid;
    int_g pol_tol;
    int_g pot_tol;
    int_g min_band;
    int_g max_band;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class dlv_3d_data_v6 : public dlv_3d_data, public grid3D_v6 {
  protected:
    dlv_3d_data_v6(const bool do_cd, const bool do_spin, const bool do_pot,
		   const bool do_efield, const bool do_density, const int_g np,
		   DLV::box *v, const int_g pol, const int_g pot,
		   const int_g minb, const int_g maxb, const Density_Matrix &dm,
		   const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dlv_3d_data_v8 : public dlv_3d_data, public grid3D_v6 {
  protected:
    dlv_3d_data_v8(const bool do_cd, const bool do_spin, const bool do_pot,
		   const bool do_efield, const bool do_density, const int_g np,
		   DLV::box *v, const int_g pol, const int_g pot,
		   const int_g minb, const int_g maxb, const real_g sp,
		   const bool use_np, const bool do_ldr, const bool do_part,
		   const bool do_hole, const bool do_ov, const int_g mine,
		   const int_g maxe, const Density_Matrix &dm, const NewK &nk);

    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool is_bin,
		     char message[], const int_g mlen);
    void write_tddft(std::ofstream &output) const;

    bool check_tddft() const;

  private:
    real_g grid_spacing;
    bool set_points;
    bool calc_response;
    bool calc_particle;
    bool calc_hole;
    bool calc_overlap;
    int_g min_excitation;
    int_g max_excitation;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dlv3d_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

    // public for serialization
    load_dlv3d_data(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dlv3d_data_v6 : public load_dlv3d_data, public grid3D_v6 {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    //public for serialization
    load_dlv3d_data_v6(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dlv_3d_calc : public property_calc {
  public:
    static operation *create(const bool do_cd, const bool do_spin,
			     const bool do_pot, const bool do_efield,
			     const bool do_density, const int_g np,
			     const int_g grid, const int_g pol, const int_g pot,
			     const int_g minb, const int_g maxb,
			     const real_g spacing, const bool use_np,
			     const bool do_ldr, const bool do_part,
			     const bool do_hole, const bool do_ov,
			     const int_g mine, const int_g maxe,
			     const Density_Matrix &dm, const NewK &nk,
			     const int_g version,const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;

    virtual bool is_tddft() const;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dlv_3d_calc_v6 : public dlv_3d_calc, public dlv_3d_data_v6 {
  public:
    dlv_3d_calc_v6(const bool do_cd, const bool do_spin, const bool do_pot,
		   const bool do_efield, const bool do_density, const int_g np,
		   DLV::box *v, const int_g pol, const int_g pot,
		   const int_g minb, const int_g maxb, const Density_Matrix &dm,
		   const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dlv_3d_calc_v8 : public dlv_3d_calc, public dlv_3d_data_v8 {
  public:
    dlv_3d_calc_v8(const bool do_cd, const bool do_spin, const bool do_pot,
		   const bool do_efield, const bool do_density, const int_g np,
		   DLV::box *v, const int_g pol, const int_g pot,
		   const int_g minb, const int_g maxb, const real_g sp,
		   const bool use_sp, const bool do_ldr, const bool do_part,
		   const bool do_hole, const bool do_ov, const int_g mine,
		   const int_g maxe, const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    bool is_tddft() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_dlv3d_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dlv_3d_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dlv_3d_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::dlv_3d_data::dlv_3d_data(const bool do_cd, const bool do_spin,
					 const bool do_pot,
					 const bool do_efield,
					 const bool do_density, const int_g np,
					 DLV::box *v, const int_g pol,
					 const int_g pot, const int_g minb,
					 const int_g maxb,
					 const Density_Matrix &dm,
					 const NewK &nk)
  : property_data_v6(dm, nk), calc_charge(do_cd), calc_spin(do_spin),
    calc_potential(do_pot), calc_efield(do_efield), calc_density(do_density),
    npoints(np), grid(v), pol_tol(pol), pot_tol(pot), min_band(minb),
    max_band(maxb)
{
}

inline CRYSTAL::dlv_3d_data_v6::dlv_3d_data_v6(const bool do_cd,
					       const bool do_spin,
					       const bool do_pot,
					       const bool do_efield,
					       const bool do_density,
					       const int_g np, DLV::box *v,
					       const int_g pol, const int_g pot,
					       const int_g minb,
					       const int_g maxb,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : dlv_3d_data(do_cd, do_spin, do_pot, do_efield, do_density, np, v,
		pol, pot, minb, maxb, dm, nk)
{
}

inline CRYSTAL::dlv_3d_data_v8::dlv_3d_data_v8(const bool do_cd,
					       const bool do_spin,
					       const bool do_pot,
					       const bool do_efield,
					       const bool do_density,
					       const int_g np, DLV::box *v,
					       const int_g pol, const int_g pot,
					       const int_g minb,
					       const int_g maxb,
					       const real_g sp,
					       const bool use_np,
					       const bool do_ldr,
					       const bool do_part,
					       const bool do_hole,
					       const bool do_ov,
					       const int_g mine,
					       const int_g maxe,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : dlv_3d_data(do_cd, do_spin, do_pot, do_efield, do_density, np, v,
		pol, pot, minb, maxb, dm, nk),
    grid_spacing(sp), set_points(use_np), calc_response(do_ldr),
    calc_particle(do_part), calc_hole(do_hole), calc_overlap(do_ov),
    min_excitation(mine), max_excitation(maxe)
{
}

inline bool CRYSTAL::dlv_3d_data_v8::check_tddft() const
{
  return (calc_response or calc_particle or calc_hole or calc_overlap);
}

inline CRYSTAL::load_dlv3d_data::load_dlv3d_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_dlv3d_data_v6::load_dlv3d_data_v6(const char file[])
  : load_dlv3d_data(file)
{
}

inline bool CRYSTAL::dlv_3d_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::dlv_3d_calc_v6::dlv_3d_calc_v6(const bool do_cd,
					       const bool do_spin,
					       const bool do_pot,
					       const bool do_efield,
					       const bool do_density,
					       const int_g np, DLV::box *v,
					       const int_g pol, const int_g pot,
					       const int_g minb,
					       const int_g maxb,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : dlv_3d_data_v6(do_cd, do_spin, do_pot, do_efield, do_density, np, v,
		   pol, pot, minb, maxb, dm, nk)
{
}

inline CRYSTAL::dlv_3d_calc_v8::dlv_3d_calc_v8(const bool do_cd,
					       const bool do_spin,
					       const bool do_pot,
					       const bool do_efield,
					       const bool do_density,
					       const int_g np, DLV::box *v,
					       const int_g pol, const int_g pot,
					       const int_g minb,
					       const int_g maxb,
					       const real_g sp,
					       const bool use_np,
					       const bool do_ldr,
					       const bool do_part,
					       const bool do_hole,
					       const bool do_ov,
					       const int_g mine,
					       const int_g maxe,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : dlv_3d_data_v8(do_cd, do_spin, do_pot, do_efield, do_density, np, v,
		   pol, pot, minb, maxb, sp, use_np, do_ldr, do_part, do_hole,
		   do_ov, mine, maxe, dm, nk)
{
}

#endif // CRYSTAL_3D_DLVDATA
