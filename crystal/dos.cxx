
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/atom_prefs.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "plots.hxx"
#include "dos.hxx"
#include "wavefn.hxx"
#include "../graphics/crystal.hxx"

CRYSTAL::dos_data::~dos_data()
{
}

DLV::operation *CRYSTAL::dos_calc::create(const int_g version,
					  char message[], const int_g mlen)
{
  dos_calc *op = 0;
  message[0] = '\0';
  Density_Matrix dm(0, 0, 0, 0, 0);
  NewK nk(0, 0, 0, 0, false, false, false, false);
  switch (version) {
  case CRYSTAL98:
    op = new dos_calc_v4(dm, nk);
    break;
  case CRYSTAL03:
    op = new dos_calc_v5(dm, nk);
    break;
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
    op = new dos_calc_v6(dm, nk);
    break;
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    op = new dos_calc_v7(dm, nk);
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::dos_calc operation",
	      mlen);
  } else {
    message[0] = '\0';
    op->attach_pending();
  }
  return op;
}

bool CRYSTAL::dos_calc::calculate(const int_g np, const int_g npoly,
				  const int_g bmin, const int_g bmax,
				  const real_g emin, const real_g emax,
				  const bool use_e, const Density_Matrix &dm,
				  const NewK &nk, const int_g version,
				  const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[],
				  char message[], const int_g mlen)
{
  // I could do this via a virtual in DLV::operation, but I don't want to
  // have to alter CCP3core to add CCP3calc options.
  DLV::operation *base = get_editing();
  dos_calc *op = dynamic_cast<dos_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  bool parallel = false;
  if (version == CRYSTAL_DEV)
    parallel = job.is_parallel();
  op->set_params(np, npoly, bmin, bmax, emin, emax, use_e, dm, nk);
  (void) accept_pending();
  if (op->create_files(parallel, job.is_local(), message, mlen))
    if (op->make_directory(message, mlen))
      op->write(op->get_infile_name(0), op, op->get_model(), message, mlen);
  if (strlen(message) == 0) {
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(), "", job, parallel,
		extern_job, extern_dir, message, mlen);
    return true;
  } else
    return false;
}

DLV::string CRYSTAL::dos_calc::get_name() const
{
  return ("CRYSTAL Density of States calculation");
}

void CRYSTAL::dos_calc_v4::set_params(const int_g np, const int_g npoly,
				      const int_g bmin, const int_g bmax,
				      const real_g emin, const real_g emax,
				      const bool use_e,
				      const Density_Matrix &dm, const NewK &nk)
{
  dos_data_v4::set_params(np, npoly, bmin, bmax, emin, emax, use_e, dm, nk);
}

void CRYSTAL::dos_calc_v5::set_params(const int_g np, const int_g npoly,
				      const int_g bmin, const int_g bmax,
				      const real_g emin, const real_g emax,
				      const bool use_e,
				      const Density_Matrix &dm, const NewK &nk)
{
  dos_data_v5::set_params(np, npoly, bmin, bmax, emin, emax, use_e, dm, nk);
}

void CRYSTAL::dos_calc_v6::set_params(const int_g np, const int_g npoly,
				      const int_g bmin, const int_g bmax,
				      const real_g emin, const real_g emax,
				      const bool use_e,
				      const Density_Matrix &dm, const NewK &nk)
{
  dos_data_v6::set_params(np, npoly, bmin, bmax, emin, emax, use_e, dm, nk);
}

void CRYSTAL::dos_calc_v7::set_params(const int_g np, const int_g npoly,
				      const int_g bmin, const int_g bmax,
				      const real_g emin, const real_g emax,
				      const bool use_e,
				      const Density_Matrix &dm, const NewK &nk)
{
  dos_data_v6::set_params(np, npoly, bmin, bmax, emin, emax, use_e, dm, nk);
}

DLV::operation *CRYSTAL::load_dos_data::create(const char filename[],
					       const int_g version,
					       char message[], const int_g mlen)
{
  switch (version) {
  case CRYSTAL98:
    return load_dos_v4::create(filename, message, mlen);
  case CRYSTAL03: 
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
    return load_dos_v5::create(filename, message, mlen);
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_dos_v7::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_dos_data::get_name() const
{
  return ("Load CRYSTAL Density of States - " + get_filename());
}

DLV::operation *CRYSTAL::load_dos_v4::create(const char filename[],
					     char message[],
					     const int_g mlen)
{
  load_dos_v4 *op = new load_dos_v4(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::operation *CRYSTAL::load_dos_v5::create(const char filename[],
					     char message[],
					     const int_g mlen)
{
  load_dos_v5 *op = new load_dos_v5(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 0, false, 0.0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::operation *CRYSTAL::load_dos_v7::create(const char filename[],
					     char message[],
					     const int_g mlen)
{
  load_dos_v7 *op = new load_dos_v7(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 0, false, 0.0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::dos_plot *CRYSTAL::dos_file_v4::read_data(DLV::operation *op,
					       DLV::electron_dos *plot,
					       const char filename[],
					       const DLV::string id,
					       const DLV::string *labels,
					       char message[],
					       const int_g mlen)
{
  DLV::electron_dos *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[160];
      // first line
      input >> line;
      int_g i;
      input >> i;
      int_g npts;
      input >> npts;
      real_g temp;
      input >> temp;
      real_g step;
      input >> step;
      input.getline(line, 128);
      // Line 2, origin
      input >> temp;
      real_g origin;
      input >> origin;
      step *= real_g(DLV::Hartree_to_eV);
      origin *= real_g(DLV::Hartree_to_eV);
      // Line 3
      int_g bidx;
      input >> bidx;
      input.getline(line, 128);
      int_g n = 1;
      int_g flag = 0;
      bool spin = false;
      bool ok = true;
      // Todo - problem initialising to correct size
      strncpy(message, "Todo - set up v4 DOS correctly", mlen);
      if (plot == 0)
	data = new DLV::electron_dos("CRYSTAL", id, op, 1, false);
      else
	data = plot;
      while ((ok = read_plot(input, data, flag, npts, message, mlen))) {
	// See if there is another plot following.
	input.getline(line, 128);
	input.getline(line, 128);
	if (!input.eof()) {
	  n++;
	  // Check header - Todo, deal with different grids?
	  input.getline(line, 128);
	  int_g index;
	  input >> index;
	  // Should be index == 1, but next integer can run into it
	  if (!spin)
	    spin = (index == bidx);
	  if (spin)
	    flag = 1;
	  input.getline(line, 128);
	} else
	  break;
      }
      if (ok) {
	// Now set up the headers.
	if (spin) {
	  n = n / 2;
	  data->set_spin();
	}
	for (i = 0; i < (n - 1); i++) {
	  if (labels == 0) {
	    if (spin)
	      snprintf(line, 160, "Alpha Projection %1d", i + 1);
	    else
	      snprintf(line, 160, "Projection %1d", i + 1);
	  } else {
	    if (spin)
	      snprintf(line, 160, "%.128s (alpha)", labels[i].c_str());
	    else
	      snprintf(line, 160, "%.128s", labels[i].c_str());
	  }
	  data->set_plot_label(line, i);
	}
	if (spin)
	  strcpy(line, "Total Alpha DOS");
	else
	  strcpy(line, "Total DOS");
	data->set_plot_label(line, n - 1);
	if (spin) {
	  for (i = 0; i < (n - 1); i++) {
	    if (labels == 0)
	      snprintf(line, 160, "Beta Projection %1d", i + 1);
	    else
	      snprintf(line, 160, "%.128s (beta)", labels[i].c_str());
	    data->set_plot_label(line, n + i);
	  }
	  strcpy(line, "Total Beta DOS");
	  data->set_plot_label(line, (2 * n - 1));
	}
      }
      data->set_xaxis_label("Energy (eV)");
      data->set_xunits("eV");
      input.close();
    }
  }
  return data;
}

DLV::dos_plot *CRYSTAL::dos_file_v5::read_data(DLV::operation *op,
					       DLV::electron_dos *plot,
					       const char filename[],
					       const DLV::string id,
					       const DLV::string *labels,
					       const bool use_fermi,
					       const real_g ef,
					       char message[],
					       const int_g mlen)
{
  DLV::dos_plot *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[160];
      input.getline(line, 128);
      if (line[0] != '#')
	strncpy(message, "Invalid header to DOS file", mlen);
      else {
	// # NEPT nk NPROJ nbands NSPIN nspin
	int_g npts;
	int_g nproj;
	int_g nspin;
	sscanf(line + 1, "%*s %d %*s %d %*s %d", &npts, &nproj, &nspin);
	if (plot == 0)
	  data = new DLV::electron_dos("CRYSTAL", id, op, nproj * nspin,
				       (nspin == 2));
	else
	  data = plot;
	real_g **plots = new_local_array1(real_g *, nproj);
	for (int_g i = 0; i < nproj; i++)
	  plots[i] = new real_g[npts];
	real_g *grid = new real_g[npts];
	for (int_g l = 0; l < nspin; l++) {
	  // Skip xmgr commands
	  do {
	    input >> line;
	    if (line[0] == '@' or line[0] == '#' or line[0] == '&')
	      input.getline(line, 128);
	    else
	      break;
	  } while (1);
	  // Need to process first line differently
	  grid[0] = (real_g)atof(line);
	  grid[0] *= real_g(DLV::Hartree_to_eV);
	  for (int_g k = 0; k < nproj; k++) {
	    input >> plots[k][0];
	  }
	  for (int_g i = 1; i < npts; i++) {
	    input >> grid[i];
	    grid[i] *= real_g(DLV::Hartree_to_eV);
	    if (use_fermi)
	      grid[i] += ef;
	    for (int_g k = 0; k < nproj; k++) {
	      input >> plots[k][i];
	    }
	  }
	  // Tidy end of line
	  input.getline(line, 128);
	  bool copy_data = (l == 0 and nspin == 2);
	  if (l == 0) {
	    // beta grid should be the same so this is safe
	    data->set_grid(grid, npts, false);
	  }
	  // Now set up the headers.
	  for (int_g i = 0; i < nproj; i++) {
	    int_g k = i;
	    if (l == 1)
	      k += nproj;
	    char text[160];
	    if (i == (nproj - 1)) {
	      if (nspin > 1) {
		if (l == 0)
		  strcpy(text, "Total Alpha DOS");
		else
		  strcpy(text, "Total Beta DOS");
	      } else
		strcpy(text, "Total DOS");
	    } else {
	      if (labels == 0) {
		if (nspin > 1) {
		  if (l == 0) 
		    snprintf(text, 160, "Alpha Projection %d", i + 1);
		  else
		    snprintf(text, 160, "Beta Projection %d", i + 1);
		} else
		  snprintf(text, 160, "Projection %d", i + 1);
	      } else {
		if (nspin > 1) {
		  if (l == 0) 
		    snprintf(text, 160, "%.127s (alpha)", labels[i].c_str());
		  else
		    snprintf(text, 160, "%.127s (beta)", labels[i].c_str());
		} else
		  snprintf(text, 160, "%.127s", labels[i].c_str());
	      }
	    }
	    int_g j = i;
	    if (l > 0)
	      j += nproj;
	    data->add_plot(plots[i], j, text, copy_data);
	  }
	}
	delete_local_array(plots);
	data->set_xaxis_label("Energy (eV)");
	data->set_xunits("eV");
      }
      input.close();
    }
  }
  return data;
}

void CRYSTAL::dos_calc_v4::add_dos_file(const char tag[], const bool is_local)
{
  add_output_file(0, tag, "dat", "fort.25", is_local, true);
}

void CRYSTAL::dos_calc_v5::add_dos_file(const char tag[], const bool is_local)
{
  add_output_file(0, tag, "dat", "fort.24", is_local, true);
}

void CRYSTAL::dos_calc_v6::add_dos_file(const char tag[], const bool is_local)
{
  add_output_file(0, tag, "dat", "DOSS.DAT", is_local, true);
}

void CRYSTAL::dos_calc_v7::add_dos_file(const char tag[], const bool is_local)
{
  add_output_file(0, tag, "dat", "DOSS.DAT", is_local, true);
}

void CRYSTAL::dos_calc_v6::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::dos_calc_v7::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::dos_calc::create_files(const bool is_parallel,
				     const bool is_local, char message[],
				     const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "dos";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_dos_file(tag, is_local);
    return true;
  } else
    return false;
}

void CRYSTAL::dos_data_v4::write_file_flag(std::ofstream &output) const
{
  output << " 1";
}

void CRYSTAL::dos_data_v5::write_file_flag(std::ofstream &output) const
{
  output << " 2";
}

void CRYSTAL::dos_data_v6::write_file_flag(std::ofstream &output) const
{
  output << " 2";
}

void CRYSTAL::dos_data::write_dos(std::ofstream &output,
				  const DLV::operation *op,
				  const DLV::model *const structure)
{
  output << "DOSS\n";
  output << nprojections;
  output << ' ' << npoints;
  if (use_energy) {
    output << " -1 -1";
  } else {
    output << ' ' << band_min;
    output << ' ' << band_max;
  }
  write_file_flag(output);
  output << ' ' << npolynomials;
  output << " 0\n";
  if (use_energy) {
    output << ' ' << energy_min / DLV::Hartree_to_eV;
    output << ' ' << energy_max / DLV::Hartree_to_eV << '\n';
  }
  for (int_g i = 0; i < nprojections; i++) {
    if (proj_type == 1)
      output << - ((int)projections[i].size());
    else
      output << (int)projections[i].size();
    std::list<int>::const_iterator ptr;
    for (ptr = projections[i].begin(); ptr != projections[i].end(); ++ptr )
      output << ' ' << *ptr;
    output << '\n';
  }
}

void CRYSTAL::dos_data_v4::write_input(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       const bool get_fermi, const bool is_bin,
				       char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, get_fermi);
    write_dos(output, op, structure);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::dos_data_v5::write_input(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       const bool get_fermi, const bool is_bin,
				       char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, get_fermi);
    write_dos(output, op, structure);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::dos_data_v6::write_input(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       const bool get_fermi, const bool is_bin,
				       char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, get_fermi);
    write_dos(output, op, structure);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::dos_calc_v4::write(const DLV::string filename,
				 const DLV::operation *op,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  write_input(filename, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

void CRYSTAL::dos_calc_v5::write(const DLV::string filename,
				 const DLV::operation *op,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  write_input(filename, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

void CRYSTAL::dos_calc_v6::write(const DLV::string filename,
				 const DLV::operation *op,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  write_input(filename, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

void CRYSTAL::dos_calc_v7::write(const DLV::string filename,
				 const DLV::operation *op,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  write_input(filename, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

bool CRYSTAL::dos_calc_v4::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::dos_plot *data = read_data(this, 0, get_outfile_name(0).c_str(),
				    id, get_labels(), message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      if (find_fermi_energy(ef, message, len)) {
	real_g value = real_g(ef);
	data->set_xpoints(&value, 1);
	DLV::string fermi;
	bool insulator = false;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::dos_calc_v5::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::dos_plot *data = read_data(this, 0, get_outfile_name(0).c_str(),
				    id, get_labels(), false, 0.0, message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      if (find_fermi_energy(ef, message, len)) {
	real_g value = real_g(ef);
	data->set_xpoints(&value, 1);
	DLV::string fermi;
	bool insulator = false;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::dos_calc_v6::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::dos_plot *data = read_data(this, 0, get_outfile_name(0).c_str(),
				    id, get_labels(), false, 0.0, message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      if (find_fermi_energy(ef, message, len)) {
	real_g value = real_g(ef);
	data->set_xpoints(&value, 1);
	DLV::string fermi;
	bool insulator = false;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::dos_calc_v7::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "DOS output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"DOS(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    // xxx
    real_l ef;
    bool use_fermi;
    if ((use_fermi = find_fermi_energy(ef, message, len)))
      shift = real_g(ef);
    real_g value = shift;
    DLV::dos_plot *data = read_data(this, 0, get_outfile_name(0).c_str(),
				    id, get_labels(), use_fermi, shift,
				    message, len);
    if (data == 0)
      return false;
    else {
      if (use_fermi) {
	//real_l ef;
	//if (find_fermi_energy(ef, message, len)) {
	//real_g value = real_g(ef);
	data->set_xpoints(&value, 1);
	DLV::string fermi;
	bool insulator = false;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_xlabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::load_dos_v4::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), 0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::load_dos_v5::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), 0, false, 0.0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::load_dos_v7::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), 0, false, 0.0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::dos_calc_v4::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", get_labels(), message, mlen) != 0);
  return false;
}

bool CRYSTAL::dos_calc_v5::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", get_labels(), false, 0.0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::dos_calc_v6::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", get_labels(), false, 0.0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::dos_calc_v7::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", get_labels(), true, shift, message, mlen) != 0);
  return false;
}

CRYSTAL::int_g CRYSTAL::dos_calc_v4::get_proj_type() const
{
  return dos_data::get_proj_type();
}

CRYSTAL::int_g CRYSTAL::dos_calc_v5::get_proj_type() const
{
  return dos_data::get_proj_type();
}

CRYSTAL::int_g CRYSTAL::dos_calc_v6::get_proj_type() const
{
  return dos_data::get_proj_type();
}

CRYSTAL::int_g CRYSTAL::dos_calc_v7::get_proj_type() const
{
  return dos_data::get_proj_type();
}

void CRYSTAL::dos_data::reset_projections(const int_g ptype, bool &toggle)
{
  toggle = false;
  if (ptype != proj_type) {
    if (proj_type == 1)
      toggle = true;
    proj_type = ptype;
    for (int_g i = 0; i < nprojections; i++) {
      labels[i] = "";
      projections[i].clear();
    }
    nprojections = 0;
    if (ptype == 1)
      toggle = true;
  }
}

void CRYSTAL::dos_calc_v4::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::dos_calc_v5::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::dos_calc_v6::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::dos_calc_v7::reset_projections(const int_g ptype)
{
  bool toggle_flags;
  dos_data::reset_projections(ptype, toggle_flags);
#ifdef ENABLE_DLV_GRAPHICS
  if (toggle_flags)
    toggle_atom_flags("DOS atom projections");
#endif // ENABLE_DLV_GRAPHICS
}

bool CRYSTAL::dos_calc::reset_projections(const int_g ptype,
					  char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  dos_calc *op = dynamic_cast<dos_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  op->reset_projections(ptype);
  if (ptype == 2)
    CRYSTAL::trigger_dos_atom_selections();
  return true;
}

bool CRYSTAL::dos_data::add_atom_projection(const DLV::model *const m,
					    int_g &nproj, DLV::string &pname,
					    const bool all_atoms,
					    const DLV::atom_integers *data,
					    char message[], const int_g mlen)
{
#ifdef ENABLE_DLV_GRAPHICS
  nproj = nprojections;
  int_g n = 0;
  int_g *prim_labels = 0;
  int_g *types = 0;
  DLV::atom *asym = 0;
  if (data->map_atom_selections(m, n, prim_labels, types, asym, all_atoms)) {
    delete [] asym;
    for (int_g i = 0; i < n; i++)
      projections[nprojections].push_back(prim_labels[i]);
    labels[nprojections] = "";
    char buff[32];
    bool first = true;
    if (all_atoms) {
      for (int_g i = 0; i < n; i++) {
	if (types[i] != -1) {
	  if (!first)
	    labels[nprojections] += ", ";
	  first = false;
	  snprintf(buff, 32, "%s (all)",
		   DLV::atomic_data->get_symbol(types[i]).c_str());
	  labels[nprojections] += buff;
	  for (int_g j = i + 1; j < n; j++)
	    if (types[j] == types[i])
	      types[j] = -1;
	}
      }
    } else {
      for (int_g i = 0; i < n; i++) {
	if (!first)
	  labels[nprojections] += ", ";
	first = false;
	snprintf(buff, 32, "%s (%d)",
		 DLV::atomic_data->get_symbol(types[i]).c_str(),
		 prim_labels[i]);
	labels[nprojections] += buff;
      }
    }
    delete [] types;
    delete [] prim_labels;
    pname = labels[nprojections];
    nprojections++;
    nproj = nprojections;
    return true;
  } else {
    strncpy(message, "No atoms selected for projection\n", mlen);
    return false;
  }
#else
  strncpy(message, "No graphics for atoms projection\n", mlen);
  return false;
#endif // ENABLE_DLV_GRAPHICS
}

bool CRYSTAL::dos_data::add_orbital_projection(wavefn_calc *wavefn,
					       const DLV::model *const m,
					       int_g &nproj, DLV::string &pname,
					       const int_g orbitals[],
					       const int_g norbitals,
					       const bool all_atoms,
					       const bool expand_atoms,
					       const bool expand_states,
					       const DLV::atom_integers *data)
{
#ifdef ENABLE_DLV_GRAPHICS
  // Todo - quite a lot is a copy of list_orbitals
  const int_g max_l = 4;
  int_g n = 0;
  int_g *prim_labels = 0;
  int_g *types = 0;
  DLV::atom *asym = 0;
  if (data->map_atom_selections(m, n, prim_labels, types, asym, all_atoms)) {
    bool first = true;
    if (expand_atoms) {
      //get primitive indices of selected atoms/symmetry info;
      int_g *copies = new_local_array1(int_g, n);
      for (int_g i = 0; i < n; i++)
	copies[i] = 1;
      for (int_g i = 0; i < n; i++) {
	if (copies[i] > 0) {
	  for (int_g j = i + 1; j < n; j++) {
	    if (asym[j] == asym[i]) {
	      copies[j] = 0;
	      copies[i]++;
	    }
	  }
	}
      }
      if (expand_states) {
	int_g largest_n = 0;
	// list possible states
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++) {
	  for (int_g i = 0; i < n; i++) {
	    if (copies[i] > 0) {
	      int_g max_n = wavefn->get_max_n(prim_labels[i] - 1, l);
	      count += (max_n - l) * (2 * l + 1);
	      if (max_n > largest_n)
		largest_n = max_n;
	    }
	  }
	}
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    for (int_g k = 0; k < n; k++) {
	      if (copies[k] > 0) {
		DLV::string tag = "";
		int_g max_n;
		// build tag
		char buff[32];
		snprintf(buff, 32, "(%s - %d",
			 DLV::atomic_data->get_symbol(types[k]).c_str(),
			 prim_labels[k]);
		tag += buff;
		for (int_g j = k + 1; j < n; j++) {
		  if (asym[k] == asym[j]) {
		    snprintf(buff, 32, ", %d", prim_labels[j]);
		    tag += buff;
		  }
		}
		tag += ")";
		max_n = wavefn->get_max_n(prim_labels[k] - 1, l);
		if (i < max_n) {
		  switch (l) {
		  case 0:
		    for (int_g p = 0; p < norbitals; p++) {
		      if (orbitals[p] == count) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			snprintf(buff, 32, "%1dS %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_s_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb);
			  }
			}
			break;
		      }
		    }
		    count++;
		    break;
		  case 1:
		    for (int_g p = 0; p < norbitals; p++) {
		      int_g offset = orbitals[p] - count;
		      if (offset >= 0 and offset < 3) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			if (offset == 0)
			  snprintf(buff, 32, "%1dPx %s", i + 1, tag.c_str());
			else if (offset == 1)
			  snprintf(buff, 32, "%1dPy %s", i + 1, tag.c_str());
			else
			  snprintf(buff, 32, "%1dPz %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb + offset);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_p_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb + offset);
			  }
			}
			break;
		      }
		    }
		    count += 3;
		    break;
		  case 2:
		    for (int_g p = 0; p < norbitals; p++) {
		      int_g offset = orbitals[p] - count;
		      if (offset >= 0 and offset < 5) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			if (offset == 0)
			  snprintf(buff, 32, "%1dD z2-r2 %s",
				   i + 1, tag.c_str());
			else if (offset == 1)
			  snprintf(buff, 32, "%1dD xz %s", i + 1, tag.c_str());
			else if (offset == 2)
			  snprintf(buff, 32, "%1dD yz %s", i + 1, tag.c_str());
			else if (offset == 3)
			  snprintf(buff, 32, "%1dD x2-y2 %s",
				   i + 1, tag.c_str());
			else
			  snprintf(buff, 32, "%1dD xy %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb = wavefn->get_d_orbital(prim_labels[k] - 1,
							  i);
			projections[nprojections].push_back(orb + offset);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb = wavefn->get_d_orbital(prim_labels[j] - 1, i);
			    projections[nprojections].push_back(orb + offset);
			  }
			}
			break;
		      }
		    }
		    count += 5;
		    break;
		  case 3:
		    for (int_g p = 0; p < norbitals; p++) {
		      int_g offset = orbitals[p] - count;
		      if (offset >= 0 and offset < 7) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			if (offset == 0)
			  snprintf(buff, 32, "%1dF (z2-x2-y2)z", i + 1);
			else if (offset == 1)
			  snprintf(buff, 32, "%1dF (z2-x2-y2)x", i + 1);
			else if (offset == 2)
			  snprintf(buff, 32, "%1dF (z2-x2-y2)y", i + 1);
			else if (offset == 3)
			  snprintf(buff, 32, "%1dF (x2-y2)z", i + 1);
			else if (offset == 4)
			  snprintf(buff, 32, "%1dF xyz", i + 1);
			else if (offset == 5)
			  snprintf(buff, 32, "%1dF (x2-y2)x", i + 1);
			else
			  snprintf(buff, 32, "%1dF (x2-y2)y", i + 1);
			labels[nprojections] += buff;
			int_g orb = wavefn->get_f_orbital(prim_labels[k] - 1,
							  i);
			projections[nprojections].push_back(orb + offset);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb = wavefn->get_f_orbital(prim_labels[j] - 1, i);
			    projections[nprojections].push_back(orb + offset);
			  }
			}
			break;
		      }
		    }
		    count += 7;
		    break;
		  default:
		    count++;
		    break;
		  }
		}
	      }
	    }
	  }
	}
      } else {
	int_g largest_n = 0;
	// list possible orbital types
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++) {
	  for (int_g i = 0; i < n; i++) {
	    if (copies[i] > 0) {
	      int_g max_n = wavefn->get_max_n(prim_labels[i] - 1, l);
	      count += (max_n - l);
	      if (max_n > largest_n)
		largest_n = max_n;
	    }
	  }
	}
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    for (int_g k = 0; k < n; k++) {
	      if (copies[k] > 0) {
		DLV::string tag = "";
		// build tag
		char buff[32];
		snprintf(buff, 32, "(%s - %d",
			 DLV::atomic_data->get_symbol(types[k]).c_str(),
			 prim_labels[k]);
		tag += buff;
		for (int_g j = k + 1; j < n; j++) {
		  if (asym[k] == asym[j]) {
		    snprintf(buff, 32, ", %d", prim_labels[j]);
		    tag += buff;
		  }
		}
		tag += ")";
		int_g max_n = wavefn->get_max_n(prim_labels[k] - 1, l);
		if (i < max_n) {
		  switch (l) {
		  case 0:
		    for (int_g p = 0; p < norbitals; p++) {
		      if (orbitals[p] == count) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			snprintf(buff, 32, "%1dS %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_s_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb);
			  }
			}
			break;
		      }
		    }
		    break;
		  case 1:
		    for (int_g p = 0; p < norbitals; p++) {
		      if (orbitals[p] == count) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			snprintf(buff, 32, "%1dP %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_s_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb);
			    projections[nprojections].push_back(orb + 1);
			    projections[nprojections].push_back(orb + 2);
			  }
			}
			break;
		      }
		    }
		    break;
		  case 2:
		    for (int_g p = 0; p < norbitals; p++) {
		      if (orbitals[p] == count) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			snprintf(buff, 32, "%1dD %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
			projections[nprojections].push_back(orb + 3);
			projections[nprojections].push_back(orb + 4);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_s_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb);
			    projections[nprojections].push_back(orb + 1);
			    projections[nprojections].push_back(orb + 2);
			    projections[nprojections].push_back(orb + 3);
			    projections[nprojections].push_back(orb + 4);
			  }
			}
			break;
		      }
		    }
		    break;
		  case 3:
		    for (int_g p = 0; p < norbitals; p++) {
		      if (orbitals[p] == count) {
			if (!first)
			  labels[nprojections] += ", ";
			first = false;
			snprintf(buff, 32, "%1dF %s", i + 1, tag.c_str());
			labels[nprojections] += buff;
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
			projections[nprojections].push_back(orb + 3);
			projections[nprojections].push_back(orb + 4);
			projections[nprojections].push_back(orb + 5);
			projections[nprojections].push_back(orb + 6);
			for (int_g j = k + 1; j < n; j++) {
			  if (asym[k] == asym[j]) {
			    orb =
			      wavefn->get_s_or_sp_orbital(prim_labels[j] - 1,
							  i);
			    projections[nprojections].push_back(orb);
			    projections[nprojections].push_back(orb + 1);
			    projections[nprojections].push_back(orb + 2);
			    projections[nprojections].push_back(orb + 3);
			    projections[nprojections].push_back(orb + 4);
			    projections[nprojections].push_back(orb + 5);
			    projections[nprojections].push_back(orb + 6);
			  }
			}
			break;
		      }
		    }
		    break;
		  default:
		    break;
		  }
		  count++;
		}
	      }
	    }
	  }
	}
      }
      delete_local_array(copies);
    } else { // !expand_atoms
      bool *check = new_local_array1(bool, n);
      for (int_g i = 0; i < n; i++)
	check[i] = true;
      for (int_g i = 0; i < n; i++) {
	if (check[i]) {
	  for (int_g j = i + 1; j < n; j++)
	    if (types[j] == types[i])
	      check[j] = false;
	}
      }
      int_g max_n[max_l];
      // list possible states
      int_g largest_n = 0;
      for (int_g l = 0; l < max_l; l++) {
	max_n[l] = 0;
	for (int_g i = 0; i < n; i++) {
	  if (check[i]) {
	    int_g val = wavefn->get_max_n(prim_labels[i] - 1, l);
	    if (val > max_n[l])
	      max_n[l] = val;
	  }
	}
	if (max_n[l] > largest_n)
	  largest_n = max_n[l];
      }
      if (expand_states) {
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++)
	  count += (max_n[l] - l) * (2 * l + 1);
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    if (i < max_n[l]) {
	      char buff[32];
	      switch (l) {
	      case 0:
		for (int_g p = 0; p < norbitals; p++) {
		  if (orbitals[p] == count) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    snprintf(buff, 32, "%1dS", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
		      }
		    }
		  }
		}
		count++;
		break;
	      case 1:
		for (int_g p = 0; p < norbitals; p++) {
		  int_g offset = orbitals[p] - count;
		  if (offset >= 0 and offset < 3) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    if (offset == 0)	
		      snprintf(buff, 32, "%1dPx", i + 1);
		    else if (offset == 1)
		      snprintf(buff, 32, "%1dPy", i + 1);
		    else
		      snprintf(buff, 32, "%1dPz", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb + offset);
		      }
		    }
		  }
		}
		count += 3;
		break;
	      case 2:
		for (int_g p = 0; p < norbitals; p++) {
		  int_g offset = orbitals[p] - count;
		  if (offset >= 0 and offset < 5) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    if (offset == 0)	
		      snprintf(buff, 32, "%1dD z2-r2", i + 1);
		    else if (offset == 1)
		      snprintf(buff, 32, "%1dD xz", i + 1);
		    else if (offset == 2)
		      snprintf(buff, 32, "%1dD yz", i + 1);
		    else if (offset == 3)
		      snprintf(buff, 32, "%1dD x2-y2", i + 1);
		    else
		      snprintf(buff, 32, "%1dD xy", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb + offset);
		      }
		    }
		  }
		}
		count += 5;
		break;
	      case 3:
		for (int_g p = 0; p < norbitals; p++) {
		  int_g offset = orbitals[p] - count;
		  if (offset >= 0 and offset < 7) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    if (offset == 0)	
		      snprintf(buff, 32, "%1dF (z2-x2-y2)z", i + 1);
		    else if (offset == 1)
		      snprintf(buff, 32, "%1dF (z2-x2-y2)x", i + 1);
		    else if (offset == 2)
		      snprintf(buff, 32, "%1dF (z2-x2-y2)y", i + 1);
		    else if (offset == 3)
		      snprintf(buff, 32, "%1dF (x2-y2)z", i + 1);
		    else if (offset == 4)
		      snprintf(buff, 32, "%1dF xyz", i + 1);
		    else if (offset == 5)
		      snprintf(buff, 32, "%1dF (x2-y2)x", i + 1);
		    else
		      snprintf(buff, 32, "%1dF (x2-y2)y", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb + offset);
		      }
		    }
		  }
		}
		count += 7;
		break;
	      default:
		count++;
		break;
	      }
	    }
	  }
	}
      } else {
	// list possible orbital types
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++)
	  count += (max_n[l] - l);
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    if (i < max_n[l]) {
	      char buff[32];
	      switch (l) {
	      case 0:
		for (int_g p = 0; p < norbitals; p++) {
		  if (orbitals[p] == count) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    snprintf(buff, 32, "%1dS", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_s_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
		      }
		    }
		  }
		}
		break;
	      case 1:
		for (int_g p = 0; p < norbitals; p++) {
		  if (orbitals[p] == count) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    snprintf(buff, 32, "%1dP", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
		      }
		    }
		  }
		}
		break;
	      case 2:
		for (int_g p = 0; p < norbitals; p++) {
		  if (orbitals[p] == count) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    snprintf(buff, 32, "%1dD", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
			projections[nprojections].push_back(orb + 3);
			projections[nprojections].push_back(orb + 4);
		      }
		    }
		  }
		}
		break;
	      case 3:
		for (int_g p = 0; p < norbitals; p++) {
		  if (orbitals[p] == count) {
		    if (!first)
		      labels[nprojections] += ", ";
		    first = false;
		    snprintf(buff, 32, "%1dF", i + 1);
		    labels[nprojections] += buff;
		    for (int_g k = 0; k < n; k++) {
		      int_g val = wavefn->get_max_n(prim_labels[k] - 1, l);
		      if (i < val) {
			int_g orb =
			  wavefn->get_p_or_sp_orbital(prim_labels[k] - 1, i);
			projections[nprojections].push_back(orb);
			projections[nprojections].push_back(orb + 1);
			projections[nprojections].push_back(orb + 2);
			projections[nprojections].push_back(orb + 3);
			projections[nprojections].push_back(orb + 4);
			projections[nprojections].push_back(orb + 5);
			projections[nprojections].push_back(orb + 6);
		      }
		    }
		  }
		}
		break;
	      default:
		break;
	      }
	      count++;
	    }
	  }
	}
      }
      labels[nprojections] += " (";
      first = true;
      for (int_g i = 0; i < n; i++) {
	if (check[i]) {
	  char buff[32];
	  if (!first)
	    labels[nprojections] += ", ";
	  first = false;
	  if (all_atoms) {
	    snprintf(buff, 32, "%s all",
		     DLV::atomic_data->get_symbol(types[i]).c_str());
	    labels[nprojections] += buff;
	  } else {
	    snprintf(buff, 32, "%s - %d",
		     DLV::atomic_data->get_symbol(types[i]).c_str(),
		     prim_labels[i]);
	    labels[nprojections] += buff;
	    for (int_g j = i + 1; j < n; j++) {
	      if (types[i] == types[j]) {
		snprintf(buff, 32, ", %d", prim_labels[j]);
		labels[nprojections] += buff;
	      }
	    }
	  }
	}
      }
      labels[nprojections] += ")";
      delete_local_array(check);
    }
    delete [] asym;
    delete [] types;
    delete [] prim_labels;
    pname = labels[nprojections];
    nprojections++;
    nproj = nprojections;
    return true;
  } else
    return false;
#else
  return false;
#endif // ENABLE_DLV_GRAPHICS
}

bool CRYSTAL::dos_data::add_projection(wavefn_calc *wavefn,
				       const DLV::model *const m,
				       int_g &nproj, DLV::string &pname,
				       const int_g orbitals[],
				       const int_g norbitals,
				       const bool all_atoms,
				       const bool orb_atoms,
				       const bool orb_states,
				       const DLV::atom_integers *data,
				       int_g &ptype, char message[],
				       const int_g mlen)
{
  ptype = proj_type;
  if (proj_type == 1) {
    // atom projected
    return add_atom_projection(m, nproj, pname, all_atoms, data,
			       message, mlen);
  } else if (proj_type == 2) {
    // orbital projected
    return add_orbital_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data);
  }
  return false;
}

bool CRYSTAL::dos_calc_v4::add_projection(wavefn_calc *wavefn,
					  const DLV::model *const m,
					  int_g &nproj, DLV::string &pname,
					  const int_g orbitals[],
					  const int_g norbitals,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  const DLV::atom_integers *data,
					  int_g &ptype, char message[],
					  const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::dos_calc_v5::add_projection(wavefn_calc *wavefn,
					  const DLV::model *const m,
					  int_g &nproj, DLV::string &pname,
					  const int_g orbitals[],
					  const int_g norbitals,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  const DLV::atom_integers *data,
					  int_g &ptype, char message[],
					  const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::dos_calc_v6::add_projection(wavefn_calc *wavefn,
					  const DLV::model *const m,
					  int_g &nproj, DLV::string &pname,
					  const int_g orbitals[],
					  const int_g norbitals,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  const DLV::atom_integers *data,
					  int_g &ptype, char message[],
					  const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::dos_calc_v7::add_projection(wavefn_calc *wavefn,
					  const DLV::model *const m,
					  int_g &nproj, DLV::string &pname,
					  const int_g orbitals[],
					  const int_g norbitals,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  const DLV::atom_integers *data,
					  int_g &ptype, char message[],
					  const int_g mlen)
{
  return dos_data::add_projection(wavefn, m, nproj, pname, orbitals, norbitals,
				  all_atoms, orb_atoms, orb_states, data,
				  ptype, message, mlen);
}

bool CRYSTAL::dos_calc::add_projection(int_g &nproj, DLV::string &pname,
				       const int_g orbitals[],
				       const int_g norbitals,
				       const bool all_atoms,
				       const bool orb_atoms,
				       const bool orb_states,
				       char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  dos_calc *op = dynamic_cast<dos_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not DOS calculation", mlen);
    return false;
  }
  message[0] = '\0';
  // Find primitive atom labels object
  const DLV::data_object *data = op->find_data(prim_atom_label, program_name);
  if (data == 0) {
    strncpy(message, "Unable to locate primitive atom indices", mlen);
    return false;
  }
  const DLV::atom_integers *s = dynamic_cast<const DLV::atom_integers *>(data);
  if (s == 0) {
    strncpy(message, "Error locating primitive atom indices", mlen);
    return false;
  }
  // The op isn't yet attached, pending is where it would be
  DLV::operation *calc = find_pending_parent(prim_atom_label, program_name);
  if (calc == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
  if (wfn == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  int_g ptype = 0;
  bool ok = op->add_projection(wfn, op->get_model(), nproj, pname, orbitals,
			       norbitals, all_atoms, orb_atoms, orb_states,
			       s, ptype, message, mlen);
  if (ok) {
#ifdef DLV_ENABLE_GRAPHICS
    if (ptype == 1)
      op->set_selected_atom_flags(true);
    if (ptype > 0)
      (void) op->deselect_atoms(message, mlen);
#endif // DLV_ENABLE_GRAPHICS
  } else {
    if (strlen(message) == 0)
      strncpy(message, "Error adding projection", mlen);
  }
  return ok;
}

bool CRYSTAL::dos_calc::list_orbitals(wavefn_calc *wavefn,
				      const DLV::model *const m,
				      const DLV::atom_integers *data,
				      int_g &nproj, DLV::string * &pname,
				      const bool all_atoms,
				      const bool expand_atoms,
				      const bool expand_states)
{
#ifdef DLV_ENABLE_GRAPHICS
  nproj = 0;
  const int_g max_l = 4;
  int_g n = 0;
  int_g *prim_labels = 0;
  int_g *types = 0;
  DLV::atom *asym = 0;
  if (data->map_atom_selections(m, n, prim_labels, types, asym, all_atoms)) {
    if (expand_atoms) {
      //get primitive indices of selected atoms/symmetry info;
      int_g *copies = new_local_array1(int_g, n);
      for (int_g i = 0; i < n; i++)
	copies[i] = 1;
      for (int_g i = 0; i < n; i++) {
	if (copies[i] > 0) {
	  for (int_g j = i + 1; j < n; j++) {
	    if (asym[j] == asym[i]) {
	      copies[j] = 0;
	      copies[i]++;
	    }
	  }
	}
      }
      if (expand_states) {
	int_g largest_n = 0;
	// list possible states
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++) {
	  for (int_g i = 0; i < n; i++) {
	    if (copies[i] > 0) {
	      int_g max_n = wavefn->get_max_n(prim_labels[i] - 1, l);
	      count += (max_n - l) * (2 * l + 1);
	      if (max_n > largest_n)
		largest_n = max_n;
	    }
	  }
	}
	nproj = count;
	pname = new DLV::string[count];
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    for (int_g k = 0; k < n; k++) {
	      if (copies[k] > 0) {
		DLV::string tag = "";
		int_g max_n;
		// build tag
		char buff[32];
		snprintf(buff, 32, "(%s - %d",
			 DLV::atomic_data->get_symbol(types[k]).c_str(),
			 prim_labels[k]);
		tag += buff;
		for (int_g j = k + 1; j < n; j++) {
		  if (asym[k] == asym[j]) {
		    snprintf(buff, 32, ", %d", prim_labels[j]);
		    tag += buff;
		  }
		}
		tag += ")";
		max_n = wavefn->get_max_n(prim_labels[k] - 1, l);
		if (i < max_n) {
		  switch (l) {
		  case 0:
		    snprintf(buff, 32, "%1dS %s", i + 1, tag.c_str());
		    pname[count] = buff;
		    count++;
		    break;
		  case 1:
		    snprintf(buff, 32, "%1dPx %s", i + 1, tag.c_str());
		    pname[count] = buff;
		    snprintf(buff, 32, "%1dPy %s", i + 1, tag.c_str());
		    pname[count + 1] = buff;
		    snprintf(buff, 32, "%1dPz %s", i + 1, tag.c_str());
		    pname[count + 2] = buff;
		    count += 3;
		    break;
		  case 2:
		    snprintf(buff, 32, "%1dD z2-r2 %s", i + 1, tag.c_str());
		    pname[count] = buff;
		    snprintf(buff, 32, "%1dD xz %s", i + 1, tag.c_str());
		    pname[count + 1] = buff;
		    snprintf(buff, 32, "%1dD yz %s", i + 1, tag.c_str());
		    pname[count + 2] = buff;
		    snprintf(buff, 32, "%1dD x2-y2 %s", i + 1, tag.c_str());
		    pname[count + 3] = buff;
		    snprintf(buff, 32, "%1dD xy %s", i + 1, tag.c_str());
		    pname[count + 4] = buff;
		    count += 5;
		    break;
		  case 3:
		    snprintf(buff, 32, "%1dF (z2-x2-y2)z %s",
			     i + 1, tag.c_str());
		    pname[count] = buff;
		    snprintf(buff, 32, "%1dF (z2-x2-y2)x %s",
			     i + 1, tag.c_str());
		    pname[count + 1] = buff;
		    snprintf(buff, 32, "%1dF (z2-x2-y2)y %s",
			     i + 1, tag.c_str());
		    pname[count + 2] = buff;
		    snprintf(buff, 32, "%1dF (x2-y2)z %s", i + 1, tag.c_str());
		    pname[count + 3] = buff;
		    snprintf(buff, 32, "%1dF xyz %s", i + 1, tag.c_str());
		    pname[count + 4] = buff;
		    snprintf(buff, 32, "%1dF (x2-y2)x %s", i + 1, tag.c_str());
		    pname[count + 5] = buff;
		    snprintf(buff, 32, "%1dF (x2-y2)y %s", i + 1, tag.c_str());
		    pname[count + 6] = buff;
		    count += 7;
		    break;
		  default:
		    snprintf(buff, 32, "%1d? %s", i + 1, tag.c_str());
		    pname[count] = buff;
		    count++;
		    break;
		  }
		}
	      }
	    }
	  }
	}
      } else {
	int_g largest_n = 0;
	// list possible orbital types
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++) {
	  for (int_g i = 0; i < n; i++) {
	    if (copies[i] > 0) {
	      int_g max_n = wavefn->get_max_n(prim_labels[i] - 1, l);
	      count += (max_n - l);
	      if (max_n > largest_n)
		largest_n = max_n;
	    }
	  }
	}
	nproj = count;
	pname = new DLV::string[count];
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    for (int_g k = 0; k < n; k++) {
	      if (copies[k] > 0) {
		// build tag
		DLV::string tag = "";
		char buff[32];
		snprintf(buff, 32, "(%s - %d",
			 DLV::atomic_data->get_symbol(types[k]).c_str(),
			 prim_labels[k]);
		tag += buff;
		for (int_g j = k + 1; j < n; j++) {
		  if (asym[k] == asym[j]) {
		    snprintf(buff, 32, ", %d", prim_labels[j]);
		    tag += buff;
		  }
		}
		tag += ")";
		int_g max_n = wavefn->get_max_n(prim_labels[k] - 1, l);
		if (i < max_n) {
		  switch (l) {
		  case 0:
		    snprintf(buff, 32, "%1dS %s", i + 1, tag.c_str());
		    break;
		  case 1:
		    snprintf(buff, 32, "%1dP %s", i + 1, tag.c_str());
		    break;
		  case 2:
		    snprintf(buff, 32, "%1dD %s", i + 1, tag.c_str());
		    break;
		  case 3:
		    snprintf(buff, 32, "%1dF %s", i + 1, tag.c_str());
		    break;
		  default:
		    snprintf(buff, 32, "%1d? %s", i + 1, tag.c_str());
		    break;
		  }
		  pname[count] = buff;
		  count++;
		}
	      }
	    }
	  }
	}
      }
      delete_local_array(copies);
    } else { // !expand_atoms
      bool *check = new_local_array1(bool, n);
      for (int_g i = 0; i < n; i++)
	check[i] = true;
      for (int_g i = 0; i < n; i++) {
	if (check[i]) {
	  for (int_g j = i + 1; j < n; j++)
	    if (types[j] == types[i])
	      check[j] = false;
	}
      }
      int_g max_n[max_l];
      // list possible states
      int_g largest_n = 0;
      for (int_g l = 0; l < max_l; l++) {
	max_n[l] = 0;
	for (int_g i = 0; i < n; i++) {
	  if (check[i]) {
	    int_g val = wavefn->get_max_n(prim_labels[i] - 1, l);
	    if (val > max_n[l])
	      max_n[l] = val;
	  }
	}
	if (max_n[l] > largest_n)
	  largest_n = max_n[l];
      }
      if (expand_states) {
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++)
	  count += (max_n[l] - l) * (2 * l + 1);
	nproj = count;
	pname = new DLV::string[count];
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    if (i < max_n[l]) {
	      char buff[32];
	      switch (l) {
	      case 0:
		snprintf(buff, 32, "%1dS", i + 1);
		pname[count] = buff;
		count++;
		break;
	      case 1:
		snprintf(buff, 32, "%1dPx", i + 1);
		pname[count] = buff;
		snprintf(buff, 32, "%1dPy", i + 1);
		pname[count + 1] = buff;
		snprintf(buff, 32, "%1dPz", i + 1);
		pname[count + 2] = buff;
		count += 3;
		break;
	      case 2:
		snprintf(buff, 32, "%1dD z2-r2", i + 1);
		pname[count] = buff;
		snprintf(buff, 32, "%1dD xz", i + 1);
		pname[count + 1] = buff;
		snprintf(buff, 32, "%1dD yz", i + 1);
		pname[count + 2] = buff;
		snprintf(buff, 32, "%1dD x2-y2", i + 1);
		pname[count + 3] = buff;
		snprintf(buff, 32, "%1dD xy", i + 1);
		pname[count + 4] = buff;
		count += 5;
		break;
	      case 3:
		snprintf(buff, 32, "%1dF (z2-x2-y2)z", i + 1);
		pname[count] = buff;
		snprintf(buff, 32, "%1dF (z2-x2-y2)x", i + 1);
		pname[count + 1] = buff;
		snprintf(buff, 32, "%1dF (z2-x2-y2)y", i + 1);
		pname[count + 2] = buff;
		snprintf(buff, 32, "%1dF (x2-y2)z", i + 1);
		pname[count + 3] = buff;
		snprintf(buff, 32, "%1dF xyz", i + 1);
		pname[count + 4] = buff;
		snprintf(buff, 32, "%1dF (x2-y2)x", i + 1);
		pname[count + 5] = buff;
		snprintf(buff, 32, "%1dF (x2-y2)y", i + 1);
		pname[count + 6] = buff;
		count += 7;
		break;
	      default:
		snprintf(buff, 32, "%1d?", i + 1);
		pname[count] = buff;
		count++;
		break;
	      }
	    }
	  }
	}
      } else {
	// list possible orbital types
	int_g count = 0;
	for (int_g l = 0; l < max_l; l++)
	  count += (max_n[l] - l);
	nproj = count;
	pname = new DLV::string[count];
	count = 0;
	for (int_g i = 0; i < largest_n; i++) {
	  for (int_g l = 0; (l < max_l and l <= i); l++) {
	    if (i < max_n[l]) {
	      char buff[32];
	      switch (l) {
	      case 0:
		snprintf(buff, 32, "%1dS", i + 1);
		break;
	      case 1:
		snprintf(buff, 32, "%1dP", i + 1);
		break;
	      case 2:
		snprintf(buff, 32, "%1dD", i + 1);
		break;
	      case 3:
		snprintf(buff, 32, "%1dF", i + 1);
		break;
	      default:
		snprintf(buff, 32, "%1d?", i + 1);
		break;
	      }
	      pname[count] = buff;
	      count++;
	    }
	  }
	}
      }
      delete_local_array(check);
    }
    delete [] asym;
    delete [] types;
    delete [] prim_labels;
  }
  return true;
#else
  return false;
#endif // DLV_ENABLE_GRAPHICS
}

bool CRYSTAL::dos_calc::list_orbital_data(int_g &nproj, DLV::string * &pname,
					  const bool all_atoms,
					  const bool orb_atoms,
					  const bool orb_states,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  const DLV::data_object *data = find_data(prim_atom_label, program_name);
  if (data == 0) {
    strncpy(message, "Unable to locate primitive atom indices", mlen);
    return false;
  }
  const DLV::atom_integers *s = dynamic_cast<const DLV::atom_integers *>(data);
  if (s == 0) {
    strncpy(message, "Error locating primitive atom indices", mlen);
    return false;
  }
  // The op isn't yet attached, pending is where it would be
  DLV::operation *calc = find_pending_parent(prim_atom_label, program_name);
  if (calc == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
  if (wfn == 0) {
    strncpy(message, "Failed to find wavefn", mlen);
    return false;
  }
  return list_orbitals(wfn, get_model(), s, nproj, pname,
		       all_atoms, orb_atoms, orb_states);
}

bool CRYSTAL::dos_calc::list_orbitals(int_g &nproj, DLV::string * &pname,
				      const bool all_atoms,
				      const bool orb_atoms,
				      const bool orb_states,
				      char message[], const int_g mlen)
{
  DLV::operation *base = get_editing();
  dos_calc *op = dynamic_cast<dos_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not DOS calculation", mlen);
    return false;
  }
  return op->list_orbital_data(nproj, pname, all_atoms, orb_atoms, orb_states,
			       message, mlen);
}

void CRYSTAL::dos_calc::atom_selections_changed()
{
  if (get_proj_type() == 2)
    CRYSTAL::trigger_dos_atom_selections();
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_data_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_data_v5(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_data_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_data_v6(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_dos_v4 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_dos_v4("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_dos_v5 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_dos_v5("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_dos_v7 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_dos_v7("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_calc_v4 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_calc_v4(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_calc_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_calc_v5(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_calc_v6(d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::dos_calc_v7 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::dos_calc_v7(d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::dos_data::serialize(Archive &ar, const unsigned int version)
{
  ar & proj_type;
  ar & nprojections;
  ar & npoints;
  ar & npolynomials;
  ar & band_min;
  ar & band_max;
  ar & projections;
  ar & labels;
  if (version > 0) {
    ar & energy_min;
    ar & energy_max;
    ar & use_energy;
  }
}

template <class Archive>
void CRYSTAL::dos_data_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::dos_data_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::dos_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::load_dos_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_dos_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_dos_data>(*this);
}

template <class Archive>
void CRYSTAL::load_dos_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_dos_data>(*this);
}

template <class Archive>
void CRYSTAL::load_dos_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_dos_data>(*this);
}

template <class Archive>
void CRYSTAL::dos_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::dos_calc_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::dos_calc_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::dos_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::dos_calc_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::dos_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::dos_data_v6>(*this);
}

BOOST_CLASS_VERSION(CRYSTAL::dos_data, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_data_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_dos_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_dos_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_dos_v7)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_calc_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::dos_calc_v7)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::dos_data_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::dos_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_dos_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_dos_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_dos_v7)
DLV_NORMAL_EXPLICIT(CRYSTAL::dos_calc_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::dos_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::dos_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::dos_calc_v7)

#endif // DLV_USES_SERIALIZE
