
#ifndef CRYSTAL_EIGENVECTORS
#define CRYSTAL_EIGENVECTORS

namespace CRYSTAL {

  class eigenvec_file {
  protected:
    DLV::data_object *read_data(DLV::operation *op, const char filename[],
				char message[], const int_g mlen);
  };

  // v6 only
  class load_eigenvec_data : public DLV::load_data_op , public eigenvec_file {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    load_eigenvec_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

inline CRYSTAL::load_eigenvec_data::load_eigenvec_data(const char file[])
  : load_data_op(file)
{
}

#endif // CRYSTAL_EIGENVECTORS
