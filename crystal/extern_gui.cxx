
#include <list>
#include <map>
#include <iostream>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/op_admin.hxx"
#include "base.hxx"
#include "model.hxx"
#include "load.hxx"
#include "save.hxx"
#include "extern_gui.hxx"

DLV::operation *CRYSTAL::load_gui(const char name[], const char file_name[],
				  std::string message)
{
  char errormsg[256];
  DLV::operation *op = CRYSTAL::load_structure::create(name, file_name, false,
						       false, errormsg, 256);
  message = errormsg;
  return op;
}

DLV::operation *CRYSTAL::save_gui(const char file_name[], std::string message)
{
  char errormsg[256];
  DLV::operation *op = CRYSTAL::save_structure::create(file_name,
						       errormsg, 256);
  message = errormsg;
  return op;
}
