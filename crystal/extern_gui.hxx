
#ifndef CRYSTAL_GUI_EXPORTS
#define CRYSTAL_GUI_EXPORTS

namespace CRYSTAL {

  DLV::operation *load_gui(const char name[], const char file_name[],
			   std::string message);
  DLV::operation *save_gui(const char file_name[], std::string message);

}

#endif // CRYSTAL_GUI_EXPORTS
