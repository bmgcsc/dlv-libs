
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "extract.hxx"

DLV::operation *CRYSTAL::extract_calc::create(const char file[],
					      const char str[],
					      const bool use_str,
					      const bool new_view,
					      const bool set_bonds,
					      const DLV::job_setup_data &job,
					      char message[],
					      const int_g mlen)
{
  extract_calc *op = 0;
  message[0] = '\0';
  if (use_str)
    op = new extract_str(file, str, new_view, set_bonds);
  else
    op = new extract_calc(file, new_view, set_bonds);
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::run_input operation",
	    mlen);
  else {
    op->attach_base_no_current();
    if (op->create_files(false, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), "crystal", "", "", job, false,
		  false, "", message, mlen);
    }
  }
  return op;
}

DLV::string CRYSTAL::extract_calc::get_name() const
{
  return ("CRYSTAL Extract structure calculation");
}

bool CRYSTAL::extract_calc::has_structure() const
{
  return false;
}

bool CRYSTAL::extract_str::has_structure() const
{
  return true;
}

bool CRYSTAL::extract_calc::create_files(const bool is_parallel,
					 const bool is_local,
					 char message[], const int_g mlen)
{
  static char tag[] = "extr";
  static char input[] = "inp";
  add_calc_error_file(tag, is_parallel, is_local);
  if (is_parallel)
    add_input_file(0, tag, input, "INPUT", is_local, false);
  else
    add_command_file(0, tag, input, is_local, false);
  add_structure_file(is_local);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "job", is_local, true);
  add_output_file(0, tag, "extstr", "fort.34", is_local, true);
  add_output_file(1, tag, "32", "fort.33", is_local, true);
  return true;
}

void CRYSTAL::extract_calc::add_structure_file(const bool is_local)
{
  // dummy
}

void CRYSTAL::extract_str::add_structure_file(const bool is_local)
{
  add_data_file(1, structure_file, "fort.34", is_local, false);
}

void CRYSTAL::extract_calc::write(const DLV::string outfile,
				  char message[], const int_g mlen)
{
  std::ofstream output;
  std::ifstream datafile;
  if (DLV::open_file_read(datafile, filename.c_str(), message, mlen)) {
    bool ok = true;
    bool finished = false;
    char buff[256], title[256];
    // title
    datafile.getline(title, 256);
    // Calc type
    datafile.getline(buff, 256);
    if (strncmp(buff, "CRYSTAL", 7) != 0 and strncmp(buff, "SLAB", 4) != 0 and
	strncmp(buff, "POLYMER", 7) != 0 and
	strncmp(buff, "MOLECULE", 8) != 0 and
	strncmp(buff, "EXTERNAL", 8) != 0 and
	strncmp(buff, "DLVINPUT", 8) != 0) {
      strncpy(message, "File does not seem to be a CRYSTAL input file", mlen);
      ok = false;
    }
    if (ok) {
      if (DLV::open_file_write(output, outfile.c_str(), message, mlen)) {
	output << title << '\n';
	output << buff << '\n';
	do {
	  datafile.getline(buff, 256);
	  if (strncmp(buff, "OPTIMIZE", 8) == 0 or
	      strncmp(buff, "OPTBERNY", 8) == 0 or
	      strncmp(buff, "OPTGEOM", 7) == 0 or
	      strncmp(buff, "OPTCOORD", 8) == 0 or
	      strncmp(buff, "FREQCALC", 8) == 0 or
	      strncmp(buff, "CPHF", 4) == 0 or
	      strncmp(buff, "BASISSET", 8) == 0 or
	      strncmp(buff, "END", 3) == 0) {
	    output << "EXTPRT\n";
	    output << "COORPRT\n";
	    output << "STOP\n";
	    output << "END\n";
	    finished = true;
	  } else if (strcmp(buff, "EXTPRT") != 0 and
		     strcmp(buff, "STOP") != 0 and
		     strcmp(buff, "COORPRT") != 0) {
	    output << buff;
	    output << '\n';
	  }
	} while (ok and !finished);
	output.close();
      }
    }
    datafile.close();
  }
}

bool CRYSTAL::extract_calc::open_new_view() const
{
  return use_new_view;
}

void CRYSTAL::extract_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

bool CRYSTAL::extract_calc::recover(const bool no_err, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (no_err) {
    DLV::string label = "CRYSTAL extract (";
    label += DLV::get_file_name(filename);
    label += ")";
    DLV::model *m = read(label, get_outfile_name(0).c_str(),
			 false, message, len);
    bool ok = (m != 0);
    if (ok) {
      attach_model(m);
      set_current();
      if (set_bonds)
	set_bond_all();
      ok = read_data(get_outfile_name(1), id, message, len);
    }
    return ok;
  }
  return true;
}

bool CRYSTAL::extract_calc::read_data(const DLV::string filename,
				      const DLV::string id, char message[],
				      const int_g mlen)
{
  std::ifstream datafile;
  bool ok = DLV::open_file_read(datafile, filename.c_str(), message, mlen);
  if (ok) {
    int_g natoms;
    datafile >> natoms;
    char buff[256];
    // End of line and skip next line
    datafile.getline(buff, 128);
    datafile.getline(buff, 128);
    // Read the atoms.
    real_g (*coords)[3] = new real_g[natoms][3];
    for (int_g i = 0; i < natoms; i++) {
      datafile >> buff;
      datafile >> coords[i][0];
      datafile >> coords[i][1];
      datafile >> coords[i][2];
    }
    datafile.close();
    DLV::atom_integers *labels = new DLV::atom_integers(program_name, id, this,
							prim_atom_label);
    attach_data(labels);
    labels->set_grid(coords, natoms, false);
    int_g *data = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      data[i] = (i + 1);
    labels->add_data(data, "labels", false);
  }
  return ok;
}

bool CRYSTAL::extract_calc::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload CRYSTAL extract structure not implemented", mlen);
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::extract_calc *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::extract_calc("recover", false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::extract_str *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::extract_str("recover", "", false, false);
    }

  }
}

template <class Archive>
void CRYSTAL::extract_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::calc_geom>(*this);
  ar & filename;
  ar & use_new_view;
}

template <class Archive>
void CRYSTAL::extract_calc::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
  else
    ar & boost::serialization::base_object<CRYSTAL::calc_geom>(*this);
  ar & filename;
  ar & use_new_view;
}

template <class Archive>
void CRYSTAL::extract_str::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::extract_calc>(*this);
  ar & structure_file;
}

BOOST_CLASS_VERSION(CRYSTAL::extract_calc, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::extract_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::extract_str)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::calc_geom)

DLV_NORMAL_EXPLICIT(CRYSTAL::extract_calc)
DLV_NORMAL_EXPLICIT(CRYSTAL::extract_str)

#endif // DLV_USES_SERIALIZE
