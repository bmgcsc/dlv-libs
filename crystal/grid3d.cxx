
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"
#include "grid3d.hxx"

CRYSTAL::grid_3d_data::~grid_3d_data()
{
}

DLV::operation *CRYSTAL::grid_3d_calc::create(const bool do_cd,
					      const bool do_pot, const int_g np,
					      const int_g rs, const int_g cell,
					      const int_g tol, const real_l xi,
					      const real_l xa, const real_l yi,
					      const real_l ya, const real_l zi,
					      const real_l za,
					      const Density_Matrix &dm,
					      const NewK &nk,
					      const int_g version,
					      const DLV::job_setup_data &job,
					      const bool extern_job,
					      const char extern_dir[],
					      char message[],
					      const int_g mlen)
{
  grid_3d_calc *op = 0;
  message[0] = '\0';
  bool parallel = false;
  switch (version) {
  case CRYSTAL98:
    op = new grid_3d_calc_v4(true, np, rs, xi, xa, yi, ya, zi, za, dm, nk);
    break;
  case CRYSTAL03:
    op = new grid_3d_calc_v5(true, np, rs, cell,
			     xi, xa, yi, ya, zi, za, dm, nk);
    break;
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
    op = new grid_3d_calc_v6(do_cd, do_pot, np, rs, cell, tol,
			     xi, xa, yi, ya, zi, za, dm, nk);
    break;
  case CRYSTAL_DEV:
    op = new grid_3d_calc_v6(do_cd, do_pot, np, rs, cell, tol,
			     xi, xa, yi, ya, zi, za, dm, nk);
    parallel = job.is_parallel();
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::grid_3d_calc operation",
	      mlen);
  } else {
    op->attach(); // attach to find  wavefunction.
    if (op->create_files(parallel, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), op, op->get_model(), message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, parallel,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

DLV::operation *CRYSTAL::grid_3d_calc::tddft(const int_g calc,
					     const int_g exc, const int_g np,
					     const int_g rs, const int_g cell,
					     const int_g tol, const real_l xi,
					     const real_l xa, const real_l yi,
					     const real_l ya, const real_l zi,
					     const real_l za,
					     const Density_Matrix &dm,
					     const NewK &nk,
					     const int_g version,
					     const DLV::job_setup_data &job,
					     const bool extern_job,
					     const char extern_dir[],
					     char message[],
					     const int_g mlen)
{
  grid_3d_calc *op = 0;
  message[0] = '\0';
  bool parallel = false;
  switch (version) {
  case CRYSTAL98:
  case CRYSTAL03:
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
    strncpy(message, "TD-DFT calc not supported in the CRYSTAL version", mlen);
    break;
    //op = new tddft_3d_calc_v8(calc, exc, np, rs, cell, tol,
    //			      xi, xa, yi, ya, zi, za, dm, nk);
    //break;
  case CRYSTAL_DEV:
    op = new tddft_3d_calc_v8(calc, exc, np, rs, cell, tol,
			      xi, xa, yi, ya, zi, za, dm, nk);
    parallel = job.is_parallel();
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::grid_3d_calc operation",
	      mlen);
  } else {
    op->attach(); // attach to find  wavefunction.
    if (op->create_files(parallel, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), op, op->get_model(), message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, parallel,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

bool CRYSTAL::grid_3d_calc::is_tddft() const
{
  return false;
}

bool CRYSTAL::tddft_3d_calc_v8::is_tddft() const
{
  return true;
}

DLV::string CRYSTAL::grid_3d_calc::get_name() const
{
  return ("CRYSTAL 3D Grid calculation");
}

DLV::operation *CRYSTAL::load_3d_data::create(const char filename[],
					      const int_g version,
					      char message[], const int_g mlen)
{
  switch (version) {
  case CRYSTAL98:
    return load_3d_data_v4::create(filename, message, mlen);
  case CRYSTAL03:
    return load_3d_data_v5::create(filename, message, mlen);
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_3d_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_3d_data::get_name() const
{
  return ("Load CRYSTAL 3D data - " + get_filename());
}

DLV::operation *CRYSTAL::load_3d_data_v4::create(const char filename[],
                                                 char message[],
                                                 const int_g mlen)
{
  load_3d_data_v4 *op = new load_3d_data_v4(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::operation *CRYSTAL::load_3d_data_v5::create(const char filename[],
                                                 char message[],
                                                 const int_g mlen)
{
  load_3d_data_v5 *op = new load_3d_data_v5(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::operation *CRYSTAL::load_3d_data_v6::create(const char filename[],
                                                 char message[],
                                                 const int_g mlen)
{
  load_3d_data_v6 *op = new load_3d_data_v6(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::grid_3d_calc_v6::add_calc_error_file(const DLV::string tag,
						   const bool is_parallel,
						   const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::tddft_3d_calc_v8::add_calc_error_file(const DLV::string tag,
						    const bool is_parallel,
						    const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::grid_3d_calc::create_files(const bool is_parallel,
					 const bool is_local, char message[],
					 const int_g mlen)
{
  DLV::string filename;
  DLV::string tddftname;
  bool ok = find_wavefunction(filename, binary_wvfn, message, mlen);
  if (ok and is_tddft())
    ok = find_tddft_wavefn(tddftname, message, mlen);
  if (ok) {
    static char tag[] = "grid";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    if (is_tddft())
      add_data_file(2, tddftname, "TDDFT.RESTART", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.31", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::grid_3d_data::write_input(std::ofstream &output,
					const DLV::model *const structure)
{
  int_g dims = structure->get_number_of_periodic_dims();
  write_cell(output, dims);
  write_command(output);
  write_data_sets(output);
  output.precision(5);
  if (dims < 3) {
    if (use_range) {
      output << "RANGE\n";
      switch (dims) {
      case 0:
	output << x_min / C09_bohr_to_angstrom << ' ';
      case 1:
	output << y_min / C09_bohr_to_angstrom << ' ';
      case 2:
	output << z_min / C09_bohr_to_angstrom;
	output << '\n';
	break;
      default:
	break;
      }
      switch (dims) {
      case 0:
	output << x_max / C09_bohr_to_angstrom << ' ';
      case 1:
	output << y_max / C09_bohr_to_angstrom << ' ';
      case 2:
	output << z_max / C09_bohr_to_angstrom;
	output << '\n';
	break;
      default:
	break;
      }
    } else {
      output << "SCALE\n";
      switch (dims) {
      case 0:
	output << x_min << ' ';
      case 1:
	output << y_min << ' ';
      case 2:
	output << z_min;
	output << '\n';
	break;
      default:
	break;
      }
    }
  }
  output << "END\n";
  output.close();
}

void CRYSTAL::grid_3d_data::write_cell(std::ofstream &output,
				       const int_g dims) const
{
  if (conv_cell and dims == 3)
    output << "CONVCELL\n";
}

void CRYSTAL::grid_3d_data_v4::write_cell(std::ofstream &output,
					  const int_g dims) const
{
}

void CRYSTAL::grid_3d_data::write_command(std::ofstream &output) const
{
  output << "ECH3\n";
}

void CRYSTAL::grid_3d_data_v6::write_command(std::ofstream &output) const
{
  output << "GRID3D\n";
}

void CRYSTAL::tddft_3d_data_v8::write_command(std::ofstream &output) const
{
  output << "TD-DFT\n";
}

void CRYSTAL::grid_3d_data::write_points(std::ofstream &output) const
{
  output << npoints;
  output << '\n';
}

void CRYSTAL::grid_3d_data::write_data_sets(std::ofstream &output) const
{
  write_points(output);
}

void CRYSTAL::grid_3d_data_v6::write_data_sets(std::ofstream &output) const
{
  write_points(output);
  if (is_charge_calc())
    output << "CHARGE\n";
  if (calc_potential) {
    output << "POTENTIAL\n";
    output << pot_itol << '\n';
  }
  output << "END\n";
}

void CRYSTAL::tddft_3d_data_v8::write_data_sets(std::ofstream &output) const
{
  switch (property) {
  case 0:
    output << "LDR\n";
    break;
  case 1:
    output << "PARTICLE\n";
    break;
  case 2:
    output << "HOLE\n";
    break;
  case 3:
    output << "HPOVERLAP\n";
    break;
  }
  output << excitation << '\n';
  write_points(output);
}

void CRYSTAL::grid_3d_calc_v4::write(const DLV::string filename,
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output, structure);
  }
}

void CRYSTAL::grid_3d_calc_v5::write(const DLV::string filename,
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output, structure);
  }
  // Todo - why?
}

void CRYSTAL::grid_3d_calc_v6::write(const DLV::string filename,
				     const DLV::operation *op,
				     const DLV::model *const structure,
				     char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output, structure);
  }
}

void CRYSTAL::tddft_3d_calc_v8::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output, structure);
  }
}

// Todo - simplify these?
bool CRYSTAL::grid_3d_calc_v4::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Grid3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Grid3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::grid_3d_calc_v5::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Grid3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Grid3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::grid_3d_calc_v6::recover(const bool no_err, const bool log_ok,
				       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Grid3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Grid3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::tddft_3d_calc_v8::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "TD-DFT3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"TD-DFT3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_3d_data_v4::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::load_3d_data_v5::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::load_3d_data_v6::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::grid_3d_calc_v4::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

bool CRYSTAL::grid_3d_calc_v5::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

bool CRYSTAL::grid_3d_calc_v6::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

bool CRYSTAL::tddft_3d_calc_v8::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_3d_data_v4 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_3d_data_v4("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_3d_data_v5 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_3d_data_v5("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_3d_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_3d_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::grid_3d_calc_v4 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::grid_3d_calc_v4(false, 0, 0, 0.0, 0.0, 0.0,
				       0.0, 0.0, 0.0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::grid_3d_calc_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::grid_3d_calc_v5(false, 0, 0, 0, 0.0, 0.0, 0.0,
				       0.0, 0.0, 0.0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::grid_3d_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::grid_3d_calc_v6(false, false, 0, 0, 0, 0,
				       0.0, 0.0, 0.0, 0.0, 0.0, 0.0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::tddft_3d_calc_v8 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::tddft_3d_calc_v8(0, 0, 0, 0, 0, 0,
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::grid_3d_data::serialize(Archive &ar, const unsigned int version)
{
  ar & calc_charge;
  ar & npoints;
  ar & use_range;
  ar & conv_cell;
  ar & x_min;
  ar & x_max;
  ar & y_min;
  ar & y_max;
  ar & z_min;
  ar & z_max;
}

template <class Archive>
void CRYSTAL::grid_3d_data_v4::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_data_v5::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_data_v6::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & calc_potential;
  ar & pot_itol;
}

template <class Archive>
void CRYSTAL::tddft_3d_data_v8::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & property;
  ar & excitation;
}

template <class Archive>
void CRYSTAL::load_3d_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_3d_data_v4::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<load_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::load_3d_data_v5::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<load_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::load_3d_data_v6::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<load_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_calc_v4::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_calc_v5::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::grid_3d_calc_v6::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::tddft_3d_calc_v8::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::grid_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::tddft_3d_data_v8>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_3d_data_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_3d_data_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_3d_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::grid_3d_calc_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::grid_3d_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::grid_3d_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::tddft_3d_calc_v8)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_3d_data_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_3d_data_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_3d_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::grid_3d_calc_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::grid_3d_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::grid_3d_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::tddft_3d_calc_v8)

#endif // DLV_USES_SERIALIZE
