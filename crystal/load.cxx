
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "base.hxx"
#include "model.hxx"
#include "load.hxx"

DLV::operation *CRYSTAL::load_structure::create(const char name[],
						const char filename[],
						const bool frac_coords,
						const bool set_bonds,
						char message[],
						const int_g mlen)
{
  DLV::string model_name = name_from_file(name, filename);
  DLV::model *structure = read(model_name, filename, frac_coords,
			       message, mlen);
  load_structure *op = 0;
  if (structure != 0) {
    op = new load_structure(structure, filename);
    attach_base(op);
    if (set_bonds)
      op->set_bond_all();
  }
  return op;
}

DLV::string CRYSTAL::load_structure::get_name() const
{
  return (get_model_name() + " - Load CRYSTAL structure");
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_structure(0, "recover");
    }

  }
}

template <class Archive>
void CRYSTAL::load_structure::serialize(Archive &ar,
					const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_structure)

DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_structure)

#endif // DLV_USES_SERIALIZE
