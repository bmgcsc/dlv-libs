
#include <cmath>
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/file.hxx"
#include "../dlv/atom_prefs.hxx" // ghosts
#include "../dlv/extern_model.hxx"
#include "base.hxx"
#include "model.hxx"

DLV::model *CRYSTAL::structure_file::read(const DLV::string name,
					  const char filename[],
					  const bool frac,
					  char message[], const int_g mlen)
{
  // Todo - do some of these structures still use bohr? (properties?)
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[256];
      input.getline(line, 256);
      int_g dim, centre, lattice;
      if (sscanf(line, "%d %d %d", &dim, &centre, &lattice) != 3) {
	strncpy(message, "Incorrect file header - is this a CRYSTAL file?",
		mlen - 1);
	ok = DLV_ERROR;
	message[mlen - 1] = '\0';
      }
      if (dim < 0 or dim > 3) {
	strncpy(message, "Invalid model dimension - is this a CRYSTAL file?",
		mlen - 1);
	ok = DLV_ERROR;
      }
      if ((centre < 1 or centre > 7) or (lattice < 1 or lattice > 6)) {
	strncpy(message, "Invalid lattice/centre - is this a CRYSTAL file?",
		mlen - 1);
	ok = DLV_ERROR;
      }
      if (ok != DLV_ERROR) {
	structure = DLV::create_atoms(name, dim);
	if (structure == 0) {
	  strncpy(message, "Create model failed", mlen - 1);
	  ok = DLV_ERROR;
	} else {
	  // Get lattice
	  input.getline(line, 256);
	  double a[3];
	  if (sscanf(line, "%lf %lf %lf", &a[0], &a[1], &a[2]) != 3) {
	    strncpy(message, "Error reading a lattice vector", mlen - 1);
	    ok = DLV_ERROR;
	  } else {
	    input.getline(line, 256);
	    double b[3];
	    if (sscanf(line, "%lf %lf %lf", &b[0], &b[1], &b[2]) != 3) {
	      strncpy(message, "Error reading b lattice vector", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      input.getline(line, 256);
	      double c[3];
	      if (sscanf(line, "%lf %lf %lf", &c[0], &c[1], &c[2]) != 3) {
		strncpy(message, "Error reading c lattice vector", mlen - 1);
		ok = DLV_ERROR;
	      } else {
		if (dim > 0) {
		  DLV::coord_type aa[3];
		  aa[0] = a[0];
		  aa[1] = a[1];
		  aa[2] = a[2];
		  DLV::coord_type bb[3];
		  bb[0] = b[0];
		  bb[1] = b[1];
		  bb[2] = b[2];
		  DLV::coord_type cc[3];
		  cc[0] = c[0];
		  cc[1] = c[1];
		  cc[2] = c[2];
		  if (!structure->set_primitive_lattice(aa, bb, cc)) {
		    strncpy(message, "Error setting lattice vectors",
			    mlen - 1);
		    ok = DLV_ERROR;
		  }
		}
	      }
	    }
	  }
	  if (ok != DLV_ERROR) {
	    // Get Symmetry
	    input.getline(line, 256);
	    int_g nops;
	    sscanf(line, "%d", &nops);
	    if (nops < 1) {
	      strncpy(message, "Error reading number of symmetry operators",
		      mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      structure->set_crystal03_lattice_type(lattice, centre);
	      real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
	      real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
	      for (int_g i = 0; i < nops; i++) {
		for (int_g j = 0; j < 3; j++) {
		  input.getline(line, 256);
		  double r[3];
		  if (sscanf(line, "%lf %lf %lf", &r[0], &r[1], &r[2]) != 3) {
		    ok = DLV_ERROR;
		    break;
		  } else {
		    rotations[i][j][0] = r[0];
		    rotations[i][j][1] = r[1];
		    rotations[i][j][2] = r[2];
		  }
		}
		if (ok == DLV_ERROR)
		  break;
		input.getline(line, 256);
		double t[3];
		if (sscanf(line, "%lf %lf %lf", &t[0], &t[1], &t[2]) != 3) {
		  ok = DLV_ERROR;
		  break;
		} else {
		  translations[i][0] = t[0];
		  translations[i][1] = t[1];
		  translations[i][2] = t[2];
		}
	      }
	      if (ok != DLV_ERROR) {
		if (!structure->set_cartesian_sym_ops(rotations,
						      translations, nops)) {
		  strncpy(message, "Error setting symmetry operators",
			  mlen - 1);
		}
	      } else
		strncpy(message, "Error reading symmetry operators",
			mlen - 1);
	      delete_local_array(translations);
	      delete_local_array(rotations);
	    }
	  }
	  if (ok != DLV_ERROR) {
	    // Get atoms
	    input.getline(line, 256);
	    int_g natoms;
	    if (sscanf(line, "%d", &natoms) != 1) {
	      strncpy(message, "Error reading number of atoms", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      // Todo - can we be sure that line didn't wrap?
	      for (int_g i = 0; i < natoms; i++) {
		input.getline(line, 256);
		int_g id;
		double cx[3];
		if (sscanf(line, "%d %lf %lf %lf", &id, &cx[0],
			   &cx[1], &cx[2]) != 4) {
		  strncpy(message, "Error reading atoms", mlen - 1);
		  ok = DLV_ERROR;
		  break;
		} else {
		  DLV::coord_type coords[3];
		  coords[0] = cx[0];
		  coords[1] = cx[1];
		  coords[2] = cx[2];
		  //DLV::atom my_atom;
		  // Todo - frac and ghosts?
		  int_g index;
		  bool atom_ok;
		  if (frac)
		    atom_ok = structure->add_fractional_atom(index, (id % 100),
							     coords, false,
							     true);
		  else {
		    if (id % 100 == 0)
		      atom_ok = structure->add_cartesian_ghost_atom(coords);
		    else if (id % 100 == 93)
		      atom_ok = structure->add_cartesian_point_charge(coords);
		    else
		      atom_ok = structure->add_cartesian_atom(index, (id % 100),
							      coords);
		  }
		  if (!atom_ok) {
		    strncpy(message, "Error adding atom to model", mlen - 1);
		    ok = DLV_ERROR;
		    break;
		  } else
		    structure->set_atom_crystal03_id(index, id);
		}
	      }
	    }
	  }
	  if (ok == DLV_ERROR) {
	    delete structure;
	    structure = 0;
	  } else
	    structure->complete();
	}
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}

DLVreturn_type
CRYSTAL::structure_file::write(const DLV::string filename,
			       const DLV::model *const structure,
			       const int_g indices[], char message[],
			       const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    int_g dim = structure->get_number_of_periodic_dims();
    int_g lattice = 1;
    int_g centre = 1;
    DLV::coord_type a[3];
    DLV::coord_type b[3];
    DLV::coord_type c[3];
    if (dim > 0)
      structure->get_primitive_lattice(a, b, c);
    switch (dim) {
    case 0:
      for (int_g i = 0; i < 3; i++) {
	a[i] = 0.0;
	b[i] = 0.0;
	c[i] = 0.0;
      }
      a[0] = 500.0;
      b[1] = 500.0;
      c[2] = 500.0;
      break;
    case 1:
      for (int_g i = 0; i < 3; i++) {
	b[i] = 0.0;
	c[i] = 0.0;
      }
      b[1] = 500.0;
      c[2] = 500.0;
      break;
    case 2:
      for (int_g i = 0; i < 3; i++)
	c[i] = 0.0;
      c[2] = 500.0;
      break;
    case 3:
      lattice = structure->get_lattice_type();
      centre = structure->get_lattice_centring();
      // Todo - check that these are correct once we've written the fns
      centre = centre + 1;
      lattice = lattice + 1;
      if (lattice > 5)
	lattice--;
      break;
    }
    output.width(5);
    output << dim;
    output.width(5);
    output << centre;
    output.width(5);
    output << lattice;
    output << '\n';
    const real_l tol = 1e-8;
    output.width(24);
    output.precision(15);
    if (DLV::abs(a[0]) < tol)
      output << 0.0;
    else
      output << a[0];
    output.precision(15);
    output.width(24);
    if (DLV::abs(a[1]) < tol)
      output << 0.0;
    else
      output << a[1];
    output.precision(15);
    output.width(24);
    if (DLV::abs(a[2]) < tol)
      output << 0.0;
    else
      output << a[2];
    output << '\n';
    output.precision(15);
    output.width(24);
    if (DLV::abs(b[0]) < tol)
      output << 0.0;
    else
      output << b[0];
    output.precision(15);
    output.width(24);
    if (DLV::abs(b[1]) < tol)
      output << 0.0;
    else
      output << b[1];
    output.precision(15);
    output.width(24);
    if (DLV::abs(b[2]) < tol)
      output << 0.0;
    else
      output << b[2];
    output << '\n';
    output.precision(15);
    output.width(24);
    if (DLV::abs(c[0]) < tol)
      output << 0.0;
    else
      output << c[0];
    output.precision(15);
    output.width(24);
    if (DLV::abs(c[1]) < tol)
      output << 0.0;
    else
      output << c[1];
    output.precision(15);
    output.width(24);
    if (DLV::abs(c[2]) < tol)
      output << 0.0;
    else
      output << c[2];
    output << '\n';
    int_g nops = structure->get_number_of_sym_ops();
    real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
    real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
    structure->get_cart_rotation_operators(rotations, nops);
    structure->get_cart_translation_operators(translations, nops);
    output.width(5);
    output << nops << '\n';
    for (int_g i = 0; i < nops; i++) {
      for (int_g j = 0; j < 3; j++) {
	for (int_g k = 0; k < 3; k++) {
	  output.precision(15);
	  output.width(24);
	  if (DLV::abs(rotations[i][j][k]) < tol)
	    output << 0.0;
	  else
	    output << rotations[i][j][k];
	}
	output << '\n';
      }
      for (int_g j = 0; j < 3; j++) {
	output.precision(15);
	output.width(24);
	if (DLV::abs(translations[i][j]) < tol)
	  output << 0.0;
	else
	  output << translations[i][j];
      }
      output << '\n';
    }
    delete_local_array(translations);
    delete_local_array(rotations);
    int_g natoms = structure->get_number_of_asym_atoms();
    int_g *atom_types = new_local_array1(int_g, natoms);
    DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,natoms,3);
    structure->get_asym_crystal03_ids(atom_types, natoms);
    structure->get_asym_atom_cart_coords(coords, natoms);
    output.width(5);
    output << natoms << '\n';
    for (int_g i = 0; i < natoms; i++) {
      int_g j = 0;
      if (indices != 0)
	j = indices[i];
      else
	j = atom_types[i];
      if (j == DLV::ghost_index)
	j = 0;
      output.width(5);
      output << j;
      output.precision(15);
      output.width(24);
      if (DLV::abs(coords[i][0]) < tol)
	output << 0.0;
      else
	output << coords[i][0];
      output.precision(15);
      output.width(24);
      if (DLV::abs(coords[i][1]) < tol)
	output << 0.0;
      else
	output << coords[i][1];
      output.precision(15);
      output.width(24);
      if (DLV::abs(coords[i][2]) < tol)
	output << 0.0;
      else
	output << coords[i][2];
      output << '\n';
    }
    delete_local_array(coords);
    delete_local_array(atom_types);
    output.close();
  } else
    ok = DLV_ERROR;
  return ok;
}
