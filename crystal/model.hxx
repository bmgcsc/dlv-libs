
#ifndef CRYSTAL_MODEL
#define CRYSTAL_MODEL

namespace CRYSTAL {

  class structure_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    const bool frac, char message[], const int_g mlen);
    DLVreturn_type write(const DLV::string filename,
			 const DLV::model *const structure,
			 const int_g indices[], char message[],
			 const int_g mlen);
  };

}

#endif // CRYSTAL_MODEL
