
#ifndef CRYSTAL_MULLIKEN_ANALYSIS
#define CRYSTAL_MULLIKEN_ANALYSIS

namespace CRYSTAL {

  class mulliken_file {
  protected:
    DLV::data_object *read_data(DLV::operation *op, class wavefn_calc *wfn,
				const DLV::atom_integers *c,
				const char filename[], const DLV::string id,
				char message[], const int_g mlen);
  };

  class mulliken_data : public mulliken_file {
  protected:
    mulliken_data(const int_g n);

    void write_cmd(std::ofstream &output);
    void write_cmd_v6(std::ofstream &output);

  private:
    int_g neighbours;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_data_v5 : public mulliken_data, public property_data {
  protected:
    mulliken_data_v5(const int_g n, const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_data_v6 : public mulliken_data, public property_data_v6 {
  protected:
    mulliken_data_v6(const int_g n, const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_data_v7 : public mulliken_data, public property_data_v6 {
  protected:
    mulliken_data_v7(const int_g n, const bool r,
		     const Density_Matrix &dm, const NewK &nk);
    void write_rotation(std::ofstream &output,
			const DLV::model *const structure,
			const DLV::atom_integers *data,
			char message[], const int_g mlen);
    bool has_rotate() const;

  private:
    bool rotate;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // v5+ only
  class load_mulliken_data : public DLV::load_data_op , public mulliken_file {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialize
    load_mulliken_data(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_calc : public property_calc {
  public:
    static DLV::operation *create(const int_g n, const int_g rotate,
				  const Density_Matrix &dm,
				  const NewK &nk, const int_g version,
				  const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_calc_v5 : public mulliken_calc, public mulliken_data_v5 {
  public:
    mulliken_calc_v5(const int_g n, const Density_Matrix &dm, const NewK &nk);

  protected:
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_calc_v6 : public mulliken_calc, public mulliken_data_v6 {
  public:
    mulliken_calc_v6(const int_g n, const Density_Matrix &dm, const NewK &nk);

  protected:
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class mulliken_calc_v7 : public mulliken_calc, public mulliken_data_v7 {
  public:
    mulliken_calc_v7(const int_g n, const bool r,
		     const Density_Matrix &dm, const NewK &nk);

  protected:
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);

  private:
    const DLV::atom_integers *prim;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_mulliken_data)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::mulliken_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::mulliken_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::mulliken_calc_v7)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::mulliken_data::mulliken_data(const int_g n) : neighbours(n)
{
}

inline CRYSTAL::mulliken_data_v5::mulliken_data_v5(const int_g n,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data(n), property_data(dm, nk)
{
}

inline CRYSTAL::mulliken_data_v6::mulliken_data_v6(const int_g n,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data(n), property_data_v6(dm, nk)
{
}

inline CRYSTAL::mulliken_data_v7::mulliken_data_v7(const int_g n, const bool r,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data(n), property_data_v6(dm, nk), rotate(r)
{
}

inline bool CRYSTAL::mulliken_data_v7::has_rotate() const
{
  return rotate;
}

inline CRYSTAL::load_mulliken_data::load_mulliken_data(const char file[])
  : load_data_op(file)
{
}

inline bool CRYSTAL::mulliken_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::mulliken_calc_v5::mulliken_calc_v5(const int_g n,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data_v5(n, dm, nk)
{
}

inline CRYSTAL::mulliken_calc_v6::mulliken_calc_v6(const int_g n,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data_v6(n, dm, nk)
{
}

inline CRYSTAL::mulliken_calc_v7::mulliken_calc_v7(const int_g n, const bool r,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : mulliken_data_v7(n, r, dm, nk), prim(0)
{
}

#endif // CRYSTAL_MULLIKEN_ANALYSIS
