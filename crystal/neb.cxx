//clb to do list
// make use of messages were appropiate
// got to L900 bool CRYSTAL::neb_calc::recover(
// check for's are on a separate line

#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/math_fns.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/op_model.hxx" // needed for struct.hxx
#include "calcs.hxx"
#include "scf.hxx"
#include <sstream>
#include "neb.hxx"
#include "struct.hxx"  // CRYSTAL::load_structure 

DLV::operation *CRYSTAL::neb_data::neb_editing = 0;

DLV::string CRYSTAL::neb_calc::get_name() const
{
  return ("CRYSTAL CI-NEB calculation");
}

void CRYSTAL::neb_data::set_params(const Neb &n)
{
  data = n;
}

CRYSTAL::Neb CRYSTAL::neb_data::get_data() const
{
  return data;
}

bool CRYSTAL::neb_data::get_restart() const
{
  return data.restart;
}

DLV::string CRYSTAL::neb_data::get_restart_file() const
{
  return data.restart_file;
}

DLV::string CRYSTAL::neb_data::get_images_dir()const
{
  return data.images_dir;
}

CRYSTAL::int_g CRYSTAL::neb_data::get_end_model() const
{
  return data.end_model;
}

CRYSTAL::int_g CRYSTAL::neb_data::get_nimages() const
{
  return data.nimages;
}

CRYSTAL::int_g CRYSTAL::neb_data::get_natoms() const
{
  return data.natoms;
}

bool CRYSTAL::neb_data::get_nocalc() const
{
  return data.nocalc;
}

void CRYSTAL::neb_data::get_mapping(int_g maparray[]) const
{
  for(int_g i=0;i<data.natoms;i++) maparray[i] = data.mapping[i];
  return;
}

void CRYSTAL::neb_data::set_nebop1(DLV::operation *op1)
{
  nebop1 = op1;
  return;
}

DLV::operation *CRYSTAL::neb_data::get_nebop1() const
{
  return nebop1;
}

void CRYSTAL::neb_data::set_nebop2(DLV::operation *op2)
{
  nebop2 = op2;
  return;
}

DLV::operation *CRYSTAL::neb_data::get_nebop2() const
{
  return nebop2;
}

void CRYSTAL::neb_data::set_neb_editing(DLV::operation *op)
{
  neb_editing = op;
  return;
}

DLV::operation *CRYSTAL::neb_data::get_neb_editing()
{
  return neb_editing;
}

void CRYSTAL::neb_data::set_images_arrays(const int_g n)
{
  updated_images = new bool[n];
  for (int_g i=0;i<n;i++) updated_images[i] = false;
  images = new DLV::operation*[n];
}

void CRYSTAL::neb_data::set_image_updated(const int_g image)
{
  updated_images[image] = true;
}

void CRYSTAL::neb_data::set_images(const int_g image, DLV::operation *op)
{
  images[image] = op;
}

bool CRYSTAL::neb_data::image_updated(const int_g image)
{
  return updated_images[image];
}

DLV::operation *CRYSTAL::neb_data::get_image_op(const int_g image)
{
  return images[image];
}

const CRYSTAL::int_g CRYSTAL::neb_data::get_frames_per_image()
{
  return frames_per_image;
}


DLV::operation *CRYSTAL::neb_calc_v8_final::get_nebop2() 
{
  DLV::operation *base1 = get_editing();
  neb_calc_v8_final* op1=dynamic_cast<neb_calc_v8_final*> (base1);   
  return op1->neb_data::get_nebop2();
}

DLV::operation *CRYSTAL::neb_calc_v8_final::get_neb_image_op(const int_g n) 
{
  DLV::operation *base1 = get_editing();
  neb_calc_v8_final* op1=dynamic_cast<neb_calc_v8_final*> (base1);   
  return op1->neb_data::get_image_op(n);
}

void  CRYSTAL::neb_calc_v8_final::set_nebop2(DLV::operation *op2)
{
  //dummy
}

void CRYSTAL::neb_calc_v8_ini1::set_nebop2(DLV::operation *op2)
{
  neb_data::set_nebop2(op2);
}

void CRYSTAL::neb_calc_v8_ini2::set_nebop2(DLV::operation *op2)
{
  neb_data::set_nebop2(op2);
}

void CRYSTAL::neb_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

void CRYSTAL::neb_calc::set_images_arrays(const int_g n, const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  op1->set_images_arrays(n);
}

void CRYSTAL::neb_calc_v8_final::set_images_arrays(const int_g n)
{
  neb_data::set_images_arrays(n);
}

void CRYSTAL::neb_calc_v8_ini1::set_images_arrays(const int_g n)
{
  //dummy
}

void CRYSTAL::neb_calc_v8_ini2::set_images_arrays(const int_g n)
{
  neb_data::set_images_arrays(n);
}

void CRYSTAL::neb_calc::save_image_str(const int_g image,
				       DLV::operation *op, const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  op1->save_image_str(image, op);
}

void CRYSTAL::neb_calc_v8_final::save_image_str(const int_g image, 
						DLV::operation *op)
{
  neb_data::set_images(image, op);
  neb_data::set_image_updated(image);
}

void CRYSTAL::neb_calc_v8_ini1::save_image_str(const int_g image, 
					       DLV::operation *op)
{
  //dummy
}

void CRYSTAL::neb_calc_v8_ini2::save_image_str(const int_g image, 
					       DLV::operation *op)
{
  //dummy
}

DLV::operation *CRYSTAL::neb_calc::get_image_op(const int_g image, 
						const char images_dir[], 
						const bool bond_all,
						const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  return op1->get_image_op(image, images_dir, bond_all);
}

DLV::operation 
*CRYSTAL::neb_calc_v8_final::get_image_op(const int_g image, 
					  const char images_dir[],
					  const bool bond_all)
{
  DLV::operation *op = 0;
  char message[256];
  message[0] = '\0';
  if(image_updated(image)){
    op = neb_data::get_image_op(image);
  }
  else{
    DLV::string file;
    char data_file[256];
    for(int_g i=0; i<256;i++) data_file[i] = '\0';
    const char *name = "";
    std::stringstream my_integer;
    bool fractional = true;
    my_integer.str("");
    my_integer << image+1;
    file = "neb.im" + my_integer.str() + "str";
    int_g i = 0;
    while(images_dir[i] !=0){
      data_file[i] = images_dir[i];
      i++;
    }
    int_g j = 0;
    while(file[j] !=0){
      data_file[i] = file[j];
      i++;
      j++;
    }
    op = CRYSTAL::load_structure::create(name, data_file, fractional,
					 bond_all, message, 256);
    if(op!=0) {
      set_images(image, op);
      set_image_updated(image);
    }
  }
  return op;
}

DLV::operation 
*CRYSTAL::neb_calc_v8_ini1::get_image_op(const int_g image, 
					 const char images_dir[],
					 const bool bond_all)
{
  return 0;  //dummy
}

DLV::operation 
*CRYSTAL::neb_calc_v8_ini2::get_image_op(const int_g image, 
					 const char images_dir[],
					 const bool bond_all)
{
  return 0;  //dummy
}

void CRYSTAL::neb_calc::reset_editing()
{
  set_editing(CRYSTAL::neb_data::get_neb_editing());
}

CRYSTAL::int_g
CRYSTAL::neb_calc::map_neb_structures(int_g mapping[], const int_g natoms, 
				      const int_g end_model, const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  return op1->map_neb_structures(mapping, natoms, end_model);
}

CRYSTAL::int_g
CRYSTAL::neb_calc_v8_final::map_neb_structures(int_g mapping[],
					       const int_g natoms,
					       const int_g end_model)
{
  neb_data::set_nebop1(this);
  neb_data::set_nebop2(get_neb_op(end_model));
  DLV::model *m1 = get_model();
  DLV::model *m2 = get_neb_op(end_model)->get_model();
  bool success;
  bool *tracking = new_local_array1(bool, natoms);
  int_g *atom_types1 = new_local_array1(int_g, natoms);
  int_g *atom_types2 = new_local_array1(int_g, natoms);
  m1->get_asym_crystal03_ids(atom_types1, natoms);
  m2->get_asym_crystal03_ids(atom_types2, natoms);
  for(int_g i=0;i<natoms;i++){ 
    mapping[i] = -1;
    tracking[i] = 1;
  }
  // Calculate symmetry related atoms and choose the best ones
  // for each atom calculate its primitive atoms + use closest to origin
  // List primitive atoms of both strucutres
  int_g natoms_prim = m1->get_number_of_primitive_atoms();
  int_g *indices1 = new_local_array1(int_g, natoms_prim);
  int_g *indices2 = new_local_array1(int_g, natoms_prim);
  DLV::coord_type (*coords3)[3] = new_local_array2(DLV::coord_type,natoms_prim,3);
  DLV::coord_type (*coords4)[3] = new_local_array2(DLV::coord_type,natoms_prim,3);
  DLV::coord_type (*fracs3)[3] = new_local_array2(DLV::coord_type,natoms_prim,3);
  DLV::coord_type (*fracs4)[3] = new_local_array2(DLV::coord_type,natoms_prim,3);
  m1->get_primitive_atom_cart_coords(coords3, natoms_prim);
  m2->get_primitive_atom_cart_coords(coords4, natoms_prim);
  m1->get_primitive_asym_indices(indices1, natoms_prim);
  m2->get_primitive_asym_indices(indices2, natoms_prim);
  m1->get_primitive_atom_frac_coords(fracs3, natoms_prim);
  m2->get_primitive_atom_frac_coords(fracs4, natoms_prim);
  //get cell parameters or similar here and check both
  DLV::coord_type (*lattice)[3] = new_local_array2(DLV::coord_type,3,3);
  DLV::coord_type (*inverse)[3] = new_local_array2(DLV::coord_type,3,3);
  m1->get_conventional_lattice(lattice[0], lattice[1], lattice[2]);
  DLV::matrix_invert(lattice, inverse, 1);
  real_g deltax, deltay, deltaz;
  DLV::coord_type frac[3];
  DLV::coord_type coords_temp[3];
  int_g dim = get_model_type();
  //Mapping identical atoms that move < 0.25A using primitive lattice\n";
  for(int_g i=0; i<natoms; i++){
    if(mapping[i]==-1){  //redundant
      success = 0;
      for(int_g j=0; j<natoms_prim; j++){
	if(indices1[j]==i){
	  for(int_g k=0; k<natoms; k++){
	    if(tracking[k]){
	      for(int_g p=0; p<natoms_prim; p++){
		if(indices2[p]==k){
		  if(atom_types1[i] == atom_types2[k]){
		    deltax = fabs(coords3[j][0]-coords4[p][0]);
		    deltay = fabs(coords3[j][1]-coords4[p][1]);
		    deltaz = fabs(coords3[j][2]-coords4[p][2]);
		    real_l dist = deltax*deltax + deltay*deltay + deltaz*deltaz;
		    dist = sqrt(dist);
		    if(dist < 0.25){
		      mapping[i] = k;
		      tracking[k] = 0;
		      success = 1;					
		      // edit prim atoms
		      m1->set_indexed_asym_cart_coords(coords3[j],i);
		      m2->set_indexed_asym_cart_coords(coords4[p],k);
		      break;
		    }
		    // check if xyz position is close to cell.
		    for (int_g q = 0; q < dim; q++)
		      if(fabs(fracs3[j][q] - fracs4[p][q]) > 0.9){
			for (int_g r = 0; r < dim; r++)
			  coords_temp[r] = 0.0;
			if (fracs3[j][q] > fracs4[p][q]){
			  for (int_g r = 0; r < 3; r++)
			    frac[r] = fracs3[j][r]; 
			  frac[q] -= 1.0;
			  for (int_g r = 0; r < dim; r++)
			    for (int_g s = 0; s < dim; s++)
			      coords_temp[r] += lattice[r][s] * frac[s];
			  for (int_g r = dim; r < 3; r++)
			    coords_temp[r] = coords3[j][r];
			  deltax = fabs(coords_temp[0]-coords4[p][0]);
			  deltay = fabs(coords_temp[1]-coords4[p][1]);
			  deltaz = fabs(coords_temp[2]-coords4[p][2]);
			  dist = sqrt(deltax*deltax + deltay*deltay + deltaz*deltaz);
			  if(dist < 0.25){
			    for (int_g r = 0; r < dim; r++)
			      coords3[j][r] = coords_temp[r];
			    break;
			  }
			}
			else{
			  for (int_g r = 0; r < 3; r++)
			    frac[r] = fracs4[p][r];
			  frac[q] -=1.0;
			  for (int_g r = 0; r < dim; r++)
			    for (int_g s = 0; s < dim; s++)
			      coords_temp[r] += lattice[r][s] * frac[s];
			  for (int_g r = dim; r < 3; r++)
			    coords_temp[r] = coords4[p][r];
			  deltax = fabs(coords3[j][0] - coords_temp[0]);
			  deltay = fabs(coords3[j][1] - coords_temp[1]);
			  deltaz = fabs(coords3[j][2] - coords_temp[2]);
			  dist = sqrt(deltax*deltax + deltay*deltay + deltaz*deltaz);
			  if(dist < 0.25){
			    for (int_g r = 0; r < dim; r++)
			      coords4[p][r] = coords_temp[r];  
			    break;
			  }
			}
		      }
		    if(dist < 0.25){
		      mapping[i] = k;
		      tracking[k] = 0;
		      success = 1;					
		      // edit prim atoms
		      m1->set_indexed_asym_cart_coords(coords3[j],i);
		      m2->set_indexed_asym_cart_coords(coords4[p],k);
		      break;
		    }
		  }
		}
	      }
	    }
	    if(success) break;
	  }
	}
	if(success) break;
      }
    }
  }
  // Check if we have mapped everything
  success = 1;
  for(int_g i=0; i<natoms; i++){
    if(mapping[i]==-1){
      success = 0;
      break;
    }
  }
  if(success==1) return 1;
  // Map atoms if there is only one of a type left  
  const int_g size = 300;
  int_g mylist[size];
  int_g map1;
  for(int_g i=0; i<size; i++) 
    mylist[i] = 0;
  for(int_g i=0; i<natoms; i++)
    if(mapping[i]==-1) mylist[atom_types1[i]]++;
  for(int_g i=0; i<size; i++){ 
    if(mylist[i] == 1){
      for(int_g j=0;j<natoms; j++)
	if((atom_types1[j]==i)&&(mapping[j]==-1)){
	  map1 = j;
	  break;
	}
      for(int_g j=0; j<natoms; j++){
	if((atom_types2[j]==i)&&(tracking[j])){
	  int_g p, q;
	  mapping[map1] = j;
	  tracking[j] = 0;
	  // attempt to get correct primitive atoms
	  real_l min_dist = 999999.0;
	  for(int_g k=0; k<natoms_prim; k++){
	    if(indices1[k] == map1){
	      for(int_g n=0; n<natoms_prim; n++){
		if(indices2[n] == j){
		  real_l dist = pow(coords3[k][0]-coords4[n][0],2)
		    + pow(coords3[k][1]-coords4[n][1],2)
		    + pow(coords3[k][2]-coords4[n][2],2);
		  if(dist<min_dist){
		    min_dist = dist;
		    p = k;
		    q = n;
		  }
		}
	      }
	    }
	  }
	  m1->set_indexed_asym_cart_coords(coords3[p],map1);
	  m2->set_indexed_asym_cart_coords(coords4[q],j);
	  break;
	}
      }
    }
  }
  // Check if we have mapped everything
  success = 1;
  for(int_g i=0; i<natoms; i++){
    if(mapping[i]==-1){
      success = 0;
      break;
    }
  }
  if(success==1) return 2;
  // Consider nearest neighbours of remaining atoms
  int_g unpaired_atoms = 0;
  int_g max_neighbours = 10;
  real_l max_separation = 3.0;
  real_l dist;
  for(int_g i=0; i<natoms; i++)
    if(mapping[i]==-1) unpaired_atoms++;
  int_g **str1_list = new int_g *[unpaired_atoms];
  int_g **str2_list = new int_g *[unpaired_atoms];
  for(int_g i=0; i<unpaired_atoms; i++){
    str1_list[i] = new int_g[max_neighbours+1];
    str2_list[i] = new int_g[max_neighbours+1];
  }
  for(int_g i=0; i<unpaired_atoms; i++)
    for(int_g j=0; j<max_neighbours+1; j++){ 
      str1_list[i][j] = -1;
      str2_list[i][j] = -1;
    }
  // get nearest neighbours for strucutre 1
  int_g num_neighbours;
  int_g atom = -1;
  for(int_g i=0; i<natoms; i++){
    if(mapping[i]==-1){
      num_neighbours = 0;
      atom++;
      str1_list[atom][num_neighbours] = i;
      for(int_g j=0; j<natoms_prim; j++){
	if(indices1[j]==i){
	  for(int_g k=0; k<natoms; k++){
	    for(int_g n=0; n<natoms_prim; n++){
	      if(indices1[n]==k){
		dist = pow(coords3[j][0]-coords3[n][0],2)
		  + pow(coords3[j][1]-coords3[n][1],2)
		  + pow(coords3[j][2]-coords3[n][2],2);
		dist = sqrt(dist);
		if((dist < max_separation)&&(k!=i)){
		  success = 1;
		  for(int_g p=0; p<num_neighbours; p++)
		    if(str1_list[atom][p+1]==k) success = 0;
		  if(success){
		    str1_list[atom][num_neighbours+1] = k;
		    num_neighbours++;
		    break;
		  }
		}
	      }
	    }
	    if(num_neighbours==max_neighbours) break;
	  }
	}
	if(num_neighbours==max_neighbours) break;
      }
    }
  }	    
  // get nearest neighbours for strucutre 2
  atom = -1;
  for(int_g i=0; i<natoms; i++){
    if(tracking[i]){
      num_neighbours = 0;
      atom++;
      str2_list[atom][num_neighbours] = i;
      for(int_g j=0; j<natoms_prim; j++){
	if(indices2[j]==i){
	  for(int_g k=0; k<natoms; k++){
	    for(int_g n=0; n<natoms_prim; n++){
	      if(indices2[n]==k){	
		dist = pow(coords4[j][0]-coords4[n][0],2)
		  + pow(coords4[j][1]-coords4[n][1],2)
		  + pow(coords4[j][2]-coords4[n][2],2);
		dist = sqrt(dist);
		if((dist < max_separation)&&(k!=i)){
		  success = 1;
		  for(int_g p=0; p<num_neighbours; p++)
		    if(str2_list[atom][p+1]==k) success = 0;
		  if(success){
		    str2_list[atom][num_neighbours+1] = k;
		    num_neighbours++;
		    break;
		  }
		}	
	      }
	    }
	    if(num_neighbours==max_neighbours) break;
	  }
	}
	if(num_neighbours==max_neighbours) break;
      }
    }
  }
  //  list types (and amounts of) un-mapped atoms
  int_g num_types = 0;
  int_g half = unpaired_atoms/2;
  int_g **store_types = new int_g*[half];
  for(int_g j=0; j<half; j++) 
    store_types[j] = new int_g[2];
  for(int_g j=0; j<half; j++){ 
    store_types[j][0] = 0;
    store_types[j][1] = 0;
  }
  for(int_g i=0; i<unpaired_atoms;i++){
    success = 1;
    for(int_g j=0; j<unpaired_atoms/2; j++){
      if(atom_types1[str1_list[i][0]] == store_types[j][0]){
	success = 0;
	store_types[j][1]++;
	break;
      }
    }
    if(success){
      store_types[num_types][0] = atom_types1[str1_list[i][0]];
      store_types[num_types][1]++;
      num_types++;
    }
  }
  //  map atoms from nearest neighbour lists
  bool successful_mapping;
  int_g counter;
  do{
    successful_mapping = 0;
    int_g num, type;
    for(int_g j=0; j<num_types; j++){
      type = store_types[j][0];
      num = store_types[j][1];
      int_g **matrix = new int_g*[num+1];
      for(int_g k=0; k<num+1; k++) matrix[k] = new int_g[num+1];
      for(int_g k=0; k<num+1; k++)
	for(int_g n=0; n<num+1; n++) 
	  matrix[k][n] = 0; 
      // Get atom numbers
      counter = 0;
      for(int_g k=0; k<unpaired_atoms; k++)
	if(atom_types1[str1_list[k][0]] == type){
	  counter++;
	  matrix[0][counter] = str1_list[k][0];
	  if(counter==num) break;
	}
      counter = 0;
      for(int_g k=0; k<unpaired_atoms; k++){
	if(atom_types2[str2_list[k][0]] == type){
	  counter++;
	  matrix[counter][0] = str2_list[k][0];
	  if(counter==num) break;
	}
      }
      // Get pairing probabilities
      for(int_g k=0; k<num; k++){
	for(int_g n=0; n<num; n++){
	  int_g pos1;
	  int_g pos2;
	  int_g p;
	  for(p=0; p<unpaired_atoms; p++)
	    if(str1_list[p][0]==matrix[0][k+1]) break;
	  pos1 = p;
	  for(p=0;p<unpaired_atoms;p++)
	    if(str2_list[p][0]==matrix[n+1][0]) break;
	  pos2 = p;
	  for(int_g p=0;p<max_neighbours;p++){
	    if(str1_list[pos1][p+1]==-1) break;
	    for(int_g q=0; q<max_neighbours; q++){ 
	      if(str2_list[pos2][q+1]==-1) break;
	      if(mapping[str1_list[pos1][p+1]]==str2_list[pos2][q+1])
		matrix[n+1][k+1]++;
	    }
	  }
	}
      }
      // analyse to see if matches can be made
      do{
	int_g max = 0;
	int_g a, b;
	for(int_g k=0; k<num; k++)
	  for(int_g n=0; n<num; n++)
	    if(matrix[k+1][n+1] > max){
	      max = matrix[k+1][n+1];
	      a = k+1;
	      b = n+1;
	      success = 1;
	    }
	    else 
	      if(matrix[k+1][n+1] == max) success = 0;
	if(success){
	  int_g p, q;
	  successful_mapping = 1;
	  mapping[matrix[0][b]] = matrix[a][0];
	  tracking[matrix[a][0]] = 0;
	  // get correct primitive stuff
	  real_l min_dist = 999999.0;
	  for(int_g k=0; k<natoms_prim; k++)
	    if(indices1[k] == matrix[0][b])
	      for(int_g n=0; n<natoms_prim; n++)
		if(indices2[n] == matrix[a][0]){
		  real_l dist = pow(coords3[k][0]-coords4[n][0],2)
		    + pow(coords3[k][1]-coords4[n][1],2)
		    + pow(coords3[k][2]-coords4[n][2],2);
		  if(dist<min_dist){
		    min_dist = dist;
		    p = k;
		    q = n;
		  }
		}
	  m1->set_indexed_asym_cart_coords(coords3[p],matrix[0][b]);
	  m2->set_indexed_asym_cart_coords(coords4[q],matrix[a][0]);
	  for(int_g k=0; k<num; k++){
	    matrix[a][k+1] = 0;
	    matrix[k+1][b] = 0;
	  }
	}
      }while(success);
    }  //loop over num_types
    if(successful_mapping){ //update num_types and store_types
      //also need to update str1_list and str2_list and unpaired_atoms
      int_g atoms_left = 0;
      int_g p;
      for(int_g k=0; k<natoms; k++) 
	atoms_left += tracking[k];
      if(atoms_left==0) 
	successful_mapping = 0;  //complete
      else{
	//update str1_list and str2_list
	// str1 update
	int_g atom = -1;
	for(int_g i=0; i<natoms; i++){
	  if(mapping[i]==-1){
	    atom++;
	    if(str1_list[atom][0] != i){
	      for(int_g j=atom+1; j<unpaired_atoms; j++)
		if(str1_list[j][0] != i){
		  for(int_g k=0; k<max_neighbours+1; k++)
		    str1_list[atom][k] = str1_list[j][k];
		  break;
		}
	    }
	  }
	}
	// str2 update
	atom = -1;
	for(int_g i=0; i<natoms; i++){
	  if(tracking[i]){
            atom++;
	    if(str2_list[atom][0] != i){
	      for(int_g j=atom+1; j<unpaired_atoms; j++)
		if(str2_list[j][0] != i){
		  for(int_g k=0; k<max_neighbours+1; k++)
		    str2_list[atom][k] = str2_list[j][k];
		  break;
		}
	    }
	  }
	}
	unpaired_atoms = atoms_left; //clb
	//update store_types
	for (int_g k=0; k<num_types; k++){
	  store_types[k][0] = 0;
	  store_types[k][1] = 0;
	}		    
	p = 0;
	for(int_g k=0; k<natoms; k++)
	  if(mapping[k]==-1){
	    success = 1;
	    for(int_g n=0; n<num_types; n++){
	      if(atom_types1[k]==store_types[n][0]){
		success = 0;
		store_types[n][1]++;
		break;
	      }
	    }
	    if(success){
	      store_types[p][0] = atom_types1[k];
	      store_types[p][1]++;
	      p++;
	    }
	  }	
	num_types = p;
      }
    }
  }while(successful_mapping);
  // Check if we have mapped everything
  success = 1;
  for(int_g i=0; i<natoms; i++){
    if(mapping[i]==-1){
      success = 0;
      break;
    }
  }
  if(success==1) return 3;
  // Finally minimise length of overall path
  for(int_g i=0; i<num_types; i++){
    real_g dist;
    int_g a, b, c, d;
    do{
      real_g min_dist = 99999.9;
      for(int_g j=0; j<natoms; j++)
	if((mapping[j]==-1) && (atom_types1[j]==store_types[i][0]))
	  for(int_g n=0; n<natoms_prim; n++)
	    if(indices1[n]==j)
	      for(int_g k=0; k<natoms; k++)
		if((tracking[k]) && (atom_types2[k]==store_types[i][0]))
		  for(int_g p=0; p<natoms_prim; p++)
		    if(indices2[p]==k){
		      dist = pow(coords3[n][0]-coords4[p][0],2)
			+ pow(coords3[n][1]-coords4[p][1],2)
			+ pow(coords3[n][2]-coords4[p][2],2);
		      if (dist < min_dist){
			min_dist = dist;
			a = j;
			b = k;
			c = n;
			d = p;
		      }	
		    }
      mapping[a] = b;
      tracking[b] = 0;
      m1->set_indexed_asym_cart_coords(coords3[c], a);
      m2->set_indexed_asym_cart_coords(coords4[d], b);
      store_types[i][1]--;
    }while(store_types[i][1]>0);
  }
  return 4;
}

CRYSTAL::int_g
CRYSTAL::neb_calc_v8_ini1::map_neb_structures(int_g mapping[],
					      const int_g natoms,
					      const int_g end_model)
{
  return 0;    //dummy
}

CRYSTAL::int_g CRYSTAL::neb_calc_v8_ini2::map_neb_structures(int_g mapping[],
						  const int_g natoms,
						  const int_g end_model)
{
  return 0;  //dummy
}

void CRYSTAL::neb_calc::view_mapping_str1(const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  op1->view_mapping_str1();
}

void CRYSTAL::neb_calc_v8_final::view_mapping_str1()
{
  DLV::operation *op1 = this;
  DLV::model *struc1 = get_nebop1()->get_model();
  //  DLV::model *struc1 = get_model();  //could just get it now
  //  neb_data::set_struc1(struc1);
  int_g totatoms = struc1->get_number_of_primitive_atoms();
  DLV::coord_type (*tmpcoords)[3] 
    = new_local_array2(DLV::coord_type, totatoms, 3);    
  struc1->get_primitive_atom_cart_coords(tmpcoords, totatoms);
  int_g *indices = new int_g[totatoms];
  struc1->get_primitive_asym_indices(indices, totatoms);
  DLV::atom_integers *labels1 = 
    new DLV::atom_integers("CRYSTAL NEB", "000", op1,
			   "Atom labels");
  real_g (*coords)[3] = new real_g[totatoms][3];
  for (int_g i = 0; i < totatoms; i++) {
    for (int_g j = 0; j < 3; j++) 
      coords[i][j] = (real_g)tmpcoords[i][j];
  }
  labels1->set_grid(coords, totatoms, true); 
  int_g *data = new int_g[totatoms];
  for (int_g i = 0; i < totatoms; i++)
    data[i] = indices[i] + 1;
  labels1->add_data(data, "labels1", true);
  attach_data(labels1);  //why can't these now be protected????
#ifdef ENABLE_DLV_GRAPHICS
  render_data(labels1);
  labels1-> set_3D_display(struc1);
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::neb_calc_v8_ini1::view_mapping_str1()
{
  //dummy 
}

void CRYSTAL::neb_calc_v8_ini2::view_mapping_str1()
{
  //dummy 
}


void CRYSTAL::neb_calc::view_mapping_str2(const int_g natoms,
					  const int_g end_model, 
					  const int_g mapping[],
					  const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  op1->view_mapping_str2(natoms, end_model, mapping);
}
void CRYSTAL::neb_calc_v8_final::view_mapping_str2(const int_g natoms, 
						   const int_g end_model,
						   const int_g mapping[])
{
  DLV::model *struc2 = get_nebop2()->get_model();
  int_g totatoms = struc2->get_number_of_primitive_atoms();
  DLV::coord_type (*tmpcoords)[3] 
    = new_local_array2(DLV::coord_type, totatoms, 3); 
  int_g *indices = new int_g[totatoms];
  struc2->get_primitive_atom_cart_coords(tmpcoords, totatoms);
  struc2->get_primitive_asym_indices(indices, totatoms);
  real_g (*coords)[3] = new real_g[totatoms][3];
  for (int_g i = 0; i < totatoms; i++) {
    for (int_g j = 0; j < 3; j++) 
      coords[i][j] = (real_g)tmpcoords[i][j];
  }
  DLV::operation *op2 = get_neb_op(end_model);
  set_labels2(op2);  
  get_labels2()->set_grid(coords, totatoms, true); 
  int_g *inverse_mapping = new int_g[natoms];
  for (int_g i = 0; i < natoms; i++) {  
    for (int_g j = 0; j < natoms; j++){ 
      if (mapping[j]==i){
	inverse_mapping[i] = j;
	break;
      }
    }
  }
  int_g *data = new int_g[totatoms];
  for (int_g i = 0; i < totatoms; i++)
    data[i] = inverse_mapping[indices[i]] + 1;
  get_labels2()->add_data(data, "labels2", true);
  op2->attach_data(get_labels2());
#ifdef ENABLE_DLV_GRAPHICS
  op2->render_data(get_labels2());
  get_labels2()-> set_3D_display(struc2);
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::neb_calc_v8_ini1::view_mapping_str2(const int_g natoms, 
						  const int_g end_model,
						  const int_g mapping[])
{
  //dummy 
}

void CRYSTAL::neb_calc_v8_ini2::view_mapping_str2(const int_g natoms, 
						  const int_g end_model,
						  const int_g mapping[])
{
  //dummy 
}

void CRYSTAL::neb_calc::hide_atoms(const int_g end_model, const real_g dist, 
				   const int_g mapping[])
{
  DLV::model *struc1 = DLV::operation::get_editing()->get_model();
  DLV::model *struc2 = DLV::operation::get_editing()
    ->get_neb_op(end_model)->get_model();
  int_g natoms = struc1->get_number_of_asym_atoms(); 
  DLV::coord_type (*coords1)[3] 
    = new_local_array2(DLV::coord_type, natoms, 3);
  DLV::coord_type (*coords2)[3] 
    = new_local_array2(DLV::coord_type, natoms, 3);    
  struc1->get_asym_atom_cart_coords(coords1, natoms);
  struc2->get_asym_atom_cart_coords(coords2, natoms);
  real_g mydist;
  for (int_g i = 0; i < natoms; i++){
    mydist = 0.0;
    for(int_g j = 0; j < 3; j++) 
      mydist += (coords1[i][j]-coords2[mapping[i]][j])
	* (coords1[i][j]-coords2[mapping[i]][j]);
    mydist = sqrt(mydist);
#ifdef ENABLE_DLV_GRAPHICS
    DLV::atom_flag_type flag;
    if(mydist > dist) flag = DLV::flag_unset;
    else flag = DLV::flag_done;
    struc1->set_atom_flag(i, flag);
    struc2->set_atom_flag(mapping[i], flag);
#endif // ENABLE_DLV_GRAPHICS
  }
#ifdef ENABLE_DLV_GRAPHICS
  struc1->activate_atom_flags("neb flag1", false);
  struc2->activate_atom_flags("neb flag2", false);
#endif // ENABLE_DLV_GRAPHICS
#ifdef ENABLE_DLV_GRAPHICS
  const int_g mlen = 256;
  char message[mlen];
  DLV::operation *op = DLV::operation::get_editing();
  DLV::render_parent *rp1 = op->get_display_obj();
  DLV::render_parent *rp2 = op->get_neb_op(end_model)
    ->get_display_obj();
  struc1->update_atoms(rp1, message,  mlen);
  struc2->update_atoms(rp2, message,  mlen);
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::neb_calc::unhide_atoms(const int_g end_model)
{
#ifdef ENABLE_DLV_GRAPHICS
  DLV::model *struc1 = DLV::operation::get_editing()->get_model();
  DLV::model *struc2 = DLV::operation::get_editing()
    ->get_neb_op(end_model)->get_model();
  struc1->deactivate_atom_flags();
  struc2->deactivate_atom_flags();
#endif // ENABLE_DLV_GRAPHICS
}

void CRYSTAL::neb_calc::label_image(DLV::operation *op)
{
  DLV::model *struc = op->get_model();
  int_g totatoms = struc->get_number_of_primitive_atoms();
  DLV::coord_type (*tmpcoords)[3] 
    = new_local_array2(DLV::coord_type, totatoms, 3);    
  struc->get_primitive_atom_cart_coords(tmpcoords, totatoms);
  int_g *indices = new int_g[totatoms];
  struc->get_primitive_asym_indices(indices, totatoms);
  DLV::atom_integers *labels1 = new DLV::atom_integers("CRYSTAL NEB",
						       "000", op,
						       "image Atom labels");
  real_g (*coords)[3] = new real_g[totatoms][3];
  for (int_g i = 0; i < totatoms; i++) {
    for (int_g j = 0; j < 3; j++) 
      coords[i][j] = (real_g)tmpcoords[i][j];
  }
  labels1->set_grid(coords, totatoms, true); 
  int_g *data = new int_g[totatoms];
  for (int_g i = 0; i < totatoms; i++)
    data[i] = indices[i] + 1;
  labels1->add_data(data, "labels1", true);
  op->attach_data(labels1);
#ifdef ENABLE_DLV_GRAPHICS
  op->render_data(labels1);
  labels1-> set_3D_display(struc);
#endif // ENABLE_DLV_GRAPHICS
}

bool CRYSTAL::neb_calc::edit_atoms(const int_g end_model, int_g mapping[], 
				   char message[], const int_g mlen,
				   const int_g dummy)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  return op1->edit_atoms(end_model, mapping, message, mlen);
}

bool CRYSTAL::neb_calc_v8::edit_atoms(const int_g end_model, int_g mapping[],
				      char message[], const int_g mlen)
{
#ifdef ENABLE_DLV_GRAPHICS
  DLV::model *struc1 = get_model();
  DLV::model *struc2 = get_neb_op(end_model)->get_model();
  int_g natoms = struc1->get_number_of_asym_atoms();
  if((struc1->get_number_selected_atoms()!=1)||
     (struc2->get_number_selected_atoms()!=1)) {
    strncpy(message, "ERROR - Only select one atom for each structure", mlen);
    return false;
  }
  int_g loc1, loc2;
  DLV::atom a;
  loc1 = struc1->find_primitive_selection(a);
  loc2 = struc2->find_primitive_selection(a);
  int_g *atom_types1 = new_local_array1(int_g, natoms);
  int_g *atom_types2 = new_local_array1(int_g, natoms);
  struc1->get_asym_crystal03_ids(atom_types1, natoms);
  struc2->get_asym_crystal03_ids(atom_types2, natoms);
  if(atom_types1[loc1] != atom_types2[loc2]){
    strncpy(message, "ERROR - Selected atoms must be of the same type", mlen);
    return false;
  }
  int_g *inverse_mapping = new int_g[natoms];
  for (int_g i = 0; i < natoms; i++) {  
    for (int_g j = 0; j < natoms; j++){ 
      if (mapping[j]==i){
	inverse_mapping[i] = j;
	break;
      }
    }
  }
  int_g tmp = mapping[loc1]; 
  mapping[loc1] = mapping[inverse_mapping[loc2]];
  mapping[inverse_mapping[loc2]] =tmp;
  get_labels2()->swop_two_data_items(loc1+1, inverse_mapping[loc2]+1, 0,
				     struc2);
  return true;
#else
  return false;
#endif // ENABLE_DLV_GRAPHICS
}

DLV::atom_integers *CRYSTAL::neb_calc_v8::get_labels2()
{
  return labels2;
}

void CRYSTAL::neb_calc_v8::set_labels2(DLV::operation *op2)
{
  labels2 = new DLV::atom_integers("CRYSTAL NEB", "1", op2,"End Structure");
}

void 
CRYSTAL::neb_calc_v8_final::set_params(const Hamiltonian &h, const Basis &b,
				       const Kpoints &k, const Tolerances &tol,
				       const Convergence &c, const Print &p,
				       const Joboptions &j, const Optimise &opt,
				       const Phonon &ph, const Neb &n, 
				       const CPHF &, const TDDFT &,
				       const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  neb_data::set_params(n);   
}

void 
CRYSTAL::neb_calc_v8_ini1::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n, 
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  neb_data::set_params(n);   
}

void 
CRYSTAL::neb_calc_v8_ini2::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n, 
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  neb_data::set_params(n);   
}

bool CRYSTAL::neb_calc_v8_final::add_basis_set(const char filename[],
					       const bool set_default,
					       int_g * &indices, int_g &value,
					       char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::neb_calc_v8_ini1::add_basis_set(const char filename[],
					      const bool set_default,
					      int_g * &indices, int_g &value,
					      char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::neb_calc_v8_ini2::add_basis_set(const char filename[],
					      const bool set_default,
					      int_g * &indices, int_g &value,
					      char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

DLV::string CRYSTAL::neb_data::optimiser_log() const
{
  return "LOG";
}

void CRYSTAL::neb_calc_v8::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::neb_calc_v8_final::add_restart_file(const DLV::string tag,
						  const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::neb_calc_v8_ini1::add_restart_file(const DLV::string tag,
						 const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::neb_calc_v8_ini2::add_restart_file(const DLV::string tag,
						 const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::neb_calc_v8_final::add_optimiser_log(const DLV::string tag,
						   const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

void CRYSTAL::neb_calc_v8_ini1::add_optimiser_log(const DLV::string tag,
						  const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

void CRYSTAL::neb_calc_v8_ini2::add_optimiser_log(const DLV::string tag,
						  const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

bool CRYSTAL::neb_calc_v8_final::create_files(const bool is_parallel,
					      const bool is_local, 
					      char message[],
					      const int_g mlen)
{
  static char tag[] = "neb";
  if (common_files(is_parallel, is_local, tag)) {
    DLV::string extension, filename;
    std::stringstream my_integer;
    add_input_file(2, "neb2", "str", "fort.35", is_local, false);
    add_optimiser_log(tag, is_local);
    for(int_g i=0;i<get_nimages();i++){
      my_integer.str("");
      my_integer << i+1;
      extension = "im" + my_integer.str() + "str";
      filename = "image" + my_integer.str() + ".str";
      add_output_file(i+2, tag, extension, filename, is_local, true);
    }
    if(!get_nocalc())
      add_output_file(get_nimages()+2, tag, "restart", "fort.91", is_local, true);
  } else
    return false;
  return true;
}

bool CRYSTAL::neb_calc_v8_ini1::create_files(const bool is_parallel,
					     const bool is_local, 
					     char message[],
					     const int_g mlen)
{
  static char tag[] = "neb";
  if (common_files(is_parallel, is_local, tag)) {
    DLV::string extension, filename;
    std::stringstream integer;
    add_input_file(2, "neb2", "str", "fort.35", is_local, false);
    add_optimiser_log(tag, is_local);
    for(int_g i=0;i<get_nimages();i++){
      integer.str("");
      integer << i+1;
      extension = "im" + integer.str() + "str";
      filename = "image" + integer.str() + ".str";
      add_output_file(i+2, tag, extension, filename, is_local, true);
    }
  } else
    return false;
  return true;
}

bool CRYSTAL::neb_calc_v8_ini2::create_files(const bool is_parallel,
					     const bool is_local, 
					     char message[],
					     const int_g mlen)
{
  static char tag[] = "neb";
  if (common_files(is_parallel, is_local, tag)) {
    DLV::string extension, filename;
    std::stringstream integer;
    add_input_file(2, "neb2", "str", "fort.35", is_local, false);
    add_optimiser_log(tag, is_local);
    for(int_g i=0;i<get_nimages();i++){
      integer.str("");
      integer << i+1;
      extension = "im" + integer.str() + "str";
      filename = "image" + integer.str() + ".str";
      add_output_file(i+2, tag, extension, filename, is_local, true);
    }
    add_output_file(get_nimages()+2, tag, "restart", "fort.91", is_local, true);
  } else
    return false;
  return true;
}

bool CRYSTAL::neb_calc_v8_final::write(const DLV::string filename,
				       const DLV::model *const structure,
				       char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::neb_calc_v8_ini1::write(const DLV::string filename,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::neb_calc_v8_ini2::write(const DLV::string filename,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

void 
CRYSTAL::neb_calc_v8_final::write_structure(const DLV::string filename,
					    const DLV::model *const structure,
					    char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void 
CRYSTAL::neb_calc_v8_ini1::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void 
CRYSTAL::neb_calc_v8_ini2::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::neb_calc_v8_final::write_2nd_structure(char message[],
						     const int_g mlen)
{
  int_g natoms = neb_data::get_natoms();
  int_g *mapping = new int_g[natoms];
  neb_data::get_mapping(mapping);
  DLV::string filename = get_infile_name(2);
  DLV::model *structure = neb_data::get_nebop2()->get_model();
  structure->update_atom_list();
  DLV::model *mycopy = structure->duplicate_model("");
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] 
    = new_local_array2(DLV::coord_type, natoms,3);
  structure->get_asym_atom_types(atom_types, natoms);
  structure->get_asym_atom_cart_coords(coords, natoms);
  for (int_g i=0;i<natoms;i++){
    mycopy->set_indexed_asym_cart_coords(coords[mapping[i]],i);
    mycopy->set_indexed_asym_atom_type(atom_types[mapping[i]],i);
  }
  mycopy->update_atom_list();
  //    scf_data::setup_atom_bases(mycopy);
  structure_file::write(filename, mycopy, get_c03_indices(), message, mlen);
  delete mycopy;
}

void CRYSTAL::neb_calc_v8_ini1::write_2nd_structure(char message[], 
						    const int_g mlen)
{
  int_g natoms = neb_data::get_natoms();
  int_g *mapping = new int_g[natoms];
  neb_data::get_mapping(mapping);
  DLV::string filename = get_infile_name(2);
  DLV::model *structure = neb_data::get_nebop2()->get_model();
  structure->update_atom_list();
  DLV::model *mycopy = structure->duplicate_model("");
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] 
    = new_local_array2(DLV::coord_type, natoms,3);
  structure->get_asym_atom_types(atom_types, natoms);
  structure->get_asym_atom_cart_coords(coords, natoms);
  for (int_g i=0;i<natoms;i++){
    mycopy->set_indexed_asym_cart_coords(coords[mapping[i]],i);
    mycopy->set_indexed_asym_atom_type(atom_types[mapping[i]],i);
  }
  mycopy->update_atom_list();
  //    scf_data::setup_atom_bases(mycopy);
  structure_file::write(filename, mycopy, get_c03_indices(), message, mlen);
  delete mycopy;
}

void 
CRYSTAL::neb_calc_v8_ini2::write_2nd_structure(char message[], const int_g mlen)
{
  int_g natoms = neb_data::get_natoms();
  int_g *mapping = new int_g[natoms];
  neb_data::get_mapping(mapping);


  DLV::string filename = get_infile_name(2);
  DLV::model *structure = neb_data::get_nebop2()->get_model();
  structure->update_atom_list();
  DLV::model *mycopy = structure->duplicate_model("");
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] 
    = new_local_array2(DLV::coord_type, natoms,3);
  structure->get_asym_atom_types(atom_types, natoms);
  structure->get_asym_atom_cart_coords(coords, natoms);
  for (int_g i=0;i<natoms;i++){
    mycopy->set_indexed_asym_cart_coords(coords[mapping[i]],i);
    mycopy->set_indexed_asym_atom_type(atom_types[mapping[i]],i);
  }
  mycopy->update_atom_list();
  //    scf_data::setup_atom_bases(mycopy);
  structure_file::write(filename, mycopy, get_c03_indices(), message, mlen);
  delete mycopy;
}

void CRYSTAL::neb_calc_v8_final::add_opt_restart_file(const DLV::string tag,
						      const bool is_local)
{
  if (neb_data::get_restart())
    add_data_file(3, neb_data::get_restart_file(), "fort.91", is_local, true);
}

void CRYSTAL::neb_calc_v8_ini1::add_opt_restart_file(const DLV::string tag,
						     const bool is_local)
{
  // To_do need to implement
  if (neb_data::get_restart())
    add_data_file(3, neb_data::get_restart_file(), "fort.91", is_local, true);
}

void CRYSTAL::neb_calc_v8_ini2::add_opt_restart_file(const DLV::string tag,
						     const bool is_local)
{
  DLV::string filename_rd, filename_wr;
  std::stringstream integer;
  int_g nimages = neb_data::get_nimages(); 
  char *message = new char[256];
  for (int_g i=0;i<nimages;i++){ 
    integer.str("");
    integer << i+1;

    if(image_updated(i)){
      filename_rd = neb_data::get_images_dir() + tag 
	+ ".im" + integer.str() + "str";
      write_structure(filename_rd, neb_data::get_image_op(i)->get_model(),
		      message, 256);
    }
    DLV::string img_dir = neb_data::get_images_dir();
    if (img_dir[0] == '/')
      filename_rd = img_dir + tag + ".im" + integer.str() + "str";    
    else
      filename_rd = "../" + img_dir + tag + ".im" + integer.str() + "str";
    filename_wr = "image" + integer.str() + ".str";
    add_data_file(3+i, filename_rd, filename_wr, is_local, true);
  }
}

DLV::string CRYSTAL::neb_data_v8::restart_opt_filename() const
{
  return get_restart_file();
}

void CRYSTAL::neb_data_v8::write_extern_label(std::ofstream &output) const
{
  output << "DLVINPUT\n";
}

void CRYSTAL::neb_data_v8::write_geom_commands(std::ofstream &output,
					       const DLV::model *const) const
{
  write_neb(output);
}

void CRYSTAL::neb_data_v8::write_section5(std::ofstream &output) const
{
  output << "DLVINPUT\n";
  output << "END\n";
}

void CRYSTAL::neb_data_v8::write_neb(std::ofstream &output) const
{
  Neb data = get_data();
  output << "NEB\n";
  if (data.restart) output<<  "RESTART\n";
  if (data.adapt) output << "ADAPT\n";
  output << "NIMAGES\n";
  output << data.nimages << '\n';
  output << "ITERS\n";
  output << data.iters << '\n';
  output << "CLIMB\n";
  output << data.climb << '\n';
  if(data.start_ene  < 0.0) {
    output << "STARTENE\n";
    output << data.start_ene << '\n';
  }
  if(data.final_ene  < 0.0) {
    output << "FINALENE\n";
    output << data.final_ene << '\n';
  }
  if(data.separation==Neb::SPRING) output << "SPRINGMD\n1\n";
  else if (data.separation==Neb::STRING) output << "STRING\n";
  if(data.optimiser==Neb::LBFGS) output << "LBFGS\n";
  else if (data.optimiser==Neb::DMD) output << "DAMPMORE\n";
  if (data.convergence==Neb::LOW) output << "APPROX\n";
  else if (data.convergence==Neb::USER) {
    output << "ENERGTOL\n" << data.tol_energy << "\n";
    output << "FORCETOL\n" << data.tol_grad << "\n";
    output << "DISTOL\n" << data.tol_step << "\n";
  }
  if(data.nocalc) output << "NOCALC\n";
  if(data.symmpath) output << "SYMMPATH\n";
  if(data.usestrsym) output << "USESTRSYM\n";
  output << "WRITEIMAGESTR\n";  
  output << "GUESSFP\n";
  output << "END\n";
}

void CRYSTAL::neb_data_ini1::write_neb(std::ofstream &output) const
{
  Neb data = get_data();
  output << "NEB\n";
  output << "NIMAGES\n";
  output << data.nimages << '\n';
  if(data.symmpath) output << "SYMMPATH\n";
  if(data.usestrsym) output << "USESTRSYM\n";
  output << "WRITEIMAGESTR\n";  
  output << "NOCALC\n";  
  output << "END\n";
}

void CRYSTAL::neb_data_ini2::write_neb(std::ofstream &output) const
{
  Neb data = get_data();
  output << "NEB\n";
  output << "NIMAGES\n";
  output << data.nimages << '\n';
  if(data.symmpath) output << "SYMMPATH\n";
  if(data.usestrsym) output << "USESTRSYM\n";
  output << "READIMAGESTR\n";  
  output << "NOCALC\n";  
  output << "END\n";
}

bool CRYSTAL::neb_calc::recover(const bool no_err, const bool log_ok,
				char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Neb output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Neb(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Neb log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Neb(failed) log file");
    attach_data(data);
  }
  return ok;
}

bool CRYSTAL::neb_calc_v8_final::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  DLV::string label = "CRYSTAL NEB Transition State";
  if (no_err) {
    // Display Transition Structure
    int_g image = get_transition_image(message, len);
    DLV::model *m = structure_file::read(label, 
    					 get_outfile_name(image+1).c_str(),
    					 false, message, len);
    ok = (m != 0);
    if (ok) {
      attach_model(m);
      set_current();
#ifdef ENABLE_DLV_GRAPHICS
      get_current()->render_r(message, len);
#endif // ENABLE_DLV_GRAPHICS
      // need to update labels
      DLV::data_object *tmplabels1 = get_current()
	->find_data("Atom labels", "CRYSTAL NEB"); 
      DLV::atom_integers* labels1
	=dynamic_cast<DLV::atom_integers*> (tmplabels1);
      int_g totatoms = m->get_number_of_asym_atoms();
      DLV::coord_type (*tmpcoords)[3] 
	= new_local_array2(DLV::coord_type, totatoms, 3); 
      real_g (*coords)[3] = new real_g[totatoms][3];
      m->get_asym_atom_cart_coords(tmpcoords, totatoms);
      for(int_g i=0;i<totatoms;i++)
	for(int_g j=0;j<3;j++)
	  coords[i][j] = tmpcoords[i][j];
      labels1->set_grid(coords, totatoms, true);
      //dynamic cast current -then we can protect
      get_current()->attach_data(labels1);
#ifdef ENABLE_DLV_GRAPHICS
      get_current()->render_data(labels1);
      labels1->set_3D_display(m);  
      labels1->unset_3D_display(m); 	
#endif // ENABLE_DLV_GRAPHICS
    }
    DLV::data_object *data = 0;
    if (log_ok) {
      // logfile
      DLV::string name = get_log_filename();
      if (no_err)
	data = new DLV::text_file(name, program_name, id,
				  "Neb output file");
      else
	data = new DLV::text_file(name, program_name, id,
				  "Neb(failed) output file");
      attach_data(data);
    }
    if (no_err) {
      // internal data logging
      DLV::string name = get_outfile_name(1);
      if (no_err)
	data = new DLV::text_file(name, program_name, id, "Neb log file");
      else
	data = new DLV::text_file(name, program_name, id,
				  "Neb(failed) log file");
      attach_data(data);
    }
    //NEB plot
    DLV::string title, xlabel, ylabel;
    int_g npts = get_nimages() + 2;
    real_g *energies = new real_g[npts];
    real_g *dists = new real_g[npts];
    ok = get_NEB_plot_data(dists, energies, npts, title,
			   xlabel, ylabel, message, len);
    if(ok){
      int_g nplots = 1;
      DLV::dos_plot *pdata = new DLV::dos_plot("CRYSTAL", "NEB", this, nplots);
      pdata->set_title(title.c_str());
      pdata->set_grid(dists, npts);
      pdata->add_plot(energies, 0);
      pdata->set_xaxis_label(xlabel.c_str());
      pdata->set_yaxis_label(ylabel.c_str());
      attach_data(pdata);
      //      render_data(pdata);
    }
    else
      strncpy(message, "Failed to obtain pathway data", len);
    delete_local_array(energies);
    delete_local_array(dists);
    //NEB Trajectory
    int_g fpimage = get_frames_per_image();  
    int_g size = fpimage*(get_nimages()+1)+1;
    m = DLV::operation::get_current()->get_model();
    int_g natoms = m->get_number_of_asym_atoms();
    real_g (*p)[max_frames][3];
    p = new real_g[natoms][max_frames][3];
    DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,
						    natoms, 3);
    ok = get_NEB_trajectory(p, coords, image, natoms, message, len);
    if(ok) {
      real_g tstep = 1E-12;  //dummy
      DLV::md_trajectory *md_data = new DLV::md_trajectory("CRYSTAL", id, this,
							 size, tstep);
      md_data->set_data(&(p[0][0]), coords, natoms, max_frames);
      attach_data(md_data);
    }
    else
      strncpy(message, "Failed to obtain trajectory data", len);
    delete [] p;
    delete_local_array(coords);
    return ok; 
  }
  return false;
}

bool CRYSTAL::neb_calc_v8_ini1::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  return true;
}

bool CRYSTAL::neb_calc_v8_ini2::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  return true;
}

CRYSTAL::int_g
CRYSTAL::neb_calc_v8_final::get_transition_image(char message[],
						 const int_g len)
{
  int_g image = 0;
  int_g position;
  DLV::string name1 = get_log_filename();
  DLV::string text1 = 
    " **                 THE TRANSITION STATE GEOMETRY IS IMAGE";
  DLV::string text2 = 
    " **                  THE LEAST STABLE STRUCTURE IS IMAGE";
  std::ifstream log;
  char buff[256];
  if (DLV::open_file_read(log, name1.c_str(), buff, 256)) {
    log.getline(buff, 256);
    while (!log.eof()) {
      if (strncmp(buff, text1.c_str(), 58) == 0){
	position = 59;
	break;
      }
      if (strncmp(buff, text2.c_str(), 56) == 0){
	std::sscanf(&buff[57], "%i", &image);
	if(image <1 ) std::sscanf(&buff[58], "%i", &image);
      }
      log.getline(buff, 256);
    }
    if(position==59){
      std::sscanf(&buff[position], "%i", &image);
      if(image <1 ) std::sscanf(&buff[position+1], "%i", &image);
    }
    else{
      strncpy(message, "WARNING CALCULATION IS NOT FULLY CONVERGED", len);
    }
    log.close();
  }
  if(image==0) {
    strncpy(message,
	    "WARNING CALCULATION NOT FULLY CONVERGED - USING MIDDLE IMAGE",
	    len);
    image = (get_nimages()+ get_nimages()%2)/2;
  }
  return image;
}

bool
CRYSTAL::neb_calc_v8_final::get_NEB_trajectory(real_g p[][max_frames][3],
					       //5000=maxsize hack
					       DLV::coord_type coords_ts[][3],
					       const int_g ts_image,
					       const int_g natoms, 
					       char message[], const int_g mlen)
{
  bool ok = true;
  DLV::string label = "NEB Trajectory";
  int_g fpimage = get_frames_per_image();
  DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,
						  natoms, 3);
  DLV::coord_type (*fracs)[3] = new_local_array2(DLV::coord_type,
							natoms, 3);
  DLV::model *m1;
  m1 = structure_file::read(label, get_infile_name(1).c_str(),
			    false, message, mlen);
  if (m1->get_number_of_sym_ops() > 1) {
    strncpy(message, "Problem mapping NEB trajectory symmetry", mlen);
    return false;
  }
  m1->get_asym_atom_cart_coords(coords, natoms);
  m1->get_asym_atom_frac_coords(fracs, natoms);
  for (int_g j = 0; j < natoms; j++)  
    for (int_g k = 0; k < 3; k++)
      p[j][0][k] = coords[j][k];
  DLV::coord_type (*delta_coords)[3] = new_local_array2(DLV::coord_type,
							natoms, 3);
  DLV::coord_type (*prev_coords)[3] = new_local_array2(DLV::coord_type,
						       natoms, 3);
  DLV::coord_type (*prev_fracs)[3] = new_local_array2(DLV::coord_type,
						       natoms, 3);
  DLV::coord_type (*lattice)[3] = new_local_array2(DLV::coord_type,3,3);
  DLV::coord_type (*inverse)[3] = new_local_array2(DLV::coord_type,3,3);
  m1->get_conventional_lattice(lattice[0], lattice[1], lattice[2]);
  DLV::matrix_invert(lattice, inverse, 1);
  int_g dim = get_model_type();
  for (int_g i=0; i<get_nimages();i++){
    for (int_g j = 0; j < natoms; j++)
      for (int_g k = 0; k < 3; k++){
	prev_coords[j][k] = coords[j][k];
	prev_fracs[j][k] = fracs[j][k];
      }
    m1 = structure_file::read(label, get_outfile_name(i+2).c_str(),
			      false, message, mlen);
    m1->get_asym_atom_cart_coords(coords, natoms);
    m1->get_asym_atom_frac_coords(fracs, natoms);
    for (int_g j = 0; j < natoms; j++){
      // check for change of lattice
      for (int_g k = 0; k < dim; k++)
	if(fabs(fracs[j][k] - prev_fracs[j][k]) > 0.5){
	  for (int_g r = 0; r < dim; r++)
	    coords[j][r] = 0.0;
	  if(fracs[j][k] > prev_fracs[j][k])
	    fracs[j][k] -= 1.0;
	  else
	    fracs[j][k] += 1.0;
	  for (int_g r = 0; r < dim; r++)
	    for (int_g s = 0; s < dim; s++)
	      coords[j][r] += lattice[r][s] * fracs[j][s];
	}
      for (int_g k = 0; k < 3; k++){
   	delta_coords[j][k] = (coords[j][k] - prev_coords[j][k])/fpimage;
	for (int_g n=0; n<fpimage; n++)
	  p[j][fpimage*i+n+1][k] = prev_coords[j][k] 
	    + (n+1)*delta_coords[j][k];
      }
      if(i == ts_image-1) //another new bit
	for (int_g j = 0; j < natoms; j++)
	  for (int_g k = 0; k < 3; k++)
	    coords_ts[j][k] = coords[j][k]; 
    }
  }
  for (int_g j = 0; j < natoms; j++)
    for (int_g k = 0; k < 3; k++){
      prev_coords[j][k] = coords[j][k];
      prev_fracs[j][k] = fracs[j][k];
    }
  m1 = structure_file::read(label, get_infile_name(2).c_str(),
			    false, message, mlen);
  m1->get_asym_atom_cart_coords(coords, natoms);
  m1->get_asym_atom_frac_coords(fracs, natoms);
  for (int_g j = 0; j < natoms; j++){
    // check for change of lattice
    for (int_g k = 0; k < dim; k++)
      if(fabs(fracs[j][k] - prev_fracs[j][k]) > 0.5){
	for (int_g r = 0; r < dim; r++)
	  coords[j][r] = 0.0;
	if(fracs[j][k] > prev_fracs[j][k])
	  fracs[j][k] -= 1.0;
	else
	  fracs[j][k] += 1.0;
	for (int_g r = 0; r < dim; r++)
	  for (int_g s = 0; s < dim; s++)
	    coords[j][r] += lattice[r][s] * fracs[j][s];
      }
    for (int_g k = 0; k < 3; k++){
      delta_coords[j][k] = (coords[j][k] - prev_coords[j][k])/fpimage;
      for (int_g n=0; n<fpimage; n++)
	p[j][fpimage*get_nimages()+n+1][k] = prev_coords[j][k] 
	  + (n+1)*delta_coords[j][k];
    }
  }
  return ok;
}

bool CRYSTAL::neb_calc_v8_final::get_NEB_plot_data(real_g dists[],
						   real_g energies[], 
						   const int_g npts,
						   DLV::string title,
						   DLV::string xlabel,
						   DLV::string ylabel, 
						   char message[],
						   const int_g mlen)
{
  DLV::string label = "CRYSTAL NEB Transition State";
  int_g image;
  for (int_g i=0; i<npts; i++)
    energies[i] = 1.0;
  bool start_ene = false;
  bool final_ene = false;
  Neb neb = get_data();
  if(neb.start_ene  < 0.0) {
    energies[0] = neb.start_ene;
    start_ene = true;
  }
  if(neb.final_ene  < 0.0){
    energies[npts-1] = neb.final_ene;
    final_ene = true;
  }
  dists[0] = 0.0;
  DLV::string logfile = get_log_filename();
  DLV::string text1 = " **                       CALCULATION";
  DLV::string text2 = " **                       ENERGY IS";
  DLV::string text3 = " **                     Initial";
  std::ifstream log;
  char buff[256];
  if (DLV::open_file_read(log, logfile.c_str(), buff, 256)) {
    log.getline(buff, 256);
    while (!log.eof()) {
      if(!start_ene){
	if (strncmp(buff, " == SCF ENDED", 13) == 0){
	  std::sscanf(&buff[49], "%f", &energies[0]);
	  start_ene = true;
	}
      }	
      else if(!final_ene){
	if (strncmp(buff, " == SCF ENDED", 13) == 0){
	  std::sscanf(&buff[49], "%f", &energies[npts-1]);
	  final_ene = true;
	}
      }
      if (strncmp(buff, text1.c_str(), 37) == 0)
	std::sscanf(&buff[51], "%i", &image);
      else if (strncmp(buff, text2.c_str(), 35) == 0)
	std::sscanf(&buff[36], "%f", &energies[image]);
      else if (strncmp(buff, text3.c_str(), 31) == 0){
	std::sscanf(&buff[47], "%f", &dists[1]);
	for (int_g i=2; i<npts; i++){
	  log.getline(buff, 256);
	  std::sscanf(&buff[47], "%f", &dists[i]);
	}
      }
      log.getline(buff, 256);
    }
    log.close();
  }
  // make distances cumulative
  for (int_g i=1; i<npts; i++)
    dists[i] += dists[i-1];
  //convert energies to eV relative to starting image
  for (int_g i=0; i<npts; i++)
    if (energies[i] > 0.0){
      strncpy(message, "Error reading pathway energies", mlen - 1);
      return false;
    }
  for (int_g i=1; i<npts; i++)
    energies[i] = (energies[i] - energies[0])*DLV::Hartree_to_eV;
  energies[0] = 0.0;
  title = " ";
  xlabel = "Reaction Pathway";
  ylabel = "Energy (eV)";
  return true;
}



bool CRYSTAL::neb_calc::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload CRYSTAL CI-NEB not implemented", mlen);
  return false;
}


bool CRYSTAL::neb_calc_v8_final::reload_data(DLV::data_object *data,
					     char message[], const int_g mlen)
{
  bool ok = true;
  //NEB PLOT
  DLV::dos_plot *plot = dynamic_cast<DLV::dos_plot *>(data);
  DLV::md_trajectory *v = dynamic_cast<DLV::md_trajectory *>(data);
  if (v == 0 && plot ==0) {
    strncpy(message, "Incorrect data object to reload NEB data", mlen);
    ok = false;
  }
  //NEB PLOT
  if(plot !=0){
    DLV::string title, xlabel, ylabel;
    int_g npts= get_nimages() + 2;
    real_g *energies = new real_g[npts];
    real_g *dists = new real_g[npts];
    ok = get_NEB_plot_data(dists, energies, npts, title,
			   xlabel, ylabel, message, mlen);
    if(ok){
      plot->set_title(title.c_str());
      plot->set_grid(dists, npts);
      plot->add_plot(energies, 0);
      plot->set_xaxis_label(xlabel.c_str());
      plot->set_yaxis_label(ylabel.c_str());
      attach_data(plot);
    }
    else
      strncpy(message, "Failed to obtain pathway data", mlen);
    delete_local_array(energies);
    delete_local_array(dists);
  }
  //NEB trajectory
  if (v != 0) { 
    DLV::model *m = DLV::operation::get_current()->get_model();
    int_g natoms = m->get_number_of_asym_atoms();
    real_g (*p)[max_frames][3];
    p = new real_g[natoms][max_frames][3];
    int_g ts_image = get_transition_image(message, mlen);
    DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,
						    natoms, 3);
    ok = get_NEB_trajectory(p, coords, ts_image, natoms, message, mlen);
    if(ok) {
      v->set_data(&(p[0][0]), coords, natoms, max_frames);
      attach_data(v);
    }
    else
      strncpy(message, "Failed to obtain trajectory data", mlen);
    delete [] p;
    delete_local_array(coords);
  }
  return ok;
}

DLV::string CRYSTAL::neb_calc::get_output_file_name(int_g index)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op=dynamic_cast<neb_calc*> (base1);
  return op->get_outfile_name(index); 
}

DLV::operation *
CRYSTAL::neb_calc::create_run1(const bool a, const Hamiltonian &h,
			       const Basis &b, const Kpoints &k,
			       const Tolerances &tol, const Convergence &c,
			       const Print &p, const Joboptions &j,
			       const Optimise &opt, const Phonon &ph,
			       const Neb &n, const DLV::job_setup_data &job,
			       const bool extern_job,
			       const char extern_dir[], DLV::operation *nebop2,
			       char dir_name[], char message[], const nat_g len)

{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  return op1->create_ini1(a, h, b, k, tol, c, p, j, opt, ph, n, job, 
			  extern_job, extern_dir, nebop2, dir_name, 
			  message, len);
}

DLV::operation *
CRYSTAL::neb_calc::create_run2(const bool a, const Hamiltonian &h,
			       const Basis &b, const Kpoints &k,
			       const Tolerances &tol, const Convergence &c,
			       const Print &p, const Joboptions &j,
			       const Optimise &opt, const Phonon &ph, 
			       const Neb &n, const DLV::job_setup_data &job,
			       const bool extern_job, const char extern_dir[],
			       DLV::operation *nebop2, char dir_name[],
			       char message[], const nat_g len)
{
  DLV::operation *base1 = get_editing();
  neb_calc* op1=dynamic_cast<neb_calc*> (base1);
  return op1->create_ini2(a, h, b, k, tol, c, p, j, opt, ph, n, 
			  job, extern_job, extern_dir, nebop2, 
			  dir_name, message, len);
}



DLV::operation *
CRYSTAL::neb_calc_v8::create_ini1(const bool a, const Hamiltonian &h,
				  const Basis &b, const Kpoints &k,
				  const Tolerances &tol, 
				  const Convergence &c,
				  const Print &p, const Joboptions &j,
				  const Optimise &opt, const Phonon &ph,
				  const Neb &n, const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[],
				  DLV::operation *nebop2,
				  char dir_name[], char message[], 
				  const nat_g len)
{
  neb_calc_v8 *op = new neb_calc_v8_ini1;  
  DLV::model *str1 = get_editing()->get_model();
  if (op == 0) {
    strncpy(message, "BUG: object is not an SCF calculation", len);
    return op;
  }
  op->set_nebop2(nebop2);
  message[0] = '\0';
  bool use_mpp = false;
  CPHF cphf;
  TDDFT tddft(true, false, true, true, true, 0, 1, 100, 100, 1e-6);
  op->set_params(h, b, k, tol, c, p, j, opt, ph, n, cphf, tddft, a);
  if (op->create_files(job.is_parallel(), job.is_local(), message, len)) {
    if (op->make_directory(message, len)) {  
      if (op->write(op->get_infile_name(0), str1, 
		    message, len)){  
	op->write_structure(op->get_infile_name(1), str1,
			    message, len);
	op->write_2nd_structure(message, len);
      }
    }
  }
  DLV::string temp1 =  op->make_dir_name();
  const char *temp2;
  temp2 = temp1.c_str();
  for(nat_g i=0; i<temp1.length(); i++)
    dir_name[i] = temp2[i];
  dir_name[temp1.length()] = '\0';
  // This will trap warnings about a structure file with too many ops
  if (strlen(message) == 0) {
    bool foreground = true;
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(use_mpp), "", job, true,
		extern_job, extern_dir, message, len, foreground);
    // job was never attached so it can't be found by the panel
    op->remove_job(message, len);
  }
  return op;
}

DLV::operation *
CRYSTAL::neb_calc_v8::create_ini2(const bool a, const Hamiltonian &h,
				  const Basis &b, const Kpoints &k,
				  const Tolerances &tol, 
				  const Convergence &c,
				  const Print &p, const Joboptions &j,
				  const Optimise &opt, const Phonon &ph,
				  const Neb &n, const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[],
				  DLV::operation *nebop2,
				  char dir_name[],  char message[],
				  const nat_g len)
{
  neb_calc_v8 *op = new neb_calc_v8_ini2;  
  DLV::model *str1 = get_editing()->get_model();
  if (op == 0) {
    strncpy(message, "BUG: object is not an SCF calculation", len);
    return op;
  }
  op->set_nebop2(nebop2);
  message[0] = '\0';
  bool use_mpp = false;
  CPHF cphf;
  TDDFT tddft(true, false, true, true, true, 0, 1, 100, 100, 1e-6);
  op->set_params(h, b, k, tol, c, p, j, opt, ph, n, cphf, tddft, a);
  // set updated_images and images
  op->copy_images_from_master();
  if (op->create_files(job.is_parallel(), job.is_local(), message, len)) {
    if (op->make_directory(message, len)) {  
      if (op->write(op->get_infile_name(0), str1, 
		    message, len)){  
	op->write_structure(op->get_infile_name(1), str1,
			    message, len);
	op->write_2nd_structure(message, len);
      }
    }
  }
  DLV::string temp1 =  op->make_dir_name();
  const char *temp2;
  temp2 = temp1.c_str();
  for(nat_g i=0; i<temp1.length(); i++)
    dir_name[i] = temp2[i];
  dir_name[temp1.length()] = '\0';
  // This will trap warnings about a structure file with too many ops
  if (strlen(message) == 0) {
    bool foreground = true;
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(use_mpp), "", job, true,
		extern_job, extern_dir, message, len, foreground);
    // job was never attached so it can't be found by the panel
    op->remove_job(message, len);
  }
  return op;
}

void CRYSTAL::neb_calc_v8_ini1::copy_images_from_master()
{
  //dummy
}

void CRYSTAL::neb_calc_v8_ini2::copy_images_from_master()
{
  DLV::operation *base1 = CRYSTAL::neb_data::get_neb_editing();
  neb_calc_v8_final* op1=dynamic_cast<neb_calc_v8_final*> (base1);
  set_images_arrays(get_nimages());
  for (int_g i=0; i< get_nimages(); i++){
    if (op1->image_updated(i)){
      set_image_updated(i);
      set_images(i, op1->neb_data::get_image_op(i));
    }
  }
}

void CRYSTAL::neb_calc_v8_final::copy_images_from_master()
{
  //dummy
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void CRYSTAL::neb_data::serialize(Archive &ar, const unsigned int version)
{
  //      ar & max_frames;
}

template <class Archive>
void CRYSTAL::neb_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
  ar & boost::serialization::base_object<CRYSTAL::neb_data>(*this);
}

template <class Archive>
void CRYSTAL::neb_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_geom>(*this);
}

template <class Archive>
void CRYSTAL::neb_calc_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::neb_calc>(*this);
}

template <class Archive>
void CRYSTAL::neb_calc_v8_final::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::neb_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::neb_data_v8>(*this);
}

template <class Archive>
void CRYSTAL::neb_calc_v8_ini1::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::neb_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::neb_data_ini1>(*this);
}

template <class Archive>
void CRYSTAL::neb_calc_v8_ini2::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::neb_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::neb_data_ini2>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::neb_calc_v8_final)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_data_v8)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_geom)

DLV_NORMAL_EXPLICIT(CRYSTAL::neb_calc_v8_final)

#endif // DLV_USES_SERIALIZE
