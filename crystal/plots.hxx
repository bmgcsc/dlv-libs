
#ifndef CRYSTAL_PLOT_FILES
#define CRYSTAL_PLOT_FILES

namespace CRYSTAL {

  // Only used by v4 calcs?
  class plot25 {
  protected:
    bool read_plot(std::ifstream &input, DLV::plot_data *data,
		   const DLV::int_g flag, const DLV::int_g npoints,
		   char message[], const DLV::int_g mlen);
  };

}

#endif // CRYSTAL_PLOT_FILES
