
#ifndef CRYSTAL_POTENTIAL_SLICE
#define CRYSTAL_POTENTIAL_SLICE

namespace CRYSTAL {

  class potential_data : public grid2D_v4 {
  public:
    // serialization
    virtual ~potential_data();

  protected:
    potential_data(const int_g np, DLV::plane *s, const int_g l,
		   const int_g tol);

    void write_input(std::ofstream &output);
    virtual void write_grid(std::ofstream &output) = 0;

    int_g get_npoints() const;
    const DLV::plane *get_vertices() const;

  private:
    int_g npoints;
    DLV::plane *grid;
    int_g multipoles;
    int_g tolerance;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_data_v4 : public property_data, public potential_data {
  protected:
    potential_data_v4(const int_g np, DLV::plane *s, const int_g l,
		      const int_g tol, const Density_Matrix &dm,
		      const NewK &nk);

    void write_grid(std::ofstream &output);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_data_v6 : public property_data_v6, public potential_data {
  protected:
    potential_data_v6(const int_g np, DLV::plane *s, const int_g l,
		      const int_g tol, const Density_Matrix &dm,
		      const NewK &nk);

    void write_grid(std::ofstream &output);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // No difference between versions at present.
  class load_potential_slice : public DLV::load_data_op, grid2D_v4 {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_potential_slice(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_calc : public property_calc {
  public:
    static operation *create(const int_g np, const int_g selection,
			     const int_g l, const int_g tol,
			     const Density_Matrix &dm, const NewK &nk,
			     const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    potential_calc();

    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_calc_v4 : public potential_calc, public potential_data_v4 {
  public:
    potential_calc_v4(const int_g np, DLV::plane *s, const int_g l,
		      const int_g tol, const Density_Matrix &dm,
		      const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_calc_v6 : public potential_calc, public potential_data_v6 {
  public:
    potential_calc_v6(const int_g np, DLV::plane *s, const int_g l,
		      const int_g tol, const Density_Matrix &dm,
		      const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_potential_slice)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::potential_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::potential_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::potential_data::potential_data(const int_g np,
					       DLV::plane *s,
					       const int_g l, const int_g tol)

  : npoints(np), grid(s), multipoles(l), tolerance(tol)
{
}

inline CRYSTAL::int_g CRYSTAL::potential_data::get_npoints() const
{
  return npoints;
}

inline const DLV::plane *CRYSTAL::potential_data::get_vertices() const
{
  return grid;
}

inline CRYSTAL::potential_data_v4::potential_data_v4(const int_g np,
						     DLV::plane *s,
						     const int_g l,
						     const int_g tol,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : property_data(dm, nk), potential_data(np, s, l, tol)
{
}

inline CRYSTAL::potential_data_v6::potential_data_v6(const int_g np,
						     DLV::plane *s,
						     const int_g l,
						     const int_g tol,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : property_data_v6(dm, nk), potential_data(np, s, l, tol)
{
}

inline CRYSTAL::load_potential_slice::load_potential_slice(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::potential_calc::potential_calc()
{
}

inline bool CRYSTAL::potential_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::potential_calc_v4::potential_calc_v4(const int_g np,
						     DLV::plane *s,
						     const int_g l,
						     const int_g tol,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : potential_data_v4(np, s, l, tol, dm, nk)
{
}

inline CRYSTAL::potential_calc_v6::potential_calc_v6(const int_g np,
						     DLV::plane *s,
						     const int_g l,
						     const int_g tol,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : potential_data_v6(np, s, l, tol, dm, nk)
{
}

#endif // CRYSTAL_POTENTIAL_SLICE
