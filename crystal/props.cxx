
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "calcs.hxx"
#include "props.hxx"

namespace CRYSTAL {

  DLV::string state_label = "Insulator";

}

DLV::string CRYSTAL::property_calc::serial_binary = "properties";
DLV::string CRYSTAL::property_calc::parallel_binary = "Pproperties";

CRYSTAL::property_data::~property_data()
{
  // Todo?
}

void CRYSTAL::property_data::output_newk(std::ofstream &output,
					 const DLV::operation *op,
					 const DLV::model *const structure,
					 const bool newk_reqd,
					 const bool fermi_reqd, const bool v6)
{
  if (op->find_data(fermi_label, program_name) == 0)
    read_fermi = fermi_reqd;
  else
    read_fermi = false;
  if (newk.calc or newk_reqd or fermi_reqd) {
    output << "NEWK\n";
    if (structure->get_number_of_periodic_dims() > 0) {
      DLV::string buffer = "0 ";
      if (v6)
	buffer = " ";
      if (newk.calc and newk.new_shrink) {
	if (newk.asym_shrink) {
	  output << "0 " << buffer << newk.isp << '\n';
	  output << newk.is1 << ' ' << newk.is2 << ' ' << newk.is3 << '\n';
	} else
	  output << newk.is1 << buffer << newk.isp << '\n';
      } else
	output << "0 " << buffer << "0\n";
    }
    if (fermi_reqd or newk.calc_fermi)
      output << 1;
    else
      output << 0;
    output << " 0\n";
  }
}

void CRYSTAL::property_data::write_newk_data(std::ofstream &output,
					     const DLV::operation *op,
					     const DLV::model *const structure,
					     const bool newk_reqd,
					     const bool fermi_reqd)
{
  output_newk(output, op, structure, newk_reqd, fermi_reqd, false);
}

void CRYSTAL::property_data_v6::write_newk_data(std::ofstream &output,
						const DLV::operation *op,
					    const DLV::model *const structure,
						const bool newk_reqd,
						const bool fermi_reqd)
{
  output_newk(output, op, structure, newk_reqd, fermi_reqd, true);
}

void CRYSTAL::property_data::write_dm_data(std::ofstream &output) const
{
  switch (density.projection) {
  case Density_Matrix::no_DM_projection:
    break;
  case Density_Matrix::DM_atoms:
    output << "PATO\n";
    output << 0 << ' ' << 0 << '\n';
    break;
  case Density_Matrix::DM_bands:
    output << "PBAN\n";
    output << (density.band_max - density.band_min + 1) << ' ' << 0 << '\n';
    for (int_g i = density.band_min; i <= density.band_max; i++)
      output << ' ' << i;
    output << '\n';
    break;
  case Density_Matrix::DM_energy:
    output << "PDIDE\n";
    output.precision(5);
    output << (density.emin / DLV::Hartree_to_eV);
    output << ' ' << (density.emax / DLV::Hartree_to_eV) << '\n';
    break;
  }
}

void CRYSTAL::property_data::write_header(std::ofstream &output,
					  const DLV::operation *op,
					  const DLV::model *const structure,
					  const bool is_binary,
					  const bool newk_reqd,
					  const bool fermi_reqd,
					  const bool use_density)
{
  if (!is_binary)
    output << "RDFMWF\n";
  if (use_density and (density.projection == Density_Matrix::DM_bands or
		       density.projection == Density_Matrix::DM_energy))
    write_newk_data(output, op, structure, true, true);
  else
    write_newk_data(output, op, structure, newk_reqd, fermi_reqd);
  if (use_density)
    write_dm_data(output);
}

bool CRYSTAL::property_calc::find_state_type(bool &state, char message[],
					     const int_g mlen) const
{
  const DLV::data_object *data = find_data(state_label, program_name);
  if (data == 0)
    return false;
  const DLV::boolean_data *s = dynamic_cast<const DLV::boolean_data *>(data);
  if (s == 0) {
    //strncpy(message, "BUG in CRYSTAL fermi energy data", mlen);
    return false;
  }
  state = s->get_value();
  return true;
}

bool CRYSTAL::property_calc::find_fermi_energy(real_l &ef, char message[],
					       const int_g mlen) const
{
  const DLV::data_object *data = find_data(fermi_label, program_name);
  if (data == 0)
    return false;
  const DLV::real_data *fermi = dynamic_cast<const DLV::real_data *>(data);
  if (fermi == 0) {
    //strncpy(message, "BUG in CRYSTAL fermi energy data", mlen);
    return false;
  }
  ef = fermi->get_value();
  return true;
}

bool CRYSTAL::property_calc::find_invalid_fermi(real_l &ef, char message[],
						const int_g mlen) const
{
  const DLV::data_object *data = find_data(invalid_fermi_label, program_name);
  if (data == 0)
    return false;
  const DLV::real_data *fermi = dynamic_cast<const DLV::real_data *>(data);
  if (fermi == 0) {
    //strncpy(message, "BUG in CRYSTAL fermi energy data", mlen);
    return false;
  }
  ef = fermi->get_value();
  return true;
}

void CRYSTAL::property_calc::read_logfile(const DLV::string filename,
					  const DLV::string id)
{
  std::ifstream log;
  char buff[256];
  if (DLV::open_file_read(log, filename.c_str(), buff, 256)) {
    // Find the newk command.
    log.getline(buff, 256);
    while (!log.eof()) {
      if (strncmp(buff, " RESTART WITH NEW K POINTS NET", 30) == 0)
	break;
      else if (strncmp(buff,
		       " FERMI ENERGY AND DENSITY MATRIX CALCULA", 39) == 0)
	break;
      log.getline(buff, 256);
    }
    do {
      log.getline(buff, 256);
    } while (strncmp(buff, " INSULATING STATE", 17) != 0 &&
	     strncmp(buff, " CONDUCTING STATE", 17) != 0 &&
	     strncmp(buff, " POSSIBLY CONDUCTING", 20) != 0);
    real_l ef;
    DLV::boolean_data *state = 0;
    if (strncmp(buff, " INSULATING", 11) == 0) {
      if (strlen(buff) < 45) {
	ef = -1e10;
	// CRYSTAL09, get next line
	do {
	  real_l temp;
	  log.getline(buff, 256);
	  //  TOP OF VALENCE BANDS -      BAND     11; K    1; EIG  1.575
	  if (strncmp(buff, " TOP", 4) == 0) {
	    std::sscanf(&buff[51], "%lf", &temp);
	    if (temp > ef)
	      ef = temp;
	  }
	} while (strncmp(buff, " CORE DENSITY", 13) != 0);
      } else
	std::sscanf(&buff[48], "%lf", &ef);
      state = new DLV::boolean_data(program_name, id,
				    state_label.c_str(), true);
    } else if (strncmp(buff, " CONDUCTING", 11) == 0) {
      std::sscanf(&buff[34], "%lf", &ef);
      state = new DLV::boolean_data(program_name, id,
				    state_label.c_str(), false);
      //Todo wavefn->set_conducting();
    } else {
      std::sscanf(&buff[39], "%lf", &ef);
      state = new DLV::boolean_data(program_name, id,
				    state_label.c_str(), false);
    }
    if (state != 0)
      attach_data(state);
    DLV::real_data *obj = new DLV::real_data(program_name, id,
					     fermi_label.c_str(),
					     ef * DLV::Hartree_to_eV);
    attach_data(obj);
    log.close();
  }
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void CRYSTAL::Density_Matrix::serialize(Archive &ar, const unsigned int version)
{
  ar & projection;
  ar & emin;
  ar & emax;
  ar & band_min;
  ar & band_max;
}

template <class Archive>
void CRYSTAL::NewK::serialize(Archive &ar, const unsigned int version)
{
  ar & is1;
  ar & is2;
  ar & is3;
  ar & isp;
  ar & calc;
  ar & new_shrink;
  ar & asym_shrink;
  ar & calc_fermi;
}

template <class Archive>
void CRYSTAL::property_data::serialize(Archive &ar, const unsigned int version)
{
  ar & density;
  ar & newk;
  ar & read_fermi;
}

template <class Archive>
void CRYSTAL::property_data_v6::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::property_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::property_calc)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::calculate)

DLV_NORMAL_EXPLICIT(CRYSTAL::Density_Matrix)
DLV_NORMAL_EXPLICIT(CRYSTAL::NewK)
DLV_NORMAL_EXPLICIT(CRYSTAL::property_data)
DLV_EXPORT_EXPLICIT(CRYSTAL::property_data)
DLV_NORMAL_EXPLICIT(CRYSTAL::property_data_v6)
DLV_EXPORT_EXPLICIT(CRYSTAL::property_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::property_calc)
DLV_EXPORT_EXPLICIT(CRYSTAL::property_calc)

#endif // DLV_USES_SERIALIZE
