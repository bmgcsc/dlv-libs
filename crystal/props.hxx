
#ifndef CRYSTAL_PROPERTIES
#define CRYSTAL_PROPERTIES

namespace CRYSTAL {

  class Density_Matrix {
  public:
    enum projection_type {
      no_DM_projection = 0, DM_atoms, DM_bands, DM_energy
    } projection;
    real_g emin;
    real_g emax;
    int_g band_min;
    int_g band_max;

    Density_Matrix(const int_g p, const real_g em, const real_g ex,
		   const int_g bm, const int_g bx);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class NewK {
  public:
    int_g is1;
    int_g is2;
    int_g is3;
    int_g isp;
    bool calc;
    bool new_shrink;
    bool asym_shrink;
    bool calc_fermi;

    NewK(const int_g s1, const int_g s2, const int_g s3, const int_g sp,
	 const bool cn, const bool ns, const bool as, const bool cf);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class property_data {
  public:
    // for serialization
    virtual ~property_data();

  protected:
    property_data(const Density_Matrix &dm, const NewK &nk);

    void set_params(const Density_Matrix &dm, const NewK &nk);
    void write_header(std::ofstream &output, const DLV::operation *op,
		      const DLV::model *const structure, const bool is_binary,
		      const bool newk_reqd = false,
		      const bool fermi_reqd = false,
		      const bool use_density = true);
    void output_newk(std::ofstream &output, const DLV::operation *op,
		     const DLV::model *const structure, const bool newk_reqd,
		     const bool fermi_reqd, const bool v6);
    virtual void write_newk_data(std::ofstream &output,
				 const DLV::operation *op,
				 const DLV::model *const structure,
				 const bool newk_reqd, const bool fermi_reqd);
    bool recover_fermi() const;

  private:
    Density_Matrix density;
    NewK newk;
    bool read_fermi;

    void write_dm_data(std::ofstream &output) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class property_data_v6 : public property_data {
  protected:
    property_data_v6(const Density_Matrix &dm, const NewK &nk);
    void write_newk_data(std::ofstream &output, const DLV::operation *op,
			 const DLV::model *const structure,
			 const bool newk_reqd, const bool fermi_reqd);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class property_calc : public calculate {
  protected:
    DLV::string get_serial_executable() const;
    DLV::string get_parallel_executable() const;

    bool has_fermi_energy() const;
    bool find_state_type(bool &state, char message[], const int_g mlen) const;
    bool find_fermi_energy(real_l &ef, char message[], const int_g mlen) const;
    bool find_invalid_fermi(real_l &ef, char message[], const int_g mlen) const;
    void read_logfile(const DLV::string filename, const DLV::string id);

  private:
    static DLV::string serial_binary;
    static DLV::string parallel_binary;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::property_data)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::property_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::property_calc)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::Density_Matrix::Density_Matrix(const int_g p, const real_g em,
					       const real_g ex, const int_g bm,
					       const int_g bx)
  : emin(em), emax(ex), band_min(bm), band_max(bx)
{
  // Todo - a better way?
  switch (p) {
  case 1:
    projection = DM_atoms;
    break;
  case 2:
    projection = DM_bands;
    break;
  case 3 :
    projection = DM_energy;
    break;
  default:
    projection = no_DM_projection;
    break;
  }
}

inline CRYSTAL::NewK::NewK(const int_g s1, const int_g s2, const int_g s3,
			   const int_g sp, const bool cn, const bool ns,
			   const bool as, const bool cf)
  : is1(s1), is2(s2), is3(s3), isp(sp), calc(cn), new_shrink(ns),
    asym_shrink(as), calc_fermi(cf)
{
}

inline CRYSTAL::property_data::property_data(const Density_Matrix &dm,
					     const NewK &nk)
  : density(dm), newk(nk), read_fermi(false)
{
}

inline CRYSTAL::property_data_v6::property_data_v6(const Density_Matrix &dm,
						   const NewK &nk)
  : property_data(dm, nk)
{
}

inline bool CRYSTAL::property_data::recover_fermi() const
{
  return read_fermi;
}

inline void CRYSTAL::property_data::set_params(const Density_Matrix &dm,
					       const NewK &nk)
{
  density = dm;
  newk = nk;
}

inline DLV::string CRYSTAL::property_calc::get_serial_executable() const
{
  return serial_binary;
}

inline DLV::string CRYSTAL::property_calc::get_parallel_executable() const
{
  return parallel_binary;
}

inline bool CRYSTAL::property_calc::has_fermi_energy() const
{
  real_l e;
  char buff[64];
  return find_fermi_energy(e, buff, 64);
}

#endif // CRYSTAL_PROPERTIES
