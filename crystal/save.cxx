
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "base.hxx"
#include "model.hxx"
#include "save.hxx"

DLV::operation *CRYSTAL::save_structure::create(const char filename[],
						char message[],
						const int_g mlen)
{
  save_structure *op = new save_structure(filename);
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::save operation", mlen);
  else {
    message[0] = '\0';
    DLVreturn_type ok = op->write(filename, op->get_current_model(), 0,
				  message, mlen);
    if (ok == DLV_ERROR) {
      delete op;
      op = 0;
    } else
      op->attach();
  }
  return op;
}

DLV::string CRYSTAL::save_structure::get_name() const
{
  return ("Save CRYSTAL structure - " + get_filename());
}

DLV::operation *CRYSTAL::save_geometry::create(const char filename[],
					       char message[], const int_g mlen)
{
  save_geometry *op = new save_geometry(filename);
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::save operation", mlen);
  else {
    message[0] = '\0';
    op->write(filename, op->get_current_model(), message, mlen);
    if (strlen(message) > 0) {
      delete op;
      op = 0;
    } else
      op->attach();
  }
  return op;
}

DLV::string CRYSTAL::save_geometry::get_name() const
{
  return ("Save CRYSTAL geometry - " + get_filename());
}

void CRYSTAL::geometry_data::write(const char filename[],
				   const DLV::model *const structure,
				   char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename, message, mlen)) {
    bool ok = true;
    int_g dim = structure->get_number_of_periodic_dims();
    DLV::coord_type a;
    DLV::coord_type b;
    DLV::coord_type c;
    DLV::coord_type alpha;
    DLV::coord_type beta;
    DLV::coord_type gamma;
    if (dim > 0)
      structure->get_lattice_parameters(a, b, c, alpha, beta, gamma);
    DLV::string symbol;
    int_g group = structure->get_hermann_mauguin_group(symbol);
    int_g origin = 0;
    convert_group_symbol(group, symbol, origin);
    int_g lattice = 1;
    switch (dim) {
    case 0:
      output << "MOLECULE\n";
      if (group == 0 or origin != 0) {
	strncpy(message, "Unable to identify point group for CRYSTAL", mlen);
	ok = false;
      } else {
	output.width(5);
	output << group << '\n';
      }
      break;
    case 1:
      output << "POLYMER\n";
      if (group == 0 or origin != 0) {
	strncpy(message, "Unable to identify polymer group for CRYSTAL", mlen);
	ok = false;
      } else {
	output.width(5);
	output << group << '\n';
	output.precision(8);
	output.width(18);
	output << a << '\n';
      }
      break;
    case 2:
      output << "SLAB\n";
      if (group == 0 or origin != 0) {
	strncpy(message, "Unable to identify slab group for CRYSTAL", mlen);
	ok = false;
      } else {
	output.width(5);
	output << group << '\n';
	lattice = structure->get_lattice_type();
	output.precision(8);
	output.width(18);
	output << a;
	if (lattice < 2) {
	  output.precision(8);
	  output.width(18);
	  output << b;
	  if (lattice < 1) {
	    output.precision(8);
	    output.width(18);
	    output << alpha;
	  }
	}
	output << '\n';
      }
      break;
    case 3:
      output << "CRYSTAL\n";
      if (group == 0) {
	output.width(5);
	output << 1;
	output.width(5);
	output << 0;
	output.width(5);
	output << origin << '\n';
	output << symbol << '\n';
      } else {
	output.width(5);
	output << 0;
	output.width(5);
	output << 0;
	output.width(5);
	output << origin << '\n';
	output << group << '\n';
      }
      lattice = structure->get_lattice_type();
      output.precision(8);
      output.width(18);
      output << a;
      if (lattice < 6) {
	if (lattice > 2) {
	  output.precision(8);
	  output.width(18);
	  output << c;
	} else {
	  output.precision(8);
	  output.width(18);
	  output << b;
	  output.precision(8);
	  output.width(18);
	  output << c;
	  if (lattice < 2) {
	    if (lattice == 1) {
	      output.precision(8);
	      output.width(18);
	      output << beta;
	    } else {
	      output.precision(8);
	      output.width(18);
	      output << alpha;
	      output.precision(8);
	      output.width(18);
	      output << beta;
	      output.precision(8);
	      output.width(18);
	      output << gamma;
	    }
	  }
	}
      }
      output << '\n';
      break;
    }
    if (ok) {
      int_g natoms = structure->get_number_of_asym_atoms();
      int_g *atom_types = new_local_array1(int_g, natoms);
      DLV::coord_type (*coords)[3] =
	new_local_array2(DLV::coord_type, natoms, 3);
      structure->get_asym_atom_types(atom_types, natoms);
      structure->get_asym_atom_frac_coords(coords, natoms);
      output.width(5);
      output << natoms << '\n';
      for (int_g i = 0; i < natoms; i++) {
	// We need to write indices that relate to the basis set for this
	// atomic number. - Todo
	/*if (n == natoms)
	  j = indices[i];
	  else
	  j = (int) structure->atom_props.prop[i].atomic_number; */
	int_g j = atom_types[i];
	output.width(5);
	output << j;
	output.precision(8);
	output.width(18);
	output << coords[i][0];
	output.precision(8);
	output.width(18);
	output << coords[i][1];
	output.precision(8);
	output.width(18);
	output << coords[i][2];
	output << '\n';
      }
      delete_local_array(coords);
      delete_local_array(atom_types);
    }
    output.close();
  }
}

void CRYSTAL::geometry_data::convert_group_symbol(int_g &gp, DLV::string &name,
						  int_g &origin)
{
  // Todo
  gp = 1;
  name = "";
  origin = 0;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::save_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::save_structure("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::save_geometry *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::save_geometry("recover");
    }

  }
}

template <class Archive>
void CRYSTAL::save_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::save_model_op>(*this);
}

template <class Archive>
void CRYSTAL::save_geometry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::save_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::save_structure)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::save_geometry)

DLV_SUPPRESS_TEMPLATES(DLV::save_model_op)

DLV_NORMAL_EXPLICIT(CRYSTAL::save_structure)
DLV_NORMAL_EXPLICIT(CRYSTAL::save_geometry)

#endif // DLV_USES_SERIALIZE
