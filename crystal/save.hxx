
#ifndef CRYSTAL_SAVE_STRUCTURE
#define CRYSTAL_SAVE_STRUCTURE

namespace CRYSTAL {

  // Internal support types - I don't think this needs to be in calcs.hxx
  // since I don't think it will be used anywhere else, but its a
  // separate type just in case.
  class geometry_data {
  protected:
    void write(const char filename[], const DLV::model *const structure,
	       char message[], const int_g mlen);

  private:
    void convert_group_symbol(int_g &gp, DLV::string &name, int_g &origin);
  };

  class save_structure : public DLV::save_model_op, public structure_file {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);

    // public for serialization
    save_structure(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class save_geometry : public DLV::save_model_op, public geometry_data {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);

    // public for serialization
    save_geometry(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::save_structure)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::save_geometry)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::save_structure::save_structure(const char file[])
  : save_model_op(file)
{
}

inline CRYSTAL::save_geometry::save_geometry(const char file[])
  : save_model_op(file)
{
}

#endif // CRYSTAL_SAVE_STRUCTURE
