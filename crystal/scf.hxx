
#ifndef CRYSTAL_SCF_CALCS
#define CRYSTAL_SCF_CALCS

namespace CRYSTAL {

  // C88 = V1, C92 = V2, C95 = V3, C98 = V4, C03 = V5, C06 = V6, C09 = V7,
  // C14 = V8, C17 = V9, dev = V10
  // Helper classes for SCF data
  class Hamiltonian {
  public:
    Hamiltonian();
    Hamiltonian(const int_g htype, const int_g hoption, const bool use_spin,
		const int_g func, const int_g corr, const int_g ex,
		const int_g aux, const bool hybrid, const int_g mix,
		const int_g num);

    enum hamiltonian_type { Hartree_Fock, DFT } hamiltonian;
    enum option_type { restricted, ropen_shell, unrestricted } options;
    bool spin;
    enum dft_func_type { corr_exch, b3lyp, b3pw, pbe0, pbesol0, b1wc,
			 wc1lyp, b97h, pbe0_third, svwn, pbexc, pbesolxc,
			 soggaxc, hse06, hsesol, hiss, rshxlda, wb97, wb97x,
			 lcwblyp, lcwpbe, lcwpbesol, lcblyp, camb3lyp, scblyp,
			 m06l, m05, m06, b2plyp, b2gpplyp, d3_blyp, d3_pbe,
			 d3_b3lyp, d3_pbe0, d3_hse06, d3_hsesol, d3_m06
    } functional;
    enum dft_correlation_type {
      no_correlation, pz_corr, pwlsd_corr, vwn_corr, vbh_corr, p86_corr,
      pwgga_corr, lyp_corr, pbe_corr, pbesol_corr, wl_corr
    } correlation;
    enum dft_exchange_type {
      hf_ex, lda_ex, vbh_ex, becke_ex, pwgga_ex, pbe_ex,
      wcgga_ex, pbesol_ex, sogga_ex, mpw91_ex
    } exchange;
    enum aux_basis_type {
      aux_user, aux_coarse, aux_medium, aux_fine, aux_extra_fine
    } aux_basis;
    bool hybrid_function;
    int_g hybrid_mixing;
    bool numerical_dft;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Basis {
  public:
    Basis();
    Basis(const int_g def);

    enum def_gaussian_type { STO3G, G321, G621, POB_DZVP, POB_DZVPP,
			     POB_TZVP, STO6G, G631ss, DEF2_TZVP } default_basis;
    bool use_default;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Kpoints {
  public:
    Kpoints();
    Kpoints(const int_g sh1, const int_g sh2, const int_g sh3,
	    const int_g gilat, const bool nonsym);

    int_g is1;
    int_g is2;
    int_g is3;
    int_g isp;
    bool asym;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Tolerances {
  public:
    Tolerances();
    Tolerances(const int_g t1, const int_g t2, const int_g t3, const int_g t4,
	       const int_g t5, const int_g e, const int_g eig, const int_g dp,
	       const int_g method);

    int_g itol1;
    int_g itol2;
    int_g itol3;
    int_g itol4;
    int_g itol5;
    int_g energy;
    int_g eigen;
    int_g density;
    enum converge_type {
      c_energy_and_eig, c_energy, c_density_matrix
    } converge_method;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Convergence {
  public:
    Convergence();
    Convergence(const int_g mthd, const int_g lmix, const int_g mcycles,
		const bool lshift, const int_g shift, const bool lock,
		const bool smear, const real_g ftemp, const bool spcalc,
		const bool slock, const int_g sp, const int_g spcycles);

    enum mixing_type { linear_mixing, anderson } method;
    int_g mixing;
    int_g maxcycles;
    int_g levshift;
    bool use_levshift;
    bool lock_levshift;
    bool use_smearing;
    real_g fermi_smear;
    bool spinlock;
    int_g spin;
    int_g spin_cycles;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Print {
  public:
    Print();
    Print(const bool ppan, const bool exch);

    bool mulliken;
    bool exchange;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Joboptions {
  public:
    Joboptions();
    Joboptions(const bool bied, const bool monod, const bool sd,
	       const int_g nbuffs, const bool dp, const int_g tol,
	       const int_g bies, const int_g mons, const bool grads,
	       const int_g restart, const char file[]);

    bool direct;
    bool monodirect;
    bool semidirect; // Todo - not used
    bool deltap;
    bool fock_restart;
    bool density_restart;
    int_g mem_buffs;
    int_g deltap_tol;
    int_g biesplit;
    int_g monsplit;
    bool calc_gradients;
    DLV::string restart_file;
    DLV::operation *restart_from_job; // Todo - not used? or serialise

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Optimise {
  public:
    Optimise();
    Optimise(const int_g opt, const int_g otype, const int_g e,
	     const real_g x, const real_g g, const real_g s,
	     const int_g dlf_t, const int_g dlf_s, const int_g dlf_i,
	     const int_g dlf_p, const real_g dlf_r, const real_g dlf_m,
	     const real_g dlf_c, const int_g dlf_cy, const int_g dlf_ns,
	     const real_g dlf_mu, const real_g dlf_d,
	     const real_g dlf_sc, const int_g dlf_re);
    int_g etol;
    real_g xtol;
    real_g gtol;
    real_g iscale;
    enum optimise_what_type {atom_opt, cell_opt, atom_and_cell_opt} opt_type;
    enum optimiser_type {berny, dlfind, domin} optimiser;
    enum dlf_opt_type {stoch, ga} dlf_type;
    enum dlf_stoch_type {uniform, force_dir_bias, force_bias} dlf_sstype;
    int_g dlf_initpop;
    int_g dlf_pop;
    real_g dlf_radius;
    real_g dlf_minradius;
    real_g dlf_contractradius;
    int_g dlf_cycles;
    int_g dlf_nsaves;
    real_g dlf_mutation;
    real_g dlf_death;
    real_g dlf_scalef;
    int_g dlf_reset;
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Phonon {
  public:
    Phonon();
    Phonon(const real_g s, const int_g d, const bool i, const bool p,
	   int_g (*c)[3]);

    real_g step;
    int_g deriv;
    bool intensity;
    bool dispersion;
    int_g cell[3][3];

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Neb {
  public:
    Neb();
    Neb(const int_g i, const int_g it, const int_g c, const real_g se,
	const real_g fe, const int_g st, const int_g ot, const int_g co,
	const real_g tole, const real_g tols, const real_g tolg, const bool sp,
	const bool us, const bool a, const bool r, const char filer[],
	const char idir[], const int_g em, int_g *map, const int_g n,
	const bool nc);
    void set_mapping(int_g maparray[], int_g natoms);
    int_g nimages, iters, climb;
    real_g start_ene, final_ene;
    enum sep_type {SPRING, STRING} separation;
    enum opt_type {LBFGS, DMD} optimiser;
    enum conv_type {HIGH, LOW, USER} convergence;
    real_g tol_energy;
    real_g tol_step;
    real_g tol_grad;
    bool symmpath;
    bool usestrsym;
    bool adapt;
    bool restart;
    DLV::string restart_file;
    DLV::string images_dir;
    int_g end_model;
    int_g *mapping;
    int_g natoms;
    bool nocalc;


  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class CPHF {
  public:
    CPHF();
    CPHF(const int_g mix, const int_g mcycles, const int_g tola,
	 const int_g tolu);

    int_g mixing;
    int_g maxcycles;
    int_g tolalpha;
    int_g toludik;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class basis_info {
  public:
    basis_info(const DLV::string file, const bool p);

    DLV::string name;
    //Todo class DLVbasis_set *basis;
    //int atomic_number;
    //int basis_index;
    //int atn_index;
    bool pseudo;
    int_g index;
    //bool new_config;
    //bool def_basis;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class TDDFT {
  public:
    TDDFT(const bool scf, const bool td, const bool jdos, const bool pes,
	  const bool fr, const int dm, const int dexc, const int dniter,
	  const int dspace, const float dconv);

    bool scf_calc;
    bool tamm_dancoff;
    bool calc_jdos;
    bool calc_pes;
    bool non_direct;
    int diag_method;
    int diag_excitation;
    int diag_niterations;
    int diag_space;
    float diag_convergence;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Base classes for containing and writing input files
  class scf_data : public structure_file {
  public:
    // serialization
    virtual ~scf_data();

  protected:
    scf_data();

    bool add_basis(const char filename[], const bool set_default,
		   const DLV::model *const m, int_g * &indices, int_g &value,
		   char message[], const int_g mlen);
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const bool a);
    bool has_restart() const;
    DLV::string restart_filename() const;
    virtual DLV::string restart_opt_filename() const;
    virtual bool need_gradients() const;
    virtual bool is_optimise() const;

    bool is_level_shifted() const;
    bool write_input(const DLV::string filename,
		     const DLV::model *const structure,
		     char message[], const int_g mlen);
    virtual void write_extern_label(std::ofstream &output) const = 0;
    virtual void write_geom_commands(std::ofstream &output,
				     const DLV::model *const structure) const;
    bool write_basis(std::ofstream &output, const DLV::model *const structure,
		     char message[], const int_g mlen) const;
    virtual void write_controls(std::ofstream &output) const;
    void write_v5_controls(std::ofstream &output) const;
    void write_dft_functional(std::ofstream &output) const;
    virtual void write_correlation_fn(std::ofstream &output) const;
    void write_correlation_fn_v7(std::ofstream &output) const;
    virtual void write_exchange_fn(std::ofstream &output) const;
    void write_exchange_v6(std::ofstream &output) const;
    void write_exchange_v7(std::ofstream &output) const;
    void write_exchange_v8(std::ofstream &output) const;
    void write_dft_basis(std::ofstream &output) const;
    void write_dft_grid(std::ofstream &output) const;
    virtual void write_dft(std::ofstream &output) const = 0;
    void write_v5_dft(std::ofstream &output) const;
    virtual void write_shrink(std::ofstream &output) const;
    void write_v6_shrink(std::ofstream &output) const;
    virtual void write_conv_type(std::ofstream &output) const;
    void write_conv_v4(std::ofstream &output) const;
    void write_conv_v5(std::ofstream &output) const;
    virtual void write_no_shift(std::ofstream &output) const;
    virtual void write_scf(std::ofstream &output,
			   const DLV::model *const m) const;
    void write_v5_scf(std::ofstream &output, const DLV::model *const m) const;
    virtual void write_section5(std::ofstream &output) const;

    int_g *get_c03_indices() const;

  private:
    Hamiltonian hamiltonian;
    Basis basis;
    Kpoints kpoints;
    Tolerances tolerances;
    Convergence converge;
    Print print;
    Joboptions options;
    std::list<basis_info> atom_bases;
    int_g *basis_indices;
    int_g *c03_indices;
    int_g natoms;
    DLV::string wavefunction;
    bool analyse;
    bool setup_atom_bases(const DLV::model *const structure);
    static void write_alle_basis(std::ofstream &output, const DLV::string name,
				 const int_g index, const int_g atn,
				 const int_g charge, char message[],
				 const int_g mlen);
    static void write_pseudo_basis(std::ofstream &output,
				   const DLV::string name, const int_g index,
				   const int_g atn, const int_g charge,
				   bool &use_charge, char message[],
				   const int_g mlen);
    static bool write_file_basis(std::ofstream &output, const DLV::string name,
				 const int_g atn, const int_g charge,
				 int_g &offset, bool &use_charge,
				 int_g &pseudo, char message[], const int_g mlen);
    static void write_pseudo_basis(std::ifstream &file, std::ofstream &output,
				   const char text[], const int_g nshells,
				   const int_g atn, const int_g charge,
				   bool &use_charge);
    static void write_sto3g(std::ofstream &output, const int_g index,
			    const int_g atn, const int_g charge);
    static void write_n21g(std::ofstream &output, const int_g index,
			   const int_g atn, const int_g charge, const int_g ng);
    static void write_s_electrons(const int_g s1, const int_g s2,
				  const int_g s3, const int_g s4,
				  const int_g s5, const int_g s6,
				  const int_g s7, int_g &n,
				  std::ofstream &output);
    static void write_sp_electrons(const int_g sp2, const int_g sp3,
				   const int_g sp4, const int_g sp5,
				   const int_g sp6, int_g &n,
				   std::ofstream &output);
    static void write_p_electrons(const int_g p2, const int_g p3,
				  const int_g p4, const int_g p5,
				  const int_g p6, int_g &n,
				  std::ofstream &output);
    static void write_d_electrons(const int_g d3, const int_g d4,
				  const int_g d5, const int_g d6, int_g &n,
				  std::ofstream &output);
    static void write_f_electrons(const int_g f4, const int_g f5,
				  int_g &n, std::ofstream &output);
    static void write_alle_config(std::ofstream &output,
				  const DLV::string name, const int_g atn,
				  const int_g charge, char message[],
				  const int_g mlen);
    static void write_pseudo_config(std::ofstream &output,
				    const DLV::string name,
				    const int_g atn, const int_g charge,
				    char message[], const int_g mlen);
    static void write_sto3g_config(std::ofstream &output, const int_g atn,
				   const int_g charge);
    static void write_n21g_config(std::ofstream &output, const int_g atn,
				  const int_g charge);
    static void get_configuration(const int_g atn, const int_g charge,
				  int_g &s1, int_g &s2, int_g &s3, int_g &s4,
				  int_g &s5, int_g &s6, int_g &s7, int_g &p2,
				  int_g &p3, int_g &p4, int_g &p5, int_g &p6,
				  int_g &d3, int_g &d4, int_g &d5, int_g &d6,
				  int_g &f4, int_g &f5);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_data_v4 : public scf_data {
  protected:
    void write_extern_label(std::ofstream &output) const;
    void write_controls(std::ofstream &output) const;
    void write_dft(std::ofstream &output) const;
    void write_conv_type(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_data_v5 : public scf_data {
  protected:
    void write_extern_label(std::ofstream &output) const;
    void write_dft(std::ofstream &output) const;
    void write_controls(std::ofstream &output) const;
    void write_scf(std::ofstream &output, const DLV::model *const m) const;
    void write_conv_type(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - derive from v5 to avoid duplicate routines with scf_data_v5?
  class scf_data_v6 : public scf_data {
  protected:
    void write_extern_label(std::ofstream &output) const;
    void write_dft(std::ofstream &output) const;
    void write_controls(std::ofstream &output) const;
    void write_shrink(std::ofstream &output) const;
    void write_scf(std::ofstream &output, const DLV::model *const m) const;
    void write_exchange_fn(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - derive from v5 to avoid duplicate routines with scf_data_v5?
  class scf_data_v7 : public scf_data_v6 {
  protected:
    void write_correlation_fn(std::ofstream &output) const;
    void write_exchange_fn(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_data_v8 : public scf_data_v7 {
  protected:
    void write_correlation_fn(std::ofstream &output) const;
    void write_exchange_fn(std::ofstream &output) const;
    void write_no_shift(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // The real DLV operations
  class scf_calc : public calculate {
  public:
    static operation *create(const int_g t, const int_g version,
			     char message[], const int_g mlen);
    static bool copy_basis(char message[], const int_g mlen);
    static bool add_basis(const char filename[], const bool set_default,
			  char message[], const int_g mlen);
    static bool calculate(const bool a, const Hamiltonian &h,
			  const Basis &b, const Kpoints &k,
			  const Tolerances &tol, const Convergence &c,
			  const Print &p, const Joboptions &j,
			  const Optimise &opt, const Phonon &ph, const Neb &n,
			  const CPHF &cphf, const TDDFT &tddft,
			  const int_g version, const DLV::job_setup_data &job,
			  const bool extern_job, const char extern_dir[],
			  const bool mpp, char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    scf_calc();
    DLV::string get_name() const;
    virtual void set_params(const Hamiltonian &h, const Basis &b,
			    const Kpoints &k, const Tolerances &tol,
			    const Convergence &c, const Print &p,
			    const Joboptions &j, const Optimise &opt,
			    const Phonon &ph, const Neb &n, const CPHF &cp,
			    const TDDFT &tddft, const bool a) = 0;
    virtual bool create_files(const bool is_parallel, const bool is_local,
			      char message[], const int_g mlen);
    bool common_files(const bool is_parallel, const bool is_local,
		      const char tag[]);
    virtual void add_restart_file(const DLV::string tag,
				  const bool is_local) = 0;
    virtual void add_opt_restart_file(const DLV::string tag,
				  const bool is_local) = 0;
    virtual bool add_basis_set(const char filename[], const bool set_default,
			       int_g * &indices, int_g &value,
			       char message[], const int_g mlen) = 0;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    virtual bool write(const DLV::string filename,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    virtual void write_structure(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen) = 0;
    virtual void write_2nd_structure(char message[], const int_g mlen);

    DLV::string get_serial_executable() const;
    DLV::string get_parallel_executable(const bool mpp = false) const;

  private:
    static DLV::string serial_binary;
    static DLV::string parallel_binary;
    static DLV::string mpp_binary;
    bool show_basis;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_geom : public scf_calc {
  public:
    bool is_geometry() const;

  protected:
    void inherit_model();
    void inherit_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
//BOOST_CLASS_EXPORT_KEY(CRYSTAL::Phonon)
//BOOST_CLASS_EXPORT_KEY(CRYSTAL::TDDFT)
//BOOST_CLASS_EXPORT_KEY(CRYSTAL::Neb)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_geom)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::Kpoints::Kpoints()
  : is1(8), is2(8), is3(8), isp(16), asym(false)
{
}

inline CRYSTAL::Kpoints::Kpoints(const int_g sh1, const int_g sh2,
				 const int_g sh3, const int_g gilat,
				 const bool nonsym)
  : is1(sh1), is2(sh2), is3(sh3), isp(gilat), asym(nonsym)
{
}

inline CRYSTAL::Print::Print()
  : mulliken(false), exchange(false)
{
}

inline CRYSTAL::Print::Print(const bool ppan, const bool exch)
  : mulliken(ppan), exchange(exch)
{
}

inline bool CRYSTAL::scf_data::has_restart() const
{
  return (options.fock_restart or options.density_restart);
}

inline DLV::string CRYSTAL::scf_data::restart_filename() const
{
  return options.restart_file;
}
inline DLV::string CRYSTAL::scf_data::restart_opt_filename() const
{
  return "";
}

inline bool CRYSTAL::scf_data::is_level_shifted() const
{
  return converge.use_levshift;
}

inline CRYSTAL::Optimise::Optimise()
  : etol(7), xtol(0.0012f), gtol(0.1f), iscale(10.0f), opt_type(atom_opt),
    optimiser(berny), dlf_type(stoch), dlf_sstype(uniform), dlf_initpop(25),
      dlf_pop(25), dlf_radius(1.0), dlf_minradius(0.000001),
      dlf_contractradius(0.9), dlf_cycles(100), dlf_nsaves(1),
    dlf_mutation(0.9), dlf_death(0.9), dlf_scalef(1.0), dlf_reset(500)
{
}

inline CRYSTAL::Optimise::Optimise(const int_g opt, const int_g otype,
				   const int_g e, const real_g x,
				   const real_g g, const real_g s,
				   const int_g dlf_t, const int_g dlf_s,
				   const int_g dlf_i, const int_g dlf_p,
				   const real_g dlf_r, const real_g dlf_m,
				   const real_g dlf_c, const int_g dlf_cy,
				   const int_g dlf_ns, const real_g dlf_mu,
				   const real_g dlf_d, const real_g dlf_sc,
				   const int_g dlf_re)
    : etol(e), xtol(x), gtol(g), iscale(s), opt_type(atom_opt),
      optimiser(berny), dlf_type(stoch), dlf_sstype(uniform),
      dlf_initpop(dlf_i), dlf_pop(dlf_p), dlf_radius(dlf_r),
      dlf_minradius(dlf_m), dlf_contractradius(dlf_c), dlf_cycles(dlf_cy),
      dlf_nsaves(dlf_ns), dlf_mutation(dlf_mu), dlf_death(dlf_d),
      dlf_scalef(dlf_sc), dlf_reset(dlf_re)
{
  switch (opt) {
  case 1:
    optimiser = dlfind;
    switch(dlf_t) {
	case 0:
	    dlf_type = stoch;
	    switch(dlf_s) {
		case 0:
		    dlf_sstype = uniform;
		    break;
		case 1:
		    dlf_sstype = force_dir_bias;
		    break;
		case 2:
		    dlf_sstype = force_bias;
		    break;
	    }
	    break;
	case 1:
	    dlf_type = ga;
	    break;
    }
    break;
  case 2:
    optimiser = domin;
    gtol = g;
    iscale = s;
    break;
  default:
    optimiser = berny;
    switch (otype) {
    case 0:
      opt_type = atom_opt;
      break;
    case 1:
      opt_type = cell_opt;
      break;
    case 2:
      opt_type = atom_and_cell_opt;
      break;
    }
    break;
  }
}

inline CRYSTAL::Phonon::Phonon()
  : step(0.005f), deriv(2), intensity(false), dispersion(false)
{
  cell[0][0] = 1;
  cell[0][1] = 0;
  cell[0][2] = 0;
  cell[1][0] = 0;
  cell[1][1] = 1;
  cell[1][2] = 0;
  cell[2][0] = 0;
  cell[2][1] = 0;
  cell[2][2] = 1;
}

inline CRYSTAL::Phonon::Phonon(const real_g s, const int_g d, const bool i,
			       const bool p, int_g (*c)[3])
  : step(s), deriv(d), intensity(i), dispersion(p)
{
  if (c == 0) {
    cell[0][0] = 1;
    cell[0][1] = 0;
    cell[0][2] = 0;
    cell[1][0] = 0;
    cell[1][1] = 1;
    cell[1][2] = 0;
    cell[2][0] = 0;
    cell[2][1] = 0;
    cell[2][2] = 1;
  } else {
    cell[0][0] = c[0][0];
    cell[0][1] = c[0][1];
    cell[0][2] = c[0][2];
    cell[1][0] = c[1][0];
    cell[1][1] = c[1][1];
    cell[1][2] = c[1][2];
    cell[2][0] = c[2][0];
    cell[2][1] = c[2][1];
    cell[2][2] = c[2][2];
  }
}

inline CRYSTAL::Neb::Neb()
    : nimages(3), iters(100), climb(10), start_ene(0.0), final_ene(0.0),
      separation(SPRING), optimiser(LBFGS), convergence(HIGH),
      tol_energy(0.0001), tol_step(0.0018), tol_grad(0.00045), symmpath(0),
      usestrsym(0), adapt(0), restart(0), end_model(-1), nocalc(0)
{
}

inline CRYSTAL::Neb::Neb(const int_g i, const int_g it, const int_g c,
			 const real_g se, const real_g fe, const int_g st,
			 const int_g ot, const int_g co, const real_g tole,
			 const real_g tols, const real_g tolg, const bool sp,
			 const bool us, const bool a, const bool r,
			 const char filer[], const char idir[], const int_g em,
			 int_g map[], const int_g n, const bool nc)
    : nimages(i), iters(it), climb(c), start_ene(se), final_ene(fe),
      separation(SPRING), optimiser(LBFGS), convergence(HIGH), tol_energy(tole),
      tol_step(tols), tol_grad(tolg), symmpath(sp), usestrsym(us),
      adapt(a), restart(r), images_dir(idir), end_model(em), natoms(n),
      nocalc(nc)
{
    mapping = new int_g[natoms];
    for(int_g i=0;i<natoms;i++) mapping[i] = map[i];
    switch (st) {
	case 0:
	    separation = SPRING;
	    break;
	case 1:
	    separation = STRING;
	    break;
    }
    switch (ot) {
	case 0:
	    optimiser = LBFGS;
	    break;
	case 1:
	    optimiser = DMD;
	    break;
    }
    switch (co) {
	case 0:
	    convergence = HIGH;
	    break;
	case 1:
	    convergence = LOW;
	    break;
	case 2:
	    convergence = USER;
	    break;
    }
    if (restart) restart_file = filer;
}

inline void CRYSTAL::Neb::set_mapping(int_g maparray[], const int_g natoms)
{
    mapping = new int_g[natoms];
    for(int_g i=0;i<natoms;i++) mapping[i] = maparray[i];
    return;
}

inline CRYSTAL::CPHF::CPHF()
  : mixing(0), maxcycles(100), tolalpha(4), toludik(6)
{
}

inline CRYSTAL::CPHF::CPHF(const int_g mix, const int_g mcycles,
			   const int_g tola, const int_g tolu)
  : mixing(mix), maxcycles(mcycles), tolalpha(tola), toludik(tolu)
{
}

inline CRYSTAL::basis_info::basis_info(const DLV::string file, const bool p)
  : name(file), pseudo(p), index(-1)
{
}

inline CRYSTAL::TDDFT::TDDFT(const bool scf, const bool td, const bool jdos,
			     const bool pes, const bool fr, const int dm,
			     const int dexc, const int dniter, const int dspace,
			     const float dconv)
  : scf_calc(scf), tamm_dancoff(td), calc_jdos(jdos), calc_pes(pes),
    non_direct(fr), diag_method(dm), diag_excitation(dexc),
    diag_niterations(dniter), diag_space(dspace), diag_convergence(dconv)
{
}

inline CRYSTAL::scf_data::scf_data()
  : basis_indices(0), c03_indices(0), natoms(0), analyse(false)
{
}

inline CRYSTAL::int_g *CRYSTAL::scf_data::get_c03_indices() const
{
  return c03_indices;
}

inline CRYSTAL::scf_calc::scf_calc() : show_basis(false)
{
}

inline DLV::string CRYSTAL::scf_calc::get_serial_executable() const
{
  return serial_binary;
}

inline
DLV::string CRYSTAL::scf_calc::get_parallel_executable(const bool mpp) const
{
  if (mpp)
    return mpp_binary;
  else
    return parallel_binary;
}

#endif // CRYSTAL_SCF_CALCS
