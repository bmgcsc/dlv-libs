
#ifndef CRYSTAL_SLICES
#define CRYSTAL_SLICES

namespace CRYSTAL {

  // interface classes for 2D grids
  class grid2D {
  public:
    // serialization
    virtual ~grid2D();

  protected:
    DLV::surface_data *read_data(DLV::operation *op, DLV::surface_data *s,
				 const char filename[], const DLV::string id,
				 const char label[], const int_g max_datasets,
				 char message[], const int_g mlen,
				 const bool periodic);
    bool get_grid(std::ifstream &input, int_g &nx, int_g &ny,
		  real_g origin[3], real_g astep[3], real_g bstep[3],
		  bool &reverse, char message[], const int_g mlen);
    virtual bool get_data(std::ifstream &input, DLV::surface_data *data,
			  const int_g nx, const int_g ny, const bool reverse,
			  const char label[], const int_g max_datasets,
			  char message[], const int_g mlen) = 0;

    bool read_grid(std::ifstream &input, int_g &nx, int_g &ny, real_g origin[3],
		   real_g astep[3], real_g bstep[3], bool &reverse,
		   char message[], const int_g mlen);
    bool read_data(std::ifstream &input, DLV::surface_data *data,
		   const int_g nx, const int_g ny, const bool reverse,
		   const char label[], char message[], const int_g mlen);
    void write_mapnet(std::ofstream &output, const DLV::plane *grid,
		      const int_g np) const;
  };

  class grid2D_v4 : public grid2D {
  protected:
    bool get_data(std::ifstream &input, DLV::surface_data *data,
		  const int_g nx, const int_g ny, const bool reverse,
		  const char label[], const int_g max_datasets,
		  char message[], const int_g mlen);
  };

  class grid2D_v5 : public grid2D {
  protected:
    bool get_data(std::ifstream &input, DLV::surface_data *data,
		  const int_g nx, const int_g ny, const bool reverse,
		  const char label[], const int_g max_datasets,
		  char message[], const int_g mlen);
  };

}

#endif // CRYSTAL_SLICES
