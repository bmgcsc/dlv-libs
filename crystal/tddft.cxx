
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/atom_prefs.hxx"
#include "calcs.hxx"
#include "plots.hxx"
#include "scf.hxx"
#include "tddft.hxx"

DLV::operation *CRYSTAL::load_tddft_plot::create(const char filename[],
						 const int_g version,
						 char message[],
						 const int_g mlen)
{
  if (version < CRYSTAL_DEV) {
    strncpy(message, "BUG: CRYSTAL version does not support TD-DFT", mlen);
    return 0;
  } else {
    load_tddft_plot *op = new load_tddft_plot(filename);
    DLV::data_object *data = op->read_data(op, 0, filename, filename,
					   message, mlen);
    if (data == 0) {
      delete op;
      op = 0;
    } else {
      op->attach();
      op->attach_data(data);
    }
    return op;
  }
}

DLV::string CRYSTAL::load_tddft_plot::get_name() const
{
  return ("Load CRYSTAL TD-DFT plot - " + get_filename());
}

DLV::string CRYSTAL::tddft_calc::get_name() const
{
  return ("CRYSTAL TD-DFT calculation");
}

void CRYSTAL::tddft_data::set_params(const TDDFT &td)
{
  tddft = td;
}

bool CRYSTAL::tddft_calc_v8::add_basis_set(const char filename[],
					   const bool set_default,
					   int_g * &indices, int_g &value,
					   char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::tddft_calc_v8::has_scf() const
{
  return do_scf();
}

bool CRYSTAL::tddft_calc_v8::has_jdos() const
{
  return do_jdos();
}

bool CRYSTAL::tddft_calc_v8::has_pes() const
{
  return do_pes();
}

bool CRYSTAL::tddft_calc::create_files(const bool is_parallel,
				       const bool is_local, char message[],
				       const int_g mlen)
{
  static char tag[] = "tddft";
  if (common_files(is_parallel, is_local, tag)) {
    add_output_file(0, tag, "scf", "fort.98", is_local, true);
    add_output_file(1, tag, "spec", "ABSORPTION.DAT", is_local, true);
    int count = 2;
    if (has_scf()) {
      add_output_file(count, tag, "tddft", "TDDFT.RESTART", is_local, true);
      count++;
    }
    if (has_jdos()) {
      add_output_file(count, tag, "jdos", "JDOS.DAT", is_local, true);
      count++;
    }
    if (has_pes())
      add_output_file(count, tag, "ips", "IPS.DAT", is_local, true);
  } else
    return false;
  return true;
}
void CRYSTAL::tddft_calc_v8::set_params(const Hamiltonian &h, const Basis &b,
					const Kpoints &k, const Tolerances &tol,
					const Convergence &c, const Print &p,
					const Joboptions &j,
					const Optimise &, const Phonon &,
					const Neb &, const CPHF &,
					const TDDFT &tddft, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  tddft_data::set_params(tddft);
}

void CRYSTAL::tddft_calc_v8::add_calc_error_file(const DLV::string tag,
						 const bool is_parallel,
						 const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::tddft_calc_v8::add_restart_file(const DLV::string tag,
					      const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::tddft_calc_v8::add_opt_restart_file(const DLV::string tag, 
						  const bool is_local)
{
}

bool CRYSTAL::tddft_calc_v8::write(const DLV::string filename,
				   const DLV::model *const structure,
				   char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

void CRYSTAL::tddft_calc_v8::write_structure(const DLV::string filename,
					     const DLV::model *const structure,
					     char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::tddft_data::write_tddft(std::ofstream &output) const
{
  output << "TDDFT\n";
  if (tddft.scf_calc) {
    if (tddft.tamm_dancoff)
      output << "TAMM-DANCOFF\n";
    if (!tddft.non_direct)
      output << "NOFASTRESPONSE\n";
    if (tddft.diag_method == 1) {
      output << "DAVIDSON\n";
      output << "DAVIDSONROOTS\n" << tddft.diag_excitation << '\n';
      output << "DAVIDSONITER\n" << tddft.diag_niterations << '\n';
      output << "DAVIDSONSPACE\n" << tddft.diag_space << '\n';
      output << "DAVIDSONCONV\n" << tddft.diag_convergence << '\n';
    }    
  } else
    output << "ONLYIPSPECTRUM\n";
  if (!tddft.calc_jdos)
    output << "NOJDOS\n";
  if (!tddft.calc_pes)
    output << "NOIPS\n";
  output << "ENDTDDFT\n";
}

void CRYSTAL::tddft_data_v8::write_geom_commands(std::ofstream &output,
						 const DLV::model *const) const
{
  write_tddft(output);
}

DLV::dos_plot *CRYSTAL::tddft_plot_file::read_data(DLV::operation *op,
						   DLV::dos_plot *plot,
						   const char filename[],
						   const DLV::string id,
						   char message[],
						   const int_g mlen)
{
  DLV::dos_plot *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[128];
      input.getline(line, 128);
      char text[128];
      sscanf(line, "%s", text);
      if (text[0] != '#')
	strncpy(message, "Invalid header to TD-DFT plot file", mlen);
      else {
	if (plot == 0)
	  data = new DLV::dos_plot("CRYSTAL", id, op, 2);
	else
	  data = plot;
	bool error = false;
	// strip # and use rest of line as title
	int_g i = 0;
	while (line[i] == ' ')
	  i++;
	while (line[i] == '#')
	  i++;
	while (line[i] == ' ')
	  i++;
	char title[128];
	strncpy(title, &line[i], 128);
	// strip trailing blanks
	i = strlen(title);
	while (title[i - 1] == ' ')
	  i--;
	title[i] = '\0';
	// get units
	input.getline(line, 128);
	char xunits[128];
	sscanf(line, "%s %s", text, xunits);
	// Todo - test text == '#'?
	input.getline(line, 128);
	char yunits[128];
	sscanf(line, "%s %s", text, yunits);
	// Todo - test text == '#'?
	const int_g buffersize = 10000;
	real_g grid[buffersize];
	real_g smeared[buffersize];
	real_g raw[buffersize];
	int_g n = 0;
	while (!input.eof()) {
	  input.getline(line, 128);
	  if (strlen(line) == 0)
	    break;
	  if (sscanf(line, "%f %f %f", &grid[n], &smeared[n], &raw[n]) != 3) {
	    strncpy(message, "Invalid data line in TD-DFT plot file", mlen);
	    error = true;
	    break;
	  }
	  n++;
	  if (n == buffersize) {
	    strncpy(message, "Data overflow in plot read", mlen);
	    break;
	  }
	}
	if (!error) {
	  data->set_grid(grid, n);
	  data->add_plot(smeared, 0, "Smeared Data");
	  data->add_plot(raw, 1, "Raw Data");
	  DLV::string label = "Energy (";
	  label += xunits;
	  label += ')';
	  data->set_xaxis_label(label.c_str());
	  data->set_yaxis_label("Intensity");
	  data->set_xunits(xunits);
	  data->set_yunits(yunits);
	  data->set_title(title);
	}
      }
      input.close();
    }
  }
  return data;
}

bool CRYSTAL::tddft_calc_v8::recover(const bool no_err, const bool log_ok,
				     char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"TD-DFT output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"TD-DFT(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    DLV::dos_plot *oas = read_data(this, 0, get_outfile_name(1).c_str(), id,
				   message, len);
    bool ok = (oas != 0);
    int count = 2;
    if (has_scf())
      count++;
    DLV::dos_plot *jdos = 0;
    if (has_jdos()) {
      jdos = read_data(this, 0, get_outfile_name(count).c_str(),
		       id, message, len);
      ok = ok and (jdos != 0);
      count++;
    }
    DLV::dos_plot *pes = 0;
    if (has_pes()) {
      pes = read_data(this, 0, get_outfile_name(count).c_str(), id,
		      message, len);
      ok = ok and (pes != 0);
    }
    if (ok) {
      set_oas(oas);
      attach_data(oas);
      if (has_jdos()) {
	set_jdos(jdos);
	attach_data(jdos);
      }
      if (has_pes()) {
	set_pes(pes);
	attach_data(pes);
      }
      DLV::string name = DLV::add_full_path(get_outfile_name(0));
      data = new DLV::file_data(name, program_name, wavefn_label);
      attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
      if (has_scf()) {
	name = DLV::add_full_path(get_outfile_name(2));
	data = new DLV::file_data(name, program_name, tddft_label);
	attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
	get_display_obj()->set_CRYSTAL_tddft();
#endif // ENABLE_DLV_GRAPHICS
      }
    }
    return ok;
  }
  return true;
}

bool CRYSTAL::load_tddft_plot::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  DLV::dos_plot *v = dynamic_cast<DLV::dos_plot *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload TD-DFT plot", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), message, mlen) != 0);
  return false;
}

bool CRYSTAL::tddft_calc_v8::reload_data(DLV::data_object *data,
					 char message[], const int_g mlen)
{
  DLV::dos_plot *v = dynamic_cast<DLV::dos_plot *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload TD-DFT plot", mlen);
    return false;
  } else if (v != get_jdos() and v != get_oas() and v != get_pes()) {
    strncpy(message, "Unrecognised data object to reload TD-DFT plot", mlen);
    return false;
  } else {
    bool ok = true;
    ok = read_data(this, get_jdos(), get_outfile_name(1).c_str(),
		   get_outfile_name(1).c_str(), message, mlen) != 0;
    int count = 2;
    if (has_scf())
      count++;
    if (has_jdos()) {
      ok = ok and (read_data(this, get_pes(), get_outfile_name(count).c_str(),
			     get_outfile_name(2).c_str(), message, mlen) != 0);
      count++;
    }
    if (has_pes())
      ok = ok and (read_data(this, get_oas(), get_outfile_name(3).c_str(),
			     get_outfile_name(3).c_str(), message, mlen) != 0);
    return ok;
  }
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_tddft_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_tddft_plot("recover");
    }

  }
}

template <class Archive>
void CRYSTAL::tddft_data::serialize(Archive &ar, const unsigned int version)
{
  ar & tddft;
}

template <class Archive>
void CRYSTAL::tddft_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
  ar & boost::serialization::base_object<CRYSTAL::tddft_data>(*this);
}

template <class Archive>
void CRYSTAL::load_tddft_plot::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::tddft_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & jdos;
  ar & oas;
  ar & pes;
}

template <class Archive>
void CRYSTAL::tddft_calc_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::tddft_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::tddft_data_v8>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_tddft_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::tddft_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::tddft_calc_v8)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::dos_plot)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_data_v8)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_tddft_plot)
DLV_NORMAL_EXPLICIT(CRYSTAL::tddft_calc)
DLV_NORMAL_EXPLICIT(CRYSTAL::tddft_calc_v8)

#endif // DLV_USES_SERIALIZE
