
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"
#include "topond3d.hxx"
#include "wavefn.hxx"

CRYSTAL::topond_3d_data::~topond_3d_data()
{
}

DLV::operation *CRYSTAL::topond_3d_calc::create(const bool do_cd,
						const bool do_spin,
						const bool do_laplace,
						const bool do_neg,
						const bool do_grad,
						const bool do_ham,
						const bool do_lagrange,
						const bool do_virial,
						const int_g do_elf,
						const int_g np,
						const real_l xi,
						const real_l xa,
						const real_l yi,
						const real_l ya,
						const real_l zi,
						const real_l za,
						const Density_Matrix &dm,
						const NewK &nk,
						const int_g version,
						const DLV::job_setup_data &job,
						const bool extern_job,
						const char extern_dir[],
						char message[],
						const int_g mlen)
{
  topond_3d_calc *op = 0;
  message[0] = '\0';
  bool parallel = false;
  switch (version) {
  case CRYSTAL98:
  case CRYSTAL03:
  case CRYSTAL06:
  case CRYSTAL09:
    strncpy(message, "TOPOND calc not supported for this CRYSTAL version",
	    mlen);
    break;
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
    op = new topond_3d_calc_v8(do_cd, do_spin, do_laplace, do_neg, do_grad,
			       do_ham, do_lagrange, do_virial, do_elf, np,
			       xi, xa, yi, ya, zi, za, dm, nk);
    break;
  case CRYSTAL_DEV:
    op = new topond_3d_calc_v8(do_cd, do_spin, do_laplace, do_neg, do_grad,
			       do_ham, do_lagrange, do_virial, do_elf, np,
			       xi, xa, yi, ya, zi, za, dm, nk);
    parallel = job.is_parallel();
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::topond_3d_calc operation",
	      mlen);
  } else {
    op->attach(); // attach to find  wavefunction.
    if (op->create_files(parallel, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), op, op->get_model(), message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, parallel,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

DLV::string CRYSTAL::topond_3d_calc::get_name() const
{
  return ("CRYSTAL 3D Topond calculation");
}

DLV::operation *CRYSTAL::load_topo3d_data::create(const char filename[],
						  const int_g version,
						  char message[],
						  const int_g mlen)
{
  switch (version) {
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_topo3d_data_v8::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_topo3d_data::get_name() const
{
  return ("Load CRYSTAL 3D Topond - " + get_filename());
}

DLV::operation *CRYSTAL::load_topo3d_data_v8::create(const char filename[],
						     char message[],
						     const int_g mlen)
{
  load_topo3d_data_v8 *op = new load_topo3d_data_v8(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::topond_3d_calc::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::topond_3d_calc::add_calc_output_files(const DLV::string tag,
						    const bool is_local,
						    const bool link)
{
  return false;
}

bool CRYSTAL::topond_3d_calc_v8::add_calc_output_files(const DLV::string tag,
						       const bool is_local,
						       const bool link)
{
  int index = 0;
  if (topond_3d_data::is_calc_density()) {
    add_output_file(index, tag + "-rho", "dat", "3DRHOO.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_spin()) {
    add_output_file(index, tag + "-spin", "dat", "3DSPDE.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_laplace()) {
    add_output_file(index, tag + "-lap", "dat", "3DLAPP.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_neg()) {
    add_output_file(index, tag + "-nap", "dat", "3DLAPM.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_grad()) {
    add_output_file(index, tag + "-grad", "dat", "3DGRHO.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_ham()) {
    add_output_file(index, tag + "-ham", "dat", "3DKKIN.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_lagrange()) {
    add_output_file(index, tag + "-lag", "dat", "3DGKIN.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_virial()) {
    add_output_file(index, tag + "-vir", "dat", "3DVIRI.DAT", is_local, true);
    index++;
  }
  if (topond_3d_data::is_calc_elf() == 1 or
      topond_3d_data::is_calc_elf() == 2) {
    add_output_file(index, tag + "-elf", "dat", "3DELFB.DAT", is_local, true);
    index++;
  }
  return true;
}

bool CRYSTAL::topond_3d_calc::create_files(const bool is_parallel,
					   const bool is_local, char message[],
					   const int_g mlen)
{
  DLV::string filename;
  DLV::string tddftname;
  bool ok = find_wavefunction(filename, binary_wvfn, message, mlen);
  if (ok) {
    static char tag[] = "topo3";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    return add_calc_output_files(tag, is_local, true);
  } else
    return false;
}

void CRYSTAL::topond_3d_data::write_input(std::ofstream &output,
					  const DLV::model *const structure)
{
  int_g dims = structure->get_number_of_periodic_dims();
  //write_cell(output, dims);
  write_command(output);
  // grid
  output << "1\n";
  output.precision(15);
  real_l cell_step = 1.0 / real_g(npoints - 1);
  // crystal sets non-periodic dims to 500, so need to scale by this
  if (dims < 1) {
    real_l x_step = (x_max - x_min) / (500.0 * real_l(npoints - 1));
    output << x_min / 500.0 << ' ' << x_max / 500.0 << ' ' << x_step << '\n';
  } else {
    output << "0.0 1.0 " << cell_step << '\n';
  }
  if (dims < 2) {
    real_l y_step = (y_max - y_min) / (500.0 * real_l(npoints - 1));
    output << y_min / 500.0 << ' ' << y_max / 500.0 << ' ' << y_step << '\n';
  } else {
    output << "0.0 1.0 " << cell_step << '\n';
  }
  if (dims < 3) {
    real_l z_step = (z_max - z_min) / (500.0 * real_l(npoints - 1));
    output << z_min / 500.0 << ' ' << z_max / 500.0 << ' ' << z_step << '\n';
  } else {
    output << "0.0 1.0 " << cell_step << '\n';
  }
  write_data_sets(output);
  output << "END\n";
  output.close();
}

void CRYSTAL::topond_3d_data::write_command(std::ofstream &output) const
{
  output << "TOPO\n";
  output << "PL3D\n";
}

void CRYSTAL::topond_3d_data::write_data_sets(std::ofstream &output) const
{
  if (calc_density)
    output << "1";
  else
    output << "0";
  if (calc_spin)
    output << " 1";
  else
    output << " 0";
  if (calc_laplace)
    output << " 1";
  else
    output << " 0";
  if (calc_neg)
    output << " 1";
  else
    output << " 0";
  if (calc_grad)
    output << " 1";
  else
    output << " 0";
  if (calc_ham)
    output << " 1";
  else
    output << " 0";
  if (calc_lagrange)
    output << " 1";
  else
    output << " 0";
  if (calc_virial)
    output << " 1";
  else
    output << " 0";
  switch (calc_elf) {
  case 1:
    output << " 1\n";
    break;
  case 2:
    output << " 2\n";
    break;
  default:
    output << " 0\n";
  }
}

void CRYSTAL::topond_3d_calc_v8::write(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output, structure);
  }
}

bool CRYSTAL::topond_3d_calc_v8::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Topond3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Topond3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    const DLV::model *const structure = get_model();
    int_g dims = structure->get_number_of_periodic_dims();
    DLV::coord_type a[3];
    DLV::coord_type b[3];
    DLV::coord_type c[3];
    if (dims > 0)
      structure->get_conventional_lattice(a, b, c);
    int index = 0;
    int total = 0;
    DLV::volume_data *data = 0;
    if (topond_3d_data::is_calc_density()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data, "Density",
			    message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_spin()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data, "Spin",
			    message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_laplace()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data, "Laplacian",
			    message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_neg()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data,
			    "Negative Laplacian", message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_grad()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data,
			    "Magnitude of Gradient", message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_ham()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data,
			    "Hamiltonian Ek", message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_lagrange()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data,
			    "Lagrangian Ek", message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_virial()) {
      data = read_topo_data(this, get_outfile_name(index).c_str(),
			    id + " Topond", a, b, c, dims, data,
			    "Virial Density", message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (topond_3d_data::is_calc_elf() == 1 or
	topond_3d_data::is_calc_elf() == 2) {
      // stuff to find spin based on wannier
      DLV::operation *calc = find_parent(prim_atom_label, program_name);
      bool spin = false;
      if (calc != 0) {
	wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
	if (wfn != 0)
	  spin = wfn->has_spin();
      }
      if (spin) {
	if (topond_3d_data::is_calc_elf() == 2)
	  data = read_topo_data(this, get_outfile_name(index).c_str(),
				id + " Topond", a, b, c, dims, data,
				"ELF beta", message, len);
	else
	  data = read_topo_data(this, get_outfile_name(index).c_str(),
				id + " Topond", a, b, c, dims, data,
				"ELF alpha", message, len);
      } else
	data = read_topo_data(this, get_outfile_name(index).c_str(),
			      id + " Topond", a, b, c, dims, data, " ELF",
			      message, len);
      if (data != 0)
	total++;
      index++;
    }
    if (total != index)
      return false;
    else if (data != 0)
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_topo3d_data_v8::reload_data(DLV::data_object *data,
					       char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  fprintf(stderr, "load reload Todo\n");
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D topond", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::topond_3d_calc_v8::reload_data(DLV::data_object *data,
					     char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  fprintf(stderr, "calc reload Todo\n");
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 3D topond", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_topo3d_data_v8 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_topo3d_data_v8("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::topond_3d_calc_v8 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::topond_3d_calc_v8(false, false, false, false, false,
					 false, false, false, 0, 0,
					 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::topond_3d_data::serialize(Archive &ar, const unsigned int version)
{
  ar & calc_density;
  ar & calc_spin;
  ar & calc_laplace;
  ar & calc_neg;
  ar & calc_grad;
  ar & calc_ham;
  ar & calc_lagrange;
  ar & calc_virial;
  ar & calc_elf;
  ar & npoints;
  ar & x_min;
  ar & x_max;
  ar & y_min;
  ar & y_max;
  ar & z_min;
  ar & z_max;
}

template <class Archive>
void CRYSTAL::topond_3d_data_v8::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::topond_3d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::load_topo3d_data::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_topo3d_data_v8::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<load_topo3d_data>(*this);
}

template <class Archive>
void CRYSTAL::topond_3d_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::topond_3d_calc_v8::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::topond_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::topond_3d_data_v8>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_topo3d_data_v8)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::topond_3d_calc_v8)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_topo3d_data_v8)
DLV_NORMAL_EXPLICIT(CRYSTAL::topond_3d_calc_v8)

#endif // DLV_USES_SERIALIZE
