
#ifndef CRYSTAL_3D_TOPOND
#define CRYSTAL_3D_TOPOND

namespace CRYSTAL {

  class topond_3d_data {
  public:
    // serialization
    virtual ~topond_3d_data();

  protected:
    topond_3d_data(const bool do_cd, const bool do_spin, const bool do_laplace,
		   const bool do_neg, const bool do_grad, const bool do_ham,
		   const bool do_lagrange, const bool do_virial,
		   const int_g do_elf, const int_g np, const real_l xi,
		   const real_l xa, const real_l yi, const real_l ya,
		   const real_l zi, const real_l za);

  protected:
    void write_input(std::ofstream &output, const DLV::model *const structure);
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;
    bool is_calc_density() const;
    bool is_calc_spin() const;
    bool is_calc_laplace() const;
    bool is_calc_neg() const;
    bool is_calc_grad() const;
    bool is_calc_ham() const;
    bool is_calc_lagrange() const;
    bool is_calc_virial() const;
    int_g is_calc_elf() const;

  private:
    bool calc_density;
    bool calc_spin;
    bool calc_laplace;
    bool calc_neg;
    bool calc_grad;
    bool calc_ham;
    bool calc_lagrange;
    bool calc_virial;
    int_g calc_elf;
    int_g npoints;
    real_l x_min;
    real_l x_max;
    real_l y_min;
    real_l y_max;
    real_l z_min;
    real_l z_max;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class topond_3d_data_v8 : public topond_3d_data, public grid3D_v8,
			    public property_data_v6 {
  protected:
    topond_3d_data_v8(const bool do_cd, const bool do_spin,
		      const bool do_laplace, const bool do_neg,
		      const bool do_grad, const bool do_ham,
		      const bool do_lagrange, const bool do_virial,
		      const int_g do_elf, const int_g np,
		      const real_l xi, const real_l xa, const real_l yi,
		      const real_l ya, const real_l zi, const real_l za,
		      const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_topo3d_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_topo3d_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_topo3d_data_v8 : public load_topo3d_data, public grid3D_v6 {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    // public for serialization
    load_topo3d_data_v8(const char file[]);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class topond_3d_calc : public property_calc {
  public:
    static operation *create(const bool do_cd, const bool do_spin,
			     const bool do_laplace, const bool do_neg,
			     const bool do_grad, const bool do_ham,
			     const bool do_lagrange, const bool do_virial,
			     const int_g do_elf, const int_g np,
			     const real_l xi, const real_l xa,
			     const real_l yi, const real_l ya, const real_l zi,
			     const real_l za, const Density_Matrix &dm,
			     const NewK &nk, const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    virtual bool add_calc_output_files(const DLV::string tag,
				       const bool is_local, const bool link);
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class topond_3d_calc_v8 : public topond_3d_calc, public topond_3d_data_v8 {
  public:
    topond_3d_calc_v8(const bool do_cd, const bool do_spin,
		      const bool do_laplace, const bool do_neg,
		      const bool do_grad, const bool do_ham,
		      const bool do_lagrange, const bool do_virial,
		      const int_g do_elf, const int_g np, const real_l xi,
		      const real_l xa, const real_l yi, const real_l ya,
		      const real_l zi, const real_l za,
		      const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    bool add_calc_output_files(const DLV::string tag, const bool is_local,
			       const bool link);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_topo3d_data_v8)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::topond_3d_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::topond_3d_data::topond_3d_data(const bool do_cd,
					       const bool do_spin,
					       const bool do_laplace,
					       const bool do_neg,
					       const bool do_grad,
					       const bool do_ham,
					       const bool do_lagrange,
					       const bool do_virial,
					       const int_g do_elf,
					       const int_g np,
					       const real_l xi, const real_l xa,
					       const real_l yi, const real_l ya,
					       const real_l zi, const real_l za)
  : calc_density(do_cd), calc_spin(do_spin), calc_laplace(do_laplace),
    calc_neg(do_neg), calc_grad(do_grad), calc_ham(do_ham),
    calc_lagrange(do_lagrange), calc_virial(do_virial), calc_elf(do_elf),
    npoints(np), x_max(xa), y_min(yi), y_max(ya), z_min(zi), z_max(za)
{
}

inline bool CRYSTAL::topond_3d_data::is_calc_density() const
{
  return calc_density;
}

inline bool CRYSTAL::topond_3d_data::is_calc_spin() const
{
  return calc_spin;
}

inline bool CRYSTAL::topond_3d_data::is_calc_laplace() const
{
  return calc_laplace;
}

inline bool CRYSTAL::topond_3d_data::is_calc_neg() const
{
  return calc_neg;
}

inline bool CRYSTAL::topond_3d_data::is_calc_grad() const
{
  return calc_grad;
}

inline bool CRYSTAL::topond_3d_data::is_calc_ham() const
{
  return calc_ham;
}

inline bool CRYSTAL::topond_3d_data::is_calc_lagrange() const
{
  return calc_lagrange;
}

inline bool CRYSTAL::topond_3d_data::is_calc_virial() const
{
  return calc_virial;
}

inline DLV::int_g CRYSTAL::topond_3d_data::is_calc_elf() const
{
  return calc_elf;
}


inline CRYSTAL::topond_3d_data_v8::topond_3d_data_v8(const bool do_cd,
						     const bool do_spin,
						     const bool do_laplace,
						     const bool do_neg,
						     const bool do_grad,
						     const bool do_ham,
						     const bool do_lagrange,
						     const bool do_virial,
						     const int_g do_elf,
						     const int_g np,
						     const real_l xi,
						     const real_l xa,
						     const real_l yi,
						     const real_l ya,
						     const real_l zi,
						     const real_l za,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : topond_3d_data(do_cd, do_spin, do_laplace, do_neg, do_grad, do_ham,
		   do_lagrange, do_virial, do_elf, np, xi, xa, yi, ya, zi, za),
    property_data_v6(dm, nk)
{
}

inline CRYSTAL::load_topo3d_data::load_topo3d_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_topo3d_data_v8::load_topo3d_data_v8(const char file[])
  : load_topo3d_data(file)
{
}

inline bool CRYSTAL::topond_3d_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::topond_3d_calc_v8::topond_3d_calc_v8(const bool do_cd,
						     const bool do_spin,
						     const bool do_laplace,
						     const bool do_neg,
						     const bool do_grad,
						     const bool do_ham,
						     const bool do_lagrange,
						     const bool do_virial,
						     const int_g do_elf,
						     const int_g np,
						     const real_l xi,
						     const real_l xa,
						     const real_l yi,
						     const real_l ya,
						     const real_l zi,
						     const real_l za,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : topond_3d_data_v8(do_cd, do_spin, do_laplace, do_neg, do_grad, do_ham,
		      do_lagrange, do_virial, do_elf, np, xi, xa, yi, ya,
		      zi, za, dm, nk)
{
}

#endif // CRYSTAL_3D_TOPOND
