
#ifndef CRYSTAL_WAVEFN_ANALYSE
#define CRYSTAL_WAVEFN_ANALYSE

namespace CRYSTAL {

  struct atom_data {
    int_g atomic_number;
    int_g asym_index;
    int_g basis_index;
    int_g nshells;
    int_g shell_index;
    real_l coords[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  struct shell_data {
    int_g shell_type;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wavefn_calc : public calculate, public structure_file {
  public:
    static operation *create(const char file[], const bool binary,
			     const bool new_view, const bool new_str,
			     const bool set_bonds, const bool do_tddft,
			     const char tddftfile[],
			     const DLV::job_setup_data &job,
			     const int_g version,
			     char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    bool has_spin() const;
    void count_atom_shell_type(const int_g atom, int_g &s, int_g &p, int_g &sp,
			       int_g &d, int_g &f, int_g &g) const;
    int_g find_s_shell(const int_g atom, const int_g n) const;
    int_g find_p_shell(const int_g atom, const int_g n) const;
    int_g find_sp_shell(const int_g atom, const int_g n) const;
    int_g find_d_shell(const int_g atom, const int_g n) const;
    int_g find_f_shell(const int_g atom, const int_g n) const;
    int_g find_g_shell(const int_g atom, const int_g n) const;
    int_g find_s_orbital(const int_g atom, const int_g n) const;
    int_g find_p_orbital(const int_g atom, const int_g n) const;
    int_g find_sp_orbital(const int_g atom, const int_g n) const;
    int_g find_d_orbital(const int_g atom, const int_g n) const;
    int_g find_f_orbital(const int_g atom, const int_g n) const;
    int_g find_g_orbital(const int_g atom, const int_g n) const;
    int_g get_s_or_sp_orbital(const int_g atom, const int_g n) const;
    int_g get_p_or_sp_orbital(const int_g atom, const int_g n) const;
    int_g get_d_orbital(const int_g atom, const int_g n) const;
    int_g get_f_orbital(const int_g atom, const int_g n) const;
    int_g get_g_orbital(const int_g atom, const int_g n) const;
    int_g get_max_n(const int_g atom, const int_g l) const;

    // public for serialization
    wavefn_calc(const char file[], const bool bin = false);

  protected:
    DLV::string get_name() const;
    DLV::string get_filename() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    void attach_wavefn();
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    void write(const DLV::string outfile, char message[], const int_g mlen);
    bool read_info(const DLV::string filename, const DLV::string id,
		   char message[], const int_g mlen);
    bool is_binary() const;

  private:
    DLV::string filename;
    int_g nshells;
    int_g norbitals;
    int_g natoms;
    int_g electrons_per_cell;
    int_g core_electrons;
    atom_data *prim_atoms;
    shell_data *shells;
    int_g spin_electrons;
    int_g nkpoints;
    int_g (*kpoints)[3];
    int_g shrinka;
    int_g shrinkb;
    int_g shrinkc;
    bool has_fermi;
    bool spin;
    bool binary;

    int_g get_atom_basis_index(const int_g atom) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class wavefn_calc_v6 : public wavefn_calc {
  public:
    wavefn_calc_v6(const char file[]);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wavefn_analyse : public wavefn_calc {
  public:
    wavefn_analyse(const char file[], const bool view, const bool sb,
		   const bool bin);

    bool is_geometry() const;
    bool open_new_view() const;

  protected:
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void inherit_model();
    void inherit_data();
    void add_standard_data_objects();

  private:
    bool use_new_view;
    bool set_bonds;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wavefn_analyse_v6 : public wavefn_analyse {
  public:
    wavefn_analyse_v6(const char file[], const bool view, const bool sb,
		      const bool bin);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wavefn_analyse_v8 : public wavefn_analyse_v6 {
  public:
    wavefn_analyse_v8(const char file[], const char tddft[],
		      const bool view, const bool sb, const bool bin);

  protected:
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
    DLV::string tddft_file;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wavefn_calc)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wavefn_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wavefn_analyse)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wavefn_analyse_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wavefn_analyse_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::wavefn_calc::wavefn_calc(const char file[], const bool bin)
  : filename(file), nshells(0), norbitals(0), natoms(0), electrons_per_cell(0),
    core_electrons(0), prim_atoms(0), shells(0), spin_electrons(0),
    nkpoints(0), kpoints(0), shrinka(0), shrinkb(0), shrinkc(0),
    has_fermi(false), spin(false), binary(bin)
{
}

inline CRYSTAL::wavefn_calc_v6::wavefn_calc_v6(const char file[])
  : wavefn_calc(file)
{
}

inline CRYSTAL::wavefn_analyse::wavefn_analyse(const char file[],
					       const bool view, const bool sb,
					       const bool bin)
  : wavefn_calc(file, bin), use_new_view(view), set_bonds(sb)
{
}

inline CRYSTAL::wavefn_analyse_v6::wavefn_analyse_v6(const char file[],
						     const bool view,
						     const bool sb,
						     const bool bin)
  : wavefn_analyse(file, view, sb, bin)
{
}

inline CRYSTAL::wavefn_analyse_v8::wavefn_analyse_v8(const char file[],
						     const char tddft[],
						     const bool view,
						     const bool sb,
						     const bool bin)
  : wavefn_analyse_v6(file, view, sb, bin), tddft_file(tddft)
{
}

inline bool CRYSTAL::wavefn_calc::has_spin() const
{
  return spin;
}

inline DLV::string CRYSTAL::wavefn_calc::get_filename() const
{
  return filename;
}

inline bool CRYSTAL::wavefn_calc::is_binary() const
{
  return binary;
}

#endif // CRYSTAL_WAVEFN_ANALYSE
