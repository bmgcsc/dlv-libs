
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#include "utils.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "atom_model.hxx"
#include "atom_model_impl.hxx"
#include "atom_pairs.hxx"
#include "atom_prefs.hxx"

const DLV::string DLV::crystal03_id_type::label = "crystal03_id";
const DLV::string DLV::core_electrons_type::label = "core_electrons";
const DLV::string DLV::charge_type::label = "point_charge";
const DLV::string DLV::isotope_type::label = "isotope";
const DLV::string DLV::debye_waller_type::label = "debye_waller";
const DLV::string DLV::displacements_type::label = "displacements";
const DLV::string DLV::rod_type::label = "rod_parameters";

namespace {

  const DLV::real_l frac_tol = 0.0001;
  const DLV::real_l cart_tol = 0.00001;
  const DLV::real_l purify_tol = 0.01;

}

DLV::int_g DLV::atom_type::get_charge() const
{
  if (inherit_charge)
    return atomic_data->find_charge(atomic_number);
  else
    return default_charge;
}

void DLV::atom_type::get_colour(colour_data &c) const
{
  if (inherit_colours)
    atomic_data->find_colour(c, atomic_number);
  else
    c = colour;
}

DLV::real_g DLV::atom_type::get_radius() const
{
  if (inherit_radius) {
    if (inherit_charge)
      return atomic_data->find_radius(atomic_number);
    else
      return atomic_data->find_radius(atomic_number, default_charge);
  } else
    return radius;
}

DLV::atom_type::~atom_type()
{
  // Todo?
}

DLV::atom_info_type::~atom_info_type()
{
  // Todo?
}

DLV::int_g DLV::atom_type::check_atomic_symbol(const char symbol[])
{
  return atomic_data->lookup_symbol(symbol);
}

DLV::string DLV::atom_type::get_atomic_symbol(const int_g atomic_number)
{
  return atomic_data->get_symbol(atomic_number);
}

void DLV::atom_type::set_properties(const int_g c, real_g &r, const real_g red,
				    const real_g green, const real_g blue,
				    const int_g s, const bool use_charge,
				    const bool use_radius,
				    const bool use_colour, const bool use_spin)
{
  bool changing = false;
  if (use_charge == inherit_charge)
    changing = true;
  else if (c != default_charge)
    changing = true;
  if (use_charge)
    set_charge(c);
  else
    inherit_charge = true;
  if (changing and !use_radius)
    r = get_radius();
  if (use_radius)
    set_radius(r);
  else
    inherit_radius = true;
  if (use_colour)
    set_colour(red, green, blue);
  else
    inherit_colours = true;
  if (use_spin) {
    switch (spin) {
    case 1:
      set_spin(alpha_spin);
      break;
    case 2:
      set_spin(beta_spin);
      break;
    default:
      set_spin(no_spin);
      break;
    }
  } else
    set_spin(no_spin);
}

void DLV::atom_type::get_properties(int_g &c, real_g &r, real_g &red,
				    real_g &green, real_g &blue, int_g &s,
				    bool &use_charge, bool &use_radius,
				    bool &use_colour, bool &use_spin) const
{
  use_charge = !inherit_charge;
  c = get_charge();
  use_radius = !inherit_radius;
  r = get_radius();
  use_colour = !inherit_colours;
  colour_data col;
  get_colour(col);
  red = col.red;
  green = col.green;
  blue = col.blue;
  use_spin = (spin != no_spin);
  s = (int)spin;
}

void DLV::atom_type::complete(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3], const int_g dim)
{
  if (valid_cartesian_coords) {
    if (dim > 0) {
      real_l cell[3][3] = { {1.0, 0.0, 0.0},
			    {0.0, 1.0, 0.0},
			    {0.0, 0.0, 1.0} };
      cell[0][0] = va[0];
      cell[1][0] = va[1];
      cell[2][0] = va[2];
      if (dim > 1) {
	cell[0][1] = vb[0];
	cell[1][1] = vb[1];
	cell[2][1] = vb[2];
	if (dim > 2) {
	  cell[0][2] = vc[0];
	  cell[1][2] = vc[1];
	  cell[2][2] = vc[2];
	}
      }
      real_l inverse[3][3];
      matrix_invert(cell, inverse, dim);
      for (int_g i = 0; i < dim; i++) {
	fractional_coords[i] = 0.0;
	for (int_g j = 0; j < dim; j++)
	  fractional_coords[i] += inverse[i][j] * cartesian_coords[j];
      }
    }
    for (int_g i = dim; i < 3; i++)
      fractional_coords[i] = cartesian_coords[i];
  } else if (valid_fractional_coords) {
    cartesian_coords[0] = va[0] * fractional_coords[0];
    cartesian_coords[1] = va[1] * fractional_coords[0];
    cartesian_coords[2] = va[2] * fractional_coords[0];
    if (dim > 1) {
      cartesian_coords[0] += vb[0] * fractional_coords[1];
      cartesian_coords[1] += vb[1] * fractional_coords[1];
      cartesian_coords[2] += vb[2] * fractional_coords[1];
      if (dim > 2) {
	cartesian_coords[0] += vc[0] * fractional_coords[2];
	cartesian_coords[1] += vc[1] * fractional_coords[2];
	cartesian_coords[2] += vc[2] * fractional_coords[2];
      }
    }
    for (int_g i = dim; i < 3; i++)
      cartesian_coords[i] = fractional_coords[i];
  } // else Todo
}

void DLV::atom_type::set_crystal03_id(const int_g id)
{
  crystal03_id_type *p = new crystal03_id_type(id);
  properties[crystal03_id_type::label] = p;
}

void DLV::atom_type::set_core_electrons(const int_g n)
{
  core_electrons_type *p = new core_electrons_type(n);
  properties[core_electrons_type::label] = p;
}

void DLV::atom_type::set_point_charge(const real_l c)
{
  charge_type *p = new charge_type(c);
  properties[charge_type::label] = p;
}

void DLV::atom_type::set_isotope(const int_g i)
{
  isotope_type *p = new isotope_type(i);
  properties[isotope_type::label] = p;
}

void DLV::atom_type::set_debye_waller(const real_g i, const real_g j)
{
  debye_waller_type *p = new debye_waller_type(i, j);
  properties[debye_waller_type::label] = p;
}

void DLV::atom_type::set_displacements(const real_g d[3])
{
  displacements_type *p = new displacements_type(d);
  properties[displacements_type::label] = p;
}

void DLV::atom_type::set_rod_ids(const int_g d, const int_g d2,
				 const int_g nocc,
				 const real_g xc1, const int_g nx1,
				 const real_g xc2, const int_g nx2,
				 const real_g yc1, const int_g ny1,
				 const real_g yc2, const int_g ny2,
				 const int_g nz)
{
  rod_type *p = new rod_type(d, d2, nocc, xc1, nx1,xc2,nx2,
			     yc1,ny1,yc2,ny2,nz);
  properties[rod_type::label] = p;
}

void DLV::atom_type::set_rod_dw1_id(const int_g d)
{
  std::map<const string, atom_info_type *>::iterator ptr =
    properties.find(rod_type::label);
  if(ptr==properties.end()){
    rod_type *p = new rod_type(d, 0, 0, 0.0, 0, 0.0, 0, 0.0, 0,
			       0.0, 0, 0);
    std::pair<std::map<const string, atom_info_type *>::iterator, bool> x =
      properties.insert(std::make_pair(rod_type::label, p));
    ptr = x.first;
  }
  atom_info_type *p1 = ptr->second;
  rod_type *p2 = dynamic_cast<rod_type *>(p1);
  p2->set_dw1(d);
}

void DLV::atom_type::set_rod_dw2_id(const int_g d)
{
  std::map<const string, atom_info_type *>::iterator ptr =
    properties.find(rod_type::label);
  if(ptr==properties.end()){
    rod_type *p = new rod_type(0, d, 0, 0.0, 0, 0.0, 0, 0.0, 0,
			       0.0, 0, 0);
    std::pair<std::map<const string, atom_info_type *>::iterator, bool> x =
      properties.insert(std::make_pair(rod_type::label, p));
    ptr = x.first;
  }
  atom_info_type *p1 = ptr->second;
  rod_type *p2 = dynamic_cast<rod_type *>(p1);
  p2->set_dw2(d);
}

void DLV::atom_type::set_rod_occ_id(const int_g d)
{
  std::map<const string, atom_info_type *>::iterator ptr =
    properties.find(rod_type::label);
  if(ptr==properties.end()){
    rod_type *p = new rod_type(0, 0, d, 0.0, 0, 0.0, 0, 0.0, 0,
			       0.0, 0, 0);
    std::pair<std::map<const string, atom_info_type *>::iterator, bool> x =
      properties.insert(std::make_pair(rod_type::label, p));
    ptr = x.first;
  }
  atom_info_type *p1 = ptr->second;
  rod_type *p2 = dynamic_cast<rod_type *>(p1);
  p2->set_occ(d);
}

DLV::int_g DLV::atom_type::get_crystal03_id() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(crystal03_id_type::label);
  int_g atn = get_atomic_number();
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    crystal03_id_type *p2 = dynamic_cast<crystal03_id_type *>(p1);
    int_g id = p2->get_id();
    // trap edited atoms that have changed type but not id
    if (id % 100 == atn)
      return id;
    else
      return atn;
  } else
    return atn;
}

DLV::int_g DLV::atom_type::get_core_electrons() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(core_electrons_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    core_electrons_type *p2 = dynamic_cast<core_electrons_type *>(p1);
    return p2->get_electrons();
  } else
    return 0;
}

DLV::real_l DLV::atom_type::get_point_charge() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(charge_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    charge_type *p2 = dynamic_cast<charge_type *>(p1);
    return p2->get_charge();
  } else
    return (real_l)get_charge();
}

DLV::int_g DLV::atom_type::get_isotope() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(isotope_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    isotope_type *p2 = dynamic_cast<isotope_type *>(p1);
    return p2->get_isotope();
  } else
    return 0;
}

DLV::real_g DLV::atom_type::get_debye_waller1() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(debye_waller_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    debye_waller_type *p2 = dynamic_cast<debye_waller_type *>(p1);
    return p2->get_debye_waller1();
  } else
    return 0.0;
}

DLV::real_g DLV::atom_type::get_debye_waller2() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(debye_waller_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    debye_waller_type *p2 = dynamic_cast<debye_waller_type *>(p1);
    return p2->get_debye_waller2();
  } else
    return 0.0;
}

void DLV::atom_type::get_displacements(real_g d[3]) const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(displacements_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    displacements_type *p2 = dynamic_cast<displacements_type *>(p1);
    p2->get_displacements(d);
  } else {
    for(int_g i=0; i<3; i++)
      d[i] = 0.0;
  }
  return;
}

DLV::int_g DLV::atom_type::get_rod_dw1() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_dw1();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_dw2() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_dw2();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_noccup() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_noccup();
  } else
    return 0;
}

DLV::real_g DLV::atom_type::get_rod_xconst() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_xconst();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_nxdis() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_nxdis();
  } else
    return 0;
}

DLV::real_g DLV::atom_type::get_rod_x2const() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_x2const();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_nx2dis() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_nx2dis();
  } else
    return 0;
}

DLV::real_g DLV::atom_type::get_rod_yconst() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_yconst();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_nydis() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_nydis();
  } else
    return 0;
}

DLV::real_g DLV::atom_type::get_rod_y2const() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_y2const();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_ny2dis() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_ny2dis();
  } else
    return 0;
}

DLV::int_g DLV::atom_type::get_rod_nzdis() const
{
  std::map<const string, atom_info_type *>::const_iterator ptr =
    properties.find(rod_type::label);
  if (ptr != properties.end()) {
    atom_info_type *p1 = ptr->second;
    rod_type *p2 = dynamic_cast<rod_type *>(p1);
    return p2->get_nzdis();
  } else
    return 0;
}

DLV::int_g DLV::atom_position::compare(const atom_position &a) const
{
  coord_type d1 = cartesian_coords[0] - a.cartesian_coords[0];
  coord_type d2 = cartesian_coords[1] - a.cartesian_coords[1];
  coord_type d3 = cartesian_coords[2] - a.cartesian_coords[2];
  if (abs(d1) < cart_tol and abs(d2) < cart_tol and abs(d3) < cart_tol) {
    int_g diff = atom_ref->get_atomic_number()
      - a.atom_ref->get_atomic_number();
    if (diff == 0) {
      // Not sure how reliable this is if edit flags aren't cleared
      // and an atom if left on top of another.
      if (atom_ref->get_edit_flag() == a.atom_ref->get_edit_flag())
	return diff;
      else if (atom_ref->get_edit_flag())
	return 1;
      else
	return -1;
    } else
      return diff;
  } else {
    coord_type l1 = (cartesian_coords[0] * cartesian_coords[0]
		     + cartesian_coords[1] * cartesian_coords[1]
		     + cartesian_coords[2] * cartesian_coords[2]);
    coord_type l2 = (a.cartesian_coords[0] * a.cartesian_coords[0]
		     + a.cartesian_coords[1] * a.cartesian_coords[1]
		     + a.cartesian_coords[2] * a.cartesian_coords[2]);
    if (abs(l1 - l2) < cart_tol) {
      if (abs(d1) < cart_tol) {
	if (abs(d2) < cart_tol) {
	  // d3 < cart_tol was trapped earlier
	  if (cartesian_coords[2] > a.cartesian_coords[2])
	    return 1;
	  else
	    return -1;
	} else if (cartesian_coords[1] > a.cartesian_coords[1])
	  return 1;
	else
	  return -1;
      } else if (cartesian_coords[0] > a.cartesian_coords[0])
	return 1;
      else
	return -1;
    } else if (l1 > l2)
      return 1;
    else
      return -1;
  }
}

bool DLV::atom_position::compare_translation(const atom_position &a,
					     const int_g dim) const
{
  if (atom_ref->get_atomic_number() - a.atom_ref->get_atomic_number() == 0) {
    coord_type fa[3];
    fa[0] = fractional_coords[0] - shift[0];
    fa[1] = fractional_coords[1] - shift[1];
    fa[2] = fractional_coords[2] - shift[2];
    coord_type fb[3];
    fb[0] = a.fractional_coords[0] - a.shift[0];
    fb[1] = a.fractional_coords[1] - a.shift[1];
    fb[2] = a.fractional_coords[2] - a.shift[2];
    coord_type d[3];
    d[0] = fa[0] - fb[0];
    d[1] = fa[1] - fb[1];
    d[2] = fa[2] - fb[2];
    bool ok = true;
    for (int_g i = 0; i < dim; i++) {
      if (abs(d[i]) > frac_tol and abs(d[i] + 1.0) > frac_tol and
	  abs(d[i] - 1.0) > frac_tol) {
	ok = false;
	break;
      }
    }
    if (ok) {
      for (int_g i = dim; i < 3; i++) {
	if (abs(cartesian_coords[i] - a.cartesian_coords[i]) > cart_tol) {
	  ok = false;
	  break;
	}
      }
    }
    return ok;
  }
  return false;
}

bool DLV::atom_position::is_near_position(const coord_type coords[3]) const
{
  if (fractional_coords[0] > coords[0] - purify_tol and
      fractional_coords[0] < coords[0] + purify_tol) {
    if (fractional_coords[1] > coords[1] - purify_tol and
	fractional_coords[1] < coords[1] + purify_tol) {
      if (fractional_coords[2] > coords[2] - purify_tol and
	  fractional_coords[2] < coords[2] + purify_tol)
	return true;
    }
  }
  return false;
}

bool DLV::atom_position::is_same_shift(const atom_position &a) const
{
  if (abs(shift[0] - a.shift[0]) < frac_tol and
      abs(shift[1] - a.shift[1]) < frac_tol and
      abs(shift[2] - a.shift[2]) < frac_tol)
    return true;
  else
    return false;
}

void DLV::atom_position::set_coords(const coord_type coords[3],
				    const real_l matrix[3][3],
				    const real_l inverse[3][3], const int_g dim)
{
  // convert to fractional coords
  coord_type frac[3] = { 0.0, 0.0, 0.0 };
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      frac[i] += inverse[i][j] * coords[j];
  for (int_g i = dim; i < 3; i++)
    frac[i] = coords[i];
  // adjust coords into cell 0
  for (int_g i = 0; i < dim; i++) {
    shift[i] = - truncate(frac[i]);
    frac[i] += shift[i];
    if (frac[i] < - frac_tol) {
      frac[i] += 1.0;
      shift[i] += 1.0;
    }
  }
  for (int_g i = 0; i < 3; i++) {
    fractional_coords[i] = frac[i];
    cartesian_coords[i] = 0.0;
  }
  // Convert back to cartesian
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      cartesian_coords[i] += matrix[i][j] * frac[j];
  for (int_g i = dim; i < 3; i++)
    cartesian_coords[i] = coords[i];
}

/*
void DLV::atom_position::set_fractional(const real_l inv[3][3])
{
  // convert to fractional coords
  coord_type frac[3] = { 0.0, 0.0, 0.0 };
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      frac[i] += inv[i][j] * cartesian_coords[j];
  shift[0] = 0.0;
  shift[1] = 0.0;
  shift[2] = 0.0;
  for (int_g i = 0; i < 3; i++)
    fractional_coords[i] = frac[i];
}
*/

void DLV::atom_position::centre(const real_l matrix[3][3], const int_g dim,
				const bool flags[3])
{
  bool changed = false;
  for (int_g i = 0; i < dim; i++) {
    if (flags[i] and fractional_coords[i] > real_l(0.5) - frac_tol) {
      shift[i] -= 1.0;
      fractional_coords[i] -= 1.0;
      changed = true;
    }
  }
  if (changed) {
    for (int_g i = 0; i < dim; i++)
      cartesian_coords[i] = 0.0;
    // Convert back to cartesian
    for (int_g i = 0; i < dim; i++)
      for (int_g j = 0; j < dim; j++)
	cartesian_coords[i] += matrix[i][j] * fractional_coords[j];
  }
}

void DLV::atom_position::translate(const real_l op[3],
				   const real_l matrix[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++) {
    fractional_coords[i] += op[i];
    shift[i] += op[i];
  }
  for (int_g i = 0; i < dim; i++) {
    if (fractional_coords[i] > (real_l(1.0) - frac_tol)) {
      fractional_coords[i] -= 1.0;
      shift[i] -= 1.0;
    } else if (fractional_coords[i] < - frac_tol) {
      fractional_coords[i] += 1.0;
      shift[i] += 1.0;
    }
  }
  for (int_g i = 0; i < dim; i++)
    cartesian_coords[i] = 0.0;
  // Convert back to cartesian
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      cartesian_coords[i] += matrix[i][j] * fractional_coords[j];
}

void DLV::atom_position::transform_cartesian(const real_l rop[3][3],
					     const real_l top[3])
{
  coord_type c[3];
  for (int_g i = 0; i < 3; i++)
    c[i] = cartesian_coords[i];
  for (int_g i = 0; i < 3; i++) {
    cartesian_coords[i] = 0.0;
    for (int_g j = 0; j < 3; j++)
      cartesian_coords[i] += rop[i][j] * c[j];
  }
  for (int_g i = 0; i < 3; i++)
    cartesian_coords[i] += top[i];
}

void DLV::atom_position::rotate_cartesian(const real_l op[3][3])
{
  coord_type c[3];
  for (int_g i = 0; i < 3; i++)
    c[i] = cartesian_coords[i];
  for (int_g i = 0; i < 3; i++) {
    cartesian_coords[i] = 0.0;
    for (int_g j = 0; j < 3; j++)
      cartesian_coords[i] += op[i][j] * c[j];
  }
}

void DLV::atom_position::offset_coords(const coord_type coords[3],
				       const coord_type cell[3])
{
  for (int_g i = 0; i < 3; i++) {
    shift[i] += cell[i];
    fractional_coords[i] += cell[i];
    cartesian_coords[i] += coords[i];
  }
}

void DLV::atom_tree::insert(const atom_position &atm, const bool cell)
{
  bool tall = false;
  root->insert(root, atm, 0, tall, nnodes, cell);
}

void DLV::atom_tree::insert(const atom_position &atm,
			    const atom_tree_node *ptr, const bool cell)
{
  bool tall = false;
  root->insert(root, atm, ptr, tall, nnodes, cell);
}

DLV::atom_tree::~atom_tree()
{
  if (root != 0)
    root->remove();
  root = 0;
  nnodes = 0;
}

void DLV::atom_tree::clear()
{
  if (root != 0)
    root->remove();
  root = 0;
  nnodes = 0;
}

void DLV::atom_tree_node::remove()
{
  if (left_child != 0)
    left_child->remove();
  if (right_child != 0)
    right_child->remove();
  delete this;
}

const DLV::atom_tree_node *
DLV::atom_tree_node::find(const atom_position &atm) const
{
  const atom_tree_node *ptr = this;
  int_g diff = 0;
  while (ptr != 0 and (diff = ptr->data.compare(atm)) != 0) {
    if (diff > 0)
      ptr = ptr->left_child;
    else
      ptr = ptr->right_child;
  }
  return ptr;
}

/* Balance algorithms from N.Wirth, algorithms+data structures, p 215- */

void DLV::atom_tree_node::insert(atom_tree_node * &node,
				 const atom_position &newitem,
				 const atom_tree_node *ptr,
				 bool &taller, int_g &natoms, const bool cell)
/* node is the top node of this branch of the tree */
{
  atom_tree_node *p1, *p2;

  if (node == 0) {
    /*	make new node */
    node = new atom_tree_node(newitem, ptr, 0, 0);
    if (cell)
      node->set_base_cell();
    taller = true;
    natoms++;
    //return node;
  } else { /* chase down tree further */
    int_g diff = node->data.compare(newitem);
    if (diff > 0) {
      insert(node->left_child, newitem, ptr, taller, natoms, cell);
      if (taller) { /* left branch has grown */
	switch (node->balance) {
	case TREE_right:
	  node->balance = TREE_equal;
	  taller = false;
	  break;
	case TREE_equal:
	  node->balance = TREE_left;
	  break;
	case TREE_left: /* rebalance */
	  p1 = node->left_child;
	  if (p1->balance == TREE_left) { /* single ll rotation */
	    node->left_child = p1->right_child;
	    p1->right_child = node;
	    node->balance = TREE_equal;
	    node = p1;
	  } else { /* real_l lr rotation */
	    p2 = p1->right_child;
	    p1->right_child = p2->left_child;
	    p2->left_child = p1;
	    node->left_child = p2->right_child;
	    p2->right_child = node;
	    if (p2->balance == TREE_left)
	      node->balance = TREE_right;
	    else
	      node->balance = TREE_equal;
	    if (p2->balance == TREE_right)
	      p1->balance = TREE_left;
	    else
	      p1->balance = TREE_equal;
	    node = p2;
	  }
	  node->balance = TREE_equal;
	  taller = false;
	  break;
	}
      } /* if newitem<p^.item */
    } else if (diff < 0) {
      insert(node->right_child, newitem, ptr, taller, natoms, cell);
      if (taller) { /* right branch has grown */
	switch (node->balance) {
	case TREE_left:
	  node->balance = TREE_equal;
	  taller = false;
	  break;
	case TREE_equal:
	  node->balance = TREE_right;
	  break;
	case TREE_right: /* rebalance */
	  p1 = node->right_child;
	  if (p1->balance == TREE_right) { /* single rr rotation */
	    node->right_child = p1->left_child;
	    p1->left_child = node;
	    node->balance = TREE_equal;
	    node = p1;
	  } else { /* real_l rl rotation */
	    p2 = p1->left_child;
	    p1->left_child = p2->right_child;
	    p2->right_child = p1;
	    node->right_child = p2->left_child;
	    p2->left_child = node;
	    if (p2->balance == TREE_right)
	      node->balance = TREE_left;
	    else
	      node->balance = TREE_equal;
	    if (p2->balance == TREE_left)
	      p1->balance = TREE_right;
	    else
	      p1->balance = TREE_equal;
	    node = p2;
	  }
	  node->balance = TREE_equal;
	  taller = false;
	  break;
	}
      } /* if newitem>p^.item */
    } else {
      taller = false;
      if (cell)
	node->set_base_cell();
      //return node;
    } /* found p^=newitem here*/
  }
}

void DLV::atom_tree_node::count_atom_types(int_g types[], const int_g n) const
{
  int_g j = data.get_atomic_number();
  if (j >= 0 and j < n)
    types[j]++;
  if (left_child != 0)
    left_child->count_atom_types(types, n);
  if (right_child != 0)
    right_child->count_atom_types(types, n);
}

void DLV::atom_tree::count_atom_types(int_g types[], const int_g n) const
{
  if (root != 0)
    root->count_atom_types(types, n);
}

void DLV::atom_tree_node::count_crystal03_ids(int_g types[]) const
{
  types[data.get_crystal03_id()]++;
  if (left_child != 0)
    left_child->count_crystal03_ids(types);
  if (right_child != 0)
    right_child->count_crystal03_ids(types);
}

void DLV::atom_tree::count_crystal03_ids(int_g types[]) const
{
  if (root != 0)
    root->count_crystal03_ids(types);
}

void DLV::atom_tree_node::count_atoms(int_g &count, const bool only_selected,
				      const bool ignore_selections,
				      const bool transparent_flags,
				      const bool use_edit_flags,
				      const bool edit) const
{
  if (edit) {
    if (atom_is_being_edited())
      count++;
  } else {
    if (!use_edit_flags or (use_edit_flags and !atom_is_being_edited())) {
      if (only_selected) {
	if (atom_is_selected() and !atom_being_selected())
	  count++;
	else if (transparent_flags and atom_flag_is_ok())
	  count++;
      } else {
	if (!atom_is_selected() or
	    (atom_is_selected() and
	     (ignore_selections or atom_being_selected())))
	  if (!transparent_flags or (transparent_flags and !atom_flag_is_ok()))
	    count++;
      }
    }
  }
  if (left_child != 0)
    left_child->count_atoms(count, only_selected, ignore_selections,
			    transparent_flags, use_edit_flags, edit);
  if (right_child != 0)
    right_child->count_atoms(count, only_selected, ignore_selections,
			     transparent_flags, use_edit_flags, edit);
}

DLV::int_g DLV::atom_tree::count_atoms(const bool only_selected,
				const bool ignore_selections,
				const bool transparent_flags,
				const bool use_edit_flags,
				const bool edit) const
{
  int_g count = 0;
  if (nnodes > 0) {
    bool ok = true;
    if (edit) {
      if (!use_edit_flags)
	ok = false;
    }
    if (ok)
      root->count_atoms(count, only_selected, ignore_selections,
			transparent_flags, use_edit_flags, edit);
  }
  return count;
}

void DLV::atom_tree_node::fill_arrays(real_g coords[][3], real_g colours[][3],
				      real_g radii[], int_g &count,
				      const bool only_selected,
				      const bool ignore_selections,
				      const bool use_flags,
				      const int_g flag_type,
				      const std::vector<real_g (*)[3]> *traj,
				      const int_g nframes, const int_g n,
				      const bool use_edit_flags,
				      const bool edit) const
{
  bool draw = false;
  if (edit) {
    if (atom_is_being_edited())
      draw = true;
  } else {
    if (!use_edit_flags or (use_edit_flags and !atom_is_being_edited())) {
      bool transparent_flags = (use_flags and flag_type == 2);
      if (only_selected) {
	if (atom_is_selected() and !atom_being_selected())
	  draw = true;
	else if (transparent_flags and atom_flag_is_ok())
	  draw = true;
      } else {
	if (!atom_is_selected() or
	    (atom_is_selected() and
	     (ignore_selections or atom_being_selected())))
	  if (!transparent_flags or (transparent_flags and !atom_flag_is_ok()))
	    draw = true;
      }
    }
  }
  if (draw) {
    coord_type c[3];
    data.get_cartesian_coords(c);
    if (traj != 0) {
      for (int_g i = 0; i < nframes; i++) {
	coords[(n * i) + count][0] = real_g(c[0]) + (*traj)[i][count][0];
	coords[(n * i) + count][1] = real_g(c[1]) + (*traj)[i][count][1];
	coords[(n * i) + count][2] = real_g(c[2]) + (*traj)[i][count][2];
      }
    } else {
      coords[count][0] = real_g(c[0]);
      coords[count][1] = real_g(c[1]);
      coords[count][2] = real_g(c[2]);
    }
    colour_data rgb;
    data.get_colour(rgb);
    if (!edit and ((atom_is_selected() and ignore_selections) or
		   atom_being_selected())) {
      if (use_flags and flag_type == 1) {
	colours[count][0] = 1.0;
	colours[count][1] = 1.0;
	colours[count][2] = 1.0;
	switch (get_atom_flag()) {
	case flag_unset:
	  colours[count][0] = 0.0;
	  break;
	case flag_partial:
	  colours[count][0] = 0.0;
	  colours[count][1] = 0.25;
	  break;
	case flag_done:
	  colours[count][1] = 0.0;
	  break;
	}
      } else {
	colours[count][0] = real_g(1.0) - rgb.red;
	colours[count][1] = real_g(1.0) - rgb.green;
	colours[count][2] = real_g(1.0) - rgb.blue;
      }
    } else {
      if (use_flags and flag_type == 1) {
	colours[count][0] = 0.0;
	colours[count][1] = 0.0;
	colours[count][2] = 0.0;
	switch (get_atom_flag()) {
	case flag_unset:
	  colours[count][0] = 1.0;
	  break;
	case flag_partial:
	  colours[count][0] = 1.0;
	  colours[count][1] = 0.75;
	  break;
	case flag_done:
	  colours[count][1] = 1.0;
	  break;
	}
      } else {
	colours[count][0] = rgb.red;
	colours[count][1] = rgb.green;
	colours[count][2] = rgb.blue;
      }
    }
    radii[count] = data.get_radius();
    if (use_flags and flag_type == 0) {
      switch (get_atom_flag()) {
      case flag_partial:
	radii[count] *= 0.5f;
	break;
      case flag_done:
	radii[count] *= 0.1f;
	break;
      default:
	break;
      }
    }
    count++;
  }
  if (left_child != 0)
    left_child->fill_arrays(coords, colours, radii, count, only_selected,
			    ignore_selections, use_flags, flag_type,
			    traj, nframes, n, use_edit_flags, edit);
  if (right_child != 0)
    right_child->fill_arrays(coords, colours, radii, count, only_selected,
			     ignore_selections, use_flags, flag_type,
			     traj, nframes, n, use_edit_flags, edit);
}

void DLV::atom_tree::fill_arrays(real_g coords[][3], real_g colours[][3],
				 real_g radii[], const bool only_selected,
				 const bool ignore_selections,
				 const bool use_flags,
				 const int_g flag_type,
				 const std::vector<real_g (*)[3]> *traj,
				 const int_g nframes, const int_g n,
				 const bool use_edit_flags,
				 const bool edit) const
{
  int_g count = 0;
  if (root != 0) {
    bool ok = true;
    if (edit) {
      if (!use_edit_flags)
	ok = false;
    }
    if (ok)
      root->fill_arrays(coords, colours, radii, count, only_selected,
			ignore_selections, use_flags, flag_type,
			traj, nframes, n, use_edit_flags, edit);
  }
}

void DLV::atom_tree_node::get_z_range(real_l &zmin, real_l &zmax) const
{
  coord_type c[3];
  data.get_cartesian_coords(c);
  if (c[2] < zmin)
    zmin = c[2];
  if (c[2] > zmax)
    zmax = c[2];
  if (left_child != 0)
    left_child->get_z_range(zmin, zmax);
  if (right_child != 0)
    right_child->get_z_range(zmin, zmax);
}

void DLV::atom_tree::get_z_range(real_l &zmin, real_l &zmax) const
{
  if (root == 0) {
    zmin = 0.0;
    zmax = 0.0;
  } else {
    zmin = 1e10;
    zmax = -1e10;
    root->get_z_range(zmin, zmax);
  }
}

void DLV::atom_tree_node::count_origin_cell_atoms(int_g &count) const
{
  if (base_cell)
    count++;
  if (left_child != 0)
    left_child->count_origin_cell_atoms(count);
  if (right_child != 0)
    right_child->count_origin_cell_atoms(count);
}

void DLV::atom_tree_node::get_origin_cell_atoms(atom_tree_node *atoms[],
						int_g &index)
{
  if (base_cell) {
    atoms[index] = this;
    index++;
  }
  if (left_child != 0)
    left_child->get_origin_cell_atoms(atoms, index);
  if (right_child != 0)
    right_child->get_origin_cell_atoms(atoms, index);
}

void DLV::atom_tree_node::get_origin_cell_atoms(bond_info atoms[], int_g &index,
						real_g &max_radius,
						const real_g overlap,
						const bond_data &bonds,
						int_g &count)
{
  if (base_cell) {
    atoms[index].node = this;
    atoms[index].data_index = count;
    data.get_cartesian_coords(atoms[index].cartesian);
    data.get_fractional_coords(atoms[index].fractional);
    data.get_colour(atoms[index].colour);
    atoms[index].radius = data.get_radius() * overlap;
    if (atoms[index].radius > max_radius)
      max_radius = atoms[index].radius;
    atoms[index].atomic_number = data.get_atomic_number();
    atoms[index].min_a = 0;
    atoms[index].max_a = 0;
    atoms[index].min_b = 0;
    atoms[index].max_b = 0;
    atoms[index].min_c = 0;
    atoms[index].max_c = 0;
    if (bonds.has_type_pairs()) {
      for (int_g i = 0; i <= index; i++) {
	if (!atoms[index].bonds[i]) {
	  if (bonds.is_set(atoms[i].atomic_number,
			   atoms[index].atomic_number)) {
	    atoms[index].bonds[i] = true;
	    for (int_g j = i + 1; j <= index; j++) {
	      if (atoms[j].atomic_number == atoms[i].atomic_number)
		atoms[index].bonds[j] = true;
	    }
	  }
	}
      }
    }
    if (bonds.has_single_pairs()) {
      for (int_g i = 0; i <= index; i++) {
	if (!atoms[index].bonds[i]) {
	  if (bonds.is_pair_set(atoms[i].node->data.get_atom(),
				atoms[index].node->data.get_atom()))
	    atoms[index].bonds[i] = true;
	}
      }
    }
    index++;
  }
  count++;
  if (left_child != 0)
    left_child->get_origin_cell_atoms(atoms, index, max_radius,
				      overlap, bonds, count);
  if (right_child != 0)
    right_child->get_origin_cell_atoms(atoms, index, max_radius,
				       overlap, bonds, count);
}

void DLV::atom_tree_node::get_all_cell_atoms(bond_info atoms[],
					     int_g &index, real_g &max_radius,
					     const real_g overlap,
					     const bond_data &bonds,
					     int_g &count, const int_g na,
					     const int_g nb, const int_g nc)
{
  atoms[index].node = this;
  atoms[index].data_index = count;
  data.get_cartesian_coords(atoms[index].cartesian);
  data.get_fractional_coords(atoms[index].fractional);
  coord_type shift[3];
  data.get_fractional_coords(shift);
  atoms[index].fractional[0] = (atoms[index].fractional[0] + shift[0]) / na;
  atoms[index].fractional[1] = (atoms[index].fractional[1] + shift[1]) / nb;
  atoms[index].fractional[2] = (atoms[index].fractional[2] + shift[2]) / nc;
  data.get_colour(atoms[index].colour);
  atoms[index].radius = data.get_radius() * overlap;
  if (atoms[index].radius > max_radius)
    max_radius = atoms[index].radius;
  atoms[index].atomic_number = data.get_atomic_number();
  atoms[index].min_a = 0;
  atoms[index].max_a = 0;
  atoms[index].min_b = 0;
  atoms[index].max_b = 0;
  atoms[index].min_c = 0;
  atoms[index].max_c = 0;
  if (bonds.has_type_pairs()) {
    for (int_g i = 0; i <= index; i++) {
      if (!atoms[index].bonds[i]) {
	if (bonds.is_set(atoms[i].atomic_number,
			 atoms[index].atomic_number)) {
	  atoms[index].bonds[i] = true;
	  for (int_g j = i + 1; j <= index; j++) {
	    if (atoms[j].atomic_number == atoms[i].atomic_number)
	      atoms[index].bonds[j] = true;
	  }
	}
      }
    }
  }
  if (bonds.has_single_pairs()) {
    for (int_g i = 0; i <= index; i++) {
      if (!atoms[index].bonds[i]) {
	if (bonds.is_pair_set(atoms[i].node->data.get_atom(),
			      atoms[index].node->data.get_atom()))
	  atoms[index].bonds[i] = true;
      }
    }
  }
  index++;
  count++;
  if (left_child != 0)
    left_child->get_all_cell_atoms(atoms, index, max_radius,
				   overlap, bonds, count, na, nb, nc);
  if (right_child != 0)
    right_child->get_all_cell_atoms(atoms, index, max_radius,
				    overlap, bonds, count, na, nb, nc);
}

void DLV::atom_tree_node::conv_origin_cell_atoms(atom_tree_node *atomlist[],
						 int_g &index, const int_g dim,
						 const real_l cell[3][3],
						 const real_l inverse[3][3])
{
  //coord_type coords[3];
  //data.get_cartesian_coords(coords);
  //data.set_coords(coords, cell, inverse, dim);
  atomlist[index] = this;
  index++;
  if (left_child != 0)
    left_child->conv_origin_cell_atoms(atomlist, index, dim, cell, inverse);
  if (right_child != 0)
    right_child->conv_origin_cell_atoms(atomlist, index, dim, cell, inverse);
}

void DLV::atom_tree::add_conv_cell_atoms(atom_tree &tree,
					 const coord_type a[3],
					 const coord_type b[3],
					 const real_l ops[][3],
					 const int_g nops,
					 const bool centre[3]) const
{
  if (nnodes > 0) {
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    cell[2][2] = 1.0;
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    int_g index = 0;
    root->conv_origin_cell_atoms(origin, index, 2, cell, inverse);
    // reset base cells in case centring has changed
    //for (int_g i = 0; i < count; i++)
    //origin[i]->clear_base_cell();
    // apply translators.
    for (int_g i = 0; i < count; i++) {
      for (int_g j = 0; j < nops; j++) {
	atom_position p(origin[i]->get_data());
	coord_type coords[3];
	p.get_cartesian_coords(coords);
	p.set_coords(coords, cell, inverse, 2);
	p.translate(ops[j], cell, 2);
	p.centre(cell, 2, centre);
	tree.insert(p, true);
      }
    }
    delete_local_array(origin);
  }
}

void DLV::atom_tree::add_conv_cell_atoms(atom_tree &tree,
					 const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3],
					 const real_l ops[][3],
					 const int_g nops,
					 const bool centre[3]) const
{
  if (nnodes > 0) {
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    int_g index = 0;
    root->conv_origin_cell_atoms(origin, index, 3, cell, inverse);
    // reset base cells in case centring has changed
    //for (int_g i = 0; i < count; i++)
    //origin[i]->clear_base_cell();
    // apply translators.
    for (int_g i = 0; i < count; i++) {
      for (int_g j = 0; j < nops; j++) {
	atom_position p(origin[i]->get_data());
	coord_type coords[3];
	p.get_cartesian_coords(coords);
	p.set_coords(coords, cell, inverse, 3);
	p.translate(ops[j], cell, 3);
	p.centre(cell, 3, centre);
	tree.insert(p, true);
      }
    }
    delete_local_array(origin);
  }
}

void DLV::atom_tree::store_atom(const atom_tree_node *atm, const real_l cella,
				const real_l cellb, const real_l cellc,
				const coord_type a[3], const coord_type b[3],
				const coord_type c[3])
{
  coord_type cell[3];
  cell[0] = cella;
  cell[1] = cellb;
  cell[2] = cellc;
  coord_type coords[3];
  coords[0] = cella * a[0] + cellb * b[0] + cellc * c[0];
  coords[1] = cella * a[1] + cellb * b[1] + cellc * c[1];
  coords[2] = cella * a[2] + cellb * b[2] + cellc * c[2];
  atom_position p(atm->get_data());
  p.offset_coords(coords, cell);
  insert(p, atm);
}

void DLV::atom_tree::replicate_atoms(const coord_type a[3], const real_l tol,
				     const int_g na, const bool centre,
				     const bool edges)
{
  if (nnodes > 0) {
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    int_g index = 0;
    root->get_origin_cell_atoms(origin, index);
    coord_type cell[3];
    cell[1] = 0.0;
    cell[2] = 0.0;
    int_g min_na = 0;
    int_g max_na = na - 1;
    if (centre) {
      max_na = na / 2;
      min_na = - max_na;
      if (na % 2 == 0)
	max_na--;
    }
    if (na > 1) {
      for (int_g i = min_na; i <= max_na; i++) {
	cell[0] = (coord_type)i;
	coord_type xa[3];
	xa[0] = cell[0] * a[0];
	xa[1] = cell[0] * a[1];
	xa[2] = cell[0] * a[2];
	for (int_g n = 0; n < count; n++) {
	  atom_position p(origin[n]->get_data());
	  p.offset_coords(xa, cell);
	  insert(p, origin[n]);
	}
      }
    }
    if (edges) {
      // edges.
      real_l min_tol = tol;
      real_l max_tol = real_l(1.0) - tol;
      real_l min_offset = na;
      real_l max_offset = -1;
      if (centre) {
	if (na % 2 == 1) {
	  min_tol = real_l(-0.5) + tol;
	  max_tol = real_l(0.5) - tol;
	  min_offset = real_l(na + 1) / 2.0;
	  max_offset = -min_offset;
	} else {
	  min_offset = real_l(na) / 2.0;
	  max_offset = -min_offset - 1.0;
	}
      }
      for (int_g n = 0; n < count; n++) {
	coord_type b[3] = { 0.0, 0.0, 0.0 };
	coord_type c[3] = { 0.0, 0.0, 0.0 };
	coord_type fractions[3];
	origin[n]->get_fractional_coords(fractions);
	if (fractions[0] < min_tol)
	  store_atom(origin[n], min_offset, 0.0, 0.0, a, b, c);
	else if (fractions[0] > max_tol)
	  store_atom(origin[n], max_offset, 0.0, 0.0, a, b, c);
      }
    }
    delete_local_array(origin);
  }
}

void DLV::atom_tree::replicate_atoms(const coord_type a[3],
				     const coord_type b[3], const real_l tol,
				     const int_g na, const int_g nb,
				     const bool centre, const bool edges)
{
  if (nnodes > 0) {
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    int_g index = 0;
    root->get_origin_cell_atoms(origin, index);
    int_g min_na = 0;
    int_g max_na = na - 1;
    int_g min_nb = 0;
    int_g max_nb = nb - 1;
    if (centre) {
      max_na = na / 2;
      min_na = - max_na;
      if (na % 2 == 0)
	max_na--;
      max_nb = nb / 2;
      min_nb = - max_nb;
      if (nb % 2 == 0)
	max_nb--;
    }
    if (na > 1 or nb > 1) {
      coord_type cell[3];
      cell[2] = 0.0;
      for (int_g i = min_na; i <= max_na; i++) {
	cell[0] = (coord_type)i;
	coord_type xa[3];
	xa[0] = cell[0] * a[0];
	xa[1] = cell[0] * a[1];
	xa[2] = cell[0] * a[2];
	for (int_g j = min_nb; j <= max_nb; j++) {
	  cell[1] = (coord_type) j;
	  coord_type xb[3];
	  xb[0] = cell[1] * b[0] + xa[0];
	  xb[1] = cell[1] * b[1] + xa[1];
	  xb[2] = cell[1] * b[2] + xa[2];
	  for (int_g n = 0; n < count; n++) {
	    atom_position p(origin[n]->get_data());
	    p.offset_coords(xb, cell);
	    insert(p, origin[n]);
	  }
	}
      }
    }
    if (edges) {
      // edges.
      real_l mina_tol = tol;
      real_l maxa_tol = real_l(1.0) - tol;
      real_l minb_tol = tol;
      real_l maxb_tol = real_l(1.0) - tol;
      real_l mina_offset = na;
      real_l maxa_offset = -1;
      real_l minb_offset = nb;
      real_l maxb_offset = -1;
      if (centre) {
	if (na % 2 == 1) {
	  mina_tol = real_l(-0.5) + tol;
	  maxa_tol = real_l(0.5) - tol;
	  mina_offset = real_l(na + 1) / 2.0;
	  maxa_offset = -mina_offset;
	} else {
	  mina_offset = real_l(na) / 2.0;
	  maxa_offset = -mina_offset - 1.0;
	}
	if (nb % 2 == 1) {
	  minb_tol = real_l(-0.5) + tol;
	  maxb_tol = real_l(0.5) - tol;
	  minb_offset = real_l(nb + 1) / 2.0;
	  maxb_offset = -minb_offset;
	} else {
	  minb_offset = real_l(nb) / 2.0;
	  maxb_offset = -minb_offset - 1.0;
	}
      }
      for (int_g n = 0; n < count; n++) {
	coord_type c[3] = { 0.0, 0.0, 0.0 };
	coord_type fractions[3];
	origin[n]->get_fractional_coords(fractions);
	if (fractions[0] < mina_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    store_atom(origin[n], mina_offset, i, 0.0, a, b, c);
	  if (fractions[1] < minb_tol)
	    store_atom(origin[n], mina_offset, minb_offset, 0.0, a, b, c);
	  else if (fractions[1] > maxb_tol)
	    store_atom(origin[n], mina_offset, maxb_offset, 0.0, a, b, c);
	} else if (fractions[0] > maxa_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    store_atom(origin[n], maxa_offset, i, 0.0, a, b, c);
	  if (fractions[1] < minb_tol)
	    store_atom(origin[n], maxa_offset, minb_offset, 0.0, a, b, c);
	  else if (fractions[1] > maxb_tol)
	    store_atom(origin[n], maxa_offset, maxb_offset, 0.0, a, b, c);
	}
	if (fractions[1] < minb_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    store_atom(origin[n], i, minb_offset, 0.0, a, b, c);
	} else if (fractions[1] > maxb_tol) {
	  for (int_g i = 0; i < na; i++)
	    store_atom(origin[n], i, maxb_offset, 0.0, a, b, c);
	}
      }
    }
    delete_local_array(origin);
  }
}

void DLV::atom_tree::replicate_atoms(const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3], const real_l tol,
				     const int_g na, const int_g nb,
				     const int_g nc, const bool centre,
				     const bool edges)
{
  if (nnodes > 0) {
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    int_g index = 0;
    root->get_origin_cell_atoms(origin, index);
    int_g min_na = 0;
    int_g max_na = na - 1;
    int_g min_nb = 0;
    int_g max_nb = nb - 1;
    int_g min_nc = 0;
    int_g max_nc = nc - 1;
    if (centre) {
      max_na = na / 2;
      min_na = - max_na;
      if (na % 2 == 0)
	max_na--;
      max_nb = nb / 2;
      min_nb = - max_nb;
      if (nb % 2 == 0)
	max_nb--;
      max_nc = nc / 2;
      min_nc = - max_nc;
      if (nc % 2 == 0)
	max_nc--;
    }
    if (na > 1 or nb > 1 or nc > 1) {
      coord_type cell[3];
      for (int_g i = min_na; i <= max_na; i++) {
	cell[0] = (coord_type)i;
	coord_type xa[3];
	xa[0] = cell[0] * a[0];
	xa[1] = cell[0] * a[1];
	xa[2] = cell[0] * a[2];
	for (int_g j = min_nb; j <= max_nb; j++) {
	  cell[1] = (coord_type) j;
	  coord_type xb[3];
	  xb[0] = cell[1] * b[0] + xa[0];
	  xb[1] = cell[1] * b[1] + xa[1];
	  xb[2] = cell[1] * b[2] + xa[2];
	  for (int_g k = min_nc; k <= max_nc; k++) {
	    cell[2] = (coord_type)k;
	    coord_type xc[3];
	    xc[0] = cell[2] * c[0] + xb[0];
	    xc[1] = cell[2] * c[1] + xb[1];
	    xc[2] = cell[2] * c[2] + xb[2];
	    for (int_g n = 0; n < count; n++) {
	      atom_position p(origin[n]->get_data());
	      p.offset_coords(xc, cell);
	      insert(p, origin[n]);
	    }
	  }
	}
      }
    }
    if (edges) {
      // edges.
      real_l mina_tol = tol;
      real_l maxa_tol = real_l(1.0) - tol;
      real_l minb_tol = tol;
      real_l maxb_tol = real_l(1.0) - tol;
      real_l minc_tol = tol;
      real_l maxc_tol = real_l(1.0) - tol;
      real_l mina_offset = na;
      real_l maxa_offset = -1;
      real_l minb_offset = nb;
      real_l maxb_offset = -1;
      real_l minc_offset = nc;
      real_l maxc_offset = -1;
      if (centre) {
	if (na % 2 == 1) {
	  mina_tol = real_l(-0.5) + tol;
	  maxa_tol = real_l(0.5) - tol;
	  mina_offset = real_l(na + 1) / 2.0;
	  maxa_offset = -mina_offset;
	} else {
	  mina_offset = real_l(na) / 2.0;
	  maxa_offset = -mina_offset - 1.0;
	}
	if (nb % 2 == 1) {
	  minb_tol = real_l(-0.5) + tol;
	  maxb_tol = real_l(0.5) - tol;
	  minb_offset = real_l(nb + 1) / 2.0;
	  maxb_offset = -minb_offset;
	} else {
	  minb_offset = real_l(nb) / 2.0;
	  maxb_offset = -minb_offset - 1.0;
	}
	if (nc % 2 == 1) {
	  minc_tol = real_l(-0.5) + tol;
	  maxc_tol = real_l(0.5) - tol;
	  minc_offset = real_l(nc + 1) / 2.0;
	  maxc_offset = -minc_offset;
	} else {
	  minc_offset = real_l(nc) / 2.0;
	  maxc_offset = -minc_offset - 1.0;
	}
      }
      for (int_g n = 0; n < count; n++) {
	coord_type fractions[3];
	origin[n]->get_fractional_coords(fractions);
	if (fractions[0] < mina_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    for (int_g j = min_nc; j <= max_nc; j++)
	      store_atom(origin[n], mina_offset, i, j, a, b, c);
	  if (fractions[1] < minb_tol) {
	    for (int_g i = min_nc; i <= max_nc; i++)
	      store_atom(origin[n], mina_offset, minb_offset, i, a, b, c);
	    if (fractions[2] < minc_tol)
	      store_atom(origin[n], mina_offset, minb_offset,
			 minc_offset, a, b, c);
	  } else if (fractions[1] > maxb_tol) {
	    for (int_g i = min_nc; i <= max_nc; i++)
	      store_atom(origin[n], mina_offset, maxb_offset, i, a, b, c);
	    if (fractions[2] < minc_tol)
	      store_atom(origin[n], mina_offset, maxb_offset,
			 minc_offset, a, b, c);
	    else if (fractions[2] > maxc_tol)
	      store_atom(origin[n], mina_offset, maxb_offset,
			 maxc_offset, a, b, c);
	  }
	  if (fractions[2] < minc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	      store_atom(origin[n], mina_offset, i, minc_offset, a, b, c);
	  } else if (fractions[2] > maxc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	      store_atom(origin[n], mina_offset, i, maxc_offset, a, b, c);
	  }
	} else if (fractions[0] > maxa_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    for (int_g j = min_nc; j <= max_nc; j++)
	      store_atom(origin[n], maxa_offset, i, j, a, b, c);
	  if (fractions[1] < minb_tol) {
	    for (int_g i = min_nc; i <= max_nc; i++)
	      store_atom(origin[n], maxa_offset, minb_offset, i, a, b, c);
	    if (fractions[2] < minc_tol)
	      store_atom(origin[n], maxa_offset, minb_offset,
			 minc_offset, a, b, c);
	  } else if (fractions[1] > maxb_tol) {
	    for (int_g i = min_nc; i <= max_nc; i++)
	      store_atom(origin[n], maxa_offset, maxb_offset, i, a, b, c);
	    if (fractions[2] < minc_tol)
	      store_atom(origin[n], maxa_offset, maxb_offset,
			 minc_offset, a, b, c);
	    else if (fractions[2] > maxc_tol)
	      store_atom(origin[n], maxa_offset, maxb_offset,
			 maxc_offset, a, b, c);
	  }
	  if (fractions[2] < minc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	      store_atom(origin[n], maxa_offset, i, minc_offset, a, b, c);
	  } else if (fractions[2] > maxc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	      store_atom(origin[n], maxa_offset, i, maxc_offset, a, b, c);
	  }
	}
	if (fractions[1] < minb_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = min_nc; j <= max_nc; j++)
	      store_atom(origin[n], i, minb_offset, j, a, b, c);
	  if (fractions[2] < minc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	      store_atom(origin[n], i, minb_offset, minc_offset, a, b, c);
	  } else if (fractions[2] > maxc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	      store_atom(origin[n], i, minb_offset, maxc_offset, a, b, c);
	  }
	} else if (fractions[1] > maxb_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = min_nc; j <= max_nc; j++)
	      store_atom(origin[n], i, maxb_offset, j, a, b, c);
	  if (fractions[2] < minc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	      store_atom(origin[n], i, maxb_offset, minc_offset, a, b, c);
	  } else if (fractions[2] > maxc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	      store_atom(origin[n], i, maxb_offset, maxc_offset, a, b, c);
	  }
	}
	if (fractions[2] < minc_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = min_nb; j <= max_nb; j++)
	      store_atom(origin[n], i, j, minc_offset, a, b, c);
	} else if (fractions[2] > maxc_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = min_nb; j <= max_nb; j++)
	      store_atom(origin[n], i, j, maxc_offset, a, b, c);
	}
      }
    }
    delete_local_array(origin);
  }
}

// similar to 3D replicate_atoms, but centre modified for c axis
void DLV::atom_tree::replicate_surface(const coord_type a[3],
				       const coord_type b[3],
				       const coord_type c[3], const real_l tol,
				       const int_g na, const int_g nb,
				       const int_g nc, const bool centre,
				       const bool edges)
{
  if (nnodes > 0) {
    /*real_l cell[3][3];
    cell[0][0] = a[0];
    cell[1][0] = a[1];
    cell[2][0] = a[2];
    cell[0][1] = b[0];
    cell[1][1] = b[1];
    cell[2][1] = b[2];
    cell[0][2] = c[0];
    cell[1][2] = c[1];
    cell[2][2] = c[2];
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);*/
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    atom_tree_node **origin = new_local_array1(atom_tree_node *, count);
    int_g index = 0;
    root->get_origin_cell_atoms(origin, index);
    // Need to set the fractional c value for bottom edge
    /*for (int_g n = 0; n < count; n++)
      if (origin[n]->get_atom()->has_special_periodicity())
      origin[n]->get_data().set_fractional(inverse);*/
    int_g min_na = 0;
    int_g max_na = na - 1;
    int_g min_nb = 0;
    int_g max_nb = nb - 1;
    int_g min_nc = -nc + 1;
    int_g max_nc = 0;
    if (centre) {
      max_na = na / 2;
      min_na = - max_na;
      if (na % 2 == 0)
	max_na--;
      max_nb = nb / 2;
      min_nb = - max_nb;
      if (nb % 2 == 0)
	max_nb--;
    }
    if (na > 1 or nb > 1 or nc > 1) {
      coord_type cell[3];
      for (int_g i = min_na; i <= max_na; i++) {
	cell[0] = (coord_type)i;
	coord_type xa[3];
	xa[0] = cell[0] * a[0];
	xa[1] = cell[0] * a[1];
	xa[2] = cell[0] * a[2];
	for (int_g j = min_nb; j <= max_nb; j++) {
	  cell[1] = (coord_type) j;
	  coord_type xb[3];
	  xb[0] = cell[1] * b[0] + xa[0];
	  xb[1] = cell[1] * b[1] + xa[1];
	  xb[2] = cell[1] * b[2] + xa[2];
	  for (int_g n = 0; n < count; n++) {
	    if (origin[n]->get_atom()->has_special_periodicity()) {
	      for (int_g k = min_nc; k <= max_nc; k++) {
		cell[2] = (coord_type)k;
		coord_type xc[3];
		xc[0] = cell[2] * c[0] + xb[0];
		xc[1] = cell[2] * c[1] + xb[1];
		xc[2] = cell[2] * c[2] + xb[2];
		atom_position p(origin[n]->get_data());
		p.offset_coords(xc, cell);
		insert(p, origin[n]);
	      }
	    } else {
	      cell[2] = 0;
	      atom_position p(origin[n]->get_data());
	      p.offset_coords(xb, cell);
	      insert(p, origin[n]);
	    }
	  }
	}
      }
    }
    if (edges) {
      // edges.
      real_l mina_tol = tol;
      real_l maxa_tol = real_l(1.0) - tol;
      real_l minb_tol = tol;
      real_l maxb_tol = real_l(1.0) - tol;
      /*real_l minc_tol = tol;*/
      //real_l maxc_tol = 1.0 - tol;
      real_l mina_offset = na;
      real_l maxa_offset = -1;
      real_l minb_offset = nb;
      real_l maxb_offset = -1;
      /*real_l minc_offset = -nc;*/
      //real_l maxc_offset = 0;
      if (centre) {
	if (na % 2 == 1) {
	  mina_tol = real_l(-0.5) + tol;
	  maxa_tol = real_l(0.5) - tol;
	  mina_offset = real_l(na + 1) / 2.0;
	  maxa_offset = -mina_offset;
	} else {
	  mina_offset = real_l(na) / 2.0;
	  maxa_offset = -mina_offset - 1.0;
	}
	if (nb % 2 == 1) {
	  minb_tol = real_l(-0.5) + tol;
	  maxb_tol = real_l(0.5) - tol;
	  minb_offset = real_l(nb + 1) / 2.0;
	  maxb_offset = -minb_offset;
	} else {
	  minb_offset = real_l(nb) / 2.0;
	  maxb_offset = -minb_offset - 1.0;
	}
      }
      for (int_g n = 0; n < count; n++) {
	// We don't do bottom edge? (has_special_periodicity) minc_tol
	int_g m_nc = min_nc;
	if (!origin[n]->get_atom()->has_special_periodicity())
	  m_nc = 0;
	coord_type fractions[3];
	origin[n]->get_fractional_coords(fractions);
	if (fractions[0] < mina_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    for (int_g j = m_nc; j <= max_nc; j++)
	      store_atom(origin[n], mina_offset, i, j, a, b, c);
	  if (fractions[1] < minb_tol) {
	    for (int_g i = m_nc; i <= max_nc; i++)
	      store_atom(origin[n], mina_offset, minb_offset, i, a, b, c);
	    /*if (fractions[2] < minc_tol)
	      store_atom(origin[n], mina_offset, minb_offset,
	      minc_offset, a, b, c);*/
	  } else if (fractions[1] > maxb_tol) {
	    for (int_g i = m_nc; i <= max_nc; i++)
	      store_atom(origin[n], mina_offset, maxb_offset, i, a, b, c);
	    /*if (fractions[2] < minc_tol)
	      store_atom(origin[n], mina_offset, maxb_offset,
	      minc_offset, a, b, c);*/
	    //else if (fractions[2] > maxc_tol)
	    //  store_atom(origin[n], mina_offset, maxb_offset,
	    //       maxc_offset, a, b, c);
	  }
	  /*if (fractions[2] < minc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	    store_atom(origin[n], mina_offset, i, minc_offset, a, b, c);
	    }*/ //else if (fractions[2] > maxc_tol) {
	  //for (int_g i = min_nb; i <= max_nb; i++)
	  //  store_atom(origin[n], mina_offset, i, maxc_offset, a, b, c);
	  //}
	} else if (fractions[0] > maxa_tol) {
	  for (int_g i = min_nb; i <= max_nb; i++)
	    for (int_g j = m_nc; j <= max_nc; j++)
	      store_atom(origin[n], maxa_offset, i, j, a, b, c);
	  if (fractions[1] < minb_tol) {
	    for (int_g i = m_nc; i <= max_nc; i++)
	      store_atom(origin[n], maxa_offset, minb_offset, i, a, b, c);
	    /*if (fractions[2] < minc_tol)
	      store_atom(origin[n], maxa_offset, minb_offset,
	      minc_offset, a, b, c);*/
	  } else if (fractions[1] > maxb_tol) {
	    for (int_g i = m_nc; i <= max_nc; i++)
	      store_atom(origin[n], maxa_offset, maxb_offset, i, a, b, c);
	    /*if (fractions[2] < minc_tol)
	      store_atom(origin[n], maxa_offset, maxb_offset,
	      minc_offset, a, b, c);*/
	    //else if (fractions[2] > maxc_tol)
	    //  store_atom(origin[n], maxa_offset, maxb_offset,
	    //       maxc_offset, a, b, c);
	  }
	  /*if (fractions[2] < minc_tol) {
	    for (int_g i = min_nb; i <= max_nb; i++)
	    store_atom(origin[n], maxa_offset, i, minc_offset, a, b, c);
	    }*/ //else if (fractions[2] > maxc_tol) {
	  //for (int_g i = min_nb; i <= max_nb; i++)
	  //  store_atom(origin[n], maxa_offset, i, maxc_offset, a, b, c);
	  //}
	}
	if (fractions[1] < minb_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = m_nc; j <= max_nc; j++)
	      store_atom(origin[n], i, minb_offset, j, a, b, c);
	  /*if (fractions[2] < minc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	    store_atom(origin[n], i, minb_offset, minc_offset, a, b, c);
	    }*/ //else if (fractions[2] > maxc_tol) {
	  // for (int_g i = min_na; i <= max_na; i++)
	  //  store_atom(origin[n], i, minb_offset, maxc_offset, a, b, c);
	  //}
	} else if (fractions[1] > maxb_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	    for (int_g j = m_nc; j <= max_nc; j++)
	      store_atom(origin[n], i, maxb_offset, j, a, b, c);
	  /*if (fractions[2] < minc_tol) {
	    for (int_g i = min_na; i <= max_na; i++)
	    store_atom(origin[n], i, maxb_offset, minc_offset, a, b, c);
	    }*/ //else if (fractions[2] > maxc_tol) {
	  //for (int_g i = min_na; i <= max_na; i++)
	  //  store_atom(origin[n], i, maxb_offset, maxc_offset, a, b, c);
	  //}
	}
	/*if (fractions[2] < minc_tol) {
	  for (int_g i = min_na; i <= max_na; i++)
	  for (int_g j = min_nb; j <= max_nb; j++)
	  store_atom(origin[n], i, j, minc_offset, a, b, c);
	  }*/ //else if (fractions[2] > maxc_tol) {
	//for (int_g i = min_na; i <= max_na; i++)
	//  for (int_g j = min_nb; j <= max_nb; j++)
	//    store_atom(origin[n], i, j, maxc_offset, a, b, c);
	//}
	//}
      }
    }
    delete_local_array(origin);
  }
}

bool DLV::atom_tree_node::is_bonded_pair(const atom_tree_node *node,
					 const bond_data &bonds) const

{
  atom atom1 = get_atom();
  atom atom2 = node->get_atom();
  return bonds.is_set(atom1, atom2);
}

bool DLV::atom_tree_node::atoms_are_bonded(const bond_info data[],
					   const int_g i, const int_g j,
					   const coord_type pos[3],
					   const real_g map_data[][3])
{
  real_l pos1[3];
  for (int_g k = 0; k < 3; k++)
    pos1[k] = data[i].cartesian[k];
  if (map_data != 0) {
    for (int_g k = 0; k < 3; k++)
      pos1[k] += real_l(map_data[data[i].data_index][k]);
  }
  real_g l = real_g((pos[0] - pos1[0]) * (pos[0] - pos1[0])
		    + (pos[1] - pos1[1]) * (pos[1] - pos1[1])
		    + (pos[2] - pos1[2]) * (pos[2] - pos1[2]));
  real_g dist = (data[i].radius + data[j].radius);
  return ((dist * dist) >= l);
}

void DLV::atom_tree_node::select_colour(colour_data &rgb, int_g &btype,
					const atom_position &atm,
					const colour_data &col,
					const bool invert_col,
					const bool edit) const
{
  const atom_tree_node *ptr = find(atm);
  btype = 1;
  rgb = col;
  if (ptr != 0) {
    if (edit and ptr->atom_is_being_edited())
      btype = 3;
    else if (ptr->atom_is_selected()) {
      if (invert_col or ptr->atom_being_selected()) {
	rgb.red = real_g(1.0) - col.red;
	rgb.green = real_g(1.0) - col.green;
	rgb.blue = real_g(1.0f) - col.blue;
      } else
	btype = 2;
    }
  }
}

void DLV::atom_tree_node::store_bond(std::list<bond_list> &info,
				     const bond_info data[],
				     const int_g i1, const int_g i2,
				     const coord_type pos2[3],
				     const real_l, const bool invert_colours,
				     const real_g map_data[][3],
				     const bool edit) const
{
  coord_type pos1[3];
  for (int_g i = 0; i < 3; i++)
    pos1[i] = data[i1].cartesian[i];
  if (map_data != 0) {
    for (int_g i = 0; i < 3; i++)
      pos1[i] += real_l(map_data[data[i1].data_index][i]);
  }
  coord_type diff[3];
  real_l r1 = real_l(data[i1].radius);
  real_l r2 = real_l(data[i2].radius);
  diff[0] = (pos2[0] - pos1[0]) * r1 / (r1 + r2);
  diff[1] = (pos2[1] - pos1[1]) * r1 / (r1 + r2);
  diff[2] = (pos2[2] - pos1[2]) * r1 / (r1 + r2);
  atom_position atom1 = data[i1].node->data;
  atom_position atom2 = data[i2].node->data;
  bond_list new_bond;
  select_colour(new_bond.colour1, new_bond.type1,
		atom1, data[i1].colour, invert_colours, edit);
  for (int_g l = 0; l < 3; l++) {
    new_bond.start[l] = real_g(pos1[l]);
    new_bond.middle[l] = real_g(pos1[l] + diff[l]);
  }
  select_colour(new_bond.colour2, new_bond.type2,
		atom2, data[i2].colour, invert_colours, edit);
  for (int_g l = 0; l < 3; l++)
    new_bond.end[l] = real_g(pos2[l]);
  info.push_back(new_bond);
}

void DLV::atom_tree_node::gen_bond_second_atom(std::list<bond_list> &info,
					       const bond_info data[],
					       const int_g index, const int_g,
					       const bool invert_colours,
					       const real_l tol,
					       const real_g map_data[][3],
					       const bool edit) const
{
  for (int_g node = 0; node < index; node++) {
    if (data[index].bonds[node]) {
      coord_type p[3];
      for (int_g i = 0; i < 3; i++)
	p[i] = data[node].cartesian[i];
      if (map_data != 0) {
	for (int_g i = 0; i < 3; i++)
	  p[i] += real_l(map_data[data[node].data_index][i]);
      }
      if (atoms_are_bonded(data, index, node, p, map_data))
	store_bond(info, data, index, node, p, tol,
		   invert_colours, map_data, edit);
    }
  }
}

void DLV::atom_tree_node::generate_bonds(std::list<bond_list> &info,
					 const bond_info data[],
					 const int_g no,
					 const bool invert_colours,
					 const real_l tol,
					 const real_g map_data[][3],
					 const bool edit) const
{
  for (int_g i = 0; i < no; i++) {
    gen_bond_second_atom(info, data, i, no, invert_colours,
			 tol, map_data, edit);
  }
}

void DLV::atom_tree_node::edge_bond(std::list<bond_list> &info,
				    const bond_info &data,
				    const coord_type pos1[3],
				    const coord_type pos2[3],
				    const coord_type diff[3],
				    const real_l cella, const coord_type a[3],
				    const bool invert_colours,
				    const bool edit) const
{
  coord_type xb[3];
  xb[0] = coord_type(cella) * a[0];
  xb[1] = coord_type(cella) * a[1];
  xb[2] = coord_type(cella) * a[2];
  coord_type xc[3];
  xc[0] = pos1[0] + xb[0];
  xc[1] = pos1[1] + xb[1];
  xc[2] = pos1[2] + xb[2];
  bond_list new_bond;
  new_bond.start[0] = (real_g)xc[0];
  new_bond.start[1] = (real_g)xc[1];
  new_bond.start[2] = (real_g)xc[2];
  new_bond.middle[0] = (real_g)(pos1[0] + xb[0] + diff[0]);
  new_bond.middle[1] = (real_g)(pos1[1] + xb[1] + diff[1]);
  new_bond.middle[2] = (real_g)(pos1[2] + xb[2] + diff[2]);
  new_bond.end[0] = (real_g)(pos2[0] + xb[0]);
  new_bond.end[1] = (real_g)(pos2[1] + xb[1]);
  new_bond.end[2] = (real_g)(pos2[2] + xb[2]);
  atom_position atm = data.node->data;
  // find requires value without map offset
  xc[0] = data.cartesian[0] + xb[0];
  xc[1] = data.cartesian[1] + xb[1];
  xc[2] = data.cartesian[2] + xb[2];
  atm.set_coords(xc);
  select_colour(new_bond.colour1, new_bond.type1,
		atm, data.colour, invert_colours, edit);
  new_bond.type2 = 0;
  info.push_back(new_bond);
}

void DLV::atom_tree_node::store_bond(std::list<bond_list> &info,
				     const bond_info data[],
				     const int_g i1, const int_g i2,
				     const coord_type pos2[3],
				     const bool inside_cell,
				     const coord_type a[3], const int_g na,
				     const real_l tol, const bool centre,
				     const bool invert_colours,
				     const real_g map_data[][3],
				     const bool edit) const
{
  coord_type pos1[3];
  for (int_g i = 0; i < 3; i++)
    pos1[i] = data[i1].cartesian[i];
  if (map_data != 0) {
    for (int_g i = 0; i < 3; i++)
      pos1[i] += real_l(map_data[data[i1].data_index][i]);
  }
  coord_type diff[3];
  real_l r1 = real_l(data[i1].radius);
  real_l r2 = real_l(data[i2].radius);
  diff[0] = (pos2[0] - pos1[0]) * r1 / (r1 + r2);
  diff[1] = (pos2[1] - pos1[1]) * r1 / (r1 + r2);
  diff[2] = (pos2[2] - pos1[2]) * r1 / (r1 + r2);
  // replicate bonds.
  int_g min_na = 0;
  int_g max_na = na - 1;
  if (centre) {
    max_na = na / 2;
    min_na = - max_na;
    if (na % 2 == 0)
      max_na--;
  }
  atom_position atom1 = data[i1].node->data;
  atom_position atom2 = data[i2].node->data;
  for (int_g i = min_na; i <= max_na; i++) {
    coord_type xa[3];
    xa[0] = real_l(i) * a[0];
    xa[1] = real_l(i) * a[1];
    xa[2] = real_l(i) * a[2];
    coord_type xd[3];
    xd[0] = data[i1].cartesian[0] + xa[0];
    xd[1] = data[i1].cartesian[1] + xa[1];
    xd[2] = data[i1].cartesian[2] + xa[2];
    atom1.set_coords(xd);
    bond_list new_bond;
    select_colour(new_bond.colour1, new_bond.type1,
		  atom1, data[i1].colour, invert_colours, edit);
    xd[0] = pos1[0] + xa[0];
    xd[1] = pos1[1] + xa[1];
    xd[2] = pos1[2] + xa[2];
    for (int_g l = 0; l < 3; l++) {
      new_bond.start[l] = real_g(xd[l]);
      new_bond.middle[l] = real_g(xd[l] + diff[l]);
    }
    if (inside_cell) {
      xd[0] = data[i2].cartesian[0] + xa[0];
      xd[1] = data[i2].cartesian[1] + xa[1];
      xd[2] = data[i2].cartesian[2] + xa[2];
      atom2.set_coords(xd);
      select_colour(new_bond.colour2, new_bond.type2,
		    atom2, data[i2].colour, invert_colours, edit);
    } else
      new_bond.type2 = 0;
    xd[0] = pos2[0] + xa[0];
    xd[1] = pos2[1] + xa[1];
    xd[2] = pos2[2] + xa[2];
    for (int_g l = 0; l < 3; l++)
      new_bond.end[l] = real_g(xd[l]);
    info.push_back(new_bond);
  }
  // edges.
  real_l min_tol = tol;
  real_l max_tol = real_l(1.0) - tol;
  real_l min_offset = na;
  real_l max_offset = -1;
  if (centre) {
    if (na % 2 == 1) {
      min_tol = real_l(-0.5) + tol;
      max_tol = real_l(0.5) - tol;
      min_offset = ((real_l)(na + 1)) / 2.0;
      max_offset = -min_offset;
    } else {
      min_offset = ((real_l)na) / 2.0;
      max_offset = -min_offset - 1.0;
    }
  }
  if (data[i1].fractional[0] < min_tol)
    edge_bond(info, data[i1], pos1, pos2, diff, min_offset,
	      a, invert_colours, edit);
  else if (data[i1].fractional[0] > max_tol)
    edge_bond(info, data[i1], pos1, pos2, diff, max_offset,
	      a, invert_colours, edit);
  if (inside_cell) {
    diff[0] = (pos1[0] - pos2[0]) * r2 / (r1 + r2);
    diff[1] = (pos1[1] - pos2[1]) * r2 / (r1 + r2);
    diff[2] = (pos1[2] - pos2[2]) * r2 / (r1 + r2);
    if (data[i2].fractional[0] < min_tol)
      edge_bond(info, data[i2], pos2, pos1, diff,
		min_offset, a, invert_colours, edit);
    else if (data[i2].fractional[0] > max_tol)
      edge_bond(info, data[i2], pos2, pos1, diff,
		max_offset, a, invert_colours, edit);
  }
}

void DLV::atom_tree_node::gen_bond_second_atom(std::list<bond_list> &info,
					       const bond_info data[],
					       const int_g index,
					       const int_g no,
					       const coord_type a[3],
					       const int_g na,
					       const bool centre,
					       const bool invert_colours,
					       const real_l tol,
					       const real_g map_data[][3],
					       const bool edit) const
{
  for (int_g node = 0; node < no; node++) {
    bool bonded;
    if (node < index)
      bonded = data[index].bonds[node];
    else
      bonded = data[node].bonds[index];
    if (bonded) {
      coord_type x[3];
      for (int_g i = 0; i < 3; i++)
	x[i] = data[node].cartesian[i];
      if (map_data != 0) {
	for (int_g i = 0; i < 3; i++)
	  x[i] += real_l(map_data[data[node].data_index][i]);
      }
      for (int_g i = data[node].min_a; i <= data[node].max_a; i++) {
	coord_type xa[3];
	xa[0] = x[0] + real_l(i) * a[0];
	xa[1] = x[1] + real_l(i) * a[1];
	xa[2] = x[2] + real_l(i) * a[2];
	if (i != 0 or node < index) {
	  if (atoms_are_bonded(data, index, node, xa, map_data)) {
	    store_bond(info, data, index, node, xa, (i == 0),
		       a, na, tol, centre, invert_colours, map_data, edit);
	  }
	}
      }
    }
  }
}

void DLV::atom_tree_node::generate_bonds(std::list<bond_list> &info,
					 const bond_info data[],
					 const int_g no, const coord_type a[3],
					 const int_g na, const bool centre,
					 const bool invert_colours,
					 const real_l tol,
					 const real_g map_data[][3],
					 const bool edit) const
{
  for (int_g i = 0; i < no; i++) {
    gen_bond_second_atom(info, data, i, no, a, na, centre,
			 invert_colours, tol, map_data, edit);
  }
}

void DLV::atom_tree_node::edge_bond(std::list<bond_list> &info,
				    const bond_info &data,
				    const coord_type pos1[3],
				    const coord_type pos2[3],
				    const coord_type diff[3],
				    const real_l cella, const real_l cellb,
				    const coord_type a[3],
				    const coord_type b[3],
				    const bool invert_colours,
				    const bool edit) const
{
  coord_type cell[3];
  cell[0] = coord_type(cella);
  cell[1] = coord_type(cellb);
  coord_type xb[3];
  xb[0] = cell[0] * a[0] + cell[1] * b[0];
  xb[1] = cell[0] * a[1] + cell[1] * b[1];
  xb[2] = cell[0] * a[2] + cell[1] * b[2];
  coord_type xc[3];
  xc[0] = pos1[0] + xb[0];
  xc[1] = pos1[1] + xb[1];
  xc[2] = pos1[2] + xb[2];
  bond_list new_bond;
  new_bond.start[0] = (real_g)xc[0];
  new_bond.start[1] = (real_g)xc[1];
  new_bond.start[2] = (real_g)xc[2];
  new_bond.middle[0] = (real_g)(pos1[0] + xb[0] + diff[0]);
  new_bond.middle[1] = (real_g)(pos1[1] + xb[1] + diff[1]);
  new_bond.middle[2] = (real_g)(pos1[2] + xb[2] + diff[2]);
  new_bond.end[0] = (real_g)(pos2[0] + xb[0]);
  new_bond.end[1] = (real_g)(pos2[1] + xb[1]);
  new_bond.end[2] = (real_g)(pos2[2] + xb[2]);
  atom_position atm = data.node->data;
  // find requires value without map offset
  xc[0] = data.cartesian[0] + xb[0];
  xc[1] = data.cartesian[1] + xb[1];
  xc[2] = data.cartesian[2] + xb[2];
  atm.set_coords(xc);
  select_colour(new_bond.colour1, new_bond.type1,
		atm, data.colour, invert_colours, edit);
  new_bond.type2 = 0;
  info.push_back(new_bond);
}

void DLV::atom_tree_node::store_bond(std::list<bond_list> &info,
				     const bond_info data[],
				     const int_g i1, const int_g i2,
				     const coord_type pos2[3],
				     const bool inside_cell,
				     const coord_type a[3],
				     const coord_type b[3],
				     const int_g na, const int_g nb,
				     const real_l tol, const bool centre,
				     const bool invert_colours,
				     const real_g map_data[][3],
				     const bool edit) const
{
  coord_type pos1[3];
  for (int_g i = 0; i < 3; i++)
    pos1[i] = data[i1].cartesian[i];
  if (map_data != 0) {
    for (int_g i = 0; i < 3; i++)
      pos1[i] += real_l(map_data[data[i1].data_index][i]);
  }
  coord_type diff[3];
  real_l r1 = real_l(data[i1].radius);
  real_l r2 = real_l(data[i2].radius);
  diff[0] = (pos2[0] - pos1[0]) * r1 / (r1 + r2);
  diff[1] = (pos2[1] - pos1[1]) * r1 / (r1 + r2);
  diff[2] = (pos2[2] - pos1[2]) * r1 / (r1 + r2);
  // replicate bonds.
  int_g min_na = 0;
  int_g max_na = na - 1;
  int_g min_nb = 0;
  int_g max_nb = nb - 1;
  if (centre) {
    max_na = na / 2;
    min_na = - max_na;
    if (na % 2 == 0)
      max_na--;
    max_nb = nb / 2;
    min_nb = - max_nb;
    if (nb % 2 == 0)
      max_nb--;
  }
  atom_position atom1 = data[i1].node->data;
  atom_position atom2 = data[i2].node->data;
  for (int_g i = min_na; i <= max_na; i++) {
    coord_type xa[3];
    xa[0] = real_l(i) * a[0];
    xa[1] = real_l(i) * a[1];
    xa[2] = real_l(i) * a[2];
    for (int_g j = min_nb; j <= max_nb; j++) {
      coord_type xb[3];
      xb[0] = xa[0] + real_l(j) * b[0];
      xb[1] = xa[1] + real_l(j) * b[1];
      xb[2] = xa[2] + real_l(j) * b[2];
      coord_type xd[3];
      xd[0] = data[i1].cartesian[0] + xb[0];
      xd[1] = data[i1].cartesian[1] + xb[1];
      xd[2] = data[i1].cartesian[2] + xb[2];
      atom1.set_coords(xd);
      bond_list new_bond;
      select_colour(new_bond.colour1, new_bond.type1,
		    atom1, data[i1].colour, invert_colours, edit);
      xd[0] = pos1[0] + xb[0];
      xd[1] = pos1[1] + xb[1];
      xd[2] = pos1[2] + xb[2];
      for (int_g l = 0; l < 3; l++) {
	new_bond.start[l] = real_g(xd[l]);
	new_bond.middle[l] = real_g(xd[l] + diff[l]);
      }
      if (inside_cell) {
	xd[0] = data[i2].cartesian[0] + xb[0];
	xd[1] = data[i2].cartesian[1] + xb[1];
	xd[2] = data[i2].cartesian[2] + xb[2];
	atom2.set_coords(xd);
	select_colour(new_bond.colour2, new_bond.type2,
		      atom2, data[i2].colour, invert_colours, edit);
      } else
	new_bond.type2 = 0;
      xd[0] = pos2[0] + xb[0];
      xd[1] = pos2[1] + xb[1];
      xd[2] = pos2[2] + xb[2];
      for (int_g l = 0; l < 3; l++)
	new_bond.end[l] = real_g(xd[l]);
      info.push_back(new_bond);
    }
  }
  // edges.
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  real_l mina_offset = na;
  real_l maxa_offset = -1;
  real_l minb_offset = nb;
  real_l maxb_offset = -1;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
      mina_offset = real_l(na + 1) / 2.0;
      maxa_offset = -mina_offset;
    } else {
      mina_offset = real_l(na) / 2.0;
      maxa_offset = -mina_offset - real_l(1.0);
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
      minb_offset = real_l(nb + 1) / 2.0;
      maxb_offset = -minb_offset;
    } else {
      minb_offset = real_l(nb) / 2.0;
      maxb_offset = -minb_offset - 1.0;
    }
  }
  if (data[i1].fractional[0] < mina_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      edge_bond(info, data[i1], pos1, pos2, diff,
		mina_offset, i, a, b,  invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol)
      edge_bond(info, data[i1], pos1, pos2, diff,
		mina_offset, minb_offset, a, b, invert_colours, edit);
    else if (data[i1].fractional[1] > maxb_tol)
      edge_bond(info, data[i1], pos1, pos2, diff,
		mina_offset, maxb_offset, a, b, invert_colours, edit);
  } else if (data[i1].fractional[0] > maxa_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      edge_bond(info, data[i1], pos1, pos2, diff,
		maxa_offset, i, a, b, invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol)
      edge_bond(info, data[i1], pos1, pos2, diff,
		maxa_offset, minb_offset, a, b, invert_colours, edit);
    else if (data[i1].fractional[1] > maxb_tol)
      edge_bond(info, data[i1], pos1, pos2, diff,
		maxa_offset, maxb_offset, a, b, invert_colours, edit);
  }
  if (data[i1].fractional[1] < minb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      edge_bond(info, data[i1], pos1, pos2, diff,
		i, minb_offset, a, b, invert_colours, edit);
  } else if (data[i1].fractional[1] > maxb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      edge_bond(info, data[i1], pos1, pos2, diff,
		i, maxb_offset, a, b, invert_colours, edit);
  }
  if (inside_cell) {
    diff[0] = (pos1[0] - pos2[0]) * r2 / (r1 + r2);
    diff[1] = (pos1[1] - pos2[1]) * r2 / (r1 + r2);
    diff[2] = (pos1[2] - pos2[2]) * r2 / (r1 + r2);
    if (data[i2].fractional[0] < mina_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  mina_offset, i, a, b, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  mina_offset, minb_offset, a, b, invert_colours, edit);
      else if (data[i2].fractional[1] > maxb_tol)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  mina_offset, maxb_offset, a, b, invert_colours, edit);
    } else if (data[i2].fractional[0] > maxa_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  maxa_offset, i, a, b, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  maxa_offset, minb_offset, a, b, invert_colours, edit);
      else if (data[i2].fractional[1] > maxb_tol)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  maxa_offset, maxb_offset, a, b, invert_colours, edit);
    }
    if (data[i2].fractional[1] < minb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i2], pos2, pos1, diff, i,
		  minb_offset, a, b, invert_colours, edit);
    } else if (data[i2].fractional[1] > maxb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i2], pos2, pos1, diff,
		  i, maxb_offset, a, b, invert_colours, edit);
    }
  }
}

void DLV::atom_tree_node::gen_bond_second_atom(std::list<bond_list> &info,
					       const bond_info data[],
					       const int_g index,
					       const int_g no,
					       const coord_type a[3],
					       const coord_type b[3],
					       const int_g na, const int_g nb,
					       const bool centre,
					       const bool invert_colours,
					       const real_l tol,
					       const real_g map_data[][3],
					       const bool edit) const
{
  for (int_g node = 0; node < no; node++) {
    bool bonded;
    if (node < index)
      bonded = data[index].bonds[node];
    else
      bonded = data[node].bonds[index];
    if (bonded) {
      coord_type x[3];
      for (int_g i = 0; i < 3; i++)
	x[i] = data[node].cartesian[i];
      if (map_data != 0) {
	for (int_g i = 0; i < 3; i++)
	  x[i] += real_l(map_data[data[node].data_index][i]);
      }
      for (int_g i = data[node].min_a; i <= data[node].max_a; i++) {
	coord_type xa[3];
	xa[0] = x[0] + real_l(i) * a[0];
	xa[1] = x[1] + real_l(i) * a[1];
	xa[2] = x[2] + real_l(i) * a[2];
	for (int_g j = data[node].min_b; j <= data[node].max_b; j++) {
	  coord_type xb[3];
	  xb[0] = xa[0] + real_l(j) * b[0];
	  xb[1] = xa[1] + real_l(j) * b[1];
	  xb[2] = xa[2] + real_l(j) * b[2];
	  if (i != 0 or j != 0 or node < index) {
	    if (atoms_are_bonded(data, index, node, xb, map_data)) {
	      store_bond(info, data, index, node, xb, (i == 0 and j == 0),
			 a, b, na, nb, tol, centre, invert_colours,
			 map_data, edit);
	    }
	  }
	}
      }
    }
  }
}

void DLV::atom_tree_node::generate_bonds(std::list<bond_list> &info,
					 const bond_info data[],
					 const int_g no, const coord_type a[3],
					 const coord_type b[3], const int_g na,
					 const int_g nb, const bool centre,
					 const bool invert_colours,
					 const real_l tol,
					 const real_g map_data[][3],
					 const bool edit) const
{
  for (int_g i = 0; i < no; i++) {
    gen_bond_second_atom(info, data, i, no, a, b, na, nb, centre,
			 invert_colours, tol, map_data, edit);
  }
}

void DLV::atom_tree_node::edge_bond(std::list<bond_list> &info,
				    const bond_info &data,
				    const coord_type pos1[3],
				    const coord_type pos2[3],
				    const coord_type diff[3],
				    const real_l cella, const real_l cellb,
				    const real_l cellc, const coord_type a[3],
				    const coord_type b[3],
				    const coord_type c[3],
				    const bool invert_colours,
				    const bool edit) const
{
  coord_type cell[3];
  cell[0] = (coord_type)cella;
  cell[1] = (coord_type)cellb;
  cell[2] = (coord_type)cellc;
  coord_type xb[3];
  xb[0] = cell[0] * a[0] + cell[1] * b[0] + cell[2] * c[0];
  xb[1] = cell[0] * a[1] + cell[1] * b[1] + cell[2] * c[1];
  xb[2] = cell[0] * a[2] + cell[1] * b[2] + cell[2] * c[2];
  coord_type xc[3];
  xc[0] = pos1[0] + xb[0];
  xc[1] = pos1[1] + xb[1];
  xc[2] = pos1[2] + xb[2];
  bond_list new_bond;
  new_bond.start[0] = (real_g)xc[0];
  new_bond.start[1] = (real_g)xc[1];
  new_bond.start[2] = (real_g)xc[2];
  new_bond.middle[0] = (real_g)(pos1[0] + xb[0] + diff[0]);
  new_bond.middle[1] = (real_g)(pos1[1] + xb[1] + diff[1]);
  new_bond.middle[2] = (real_g)(pos1[2] + xb[2] + diff[2]);
  new_bond.end[0] = (real_g)(pos2[0] + xb[0]);
  new_bond.end[1] = (real_g)(pos2[1] + xb[1]);
  new_bond.end[2] = (real_g)(pos2[2] + xb[2]);
  atom_position atm = data.node->data;
  // find requires value without map offset
  xc[0] = data.cartesian[0] + xb[0];
  xc[1] = data.cartesian[1] + xb[1];
  xc[2] = data.cartesian[2] + xb[2];
  atm.set_coords(xc);
  select_colour(new_bond.colour1, new_bond.type1,
		atm, data.colour, invert_colours, edit);
  new_bond.type2 = 0;
  info.push_back(new_bond);
}

void DLV::atom_tree_node::store_bond(std::list<bond_list> &info,
				     const bond_info data[],
				     const int_g i1, const int_g i2,
				     const coord_type pos2[3],
				     const bool inside_cell,
				     const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3], const int_g na,
				     const int_g nb, const int_g nc,
				     const real_l tol, const bool centre,
				     const bool invert_colours,
				     const real_g map_data[][3],
				     const bool edit) const
{
  coord_type pos1[3];
  for (int_g i = 0; i < 3; i++)
    pos1[i] = data[i1].cartesian[i];
  if (map_data != 0) {
    for (int_g i = 0; i < 3; i++)
      pos1[i] += real_l(map_data[data[i1].data_index][i]);
  }
  coord_type diff[3];
  real_l r1 = real_l(data[i1].radius);
  real_l r2 = real_l(data[i2].radius);
  diff[0] = (pos2[0] - pos1[0]) * r1 / (r1 + r2);
  diff[1] = (pos2[1] - pos1[1]) * r1 / (r1 + r2);
  diff[2] = (pos2[2] - pos1[2]) * r1 / (r1 + r2);
  // replicate bonds.
  int_g min_na = 0;
  int_g max_na = na - 1;
  int_g min_nb = 0;
  int_g max_nb = nb - 1;
  int_g min_nc = 0;
  int_g max_nc = nc - 1;
  if (centre) {
    max_na = na / 2;
    min_na = - max_na;
    if (na % 2 == 0)
      max_na--;
    max_nb = nb / 2;
    min_nb = - max_nb;
    if (nb % 2 == 0)
      max_nb--;
    max_nc = nc / 2;
    min_nc = - max_nc;
    if (nc % 2 == 0)
      max_nc--;
  }
  atom_position atom1 = data[i1].node->data;
  atom_position atom2 = data[i2].node->data;
  for (int_g i = min_na; i <= max_na; i++) {
    coord_type xa[3];
    xa[0] = real_l(i) * a[0];
    xa[1] = real_l(i) * a[1];
    xa[2] = real_l(i) * a[2];
    for (int_g j = min_nb; j <= max_nb; j++) {
      coord_type xb[3];
      xb[0] = xa[0] + real_l(j) * b[0];
      xb[1] = xa[1] + real_l(j) * b[1];
      xb[2] = xa[2] + real_l(j) * b[2];
      for (int_g k = min_nc; k <= max_nc; k++) {
	coord_type xc[3];
	xc[0] = xb[0] + real_l(k) * c[0];
	xc[1] = xb[1] + real_l(k) * c[1];
	xc[2] = xb[2] + real_l(k) * c[2];
	coord_type xd[3];
	xd[0] = data[i1].cartesian[0] + xc[0];
	xd[1] = data[i1].cartesian[1] + xc[1];
	xd[2] = data[i1].cartesian[2] + xc[2];
	atom1.set_coords(xd);
	bond_list new_bond;
	select_colour(new_bond.colour1, new_bond.type1,
		      atom1, data[i1].colour, invert_colours, edit);
	xd[0] = pos1[0] + xc[0];
	xd[1] = pos1[1] + xc[1];
	xd[2] = pos1[2] + xc[2];
	for (int_g l = 0; l < 3; l++) {
	  new_bond.start[l] = real_g(xd[l]);
	  new_bond.middle[l] = real_g(xd[l] + diff[l]);
	}
	if (inside_cell) {
	  xd[0] = data[i2].cartesian[0] + xc[0];
	  xd[1] = data[i2].cartesian[1] + xc[1];
	  xd[2] = data[i2].cartesian[2] + xc[2];
	  atom2.set_coords(xd);
	  select_colour(new_bond.colour2, new_bond.type2,
			atom2, data[i2].colour, invert_colours, edit);
	} else
	  new_bond.type2 = 0;
	xd[0] = pos2[0] + xc[0];
	xd[1] = pos2[1] + xc[1];
	xd[2] = pos2[2] + xc[2];
	for (int_g l = 0; l < 3; l++)
	  new_bond.end[l] = real_g(xd[l]);
	info.push_back(new_bond);
      }
    }
  }
  // edges.
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  real_l minc_tol = tol;
  real_l maxc_tol = real_l(1.0) - tol;
  real_l mina_offset = na;
  real_l maxa_offset = -1;
  real_l minb_offset = nb;
  real_l maxb_offset = -1;
  real_l minc_offset = nc;
  real_l maxc_offset = -1;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
      mina_offset = real_l(na + 1) / 2.0;
      maxa_offset = -mina_offset;
    } else {
      mina_offset = real_l(na) / 2.0;
      maxa_offset = -mina_offset - real_l(1.0);
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
      minb_offset = real_l(nb + 1) / 2.0;
      maxb_offset = -minb_offset;
    } else {
      minb_offset = real_l(nb) / 2.0;
      maxb_offset = -minb_offset - real_l(1.0);
    }
    if (nc % 2 == 1) {
      minc_tol = real_l(-0.5) + tol;
      maxc_tol = real_l(0.5) - tol;
      minc_offset = real_l(nc + 1) / 2.0;
      maxc_offset = -minc_offset;
    } else {
      minc_offset = real_l(nc) / 2.0;
      maxc_offset = -minc_offset - real_l(1.0);
    }
  }
  if (data[i1].fractional[0] < mina_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      for (int_g j = min_nc; j <= max_nc; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol) {
      for (int_g i = min_nc; i <= max_nc; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, minb_offset, i, a, b, c, invert_colours, edit);
      if (data[i1].fractional[2] < minc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, minb_offset, minc_offset, a, b, c,
		  invert_colours, edit);
    } else if (data[i1].fractional[1] > maxb_tol) {
      for (int_g i = min_nc; i <= max_nc; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, i, a, b, c, invert_colours, edit);
      if (data[i1].fractional[2] < minc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, minc_offset, a, b, c,
		  invert_colours, edit);
      else if (data[i1].fractional[2] > maxc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, maxc_offset, a, b, c,
		  invert_colours, edit);
    }
    if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, minc_offset, a, b, c, invert_colours, edit);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, maxc_offset, a, b, c, invert_colours, edit);
    }
  } else if (data[i1].fractional[0] > maxa_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      for (int_g j = min_nc; j <= max_nc; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol) {
      for (int_g i = min_nc; i <= max_nc; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, minb_offset, i, a, b, c, invert_colours, edit);
      if (data[i1].fractional[2] < minc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, minb_offset, minc_offset, a, b, c,
		  invert_colours, edit);
    } else if (data[i1].fractional[1] > maxb_tol) {
      for (int_g i = min_nc; i <= max_nc; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, i, a, b, c, invert_colours, edit);
      if (data[i1].fractional[2] < minc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, minc_offset, a, b, c,
		  invert_colours, edit);
      else if (data[i1].fractional[2] > maxc_tol)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, maxc_offset, a, b, c,
		  invert_colours, edit);
    }
    if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, minc_offset, a, b, c, invert_colours, edit);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, maxc_offset, a, b, c, invert_colours, edit);
    }
  }
  if (data[i1].fractional[1] < minb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nc; j <= max_nc; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, minc_offset, a, b, c, invert_colours, edit);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, maxc_offset, a, b, c, invert_colours, edit);
    }
  } else if (data[i1].fractional[1] > maxb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nc; j <= max_nc; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, minc_offset, a, b, c, invert_colours, edit);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, maxc_offset, a, b, c, invert_colours, edit);
    }
  }
  if (data[i1].fractional[2] < minc_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nb; j <= max_nb; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, j, minc_offset, a, b, c, invert_colours, edit);
  } else if (data[i1].fractional[2] > maxc_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nb; j <= max_nb; j++)
	edge_bond(info, data[i1], pos1, pos2, diff,
		  i, j, maxc_offset, a, b, c, invert_colours, edit);
  }
  if (inside_cell) {
    diff[0] = (pos1[0] - pos2[0]) * r2 / (r1 + r2);
    diff[1] = (pos1[1] - pos2[1]) * r2 / (r1 + r2);
    diff[2] = (pos1[2] - pos2[2]) * r2 / (r1 + r2);
    if (data[i2].fractional[0] < mina_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	for (int_g j = min_nc; j <= max_nc; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    mina_offset, i, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol) {
	for (int_g i = min_nc; i <= max_nc; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, mina_offset,
		    minb_offset, i, a, b, c, invert_colours, edit);
	if (data[i2].fractional[2] < minc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    mina_offset, minb_offset, minc_offset, a, b, c,
		    invert_colours, edit);
      } else if (data[i2].fractional[1] > maxb_tol) {
	for (int_g i = min_nc; i <= max_nc; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, mina_offset,
		    maxb_offset, i, a, b, c, invert_colours, edit);
	if (data[i2].fractional[2] < minc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    mina_offset, maxb_offset, minc_offset, a, b, c,
		    invert_colours, edit);
	else if (data[i2].fractional[2] > maxc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    mina_offset, maxb_offset, maxc_offset, a, b, c,
		    invert_colours, edit);
      }
      if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, mina_offset,
		    i, minc_offset, a, b, c, invert_colours, edit);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, mina_offset,
		    i, maxc_offset, a, b, c, invert_colours, edit );
      }
    } else if (data[i2].fractional[0] > maxa_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	for (int_g j = min_nc; j <= max_nc; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    maxa_offset, i, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol) {
	for (int_g i = min_nc; i <= max_nc; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    maxa_offset, minb_offset, i, a, b, c,
		    invert_colours, edit);
	if (data[i2].fractional[2] < minc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    maxa_offset, minb_offset, minc_offset, a, b, c,
		    invert_colours, edit);
      } else if (data[i2].fractional[1] > maxb_tol) {
	for (int_g i = min_nc; i <= max_nc; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, maxa_offset,
		    maxb_offset, i, a, b, c, invert_colours, edit);
	if (data[i2].fractional[2] < minc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    maxa_offset, maxb_offset, minc_offset, a, b, c,
		    invert_colours, edit);
	else if (data[i2].fractional[2] > maxc_tol)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    maxa_offset, maxb_offset, maxc_offset, a, b, c,
		    invert_colours, edit);
      }
      if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, maxa_offset,
		    i, minc_offset, a, b, c, invert_colours, edit);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, maxa_offset,
		    i, maxc_offset, a, b, c, invert_colours, edit);
      }
    }
    if (data[i2].fractional[1] < minb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nc; j <= max_nc; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    i, minb_offset, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, minb_offset,
		    minc_offset, a, b, c, invert_colours, edit);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, minb_offset,
		    maxc_offset, a, b, c, invert_colours, edit);
      }
    } else if (data[i2].fractional[1] > maxb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nc; j <= max_nc; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff,
		    i, maxb_offset, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, maxb_offset,
		    minc_offset, a, b, c, invert_colours, edit);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, maxb_offset,
		    maxc_offset, a, b, c, invert_colours, edit);
      }
    }
    if (data[i2].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nb; j <= max_nb; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, j,
		    minc_offset, a, b, c, invert_colours, edit);
    } else if (data[i2].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nb; j <= max_nb; j++)
	  edge_bond(info, data[i2], pos2, pos1, diff, i, j,
		    maxc_offset, a, b, c, invert_colours, edit);
    }
  }
}

void DLV::atom_tree_node::gen_bond_second_atom(std::list<bond_list> &info,
					       const bond_info data[],
					       const int_g index,
					       const int_g no,
					       const coord_type a[3],
					       const coord_type b[3],
					       const coord_type c[3],
					       const int_g na, const int_g nb,
					       const int_g nc,
					       const bool centre,
					       const bool invert_colours,
					       const real_l tol,
					       const real_g map_data[][3],
					       const bool edit) const
{
  for (int_g node = 0; node < no; node++) {
    bool bonded;
    if (node < index)
      bonded = data[index].bonds[node];
    else
      bonded = data[node].bonds[index];
    if (bonded) {
      coord_type x[3];
      for (int_g i = 0; i < 3; i++)
	x[i] = data[node].cartesian[i];
      if (map_data != 0) {
	for (int_g i = 0; i < 3; i++)
	  x[i] += real_l(map_data[data[node].data_index][i]);
      }
      for (int_g i = data[node].min_a; i <= data[node].max_a; i++) {
	coord_type xa[3];
	xa[0] = x[0] + real_l(i) * a[0];
	xa[1] = x[1] + real_l(i) * a[1];
	xa[2] = x[2] + real_l(i) * a[2];
	for (int_g j = data[node].min_b; j <= data[node].max_b; j++) {
	  coord_type xb[3];
	  xb[0] = xa[0] + real_l(j) * b[0];
	  xb[1] = xa[1] + real_l(j) * b[1];
	  xb[2] = xa[2] + real_l(j) * b[2];
	  for (int_g k = data[node].min_c; k <= data[node].max_c; k++) {
	    coord_type xc[3];
	    xc[0] = xb[0] + real_l(k) * c[0];
	    xc[1] = xb[1] + real_l(k) * c[1];
	    xc[2] = xb[2] + real_l(k) * c[2];
	    if (node < index or i != 0 or j != 0 or k != 0) {
	      if (atoms_are_bonded(data, index, node, xc, map_data)) {
		store_bond(info, data, index, node, xc,
			   (i == 0 and j == 0 and k == 0), a, b, c, na, nb, nc,
			   tol, centre, invert_colours, map_data, edit);
	      }
	    }
	  }
	}
      }
    }
  }
}

void DLV::atom_tree_node::generate_bonds(std::list<bond_list> &info,
					 const bond_info data[],
					 const int_g no, const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3], const int_g na,
					 const int_g nb, const int_g nc,
					 const bool centre,
					 const bool invert_colours,
					 const real_l tol,
					 const real_g map_data[][3],
					 const bool edit) const
{
  for (int_g i = 0; i < no; i++) {
    gen_bond_second_atom(info, data, i, no, a, b, c, na, nb, nc, centre,
			 invert_colours, tol, map_data, edit);
  }
}

void DLV::atom_tree_node::set_atom_overlaps(bond_info data[], const int_g n,
					    const real_g fla,  const int_g na,
					    const bool centre,
					    const real_l tol)
{
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
    }
  }
  for (int_g i = 0; i < n; i++) {
    if (data[i].fractional[0] < mina_tol + real_l(fla))
      data[i].max_a = 1;
    if (data[i].fractional[0] > maxa_tol - real_l(fla))
      data[i].min_a = -1;
  }
}

void DLV::atom_tree_node::set_atom_overlaps(bond_info data[], const int_g n,
					    const real_g fla, const real_g flb,
					    const int_g na, const int_g nb,
					    const bool centre,
					    const real_l tol)
{
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
    }
  }
  for (int_g i = 0; i < n; i++) {
    if (data[i].fractional[0] < mina_tol + real_l(fla))
      data[i].max_a = 1;
    if (data[i].fractional[0] > maxa_tol - real_l(fla))
      data[i].min_a = -1;
    if (data[i].fractional[1] < minb_tol + real_l(flb))
      data[i].max_b = 1;
    if (data[i].fractional[1] > maxb_tol - real_l(flb))
      data[i].min_b = -1;
  }
}

void DLV::atom_tree_node::set_atom_overlaps(bond_info data[], const int_g n,
					    const real_g fla, const real_g flb,
					    const real_g flc, const int_g na,
					    const int_g nb, const int_g nc,
					    const bool centre,
					    const real_l tol)
{
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  real_l minc_tol = tol;
  real_l maxc_tol = real_l(1.0) - tol;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
    }
    if (nc % 2 == 1) {
      minc_tol = real_l(-0.5) + tol;
      maxc_tol = real_l(0.5) - tol;
    }
  }
  for (int_g i = 0; i < n; i++) {
    if (data[i].fractional[0] < mina_tol + real_l(fla))
      data[i].max_a = 1;
    if (data[i].fractional[0] > maxa_tol - real_l(fla))
      data[i].min_a = -1;
    if (data[i].fractional[1] < minb_tol + real_l(flb))
      data[i].max_b = 1;
    if (data[i].fractional[1] > maxb_tol - real_l(flb))
      data[i].min_b = -1;
    if (data[i].fractional[2] < minc_tol + real_l(flc))
      data[i].max_c = 1;
    if (data[i].fractional[2] > maxc_tol - real_l(flc))
      data[i].min_c = -1;
  }
}

void
DLV::atom_tree::generate_bonds(std::list<bond_list> info[],
			       const int_g nframes, const real_g overlap,
			       const bool invert_colours,
			       const bond_data &bonds, const real_l tol,
			       const std::vector<real_g (*)[3]> *traj,
			       const bool edit) const
{
  if (bonds.has_bonds() and root != 0) {
    // Not sure this is ideal, but count_origin takes no real time vs get
    int_g count = 0;
    root->count_origin_cell_atoms(count);
    bond_info *origin = new_local_array1(bond_info, count);
    // linearised bond storage in origin data
    int_g nbonds = count * (count + 1) / 2;
    bool *save_bonds = new_local_array1(bool, nbonds);
    for (int_g i = 0; i < nbonds; i++)
      save_bonds[i] = false;
    int_g j = 0;
    for (int_g i = 0; i < count; i++) {
      origin[i].bonds = save_bonds + j;
      j += (i + 1);
    }
    int_g index = 0;
    real_g max_radius = 0.0;
    count = 0;
    root->get_origin_cell_atoms(origin, index, max_radius, overlap, bonds,
				count);
    if (traj == 0)
      root->generate_bonds(info[0], origin, index, invert_colours,
			   tol, 0, edit);
    else {
      for (int_g i = 0; i < nframes; i++)
	root->generate_bonds(info[i], origin, index, invert_colours,
			     tol, (*traj)[i], edit);
    }
    delete_local_array(save_bonds);
    delete_local_array(origin);
  }
}

void DLV::atom_tree::generate_bonds(const coord_type a[3], const int_g na,
				    std::list<bond_list> info[],
				    const int_g nframes, const real_g overlap,
				    const bool centre,
				    const bool invert_colours,
				    const bond_data &bonds, const real_l tol,
				    const std::vector<real_g (*)[3]> *traj,
				    const bool gamma,
				    const bool edit) const
{
  if (bonds.has_bonds() and root != 0) {
    bond_info *origin = 0;
    bool *save_bonds = 0;
    int_g cella = na;
    coord_type ca[3];
    for (int_g i = 0; i < 3; i++) {
      ca[i] = a[i];
    }
    real_g la = real_g(sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
    real_g max_radius = 0.0;
    int_g index = 0;
    int_g count = 0;
    if (gamma) {
      // Not sure this is ideal, but count_origin takes no real time vs get
      root->count_origin_cell_atoms(count);
    } else
      count = nnodes;
    origin = new_local_array1(bond_info, count);
    // linearised bond storage in origin data
    int_g nbonds = count * (count + 1) / 2;
    save_bonds = new_local_array1(bool, nbonds);
    for (int_g i = 0; i < nbonds; i++)
      save_bonds[i] = false;
    int_g j = 0;
    for (int_g i = 0; i < count; i++) {
      origin[i].bonds = save_bonds + j;
      j += (i + 1);
    }
    count = 0;
    if (gamma) {
      root->get_origin_cell_atoms(origin, index, max_radius, overlap, bonds,
				  count);
    } else {
      // non gamma point phonons
      // Not sure this is ideal
      root->get_all_cell_atoms(origin, index, max_radius, overlap, bonds,
			       count, na, 1, 1);
      for (int_g i = 0; i < 3; i++) {
	ca[i] = real_l(na) * a[i];
      }
      // work out external overlap range for external bonds
      la *= na;
      cella = 1;
    }
    // work out external overlap range for external bonds
    // use 2 * for atom on edge of cell
    // 2.0 * is to close to exact distance, use 2.5 for tolerance
    real_g fla = real_g(2.5) * max_radius / la;
    // Todo - do we need an overlap for each frame?
    root->set_atom_overlaps(origin, index, fla, cella, centre, tol);
    if (traj == 0)
      root->generate_bonds(info[0], origin, index, ca, cella,
			   centre, invert_colours, tol, 0, edit);
    else {
      for (int_g i = 0; i < nframes; i++)
	root->generate_bonds(info[i], origin, index, ca, cella,
			     centre, invert_colours, tol,
			     (*traj)[i], edit);
    }
    delete_local_array(save_bonds);
    delete_local_array(origin);
  }
}

void DLV::atom_tree::generate_bonds(const coord_type a[3],
				    const coord_type b[3],
				    const int_g na, const int_g nb,
				    std::list<bond_list> info[],
				    const int_g nframes, const real_g overlap,
				    const bool centre,
				    const bool invert_colours,
				    const bond_data &bonds, const real_l tol,
				    const std::vector<real_g (*)[3]> *traj,
				    const bool gamma, const bool edit) const
{
  if (bonds.has_bonds() and root != 0) {
    bond_info *origin = 0;
    bool *save_bonds = 0;
    int_g cella = na;
    int_g cellb = nb;
    coord_type ca[3];
    coord_type cb[3];
    for (int_g i = 0; i < 3; i++) {
      ca[i] = a[i];
      cb[i] = b[i];
    }
    real_g la = real_g(sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
    real_g lb = real_g(sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]));
    real_g max_radius = 0.0;
    int_g index = 0;
    int_g count = 0;
    if (gamma) {
      // Not sure this is ideal, but count_origin takes no real time vs get
      root->count_origin_cell_atoms(count);
    } else
      count = nnodes;
    origin = new_local_array1(bond_info, count);
    // linearised bond storage in origin data
    int_g nbonds = count * (count + 1) / 2;
    save_bonds = new_local_array1(bool, nbonds);
    for (int_g i = 0; i < nbonds; i++)
      save_bonds[i] = false;
    int_g j = 0;
    for (int_g i = 0; i < count; i++) {
      origin[i].bonds = save_bonds + j;
      j += (i + 1);
    }
    count = 0;
    if (gamma) {
      root->get_origin_cell_atoms(origin, index, max_radius, overlap, bonds,
				  count);
    } else {
      // non gamma point phonons
      // Not sure this is ideal
      root->get_all_cell_atoms(origin, index, max_radius, overlap, bonds,
			       count, na, nb, 1);
      for (int_g i = 0; i < 3; i++) {
	ca[i] = real_l(na) * a[i];
	cb[i] = real_l(nb) * b[i];
      }
      // work out external overlap range for external bonds
      la *= na;
      lb *= nb;
      cella = 1;
      cellb = 1;
    }
    // work out external overlap range for external bonds
    // use 2 * for atom on edge of cell
    // 2.0 * is to close to exact distance, use 2.5 for tolerance
    real_g fla = real_g(2.5) * max_radius / la;
    real_g flb = real_g(2.5) * max_radius / lb;
    // Todo - do we need an overlap for each frame?
    root->set_atom_overlaps(origin, index, fla, flb, cella, cellb,
			    centre, tol);
    if (traj == 0)
      root->generate_bonds(info[0], origin, index, ca, cb, cella, cellb,
			   centre, invert_colours, tol, 0, edit);
    else {
      for (int_g i = 0; i < nframes; i++)
	root->generate_bonds(info[i], origin, index, ca, cb, cella,
			     cellb, centre, invert_colours, tol,
			     (*traj)[i], edit);
    }
    delete_local_array(save_bonds);
    delete_local_array(origin);
  }
}

void DLV::atom_tree::generate_bonds(const coord_type a[3],
				    const coord_type b[3],
				    const coord_type c[3], const int_g na,
				    const int_g nb, const int_g nc,
				    std::list<bond_list> info[],
				    const int_g nframes, const real_g overlap,
				    const bool centre,
				    const bool invert_colours,
				    const bond_data &bonds, const real_l tol,
				    const std::vector<real_g (*)[3]> *traj,
				    const bool gamma, const bool edit) const
{
  if (bonds.has_bonds() and root != 0) {
    bond_info *origin = 0;
    bool *save_bonds = 0;
    int_g cella = na;
    int_g cellb = nb;
    int_g cellc = nc;
    coord_type ca[3];
    coord_type cb[3];
    coord_type cc[3];
    for (int_g i = 0; i < 3; i++) {
      ca[i] = a[i];
      cb[i] = b[i];
      cc[i] = c[i];
    }
    real_g la = real_g(sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
    real_g lb = real_g(sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]));
    real_g lc = real_g(sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]));
    real_g max_radius = 0.0;
    int_g index = 0;
    int_g count = 0;
    if (gamma) {
      // Not sure this is ideal, but count_origin takes no real time vs get
      root->count_origin_cell_atoms(count);
    } else
      count = nnodes;
    origin = new_local_array1(bond_info, count);
    // linearised bond storage in origin data
    int_g nbonds = count * (count + 1) / 2;
    save_bonds = new_local_array1(bool, nbonds);
    for (int_g i = 0; i < nbonds; i++)
      save_bonds[i] = false;
    int_g j = 0;
    for (int_g i = 0; i < count; i++) {
      origin[i].bonds = save_bonds + j;
      j += (i + 1);
    }
    count = 0;
    if (gamma) {
      root->get_origin_cell_atoms(origin, index, max_radius, overlap, bonds,
				  count);
    } else {
      // non gamma point phonons
      // Not sure this is ideal
      root->get_all_cell_atoms(origin, index, max_radius, overlap, bonds,
			       count, na, nb, nc);
      for (int_g i = 0; i < 3; i++) {
	ca[i] = real_l(na) * a[i];
	cb[i] = real_l(nb) * b[i];
	cc[i] = real_l(nc) * c[i];
      }
      // work out external overlap range for external bonds
      la *= na;
      lb *= nb;
      lc *= nc;
      cella = 1;
      cellb = 1;
      cellc = 1;
    }
    // work out external overlap range for external bonds
    // use 2 * for atom on edge of cell
    // 2.0 * is to close to exact distance, use 2.5 for tolerance
    real_g fla = real_g(2.5) * max_radius / la;
    real_g flb = real_g(2.5) * max_radius / lb;
    real_g flc = real_g(2.5) * max_radius / lc;
    // Todo - do we need an overlap for each frame?
    root->set_atom_overlaps(origin, index, fla, flb, flc, cella, cellb, cellc,
			    centre, tol);
    if (traj == 0)
      root->generate_bonds(info[0], origin, index, ca, cb, cc, cella, cellb,
			   cellc, centre, invert_colours, tol, 0, edit);
    else {
      for (int_g i = 0; i < nframes; i++)
	root->generate_bonds(info[i], origin, index, ca, cb, cc, cella,
			     cellb, cellc, centre, invert_colours, tol,
			     (*traj)[i], edit);
    }
    delete_local_array(save_bonds);
    delete_local_array(origin);
  }
}

void DLV::atom_tree_node::edge_surf(std::list<bond_list> &info,
				    const bond_info &data,
				    const coord_type pos1[3],
				    const coord_type pos2[3],
				    const coord_type diff[3],
				    const real_l cella, const real_l cellb,
				    const real_l cellc, const coord_type a[3],
				    const coord_type b[3],
				    const coord_type c[3],
				    const bool invert_colours,
				    const bool edit) const
{
  coord_type cell[3];
  cell[0] = (coord_type)cella;
  cell[1] = (coord_type)cellb;
  cell[2] = (coord_type)cellc;
  coord_type xb[3];
  xb[0] = cell[0] * a[0] + cell[1] * b[0] + cell[2] * c[0];
  xb[1] = cell[0] * a[1] + cell[1] * b[1] + cell[2] * c[1];
  xb[2] = cell[0] * a[2] + cell[1] * b[2] + cell[2] * c[2];
  coord_type xc[3];
  xc[0] = pos1[0] + xb[0];
  xc[1] = pos1[1] + xb[1];
  xc[2] = pos1[2] + xb[2];
  bond_list new_bond;
  new_bond.start[0] = (real_g)xc[0];
  new_bond.start[1] = (real_g)xc[1];
  new_bond.start[2] = (real_g)xc[2];
  new_bond.middle[0] = (real_g)(pos1[0] + xb[0] + diff[0]);
  new_bond.middle[1] = (real_g)(pos1[1] + xb[1] + diff[1]);
  new_bond.middle[2] = (real_g)(pos1[2] + xb[2] + diff[2]);
  new_bond.end[0] = (real_g)(pos2[0] + xb[0]);
  new_bond.end[1] = (real_g)(pos2[1] + xb[1]);
  new_bond.end[2] = (real_g)(pos2[2] + xb[2]);
  atom_position atm = data.node->data;
  // find requires value without map offset
  xc[0] = data.cartesian[0] + xb[0];
  xc[1] = data.cartesian[1] + xb[1];
  xc[2] = data.cartesian[2] + xb[2];
  atm.set_coords(xc);
  select_colour(new_bond.colour1, new_bond.type1,
		atm, data.colour, invert_colours, edit);
  new_bond.type2 = 0;
  info.push_back(new_bond);
}

void DLV::atom_tree_node::store_surf(std::list<bond_list> &info,
				     const bond_info data[],
				     const int_g i1, const int_g i2,
				     const coord_type pos2[3],
				     const bool inside_cell,
				     const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3], const int_g na,
				     const int_g nb, const int_g nc,
				     const real_l tol, const bool centre,
				     const bool invert_colours,
				     const real_g map_data[][3],
				     const bool edit) const
{
  coord_type pos1[3];
  for (int_g i = 0; i < 3; i++)
    pos1[i] = data[i1].cartesian[i];
  if (map_data != 0) {
    for (int_g i = 0; i < 3; i++)
      pos1[i] += real_l(map_data[data[i1].data_index][i]);
  }
  coord_type diff[3];
  real_l r1 = real_l(data[i1].radius);
  real_l r2 = real_l(data[i2].radius);
  diff[0] = (pos2[0] - pos1[0]) * r1 / (r1 + r2);
  diff[1] = (pos2[1] - pos1[1]) * r1 / (r1 + r2);
  diff[2] = (pos2[2] - pos1[2]) * r1 / (r1 + r2);
  // replicate bonds.
  int_g min_na = 0;
  int_g max_na = na - 1;
  int_g min_nb = 0;
  int_g max_nb = nb - 1;
  int_g min_nc = -nc + 1;
  int_g max_nc = 0;
  if (centre) {
    max_na = na / 2;
    min_na = - max_na;
    if (na % 2 == 0)
      max_na--;
    max_nb = nb / 2;
    min_nb = - max_nb;
    if (nb % 2 == 0)
      max_nb--;
  }
  atom_position atom1 = data[i1].node->data;
  atom_position atom2 = data[i2].node->data;
  for (int_g i = min_na; i <= max_na; i++) {
    coord_type xa[3];
    xa[0] = real_l(i) * a[0];
    xa[1] = real_l(i) * a[1];
    xa[2] = real_l(i) * a[2];
    for (int_g j = min_nb; j <= max_nb; j++) {
      coord_type xb[3];
      xb[0] = xa[0] + real_l(j) * b[0];
      xb[1] = xa[1] + real_l(j) * b[1];
      xb[2] = xa[2] + real_l(j) * b[2];
      if (data[i1].node->get_atom()->has_special_periodicity()
	  and data[i2].node->get_atom()->has_special_periodicity()) {
	for (int_g k = min_nc; k <= max_nc; k++) {
	  coord_type xc[3];
	  xc[0] = xb[0] + real_l(k) * c[0];
	  xc[1] = xb[1] + real_l(k) * c[1];
	  xc[2] = xb[2] + real_l(k) * c[2];
	  coord_type xd[3];
	  xd[0] = data[i1].cartesian[0] + xc[0];
	  xd[1] = data[i1].cartesian[1] + xc[1];
	  xd[2] = data[i1].cartesian[2] + xc[2];
	  atom1.set_coords(xd);
	  bond_list new_bond;
	  select_colour(new_bond.colour1, new_bond.type1,
			atom1, data[i1].colour, invert_colours, edit);
	  xd[0] = pos1[0] + xc[0];
	  xd[1] = pos1[1] + xc[1];
	  xd[2] = pos1[2] + xc[2];
	  for (int_g l = 0; l < 3; l++) {
	    new_bond.start[l] = (real_g)xd[l];
	    new_bond.middle[l] = (real_g)(xd[l] + diff[l]);
	  }
	  if (inside_cell) {
	    xd[0] = data[i2].cartesian[0] + xc[0];
	    xd[1] = data[i2].cartesian[1] + xc[1];
	    xd[2] = data[i2].cartesian[2] + xc[2];
	    atom2.set_coords(xd);
	    select_colour(new_bond.colour2, new_bond.type2,
			  atom2, data[i2].colour, invert_colours, edit);
	  } else
	    new_bond.type2 = 0;
	  xd[0] = pos2[0] + xc[0];
	  xd[1] = pos2[1] + xc[1];
	  xd[2] = pos2[2] + xc[2];
	  for (int_g l = 0; l < 3; l++)
	    new_bond.end[l] = (real_g)xd[l];
	  info.push_back(new_bond);
	}
      } else {
	coord_type xd[3];
	xd[0] = data[i1].cartesian[0] + xb[0];
	xd[1] = data[i1].cartesian[1] + xb[1];
	xd[2] = data[i1].cartesian[2] + xb[2];
	atom1.set_coords(xd);
	bond_list new_bond;
	select_colour(new_bond.colour1, new_bond.type1,
		      atom1, data[i1].colour, invert_colours, edit);
	xd[0] = pos1[0] + xb[0];
	xd[1] = pos1[1] + xb[1];
	xd[2] = pos1[2] + xb[2];
	for (int_g l = 0; l < 3; l++) {
	  new_bond.start[l] = (real_g)xd[l];
	  new_bond.middle[l] = (real_g)(xd[l] + diff[l]);
	}
	if (inside_cell) {
	  xd[0] = data[i2].cartesian[0] + xb[0];
	  xd[1] = data[i2].cartesian[1] + xb[1];
	  xd[2] = data[i2].cartesian[2] + xb[2];
	  atom2.set_coords(xd);
	  select_colour(new_bond.colour2, new_bond.type2,
			atom2, data[i2].colour, invert_colours, edit);
	} else
	  new_bond.type2 = 0;
	xd[0] = pos2[0] + xb[0];
	xd[1] = pos2[1] + xb[1];
	xd[2] = pos2[2] + xb[2];
	for (int_g l = 0; l < 3; l++)
	  new_bond.end[l] = (real_g)xd[l];
	info.push_back(new_bond);
      }
    }
  }
  // edges.
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  //real_l minc_tol = tol;
  //real_l maxc_tol = 1.0 - tol;
  real_l mina_offset = na;
  real_l maxa_offset = -1;
  real_l minb_offset = nb;
  real_l maxb_offset = -1;
  //real_l minc_offset = nc;
  //real_l maxc_offset = -1;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
      mina_offset = real_l(na + 1) / 2.0;
      maxa_offset = -mina_offset;
    } else {
      mina_offset = real_l(na) / 2.0;
      maxa_offset = -mina_offset - real_l(1.0);
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
      minb_offset = real_l(nb + 1) / 2.0;
      maxb_offset = -minb_offset;
    } else {
      minb_offset = real_l(nb) / 2.0;
      maxb_offset = -minb_offset - real_l(1.0);
    }
  }
  int_g m_nc = min_nc;
  if (!data[i1].node->get_atom()->has_special_periodicity())
    m_nc = 0;
  if (data[i1].fractional[0] < mina_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      for (int_g j = m_nc; j <= max_nc; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol) {
      for (int_g i = m_nc; i <= max_nc; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, minb_offset, i, a, b, c, invert_colours, edit);
      /*if (data[i1].fractional[2] < minc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, minb_offset, minc_offset, a, b, c,
		  invert_colours);*/
    } else if (data[i1].fractional[1] > maxb_tol) {
      for (int_g i = m_nc; i <= max_nc; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, i, a, b, c, invert_colours, edit);
      /*if (data[i1].fractional[2] < minc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, minc_offset, a, b, c,
		  invert_colours);
      else if (data[i1].fractional[2] > maxc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, maxb_offset, maxc_offset, a, b, c,
		  invert_colours);*/
    }
    /*if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, minc_offset, a, b, c, invert_colours);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  mina_offset, i, maxc_offset, a, b, c, invert_colours);
		  }*/
  } else if (data[i1].fractional[0] > maxa_tol) {
    for (int_g i = min_nb; i <= max_nb; i++)
      for (int_g j = m_nc; j <= max_nc; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, j, a, b, c, invert_colours, edit);
    if (data[i1].fractional[1] < minb_tol) {
      for (int_g i = m_nc; i <= max_nc; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, minb_offset, i, a, b, c, invert_colours, edit);
      /*if (data[i1].fractional[2] < minc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, minb_offset, minc_offset, a, b, c,
		  invert_colours);*/
    } else if (data[i1].fractional[1] > maxb_tol) {
      for (int_g i = m_nc; i <= max_nc; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, i, a, b, c, invert_colours, edit);
      /*if (data[i1].fractional[2] < minc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, minc_offset, a, b, c,
		  invert_colours);
      else if (data[i1].fractional[2] > maxc_tol)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, maxb_offset, maxc_offset, a, b, c,
		  invert_colours);*/
    }
    /*if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, minc_offset, a, b, c, invert_colours);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  maxa_offset, i, maxc_offset, a, b, c, invert_colours);
		  }*/
  }
  if (data[i1].fractional[1] < minb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = m_nc; j <= max_nc; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, j, a, b, c, invert_colours, edit);
    /*if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, minc_offset, a, b, c, invert_colours);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, minb_offset, maxc_offset, a, b, c, invert_colours);
		  }*/
  } else if (data[i1].fractional[1] > maxb_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = m_nc; j <= max_nc; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, j, a, b, c, invert_colours, edit);
    /*if (data[i1].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, minc_offset, a, b, c, invert_colours);
    } else if (data[i1].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, maxb_offset, maxc_offset, a, b, c, invert_colours);
		  }*/
  }
  /*if (data[i1].fractional[2] < minc_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nb; j <= max_nb; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, j, minc_offset, a, b, c, invert_colours);
  } else if (data[i1].fractional[2] > maxc_tol) {
    for (int_g i = min_na; i <= max_na; i++)
      for (int_g j = min_nb; j <= max_nb; j++)
	edge_surf(info, data[i1], pos1, pos2, diff,
		  i, j, maxc_offset, a, b, c, invert_colours);
		  }*/
  if (inside_cell) {
    diff[0] = (pos1[0] - pos2[0]) * r2 / (r1 + r2);
    diff[1] = (pos1[1] - pos2[1]) * r2 / (r1 + r2);
    diff[2] = (pos1[2] - pos2[2]) * r2 / (r1 + r2);
    m_nc = min_nc;
    if (!data[i2].node->get_atom()->has_special_periodicity())
      m_nc = 0;
    if (data[i2].fractional[0] < mina_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	for (int_g j = m_nc; j <= max_nc; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, i, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol) {
	for (int_g i = m_nc; i <= max_nc; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff, mina_offset,
		    minb_offset, i, a, b, c, invert_colours, edit);
	/*if (data[i2].fractional[2] < minc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, minb_offset, minc_offset, a, b, c,
		    invert_colours);*/
      } else if (data[i2].fractional[1] > maxb_tol) {
	for (int_g i = m_nc; i <= max_nc; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff, mina_offset,
		    maxb_offset, i, a, b, c, invert_colours, edit);
	/*if (data[i2].fractional[2] < minc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, maxb_offset, minc_offset, a, b, c,
		    invert_colours);
	else if (data[i2].fractional[2] > maxc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, maxb_offset, maxc_offset, a, b, c,
		    invert_colours);*/
      }
      /*if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, i, minc_offset, a, b, c, invert_colours);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    mina_offset, i, maxc_offset, a, b, c, invert_colours);
		    }*/
    } else if (data[i2].fractional[0] > maxa_tol) {
      for (int_g i = min_nb; i <= max_nb; i++)
	for (int_g j = m_nc; j <= max_nc; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, i, j, a, b, c, invert_colours, edit);
      if (data[i2].fractional[1] < minb_tol) {
	for (int_g i = m_nc; i <= max_nc; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, minb_offset, i, a, b, c,
		    invert_colours, edit);
	/*if (data[i2].fractional[2] < minc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, minb_offset, minc_offset, a, b, c,
		    invert_colours);*/
      } else if (data[i2].fractional[1] > maxb_tol) {
	for (int_g i = m_nc; i <= max_nc; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff, maxa_offset,
		    maxb_offset, i, a, b, c, invert_colours, edit);
	/*if (data[i2].fractional[2] < minc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, maxb_offset, minc_offset, a, b, c,
		    invert_colours);
	else if (data[i2].fractional[2] > maxc_tol)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, maxb_offset, maxc_offset, a, b, c,
		    invert_colours);*/
      }
      /*if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, i, minc_offset, a, b, c, invert_colours);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_nb; i <= max_nb; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    maxa_offset, i, maxc_offset, a, b, c, invert_colours);
		    }*/
    }
    if (data[i2].fractional[1] < minb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = m_nc; j <= max_nc; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, minb_offset, j, a, b, c, invert_colours, edit);
      /*if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, minb_offset, minc_offset, a, b, c, invert_colours);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, minb_offset, maxc_offset, a, b, c, invert_colours);
		    }*/
    } else if (data[i2].fractional[1] > maxb_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = m_nc; j <= max_nc; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, maxb_offset, j, a, b, c, invert_colours, edit);
      /*if (data[i2].fractional[2] < minc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, maxb_offset, minc_offset, a, b, c, invert_colours);
      } else if (data[i2].fractional[2] > maxc_tol) {
	for (int_g i = min_na; i <= max_na; i++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, maxb_offset, maxc_offset, a, b, c, invert_colours);
		    }*/
    }
    /*if (data[i2].fractional[2] < minc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nb; j <= max_nb; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, j, minc_offset, a, b, c, invert_colours);
    } else if (data[i2].fractional[2] > maxc_tol) {
      for (int_g i = min_na; i <= max_na; i++)
	for (int_g j = min_nb; j <= max_nb; j++)
	  edge_surf(info, data[i2], pos2, pos1, diff,
		    i, j, maxc_offset, a, b, c, invert_colours);
		    }*/
  }
}

void
DLV::atom_tree_node::surf_bond_second_atom(std::list<bond_list> &info,
					   const bond_info data[],
					   const int_g index, const int_g no,
					   const coord_type a[3],
					   const coord_type b[3],
					   const coord_type c[3],
					   const int_g na, const int_g nb,
					   const int_g nc, const bool centre,
					   const bool invert_colours,
					   const real_l tol,
					   const real_g map_data[][3],
					   const bool edit) const
{
  for (int_g node = 0; node < no; node++) {
    coord_type x[3];
    for (int_g i = 0; i < 3; i++)
      x[i] = data[node].cartesian[i];
    if (map_data != 0) {
      for (int_g i = 0; i < 3; i++)
	x[i] += real_l(map_data[data[node].data_index][i]);
    }
    for (int_g i = data[node].min_a; i <= data[node].max_a; i++) {
      coord_type xa[3];
      xa[0] = x[0] + real_l(i) * a[0];
      xa[1] = x[1] + real_l(i) * a[1];
      xa[2] = x[2] + real_l(i) * a[2];
      for (int_g j = data[node].min_b; j <= data[node].max_b; j++) {
	coord_type xb[3];
	xb[0] = xa[0] + real_l(j) * b[0];
	xb[1] = xa[1] + real_l(j) * b[1];
	xb[2] = xa[2] + real_l(j) * b[2];
	for (int_g k = data[node].min_c; k <= data[node].max_c; k++) {
	  coord_type xc[3];
	  xc[0] = xb[0] + real_l(k) * c[0];
	  xc[1] = xb[1] + real_l(k) * c[1];
	  xc[2] = xb[2] + real_l(k) * c[2];
	  if (i != 0 or j != 0 or k != 0 or node < index) {
	    bool bonded;
	    if (node < index)
	      bonded = data[index].bonds[node];
	    else
	      bonded = data[node].bonds[index];
	    if (bonded) {
	      if (atoms_are_bonded(data, index, node, xc, map_data)) {
		store_surf(info, data, index, node, xc,
			   (i == 0 and j == 0 and k == 0), a, b, c, na, nb, nc,
			   tol, centre, invert_colours, map_data, edit);
	      }
	    }
	  }
	}
      }
    }
  }
}

void
DLV::atom_tree_node::set_surface_overlaps(bond_info data[], const int_g n,
					  const real_g fla, const real_g flb,
					  const real_g flc, const int_g na,
					  const int_g nb, const int_g nc,
					  const bool centre, const real_l tol)
{
  real_l mina_tol = tol;
  real_l maxa_tol = real_l(1.0) - tol;
  real_l minb_tol = tol;
  real_l maxb_tol = real_l(1.0) - tol;
  real_l minc_tol = tol;
  real_l maxc_tol = real_l(1.0) - tol;
  if (centre) {
    if (na % 2 == 1) {
      mina_tol = real_l(-0.5) + tol;
      maxa_tol = real_l(0.5) - tol;
    }
    if (nb % 2 == 1) {
      minb_tol = real_l(-0.5) + tol;
      maxb_tol = real_l(0.5) - tol;
    }
    if (nc % 2 == 1) {
      minc_tol = real_l(-0.5) + tol;
      maxc_tol = real_l(0.5) - tol;
    }
  }
  for (int_g i = 0; i < n; i++) {
    if (data[i].fractional[0] < mina_tol + real_l(fla))
      data[i].max_a = 1;
    if (data[i].fractional[0] > maxa_tol - real_l(fla))
      data[i].min_a = -1;
    if (data[i].fractional[1] < minb_tol + real_l(flb))
      data[i].max_b = 1;
    if (data[i].fractional[1] > maxb_tol - real_l(flb))
      data[i].min_b = -1;
    if (data[i].node->get_atom()->has_special_periodicity()) {
      if (data[i].fractional[2] < minc_tol + real_l(flc))
	data[i].max_c = 1;
      if (data[i].fractional[2] > maxc_tol - real_l(flc))
	data[i].min_c = -1;
    } else {
      data[i].max_c = 0;
      data[i].min_c = 0;
    }
  }
}

void DLV::atom_tree_node::surface_bonds(std::list<bond_list> &info,
					const bond_info data[],
					const int_g no, const coord_type a[3],
					const coord_type b[3],
					const coord_type c[3], const int_g na,
					const int_g nb, const int_g nc,
					const bool centre,
					const bool invert_colours,
					const real_l tol,
					const real_g map_data[][3],
					const bool edit) const
{
  for (int_g i = 0; i < no; i++) {
    surf_bond_second_atom(info, data, i, no, a, b, c, na, nb, nc, centre,
			  invert_colours, tol, map_data, edit);
  }
}

void DLV::atom_tree::surface_bonds(const coord_type a[3],
				   const coord_type b[3],
				   const coord_type c[3], const int_g na,
				   const int_g nb, const int_g nc,
				   std::list<bond_list> info[],
				   const int_g nframes, const real_g overlap,
				   const bool centre,
				   const bool invert_colours,
				   const bond_data &bonds, const real_l tol,
				   const std::vector<real_g (*)[3]> *traj,
				   const bool gamma, const bool edit) const
{
  if (bonds.has_bonds() and root != 0) {
    if (gamma) {
      // Not sure this is ideal, but count_origin takes no real time vs get
      int_g count = 0;
      root->count_origin_cell_atoms(count);
      bond_info *origin = new_local_array1(bond_info, count);
      // linearised bond storage in origin data
      int_g nbonds = count * (count + 1) / 2;
      bool *save_bonds = new_local_array1(bool, nbonds);
      for (int_g i = 0; i < nbonds; i++)
	save_bonds[i] = false;
      int_g j = 0;
      for (int_g i = 0; i < count; i++) {
	origin[i].bonds = save_bonds + j;
	j += (i + 1);
      }
      int_g index = 0;
      real_g max_radius = 0.0;
      count = 0;
      root->get_origin_cell_atoms(origin, index, max_radius, overlap, bonds,
				  count);
      // work out external overlap range for external bonds
      // we can have very odd shaped cells so use whole cell
      //real_g la = (real_g)std::sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
      //real_g lb = (real_g)std::sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
      //real_g lc = (real_g)std::sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
      // use 2 * for atom on edge of cell
      //real_g fla = 2.0f * max_radius / la;
      //real_g flb = 2.0f * max_radius / lb;
      //real_g flc = 2.0f * max_radius / lc;
      real_g fla = 1.0;
      real_g flb = 1.0;
      real_g flc = 1.0;
      // Todo - do we need an overlap for each frame?
      root->set_surface_overlaps(origin, index, fla, flb, flc, na, nb, nc,
				 centre, tol);
      if (traj == 0)
	root->surface_bonds(info[0], origin, index, a, b, c, na, nb, nc,
			    centre, invert_colours, tol, 0, edit);
      else {
	for (int_g i = 0; i < nframes; i++)
	  root->surface_bonds(info[i], origin, index, a, b, c, na, nb, nc,
			      centre, invert_colours, tol, (*traj)[i], edit);
      }
      delete_local_array(save_bonds);
      delete_local_array(origin);
    } else {
      // Todo - non gamma point phonons
    }
  }
}

DLV::atom_tree_node *DLV::atom_tree_node::select(const real_g origin[3],
						 const real_g offset,
						 const bool active,
						 char message[],
						 const int_g mlen)
{
  coord_type coords[3];
  data.get_cartesian_coords(coords);
  real_g distance = 
    (real_g(coords[0]) - origin[0]) * (real_g(coords[0]) - origin[0])
    + (real_g(coords[1]) - origin[1]) * (real_g(coords[1]) - origin[1])
    + (real_g(coords[2]) - origin[2]) * (real_g(coords[2]) -origin[2]);
  const real_g tol = 0.001f;
  atom_tree_node *selection = 0;
  if (distance < offset) {
    if (selected == is_selected)
      selected = dragging_off;
    else if (selected == not_selected)
      selected = dragging_on;
    if (distance < tol)
      selection = this;
  } else if (selected == dragging_on)
    selected = not_selected;
  else if (selected == dragging_off)
    selected = is_selected;
  if (!active) {
    if (selected == dragging_off)
      selected = not_selected;
    else if (selected == dragging_on)
      selected = is_selected;
  }
  atom_tree_node *node;
  if (left_child != 0) {
    node = left_child->select(origin, offset, active, message, mlen);
    if (node != 0)
      selection = node;
  }
  if (right_child != 0) {
    node = right_child->select(origin, offset, active, message, mlen);
    if (node != 0)
      selection = node;
  }
  return selection;
}

DLV::selection_type
DLV::atom_tree_node::search_inequivalent_atoms(const atom_position &p) const
{
  selection_type s = not_selected;
  if (data.same_inequivalent(p)) {
    if (selected == is_selected)
      return is_selected;
    else if (selected == dragging_on)
      s = dragging_on;
  }
  selection_type l = not_selected;
  if (left_child != 0)
    l = left_child->search_inequivalent_atoms(p);
  if (l == is_selected)
    return l;
  else if (l == dragging_on)
    s = l;
  if (right_child != 0)
    l = right_child->search_inequivalent_atoms(p);
  if (l == is_selected)
    return l;
  else if (l == dragging_on)
    s = l;
  return s;
}

DLV::selection_type
DLV::atom_tree_node::search_parents(const atom_tree_node *p) const
{
  selection_type s = not_selected;
  if (parent == p or (parent == 0 and p == this)) {
    if (selected == is_selected)
      return is_selected;
    else if (selected == dragging_on)
      s = dragging_on;
  }
  selection_type l = not_selected;
  if (left_child != 0)
    l = left_child->search_parents(p);
  if (l == is_selected)
    return l;
  else if (l == dragging_on)
    s = l;
  if (right_child != 0)
    l = right_child->search_parents(p);
  if (l == is_selected)
    return l;
  else if (l == dragging_on)
    s = l;
  return s;
}

void DLV::atom_tree_node::select_copies(const int_g symmetry_select,
					const atom_tree_node *ptr)
{
  if (selected != is_selected and selected != dragging_on and
      selected != dragging_off) {
    selection_type s = not_selected;
    switch (symmetry_select) {
    case 0:
      s = ptr->search_inequivalent_atoms(data);
      if (s == is_selected)
	selected = symmetry_copy;
      else if (s == dragging_on)
	selected = symmetry_drag;
      else
	selected = not_selected;
      break;
    case 1:
      if (parent == 0)
	s = ptr->search_parents(this);
      else
	s = ptr->search_parents(parent);
      if (s == is_selected)
	selected = trans_copy;
      else if (s == dragging_on)
	selected = trans_drag;
      else
	selected = not_selected;
      break;
    default:
      selected = not_selected;
      break;
    }
  }
  if (left_child != 0)
    left_child->select_copies(symmetry_select, ptr);
  if (right_child != 0)
    right_child->select_copies(symmetry_select, ptr);
}

//Todo  - tidy up
void DLV::atom_tree::select(const real_g origin[3], const real_g offset,
			    const bool active, const int_g /*symmetry_select*/,
			    const bool /*select_groups*/, char message[],
			    const int_g mlen)
{
  if (root == 0)
    strncpy(message, "No atoms to select", mlen);
  else {
    atom_tree_node *node = root->select(origin, offset, active, message, mlen);
    if (!active) {
      if (node->atom_is_selected()) {
	selections.push_front(node);
	//select6 = select5;
	//select5 = select4;
	//select4 = select3;
	//select3 = select2;
	//select2 = select1;
	//select1 = node;
      } else {
	// User clicked on it again and deselected it.
	/*if (node == select1) {
	  select1 = select2;
	  select2 = select3;
	  select3 = select4;
	  select4 = select5;
	  select5 = select6;
	} else if (node == select2) {
	  select2 = select3;
	  select3 = select4;
	  select4 = select5;
	  select5 = select6;
	} else if (node == select3) {
	  select3 = select4;
	  select4 = select5;
	  select5 = select6;
	} else if (node == select4) {
	  select4 = select5;
	  select5 = select6;
	} else if (node == select5)
	  select5 = select6;
	  select6 = 0;*/
	std::list<atom_tree_node *>::iterator ptr;
	for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
	  if (*ptr == node) {
	    selections.erase(ptr);
	    break;
	  }
	}
      }
    }
    //root->select_copies(symmetry_select, select_groups, root);
  }
}

bool DLV::atom_tree_node::set_atom_selection(const atom_position &atm,
					     const selection_type s)
{
  bool found = false;
  if (data.compare(atm) == 0) {
    selected = s;
    found = true;
  } else {
    if (!found) {
      if (left_child != 0)
	found = left_child->set_atom_selection(atm, s);
      if (!found)
	if (right_child != 0)
	  found = right_child->set_atom_selection(atm, s);
    }
  }
  return found;
}

bool DLV::atom_tree_node::set_translated_selection(const atom_position &atm,
						   const int_g dim)
{
  bool found = false;
  if (data.compare_translation(atm, dim)) {
    selected = is_selected;
    found = true;
  } else {
    if (!found) {
      if (left_child != 0)
	found = left_child->set_translated_selection(atm, dim);
      if (!found)
	if (right_child != 0)
	  found = right_child->set_translated_selection(atm, dim);
    }
  }
  return found;
}

void DLV::atom_tree_node::copy_selected_atoms(atom_tree_node *node,
					      const int_g symmetry_select,
					      const bool force,
					      const int_g dim) const
{
  selection_type s = selected;
  //bool copy = false;
  if (force and atom_is_selected())
    s = is_selected;
  /*else if (dim > 0) {
    if ((symmetry_select == 1 and atom_is_selected_trans()) or
	(symmetry_select == 0 and atom_is_selected_sym()))
      copy = true;
      }*/
  if (!node->set_atom_selection(data, s)) {
    if (dim > 0 and selected == is_selected)
      node->set_translated_selection(data, dim);
  }
  if (left_child != 0)
    left_child->copy_selected_atoms(node, symmetry_select, force, dim);
  if (right_child != 0)
    right_child->copy_selected_atoms(node, symmetry_select, force, dim);
}

bool DLV::atom_tree_node::update_selection_list(const atom_position &atm,
						std::list<atom_tree_node *> &s)
{
  bool found = false;
  if (data.compare(atm) == 0) {
    s.push_back(this);
    found = true;
  } else {
    if (left_child != 0)
      found = left_child->update_selection_list(atm, s);
    if (!found)
      if (right_child != 0)
	found = right_child->update_selection_list(atm, s);
  }
  return found;
}

DLV::selection_type
DLV::atom_tree_node::search_symmetry_groups(const string group,
					    const string label) const
{
  if (atom_is_selected()) {
    string name = get_atom()->get_group(group);
    if (name == label)
      return selected;
  }
  selection_type s = not_selected;
  if (left_child != 0)
    s = left_child->search_symmetry_groups(group, label);
  if (s == not_selected)
    if (right_child != 0)
      s = right_child->search_symmetry_groups(group, label);
  return s;
}

DLV::selection_type
DLV::atom_tree_node::search_parent_groups(const atom_tree_node *p,
					  const string group,
					  const string label) const
{
  if (atom_is_selected()) {
    if (parent == p or (parent == 0 and p == this))
      return selected;
    else if (parent == 0) {
      string name = get_atom()->get_group(group);
      if (name == label)
	return selected;
    }
  }
  selection_type s = not_selected;
  if (left_child != 0)
    s = left_child->search_parent_groups(p, group, label);
  if (s == not_selected)
    if (right_child != 0)
      s = right_child->search_parent_groups(p, group, label);
  return s;
}

DLV::selection_type
DLV::atom_tree_node::search_cell(const atom_tree_node *p, const string group,
				 const string label) const
{
  if (atom_is_selected()) {
    if (data.is_same_shift(p->data)) {
      string name = get_atom()->get_group(group);
      if (name == label)
	return selected;
    }
  }
  selection_type s = not_selected;
  if (left_child != 0)
    s = left_child->search_cell(p, group, label);
  if (s == not_selected)
    if (right_child != 0)
      s = right_child->search_cell(p, group, label);
  return s;
}

void DLV::atom_tree_node::select_groups(const int_g symmetry_select,
					const string group,
					const atom_tree_node *ptr)
{
  if (selected == not_selected) {
    selection_type s = not_selected;
    string label = get_atom()->get_group(group);
    if (label != "") {
      switch (symmetry_select) {
      case 0:
	s = ptr->search_symmetry_groups(group, label);
	if (s == is_selected or s == group_copy)
	  selected = group_copy;
	else if (s == dragging_on or s == group_drag)
	  selected = group_drag;
	else if (s == symmetry_copy or s == group_sym_copy)
	  selected = group_sym_copy;
	else if (s == symmetry_drag or s == group_sym_drag)
	  s = group_sym_drag;
	else
	  selected = not_selected;
	break;
      case 1:
	// parent == 0 is in base cell, others point to base cell its a copy of
	if (parent == 0)
	  s = ptr->search_parent_groups(this, group, label);
	else
	  s = ptr->search_parent_groups(parent, group, label);
	if (s == is_selected or s == group_copy)
	  selected = group_copy;
	else if (s == dragging_on or s == group_drag)
	  selected = group_drag;
	else if (s == trans_copy or s == group_trans_copy)
	  selected = group_trans_copy;
	else if (s == trans_drag or s == group_trans_drag)
	  s = group_trans_drag;
	else
	  selected = not_selected;
	break;
      default:
	// Need to find atoms in same cell that are members of the group
	s = ptr->search_cell(this, group, label);
	if (s == is_selected or s == group_copy)
	  selected = group_copy;
	else if (s == dragging_on or s == group_drag)
	  selected = group_drag;
	else
	  selected = not_selected;
	break;
      }
    }
  }
  if (left_child != 0)
    left_child->select_groups(symmetry_select, group, ptr);
  if (right_child != 0)
    right_child->select_groups(symmetry_select, group, ptr);
}

void DLV::atom_tree::copy_selected_atoms(atom_tree &tree,
					 const int_g symmetry_select,
					 const string group,
					 const bool force,
					 const int_g dim) const
{
  if (root != 0 and tree.root != 0 and selections.size() > 0) {
    root->copy_selected_atoms(tree.root, symmetry_select, force, dim);
    // copy selection data
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr )
      (void) tree.root->update_selection_list((*ptr)->get_data(),
					      tree.selections);
    tree.root->select_copies(symmetry_select, tree.root);
    if (group != "" and group != "Atoms")
      tree.root->select_groups(symmetry_select, group, tree.root);
  }
}

void DLV::atom_tree::update_selected_atoms(const int_g symmetry_select,
					   const string group)
{
  if (root != 0 and selections.size() > 0) {
    root->select_copies(symmetry_select, root);
    if (group != "" and group != "Atoms")
      root->select_groups(symmetry_select, group, root);
  }
}

void DLV::atom_tree_node::deselect()
{
  selected = not_selected;
  if (left_child != 0)
    left_child->deselect();
  if (right_child != 0)
    right_child->deselect();
}

void DLV::atom_tree::deselect(char message[], const int_g mlen)
{
  if (root == 0)
    strncpy(message, "No atoms to deselect", mlen);
  else {
    root->deselect();
    selections.clear();
    //select6 = 0;
    //select5 = 0;
    //select4 = 0;
    //select3 = 0;
    //select2 = 0;
    //select1 = 0;
  }
}

bool DLV::atom_tree::get_selected_pair(atom &atom1, atom &atom2) const
{
  if (selections.size() < 2)
    return false;
  else {
    std::list<atom_tree_node *>::const_iterator ptr = selections.begin();
    //atom1 = select1->get_atom();
    //atom2 = select2->get_atom();
    atom1 = (*ptr)->get_atom();
    ++ptr;
    atom2 = (*ptr)->get_atom();
    return true;
  }
}

void DLV::atom_tree_node::copy(atom_tree &tree) const
{
  tree.insert(data, base_cell);
  if (left_child != 0)
    left_child->copy(tree);
  if (right_child != 0)
    right_child->copy(tree);
}

void DLV::atom_tree_node::centre(atom_tree &tree, const real_l matrix[3][3],
				 const int_g dim, const bool shift[3]) const
{
  atom_position pos = data;
  pos.centre(matrix, dim, shift);
  tree.insert(pos, base_cell);
  if (left_child != 0)
    left_child->centre(tree, matrix, dim, shift);
  if (right_child != 0)
    right_child->centre(tree, matrix, dim, shift);
}

void DLV::atom_tree::copy(atom_tree &tree) const
{
  if (nnodes > 0)
    root->copy(tree);
}

void DLV::atom_tree::copy_and_centre(atom_tree &tree,
				     const real_l matrix[3][3], const int_g dim,
				     const bool shift[3]) const
{
  if (nnodes > 0)
    root->centre(tree, matrix, dim, shift);
}

void DLV::atom_tree_node::get_atom_types(int_g atom_types[], const int_g n,
					 int_g &i) const
{
  if (left_child != 0)
    left_child->get_atom_types(atom_types, n, i);
  if (i < n) {
    atom_types[i] = data.get_atomic_number();
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_atom_types(atom_types, n, i);
}

void DLV::atom_tree::get_atom_types(int_g atom_types[], const int_g n) const
{
  int_g i = 0;
  root->get_atom_types(atom_types, n, i);
}

void DLV::atom_tree_node::get_atom_regions(const char group[], int_g regions[],
					   const int_g n, int_g &i) const
{
  if (left_child != 0)
    left_child->get_atom_regions(group, regions, n, i);
  if (i < n) {
    char buff[64];
    if (sscanf(data.get_atom()->get_group(group).c_str(),
	       "%s %d", buff, &regions[i]) != 2)
      regions[i] = 0;
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_atom_regions(group, regions, n, i);
}

void DLV::atom_tree::get_atom_regions(const char group[], int_g regions[],
				      const int_g n) const
{
  int_g i = 0;
  root->get_atom_regions(group, regions, n, i);
}

void DLV::atom_tree_node::get_asym_indices(int_g indices[], const int_g n,
					   int_g &i,
				      const std::map<int, atom> &basis) const
{
  if (left_child != 0)
    left_child->get_asym_indices(indices, n, i, basis);
  if (i < n) {
    indices[i] = data.get_atom()->get_id();
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_asym_indices(indices, n, i, basis);
}

void DLV::atom_tree::get_asym_indices(int_g indices[], const int_g n,
				      const std::map<int, atom> &basis) const
{
  int_g i = 0;
  root->get_asym_indices(indices, n, i, basis);
}

void DLV::atom_tree_node::get_cartesian_coords(coord_type coords[][3],
					       const int_g n, int_g &i) const
{
  if (left_child != 0)
    left_child->get_cartesian_coords(coords, n, i);
  if (i < n) {
    coord_type c[3];
    data.get_cartesian_coords(c);
    coords[i][0] = c[0];
    coords[i][1] = c[1];
    coords[i][2] = c[2];
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_cartesian_coords(coords, n, i);
}

void DLV::atom_tree::get_cartesian_coords(coord_type coords[][3],
					  const int_g n) const
{
  int_g i = 0;
  root->get_cartesian_coords(coords, n, i);
}

void DLV::atom_tree_node::get_cartesian_coords(coord_type coords[][3],
					       bool p[], const int_g n,
					       int_g &i) const
{
  if (left_child != 0)
    left_child->get_cartesian_coords(coords, p, n, i);
  if (i < n) {
    coord_type c[3];
    data.get_cartesian_coords(c);
    coords[i][0] = c[0];
    coords[i][1] = c[1];
    coords[i][2] = c[2];
    p[i] = data.get_atom()->has_special_periodicity();
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_cartesian_coords(coords, p, n, i);
}

void DLV::atom_tree::get_cartesian_coords(coord_type coords[][3],
					  const int_g n, bool p[]) const
{
  int_g i = 0;
  root->get_cartesian_coords(coords, p, n, i);
}

void DLV::atom_tree_node::get_fractional_coords(coord_type coords[][3],
					       const int_g n, int_g &i) const
{
  if (left_child != 0)
    left_child->get_fractional_coords(coords, n, i);
  if (i < n) {
    coord_type c[3];
    data.get_fractional_coords(c);
    coords[i][0] = c[0];
    coords[i][1] = c[1];
    coords[i][2] = c[2];
    i++;
  }
  if (i < n and right_child != 0)
    right_child->get_fractional_coords(coords, n, i);
}

void DLV::atom_tree::get_fractional_coords(coord_type coords[][3],
					   const int_g n) const
{
  int_g i = 0;
  root->get_fractional_coords(coords, n, i);
}

DLV::int_g
DLV::atom_tree::get_atom_selection_info(real_l &x, real_l &y, real_l &z,
					real_l &length, real_l &x2,
					real_l &y2, real_l &z2,
					real_l &angle, char sym1[],
					char sym2[], char sym3[],
					real_g &r1, real_g &r2,
					real_g &r3, const string gp,
					string &label) const
{
  x = 0.0;
  y = 0.0;
  z = 0.0;
  length = 0.0;
  x2 = 0.0;
  y2 = 0.0;
  z2 = 0.0;
  angle = 0.0;
  sym1[0] = '\0';
  sym2[0] = '\0';
  sym3[0] = '\0';
  r1 = 0.0;
  r2 = 0.0;
  r3 = 0.0;
  if (selections.size() < 1)
    return 0;
  else {
    std::list<atom_tree_node *>::const_iterator ptr = selections.begin();
    const atom_position p = (*ptr)->get_data();
    coord_type coords[3];
    p.get_cartesian_coords(coords);
    x = coords[0];
    y = coords[1];
    z = coords[2];
    r1 = p.get_radius();
    int_g atn = p.get_atomic_number();
    string symbol = atom_type::get_atomic_symbol(atn);
    strcpy(sym1, symbol.c_str());
    label = p.get_atom()->get_group(gp);
    if (selections.size() < 2)
      return 1;
    else {
      ++ptr;
      const atom_position p2 = (*ptr)->get_data();
      coord_type c2[3];
      p2.get_cartesian_coords(c2);
      x2 = c2[0];
      y2 = c2[1];
      z2 = c2[2];
      real_l diff1[3];
      for (int_g i = 0; i < 3; i++)
	diff1[i] = coords[i] - c2[i];
      length = sqrt(diff1[0] * diff1[0] + diff1[1] * diff1[1]
		    + diff1[2] * diff1[2]);
      r2 = p2.get_radius();
      atn = p2.get_atomic_number();
      symbol = atom_type::get_atomic_symbol(atn);
      strcpy(sym2, symbol.c_str());
      if (selections.size() < 3)
	return 2;
      else {
	++ptr;
	const atom_position p3 = (*ptr)->get_data();
	coord_type c3[3];
	p3.get_cartesian_coords(c3);
	real_l diff2[3];
	for (int_g i = 0; i < 3; i++)
	  diff2[i] = c3[i] - c2[i];
	real_l cb = sqrt(diff2[0] * diff2[0] + diff2[1] * diff2[1]
			 + diff2[2] * diff2[2]);
	real_l ctheta = (diff1[0] * diff2[0] + diff1[1] * diff2[1]
			 + diff1[2] * diff2[2]) / (length * cb);
	angle = to_degrees(acos(ctheta));
	r3 = p3.get_radius();
	atn = p3.get_atomic_number();
	symbol = atom_type::get_atomic_symbol(atn);
	strcpy(sym3, symbol.c_str());
	return 3;
      }
    }
  }
}

bool DLV::atom_tree::get_selected_positions(coord_type p[3]) const
{
  if (selections.size() < 1)
    return false;
  else {
    const atom_position d1 = selections.front()->get_data();
    coord_type c[3];
    d1.get_cartesian_coords(c);
    p[0] = c[0];
    p[1] = c[1];
    p[2] = c[2];
  }
  return true;
}

bool DLV::atom_tree::get_selected_positions(coord_type p1[3],
					    coord_type p2[3]) const
{
  if (selections.size() < 2)
    return false;
  else {
    std::list<atom_tree_node *>::const_iterator ptr = selections.begin();
    const atom_position d1 = (*ptr)->get_data();
    coord_type c[3];
    d1.get_cartesian_coords(c);
    p1[0] = c[0];
    p1[1] = c[1];
    p1[2] = c[2];
    ++ptr;
    const atom_position d2 = (*ptr)->get_data();
    d2.get_cartesian_coords(c);
    p2[0] = c[0];
    p2[1] = c[1];
    p2[2] = c[2];
  }
  return true;
}

bool DLV::atom_tree::get_selected_positions(coord_type p1[3], coord_type p2[3],
					    coord_type p3[3]) const
{
  if (selections.size() < 3)
    return false;
  else {
    std::list<atom_tree_node *>::const_iterator ptr = selections.begin();
    const atom_position d1 = (*ptr)->get_data();
    coord_type c[3];
    d1.get_cartesian_coords(c);
    p1[0] = c[0];
    p1[1] = c[1];
    p1[2] = c[2];
    ++ptr;
    const atom_position d2 = (*ptr)->get_data();
    d2.get_cartesian_coords(c);
    p2[0] = c[0];
    p2[1] = c[1];
    p2[2] = c[2];
    ++ptr;
    const atom_position d3 = (*ptr)->get_data();
    d3.get_cartesian_coords(c);
    p3[0] = c[0];
    p3[1] = c[1];
    p3[2] = c[2];
  }
  return true;
}

bool DLV::atom_tree::get_average_selection_position(real_l p[3]) const
{
  p[0] = 0.0;
  p[1] = 0.0;
  p[2] = 0.0;
  size_t n = selections.size();
  bool ok = false;
  if (n > 0) {
    ok = true;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      const atom_position p1 = (*ptr)->get_data();
      coord_type c[3];
      p1.get_cartesian_coords(c);
      p[0] += c[0];
      p[1] += c[1];
      p[2] += c[2];
    }
    p[0] /= (coord_type)n;
    p[1] /= (coord_type)n;
    p[2] /= (coord_type)n;
  }
  return ok;
}

DLV::int_g DLV::atom_position::locate_coord(const real_g coords[][3],
					    coord_type offset[3], const int_g n,
					    const real_l inverse[3][3],
					    const int_g dim) const
{
  // Like set_coords, but we're using the primitive lattice, so can't
  // rely on the fractional_coords.
  coord_type frac[3] = { 0.0, 0.0, 0.0 };
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      frac[i] += inverse[i][j] * cartesian_coords[j];
  for (int_g i = dim; i < 3; i++)
    frac[i] = cartesian_coords[i];
  // adjust coords into cell 0
  for (int_g i = 0; i < dim; i++) {
    offset[i] = - truncate(frac[i]);
    frac[i] += offset[i];
    if (frac[i] < - frac_tol) {
      frac[i] += 1.0;
      offset[i] += 1.0;
    }
  }
  int_g j;
  for (j = 0; j < n; j++) {
    bool ok = true;
    for (int_g i = 0; (i < dim and ok); i++)
      ok = abs(frac[i] - real_l(coords[j][i])) < frac_tol;
    for (int_g i = dim; (i < 3 and ok); i++)
      ok = abs(frac[i] - real_l(coords[j][i])) < cart_tol;
    if (ok)
      break;
  }
  //if (j == n) {
  //  fprintf(stderr, "Ooops - didn't locate atom\n");
  //}
  return j;
}

void DLV::atom_tree_node::map_data(const real_g grid[][3], int_g **const elem,
				   const int_g ndata, const int_g ngrid,
				   real_g (*map_grid)[3], int_g **map_data,
				   int_g &count, const real_l inverse[3][3],
				   const int_g dim) const
{
  if (left_child != 0)
    left_child->map_data(grid, elem, ndata, ngrid,
			 map_grid, map_data, count, inverse, dim);
  coord_type c[3];
  data.get_cartesian_coords(c);
  map_grid[count][0] = (real_g)c[0];
  map_grid[count][1] = (real_g)c[1];
  map_grid[count][2] = (real_g)c[2];
  coord_type offset[3];
  int_g i = data.locate_coord(grid, offset, ngrid, inverse, dim);
  for (int_g j = 0; j < ndata; j++)
    map_data[j][count] = elem[j][i];
  count++;
  if (right_child != 0)
    right_child->map_data(grid, elem, ndata, ngrid,
			  map_grid, map_data, count, inverse, dim);
}

void DLV::atom_tree::std_coords(real_g coords[][3], int_g shifts[][3],
				const int_g n, const real_l inverse[3][3],
				const int_g dim)
{
  for (int_g k = 0; k < n; k++) {
    // convert to fractional coords
    real_l frac[3] = { 0.0, 0.0, 0.0 };
    for (int_g i = 0; i < dim; i++)
      for (int_g j = 0; j < dim; j++)
	frac[i] += inverse[i][j] * real_l(coords[k][j]);
    // adjust coords into cell 0
    int_g offset[3];
    for (int_g i = 0; i < dim; i++) {
      offset[i] = - to_integer(truncate(frac[i]));
      frac[i] += offset[i];
      if (frac[i] < - frac_tol) {
	frac[i] += 1.0;
	offset[i] += 1;
      } else if (frac[i] > 1.0 - frac_tol) {
	frac[i] -= 1.0;
	offset[i] -= 1;
      }
    }
    for (int_g i = 0; i < dim; i++)
      coords[k][i] = real_g(frac[i]);
    if (shifts != 0) {
      for (int_g i = 0; i < dim; i++)
	shifts[k][i] = offset[i];
      for (int_g i = dim; i < 3; i++)
	shifts[k][i] = 0;
    }
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], int_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], int_g **map_data,
			      int_g) const
{
  // molecule coords aren't replicated and don't need conversion
  int_g count = 0;
  real_l inverse[3][3];
  root->map_data(grid, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 0);
}

void DLV::atom_tree::map_data(const real_g grid[][3], int_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], int_g **map_data,
			      int_g, const coord_type a[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = 0.0;
    cell[i][2] = 0.0;
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 1);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 1);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 1);
  delete_local_array(coords);
}

void DLV::atom_tree::map_data(const real_g grid[][3], int_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], int_g **map_data,
			      int_g, const coord_type a[3],
			      const coord_type b[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = 0.0;
  }
  cell[2][2] = 1.0;
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 2);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 2);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 2);
  delete_local_array(coords);
}

void DLV::atom_tree::map_data(const real_g grid[][3], int_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], int_g **map_data,
			      int_g, const coord_type a[3],
			      const coord_type b[3],
			      const coord_type c[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 3);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 3);
  delete_local_array(coords);
}

void DLV::atom_tree_node::map_data(const real_g grid[][3], real_g **const elem,
				   const int_g ndata, const int_g ngrid,
				   real_g (*map_grid)[3], real_g **map_data,
				   int_g &count, const real_l inverse[3][3],
				   const int_g dim) const
{
  if (left_child != 0)
    left_child->map_data(grid, elem, ndata, ngrid,
			 map_grid, map_data, count, inverse, dim);
  coord_type c[3];
  data.get_cartesian_coords(c);
  map_grid[count][0] = (real_g)c[0];
  map_grid[count][1] = (real_g)c[1];
  map_grid[count][2] = (real_g)c[2];
  coord_type offset[3];
  int_g i = data.locate_coord(grid, offset, ngrid, inverse, dim);
  for (int_g j = 0; j < ndata; j++)
    map_data[j][count] = elem[j][i];
  count++;
  if (right_child != 0)
    right_child->map_data(grid, elem, ndata, ngrid,
			  map_grid, map_data, count, inverse, dim);
}

void DLV::atom_tree::map_data(const real_g grid[][3], real_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], real_g **map_data,
			      int_g) const
{
  // molecule coords aren't replicated and don't need conversion
  int_g count = 0;
  real_l inverse[3][3];
  root->map_data(grid, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 0);
}

void DLV::atom_tree::map_data(const real_g grid[][3], real_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], real_g **map_data,
			      int_g, const coord_type a[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = 0.0;
    cell[i][2] = 0.0;
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 1);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 1);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 1);
  delete_local_array(coords);
}

void DLV::atom_tree::map_data(const real_g grid[][3], real_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], real_g **map_data,
			      int_g, const coord_type a[3],
			      const coord_type b[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = 0.0;
  }
  cell[2][2] = 1.0;
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 2);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 2);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 2);
  delete_local_array(coords);
}

void DLV::atom_tree::map_data(const real_g grid[][3], real_g **const data,
			      const int_g ndata, const int_g ngrid,
			      real_g (*map_grid)[3], real_g **map_data,
			      int_g, const coord_type a[3],
			      const coord_type b[3],
			      const coord_type c[3]) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
  for (int_g i = 0; i < ngrid; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
  // Todo - do it once in data_object and save?
  std_coords(coords, 0, ngrid, inverse, 3);
  int_g count = 0;
  root->map_data(coords, data, ndata, ngrid, map_grid, map_data, count,
		 inverse, 3);
  delete_local_array(coords);
}

void DLV::atom_tree_node::map_data(const real_g grid[][3],
				   const int_g shifts[][3],
				   const real_g mag[][3],
				   const real_g phases[][3], const int_g ngrid,
				   real_g map_grid[][3], real_g map_data[][3],
				   int_g &count, const real_g ka,
				   const real_g kb, const real_g kc,
				   const real_l inv[3][3],
				   const int_g dim) const
{
  if (left_child != 0)
    left_child->map_data(grid, shifts, mag, phases, ngrid, map_grid, map_data,
			 count, ka, kb, kc, inv, dim);
  coord_type c[3];
  data.get_cartesian_coords(c);
  coord_type offset[3];
  int_g i = data.locate_coord(grid, offset, ngrid, inv, dim);
  if (i < ngrid) {
    // Due to fragments it might not be found
    // Grid was shifted to the std coord by shift, this atom was shifted to
    // the std coord by offset, total shift to the atom position of the grid
    // is shift - offset
    map_grid[count][0] = (real_g)c[0];
    map_grid[count][1] = (real_g)c[1];
    map_grid[count][2] = (real_g)c[2];
    if (dim > 0) {
      real_g phase = (ka * real_g(real_l(shifts[i][0]) - offset[0])
		      + kb * real_g(real_l(shifts[i][1]) - offset[1])
		      + kc * real_g(real_l(shifts[i][2]) - offset[2]));
      phase *= real_g(2.0 * pi);
      if (phases == 0) {
	map_data[count][0] = cos(phase) * mag[i][0];
	map_data[count][1] = cos(phase) * mag[i][1];
	map_data[count][2] = cos(phase) * mag[i][2];
      } else {
	map_data[count][0] = cos(phase - phases[i][0]) * mag[i][0];
	map_data[count][1] = cos(phase - phases[i][1]) * mag[i][1];
	map_data[count][2] = cos(phase - phases[i][2]) * mag[i][2];
      }
    } else {
      if (phases == 0) {
	map_data[count][0] = mag[i][0];
	map_data[count][1] = mag[i][1];
	map_data[count][2] = mag[i][2];
      } else {
	map_data[count][0] = cos(- phases[i][0]) * mag[i][0];
	map_data[count][1] = cos(- phases[i][1]) * mag[i][1];
	map_data[count][2] = cos(- phases[i][2]) * mag[i][2];
      }
    }
    count++;
  }
  if (right_child != 0)
    right_child->map_data(grid, shifts, mag, phases, ngrid, map_grid, map_data,
			  count, ka, kb, kc, inv, dim);
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mag[][3],
			      const real_g phases[][3], const int_g ngrid,
			      real_g (* &map_grid)[3], real_g (* &map_data)[3],
			      int_g &n) const
{
  n = 0;
  if (nnodes > 0) {
    real_l inverse[3][3];
    // these are bigger than necessary for a fragment, but safe
    map_grid = new real_g[nnodes][3];
    map_data = new real_g[nnodes][3];
    root->map_data(grid, 0, mag, phases, ngrid, map_grid,
		   map_data, n, 0.0, 0.0, 0.0, inverse, 0);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mag[][3],
			      const real_g phases[][3], const int_g ngrid,
			      real_g (* &map_grid)[3], real_g (* &map_data)[3],
			      int_g &n, const real_g ka,
			      const coord_type a[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = 0.0;
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 1);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 1);
    // these are bigger than necessary for a fragment, but safe
    map_grid = new real_g[nnodes][3];
    map_data = new real_g[nnodes][3];
    root->map_data(coords, shifts, mag, phases, ngrid, map_grid,
		   map_data, n, ka, 0.0, 0.0, inverse, 1);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mag[][3],
			      const real_g phases[][3], const int_g ngrid,
			      real_g (* &map_grid)[3], real_g (* &map_data)[3],
			      int_g &n, const real_g ka, const real_g kb,
			      const coord_type a[3],
			      const coord_type b[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    cell[2][2] = 1.0;
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 2);
    // these are bigger than necessary for a fragment, but safe
    map_grid = new real_g[nnodes][3];
    map_data = new real_g[nnodes][3];
    root->map_data(coords, shifts, mag, phases, ngrid, map_grid,
		   map_data, n, ka, kb, 0.0, inverse, 2);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mag[][3],
			      const real_g phases[][3], const int_g ngrid,
			      real_g (* &map_grid)[3], real_g (* &map_data)[3],
			      int_g &n, const real_g ka, const real_g kb,
			      const real_g kc, const coord_type a[3],
			      const coord_type b[3],
			      const coord_type c[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 3);
    // these are bigger than necessary for a fragment, but safe
    map_grid = new real_g[nnodes][3];
    map_data = new real_g[nnodes][3];
    root->map_data(coords, shifts, mag, phases, ngrid, map_grid,
		   map_data, n, ka, kb, kc, inverse, 3);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

// Todo - is there duplication between this and the previous map routines
void DLV::atom_tree_node::map_data(const real_g grid[][3],
				   const int_g shifts[][3],
				   const real_g mag[][3],
				   const real_g phases[][3], const int_g ngrid,
				   std::vector<real_g (*)[3]> &map_data,
				   int_g &count, const real_g scale,
				   const int_g nframes, const int_g ncopies,
				   const real_g ka, const real_g kb,
				   const real_g kc, const real_l inv[3][3],
				   const int_g dim) const
{
  // Needs to unpack coords in same order as fill_array
  coord_type c[3];
  data.get_cartesian_coords(c);
  coord_type offset[3];
  int_g i = data.locate_coord(grid, offset, ngrid, inv, dim);
  if (i < ngrid) {
    // Due to fragments it might not be found
    // Grid was shifted to the std coord by shift, this atom was shifted to
    // the std coord by offset, total shift to the atom position of the grid
    // is shift - offset
    int_g frames = nframes / ncopies;
    real_g step = real_g(2.0 * pi / (frames + 1));
    real_g base = real_g(- pi / 2.0);
    real_g phase = 0.0f;
    if (dim > 0) {
      phase = (ka * real_g(real_l(shifts[i][0]) - offset[0])
	       + kb * real_g(real_l(shifts[i][1]) - offset[1])
	       + kc * real_g(real_l(shifts[i][2]) - offset[2]));
      phase *= real_g(2.0 * pi);
    }
    for (int_g j = 0; j < nframes; j++) {
      real_g inc = (real_g(j) * step) + base;
      if (dim > 0)
	inc += phase;
      if (phases == 0) {
	map_data[j][count][0] += cos(inc) * scale * mag[i][0];
	map_data[j][count][1] += cos(inc) * scale * mag[i][1];
	map_data[j][count][2] += cos(inc) * scale * mag[i][2];
      } else {
	map_data[j][count][0] += cos(inc - phases[i][0]) * scale * mag[i][0];
	map_data[j][count][1] += cos(inc - phases[i][1]) * scale * mag[i][1];
	map_data[j][count][2] += cos(inc - phases[i][2]) * scale * mag[i][2];
      }
    }
  }/* else {
    for (int_g j = 0; j < nframes; j++) {
      map_data[j][count][0] = 0.0;
      map_data[j][count][1] = 0.0;
      map_data[j][count][2] = 0.0;
    }
    }*/
  count++;
  if (left_child != 0)
    left_child->map_data(grid, shifts, mag, phases, ngrid, map_data, count,
			 scale, nframes, ncopies, ka, kb, kc, inv, dim);
  if (right_child != 0)
    right_child->map_data(grid, shifts, mag, phases, ngrid, map_data, count,
			  scale, nframes, ncopies, ka, kb, kc, inv, dim);
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mags[][3],
			      const real_g phases[][3], const int_g ngrid,
			      std::vector<real_g (*)[3]> &new_data, int_g &n,
			      const real_g scale, const int_g nframes,
			      const int_g ncopies) const
{
  n = 0;
  if (nnodes > 0) {
    real_l inverse[3][3];
    root->map_data(grid, 0, mags, phases, ngrid, new_data,
		   n, scale, nframes, ncopies, 0.0, 0.0, 0.0, inverse, 0);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mags[][3],
			      const real_g phases[][3], const int_g ngrid,
			      std::vector<real_g (*)[3]> &new_data, int_g &n,
			      const real_g scale, const int_g nframes,
			      const int_g ncopies, const real_g ka,
			      const coord_type a[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = 0.0;
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 1);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 1);
    root->map_data(coords, shifts, mags, phases, ngrid, new_data,
		   n, scale, nframes, ncopies, ka, 0.0, 0.0, inverse, 1);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mags[][3],
			      const real_g phases[][3], const int_g ngrid,
			      std::vector<real_g (*)[3]> &new_data, int_g &n,
			      const real_g scale, const int_g nframes,
			      const int_g ncopies, const real_g ka,
			      const real_g kb, const coord_type a[3],
			      const coord_type b[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    cell[2][2] = 1.0;
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 2);
    root->map_data(coords, shifts, mags, phases, ngrid, new_data,
		   n, scale, nframes, ncopies, ka, kb, 0.0, inverse, 2);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const real_g mags[][3],
			      const real_g phases[][3], const int_g ngrid,
			      std::vector<real_g (*)[3]> &new_data, int_g &n,
			      const real_g scale, const int_g nframes,
			      const int_g ncopies, const real_g ka,
			      const real_g kb, const real_g kc,
			      const coord_type a[3], const coord_type b[3],
			      const coord_type c[3]) const
{
  n = 0;
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 3);
    root->map_data(coords, shifts, mags, phases, ngrid, new_data,
		   n, scale, nframes, ncopies, ka, kb, kc, inverse, 3);
    delete_local_array(shifts);
    delete_local_array(coords);
  }
}

// Todo - is there duplication between this and the previous map routines
void DLV::atom_tree_node::map_data(const real_g grid[][3], const int_g ngrid,
				   const std::vector<real_g (*)[3]> &traj,
				   std::vector<real_g (*)[3]> &map_data,
				   int_g &count, const int_g nframes,
				   const real_l inv[3][3],
				   const int_g dim) const
{
  // Needs to unpack coords in same order as fill_array
  coord_type c[3];
  data.get_cartesian_coords(c);
  coord_type offset[3];
  int_g i = data.locate_coord(grid, offset, ngrid, inv, dim);
  if (i < ngrid) {
    for (int_g j = 0; j < nframes; j++) {
      map_data[j][count][0] = traj[j][i][0];
      map_data[j][count][1] = traj[j][i][1];
      map_data[j][count][2] = traj[j][i][2];
    }
  } // else error?
  count++;
  if (left_child != 0)
    left_child->map_data(grid, ngrid, traj, map_data, count, nframes,
			 inv, dim);
  if (right_child != 0)
    right_child->map_data(grid, ngrid, traj, map_data, count,
			  nframes, inv, dim);
}

void DLV::atom_tree::map_data(const real_g grid[][3], const int_g ngrid,
			      const std::vector<real_g (*)[3]> &traj,
			      std::vector<real_g (*)[3]> &new_data,
			      const int_g nframes) const
{
  if (nnodes > 0) {
    real_l inverse[3][3];
    int_g n = 0;
    root->map_data(grid, ngrid, traj, new_data, n, nframes, inverse, 0);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const int_g ngrid,
			      const std::vector<real_g (*)[3]> &traj,
			      std::vector<real_g (*)[3]> &new_data,
			      const int_g nframes, const coord_type a[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = 0.0;
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 1);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 1);
    delete_local_array(shifts);
    int_g n = 0;
    root->map_data(coords, ngrid, traj, new_data, n, nframes, inverse, 1);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const int_g ngrid,
			      const std::vector<real_g (*)[3]> &traj,
			      std::vector<real_g (*)[3]> &new_data,
			      const int_g nframes, const coord_type a[3],
			      const coord_type b[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    cell[2][2] = 1.0;
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 2);
    delete_local_array(shifts);
    int_g n = 0;
    root->map_data(coords, ngrid, traj, new_data, n, nframes, inverse, 2);
    delete_local_array(coords);
  }
}

void DLV::atom_tree::map_data(const real_g grid[][3], const int_g ngrid,
			      const std::vector<real_g (*)[3]> &traj,
			      std::vector<real_g (*)[3]> &new_data,
			      const int_g nframes, const coord_type a[3],
			      const coord_type b[3],
			      const coord_type c[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*shifts)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, shifts, ngrid, inverse, 3);
    delete_local_array(shifts);
    int_g n = 0;
    root->map_data(coords, ngrid, traj, new_data, n, nframes, inverse, 3);
    delete_local_array(coords);
  }
}

DLV::int_g DLV::atom_tree::get_number_selected_atoms() const
{
  return (int)selections.size();
}

void DLV::atom_tree_node::locate_selected_atom(const atom_tree_node *s,
					       int_g &count, bool &found) const
{
  if (left_child != 0)
    left_child->locate_selected_atom(s, count, found);
  if (!found) {
    count++;
    if (this == s)
      found = true;
    else if (right_child != 0)
      right_child->locate_selected_atom(s, count, found);
  }
}

DLV::int_g DLV::atom_tree::locate_selected_atom() const
{
  if (selections.size() < 1)
    return -1;
  else {
    int_g count = -1;
    bool found = false;
    root->locate_selected_atom(selections.front(), count, found);
    if (found)
      return count;
    else
      return -1;
  }
}

void DLV::atom_tree_node::locate_atom(const atom_position &p,
				      int_g &count, bool &found) const
{
  if (left_child != 0)
    left_child->locate_atom(p, count, found);
  if (!found) {
    if (get_data().compare(p) == 0) {
      found = true;
      return;
    } else {
      count++;
      if (right_child != 0)
	right_child->locate_atom(p, count, found);
    }
  }
}

DLV::int_g DLV::atom_tree::locate_atom(const atom_position &p) const
{
  int_g count = 0;
  bool found = false;
  root->locate_atom(p, count, found);
  if (!found)
    count = nnodes;
  return count;
}

DLV::int_g
DLV::atom_tree::find_primitive_selection(const std::map<int, atom> &basis) const
{
  if (selections.size() < 1)
    return -1;
  else {
    atom a = selections.front()->get_atom();
    // Todo - can this fail?
    return a->get_id();
  }
}

bool DLV::atom_tree::is_selected(const atom atm) const
{
  if (selections.size() < 1)
    return false;
  else {
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr) {
      atom a = (*ptr)->get_atom();
      if (a == atm)
	return true;
    }
    return false;
  }
}

void DLV::atom_tree_node::copy_atoms(std::map<int, atom> &basis,
				     atom prim[], const real_l shift[3],
				     int_g &count) const
{
  atom p(new atom_type());
  // copy all data
  atom x = get_atom();
  *p = *x;
  coord_type c[3];
  data.get_cartesian_coords(c);
  for (int_g i = 0; i < 3; i++)
    c[i] += shift[i];
  p->set_cartesian_coords(c);
  add_basis(basis, p, count);
  prim[count] = x;
  count++;
  if (left_child != 0)
    left_child->copy_atoms(basis, prim, shift, count);
  if (right_child != 0)
    right_child->copy_atoms(basis, prim, shift, count);
}

void DLV::atom_tree::copy_bonds(std::map<int, atom> &basis, bond_data &bonds,
				const bond_data &pbonds, const atom prim[])
{
  if (pbonds.has_single_pairs()) {
    // loop over new basis, if the parent atoms were bonded then the pair is
    std::map<int, atom>::const_iterator basis1;
    int_g i = 0;
    for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
      std::map<int, atom>::const_iterator basis2;
      int_g j = i;
      for (basis2 = basis1; basis2 != basis.end(); ++basis2) {
	if (pbonds.is_pair_set(prim[i], prim[j]))
	  bonds.set_pair(basis1->second, basis2->second, true, basis);
	j++;
      }
      i++;
    }
  }
  bonds.copy_type_bonds(pbonds);
}

void DLV::atom_tree::copy_atoms_and_bonds(std::map<int, atom> &basis,
					  bond_data &bonds,
					  const bond_data &pbonds) const
{
  if (nnodes > 0) {
    // Todo - local array, maybe unsafe with constructor
    atom *prim = new atom[nnodes];
    int_g count = 0;
    real_l shift[3] = {0.0, 0.0, 0.0};
    root->copy_atoms(basis, prim, shift, count);
    copy_bonds(basis, bonds, pbonds, prim);
    delete [] prim;
  }
}

void DLV::atom_tree::copy_atoms_and_bonds(std::map<int, atom> &basis,
					  bond_data &bonds,
					  const bond_data &pbonds,
					  atom * &prim,
					  const real_l shift[3]) const
{
  if (nnodes > 0) {
    if (prim == 0)
      prim = new atom[nnodes];
    int_g count = 0;
    root->copy_atoms(basis, prim, shift, count);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::copy_atoms(std::map<int, atom> &basis,
				     atom prim[], const int_g atn,
				     int_g &count) const
{
  atom p(new atom_type());
  // copy all data
  atom x = get_atom();
  *p = *x;
  if (atom_is_selected())
    p->set_atomic_number(atn);
  add_basis(basis, p, count);
  // we list the old type for the moment for bonding purposes
  prim[count] = x;
  count++;
  if (left_child != 0)
    left_child->copy_atoms(basis, prim, atn, count);
  if (right_child != 0)
    right_child->copy_atoms(basis, prim, atn, count);
}

void DLV::atom_tree::edit_bonds(std::map<int, atom> &basis, bond_data &bonds,
				const bond_data &pbonds, const atom prim[])
{
  if (pbonds.has_single_pairs()) {
    // loop over new basis, if the parent atoms were bonded then the pair is
    std::map<int, atom>::const_iterator basis1;
    int_g i = 0;
    for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
      std::map<int, atom>::const_iterator basis2;
      int_g j = i;
      for (basis2 = basis1; basis2 != basis.end(); ++basis2) {
	if (pbonds.is_pair_set(prim[i], prim[j]))
	  bonds.set_pair(basis1->second, basis2->second, true, basis);
	j++;
      }
      i++;
    }
  }
  if (pbonds.has_type_pairs()) {
    bonds.copy_type_bonds(pbonds);
    // If the parent type was bonded then so is the new one
    std::map<int, atom>::iterator ptr;
    int_g i = 0;
    for (ptr = basis.begin(); ptr != basis.end(); ++ptr ) {
      if (prim[i]->get_atomic_number() != ptr->second->get_atomic_number())
	bonds.duplicate_types(pbonds, prim[i]->get_atomic_number(),
			      ptr->second->get_atomic_number(), basis);
      i++;
    }
  }
}

void DLV::atom_tree::copy_atoms_and_bonds(std::map<int, atom> &basis,
					  bond_data &bonds,
					  const bond_data &pbonds,
					  atom * &prim,
					  const int_g atn) const
{
  if (nnodes > 0) {
    prim = new atom[nnodes];
    int_g count = 0;
    root->copy_atoms(basis, prim, atn, count);
    edit_bonds(basis, bonds, pbonds, prim);
    // reset prim info for check symmetry
    std::map<int, atom>::iterator ptr;
    int_g i = 0;
    atom newatom;
    for (ptr = basis.begin(); ptr != basis.end(); ++ptr ) {
      if (prim[i]->get_atomic_number() != ptr->second->get_atomic_number()) {
	if (newatom == 0)
	  newatom = ptr->second;
	prim[i] = newatom;
      }
      i++;
    }
  }
}

void DLV::atom_tree_node::copy_atoms(std::map<int, atom> &basis,
				     atom prim[], const real_l shift[3],
				     int_g &count, const string label,
				     const string group) const
{
  atom p(new atom_type());
  // copy all data
  atom x = get_atom();
  *p = *x;
  coord_type c[3];
  data.get_cartesian_coords(c);
  for (int_g i = 0; i < 3; i++)
    c[i] += shift[i];
  p->set_cartesian_coords(c);
  p->set_group(group, label);
  p->set_edit_flag();
  add_basis(basis, p, count);
  prim[count] = x;
  count++;
  if (left_child != 0)
    left_child->copy_atoms(basis, prim, shift, count, label, group);
  if (right_child != 0)
    right_child->copy_atoms(basis, prim, shift, count, label, group);
}

void DLV::atom_tree::add_atoms_and_bonds(std::map<int, atom> &basis,
					 bond_data &bonds,
					 const bond_data &pbonds,
					 atom * &prim, const int_g size,
					 const real_l shift[3],
					 const string label,
					 const string group) const
{
  if (nnodes > 0) {
    int_g count = size;
    root->copy_atoms(basis, prim, shift, count, label, group);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::copy_atoms(std::map<int, atom> &basis,
				     atom prim[], const real_l zshift,
				     int_g &count, const string label,
				     const string group) const
{
  atom p(new atom_type());
  // copy all data
  atom x = get_atom();
  *p = *x;
  coord_type c[3];
  data.get_fractional_coords(c);
  c[2] += zshift;
  p->set_fractional_coords(c);
  p->set_group(group, label);
  add_basis(basis, p, count);
  prim[count] = x;
  count++;
  if (left_child != 0)
    left_child->copy_atoms(basis, prim, zshift, count, label, group);
  if (right_child != 0)
    right_child->copy_atoms(basis, prim, zshift, count, label, group);
}

void DLV::atom_tree::add_atoms_and_bonds(std::map<int, atom> &basis,
					 bond_data &bonds,
					 const bond_data &pbonds,
					 atom * &prim, const int_g size,
					 const real_l zshift,
					 const string label,
					 const string group) const
{
  if (nnodes > 0) {
    int_g count = size;
    root->copy_atoms(basis, prim, zshift, count, label, group);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::delete_atoms(std::map<int, atom> &basis,
				       atom prim[], int_g &count) const
{
  if (!atom_is_selected()) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    coord_type c[3];
    data.get_cartesian_coords(c);
    p->set_cartesian_coords(c);
    add_basis(basis, p, count);
    // we list the old type for the moment for bonding purposes
    prim[count] = x;
    count++;
  }
  if (left_child != 0)
    left_child->delete_atoms(basis, prim, count);
  if (right_child != 0)
    right_child->delete_atoms(basis, prim, count);
}

void DLV::atom_tree::delete_selected_atoms(std::map<int, atom> &basis,
					   bond_data &bonds,
					   const bond_data &pbonds,
					   atom * &prim) const
{
  if (nnodes > 0) {
    prim = new atom[nnodes];
    int_g count = 0;
    root->delete_atoms(basis, prim, count);
    edit_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::edit_atoms(std::map<int, atom> &basis,
				     atom prim[], int_g &count,
				     const bool keep_sym) const
{
  // Try to force coords to the ones the user selected, not the copies
  if ((keep_sym and (selected == is_selected or selected == not_selected)) or
      (!keep_sym)) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    if (keep_sym) {
      // we should end up with the same list of atoms, since we copied
      // the undelying atom, everything should be the same
      if (basis.find(x->get_id()) == basis.end()) {
	if (atom_is_selected())
	  p->set_edit_flag();
	else
	  p->clear_edit_flag();
	basis[x->get_id()] = p;
	prim[x->get_id()] = x;
	count++;
      }
    } else {
      coord_type c[3];
      data.get_cartesian_coords(c);
      p->set_cartesian_coords(c);
      if (atom_is_selected())
	p->set_edit_flag();
      else
	p->clear_edit_flag();
      add_basis(basis, p, count);
      prim[count] = x;
      count++;
    }
  }
  if (left_child != 0)
    left_child->edit_atoms(basis, prim, count, keep_sym);
  if (right_child != 0)
    right_child->edit_atoms(basis, prim, count, keep_sym);
}

void DLV::atom_tree::edit_atoms_and_bonds(std::map<int, atom> &basis,
					  bond_data &bonds,
					  const bond_data &pbonds,
					  atom * &prim, const bool keep,
					  const bool update) const
{
  if (nnodes > 0) {
    if (prim == 0)
      prim = new atom[nnodes];
    int_g count = 0;
    root->edit_atoms(basis, prim, count, keep);
    if (keep and update) {
      // scan selected list and set asym coords to the selected ones so
      // that shifts will be calculated sensibly.
      for (std::list<atom_tree_node *>::const_iterator p = selections.begin();
	   p != selections.end(); ++p) {
	coord_type c[3];
	(*p)->get_data().get_cartesian_coords(c);
	for (std::map<int, atom>::iterator b = basis.begin();
	     b != basis.end(); ++b) {
	  if ((*p)->get_atom() == prim[b->first]) {
	    b->second->set_cartesian_coords(c);
	    break;
	  }
	}
      }
    }
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::copy_atom_layers(std::map<int, atom> &basis,
					   atom prim[], const real_l caxis[3],
					   int_g &index, int_g &count,
					   const int_g term,
					   const int_g lcopies[],
					   const int_g lindex[],
					   const string group,
					   const bool slab,
					   const int_g salvage) const
{
  // must match get_cartesian_coords
  if (left_child != 0)
    left_child->copy_atom_layers(basis, prim, caxis, index, count,
				 term, lcopies, lindex, group, slab, salvage);
  int_g start = 0;
  int_g end = lcopies[lindex[index]];
  coord_type c[3];
  data.get_cartesian_coords(c);
  if (slab) {
    //if (lindex[index] != 0) {
    if (salvage == 0) {
      if (c[2] > cart_tol) {
	// coords are in +z so all atoms not at 0.0 need to shift down one.
	start++;
	end++;
      }
      if (lindex[index] < term) {
	// shift by 1-n axes, rather then 0-(n-1)
	start++;
	end++;
      }
    }
  }
  int_g lc = 1;
  for (int_g i = start; i < end; i++) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    coord_type nc[3];
    for (int_g j = 0; j < 3; j++)
      nc[j] = c[j] - (coord_type)i * caxis[j];
    p->set_cartesian_coords(nc);
    char buff[40];
    if (slab) {
      snprintf(buff, 40, "layer %d (%d)", lindex[index], lc);
      p->set_group(group, buff);
    } else {
      if (salvage > 0 and lindex[index] == 0)
	/* there will be an existing label */;
      else {
	if (i < end - 1) {
	  p->set_special_periodicity(false);
	  if (salvage > 0)
	    snprintf(buff, 40, "salvage %d (%d)",
		     lindex[index] - 1 + salvage, lc);
	  else
	    snprintf(buff, 40, "salvage %d (%d)", lindex[index], lc);
	  p->set_group(group, buff);
	} else if (salvage == 0) {
	  snprintf(buff, 40, "bulk %d", lindex[index]);
	  p->set_group(group, buff);
	}
      }
    }
    lc++;
    add_basis(basis, p, count);
    prim[count] = x;
    count++;
  }
  index++;
  if (right_child != 0)
    right_child->copy_atom_layers(basis, prim, caxis, index, count,
				  term, lcopies, lindex, group, slab, salvage);
}

void DLV::atom_tree::copy_atom_layers_and_bonds(std::map<int, atom> &basis,
						bond_data &bonds,
						const bond_data &pbonds,
						atom * &prim,
						const real_l caxis[3],
						const int_g ncells,
						const int_g term,
						const int_g lcopies[],
						const int_g lindex[],
						const string group,
						const bool slab,
						const int_g salvage) const
{
  if (nnodes > 0) {
    // Todo - local array, maybe unsafe with constructor
    prim = new atom[ncells * nnodes];
    int_g count = 0;
    int_g index = 0;
    root->copy_atom_layers(basis, prim, caxis, index, count, term,
			   lcopies, lindex, group, slab, salvage);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::shift_atoms(const coord_type centre[3],
				      real_l distance[], int_g &count) const
{
  coord_type c[3];
  data.get_cartesian_coords(c);
  distance[count] = (c[0] - centre[0]) * (c[0] - centre[0])
    + (c[1] - centre[1]) * (c[1] - centre[1])
    + (c[2] - centre[2]) * (c[2] - centre[2]);
  count++;
  if (left_child != 0)
    left_child->shift_atoms(centre, distance, count);
  if (right_child != 0)
    right_child->shift_atoms(centre, distance, count);
}

void DLV::atom_tree::order_distances(int_g order[], const real_l distance[],
				     const int_g n)
{
  for (int_g k = 0; k < n; k++)
    order[k] = k;
  // Crude shellsort
  int_g increment = n;
  do {
    increment = (increment / 3) + 1;
    for (int_g start = 0; start < increment; start++) {
      for (int_g i = start + increment; i < n; i += increment) {
        if (distance[order[i]] < distance[order[i - increment]]) {
          int_g j = i;
          int_g t = order[i];
          do {
            j -= increment;
            order[j + increment] = order[j];
            if (j == start)
              break;
          } while (distance[order[j - increment]] >= distance[t]);
          order[j] = t;
        }
      }
    }
  } while (increment > 1);
}

void DLV::atom_tree_node::build_cluster_atoms(std::map<int, atom> &basis,
					      const coord_type centre[3],
					      const int_g flags[],
					      atom prim[], int_g &index,
					      int_g &count,
					      const string gpname) const
{
  if (flags[index] > -1) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    coord_type c[3];
    data.get_cartesian_coords(c);
    c[0] -= centre[0];
    c[1] -= centre[1];
    c[2] -= centre[2];
    p->set_cartesian_coords(c);
    if (flags[index] > 0) {
      char buff[32];
      snprintf(buff, 32, "shell %1d", flags[index]);
      p->set_group(gpname, buff);
    } else
      p->set_group(gpname, "central atom");
    add_basis(basis, p, count);
    prim[count] = x;
    count++;
  }
  index++;
  if (left_child != 0)
    left_child->build_cluster_atoms(basis, centre, flags, prim,
				    index, count, gpname);
  if (right_child != 0)
    right_child->build_cluster_atoms(basis, centre, flags, prim,
				     index, count, gpname);
}

void DLV::atom_tree_node::find_cell0_atom(const atom &a, coord_type centre[3],
					  real_l &l) const
{
  if (get_atom() == a) {
    coord_type c[3];
    data.get_cartesian_coords(c);
    real_l d = (c[0] * c[0]) + (c[1] * c[1]) + (c[2] * c[2]);
    if (d < l) {
      centre[0] = c[0];
      centre[1] = c[1];
      centre[2] = c[2];
      l = d;
    }
  }
  if (left_child != 0)
    left_child->find_cell0_atom(a, centre, l);
  if (right_child != 0)
    right_child->find_cell0_atom(a, centre, l);
}

void DLV::atom_tree::build_cluster_atoms_and_bonds(std::map<int, atom> &basis,
						   bond_data &bonds,
						   const bond_data &pbonds,
						   atom * &prim, const int_g n,
						   const string gpname,
						   const real_l x,
						   const real_l y,
						   const real_l z) const
{
  if (nnodes > 0) {
    coord_type centre[3];
    centre[0] = x;
    centre[1] = y;
    centre[2] = z;
    real_l *distance = new_local_array1(real_l, nnodes);
    int_g count = 0;
    root->shift_atoms(centre, distance, count);
    int_g *order = new_local_array1(int_g, nnodes);
    order_distances(order, distance, nnodes);
    int_g *flags = new_local_array1(int_g, nnodes);
    for (int_g i = 0; i < nnodes; i++)
      flags[i] = -1;
    int_g shells = 0;
    const real_l tol = 0.01;
    int_g j = 0;
    if (distance[order[0]] < cart_tol) {
      flags[order[0]] = 0;
      j = 1;
    }
    for (int_g i = j; i < nnodes; i++) {
      flags[order[i]] = shells + 1;
      j++;
      if (i < nnodes - 1) {
	if (distance[order[i + 1]] > distance[order[i]] + tol)
	  shells++;
	if (shells == n)
	  break;
      }
    }
    delete_local_array(order);
    delete_local_array(distance);
    prim = new atom[j];
    count = 0;
    int_g index = 0;
    root->build_cluster_atoms(basis, centre, flags, prim,
			      index, count, gpname);
    delete_local_array(flags);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree::build_cluster_atoms_and_bonds(std::map<int, atom> &basis,
						   bond_data &bonds,
						   const bond_data &pbonds,
						   atom * &prim, const real_l r,
						   const string gpname,
						   const real_l x,
						   const real_l y,
						   const real_l z) const
{
  if (nnodes > 0) {
    //real_l l = 10000.0;
    coord_type centre[3];
    centre[0] = x;
    centre[1] = y;
    centre[2] = z;
    real_l *distance = new_local_array1(real_l, nnodes);
    int_g count = 0;
    root->shift_atoms(centre, distance, count);
    int_g *order = new_local_array1(int_g, nnodes);
    order_distances(order, distance, nnodes);
    int_g *flags = new_local_array1(int_g, nnodes);
    for (int_g i = 0; i < nnodes; i++)
      flags[i] = -1;
    // first atom should be the central one at (0.0, 0.0, 0.0)
    int_g j = 0;
    if (distance[order[0]] < cart_tol) {
      flags[order[0]] = 0;
      j = 1;
    }
    real_l sp = r * r;
    const real_l tol = 0.01;
    int_g shells = 1;
    for (int_g i = j; i < nnodes; i++) {
      if (distance[order[i]] < sp + tol) {
	flags[order[i]] = shells;
	j++;
	if (i < nnodes - 1) {
	  if (distance[order[i + 1]] > distance[order[i]] + tol)
	    shells++;
	}
      } else
	break;
    }
    delete_local_array(order);
    delete_local_array(distance);
    prim = new atom[j];
    count = 0;
    int_g index = 0;
    root->build_cluster_atoms(basis, centre, flags, prim,
			      index, count, gpname);
    delete_local_array(flags);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::build_wulff_atoms(std::map<int, atom> &basis,
					    atom prim[], int_g &count,
					    const real_g points[][3],
					    const real_g normals[][3],
					    const int_g nplanes,
					    const real_g offsets[]) const
{
  coord_type c[3];
  data.get_cartesian_coords(c);
  bool inside = true;
  // find distance of atom from plane
  for (int i = 0; i < nplanes; i++) {
    real_g dot = (points[i][0] - c[0]) * normals[i][0]
      + (points[i][1] - c[1]) * normals[i][1]
      + (points[i][2] - c[2]) * normals[i][2];
    if (dot > offsets[i]) {
      inside = false;
      break;
    }
  }
  if (inside) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    //coord_type c[3];
    //data.get_cartesian_coords(c);
    //c[0] -= centre[0];
    //c[1] -= centre[1];
    //c[2] -= centre[2];
    p->set_cartesian_coords(c);
    add_basis(basis, p, count);
    prim[count] = x;
    count++;
  }
  if (left_child != 0)
    left_child->build_wulff_atoms(basis, prim, count, points, normals, nplanes,
				  offsets);
  if (right_child != 0)
    right_child->build_wulff_atoms(basis, prim, count,
				   points, normals, nplanes, offsets);
}

void DLV::atom_tree::build_wulff_shape(std::map<int, atom> &basis,
				       bond_data &bonds,
				       const bond_data &pbonds, atom * &prim,
				       const real_l verts[][3],
				       const int_g nvertices,
				       int_g **face_vertices,
				       const int_g nface_vertices[],
				       const int_g nfaces,
				       const real_g offsets[]) const
{
  if (nnodes > 0) {
    // for each plane define A,B,C as the first 3 points, then B-A and C-A are
    // vectors in the plane and (B-A)x(C-A) is normal. The origin is inside the
    // plane so if the distance of the origin d (A-O).n is +ve change sign
    // of normal. Then any atom position whose distance from the plane is -ve
    // will be inside the plane (same side as origin)
    // plane has formula (r-a).n = 0
    int nplanes = 0;
    for (int i = 0; i < nfaces; i++)
      if (nface_vertices[i] > 2)
	nplanes++;
    real_g (*points)[3] = new_local_array2(real_g, nplanes, 3);
    real_g *shifts = new_local_array1(real_g, nplanes);
    const real_g tol = 0.0001;
    int idx = 0;
    for (int i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	points[idx][0] = verts[face_vertices[i][0]][0];
	points[idx][1] = verts[face_vertices[i][0]][1];
	points[idx][2] = verts[face_vertices[i][0]][2];
	shifts[idx] = offsets[i] + tol;
	idx++;
      }
    }
    real_g (*normals)[3] = new_local_array2(real_g, nplanes, 3);
    idx = 0;
    for (int i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	real_g ba[3];
	ba[0] = verts[face_vertices[i][1]][0] - verts[face_vertices[i][0]][0];
	ba[1] = verts[face_vertices[i][1]][1] - verts[face_vertices[i][0]][1];
	ba[2] = verts[face_vertices[i][1]][2] - verts[face_vertices[i][0]][2];
	real_g ca[3];
	ca[0] = verts[face_vertices[i][2]][0] - verts[face_vertices[i][0]][0];
	ca[1] = verts[face_vertices[i][2]][1] - verts[face_vertices[i][0]][1];
	ca[2] = verts[face_vertices[i][2]][2] - verts[face_vertices[i][0]][2];
	normals[idx][0] = ba[1] * ca[2] - ba[2] * ca[1];
	normals[idx][1] = ba[2] * ca[0] - ba[0] * ca[2];
	normals[idx][2] = ba[0] * ca[1] - ba[1] * ca[0];
	real_g len = sqrt(normals[idx][0] * normals[idx][0]
			  + normals[idx][1] * normals[idx][1]
			  + normals[idx][2] * normals[idx][2]);
	// normalize
	normals[idx][0] /= len;
	normals[idx][1] /= len;
	normals[idx][2] /= len;
	// find distance of origin
	real_g dot = points[idx][0] * normals[idx][0]
	  + points[idx][1] * normals[idx][1] + points[idx][2] * normals[idx][2];
	// should be safe since wulff planes shouldn't go through origin
	if (dot > 0.0) {
	  normals[idx][0] = - normals[idx][0];
	  normals[idx][1] = - normals[idx][1];
	  normals[idx][2] = - normals[idx][2];
	}
	idx++;
      }
    }
    prim = new atom[nnodes];
    int_g count = 0;
    root->build_wulff_atoms(basis, prim, count, points,
			    normals, nplanes, shifts);
    delete_local_array(normals);
    delete_local_array(shifts);
    delete_local_array(points);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::build_box_atoms(std::map<int, atom> &basis,
					  atom prim[], int_g &count,
					  const real_g points[][3],
					  const real_g normals[][3],
					  const int_g nplanes,
					  const int_g planes_per_obj[],
					  const int_g nobjs) const
{
  coord_type c[3];
  data.get_cartesian_coords(c);
  bool inside = true;
  int idx = 0;
  for (int i = 0; i < nobjs; i++) {
    int jdx = idx;
    inside = true;
    // find distance of atom from plane
    for (int j = 0; j < planes_per_obj[i]; j++) {
      real_g dot = (points[jdx][0] - c[0]) * normals[jdx][0]
	+ (points[jdx][1] - c[1]) * normals[jdx][1]
	+ (points[jdx][2] - c[2]) * normals[jdx][2];
      if (dot > 0.0) {
	inside = false;
	break;
      }
      jdx++;
    }
    // Its inside one of them so keep it
    if (inside)
      break;
    idx += planes_per_obj[i];
  }
  if (inside) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    p->set_cartesian_coords(c);
    add_basis(basis, p, count);
    prim[count] = x;
    count++;
  }
  if (left_child != 0)
    left_child->build_box_atoms(basis, prim, count, points, normals, nplanes,
				planes_per_obj, nobjs);
  if (right_child != 0)
    right_child->build_box_atoms(basis, prim, count, points, normals, nplanes,
				 planes_per_obj, nobjs);
}

void DLV::atom_tree::build_box(std::map<int, atom> &basis, bond_data &bonds,
			       const bond_data &pbonds, atom * &prim,
			       const real_g verts[][3], const int_g nvertices,
			       const int_g planes[], const int_g nplanes,
			       const int_g planes_per_obj[],
			       const int_g nobjs) const
{
  if (nnodes > 0) {
    // based on wulff_shape, but needs to use centre of each shape instead
    // of the origin
    // use vertex average as centre of plane
    real_g (*centres)[3] = new_local_array2(real_g, nplanes, 3);
    int idx = 0;
    for (int i = 0; i < nplanes; i++) {
      centres[i][0] = 0.0;
      centres[i][1] = 0.0;
      centres[i][2] = 0.0;
      for (int j = 0; j < planes[i]; j++) {
	centres[i][0] += verts[idx][0];
	centres[i][1] += verts[idx][1];
	centres[i][2] += verts[idx][2];
	idx++;
      }
      centres[i][0] /= real_g(planes[i]);
      centres[i][1] /= real_g(planes[i]);
      centres[i][2] /= real_g(planes[i]);
    }
    // now use average of centres as origins of objects
    real_g (*origins)[3] = new_local_array2(real_g, nobjs, 3);
    idx = 0;
    for (int i = 0; i < nobjs; i++) {
      origins[i][0] = 0.0;
      origins[i][1] = 0.0;
      origins[i][2] = 0.0;
      for (int j = 0; j < planes_per_obj[i]; j++) {
	origins[i][0] += centres[idx][0];
	origins[i][1] += centres[idx][1];
	origins[i][2] += centres[idx][2];
	idx++;
      }
      origins[i][0] /= real_g(planes_per_obj[i]);
      origins[i][1] /= real_g(planes_per_obj[i]);
      origins[i][2] /= real_g(planes_per_obj[i]);
    }
    /*
    real_g (*points)[3] = new_local_array2(real_g, nplanes, 3);
    idx = 0;
    for (int i = 0; i < nplanes; i++) {
      points[i][0] = verts[idx][0];
      points[i][1] = verts[idx][1];
      points[i][2] = verts[idx][2];
      idx += planes[i];
    }
    */
    real_g (*normals)[3] = new_local_array2(real_g, nplanes, 3);
    idx = 0;
    int plane = 0;
    for (int i = 0; i < nobjs; i++) {
      for (int j = 0; j < planes_per_obj[i]; j++) {
	real_g ba[3];
	ba[0] = verts[idx + 1][0] - verts[idx][0];
	ba[1] = verts[idx + 1][1] - verts[idx][1];
	ba[2] = verts[idx + 1][2] - verts[idx][2];
	real_g ca[3];
	ca[0] = verts[idx + 2][0] - verts[idx][0];
	ca[1] = verts[idx + 2][1] - verts[idx][1];
	ca[2] = verts[idx + 2][2] - verts[idx][2];
	normals[plane][0] = ba[1] * ca[2] - ba[2] * ca[1];
	normals[plane][1] = ba[2] * ca[0] - ba[0] * ca[2];
	normals[plane][2] = ba[0] * ca[1] - ba[1] * ca[0];
	real_g len = sqrt(normals[plane][0] * normals[plane][0]
			  + normals[plane][1] * normals[plane][1]
			  + normals[plane][2] * normals[plane][2]);
	// normalize
	normals[plane][0] /= len;
	normals[plane][1] /= len;
	normals[plane][2] /= len;
	// find distance of origin
	real_g dot = (centres[plane][0] - origins[i][0]) * normals[plane][0]
	  + (centres[plane][1] - origins[i][1]) * normals[plane][1]
	  + (centres[plane][2] - origins[i][2]) * normals[plane][2];
	// should be safe since planes shouldn't go through origin
	if (dot > 0.0) {
	  normals[plane][0] = - normals[plane][0];
	  normals[plane][1] = - normals[plane][1];
	  normals[plane][2] = - normals[plane][2];
	}
	idx += planes[plane];
	plane++;
      }
    }
    prim = new atom[nnodes];
    int_g count = 0;
    root->build_box_atoms(basis, prim, count, centres, normals, nplanes,
			  planes_per_obj, nobjs);
    delete_local_array(normals);
    //delete_local_array(points);
    delete_local_array(origins);
    delete_local_array(centres);
    copy_bonds(basis, bonds, pbonds, prim);
  }
}

void DLV::atom_tree_node::build_cyl_atoms(std::map<int, atom> &basis,
					  atom prim[], int_g &count,
					  const real_g iradius[],
					  const real_g oradius[],
					  const real_g zmin[],
					  const real_g zmax[],
					  const int orient[],
					  const int ncylinders) const
{
  coord_type c[3];
  data.get_cartesian_coords(c);
  bool inside = false;
  for (int i = 0; i < ncylinders; i++) {
    // test if atom inside cylinder.
    int x = orient[i];
    int y = (x + 1) % 3;
    int z = (y + 1) % 3;
    real_g rho = c[y] * c[y] + c[z] * c[z];
    if (c[x] <= zmax[i] and c[x] >= zmin[i] and
	rho >= iradius[i] * iradius[i] and rho <= oradius[i] * oradius[i]) {
      inside = true;
      break;
    }
  }
  if (inside) {
    atom p(new atom_type());
    // copy all data
    atom x = get_atom();
    *p = *x;
    p->set_cartesian_coords(c);
    add_basis(basis, p, count);
    prim[count] = x;
    count++;
  }
  if (left_child != 0)
    left_child->build_cyl_atoms(basis, prim, count, iradius, oradius, zmin,
				zmax, orient, ncylinders);
  if (right_child != 0)
    right_child->build_cyl_atoms(basis, prim, count, iradius, oradius, zmin,
				 zmax, orient, ncylinders);
}

void DLV::atom_tree::build_cylinders(std::map<int, atom> &basis,
				     bond_data &bonds, const bond_data &pbonds,
				     const real_g iradius[],
				     const real_g oradius[],
				     const real_g zmin[], const real_g zmax[],
				     const int orient[],
				     const int ncylinders) const
{
  if (nnodes > 0) {
    atom *prim = new atom[nnodes];
    int_g count = 0;
    root->build_cyl_atoms(basis, prim, count, iradius, oradius, zmin, zmax,
			  orient, ncylinders);
    copy_bonds(basis, bonds, pbonds, prim);
    delete [] prim;
  }
}

void DLV::atom_tree_node::map_atom_count(const atom &myatom,
					 const real_g coords[][3],
					 const int_g ngrid,
					 const real_l inv[3][3],
					 const int_g dim, int_g &count,
					 const bool all_atoms) const
{
  int_g atn = myatom->get_atomic_number();
  if ((all_atoms and data.get_atomic_number() == atn) or
      data.get_atom() == myatom) {
    // find all symmetry copies or all atoms of type and add to list
    //Todo? coord_type offset[3];
    //int i = data.locate_coord(coords, offset, ngrid, inv, 3);
    count++;
  }
  if (left_child != 0)
    left_child->map_atom_count(myatom, coords, ngrid, inv, dim,
			       count, all_atoms);
  if (right_child != 0)
    right_child->map_atom_count(myatom, coords, ngrid, inv, dim,
				count, all_atoms);
}

void DLV::atom_tree_node::map_atom_selection(const atom &myatom,
					     const real_g coords[][3],
					     const int_g ngrid,
					     int_g **const info,
					     const int_g ninfo,
					     const real_l inv[3][3],
					     const int_g dim, int_g labels[],
					     int_g atom_types[], atom parents[],
					     int_g &count,
					     const bool all_atoms) const
{
  int_g atn = myatom->get_atomic_number();
  if ((all_atoms and data.get_atomic_number() == atn) or
      data.get_atom() == myatom) {
    // find all symmetry copies or all atoms of type and add to list
    coord_type offset[3];
    int_g i = data.locate_coord(coords, offset, ngrid, inv, dim);
    if (i < ngrid) {
      // check we haven't already got this primitive atom.
      int_g j;
      for (j = 0; j < count; j++)
	if (labels[j] == info[0][i])
	  break;
      if (j == count) {
	labels[count] = info[0][i];
	atom_types[count] = atn;
	parents[count] = data.get_atom();
	count++;
      }
    }
  }
  if (left_child != 0)
    left_child->map_atom_selection(myatom, coords, ngrid, info, ninfo, inv,
				   dim, labels, atom_types, parents, count,
				   all_atoms);
  if (right_child != 0)
    right_child->map_atom_selection(myatom, coords, ngrid, info, ninfo, inv,
				    dim, labels, atom_types, parents, count,
				    all_atoms);
}

bool DLV::atom_tree::map_atom_selections(const atom_tree &primitives,
					 int_g &n, int_g * &labels,
					 int_g * &atom_types, atom * &parents,
					 const bool all_atoms,
					 const real_g grid[][3],
					 int_g **const info, const int_g ninfo,
					 const int_g ngrid) const
{
  if (nnodes > 0 and selections.size() > 0) {
    real_l inverse[3][3];
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    // Todo - do it once in data_object and save?
    std_coords(coords, 0, ngrid, inverse, 0);
    int_g count = 0;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      // loop over primitive atoms related to this selection
      primitives.root->map_atom_count((*ptr)->get_atom(), coords, ngrid,
				      inverse, 0, count, all_atoms);
    }
    if (count > primitives.size())
      count = primitives.size();
    labels = new int_g[count];
    atom_types = new int_g[count];
    parents = new atom[count];
    n = 0;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      primitives.root->map_atom_selection((*ptr)->get_atom(), coords, ngrid,
					  info, ninfo, inverse, 0, labels,
					  atom_types, parents, n, all_atoms);
    }
    delete_local_array(coords);
    return true;
  } else
    return false;
}

bool DLV::atom_tree::map_atom_selections(const atom_tree &primitives,
					 int_g &n, int_g * &labels,
					 int_g * &atom_types, atom * &parents,
					 const bool all_atoms,
					 const real_g grid[][3],
					 int_g **const info, const int_g ninfo,
					 const int_g ngrid,
					 const coord_type a[3]) const
{
  if (nnodes > 0 and selections.size() > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = 0.0;
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 1);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    // Todo - do it once in data_object and save?
    std_coords(coords, 0, ngrid, inverse, 1);
    int_g count = 0;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      // loop over primitive atoms related to this selection
      primitives.root->map_atom_count((*ptr)->get_atom(), coords, ngrid,
				      inverse, 1, count, all_atoms);
    }
    if (count > primitives.size())
      count = primitives.size();
    labels = new int_g[count];
    atom_types = new int_g[count];
    parents = new atom[count];
    n = 0;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      primitives.root->map_atom_selection((*ptr)->get_atom(), coords, ngrid,
					  info, ninfo, inverse, 1, labels,
					  atom_types, parents, n, all_atoms);
    }
    delete_local_array(coords);
    return true;
  } else
    return false;
}

bool DLV::atom_tree::map_atom_selections(const atom_tree &primitives,
					 int_g &n, int_g * &labels,
					 int_g * &atom_types, atom * &parents,
					 const bool all_atoms,
					 const real_g grid[][3],
					 int_g **const info, const int_g ninfo,
					 const int_g ngrid,
					 const coord_type a[3],
					 const coord_type b[3]) const
{
  if (nnodes > 0 and selections.size() > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    // Todo - do it once in data_object and save?
    std_coords(coords, 0, ngrid, inverse, 2);
    int_g count = 0;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      // loop over primitive atoms related to this selection
      primitives.root->map_atom_count((*ptr)->get_atom(), coords, ngrid,
				      inverse, 2, count, all_atoms);
    }
    if (count > primitives.size())
      count = primitives.size();
    labels = new int_g[count];
    atom_types = new int_g[count];
    parents = new atom[count];
    n = 0;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      primitives.root->map_atom_selection((*ptr)->get_atom(), coords, ngrid,
					  info, ninfo, inverse, 2, labels,
					  atom_types, parents, n, all_atoms);
    }
    delete_local_array(coords);
    return true;
  } else
    return false;
}

bool DLV::atom_tree::map_atom_selections(const atom_tree &primitives,
					 int_g &n, int_g * &labels,
					 int_g * &atom_types, atom * &parents,
					 const bool all_atoms,
					 const real_g grid[][3],
					 int_g **const info, const int_g ninfo,
					 const int_g ngrid,
					 const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3]) const
{
  if (nnodes > 0 and selections.size() > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    // Todo - do it once in data_object and save?
    std_coords(coords, 0, ngrid, inverse, 3);
    int_g count = 0;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      // loop over primitive atoms related to this selection
      primitives.root->map_atom_count((*ptr)->get_atom(), coords, ngrid,
				      inverse, 3, count, all_atoms);
    }
    if (count > primitives.size())
      count = primitives.size();
    labels = new int_g[count];
    atom_types = new int_g[count];
    parents = new atom[count];
    n = 0;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      primitives.root->map_atom_selection((*ptr)->get_atom(), coords, ngrid,
					  info, ninfo, inverse, 3, labels,
					  atom_types, parents, n, all_atoms);
    }
    delete_local_array(coords);
    return true;
  } else
    return false;
}

bool DLV::atom_tree::map_selected_atoms(int_g &n, int_g * &labels,
					int_g (* &shifts)[3],
					const real_g grid[][3],
					int_g **const info, const int_g ninfo,
					const int_g ngrid) const
{
  return false;
}

bool DLV::atom_tree::map_selected_atoms(int_g &n, int_g * &labels,
					int_g (* &shifts)[3],
					const real_g grid[][3],
					int_g **const info, const int_g ninfo,
					const int_g ngrid,
					const coord_type a[3]) const
{
  return false;
}

bool DLV::atom_tree::map_selected_atoms(int_g &n, int_g * &labels,
					int_g (* &shifts)[3],
					const real_g grid[][3],
					int_g **const info, const int_g ninfo,
					const int_g ngrid,
					const coord_type a[3],
					const coord_type b[3]) const
{
  return false;
}

bool DLV::atom_tree::map_selected_atoms(int_g &n, int_g * &labels,
					int_g (* &shifts)[3],
					const real_g grid[][3],
					int_g **const info, const int_g ninfo,
					const int_g ngrid,
					const coord_type a[3],
					const coord_type b[3],
					const coord_type c[3]) const
{
  if (nnodes > 0 and selections.size() > 0) {
    // Todo - lots of common code here?
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*coords)[3] = new_local_array2(real_g, ngrid, 3);
    for (int_g i = 0; i < ngrid; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
    int_g (*offset)[3] = new_local_array2(int_g, ngrid, 3);
    // Todo - do it once in data_object and save?
    std_coords(coords, offset, ngrid, inverse, 3);
    int_g count = int(selections.size());
    labels = new int_g[count];
    shifts = new int_g[count][3];
    n = 0;
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr) {
      DLV::coord_type s[3];
      int_g i = (*ptr)->get_data().locate_coord(coords, s, ngrid, inverse, 3);
      if (i < ngrid) {
	labels[n] = info[0][i];
	shifts[n][0] = to_integer(s[0]) - offset[i][0];
	shifts[n][1] = to_integer(s[1]) - offset[i][1];
	shifts[n][2] = to_integer(s[2]) - offset[i][2];
      }
      n++;
    }
    delete_local_array(offset);
    delete_local_array(coords);
    return true;
  } else
    return false;
}

void DLV::atom_tree::set_selected_atom_flags(const bool done)
{
  std::list<atom_tree_node *>::const_iterator ptr;
  for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
    if (done)
      (*ptr)->set_atom_flag(flag_done);
    else
      (*ptr)->set_atom_flag(flag_partial);
  }
}

bool DLV::atom_tree::set_selected_index_and_flags(int_g indices[],
						  const int_g value,
						  const bool set_def,
					     const std::map<int, atom> &basis,
						  char message[],
						  const int_g mlen)
{
  if (selections.size() == 0) {
    strncpy(message, "No atoms selected for basis", mlen);
    return false;
  }
  std::list<atom_tree_node *>::const_iterator ptr = selections.begin();
  int_g atn = (*ptr)->get_atom()->get_atomic_number();
  ++ptr;
  for (; ptr != selections.end(); ++ptr ) {
    if (atn != (*ptr)->get_atom()->get_atomic_number()) {
      strncpy(message, "Selected atoms are of different types", mlen);
      return false;
    }
  }
  std::map<int, atom>::const_iterator atom_ptr;
  for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
    (*ptr)->set_atom_flag(flag_done);
    indices[(*ptr)->get_atom()->get_id()] = value;
  }
  if (set_def) {
    int_g i = 0;
    for (atom_ptr = basis.begin(); atom_ptr != basis.end(); ++atom_ptr ) {
      if (atom_ptr->second->get_atomic_number() == atn) {
	if (indices[i] == -1) {
	  (*ptr)->set_atom_flag(flag_partial);
	  indices[i] = value;
	}
      }
      i++;
    }
  }
  return true;
}

void DLV::atom_tree::get_atom_shifts(const coord_type coords[][3],
				     int_g shifts[][3], int_g prim_id[],
				     const int_g n) const
{
  if (nnodes > 0) {
    for (int_g i = 0; i < n; i++) {
      shifts[i][0] = 0;
      shifts[i][1] = 0;
      shifts[i][2] = 0;
    }
    coord_type (*prim)[3] = new_local_array2(coord_type, nnodes, 3);
    get_cartesian_coords(prim, nnodes);
    for (int_g i = 0; i < n; i++) {
      int_g j;
      for (j = 0; j < nnodes; j++) {
	if (abs(coords[i][0] - prim[j][0]) < cart_tol and
	    abs(coords[i][1] - prim[j][1]) < cart_tol and
	    abs(coords[i][2] - prim[j][2]) < cart_tol)
	  break;
      }
      if (j == nnodes)
	fprintf(stderr, "Get atom shifts failed to find atom\n");
      prim_id[i] = j;
    }
    delete_local_array(prim);
  }
}

void DLV::atom_tree::get_atom_shifts(const coord_type coords[][3],
				     int_g shifts[][3], int_g prim_id[],
				     const int_g n, const coord_type a[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = 0.0;
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 1);
    real_g (*grid)[3] = new_local_array2(real_g, n, 3);
    for (int_g i = 0; i < n; i++) {
      grid[i][0] = real_g(coords[i][0]);
      grid[i][1] = real_g(coords[i][1]);
      grid[i][2] = real_g(coords[i][2]);
    }
    std_coords(grid, shifts, n, inverse, 1);
    for (int_g i = 0; i < n; i++) {
      grid[i][0] = real_g(cell[0][0]) * grid[i][0];
      grid[i][1] = real_g(coords[i][1]);
      grid[i][2] = real_g(coords[i][2]);
    }
    coord_type (*prim)[3] = new_local_array2(coord_type, nnodes, 3);
    get_cartesian_coords(prim, nnodes);
    for (int_g i = 0; i < n; i++) {
      int_g j;
      for (j = 0; j < nnodes; j++) {
	if (abs(real_l(grid[i][0]) - prim[j][0]) < cart_tol and
	    abs(real_l(grid[i][1]) - prim[j][1]) < cart_tol and
	    abs(real_l(grid[i][2]) - prim[j][2]) < cart_tol)
	  break;
      }
      if (j == nnodes)
	fprintf(stderr, "Get atom shifts failed to find atom\n");
      prim_id[i] = j;
    }
    delete_local_array(prim);
    delete_local_array(grid);
  }
}

void DLV::atom_tree::get_atom_shifts(const coord_type coords[][3],
				     int_g shifts[][3], int_g prim_id[],
				     const int_g n, const coord_type a[3],
				     const coord_type b[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = 0.0;
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 2);
    real_g (*grid)[3] = new_local_array2(real_g, n, 3);
    for (int_g i = 0; i < n; i++) {
      grid[i][0] = real_g(coords[i][0]);
      grid[i][1] = real_g(coords[i][1]);
      grid[i][2] = real_g(coords[i][2]);
    }
    std_coords(grid, shifts, n, inverse, 2);
    for (int_g i = 0; i < n; i++) {
      real_g x = grid[i][0];
      real_g y = grid[i][1];
      grid[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y;
      grid[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y;
      grid[i][2] = real_g(coords[i][2]);
    }
    coord_type (*prim)[3] = new_local_array2(coord_type, nnodes, 3);
    get_cartesian_coords(prim, nnodes);
    for (int_g i = 0; i < n; i++) {
      int_g j;
      for (j = 0; j < nnodes; j++) {
	if (abs(real_l(grid[i][0]) - prim[j][0]) < cart_tol and
	    abs(real_l(grid[i][1]) - prim[j][1]) < cart_tol and
	    abs(real_l(grid[i][2]) - prim[j][2]) < cart_tol)
	  break;
      }
      if (j == nnodes)
	fprintf(stderr, "Get atom shifts failed to find atom\n");
      prim_id[i] = j;
    }
    delete_local_array(prim);
    delete_local_array(grid);
  }
}

void DLV::atom_tree::get_atom_shifts(const coord_type coords[][3],
				     int_g shifts[][3], int_g prim_id[],
				     const int_g n, const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3]) const
{
  if (nnodes > 0) {
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    real_g (*grid)[3] = new_local_array2(real_g, n, 3);
    for (int_g i = 0; i < n; i++) {
      grid[i][0] = real_g(coords[i][0]);
      grid[i][1] = real_g(coords[i][1]);
      grid[i][2] = real_g(coords[i][2]);
    }
    std_coords(grid, shifts, n, inverse, 3);
    for (int_g i = 0; i < n; i++) {
      real_g x = grid[i][0];
      real_g y = grid[i][1];
      real_g z = grid[i][2];
      grid[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y
	+ real_g(cell[0][2]) * z;
      grid[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y
	+ real_g(cell[1][2]) * z;
      grid[i][2] = real_g(cell[2][0]) * x + real_g(cell[2][1]) * y
	+ real_g(cell[2][2]) * z;
    }
    coord_type (*prim)[3] = new_local_array2(coord_type, nnodes, 3);
    get_cartesian_coords(prim, nnodes);
    for (int_g i = 0; i < n; i++) {
      int_g j;
      for (j = 0; j < nnodes; j++) {
	if (abs(real_l(grid[i][0]) - prim[j][0]) < cart_tol and
	    abs(real_l(grid[i][1]) - prim[j][1]) < cart_tol and
	    abs(real_l(grid[i][2]) - prim[j][2]) < cart_tol)
	  break;
      }
      if (j == nnodes)
	fprintf(stderr, "Get atom shifts failed to find atom\n");
      prim_id[i] = j;
    }
    delete_local_array(prim);
    delete_local_array(grid);
  }
}

void DLV::atom_tree::get_atom_shifts(const coord_type coords[][3],
				     int_g shifts[][3], int_g prim_id[],
				     const int_g n, const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3],
				     const bool surface) const
{
  if (nnodes > 0) {
    const real_g tol = 0.00001;
    real_l cell[3][3];
    for (int_g i = 0; i < 3; i++) {
      cell[i][0] = a[i];
      cell[i][1] = b[i];
      cell[i][2] = c[i];
    }
    real_l inverse3[3][3];
    matrix_invert(cell, inverse3, 3);
    real_l inverse2[3][3];
    matrix_invert(cell, inverse2, 2);
    real_g (*grid3)[3] = new_local_array2(real_g, n, 3);
    real_g (*grid2)[3] = new_local_array2(real_g, n, 3);
    bool *nsalvage = new_local_array1(bool, n);
    for (int_g i = 0; i < n; i++) {
      if (coords[i][2] > real_l(tol)) {
	nsalvage[i] = false;
	grid2[i][0] = real_g(coords[i][0]);
	grid2[i][1] = real_g(coords[i][1]);
	grid2[i][2] = real_g(coords[i][2]);
	grid3[i][0] = 0.0;
	grid3[i][1] = 0.0;
	grid3[i][2] = 0.0;
      } else {
	nsalvage[i] = true;
	grid3[i][0] = real_g(coords[i][0]);
	grid3[i][1] = real_g(coords[i][1]);
	grid3[i][2] = real_g(coords[i][2]);
	grid2[i][0] = 0.0;
	grid2[i][1] = 0.0;
	grid2[i][2] = 0.0;
      }
    }
    std_coords(grid3, shifts, n, inverse3, 3);
    int_g (*pshift)[3] = new_local_array2(int_g, n, 3);
    std_coords(grid2, pshift, n, inverse2, 2);
    for (int_g i = 0; i < n; i++) {
      if (nsalvage[i]) {
	real_g x = grid3[i][0];
	real_g y = grid3[i][1];
	real_g z = grid3[i][2];
	grid3[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y
	  + real_g(cell[0][2]) * z;
	grid3[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y
	  + real_g(cell[1][2]) * z;
	grid3[i][2] = real_g(cell[2][0]) * x + real_g(cell[2][1]) * y
	  + real_g(cell[2][2]) * z;
	if (grid3[i][2] > tol) {
	  grid3[i][0] += real_g(cell[0][2]);
	  grid3[i][1] += real_g(cell[1][2]);
	  grid3[i][2] += real_g(cell[2][2]);
	  shifts[i][2]++;
	}
      } else {
	real_g x = grid2[i][0];
	real_g y = grid2[i][1];
	real_g z = grid2[i][2];
	grid3[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y;
	grid3[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y;
	grid3[i][2] = z;
	shifts[i][0] = pshift[i][0];
	shifts[i][1] = pshift[i][1];
	shifts[i][2] = 0;
      }
    }
    if (nnodes > n)
      fprintf(stderr, "Size failure in get_atom_shifts\n");
    else {
      coord_type (*pgrid)[3] = new_local_array2(coord_type, nnodes, 3);
      get_cartesian_coords(pgrid, nnodes, nsalvage);
      real_g (*prim)[3] = new_local_array2(real_g, nnodes, 3);
      for (int_g i = 0; i < nnodes; i++) {
	if (nsalvage[i]) {
	  prim[i][0] = real_g(pgrid[i][0]);
	  prim[i][1] = real_g(pgrid[i][1]);
	  prim[i][2] = real_g(pgrid[i][2]);
	} else {
	  grid2[i][0] = real_g(pgrid[i][0]);
	  grid2[i][1] = real_g(pgrid[i][1]);
	  grid2[i][2] = real_g(pgrid[i][2]);
	}
      }
      delete_local_array(pgrid);
      std_coords(prim, pshift, nnodes, inverse3, 3);
      std_coords(grid2, pshift, nnodes, inverse2, 2);
      for (int_g i = 0; i < nnodes; i++) {
	if (nsalvage[i]) {
	  real_g x = prim[i][0];
	  real_g y = prim[i][1];
	  real_g z = prim[i][2];
	  prim[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y
	    + real_g(cell[0][2]) * z;
	  prim[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y
	    + real_g(cell[1][2]) * z;
	  prim[i][2] = real_g(cell[2][0]) * x + real_g(cell[2][1]) * y
	    + real_g(cell[2][2]) * z;
	  if (prim[i][2] > tol) {
	    prim[i][0] += real_g(cell[0][2]);
	    prim[i][1] += real_g(cell[1][2]);
	    prim[i][2] += real_g(cell[2][2]);
	  }
	} else {
	  real_g x = grid2[i][0];
	  real_g y = grid2[i][1];
	  real_g z = grid2[i][2];
	  prim[i][0] = real_g(cell[0][0]) * x + real_g(cell[0][1]) * y;
	  prim[i][1] = real_g(cell[1][0]) * x + real_g(cell[1][1]) * y;
	  prim[i][2] = z;
	}
      }
      for (int_g i = 0; i < n; i++) {
	int_g j;
	for (j = 0; j < nnodes; j++) {
	  if (abs(grid3[i][0] - prim[j][0]) < real_g(cart_tol) and
	      abs(grid3[i][1] - prim[j][1]) < real_g(cart_tol) and
	      abs(grid3[i][2] - prim[j][2]) < real_g(cart_tol))
	    break;
	}
	if (j == nnodes)
	  fprintf(stderr, "Get atom shifts failed to find atom\n");
	prim_id[i] = j;
      }
      delete_local_array(prim);
    }
    delete_local_array(pshift);
    delete_local_array(nsalvage);
    delete_local_array(grid2);
    delete_local_array(grid3);
  }
}

bool DLV::atom_tree::group_atoms_from_selections(std::map<int, atom> &basis,
						 const string gp,
						 const string label,
						 const bool all,
						 const string remainder) const
{
  bool no_overlaps = true;
  if (nnodes > 0) {
    std::list<atom_tree_node *>::const_iterator ptr;
    for (ptr = selections.begin(); ptr != selections.end(); ++ptr ) {
      bool ok = (*ptr)->get_atom()->add_group(gp, label);
      no_overlaps = no_overlaps and ok;
    }
    if (all) {
      std::map<int, atom>::iterator x;
      // relies on add_group not over-writing existing label
      for (x = basis.begin(); x != basis.end(); ++x )
	x->second->add_group(gp, remainder);
    }
  }
  return no_overlaps;
}

void DLV::atom_tree_node::layer_groups(const int_g lindex[], const string group,
				       int_g &index) const
{
  // must match get_cartesian_coords
  if (left_child != 0)
    left_child->layer_groups(lindex, group, index);
  atom x = get_atom();
  char buff[32];
  snprintf(buff, 32, "layer %d", lindex[index]);
  (void) x->add_group(group, buff);
  index++;
  if (right_child != 0)
    right_child->layer_groups(lindex, group, index);
}

void DLV::atom_tree::layer_groups(const int_g layers[], const string gp) const
{
  if (nnodes > 0) {
    int_g index = 0;
    root->layer_groups(layers, gp, index);
  }
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::atom_tree_node *t,
				    const unsigned int file_version)
    {
      ar << t->get_data();
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_tree_node *t,
				    const unsigned int file_version)
    {
      DLV::atom a;
      DLV::atom_position data(a);
      ar >> data;
      ::new(t)DLV::atom_tree_node(data, 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::crystal03_id_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::crystal03_id_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::core_electrons_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::core_electrons_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::debye_waller_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::debye_waller_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::displacements_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::displacements_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rod_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::rod_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::charge_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::charge_type(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::isotope_type *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::isotope_type(0);
    }
  }
}

template <class Archive>
inline void DLV::atom_tree_node::serialize(Archive &ar,
					   const unsigned int version)
{
  // Todo - we don't have any nodes loaded!
  ar & data;
}

template <class Archive>
inline void DLV::atom_info_type::serialize(Archive &ar,
					   const unsigned int version)
{
}

template <class Archive>
inline void DLV::crystal03_id_type::serialize(Archive &ar,
					      const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & id;
}

template <class Archive>
inline void DLV::core_electrons_type::serialize(Archive &ar,
						const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & electrons;
}

template <class Archive>
inline void DLV::charge_type::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & charge;
}

template <class Archive>
inline void DLV::isotope_type::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & isotope;
}

template <class Archive>
inline void DLV::debye_waller_type::serialize(Archive &ar,
					      const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & dw1;
  ar & dw2;
}

template <class Archive>
inline void DLV::displacements_type::serialize(Archive &ar,
					       const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & disps;
}

template <class Archive>
inline void DLV::rod_type::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_info_type>(*this);
  ar & dw1;
  ar & dw2;
  ar & noccup;	   
  ar & xconst;  
  ar & nxdis;	   
  ar & x2const; 
  ar & nx2dis;	   
  ar & yconst;  
  ar & nydis;	   
  ar & y2const; 
  ar & ny2dis;	   
  ar & nzdis;     
}

template <class Archive>
inline void DLV::atom_type::serialize(Archive &ar, const unsigned int version)
{
  ar & id;
  ar & atomic_number;
  ar & valid_cartesian_coords;
  ar & valid_fractional_coords;
  //ar & valid_zmatrix;
  //ar & gen_coords_from_zmatrix;
  ar & inherit_colours;
  ar & inherit_charge;
  ar & inherit_radius;
  ar & colour;
  ar & default_charge;
  ar & radius;
  ar & spin;
  ar & cartesian_coords;
  ar & fractional_coords;
  //ar & z_matrix_atom1;
  //ar & z_matrix_atom2;
  //ar & z_matrix_atom3;
  //ar & zmatrix_value1;
  //ar & zmatrix_value2;
  //ar & zmatrix_value3;
  ar & occupancy;
  ar & partial_occupancy;
  ar & data_flag;
  ar & groups;
  ar & properties;
  ar & special_periodicity;
  ar & edit_flag;
}

template <class Archive>
inline void DLV::atom_position::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & atom_ref;
  ar & cartesian_coords;
  ar & fractional_coords;
  ar & shift;
}

template <class Archive>
void DLV::atom_tree::serialize(Archive &ar, const unsigned int version)
{
  // Todo - unless we serialize the nodes this may not work.
  nnodes = 0;
  ar & selections;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_tree_node)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::crystal03_id_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::core_electrons_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::charge_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::isotope_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::debye_waller_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::displacements_type)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rod_type)
//BOOST_CLASS_EXPORT_GUID(DLV::atom_info_type, "DLV::atom_info_type")
//BOOST_CLASS_EXPORT_GUID(DLV::atom_type, "DLV::atom_info_type")
//BOOST_CLASS_EXPORT_GUID(DLV::atom_position, "DLV::atom_position")
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_tree)

DLV_NORMAL_EXPLICIT(DLV::atom_tree_node)
DLV_NORMAL_EXPLICIT(DLV::crystal03_id_type)
DLV_NORMAL_EXPLICIT(DLV::core_electrons_type)
DLV_NORMAL_EXPLICIT(DLV::charge_type)
DLV_NORMAL_EXPLICIT(DLV::isotope_type)
DLV_NORMAL_EXPLICIT(DLV::debye_waller_type)
DLV_NORMAL_EXPLICIT(DLV::displacements_type)
DLV_NORMAL_EXPLICIT(DLV::rod_type)
DLV_NORMAL_EXPLICIT(DLV::atom_tree)
DLV_NORMAL_EXPLICIT(DLV::atom_type)

#endif // DLV_USES_SERIALIZE
