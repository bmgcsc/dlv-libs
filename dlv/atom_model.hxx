
#ifndef DLV_MODEL_ATOMS
#define DLV_MODEL_ATOMS

#include <list>
#include <map>
#include <vector>

namespace DLV {

  enum uhf_spin_type { no_spin, alpha_spin, beta_spin };

  typedef shared_ptr<class atom_type> atom;

  // forward declare
  template <class data> class pair_data;
  typedef pair_data<bool> bond_data;

  struct bond_list {
    real_g start[3];
    real_g middle[3];
    real_g end[3];
    colour_data colour1;
    colour_data colour2;
    int_g type1;
    int_g type2;
  };

  class atom_info_type {
  public:
    virtual ~atom_info_type();

  protected:
    atom_info_type();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_type {
  public:
    atom_type();
    ~atom_type();

    static int_g check_atomic_symbol(const char symbol[]);
    static string get_atomic_symbol(const int_g atomic_number);

    void set_atomic_number(const int_g atn);
    void set_atomic_number_and_props(const int_g atn);
    void set_charge(const int_g charge);
    void set_radius(const real_g r);
    void set_occupancy(const real_g o);
    void set_colour(const real_g r, const real_g g, const real_g b);
    void set_spin(const uhf_spin_type s);
    void set_properties(const int_g c, real_g &r, const real_g red,
			const real_g green, const real_g blue, const int_g s,
			const bool use_charge, const bool use_radius,
			const bool use_colour, const bool use_spin);
    void set_cartesian_coords(const coord_type c[3]);
    void set_fractional_coords(const coord_type c[3]);
    void set_crystal03_id(const int_g id);
    void set_core_electrons(const int_g n);
    void set_point_charge(const real_l c);
    void set_isotope(const int_g i);
    void set_debye_waller(const real_g i, const real_g j=0.0);
    void set_displacements(const real_g d[3]);
    void set_rod_ids(const int_g d=0, const int_g d2=0, const int_g nocc=0,
		     const real_g xc1=0, const int_g nx1=0, 
		     const real_g xc2=0, const int_g nx2=0, 
		     const real_g yc1=0, const int_g ny1=0, 
		     const real_g yc2=0, const int_g ny2=0,
		     const int_g nz=0);
    void set_rod_dw1_id(const int_g d);
    void set_rod_dw2_id(const int_g d);
    void set_rod_occ_id(const int_g d);
    void get_cartesian_coords(coord_type c[3]) const;
    void get_fractional_coords(coord_type c[3]) const;
    int_g get_atomic_number() const;
    int_g get_crystal03_id() const;
    int_g get_core_electrons() const;
    real_l get_point_charge() const;
    int_g get_isotope() const;
    real_g get_debye_waller1() const;
    real_g get_debye_waller2() const;
    void get_displacements(real_g d[3]) const;
    int_g get_rod_dw1() const;
    int_g get_rod_dw2() const;
    int_g get_rod_noccup() const;
    real_g get_rod_xconst() const;
    int_g get_rod_nxdis() const;
    real_g get_rod_x2const() const;
    int_g get_rod_nx2dis() const;
    real_g get_rod_yconst() const;
    int_g get_rod_nydis() const;
    real_g get_rod_y2const() const;
    int_g get_rod_ny2dis() const;
    int_g get_rod_nzdis() const;
    uhf_spin_type get_uhf_spin() const;
    int_g get_charge() const;
    void get_colour(colour_data &c) const;
    real_g get_radius() const;
    real_g get_occupancy() const;
    void get_properties(int_g &c, real_g &r, real_g &red, real_g &green,
			real_g &blue, int_g &s, bool &use_charge,
			bool &use_radius, bool &use_colour,
			bool &use_spin) const;
    bool atom_flag_is_ok() const;
    atom_flag_type get_atom_flag() const;
    void set_atom_flag(const atom_flag_type flag);
    void clear_flag();
    void set_edit_flag();
    void clear_edit_flag();
    bool get_edit_flag() const;
    void complete(const coord_type va[3], const coord_type vb[3],
		  const coord_type vc[3], const int_g dim);

    bool same_type(const atom_type *a) const;

    string get_group(const string gpname) const;
    void set_group(const string gpname, const string label);
    bool add_group(const string gpname, const string label);
    void clear_group(const string gpname);

    bool has_partial_occupancy() const;
    bool has_special_periodicity() const;
    void set_special_periodicity(const bool p = true);

    void set_id(const int_g i);
    int_g get_id() const;

  private:
    int_g id;
    int_g atomic_number;
    bool valid_cartesian_coords;
    bool valid_fractional_coords;
    //bool valid_zmatrix;
    //bool gen_coords_from_zmatrix;
    bool inherit_colours;
    bool inherit_charge;
    bool inherit_radius;
    colour_data colour;
    int_g default_charge;
    real_g radius;
    uhf_spin_type spin;
    coord_type cartesian_coords[3];
    coord_type fractional_coords[3];
    // Todo - Paul wants variables
    //atom z_matrix_atom1;
    //atom z_matrix_atom2;
    //atom z_matrix_atom3;
    //coord_type zmatrix_value1;
    //coord_type zmatrix_value2;
    //coord_type zmatrix_value3;
    real_g occupancy;
    bool partial_occupancy;
    atom_flag_type data_flag;
    std::map<string, string> groups;
    // Todo - do we use this?
    std::map<const string, class atom_info_type *> properties;
    bool special_periodicity;
    bool edit_flag;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_position {
  public:
    atom_position(const atom a);
    int_g compare(const atom_position &a) const;
    bool compare_translation(const atom_position &a, const int_g dim) const;
    bool is_near_position(const coord_type coords[3]) const;
    bool is_same_shift(const atom_position &a) const;
    void set_coords(const coord_type coords[3]);
    void set_coords(const coord_type coords[3], const real_l matrix[3][3],
		    const real_l inverse[3][3], const int_g dim);
    //void set_fractional(const real_l inv[3][3]);
    void centre(const real_l matrix[3][3], const int_g dim,
		const bool flags[3]);
    //void set_fractional_coords(const coord_type coords[3]);
    void offset_coords(const coord_type coords[3], const coord_type cell[3]);
    void translate(const real_l op[3], const real_l matrix[3][3],
		   const int_g dim);
    void transform_cartesian(const real_l rop[3][3], const real_l top[3]);
    void rotate_cartesian(const real_l op[3][3]);
    int_g get_atomic_number() const;
    int_g get_crystal03_id() const;
    void get_cartesian_coords(coord_type coords[3]) const;
    void get_fractional_coords(coord_type coords[3]) const;
    void get_shift(coord_type coords[3]) const;
    void get_colour(colour_data &c) const;
    real_g get_radius() const;
    bool atom_flag_is_ok() const;
    atom_flag_type get_atom_flag() const;
    void set_atom_flag(const atom_flag_type flag);
    atom get_atom() const;
    bool same_inequivalent(const atom_position &a) const;
    int_g locate_coord(const real_g coords[][3], coord_type offset[3],
		     const int_g n, const real_l inverse[3][3],
		     const int_g dim) const;

  private:
    // Todo - should be const
    atom atom_ref;
    coord_type cartesian_coords[3];
    coord_type fractional_coords[3];
    coord_type shift[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
    //{
    // ar & atom_ref;
    //ar & cartesian_coords;
    //ar & fractional_coords;
    //ar & shift;
    //}
#endif // DLV_USES_SERIALIZE
  };

  class atom_tree {
  public:
    atom_tree();
    ~atom_tree();

    int_g size() const;
    void clear();
    void insert(const atom_position &atm, const bool cell = false);
    void insert(const atom_position &atm, const class atom_tree_node *ptr,
		const bool cell = false);
    void copy(atom_tree &tree) const;
    void copy_and_centre(atom_tree &tree, const real_l matrix[3][3],
			 const int_g dim, const bool shift[3]) const;
    void get_atom_types(int_g atom_types[], const int_g n) const;
    void get_atom_regions(const char group[], int_g regions[],
			  const int_g n) const;
    void get_asym_indices(int_g indices[], const int_g n,
			  const std::map<int, atom> &basis) const;
    void get_cartesian_coords(coord_type coords[][3], const int_g n) const;
    void get_cartesian_coords(coord_type coords[][3],
			      const int_g n, bool p[]) const;
    void get_fractional_coords(coord_type coords[][3], const int_g n) const;
    void count_atom_types(int_g types[], const int_g n) const;
    void count_crystal03_ids(int_g types[]) const;
    int_g count_atoms(const bool only_selections, const bool ignore_selections,
		    const bool transparent_flags, const bool use_edit_flags,
		    const bool edit) const;
    void fill_arrays(real_g coords[][3], real_g colours[][3], real_g radii[],
		     const bool only_selected, const bool ignore_selections,
		     const bool use_flags, const int_g flag_type,
		     const std::vector<real_g (*)[3]> *traj,
		     const int_g nframes, const int_g n,
		     const bool use_edit_flags, const bool edit) const;
    void get_z_range(real_l &zmin, real_l &zmax) const;
    void add_conv_cell_atoms(atom_tree &tree, const coord_type a[3],
			     const coord_type b[3], const real_l ops[][3],
			     const int_g nops, const bool centre[3]) const;
    void add_conv_cell_atoms(atom_tree &tree, const coord_type a[3],
			     const coord_type b[3], const coord_type c[3],
			     const real_l ops[][3], const int_g nops,
			     const bool centre[3]) const;
    void replicate_atoms(const coord_type a[3], const real_l tol,
			 const int_g na, const bool centre, const bool edges);
    void replicate_atoms(const coord_type a[3], const coord_type b[3],
			 const real_l tol, const int_g na, const int_g nb,
			 const bool centre, const bool edges);
    void replicate_atoms(const coord_type a[3], const coord_type b[3],
			 const coord_type c[3], const real_l tol,
			 const int_g na, const int_g nb, const int_g nc,
			 const bool centre, const bool edges);
    void replicate_surface(const coord_type a[3], const coord_type b[3],
			   const coord_type c[3], const real_l tol,
			   const int_g na, const int_g nb, const int_g nc,
			   const bool centre, const bool edges);
    void generate_bonds(std::list<bond_list> info[], const int_g nframes,
			const real_g overlap, const bool invert_colours,
			const bond_data &bonds, const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const bool edit) const;
    void generate_bonds(const coord_type a[3], const int_g na,
			std::list<bond_list> info[], const int_g nframes,
			const real_g overlap, const bool centre,
			const bool invert_colours, const bond_data &bonds,
			const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const bool gamma, const bool edit) const;
    void generate_bonds(const coord_type a[3], const coord_type b[3],
			const int_g na, const int_g nb,
			std::list<bond_list> info[], const int_g nframes,
			const real_g overlap, const bool centre,
			const bool invert_colours, const bond_data &bonds,
			const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const bool gamma, const bool edit) const;
    void generate_bonds(const coord_type a[3], const coord_type b[3],
			const coord_type c[3], const int_g na, const int_g nb,
			const int_g nc, std::list<bond_list> info[],
			const int_g nframes, const real_g overlap,
			const bool centre, const bool invert_colours,
			const bond_data &bonds, const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const bool gamma, const bool edit) const;
    void surface_bonds(const coord_type a[3], const coord_type b[3],
		       const coord_type c[3], const int_g na, const int_g nb,
		       const int_g nc, std::list<bond_list> info[],
		       const int_g nframes, const real_g overlap,
		       const bool centre, const bool invert_colours,
		       const bond_data &bonds, const real_l tol,
		       const std::vector<real_g (*)[3]> *traj,
		       const bool gamma, const bool edit) const;
    void select(const real_g origin[3], const real_g offset, const bool active,
		const int_g symmetry_select, const bool select_groups,
		char message[], const int_g mlen);
    void copy_selected_atoms(atom_tree &tree, const int_g symmetry_select,
			     const string group, const bool force = false,
			     const int_g dim = 0) const;
    void update_selected_atoms(const int_g symmetry_select,
			       const string group);
    void deselect(char message[], const int_g mlen);
    int_g get_atom_selection_info(real_l &x, real_l &y, real_l &z,
				real_l &length, real_l &x2, real_l &y2,
				real_l &z2, real_l &angle, char sym1[],
				char sym2[], char sym3[], real_g &r1,
				real_g &r2, real_g &r3, const string gp,
				string &label) const;
    bool get_selected_positions(coord_type p[3]) const;
    bool get_selected_positions(coord_type p1[3], coord_type p2[3]) const;
    bool get_selected_positions(coord_type p1[3], coord_type p2[3],
				coord_type p3[3]) const;
    bool get_average_selection_position(coord_type p[3]) const;

    bool get_selected_pair(atom &atom1, atom &atom2) const;
    void set_selected_atom_flags(const bool done);
    bool set_selected_index_and_flags(int_g indices[], const int_g value,
				      const bool set_def,
				      const std::map<int, atom> &basis,
				      char message[], const int_g mlen);
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], const int_g n) const;
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], const int_g n,
			 const coord_type a[3]) const;
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], const int_g n, const coord_type a[3],
			 const coord_type b[3]) const;
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], const int_g n, const coord_type a[3],
			 const coord_type b[3], const coord_type c[3]) const;
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], const int_g n, const coord_type a[3],
			 const coord_type b[3], const coord_type c[3],
			 const bool surface) const;

    void map_data(const real_g grid[][3], int_g **const data, const int_g ndata,
		  const int_g ngrid, real_g (*map_grid)[3],
		  int_g **map_data, int_g n) const;
    void map_data(const real_g grid[][3], int_g **const data, const int_g ndata,
		  const int_g ngrid, real_g (*map_grid)[3],
		  int_g **map_data, int_g n, const coord_type a[3]) const;
    void map_data(const real_g grid[][3], int_g **const data, const int_g ndata,
		  const int_g ngrid, real_g (*map_grid)[3],
		  int_g **map_data, int_g n, const coord_type a[3],
		  const coord_type b[3]) const;
    void map_data(const real_g grid[][3], int_g **const data, const int_g ndata,
		  const int_g ngrid, real_g (*map_grid)[3],
		  int_g **map_data, int_g n, const coord_type a[3],
		  const coord_type b[3], const coord_type c[3]) const;
    void map_data(const real_g grid[][3], real_g **const data,
		  const int_g ndata, const int_g ngrid, real_g (*map_grid)[3],
		  real_g **map_data, int_g n) const;
    void map_data(const real_g grid[][3], real_g **const data,
		  const int_g ndata, const int_g ngrid, real_g (*map_grid)[3],
		  real_g **map_data, int_g n, const coord_type a[3]) const;
    void map_data(const real_g grid[][3], real_g **const data,
		  const int_g ndata, const int_g ngrid, real_g (*map_grid)[3],
		  real_g **map_data, int_g n, const coord_type a[3],
		  const coord_type b[3]) const;
    void map_data(const real_g grid[][3], real_g **const data,
		  const int_g ndata, const int_g ngrid, real_g (*map_grid)[3],
		  real_g **map_data, int_g n, const coord_type a[3],
		  const coord_type b[3], const coord_type c[3]) const;
    void map_data(const real_g grid[][3], const real_g mag[][3],
		  const real_g phases[][3], const int_g ngrid,
		  real_g (* &map_grid)[3], real_g (* &map_data)[3],
		  int_g &n) const;
    void map_data(const real_g grid[][3], const real_g mag[][3],
		  const real_g phases[][3], const int_g ngrid,
		  real_g (* &map_grid)[3], real_g (* &map_data)[3], int_g &n,
		  const real_g ka, const coord_type a[3]) const;
    void map_data(const real_g grid[][3], const real_g mag[][3],
		  const real_g phases[][3], const int_g ngrid,
		  real_g (* &map_grid)[3], real_g (* &map_data)[3], int_g &n,
		  const real_g ka, const real_g kb, const coord_type a[3],
		  const coord_type b[3]) const;
    void map_data(const real_g grid[][3], const real_g mag[][3],
		  const real_g phases[][3], const int_g ngrid,
		  real_g (* &map_grid)[3], real_g (* &map_data)[3], int_g &n,
		  const real_g ka, const real_g kb, const real_g kc,
		  const coord_type a[3], const coord_type b[3],
		  const coord_type c[3]) const;
    void map_data(const real_g grid[][3], const real_g mags[][3],
		  const real_g phases[][3], const int_g ngrid,
		  std::vector<real_g (*)[3]> &new_data, int_g &n,
		  const real_g scale, const int_g nframes,
		  const int_g ncopies) const;
    void map_data(const real_g grid[][3], const real_g mags[][3],
		  const real_g phases[][3], const int_g ngrid,
		  std::vector<real_g (*)[3]> &new_data, int_g &n,
		  const real_g scale, const int_g nframes, const int_g ncopies,
		  const real_g ka, const coord_type a[3]) const;
    void map_data(const real_g grid[][3], const real_g mags[][3],
		  const real_g phases[][3], const int_g ngrid,
		  std::vector<real_g (*)[3]> &new_data, int_g &n,
		  const real_g scale, const int_g nframes, const int_g ncopies,
		  const real_g ka, const real_g kb, const coord_type a[3],
		  const coord_type b[3]) const;
    void map_data(const real_g grid[][3], const real_g mags[][3],
		  const real_g phases[][3], const int_g ngrid,
		  std::vector<real_g (*)[3]> &new_data, int_g &n,
		  const real_g scale, const int_g nframes, const int_g ncopies,
		  const real_g ka, const real_g kb, const real_g kc,
		  const coord_type a[3], const coord_type b[3],
		  const coord_type c[3]) const;
    void map_data(const real_g grid[][3], const int_g ngrid,
		  const std::vector<real_g (*)[3]> &traj,
		  std::vector<real_g (*)[3]> &new_data,
		  const int_g nframes) const;
    void map_data(const real_g grid[][3], const int_g ngrid,
		  const std::vector<real_g (*)[3]> &traj,
		  std::vector<real_g (*)[3]> &new_data, const int_g nframes,
		  const coord_type a[3]) const;
    void map_data(const real_g grid[][3], const int_g ngrid,
		  const std::vector<real_g (*)[3]> &traj,
		  std::vector<real_g (*)[3]> &new_data, const int_g nframes,
		  const coord_type a[3], const coord_type b[3]) const;
    void map_data(const real_g grid[][3], const int_g ngrid,
		  const std::vector<real_g (*)[3]> &traj,
		  std::vector<real_g (*)[3]> &new_data, const int_g nframes,
		  const coord_type a[3], const coord_type b[3],
		  const coord_type c[3]) const;
    bool map_atom_selections(const atom_tree &primitives, int_g &n,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, const bool all_atoms,
			     const real_g grid[][3], int_g **const info,
			     const int_g ninfo, const int_g ngrid) const;
    bool map_atom_selections(const atom_tree &primitives, int_g &n,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, const bool all_atoms,
			     const real_g grid[][3], int_g **const info,
			     const int_g ninfo, const int_g ngrid,
			     const coord_type a[3]) const;
    bool map_atom_selections(const atom_tree &primitives, int_g &n,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, const bool all_atoms,
			     const real_g grid[][3], int_g **const info,
			     const int_g ninfo, const int_g ngrid,
			    const coord_type a[3], const coord_type b[3]) const;
    bool map_atom_selections(const atom_tree &primitives, int_g &n,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, const bool all_atoms,
			     const real_g grid[][3], int_g **const info,
			     const int_g ninfo, const int_g ngrid,
			     const coord_type a[3], const coord_type b[3],
			     const coord_type c[3]) const;
    bool map_selected_atoms(int_g &n, int_g * &labels, int_g (* &shifts)[3],
			    const real_g grid[][3], int_g **const info,
			    const int_g ninfo, const int_g ngrid) const;
    bool map_selected_atoms(int_g &n, int_g * &labels, int_g (* &shifts)[3],
			    const real_g grid[][3], int_g **const info,
			    const int_g ninfo, const int_g ngrid,
			    const coord_type a[3]) const;
    bool map_selected_atoms(int_g &n, int_g * &labels, int_g (* &shifts)[3],
			    const real_g grid[][3], int_g **const info,
			    const int_g ninfo, const int_g ngrid,
			    const coord_type a[3], const coord_type b[3]) const;
    bool map_selected_atoms(int_g &n, int_g * &labels, int_g (* &shifts)[3],
			    const real_g grid[][3], int_g **const info,
			    const int_g ninfo, const int_g ngrid,
			    const coord_type a[3], const coord_type b[3],
			    const coord_type c[3]) const;
    int_g get_number_selected_atoms() const;
    int_g locate_atom(const atom_position &p) const;
    int_g locate_selected_atom() const;
    int_g find_primitive_selection(const std::map<int, atom> &basis) const;
    bool is_selected(const atom atm) const;

    static void copy_bonds(std::map<int, atom> &basis, bond_data &bonds,
			   const bond_data &pbonds, const atom prim[]);
    static void edit_bonds(std::map<int, atom> &basis, bond_data &bonds,
			   const bond_data &pbonds, const atom prim[]);
    void copy_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			      const bond_data &pbonds) const;
    void copy_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			      const bond_data &pbonds, atom * &prim,
			      const real_l shift[3]) const;
    void copy_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			      const bond_data &pbonds, atom * &prim,
			      const int_g atn) const;
    void add_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			     const bond_data &pbonds, atom * &prim,
			     const int_g size, const real_l shift[3],
			     const string label, const string group) const;
    void add_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			     const bond_data &pbonds, atom * &prim,
			     const int_g size, const real_l zshift,
			     const string label, const string group) const;
    void delete_selected_atoms(std::map<int, atom> &basis, bond_data &bonds,
			       const bond_data &pbonds, atom * &prim) const;
    void edit_atoms_and_bonds(std::map<int, atom> &basis, bond_data &bonds,
			      const bond_data &pbonds, atom * &prim,
			      const bool keep_sym, const bool update) const;
    void copy_atom_layers_and_bonds(std::map<int, atom> &basis,
				    bond_data &bonds, const bond_data &pbonds,
				    atom * &prim, const real_l caxis[3],
				    const int_g ncells, const int_g term,
				    const int_g lcopies[], const int_g lindex[],
				    const string group, const bool slab = true,
				    const int_g salvage = 0) const;
    void build_cluster_atoms_and_bonds(std::map<int, atom> &basis,
				       bond_data &bonds,
				       const bond_data &pbonds,
				       atom * &prim, const int_g n,
				       const string gpname,
				       const real_l x, const real_l y,
				       const real_l z) const;
    void build_cluster_atoms_and_bonds(std::map<int, atom> &basis,
				       bond_data &bonds,
				       const bond_data &pbonds,
				       atom * &prim, const real_l r,
				       const string gpname,
				       const real_l x, const real_l y,
				       const real_l z) const;
    void build_wulff_shape(std::map<int, atom> &basis, bond_data &bonds,
			   const bond_data &pbonds, atom * &prim,
			   const real_l verts[][3], const int_g nvertices,
			   int_g **face_vertices, const int_g nface_vertices[],
			   const int_g nfaces, const real_g offsets[]) const;
    void build_box(std::map<int, atom> &basis, bond_data &bonds,
		   const bond_data &pbonds, atom * &prim,
		   const real_g verts[][3], const int_g nvertices,
		   const int_g planes[], const int_g nplanes,
		   const int_g planes_per_obj[], const int_g nobjs) const;
    void build_cylinders(std::map<int, atom> &basis, bond_data &bonds,
			 const bond_data &pbonds, const real_g iradius[],
			 const real_g oradius[], const real_g zmin[],
			 const real_g zmax[], const int orient[],
			 const int ncylinders) const;

    bool group_atoms_from_selections(std::map<int, atom> &basis,
				     const string gp, const string label,
				     const bool all,
				     const string remainder) const;
    void layer_groups(const int_g layers[], const string gp) const;

    static void order_distances(int_g order[], const real_l distance[],
				const int_g n);

  private:
    class atom_tree_node *root;
    int_g nnodes;
    std::list<atom_tree_node *> selections;

    void store_atom(const atom_tree_node *atm, const real_l cella,
		    const real_l cellb, const real_l cellc,
		    const coord_type a[3], const coord_type b[3],
		    const coord_type c[3]);
    static void std_coords(real_g coords[][3], int_g shifts[][3],
			   const int_g n, const real_l inverse[3][3],
			   const int_g dim);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::atom_tree)
#endif // DLV_USES_SERIALIZE

inline DLV::atom_type::atom_type() :
  atomic_number(0), valid_cartesian_coords(false),
  valid_fractional_coords(false), /*valid_zmatrix(false),
  gen_coords_from_zmatrix(false),*/ inherit_colours(true),
  inherit_charge(true), inherit_radius(true), default_charge(0), radius(0.0),
  spin(no_spin), occupancy(1.0), partial_occupancy(false), 
  data_flag(flag_unset), special_periodicity(false), edit_flag(false)
{
  cartesian_coords[0] = 0.0;
  cartesian_coords[1] = 0.0;
  cartesian_coords[2] = 0.0;
  fractional_coords[0] = 0.0;
  fractional_coords[1] = 0.0;
  fractional_coords[2] = 0.0;
}

inline void DLV::atom_type::set_atomic_number(const int_g atn)
{
  atomic_number = atn;
  groups["Atom Types"] = get_atomic_symbol(atn);
}

inline void DLV::atom_type::set_atomic_number_and_props(const int_g atn)
{
  set_atomic_number(atn);
  inherit_charge = true;
  inherit_radius = true;
  inherit_colours = true;
  spin = no_spin;
}

inline void DLV::atom_type::set_charge(const int_g charge)
{
  inherit_charge = false;
  default_charge = charge;
}

inline void DLV::atom_type::set_radius(const real_g r)
{
  inherit_radius = false;
  radius = r;
}

inline void DLV::atom_type::set_occupancy(const real_g o)
{
  if (o < real_g(1.0 - 0.001)) {
    partial_occupancy = true;
    occupancy = o;
  }
}

inline void DLV::atom_type::set_colour(const real_g r, const real_g g,
				       const real_g b)
{
  inherit_colours = false;
  colour = colour_data(r, g, b);
}

inline void DLV::atom_type::set_spin(const uhf_spin_type s)
{
  spin = s;
}

inline void DLV::atom_type::set_cartesian_coords(const coord_type c[3])
{
  valid_cartesian_coords = true;
  valid_fractional_coords = false;
  // valid_zmatrix?
  //gen_coords_from_zmatrix = false;
  cartesian_coords[0] = c[0];
  cartesian_coords[1] = c[1];
  cartesian_coords[2] = c[2];
}

inline void DLV::atom_type::set_fractional_coords(const coord_type c[3])
{
  valid_cartesian_coords = false;
  valid_fractional_coords = true;
  // valid_zmatrix?
  //gen_coords_from_zmatrix = false;
  fractional_coords[0] = c[0];
  fractional_coords[1] = c[1];
  fractional_coords[2] = c[2];
}

inline DLV::int_g DLV::atom_type::get_atomic_number() const
{
  return atomic_number;
}

inline DLV::real_g DLV::atom_type::get_occupancy() const
{
  if (partial_occupancy)
    return occupancy;
  else
    return 1.0;
}

inline DLV::uhf_spin_type DLV::atom_type::get_uhf_spin() const
{
  return spin;
}

inline void DLV::atom_type::clear_flag()
{
  data_flag = flag_unset;
}

inline bool DLV::atom_type::atom_flag_is_ok() const
{
  return (data_flag == flag_done);
}

inline DLV::atom_flag_type DLV::atom_type::get_atom_flag() const
{
  return data_flag;
}

inline void DLV::atom_type::set_atom_flag(const atom_flag_type flag)
{
  if (flag == flag_partial) {
    if (data_flag != flag_done)
      data_flag = flag;
  } else
    data_flag = flag;
}

inline void DLV::atom_type::get_cartesian_coords(coord_type c[3]) const
{
  c[0] = cartesian_coords[0];
  c[1] = cartesian_coords[1];
  c[2] = cartesian_coords[2];
}

inline void DLV::atom_type::get_fractional_coords(coord_type c[3]) const
{
  c[0] = fractional_coords[0];
  c[1] = fractional_coords[1];
  c[2] = fractional_coords[2];
}

inline bool DLV::atom_type::same_type(const atom_type *a) const
{
  return (atomic_number == a->atomic_number);
}

inline DLV::string DLV::atom_type::get_group(const string gpname) const
{
  std::map<string, string>::const_iterator x = groups.find(gpname);
  if (x != groups.end())
    return x->second;
  else
    return "";
}

inline void DLV::atom_type::set_group(const string gpname, const string label)
{
  groups[gpname] = label;
}

inline bool DLV::atom_type::add_group(const string gpname, const string label)
{
  if (groups.find(gpname) != groups.end())
    return (groups[gpname] == label);
  else {
    groups[gpname] = label;
    return true;
  }
}

inline void DLV::atom_type::clear_group(const string gpname)
{
  groups.erase(gpname);
}

inline bool DLV::atom_type::has_partial_occupancy() const
{
  return partial_occupancy;
}

inline bool DLV::atom_type::has_special_periodicity() const
{
  return special_periodicity;
}

inline void DLV::atom_type::set_special_periodicity(const bool p)
{
  special_periodicity = p;
}

inline void DLV::atom_type::set_edit_flag()
{
  edit_flag = true;
}

inline void DLV::atom_type::clear_edit_flag()
{
  edit_flag = false;
}

inline bool DLV::atom_type::get_edit_flag() const
{
  return edit_flag;
}

inline void DLV::atom_type::set_id(const int_g i)
{
  id = i;
}

inline DLV::int_g DLV::atom_type::get_id() const
{
  return id;
}

inline DLV::atom_tree::atom_tree()
  : root(0), nnodes(0)
{
}

inline DLV::atom_position::atom_position(const atom a) : atom_ref(a)
{
}

inline void DLV::atom_position::set_coords(const coord_type coords[3])
{
  cartesian_coords[0] = coords[0];
  cartesian_coords[1] = coords[1];
  cartesian_coords[2] = coords[2];
  fractional_coords[0] = coords[0];
  fractional_coords[1] = coords[1];
  fractional_coords[2] = coords[2];
  shift[0] = 0.0;
  shift[1] = 0.0;
  shift[2] = 0.0;
}

/*inline
void DLV::atom_position::set_fractional_coords(const coord_type coords[3])
{
  fractional_coords[0] = coords[0];
  fractional_coords[1] = coords[1];
  fractional_coords[2] = coords[2];
}*/

inline DLV::int_g DLV::atom_position::get_atomic_number() const
{
  return atom_ref->get_atomic_number();
}

inline
void DLV::atom_position::get_cartesian_coords(coord_type coords[3]) const
{
  coords[0] = cartesian_coords[0];
  coords[1] = cartesian_coords[1];
  coords[2] = cartesian_coords[2];
}

inline
void DLV::atom_position::get_fractional_coords(coord_type coords[3]) const
{
  coords[0] = fractional_coords[0];
  coords[1] = fractional_coords[1];
  coords[2] = fractional_coords[2];
}

inline void DLV::atom_position::get_shift(coord_type coords[3]) const
{
  coords[0] = shift[0];
  coords[1] = shift[1];
  coords[2] = shift[2];
}

inline void DLV::atom_position::get_colour(colour_data &c) const
{
  atom_ref->get_colour(c);
}

inline DLV::real_g DLV::atom_position::get_radius() const
{
  return atom_ref->get_radius();
}

inline DLV::atom DLV::atom_position::get_atom() const
{
  return atom_ref;
}

inline bool DLV::atom_position::atom_flag_is_ok() const
{
  return atom_ref->atom_flag_is_ok();
}

inline DLV::atom_flag_type DLV::atom_position::get_atom_flag() const
{
  return atom_ref->get_atom_flag();
}

inline void DLV::atom_position::set_atom_flag(const atom_flag_type flag)
{
  atom_ref->set_atom_flag(flag);
}

inline bool DLV::atom_position::same_inequivalent(const atom_position &a) const
{
  return (atom_ref == a.atom_ref);
}

inline DLV::int_g DLV::atom_position::get_crystal03_id() const
{
    return atom_ref->get_crystal03_id();
}

inline DLV::int_g DLV::atom_tree::size() const
{
  return nnodes;
}

#endif // DLV_MODEL_ATOMS
