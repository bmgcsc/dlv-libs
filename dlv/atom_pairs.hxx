
#ifndef DLV_PAIR_INFO
#define DLV_PAIR_INFO

#include <list>
#include <map>

namespace DLV {

  // Todo - if only used for bonds would a set be better than a map?

  template <class data> class pair_data {
  public:
    bool set_pair_type(const atom atom1, const atom atom2,
		       const std::map<int, atom> &basis, const data value);
    bool set_pair(const atom atom1, const atom atom2, const data value,
		  const std::map<int, atom> &basis);
    void clear_pair(const atom atom1, const atom atom2);
    bool clear_pair_type(const atom atom1, const atom atom2,
			 const std::map<int, atom> &basis);
    bool clear_pair_type(const int_g atn1, const int_g atn2);
    bool clear_pair(const atom atom1, const atom atom2,
		    const std::map<int, atom> &basis);
    bool clear_pairs();
    bool clear_all();
    void clear_types();
    bool set_all(const data value, const std::map<int, atom> &basis);

    bool is_set(const atom atom1, const atom atom2) const;
    bool is_set(const int_g atn1, const int_g atn2) const;
    bool is_pair_set(const atom atom1, const atom atom2) const;
    bool has_single_pairs() const;
    bool has_type_pairs() const;
    bool has_bonds() const;
    // const data &get_data() const;

    void copy_pairs(const std::map<int, atom> &basis, const pair_data &pdata,
		    const std::map<int, atom> &pbasis);
    void copy_type_bonds(const pair_data &pdata);
    void duplicate_types(const pair_data &pdata, const int_g oldtype,
			 const int_g newtype, const std::map<int, atom> &basis);
    void reset_pair_type(const int_g oldtype, const int_g newtype,
			 const std::map<int, atom> &basis);
    void replicate_pair_type(const int_g oldtype, const int_g newtype,
			     const std::map<int, atom> &basis);
    void reindex(const int_g oldidx, const int_g newidx, const int_g maxidx);

  private:
    std::map< std::pair<int, int>, data> specific;
    std::map< std::pair<int, int>, data> types;

    bool set_pair_type(const int_g atn1, const int_g atn2,
		       const std::map<int, atom> &basis, const data value);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & specific;
      ar & types;
    }
#endif // DLV_USES_SERIALIZE
  };

}

template <class data>
bool DLV::pair_data<data>::is_set(const atom atom1, const atom atom2) const
{
  std::pair<int, int> patoms;
  int_g id1 = atom1->get_id();
  int_g id2 = atom2->get_id();
  if (id1 < id2)
    patoms = std::make_pair(id1, id2);
  else
    patoms = std::make_pair(id2, id1);
  if (specific.find(patoms) != specific.end())
    return true;
  else {
    int_g atn1 = atom1->get_atomic_number();
    int_g atn2 = atom2->get_atomic_number();
    std::pair<int, int> patn;
    if (atn1 < atn2)
      patn = std::make_pair(atn1, atn2);
    else
      patn = std::make_pair(atn2, atn1);
    if (types.find(patn) != types.end())
      return true;
  }
  return false;
}

template <class data>
bool DLV::pair_data<data>::is_set(const int_g atn1, const int_g atn2) const
{
  std::pair<int, int> patn;
  if (atn1 < atn2)
    patn = std::make_pair(atn1, atn2);
  else
    patn = std::make_pair(atn2, atn1);
  if (types.find(patn) != types.end())
    return true;
  return false;
}

template <class data>
bool DLV::pair_data<data>::is_pair_set(const atom atom1,
				       const atom atom2) const
{
  std::pair<int, int> patoms;
  int_g id1 = atom1->get_id();
  int_g id2 = atom2->get_id();
  if (id1 < id2)
    patoms = std::make_pair(id1, id2);
  else
    patoms = std::make_pair(id2, id1);
  if (specific.find(patoms) != specific.end())
    return true;
  return false;
}

template <class data> bool DLV::pair_data<data>::has_bonds() const
{
  return (specific.size() > 0 or types.size() > 0);
}

template <class data> bool DLV::pair_data<data>::has_single_pairs() const
{
  return specific.size() > 0;
}

template <class data> bool DLV::pair_data<data>::has_type_pairs() const
{
  return types.size() > 0;
}

template <class data> bool DLV::pair_data<data>::clear_pairs()
{
  bool changed = false;
  if (specific.size() > 0) {
    specific.clear();
    changed = true;
  }
  return changed;
}

template <class data> bool DLV::pair_data<data>::clear_all()
{
  bool changed = false;
  if (specific.size() > 0) {
    specific.clear();
    changed = true;
  }
  if (types.size() > 0) {
    types.clear();
    changed = true;
  }
  return changed;
}

template <class data> void DLV::pair_data<data>::clear_types()
{
  if (types.size() > 0)
    types.clear();
}

template <class data>
void DLV::pair_data<data>::clear_pair(const atom atom1, const atom atom2)
{
  std::pair<int, int> patoms;
  int_g id1 = atom1->get_id();
  int_g id2 = atom2->get_id();
  if (id1 < id2)
    patoms = std::make_pair(id1, id2);
  else
    patoms = std::make_pair(id2, id1);
  (void) specific.erase(patoms);
}

template <class data>
bool DLV::pair_data<data>::clear_pair(const atom atom1, const atom atom2,
				      const std::map<int, atom> &basis)
{
  std::pair<int, int> patoms;
  int_g id1 = atom1->get_id();
  int_g id2 = atom2->get_id();
  if (id1 < id2)
    patoms = std::make_pair(id1, id2);
  else
    patoms = std::make_pair(id2, id1);
  if (specific.erase(patoms) > 0)
    return true;
  else {
    int_g atn1 = atom1->get_atomic_number();
    int_g atn2 = atom2->get_atomic_number();
    std::pair<int, int> patn;
    if (atn1 < atn2)
      patn = std::make_pair(atn1, atn2);
    else
      patn = std::make_pair(atn2, atn1);
    typename std::map< std::pair< int_g, int_g > , data >::iterator x =
      types.find(patn);
    if (x != types.end()) {
      data d = x->second;
      types.erase(x);
      // Add all ones of this type except the one we're deleting
      // search for all atoms that match the types.
      std::map<int, atom>::const_iterator basis1;
      for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
	atom batom1 = basis1->second;
	if (batom1->same_type(atom1.get())) {
	  std::map<int, atom>::const_iterator basis2;
	  for (basis2 = basis.begin(); basis2 != basis.end(); ++basis2) {
	    atom batom2 = basis2->second;
	    if (batom2->same_type(atom2.get())) {
	      if ((atom1 != batom1 or atom2 != batom2) and
		  (atom1 != batom2 or atom2 != batom1)) {
		id1 = batom1->get_id();
		id2 = batom2->get_id();
		if (id1 < id2)
		  patoms = std::make_pair(id1, id2);
		else
		  patoms = std::make_pair(id2, id1);
		specific[patoms] = d;
	      }
	    }
	  }
	}
      }
    }
    return true;
  }
  //return false;
}

template <class data>
bool DLV::pair_data<data>::clear_pair_type(const atom atom1, const atom atom2,
					   const std::map<int, atom> &basis)

{
  int_g atn1 = atom1->get_atomic_number();
  int_g atn2 = atom2->get_atomic_number();
  std::pair<int, int> patn;
  if (atn1 < atn2)
    patn = std::make_pair(atn1, atn2);
  else
    patn = std::make_pair(atn2, atn1);
  if (types.erase(patn) > 0)
    return true;
  else {
    // delete any specific pairs of this type.
    bool changed = false;
    typename std::map< std::pair< int_g, int_g > , data >::iterator x =
      specific.begin();
    while (x != specific.end()) {
      atom a = basis.find(x->first.first)->second;
      int_g x1 = a->get_atomic_number();
      a = basis.find(x->first.second)->second;
      int_g x2 = a->get_atomic_number();
      if ((x1 == atn1 and x2 == atn2) or (x1 == atn2 and x2 == atn1)) {
	std::pair<int, int> patoms = x->first;
	++x;
	specific.erase(patoms);
	changed = true;
      } else
	++x;
    }
    return changed;
  }
  //return false;
}

template <class data>
bool DLV::pair_data<data>::clear_pair_type(const int_g atn1, const int_g atn2)
{
  std::pair<int, int> patn;
  if (atn1 < atn2)
    patn = std::make_pair(atn1, atn2);
  else
    patn = std::make_pair(atn2, atn1);
  if (types.erase(patn) > 0)
    return true;
  return false;
}

template <class data>
bool DLV::pair_data<data>::set_pair(const atom atom1, const atom atom2,
				    const data value,
				    const std::map<int, atom> &basis)
{
  int_g atn1 = atom1->get_atomic_number();
  int_g atn2 = atom2->get_atomic_number();
  std::pair<int, int> patn;
  if (atn1 < atn2)
    patn = std::make_pair(atn1, atn2);
  else
    patn = std::make_pair(atn2, atn1);
  if (types.find(patn) != types.end())
    return false;
  else {
    std::pair<int, int> patoms;
    int_g id1 = atom1->get_id();
    int_g id2 = atom2->get_id();
    if (id1 < id2)
      patoms = std::make_pair(id1, id2);
    else
      patoms = std::make_pair(id2, id1);
    if (specific.find(patoms) != specific.end())
      return false;
    else {
      // check whether all of this type are now set.
      std::map<int, atom>::const_iterator basis1;
      int_g ca1 = 0;
      for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
	atom batom = basis1->second;
	if (batom->same_type(atom1.get()))
	  ca1++;
      }
      int_g ca2 = 0;
      for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
	atom batom = basis1->second;
	if (batom->same_type(atom2.get()))
	  ca2++;
      }
      // Loop over map to find number of pairs that match types
      int_g count = 0;
      typename std::map< std::pair< int_g, int_g > , data >::iterator x;
      for (x = specific.begin(); x != specific.end(); ++x ) {
	atom a = basis.find(x->first.first)->second;
	int_g x1 = a->get_atomic_number();
	a = basis.find(x->first.second)->second;
	int_g x2 = a->get_atomic_number();
	if ((x1 == atn1 and x2 == atn2) or (x1 == atn2 and x2 == atn1))
	  count++;
      }
      if (count + 1 == (ca1 * (ca2 + 1) / 2))
	// the one we're adding will set all of the type
	return set_pair_type(atom1, atom2, basis, value);
      else
	specific[patoms] = value;
      return true;
    }
  }
}

template <class data>
bool DLV::pair_data<data>::set_pair_type(const atom atom1, const atom atom2,
					 const std::map<int, atom> &basis,
					 const data value)
{
  int_g atn1 = atom1->get_atomic_number();
  int_g atn2 = atom2->get_atomic_number();
  return set_pair_type(atn1, atn2, basis, value);
}

template <class data>
bool DLV::pair_data<data>::set_pair_type(const int_g atn1, const int_g atn2,
					 const std::map<int, atom> &basis,
					 const data value)
{
  std::pair<int, int> patn;
  if (atn1 < atn2)
    patn = std::make_pair(atn1, atn2);
  else
    patn = std::make_pair(atn2, atn1);
  if (types.find(patn) != types.end())
    return false;
  else {
    types[patn] = value;
    // check if specific pairs of this type were set and delete.
    typename std::map< std::pair< int_g, int_g > , data >::iterator x =
      specific.begin();
    while (x != specific.end()) {
      atom a = basis.find(x->first.first)->second;
      int_g x1 = a->get_atomic_number();
      a = basis.find(x->first.second)->second;
      int_g x2 = a->get_atomic_number();
      if ((x1 == atn1 and x2 == atn2) or (x1 == atn2 and x2 == atn1)) {
	std::pair<int, int> patoms = x->first;
	++x;
	specific.erase(patoms);
      } else
	++x;
    }
    return true;
  }
}

template <class data>
bool DLV::pair_data<data>::set_all(const data value,
				   const std::map<int, atom> &basis)
{
  bool changed = false;
  std::map<int, atom>::const_iterator basis1;
  for (basis1 = basis.begin(); basis1 != basis.end(); ++basis1) {
    int_g atn1 = basis1->second->get_atomic_number();
    std::map<int, atom>::const_iterator basis2;
    for (basis2 = basis1; basis2 != basis.end(); ++basis2) {
      int_g atn2 = basis2->second->get_atomic_number();
      std::pair<int, int> patn;
      if (atn1 <= atn2)
	patn = std::make_pair(atn1, atn2);
      else
	patn = std::make_pair(atn2, atn1);
      typename std::map< std::pair<int, int>, data >::iterator x =
	types.find(patn);
      if (x == types.end())
	changed = true;
      types[patn] = value;
    }
  }
  if (changed and specific.size() > 0)
    specific.clear();
  return changed;
}

template <class data>
void DLV::pair_data<data>::copy_pairs(const std::map<int, atom> &basis,
				      const pair_data<data> &pdata,
				      const std::map<int, atom> &pbasis)
{
  if (pdata.specific.size() > 0) {
    // Assumes atoms were copied in same order, see copy_cartesian_atoms
    typename std::map< std::pair<int, int>, data>::const_iterator ptr;
    for (ptr = pdata.specific.begin(); ptr != pdata.specific.end(); ++ptr )
      specific[ptr->first] = ptr->second;
    /*
    std::map<int, atom>::const_iterator basis1 = basis.begin();
    std::map<int, atom>::const_iterator pbasis1;
    for (pbasis1 = pbasis.begin(); pbasis1 != pbasis.end(); ++pbasis1) {
      std::map<int, atom>::const_iterator basis2 = basis1;
      std::map<int, atom>::const_iterator pbasis2;
      for (pbasis2 = pbasis1; pbasis2 != pbasis.end(); ++pbasis2) {
	std::pair<int, int> patoms;
	int_g id1 = pbasis1->second->get_id();
	int_g id2 = pbasis2->second->get_id();
	if (id1 < id2)
	  patoms = std::make_pair(id1, id2);
	else
	  patoms = std::make_pair(id2, id1);
	if (pdata.specific.find(patoms) != pdata.specific.end()) {
	  specific[patoms] = true;
	}
	++basis2;
      }
      ++basis1;
    }
    */
  }
  if (pdata.types.size() > 0) {
    typename std::map< std::pair<int, int>, data>::const_iterator ptr;
    for (ptr = pdata.types.begin(); ptr != pdata.types.end(); ++ptr )
      types[ptr->first] = ptr->second;
  }
}

template <class data>
void DLV::pair_data<data>::copy_type_bonds(const pair_data<data> &pdata)
{
  if (pdata.types.size() > 0) {
    typename std::map< std::pair<int, int>, data>::const_iterator ptr;
    for (ptr = pdata.types.begin(); ptr != pdata.types.end(); ++ptr )
      types[ptr->first] = ptr->second;
  }
}

template <class data>
void DLV::pair_data<data>::duplicate_types(const pair_data &pdata,
					   const int_g oldtype,
					   const int_g newtype,
					   const std::map<int, atom> &basis)
{
  if (pdata.types.size() > 0) {
    typename std::map< std::pair<int, int>, data>::const_iterator ptr;
    for (ptr = pdata.types.begin(); ptr != pdata.types.end(); ++ptr ) {
      if (ptr->first.first == oldtype) {
	if (ptr->first.second == oldtype)
	  set_pair_type(newtype, newtype, basis, true);
	set_pair_type(newtype, ptr->first.second, basis, true);
      }
      if (ptr->first.second == oldtype)
	set_pair_type(newtype, ptr->first.first, basis, true);
    }
  }
}

template <class data> void
DLV::pair_data<data>::reset_pair_type(const int_g oldtype, const int_g newtype,
				      const std::map<int, atom> &basis)
{
  if (types.size() > 0) {
    std::map<int, atom>::const_iterator x;
    for (x = basis.begin(); x != basis.end(); ++x ) {
      int_g atn = x->second->get_atomic_number();
      std::pair<int, int> patn;
      if (oldtype < atn)
	patn = std::make_pair(oldtype, atn);
      else
	patn = std::make_pair(atn, oldtype);
      if (types.find(patn) != types.end()) {
	types.erase(patn);
	std::pair<int, int> natn;
	if (newtype < atn)
	  natn = std::make_pair(newtype, atn);
	else
	  natn = std::make_pair(atn, newtype);
	types[natn] = true;
      }
    }
  }
}

template <class data> void
DLV::pair_data<data>::replicate_pair_type(const int_g oldtype,
					  const int_g newtype,
					  const std::map<int, atom> &basis)
{
  if (types.size() > 0) {
    std::map<int, atom>::const_iterator x;
    for (x = basis.begin(); x != basis.end(); ++x ) {
      int_g atn = x->second->get_atomic_number();
      std::pair<int, int> patn;
      if (oldtype < atn)
	patn = std::make_pair(oldtype, atn);
      else
	patn = std::make_pair(atn, oldtype);
      if (types.find(patn) != types.end()) {
	std::pair<int, int> natn;
	if (newtype < atn)
	  natn = std::make_pair(newtype, atn);
	else
	  natn = std::make_pair(atn, newtype);
	types[natn] = true;
      }
    }
  }
}

template <class data> void
DLV::pair_data<data>::reindex(const int_g oldidx, const int_g newidx,
			      const int_g maxidx)
{
  if (specific.size() > 0) {
    for (int_g i = 0; i < maxidx; i++) {
      std::pair<int, int> pair;
      if (oldidx < i)
	pair = std::make_pair(oldidx, i);
      else
	pair = std::make_pair(i, oldidx);
      typename std::map< std::pair<int, int>, data>::iterator ptr =
	specific.find(pair);
      if (ptr != specific.end()) {
	specific.erase(ptr);
	if (newidx < i)
	  pair = std::make_pair(newidx, i);
	else
	  pair = std::make_pair(i, newidx);
	specific[pair] = true;
      }
    }
  }
}

#endif // DLV_PAIR_INFO
