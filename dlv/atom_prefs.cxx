
#include <cstdio>
#include <iostream>
#include "types.hxx"
#include "atom_prefs.hxx"
#include "boost_lib.hxx"
#include "utils.hxx"
#include "atom_prefs_impl.hxx"

DLV::atomic_prefs *DLV::atomic_data = 0;

namespace {

  const char *atomic_symbols[] = {
    "?",
    "H",
    "He",
    "Li",
    "Be",
    "B",
    "C",
    "N",
    "O",
    "F",
    "Ne",
    "Na",
    "Mg",
    "Al",
    "Si",
    "P",
    "S",
    "Cl",
    "Ar",
    "K",
    "Ca",
    "Sc",
    "Ti",
    "V",
    "Cr",
    "Mn",
    "Fe",
    "Co",
    "Ni",
    "Cu",
    "Zn",
    "Ga",
    "Ge",
    "As",
    "Se",
    "Br",
    "Kr",
    "Rb",
    "Sr",
    "Y",
    "Zr",
    "Nb",
    "Mo",
    "Tc",
    "Ru",
    "Rh",
    "Pd",
    "Ag",
    "Cd",
    "In",
    "Sn",
    "Sb",
    "Te",
    "I",
    "Xe",
    "Cs",
    "Ba",
    "La",
    "Ce",
    "Pr",
    "Nd",
    "Pm",
    "Sm",
    "Eu",
    "Gd",
    "Tb",
    "Dy",
    "Ho",
    "Er",
    "Tm",
    "Yb",
    "Lu",
    "Hf",
    "Ta",
    "W",
    "Re",
    "Os",
    "Ir",
    "Pt",
    "Au",
    "Hg",
    "Tl",
    "Pb",
    "Bi",
    "Po",
    "At",
    "Rn",
    "Fr",
    "Ra",
    "Ac",
    "Th",
    "Pa",
    "U",
    "Np",
    "Pu",
    "Am",
    "Cm",
    "Bk",
    "Cf",
    "Es",
    "Fm",
    "Md",
    "No",
    "Lr",
    0,
  };

}

void DLV::atom_pref_data::get_colour(colour_data &c) const
{
  c = colour;
}

DLV::real_g DLV::atom_pref_data::get_radius() const
{
  std::map<int_g, real_g>::const_iterator x = radii.find(default_charge);
  return x->second;
}

DLV::real_g DLV::atom_pref_data::get_radius(const int_g charge) const
{
  std::map<int_g, real_g>::const_iterator x = radii.find(charge);
  if (x == radii.end())
    x = radii.find(default_charge);
  return x->second;
}

DLV::atomic_prefs::atomic_prefs()
{
  data = new atom_pref_data[max_atomic_number + 1];
  int_g i = 0;
  for (i = 0; atomic_symbols[i] != 0; i++)
    data[i].set_symbol(i, atomic_symbols[i]);
  for (; i <= max_atomic_number; i++)
    data[i].set_symbol(i, "?");
}

DLV::atomic_prefs::~atomic_prefs()
{
  delete [] data;
}

DLV::atomic_prefs *DLV::atomic_prefs::load(std::ifstream &input)
{
  atomic_prefs *obj = atomic_data;
  if (atomic_data == 0)
    obj = new atomic_prefs;
#ifdef DLV_USES_SERIALIZE
  // Todo - change this to new value?
  boost::archive::text_iarchive ar(input);
  for (int i = 0; i <= 110; i++)
    ar & obj->data[i];
#endif // DLV_USES_SERIALIZE
  return obj;
}

void DLV::atomic_prefs::save(std::ofstream &output)
{
#ifdef DLV_USES_SERIALIZE
  boost::archive::text_oarchive ar(output);
  for (int i = 0; i <= max_atomic_number; i++)
    ar & data[i];
#endif // DLV_USES_SERIALIZE
}

DLVreturn_type DLV::atomic_prefs::read(const char filename[], char message[],
				       const int mlen)
{
  FILE *ifile = fopen(filename, "r");
  if (ifile == 0) {
    snprintf(message, mlen,
	     "Unable to open atom preferences file %s", filename);
    return DLV_ERROR;
  } else {
    float r, g, b, radius;
    int_g atn = 0, charge;
    char buff[256], name[32], label[32];
    DLVreturn_type value = DLV_OK;
    while (!feof(ifile)) {
      fgets(buff, 256, ifile);
      sscanf(buff, "%s", name);
      if (strcmp(name, "Atom") == 0) {
	sscanf(buff, "%s %d %s %d %f %f %f", name, &atn, label, &charge,
	       &r, &g, &b);
	if (r < 0.0)
	  r = 0.0;
	else if (r > 1.0)
	  r = 1.0;
	if (g < 0.0)
	  g = 0.0;
	else if (g > 1.0)
	  g = 1.0;
	if (b < 0.0)
	  b = 0.0;
	else if (b > 1.0)
	  b = 1.0;
	colour_data c(r, g, b);
	if (atn > -1 and atn <= max_atomic_number) {
	  data[atn].set_symbol(atn, label);
	  data[atn].set_data(charge, c);
	}
      } else if (strcmp(name, "Radius") == 0) {
	sscanf(buff, "%s %d %f", label, &charge, &radius);
	if (atn > -1 and atn <= max_atomic_number)
	  data[atn].add_radius(charge, radius);
      } else if (name[0] != '\0') {
	if (name[0] != '#') {
	  snprintf(message, mlen,
		   "Ignoring unrecognised atom preferences keyword %s", name);
	  value = DLV_WARNING;
	}
      }
    }
    fclose(ifile);
    return value;
  }
}

void DLV::atomic_prefs::find_colour(colour_data &c,
				    const int atomic_number) const
{
  data[atomic_number].get_colour(c);
}

int DLV::atomic_prefs::find_charge(const int atomic_number) const
{
  return data[atomic_number].get_charge();
}

DLV::real_g DLV::atomic_prefs::find_radius(const int atomic_number) const
{
  return data[atomic_number].get_radius();
}

DLV::real_g DLV::atomic_prefs::find_radius(const int atomic_number,
					   const int charge) const
{
  return data[atomic_number].get_radius(charge);
}

int DLV::atomic_prefs::lookup_symbol(const char symbol[])
{
  for (int i = 0; i <= max_atomic_number; i++) {
    if (atomic_data->data[i].get_symbol() == symbol)
      return i;
  }
  return 0;
}

DLV::string DLV::atomic_prefs::get_symbol(const int atomic_number)
{
  string symbol;
  if (atomic_number >= 0 and atomic_number <= max_atomic_number)
    symbol = atomic_data->data[atomic_number].get_symbol();
  else
    symbol = atomic_symbols[0];
  return symbol;
}

#ifndef OVERRIDE_ATOM_PREFS

void DLV::atomic_prefs::set_data(const int, const char[])
{
}

#endif // OVERRIDE_ATOM_PREFS
