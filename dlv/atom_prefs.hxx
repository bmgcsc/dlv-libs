
#ifndef DLV_ATOM_PREFS
#define DLV_ATOM_PREFS

namespace DLV {

  const int_g ghost_index = 200;
  const int_g point_charge_index = 300;
  const int_g max_atomic_number = DLV::point_charge_index;

  class atomic_prefs {
  public:
    atomic_prefs();
    ~atomic_prefs();
    DLVreturn_type read(const char filename[], char message[],
			const int_g mlen);
    static atomic_prefs *load(std::ifstream &input);
    void save(std::ofstream &output);
    void find_colour(class colour_data &c, const int_g atomic_number) const;
    int_g find_charge(const int_g atomic_number) const;
    real_g find_radius(const int_g atomic_number) const;
    real_g find_radius(const int_g atomic_number, const int_g charge) const;
    static int_g lookup_symbol(const char symbol[]);
    static string get_symbol(const int_g atomic_number);
    void set_data(const int_g index, const char sym[]);

  private:
    class atom_pref_data *data;
  };

  extern atomic_prefs *atomic_data;

}

#endif // DLV_ATOM_PREFS
