
#ifndef DLV_ATOM_PREF_IMPL
#define DLV_ATOM_PREF_IMPL

#include <map>

#ifdef DLV_USES_SERIALIZE
  #include <boost/serialization/map.hpp>
#endif // DLV_USES_SERIALIZE

namespace DLV {

  class atom_pref_data {
  public:
    atom_pref_data();
    void set_symbol(const int_g atn, const char *sym);
    void set_data(const int_g dcharge, const colour_data &c);
    void add_radius(const int_g charge, const real_g radius);
    void get_colour(colour_data &c) const;
    int_g get_charge() const;
    string get_symbol() const;
    real_g get_radius() const;
    real_g get_radius(const int_g charge) const;

  private:
    int_g atomic_number;
    string symbol;
    int_g default_charge;
    colour_data colour;
    std::map<int_g, real_g> radii;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & atomic_number;
      ar & symbol;
      ar & default_charge;
      ar & colour;
      ar & radii;
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline DLV::atom_pref_data::atom_pref_data() : atomic_number(-1)
{
}

inline DLV::int_g DLV::atom_pref_data::get_charge() const
{
  return default_charge;
}

inline DLV::string DLV::atom_pref_data::get_symbol() const
{
  return symbol;
}

inline void DLV::atom_pref_data::set_symbol(const int_g atn, const char *sym)
{
  atomic_number = atn;
  symbol = sym;
}

inline void DLV::atom_pref_data::set_data(const int_g dcharge,
					  const colour_data &c)
{
  default_charge = dcharge;
  colour = c;
}

inline void DLV::atom_pref_data::add_radius(const int_g charge,
					    const real_g radius)
{
  radii[charge] = radius;
}

#endif // DLV_ATOM_PREF_IMPL
