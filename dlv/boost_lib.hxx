
#ifndef DLV_BOOST_WRAPPER
#define DLV_BOOST_WRAPPER

#ifdef ENABLE_DLV_JOB_THREADS
#  include <boost/thread/thread.hpp>
#  include <boost/thread/mutex.hpp>
#  include <boost/thread/xtime.hpp>
//#else
//#  define BOOST_DISABLE_THREADS
#endif // ENABLE_DLV_JOB_THREADS

#include <boost/version.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/exception.hpp>

#ifdef DLV_USES_SERIALIZE
// Serialization objects for saving/loading projects
#  include <boost/archive/text_oarchive.hpp>
#  include <boost/archive/text_iarchive.hpp>

#  include <boost/serialization/serialization.hpp>
#  include <boost/serialization/base_object.hpp>
#  include <boost/serialization/split_member.hpp>
#  include <boost/serialization/assume_abstract.hpp>
#  include <boost/serialization/string.hpp>
#  include <boost/serialization/list.hpp>
#  include <boost/serialization/map.hpp>
#  include <boost/serialization/vector.hpp>
#  include <boost/serialization/shared_ptr.hpp>
#  include <boost/serialization/export.hpp>

#define DLV_SUPPRESS_TEMPLATES(type) \
  extern template class boost::archive::detail::oserializer<boost::archive::text_oarchive, type>; \
  extern template class boost::archive::detail::iserializer<boost::archive::text_iarchive, type>; \
  extern template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, type>; \
  extern template class boost::archive::detail::pointer_oserializer<boost::archive::text_iarchive, type>; \
  extern template boost::archive::text_iarchive& boost::archive::detail::interface_iarchive<boost::archive::text_iarchive>::operator&<type>(type&); \
  extern template boost::archive::text_oarchive& boost::archive::detail::interface_oarchive<boost::archive::text_oarchive>::operator&<type>(type const&); \
  extern template boost::serialization::extended_type_info_typeid<type> &boost::serialization::singleton<boost::serialization::extended_type_info_typeid<type> >::get_instance(); \
  extern template const boost::serialization::extended_type_info_typeid<type> &boost::serialization::singleton<boost::serialization::extended_type_info_typeid<type> >::get_const_instance(); \
  extern template boost::archive::detail::iserializer<boost::archive::text_iarchive, type> &boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, type> >::get_instance(); \
  extern template boost::archive::detail::oserializer<boost::archive::text_oarchive, type> &boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, type> >::get_instance();

// probably not needed
//extern template class boost::serialization::extended_type_info_typeid<type>;
//extern template void boost::archive::load<boost::archive::text_iarchive, type>(boost::archive::text_iarchive&, type&);
//extern template void boost::archive::save<boost::archive::text_oarchive, type>(boost::archive::text_oarchive&, type&);
//extern template void boost::archive::detail::save_non_pointer_type<boost::archive::text_oarchive>::invoke<type>(boost::archive::text_oarchive&, type const&);

//extern template void boost::archive::text_iarchive_impl<boost::archive::text_iarchive>::load_override<type>(type&);
//extern template void boost::archive::basic_text_oarchive<boost::archive::text_oarchive>::save_override<type>(type&);
//extern template void boost::archive::basic_text_oarchive<boost::archive::text_oarchive>::save_override<type const>(type const&);
//extern template const boost::serialization::extended_type_info_typeid<type> &boost::serialization::singleton<boost::serialization::extended_type_info_typeid<type> >::get_const_instance();

#define DLV_NORMAL_EXPLICIT(type) \
  template void type::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version); \
  template void type::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version);

#define DLV_SPLIT_EXPLICIT(type) \
  template void type::save<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version) const; \
  template void type::load<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version);

// extra needed for base class in separate file..., inverse of SUPPRESS
#define DLV_EXPORT_EXPLICIT(type) \
  template class boost::archive::detail::oserializer<boost::archive::text_oarchive, type>; \
  template class boost::archive::detail::iserializer<boost::archive::text_iarchive, type>; \
  template boost::archive::text_iarchive& boost::archive::detail::interface_iarchive<boost::archive::text_iarchive>::operator&<type>(type&); \
  template boost::archive::text_oarchive& boost::archive::detail::interface_oarchive<boost::archive::text_oarchive>::operator&<type>(type const&); \
  template boost::serialization::extended_type_info_typeid<type> &boost::serialization::singleton<boost::serialization::extended_type_info_typeid<type> >::get_instance(); \
  template const boost::serialization::extended_type_info_typeid<type> &boost::serialization::singleton<boost::serialization::extended_type_info_typeid<type> >::get_const_instance(); \
  template boost::archive::detail::iserializer<boost::archive::text_iarchive, type> &boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, type> >::get_instance(); \
  template boost::archive::detail::oserializer<boost::archive::text_oarchive, type> &boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, type> >::get_instance();

#endif // DLV_USES_SERIALIZE

#endif // DLV_BOOST_WRAPPER
