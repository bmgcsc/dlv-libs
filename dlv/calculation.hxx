
#ifndef DLV_OP_CALCULATIONS
#define DLV_OP_CALCULATIONS

#include <map>

namespace DLV {

  class calculate_data : public operation {
  public:
    bool is_calc() const;

  protected:
    calculate_data();
    calculate_data(model *m);
    void add_standard_data_objects();
    bool job_failed() const;
    void set_job_failed();

    string make_dir_name() const;
    string make_name(const string tag, const string suffix) const;
    bool make_directory(char message[], const int_g mlen) const;

    void add_data_file(const int_g index, const string filename,
		       const bool is_local, const bool link = false);
    void add_data_file(const int_g index, const string filename,
		       const string name, const bool is_local,
		       const bool link = false);
    void add_input_file(const int_g index, const string tag,
			const string suffix, const bool is_local,
			const bool link = false);
    void add_input_file(const int_g index, const string tag,
			const string suffix, const string name,
			const bool is_local, const bool link = false);
    void add_job_error_file(const string tag, const string suffix,
			    const string name, const bool is_local,
			    const bool link = false);
    void add_output_file(const int_g index, const string tag,
			 const string suffix, const bool is_local,
			 const bool link = false);
    void add_output_file(const int_g index, const string tag,
			 const string suffix, const string name,
			 const bool is_local, const bool link = false);
    void attach_input(const int_g index, file_obj *file);
    string get_infile_name(const int_g index) const;
    string get_outfile_name(const int_g index) const;
    file_obj *get_error_file() const;
    string get_job_error_file() const;

    std::map<int, file_obj *> &get_input_files();
    std::map<int, file_obj *> &get_output_files();

  private:
    bool job_had_errors;
    file_obj *errors;
    std::map<int, file_obj *> inp_files;
    std::map<int, file_obj *> out_files;
    
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class batch_calc : public calculate_data {
  protected:
    batch_calc();

    void add_sys_error_file(const string tag, const string suffix,
			    const bool is_local, const bool link = false);
    void add_command_file(const int_g index, const string tag,
			  const string suffix, const bool is_local,
			  const bool link = false);
    void add_log_file(const string tag, const string suffix,
		      const bool is_local, const bool link = false);
    void add_log_file(const string tag, const string suffix,
		      const string name, const bool is_local,
		      const bool link = false);
    string get_log_filename() const;
    string get_sys_error_file() const;

    DLVreturn_type recover_job(char message[], const int_g mlen);
    DLVreturn_type remove_job(char message[], const int_g mlen);
    DLVreturn_type kill_job(char message[], const int_g mlen);

    void add_job(job *j, const string hostname, const bool external_job);
    int_g get_job_number() const;
    string get_job_id() const;
    job *get_job() const;

    virtual bool create_files(const bool is_parallel, const bool is_local,
			      char message[], const int_g mlen) = 0;
    bool execute(const string local_path, const string serial_exec,
		 const string parallel_exec, const string args,
		 const class job_setup_data &job_data,
		 const bool allow_parallel_job, const bool external_job,
		 const string extern_dir, char message[], const int_g mlen,
		 const bool foreground = false);
    virtual bool recover(const bool no_err, const bool log_ok,
			 char message[], const int_g len) = 0;
    virtual bool no_errors(char message[], const int_g len) = 0;
    virtual void fixup_job();
    void set_failure();

  private:
    file_obj *stdoutput;
    file_obj *stderror;
    bool uses_stdin;
    int_g stdin_index;
    job *calc;
    int_g job_index;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class socket_calc : public calculate_data {
  protected:
    socket_calc();
    socket_calc(model *m);

    bool execute(const string binary, class text_buffer *data, char message[],
		 const int_g mlen);
    bool inherit_connections();
    DLVreturn_type write_connection(const char command[]);
    void read_connection(operation *op, const bool complete = true);
    void close_connection();
    void add_connection(class job *task);
    bool has_connection() const;
    job *get_job() const;

  private:
    class job *connection;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::batch_calc)
BOOST_CLASS_EXPORT_KEY(DLV::calculate_data)
BOOST_CLASS_EXPORT_KEY(DLV::batch_calc)
BOOST_CLASS_EXPORT_KEY(DLV::socket_calc)
#endif // DLV_USES_SERIALIZE

inline DLV::calculate_data::calculate_data()
  : operation(), job_had_errors(false), errors(0)
{
}

inline DLV::calculate_data::calculate_data(model *m)
  : operation(m), job_had_errors(false), errors(0)
{
}

inline DLV::batch_calc::batch_calc()
  : stdoutput(0), stderror(0), uses_stdin(false), stdin_index(-1),
    calc(0), job_index(-1)
{
}

inline DLV::socket_calc::socket_calc() : connection(0)
{
}

inline DLV::socket_calc::socket_calc(model *m)
  : calculate_data(m), connection(0)
{
}

inline void DLV::calculate_data::set_job_failed()
{
  job_had_errors = true;
}

inline DLV::file_obj *DLV::calculate_data::get_error_file() const
{
  return errors;
}

inline std::map<int, DLV::file_obj *> &
DLV::calculate_data::get_input_files()
{
  return inp_files;
}

inline std::map<int, DLV::file_obj *> &
DLV::calculate_data::get_output_files()
{
  return out_files;
}

inline void DLV::socket_calc::add_connection(job *task)
{
  connection = task;
}

inline bool DLV::socket_calc::has_connection() const
{
  return (connection != 0);
}

#endif // DLV_OP_CALCULATIONS
