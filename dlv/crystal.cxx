
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "data_objs.hxx"
//#include "data_vol.hxx" // wulff_plot
#include "crystal.hxx"

#ifdef ENABLE_DLV_GRAPHICS

// Wulff code call
extern void output_OOGL(const int nnormals, const double normals[][3],
			const double surface_energy[], const int colours[],
			double (* &verts)[3], int &nverts,
			int ** &face_vertices, int * &nface_vertices,
			int &num_faces);


#endif // ENABLE_DLV_GRAPHICS

DLV::model *DLV::create_crystal(const string model_name)
{
  return new crystal(model_name);
}

DLV::model_base *DLV::crystal::duplicate(const string label) const
{
  crystal *m = new crystal(label);
  m->copy_atom_groups(this);
  return m;
}

bool DLV::crystal::is_crystal() const
{
  return true;
}

void DLV::crystal::use_which_lattice_parameters(bool usage[6]) const
{
  usage[0] = true;
  usage[1] = false;
  usage[2] = false;
  usage[3] = false;
  usage[4] = false;
  usage[5] = false;
  switch (get_lattice_type()) {
  case 0:
    usage[3] = true;
    usage[5] = true;
  case 1:
    usage[4] = true;
  case 2:
    usage[1] = true;
  case 3:
  case 4:
  case 5:
    usage[2] = true;
    break;
  default:
    break;
  }
}

bool DLV::crystal::set_primitive_lattice(const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3])
{
  return set_lattice(a, b, c);
}

bool DLV::crystal::set_lattice_parameters(const coord_type a,
					  const coord_type b,
					  const coord_type c,
					  const coord_type alpha,
					  const coord_type beta,
					  const coord_type gamma)
{
  return set_parameters(a, b, c, alpha, beta, gamma);
}

DLV::int_g DLV::crystal::get_number_of_periodic_dims() const
{
  return 3;
}

DLV::int_g DLV::crystal::get_lattice_type() const
{
  return lattice_type();
}

DLV::int_g DLV::crystal::get_lattice_centring() const
{
  return get_centre_type();
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::crystal::get_model_type() const
{
  return 3;
}

void DLV::crystal::get_frac_rotation_operators(real_l r[][3][3],
					       const int_g nops) const
{
  get_cart_rotation_operators(r, nops);
  coord_type va[3];
  coord_type vb[3];
  coord_type vc[3];
  get_primitive_lattice(va, vb, vc);
  real_l cell[3][3];
  cell[0][0] = va[0];
  cell[1][0] = va[1];
  cell[2][0] = va[2];
  cell[0][1] = vb[0];
  cell[1][1] = vb[1];
  cell[2][1] = vb[2];
  cell[0][2] = vc[0];
  cell[1][2] = vc[1];
  cell[2][2] = vc[2];
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  real_l temp[3][3];
  // transform is Rf = P^-1 Rc P, where P transforms from frac to cart
  for (int_g i = 0; i < nops; i++) {
    mat_mult(r[i], cell, temp);
    mat_mult(inverse, temp, r[i]);
  }
}

void DLV::crystal::set_crystal03_lattice_type(const int_g lattice,
					      const int_g centre)
{
  set_crystal03_lattice(lattice, centre);
}

void DLV::crystal::get_primitive_lattice(coord_type a[3], coord_type b[3],
					 coord_type c[3]) const
{
  coord_type l[3][3];
  get_primitive_vectors(l);
  a[0] = l[0][0];
  a[1] = l[0][1];
  a[2] = l[0][2];
  b[0] = l[1][0];
  b[1] = l[1][1];
  b[2] = l[1][2];
  c[0] = l[2][0];
  c[1] = l[2][1];
  c[2] = l[2][2];
}

void DLV::crystal::get_conventional_lattice(coord_type a[3], coord_type b[3],
					    coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
  coord_type t[3][3];
  get_transform_to_conv_cell(t);
  coord_type conv[3][3];
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      conv[i][j] = t[i][0] * a[j] + t[i][1] * b[j] + t[i][2] * c[j];
  for (int_g i = 0; i < 3; i++) {
    a[i] = conv[0][i];
    b[i] = conv[1][i];
    c[i] = conv[2][i];
  }
}

void DLV::crystal::get_reciprocal_lattice(coord_type a[3], coord_type b[3],
					  coord_type c[3]) const
{
  coord_type pa[3];
  coord_type pb[3];
  coord_type pc[3];
  get_primitive_lattice(pa, pb, pc);
  // Todo - old version had a rotate option!?
  coord_type cx = pb[1] * pc[2] - pb[2] * pc[1];
  coord_type cy = pb[2] * pc[0] - pb[0] * pc[2];
  coord_type cz = pb[0] * pc[1] - pb[1] * pc[0];
  coord_type vol = pa[0] * cx + pa[1] * cy + pa[2] * cz;
  a[0] = real_l(2.0 * pi) * cx / vol;
  a[1] = real_l(2.0 * pi) * cy / vol;
  a[2] = real_l(2.0 * pi) * cz / vol;
  b[0] = real_l(2.0 * pi) * (pc[1] * pa[2] - pc[2] * pa[1]) / vol;
  b[1] = real_l(2.0 * pi) * (pc[2] * pa[0] - pc[0] * pa[2]) / vol;
  b[2] = real_l(2.0 * pi) * (pc[0] * pa[1] - pc[1] * pa[0]) / vol;
  c[0] = real_l(2.0 * pi) * (pa[1] * pb[2] - pa[2] * pb[1]) / vol;
  c[1] = real_l(2.0 * pi) * (pa[2] * pb[0] - pa[0] * pb[2]) / vol;
  c[2] = real_l(2.0 * pi) * (pa[0] * pb[1] - pa[1] * pb[0]) / vol;
}

void DLV::crystal::generate_real_space_lattice(real_g (* &ipoints)[3],
					       real_g (* &fpoints)[3],
					       int_g &nlines, const int_g na,
					       const int_g nb, const int_g nc,
					       const bool centre_cell,
					       const bool conventional,
					       char [], const int_g) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0] + real_l(nb) * b[0]
			  + real_l(nc) * c[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1] + real_l(nb) * b[1]
			  + real_l(nc) * c[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2] + real_l(nb) * b[2]
			  + real_l(nc) * c[2]) / 2.0);
  }
  int_g size = (na + 1) * (nb + 1) + (na + 1) * (nc + 1) + (nb + 1) * (nc + 1);
  ipoints = new real_g[size][3];
  fpoints = new real_g[size][3];
  // Generate (nb + 1) * (nc + 1) lines in 'a' direction.
  for (int_g i = 0; i <= nb; i++) {
    real_g x[3];
    x[0] = origin[0] + real_g(b[0]) * real_g(i);
    x[1] = origin[1] + real_g(b[1]) * real_g(i);
    x[2] = origin[2] + real_g(b[2]) * real_g(i);
    for (int_g j = 0; j <= nc; j++) {
      ipoints[i * (nc + 1) + j][0] = x[0] + real_g(c[0]) * real_g(j);
      ipoints[i * (nc + 1) + j][1] = x[1] + real_g(c[1]) * real_g(j);
      ipoints[i * (nc + 1) + j][2] = x[2] + real_g(c[2]) * real_g(j);
      fpoints[i * (nc + 1) + j][0] = ipoints[i * (nc + 1) + j][0]
	+ real_g(a[0]) * real_g(na);
      fpoints[i * (nc + 1) + j][1] = ipoints[i * (nc + 1) + j][1]
	+ real_g(a[1]) * real_g(na);
      fpoints[i * (nc + 1) + j][2] = ipoints[i * (nc + 1) + j][2]
	+ real_g(a[2]) * real_g(na);
    }
  }
  // Generate (na + 1) * (nc + 1) lines in 'b' direction.
  int_g k = (nb + 1) * (nc + 1);
  for (int_g i = 0; i <= nc; i++) {
    real_g x[3];
    x[0] = origin[0] + real_g(c[0]) * real_g(i);
    x[1] = origin[1] + real_g(c[1]) * real_g(i);
    x[2] = origin[2] + real_g(c[2]) * real_g(i);
    for (int_g j = 0; j <= na; j++) {
      ipoints[k + i * (na + 1) + j][0] = x[0] + real_g(a[0]) * real_g(j);
      ipoints[k + i * (na + 1) + j][1] = x[1] + real_g(a[1]) * real_g(j);
      ipoints[k + i * (na + 1) + j][2] = x[2] + real_g(a[2]) * real_g(j);
      fpoints[k + i * (na + 1) + j][0] =
	ipoints[k + i * (na + 1) + j][0] + real_g(b[0]) * real_g(nb);
      fpoints[k + i * (na + 1) + j][1] =
	ipoints[k + i * (na + 1) + j][1] + real_g(b[1]) * real_g(nb);
      fpoints[k + i * (na + 1) + j][2] =
	ipoints[k + i * (na + 1) + j][2] + real_g(b[2]) * real_g(nb);
    }
  }
  // Generate (na + 1) * (nb + 1) lines in 'c' direction.
  k += (na + 1) * (nc + 1);
  for (int_g i = 0; i <= na; i++) {
    real_g x[3];
    x[0] = origin[0] + real_g(a[0]) * real_g(i);
    x[1] = origin[1] + real_g(a[1]) * real_g(i);
    x[2] = origin[2] + real_g(a[2]) * real_g(i);
    for (int_g j = 0; j <= nb; j++) {
      ipoints[k + i * (nb + 1) + j][0] = x[0] + real_g(b[0]) * real_g(j);
      ipoints[k + i * (nb + 1) + j][1] = x[1] + real_g(b[1]) * real_g(j);
      ipoints[k + i * (nb + 1) + j][2] = x[2] + real_g(b[2]) * real_g(j);
      fpoints[k + i * (nb + 1) + j][0] =
	ipoints[k + i * (nb + 1) + j][0] + real_g(c[0]) * real_g(nc);
      fpoints[k + i * (nb + 1) + j][1] =
	ipoints[k + i * (nb + 1) + j][1] + real_g(c[1]) * real_g(nc);
      fpoints[k + i * (nb + 1) + j][2] =
	ipoints[k + i * (nb + 1) + j][2] + real_g(c[2]) * real_g(nc);
    }
  }
  nlines = size;
}

DLV::int_g DLV::crystal::get_real_space_lattice_labels(real_g points[][3],
						const int_g na, const int_g nb,
						const int_g nc,
						const bool centre_cell,
						const bool conventional) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0] + real_l(nb) * b[0]
			  + real_l(nc) * c[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1] + real_l(nb) * b[1]
			  + real_l(nc) * c[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2] + real_l(nb) * b[2]
			  + real_l(nc) * c[2]) / 2.0);
  }
  points[0][0] = origin[0];
  points[0][1] = origin[1];
  points[0][2] = origin[2];
  points[1][0] = origin[0] + real_g(na) * real_g(a[0]);
  points[1][1] = origin[1] + real_g(na) * real_g(a[1]);
  points[1][2] = origin[2] + real_g(na) * real_g(a[2]);
  points[2][0] = origin[0] + real_g(nb) * real_g(b[0]);
  points[2][1] = origin[1] + real_g(nb) * real_g(b[1]);
  points[2][2] = origin[2] + real_g(nb) * real_g(b[2]);
  points[3][0] = origin[0] + real_g(nc) * real_g(c[0]);
  points[3][1] = origin[1] + real_g(nc) * real_g(c[1]);
  points[3][2] = origin[2] + real_g(nc) * real_g(c[2]);
  return 4;
}

void DLV::crystal::generate_k_space_vertices(real_g (* &vertices)[3],
					     int_g &nlines, int_l * &connects,
					     int_l &nconnects, char [],
					     const int_g) const
{
#ifdef ENABLE_DLV_GRAPHICS
  // Brillouin zone drawing, seems ok for tetragonal, tig/hex, and cubic
  // Untested for Triclinic/Monoclinic.
  // Orthorhombic prim and base seem ok, but not sure about body or face
  // as there are so many complicated shapes involved.
  static real_g hkl[][3] = {
    {  1.0,  0.0,  0.0 },
    { -1.0,  0.0,  0.0 },
    {  0.0,  1.0,  0.0 },
    {  0.0, -1.0,  0.0 },
    {  0.0,  0.0,  1.0 },
    {  0.0,  0.0, -1.0 },
    {  1.0,  1.0,  1.0 },
    { -1.0, -1.0, -1.0 },
    { -1.0,  1.0,  1.0 },
    {  1.0, -1.0,  1.0 },
    {  1.0,  1.0, -1.0 },
    {  1.0, -1.0, -1.0 },
    { -1.0,  1.0, -1.0 },
    { -1.0, -1.0,  1.0 },
    {  1.0,  1.0,  0.0 },
    { -1.0, -1.0,  0.0 },
    { -1.0,  1.0,  0.0 },
    {  1.0, -1.0,  0.0 },
    {  1.0,  0.0,  1.0 },
    { -1.0,  0.0,  1.0 },
    {  1.0,  0.0, -1.0 },
    { -1.0,  0.0, -1.0 },
    {  0.0,  1.0,  1.0 },
    {  0.0, -1.0,  1.0 },
    {  0.0,  1.0, -1.0 },
    {  0.0, -1.0, -1.0 }
  };
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_reciprocal_lattice(a, b, c);
  int_g ntemp = 26;
  real_l temp_norms[26][3];
  real_l temp_e[26];
  int_g colours[26];
  for (int_g i = 0; i < ntemp; i++)
    colours[i] = 1;
  real_l min_e = 10000.0;
  for (int_g i = 0; i < ntemp; i++) {
    real_l sum = 0.0;
    for (int_g j = 0; j < 3; j++) {
      temp_norms[i][j] = (a[j] * hkl[i][0]
			  + b[j] * hkl[i][1]
			  + c[j] * hkl[i][2]);
      sum += temp_norms[i][j] * temp_norms[i][j];
    }
    sum = sqrt(sum);
    temp_e[i] = 0.5 * sum;
    if (temp_e[i] < min_e)
      min_e = temp_e[i];
    for (int_g j = 0; j < 3; j++)
      temp_norms[i][j] /= sum;
  }
  // added Sep 07 BGS, orthorhombic 10, 8, 6 seems to suggest we must
  // always include (001) vector set
  int_g nnormals = 0;
  real_l normals[26][3];
  real_l energies[26];
  real_l max_e = 0.0;
  for (int_g i = 0; i < 6; i++) {
    normals[nnormals][0] = temp_norms[i][0];
    normals[nnormals][1] = temp_norms[i][1];
    normals[nnormals][2] = temp_norms[i][2];
    energies[nnormals] = temp_e[i];
    if (temp_e[i] > max_e)
      max_e = temp_e[i];
    nnormals++;
  }
  // Sort normals based on length
  const real_l tol = 0.01;
  real_l min2 = 10000.0;
  // Find shortest lattice vectors
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min_e) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    } else if (temp_e[i] < min2)
      min2 = temp_e[i];
  }
  real_l min3 = 10000.0;
  // Include 2nd shortest lengths
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min2) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    } else if ((temp_e[i] < min3) && (temp_e[i] > min2))
      min3 = temp_e[i];
  }
  // fcc, bcc, sc, hex + P trig, R trig a > c seem ok with
  // first 2 lengths. Try 3rd shortest for R trig c > a.
  // Doesn't seem to cause problems! but still not sure whether
  // sqrt(2) c > a or < for Trig R cross over where we expect.
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min3) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    }
  }
  if (abs(max_e - min3) > tol) {
    // the base 001 set included a longer distance, make sure we include
    // any others of the same length
    for (int_g i = 0; i < ntemp; i++) {
      if (temp_e[i] - max_e < tol and temp_e[i] > min3 + tol) {
	if (i >= 6) {
	  normals[nnormals][0] = temp_norms[i][0];
	  normals[nnormals][1] = temp_norms[i][1];
	  normals[nnormals][2] = temp_norms[i][2];
	  energies[nnormals] = temp_e[i];
	  nnormals++;
	}
      }
    }
  }
  // Call wulffman code to generate object
  int_g nverts = 0, nfaces = 0;
  int_g **face_vertices, *nface_vertices = 0;
  real_l (*myverts)[3];
  output_OOGL(nnormals, normals, energies, colours, myverts, nverts,
	      face_vertices, nface_vertices, nfaces);
  nlines = nverts;
  if (nverts > 0) {
    vertices = new real_g[nverts][3];
    for (int_g i = 0; i < nverts; i++) {
      vertices[i][0] = (real_g) myverts[i][0];
      vertices[i][1] = (real_g) myverts[i][1];
      vertices[i][2] = (real_g) myverts[i][2];
    }
    delete [] myverts;
    // Number of lines
    int_g count = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	count += nface_vertices[i];
      }
    }
    nconnects = 2 * count;
    connects = new int_l[2 * count];
    count = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	for (int_g j = 0; j < nface_vertices[i]; j++) {
	  connects[count] = face_vertices[i][j];
	  connects[count + 1] = face_vertices[i][(j + 1) % nface_vertices[i]];
	  count += 2;
	}
      }
    }
    for (int_g i = 0; i < nfaces; i++)
      if (face_vertices[i] != 0)
	delete [] face_vertices[i];
    delete [] nface_vertices;
  }
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::crystal::generate_transforms(const int_g na, const int_g nb,
				       const int_g nc, const int_g sa,
				       const int_g sb, const int_g sc,
				       const bool centre_cell,
				       const bool conventional,
				       int_g &ntransforms,
				       real_g (* &transforms)[3]) const
{
  int_g size = na * nb * nc;
  ntransforms = size;
  transforms = new real_g[size][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  int_g count = 0;
  int_g min_a = 0;
  int_g max_a = na;
  int_g min_b = 0;
  int_g max_b = nb;
  int_g min_c = 0;
  int_g max_c = nc;
  if (centre_cell) {
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
    max_b = nb / 2;
    min_b = - max_b;
    if (max_b * 2 != nb)
      max_b++;
    max_c = nc / 2;
    min_c = - max_c;
    if (max_c * 2 != nc)
      max_c++;
  }
  for (int_g i = min_a; i < max_a; i++) {
    real_g x[3];
    x[0] = (real_g)(i * sa) * (real_g)a[0];
    x[1] = (real_g)(i * sa) * (real_g)a[1];
    x[2] = (real_g)(i * sa) * (real_g)a[2];
    for (int_g j = min_b; j < max_b; j++) {
      real_g y[3];
      y[0] = x[0] + (real_g)(j * sb) * (real_g)b[0];
      y[1] = x[1] + (real_g)(j * sb) * (real_g)b[1];
      y[2] = x[2] + (real_g)(j * sb) * (real_g)b[2];
      for (int_g k = min_c; k < max_c; k++) {
	transforms[count][0] = y[0] + (real_g)(k * sc) * (real_g)c[0];
	transforms[count][1] = y[1] + (real_g)(k * sc) * (real_g)c[1];
	transforms[count][2] = y[2] + (real_g)(k * sc) * (real_g)c[2];
	count++;
      }
    }
  }
}

bool DLV::crystal::set_fractional_sym_ops(const real_l rotations[][3][3],
					  const real_l translations[][3],
					  const int_g nops)
{
  // Todo - do we have to deal with operators defined on primitive cell?
  coord_type ca[3];
  coord_type cb[3];
  coord_type cc[3];
  if (has_valid_parameters())
    complete_lattice(ca, cb, cc);
  else if (has_valid_vectors())
    get_primitive_lattice(ca, cb, cc);
  else
    return false;
  //coord_type ca[3];
  //coord_type cb[3];
  //coord_type cc[3];
  get_conventional_lattice(ca, cb, cc);
  // from load_model.cxx gen_vectors().
  real_l uxyz[3][3];
  // transposed
  uxyz[0][0] = ca[0];
  uxyz[1][0] = ca[1];
  uxyz[2][0] = ca[2];
  uxyz[0][1] = cb[0];
  uxyz[1][1] = cb[1];
  uxyz[2][1] = cb[2];
  uxyz[0][2] = cc[0];
  uxyz[1][2] = cc[1];
  uxyz[2][2] = cc[2];
  real_l uxyzi[3][3];
  matrix_invert(uxyz, uxyzi, 3);
  real_l (*r)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*t)[3] = new_local_array2(real_l, nops, 3);
  real_l zr[3][3];
  for (int_g i = 0; i < nops; i++) {
    mat_mult(rotations[i], uxyzi, zr);
    mat_mult(uxyz, zr, r[i]);
    for (int_g j = 0; j < 3; j++)
      t[i][j] = ca[j] * translations[i][0]
	+ cb[j] * translations[i][1]
	+ cc[j] * translations[i][2];
  }
  bool ok = set_cartesian_sym_ops(r, t, nops);
  delete_local_array(t);
  delete_local_array(r);
  return ok;
}

void DLV::crystal::generate_primitive_atoms(const bool tidy)
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  generate_primitive_copies(a, b, c, rotations, translations, nops, tidy);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::crystal::find_asym_pos(const int_g asym_idx, const coord_type a[3],
				 int_g &op, coord_type a_coords[3],
				 const coord_type b[][3],
				 const int_g b_asym_idx[],
				 coord_type b_coords[][3], int_g ab_shift[3],
				 const int_g nbatoms) const
{
  // only designed for i, j to i, j, k, l
  if (nbatoms < 1 or nbatoms > 3)
    exit(1);
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type la[3];
  coord_type lb[3];
  coord_type lc[3];
  get_primitive_lattice(la, lb, lc);
  map_asym_pos(asym_idx, a, op, a_coords, b, b_asym_idx, b_coords, ab_shift,
	       nbatoms, la, lb, lc, rotations, translations, nops);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::crystal::conventional_atoms(const atom_tree &primitive, atom_tree &tree,
				      const bool centre[3]) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_conventional_lattice(a, b, c);
  int_g n = get_number_conv_cell_translators();
  real_l (*translators)[3] = new_local_array2(real_l, n, 3);
  get_conv_cell_translators(translators);
  primitive.add_conv_cell_atoms(tree, a, b, c, translators, n, centre);
  delete_local_array(translators);
}

void DLV::crystal::centre_atoms(const atom_tree &primitive, atom_tree &tree,
				const bool centre[3]) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  primitive.copy_and_centre(tree, cell, 3, centre);
}

void DLV::crystal::update_display_atoms(atom_tree &tree, const real_l tol,
					const int_g na, const int_g nb,
					const int_g nc, const bool conventional,
					const bool centre, const bool edges) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.replicate_atoms(a, b, c, tol, na, nb, nc, centre, edges);
}

void DLV::crystal::generate_bonds(const atom_tree &tree, const int_g na,
				  const int_g nb, const int_g nc,
				  const bool conventional,
				  std::list<bond_list> info[],
				  const real_g overlap, const bool centre,
				  const bool invert_colours,
				  const bond_data &bond_info,
				  const real_l tol,
				  const std::vector<real_g (*)[3]> *traj,
				  const int_g nframes, const bool gamma,
				  const bool edit) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.generate_bonds(a, b, c, na, nb, nc, info, nframes, overlap, centre,
		      invert_colours, bond_info, tol, traj, gamma, edit);
}

void DLV::crystal::get_lattice_parameters(coord_type &a, coord_type &b,
					  coord_type &c, coord_type &alpha,
					  coord_type &beta, coord_type &gamma,
					  const bool conventional) const
{
  if (conventional)
    get_parameters(a, b, c, alpha, beta, gamma);
  else
    create_primitive_params(a, b, c, alpha, beta, gamma);
}

void DLV::crystal::identify_bravais_lattice(const int_g old_l,
					    const int old_c)
{
  // attempt to identify the bravais lattice, from e.g. a supercell
  int_g lattice = 0;
  int_g centre = 0;
  int nops = get_number_of_sym_ops();
  if (old_l > 0 and nops > 1) {
    coord_type a;
    coord_type b;
    coord_type c;
    coord_type alpha;
    coord_type beta;
    coord_type gamma;
    get_lattice_parameters(a, b, c, alpha, beta, gamma, false);
    const coord_type tol = 0.001;
    if (alpha > 90.0 - tol and alpha < 90.0 + tol and beta > 90.0 - tol and
	beta < 90.0 + tol) {
      if (gamma > 90.0 - tol and gamma < 90.0 + tol) {
	// orthorhombic, tetragonal or cubic
	if (nops >= 4 and (old_l == 2 or old_l == 3 or old_l == 6)) {
	  if (old_l > 2 and a < b + tol and a > b - tol) {
	    if (old_l == 6 and a < c + tol and a > c - tol and
		b < c + tol and b > c - tol) {
	      lattice = 6; // cubic
	    } else
	      lattice = 3; // tetragonal
	  } else
	    lattice = 2;
	}
      } else if (gamma > 120.0 - tol and gamma < 120.0 + tol) {
	if ((old_l == 4 or old_l == 5) and nops > 2) {
	  // trigonal or hexagonal - how to get hex? nops > 12?
	  if (a < b + tol and a > b - tol) {
	    lattice = 4;
	  }
	}
      }
    } else if (alpha < beta + tol and alpha > beta - tol and
	       alpha < gamma + tol and alpha > gamma - tol and
	       beta < gamma + tol and beta > gamma - tol and
	       a < b + tol and a > b - tol and a < c + tol and a > c - tol and
	       b < c + tol and b > c - tol) {
      // fcc or rhombohedral maybe
      if (old_l == 6) {
	if (centre == 4) {
	  if (alpha < 60.0 + tol and alpha > 60.0 - tol and
	      beta < 60.0 + tol and beta > 60.0 - tol and
	      gamma < 60.0 + tol and gamma > 60.0 - tol) {
	    lattice = 6;
	    centre = 4;
	  } else if (nops >= 3) {
	    lattice = 4;
	    centre = 6;
	  }
	} else if (centre == 5) {
	  // bcc?
	  coord_type angle = to_degrees(acos(1.0 / 3.0));
	  if (alpha < angle + tol and alpha > angle - tol and
	      beta < angle + tol and beta > angle - tol and
	      gamma < angle + tol and gamma > angle - tol) {
	    lattice = 6;
	    centre = 5;
	  }
	}
      } else if (old_l == 4 and centre == 6 and nops >= 3) {
	lattice = 4;
	centre = 6;
      }
    }
    if (old_c == 5 and old_l == 3) {
      if (alpha < beta + tol and alpha > beta - tol and
	  a < b + tol and a > b - tol and a < c + tol and a > c - tol and
	  b < c + tol and b > c - tol) {
	// body centred tetragonal?
	lattice = 3;
	centre = 5;
      }
    }
    // Todo - how to id a/b/c centred mono or orth, body orth?
    if (lattice == 0 and nops > 2) {
      // monoclinic probably, also nops == 2 and op[1] != inversion?
      lattice = 1;
    }
  }
  set_lattice_and_centre(lattice, centre);
}

void DLV::crystal::complete_lattice(coord_type va[3], coord_type vb[3],
				    coord_type vc[3])
{
  const real_l tol = 0.01;
  if (has_valid_parameters() and !has_valid_vectors()) {
    coord_type a;
    coord_type b;
    coord_type c;
    coord_type alpha;
    coord_type beta;
    coord_type gamma;
    get_parameters(a, b, c, alpha, beta, gamma);
    switch (get_lattice_type()) {
    case 1:
      // We should a able to assume these are correct for the 'unique axis'
      // Do we only need > 90.0 + tol?
      if ((abs(alpha) > tol and abs(alpha) < real_l(90.0) - tol) or
	  abs(alpha) > real_l(90.0) + tol) { 
	beta = 90.0;
	gamma = 90.0;
      } else if ((abs(gamma) > tol and abs(gamma) < real_l(90.0) - tol) or
		 abs(gamma) > real_l(90.0) + tol) {
	alpha = 90.0;
	beta = 90.0;
      } else {
	alpha = 90.0;
	gamma = 90.0;
      }
      break;
    case 2:
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    case 3:
      b = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    case 4:
      if ((abs(gamma) > real_l(120.0) - tol and
	   abs(gamma) < real_l(120.0) + tol) or abs(alpha) < tol) {
	// Should have been a hexagonal setting
	b = a;
	alpha = 90.0;
	beta = 90.0;
	gamma = 120.0;
      } else {
	b = a;
	c = a;
	beta = alpha;
	gamma = alpha;
      }
      break;
    case 5:
      b = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 120.0;
      break;
    case 6:
      b = a;
      c = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    default:
      break;
    }
    set_parameters(a, b, c, alpha, beta, gamma);
    create_lattice_vectors(a, b, c, alpha, beta, gamma, va, vb, vc);
    set_lattice(va, vb, vc);
    set_valid_vectors();
    set_valid_parameters();
  } else if (has_valid_vectors() and !has_valid_parameters()) {
    coord_type al[3];
    coord_type bl[3];
    coord_type cl[3];
    get_conventional_lattice(al, bl, cl);
    coord_type a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
    coord_type b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
    coord_type c = sqrt(cl[0] * cl[0] + cl[1] * cl[1] + cl[2] * cl[2]);
    coord_type alpha = to_degrees(acos((bl[0] * cl[0]
					     + bl[1] * cl[1]
					     + bl[2] * cl[2]) / (b * c)));
    coord_type beta  = to_degrees(acos((al[0] * cl[0]
					     + al[1] * cl[1]
					     + al[2] * cl[2]) / (a * c)));
    coord_type gamma = to_degrees(acos((al[0] * bl[0]
					     + al[1] * bl[1]
					     + al[2] * bl[2]) / (a * b)));
    set_parameters(a, b, c, alpha, beta, gamma);
    set_valid_parameters();
    set_valid_vectors();
    get_primitive_lattice(va, vb, vc);
  }
  else if (has_valid_parameters() and has_valid_vectors())
    get_primitive_lattice(va, vb, vc);
}

void DLV::crystal::create_primitive_params(coord_type &a, coord_type &b,
					   coord_type &c, coord_type &alpha,
					   coord_type &beta,
					   coord_type &gamma) const
{
  coord_type al[3];
  coord_type bl[3];
  coord_type cl[3];
  get_primitive_lattice(al, bl, cl);
  a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
  b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
  c = sqrt(cl[0] * cl[0] + cl[1] * cl[1] + cl[2] * cl[2]);
  alpha = to_degrees(acos((bl[0] * cl[0] + bl[1] * cl[1]
			   + bl[2] * cl[2]) / (b * c)));
  beta  = to_degrees(acos((al[0] * cl[0] + al[1] * cl[1]
			   + al[2] * cl[2]) / (a * c)));
  gamma = to_degrees(acos((al[0] * bl[0] + al[1] * bl[1]
			   + al[2] * bl[2]) / (a * b)));
}

void DLV::crystal::create_lattice_vectors(const coord_type a,
					  const coord_type b,
					  const coord_type c,
					  const coord_type alpha,
					  const coord_type beta,
					  const coord_type gamma,
					  coord_type va[3],
					  coord_type vb[3],
					  coord_type vc[3]) const
{
  const real_l tol = 0.01;
  real_l scale[3];
  scale[0] = a;
  scale[1] = b;
  scale[2] = c;
  real_l calpha = alpha;
  real_l cbeta = beta;
  real_l cgamma = gamma;
  // Create unit conventional cell
  real_l ma[3][3] = {{ 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 }};
  real_l xalpha = cos(to_radians(calpha));
  real_l gg;
  switch (get_lattice_type()) {
  case 0:
  case 1:
    gg = to_radians(cgamma);
    ma[0][0] = sin(gg);
    ma[0][1] = cos(gg);
    ma[2][0] = (cos(to_radians(cbeta)) - ma[0][1] * xalpha) / ma[0][0];
    ma[2][1] = xalpha;
    ma[2][2] = sqrt(real_l(1.0) - ma[2][0] * ma[2][0] - xalpha * xalpha);
    break;
  case 4:
    if (abs(gamma) < real_l(120.0) - tol or abs(gamma) > real_l(120.0) + tol) {
      // calculate rhombohedral metric
      real_l gr[3][3];
      gg = scale[0] * scale[0];
      xalpha *= gg;
      for (int_g i = 0; i < 3; i++) {
        for (int_g j = 0; j < 3; j++)
          gr[i][j] = xalpha;
        gr[i][i] = gg;
      }
      // Transform to hex cell
      coord_type winv[3][3];
      get_transform_to_conv_cell(winv);
      real_l s[3][3];
      for (int_g i = 0; i < 3; i++)
	for (int_g j = 0; j < 3; j++)
	  s[i][j] = winv[i][0] * gr[0][j] + winv[i][1] * gr[1][j]
	    + winv[i][2] * gr[2][j];
      real_l gh[3][3];
      for (int_g i = 0; i < 3; i++)
	for (int_g j = 0; j < 3; j++)
	  gh[i][j] = s[i][0] * winv[j][0] + s[i][1] * winv[j][1]
	    + s[i][2] * winv[j][2];
      scale[0] = sqrt(gh[0][0]);
      scale[2] = sqrt(gh[2][2]);
      scale[1] = scale[0];
    }
    ma[0][1] = -0.5;
    ma[0][0] = std::sqrt(0.75);
    break;
  case 5:
    ma[0][1] = -0.5;
    ma[0][0] = std::sqrt(0.75);
    break;
  default:
    break;
  }
  // scale to actual conventional cell
  for (int_g i = 0; i < 3; i++) {
    real_l p = scale[i];
    for (int_g j = 0; j < 3; j++)
      ma[i][j] *= p;
  }
  coord_type w[3][3];
  // Convert cell to primitive
  get_transform_to_prim_cell(w);
  for (int_g i = 0; i < 3; i++) {
    va[i] = w[0][0] * ma[0][i] + w[0][1] * ma[1][i] + w[0][2] * ma[2][i];
    vb[i] = w[1][0] * ma[0][i] + w[1][1] * ma[1][i] + w[1][2] * ma[2][i];
    vc[i] = w[2][0] * ma[0][i] + w[2][1] * ma[1][i] + w[2][2] * ma[2][i];
  }
}

void DLV::crystal::map_data(const atom_tree &tree, const real_g grid[][3],
			    int_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    int_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a, b, c);
}

void DLV::crystal::map_data(const atom_tree &tree, const real_g grid[][3],
			    real_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a, b, c);
}

void DLV::crystal::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mag[][3], const real_g phases[][3],
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g (* &map_data)[3], int_g &n, const real_g ka,
			    const real_g kb, const real_g kc) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mag, phases, ngrid, map_grid, map_data, n,
		ka, kb, kc, a, b, c);
}

void DLV::crystal::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mags[][3], const real_g phases[][3],
			    const int_g ngrid,
			    std::vector<real_g (*)[3]> &new_data, int_g &n,
			    const int_g nframes, const int_g ncopies,
			    const real_g ka, const real_g kb,
			    const real_g kc, const real_g scale) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mags, phases, ngrid, new_data, n, scale,
		nframes, ncopies, ka, kb, kc, a, b, c);
}

void DLV::crystal::map_data(const atom_tree &tree, const real_g grid[][3],
			    const int_g ngrid,
			    const std::vector<real_g (*)[3]> &traj,
			    const int_g nframes,
			    std::vector<real_g (*)[3]> &new_data) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, ngrid, traj, new_data, nframes, a, b, c);
}

void DLV::crystal::build_supercell_symmetry(const model *m,
					    const coord_type a[3],
					    const coord_type b[3],
					    const coord_type c[3],
					    const coord_type va[3],
					    const coord_type vb[3],
					    const coord_type vc[3],
					    const bool conv)
{
  const crystal *sym = static_cast<const crystal *>(m);
  copy_symmetry(sym, false);
  if (conv)
    add_conv_cell_operators(sym, a, b, c, 3);
  build_supercell_operators(a, b, c, va, vb, vc, 3);
}

void DLV::crystal::copy_lattice(const model_base *m)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  set_primitive_lattice(a, b, c);
  coord_type a1;
  coord_type b1;
  coord_type c1;
  coord_type alpha;
  coord_type beta;
  coord_type gamma;
  m->get_lattice_parameters(a1, b1, c1, alpha, beta, gamma);
  set_parameters(a1, b1, c1, alpha, beta, gamma);
  set_valid_vectors();
}

void DLV::crystal::copy_bravais_lattice(const model_base *m)
{
  const crystal *sym = static_cast<const crystal *>(m);
  copy_lattice_type(sym);
}

void DLV::crystal::copy_symmetry_ops(const model_base *m, const bool frac)
{
  const crystal *sym = static_cast<const crystal *>(m);
  copy_symmetry(sym, frac);
}

void DLV::crystal::rotate_symmetry_ops(const model_base *m,
				       const real_l r[3][3])
{
  const crystal *sym = static_cast<const crystal *>(m);
  rotate_symmetry(sym, r);
}

void DLV::crystal::shift_cartesian_operators(const model_base *m,
					     const real_l x, const real_l y,
					     const real_l z,
					     const coord_type a[3],
					     const coord_type b[3],
					     const coord_type c[3])
{
  const crystal *sym = static_cast<const crystal *>(m);
  shift_ops_cartesian(sym->get_symmetry(), x, y, z, a, b, c, 3);
}

void DLV::crystal::shift_fractional_operators(const model_base *m,
					      const real_l x, const real_l y,
					      const real_l z,
					      const coord_type a[3],
					      const coord_type b[3],
					      const coord_type c[3])
{
  const crystal *sym = static_cast<const crystal *>(m);
  shift_ops_fractional(sym->get_symmetry(), x, y, z, a, b, c, 3);
}

void DLV::crystal::build_bravais_lattice_from_slab(const int_g l, const int_g c)
{
  // Todo - improve centring info?
  int_g lattice; 
  switch (l) {
  case 1:
    lattice = (int)orthorhombic;
    break;
  case 2:
    lattice = (int)trigonal;
    break;
  case 3:
    lattice = (int)tetragonal;
    break;
  case 4:
    lattice = (int)hexagonal;
    break;
  default:
    lattice = (int)monoclinic;
    break;
  }
  set_lattice_and_centre(lattice, c);
}

void DLV::crystal::cartesian_to_fractional_coords(real_l &x, real_l &y,
						  real_l &z) const
{
  // get x, y, z as fractional
  coord_type l[3][3];
  get_primitive_vectors(l);
  real_l inverse[3][3];
  matrix_invert(l, inverse, 3);
  real_l s;
  real_l t;
  s = inverse[0][0] * x + inverse[1][0] * y + inverse[2][0] * z;
  t = inverse[0][1] * x + inverse[1][1] * y + inverse[2][1] * z;
  z = inverse[0][2] * x + inverse[1][2] * y + inverse[2][2] * z;
  x = s;
  y = t;
}

void DLV::crystal::fractional_to_cartesian_coords(real_l &x, real_l &y,
						  real_l &z) const
{
  // get x, y, z as cartesian
  coord_type l[3][3];
  get_primitive_vectors(l);
  real_l s;
  real_l t;
  s = l[0][0] * x + l[0][1] * y + l[0][2] * z;
  t = l[1][0] * x + l[1][1] * y + l[1][2] * z;
  z = l[2][0] * x + l[2][1] * y + l[2][2] * z;
  x = s;
  y = t;
}

bool DLV::crystal::map_atom_selections(const atom_tree &display,
				       const atom_tree &prim,
				       const real_g grid[][3],
				       int_g **const data, const int_g ndata,
				       const int_g ngrid, int_g * &labels,
				       int_g * &atom_types, atom * &parents,
				       int_g &n, const bool all_atoms) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_atom_selections(prim, n, labels, atom_types, parents,
				     all_atoms, grid, data, ndata, ngrid,
				     a, b, c);
}

bool DLV::crystal::map_selected_atoms(const atom_tree &display, int_g &n,
				      int_g * &labels, int_g (* &shifts)[3],
				      const real_g grid[][3],
				      int_g **const data, const int_g ndata,
				      const int_g ngrid) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_selected_atoms(n, labels, shifts, grid, data, ndata,
				    ngrid, a, b, c);
}

void DLV::crystal::get_atom_shifts(const atom_tree &tree,
				   const coord_type coords[][3],
				   int_g shifts[][3], int_g prim_id[],
				   const int_g n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.get_atom_shifts(coords, shifts, prim_id, n, a, b, c);
}  

#ifdef ENABLE_DLV_GRAPHICS

void DLV::crystal::generate_plane_normal(const int_g h, const int_g k,
					 const int_g l, const real_l a[3],
					 const real_l b[3], const real_l c[3],
					 real_l normal[3]) const
{
  // update2D (plane)
  real_l origin[3];
  real_l pointa[3];
  real_l pointb[3];
  real_l t;
  if (h == 0 and k == 0) {
    t = 1.0 / (real_l) l;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * c[i];
      pointa[i] = origin[i] + a[i];
      pointb[i] = origin[i] + b[i];
    }
  } else if (h == 0 and l == 0) {
    t = 1.0 / (real_l) k;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * b[i];
      pointa[i] = origin[i] + c[i];
      pointb[i] = origin[i] + a[i];
    }
  } else if (k == 0 and l == 0) {
    t = 1.0 / (real_l) h;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * a[i];
      pointa[i] = origin[i] + b[i];
      pointb[i] = origin[i] + c[i];
    }
  } else if (h == 0) {
    t = 1.0 / (real_l) k;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * b[i];
      pointa[i] = origin[i] + a[i];
    }
    t = 1.0 / (real_l) l;
    for (int_g i = 0; i < 3; i++)
      pointb[i] = t * c[i];
  } else if (k == 0) {
    t = 1.0 / (real_l) h;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * a[i];
      pointa[i] = origin[i] + b[i];
    }
    t = 1.0 / (real_l) l;
    for (int_g i = 0; i < 3; i++)
      pointb[i] = t * c[i];
  } else if (l == 0) {
    t = 1.0 / (real_l) h;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = t * a[i];
      pointa[i] = origin[i] + c[i];
    }
    t = 1.0 / (real_l) k;
    for (int_g i = 0; i < 3; i++)
      pointb[i] = t * b[i];
  } else {
    t = 1.0 / (real_l) h;
    for (int_g i = 0; i < 3; i++)
      origin[i] = t * a[i];
    t = 1.0 / (real_l) k;
    for (int_g i = 0; i < 3; i++)
      pointa[i] = t * b[i];
    t = 1.0 / (real_l) l;
    for (int_g i = 0; i < 3; i++)
      pointb[i] = t * c[i];
  }
  real_l oa[3];
  for (int_g i = 0; i < 3; i++)
    oa[i] = pointa[i] - origin[i];
  real_l ob[3];
  for (int_g i = 0; i < 3; i++)
    ob[i] = pointb[i] - origin[i];
  real_l len = 0.0;
  normal[0] = (oa[1] * ob[2] - oa[2] * ob[1]);
  len += normal[0] * normal[0];
  normal[1] = (oa[2] * ob[0] - oa[0] * ob[2]);
  len += normal[1] * normal[1];
  normal[2] = (oa[0] * ob[1] - oa[1] * ob[0]);
  len += normal[2] * normal[2];
  len = std::sqrt(len);
  for (int_g i = 0; i < 3; i++)
    normal[i] /= len;
  // add normal to origin and make sure its distance is the opposite sign
  // to the origin, i.e. outward facing
  // (origin - 0).n
  real_l odist = origin[0] * normal[0] + origin[1] * normal[1]
    + origin[2] * normal[2];
  // (origin - no).n point no is origin + normal
  real_l ndist = -(normal[0] * normal[0] + normal[1] * normal[1] +
		   normal[2] * normal[2]);
  // They should be opposite signs
  if ((odist < 0.0 and ndist < 0.0) or (odist > 0.0 and ndist > 0.0)) {
    normal[0] = - normal[0];
    normal[1] = - normal[1];
    normal[2] = - normal[2];
  }
}

void DLV::crystal::generate_wulff_data(const std::list<wulff_data> &planes,
				       string labels[], real_g rgb[][3],
				       real_l normals[][3], int_g &nnormals,
				       real_l (* &myverts)[3],
				       int_g ** &face_vertices,
				       int_g * &nface_vertices, int_g &nfaces,
				       int_g &nvertices) const
{
  coord_type pa[3];
  coord_type pb[3];
  coord_type pc[3];
  get_primitive_lattice(pa, pb, pc);
  coord_type ca[3];
  coord_type cb[3];
  coord_type cc[3];
  get_conventional_lattice(ca, cb, cc);
  //int lattice_type = get_lattice_type();
  int_g count = 0;
  std::list<wulff_data>::const_iterator ptr;
  for (ptr = planes.begin(); ptr != planes.end(); ++ptr ) {
    // Todo - does the centring cause problems?
    count += 2;
  }
  real_l *energies = new_local_array1(real_l, count);
  int_g *colours = new_local_array1(int_g, count);
  for (int_g i = 0; i < count; i++)
    colours[i] = 1;
  for (ptr = planes.begin(); ptr != planes.end(); ++ptr ) {
    int_g h = ptr->h;
    int_g k = ptr->k;
    int_g l = ptr->l;
    real_l a[3];
    real_l b[3];
    real_l c[3];
    if (ptr->conventional) {
      for (int_g i = 0; i < 3; i++) {
	a[i] = ca[i];
	b[i] = cb[i];
	c[i] = cc[i];
      }
    } else {
      for (int_g i = 0; i < 3; i++) {
	a[i] = pa[i];
	b[i] = pb[i];
	c[i] = pc[i];
      }
    }
    generate_plane_normal(h, k, l, a, b, c, normals[nnormals]);
    energies[nnormals] = ptr->energy;
    if (rgb != 0) {
      rgb[nnormals][0] = ptr->red;
      rgb[nnormals][1] = ptr->green;
      rgb[nnormals][2] = ptr->blue;
    }
    char buff[128];
    if (labels != 0) {
      snprintf(buff, 128, "{%1d%1d%1d}", h, k, l);
      labels[nnormals] = buff;
    }
    // (-h -k -l)
    energies[nnormals + 1] = ptr->energy;
    for (int_g i = 0; i < 3; i++)
      normals[nnormals + 1][i] = - normals[nnormals][i];
    if (rgb != 0) {
      for (int_g i = 0; i < 3; i++)
	rgb[nnormals + 1][i] = rgb[nnormals][i];
    }
    if (labels != 0) {
      snprintf(buff, 128, "{%1d%1d%1d}", -h, -k, -l);
      labels[nnormals + 1] = buff;
    }
    nnormals += 2;
  }
  // Call wulffman code to generate object
  output_OOGL(nnormals, normals, energies, colours, myverts, nvertices,
	      face_vertices, nface_vertices, nfaces);
  delete_local_array(colours);
  delete_local_array(energies);
}

bool DLV::crystal::generate_wulff_plot(std::list<wulff_data> &planes,
				       real_g (* &vertices)[3], int_g &nverts,
				       real_g (* &vert_cols)[3],
				       real_g (* &vert_norms)[3],
				       real_g (* &pos)[3], string * &names,
				       int_l * &connects, int_l &nconnects,
				       int_g * &nodes, int_g &nnodes,
				       real_g (* &line_verts)[3], int_g &nlv,
				       int_l * &lines, int_l &nlines) const
{
  int_g count = 0;
  std::list<wulff_data>::const_iterator ptr;
  for (ptr = planes.begin(); ptr != planes.end(); ++ptr ) {
    // Todo - does the centring cause problems?
    count += 2;
  }
  int_g nnormals = 0;
  real_l (*normals)[3] = new_local_array2(real_l, count, 3);
  real_g (*rgb)[3] = new_local_array2(real_g, count, 3);
  string *labels = new string[count];
  int_g nvertices = 0, nfaces = 0;
  int_g **face_vertices, *nface_vertices = 0;
  real_l (*myverts)[3];
  generate_wulff_data(planes, labels, rgb, normals, nnormals, myverts,
		      face_vertices, nface_vertices, nfaces, nvertices);
  nverts = nvertices;
  nlv = nvertices;
  if (nvertices > 0) {
    // replicate vertices for each object so can colour separately
    nodes = new int_g[nfaces];
    int_g count = 0;
    nnodes = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	nodes[nnodes] = nface_vertices[i];
	count += nface_vertices[i];
	nnodes++;
      }
    }
    nverts = count;
    vertices = new real_g[count][3];
    vert_cols = new real_g[count][3];
    vert_norms = new real_g[count][3];
    nconnects = count;
    connects = new int_l[count];
    names = new string[nnodes];
    pos = new real_g[nnodes][3];
    count = 0;
    int_g index = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	pos[index][0] = 0.0;
	pos[index][1] = 0.0;
	pos[index][2] = 0.0;
	//fprintf(stderr, "face %d - %s\n", i, labels[i].c_str());
	for (int_g j = 0; j < nface_vertices[i]; j++) {
	  vertices[count][0] = (real_g)myverts[face_vertices[i][j]][0];
	  vertices[count][1] = (real_g)myverts[face_vertices[i][j]][1];
	  vertices[count][2] = (real_g)myverts[face_vertices[i][j]][2];
	  //fprintf(stderr, "%f %f %f\n", vertices[count][0],
	  //	  vertices[count][1], vertices[count][2]);
	  pos[index][0] += vertices[count][0];
	  pos[index][1] += vertices[count][1];
	  pos[index][2] += vertices[count][2];
	  vert_cols[count][0] = rgb[i][0];
	  vert_cols[count][1] = rgb[i][1];
	  vert_cols[count][2] = rgb[i][2];
	  vert_norms[count][0] = (real_g)normals[i][0];
	  vert_norms[count][1] = (real_g)normals[i][1];
	  vert_norms[count][2] = (real_g)normals[i][2];
	  connects[count] = count;
	  count++;
	}
	pos[index][0] /= (real_g)nface_vertices[i];
	pos[index][1] /= (real_g)nface_vertices[i];
	pos[index][2] /= (real_g)nface_vertices[i];
	names[index] = labels[i];
	index++;
      }
    }
    nlv = nvertices;
    line_verts = new real_g[nvertices][3];
    for (int_g i = 0; i < nvertices; i++) {
      line_verts[i][0] = (real_g) myverts[i][0];
      line_verts[i][1] = (real_g) myverts[i][1];
      line_verts[i][2] = (real_g) myverts[i][2];
    }
    delete [] myverts;
    // Number of lines
    count = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	count += nface_vertices[i];
      }
    }
    nlines = 2 * count;
    lines = new int_l[2 * count];
    count = 0;
    for (int_g i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	for (int_g j = 0; j < nface_vertices[i]; j++) {
	  lines[count] = face_vertices[i][j];
	  lines[count + 1] = face_vertices[i][(j + 1) % nface_vertices[i]];
	  count += 2;
	}
      }
    }
    for (int_g i = 0; i < nfaces; i++)
      if (face_vertices[i] != 0)
	delete [] face_vertices[i];
    delete [] nface_vertices;
  }
  delete_local_array(rgb);
  delete_local_array(normals);
  delete [] labels;
  return true;
}

#endif // ENABLE_DLV_GRAPHICS

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::crystal *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::crystal *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::crystal(n);
    }

  }
}

template <class Archive>
void DLV::crystal::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<periodic_model<volume_symmetry> >
    (*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::crystal)

DLV_SUPPRESS_TEMPLATES(DLV::model_impl<DLV::volume_symmetry>)

DLV_NORMAL_EXPLICIT(DLV::crystal)

#endif // DLV_USES_SERIALIZE
