
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "constants.hxx"
#include "math_fns.hxx"
#include "data_objs.hxx"
// for update in atom_scalars
#include "utils.hxx"
#include "model.hxx"
#include "data_simple.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "symmetry.hxx"
#include "model_atoms.hxx"
#include "data_atoms.hxx"

static const DLV::int_g block_size = 16;

DLV::atom_scalars::~atom_scalars()
{
  delete [] coords;
  delete [] labels;
}

DLV::atom_integers::~atom_integers()
{
  for (int_g i = 0; i < get_ndata_sets(); i++)
    delete [] data[i];
  delete [] data;
}

DLV::atom_reals::~atom_reals()
{
  for (int_g i = 0; i < get_ndata_sets(); i++)
    delete [] data[i];
  delete [] data;
}

DLV::phonon_vectors::~phonon_vectors()
{
  delete [] coords;
  delete [] vectors;
}

DLV::md_trajectory::~md_trajectory()
{
  delete [] atom_coords;
  for (int_g i = 0; i < nsteps; i++) {
    delete [] vectors[i];
    delete [] trajectory[i];
  }
}

bool DLV::atom_based_data::is_displayable() const
{
  return true;
}

bool DLV::atom_based_data::is_editable() const
{
  return false;
}

DLV::string DLV::atom_based_data::get_edit_label() const
{
  string name = "BUG: atom data";
  return name;
}

DLV::int_g DLV::atom_based_data::get_display_type() const
{
  return (int)display_atoms;
}

DLV::int_g DLV::atom_based_data::get_edit_type() const
{
  return (int)no_edit;
}

bool DLV::atom_based_data::get_sub_is_vector(const int_g) const
{
  return false;
}

DLV::int_g DLV::atom_scalars::get_number_data_sets() const
{
  return n_data_sets;
}

DLV::string DLV::atom_scalars::get_obj_label() const
{
  return name;
}

DLV::string DLV::atom_scalars::get_data_label() const
{
  string tag = name + get_tags();
  return tag;
}

DLV::string DLV::atom_scalars::get_edit_label() const
{
  string tag = name + get_tags();
  return tag;
}

DLV::string DLV::atom_scalars::get_sub_data_label(const int_g n) const
{
  return labels[n];
}

void DLV::atom_scalars::unload_data()
{
  fprintf(stderr, "atom scalars unload not implemented - Todo\n");
}

void DLV::atom_scalars::set_grid(real_g grid[][3], const int_g n,
				 const bool copy_data)
{
  natoms = n;
  if (copy_data) {
    coords = new real_g[n][3];
    for (int_g i = 0; i < n; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
  } else
    coords = grid;
}

void DLV::atom_scalars::set_grid(const atom_scalars *copy)
{
  natoms = copy->get_data_length();
  coords = new real_g[natoms][3];
  const real_g (*grid)[3] = copy->get_grid();
  for (int_g i = 0; i < natoms; i++) {
    coords[i][0] = grid[i][0];
    coords[i][1] = grid[i][1];
    coords[i][2] = grid[i][2];
  }
}

void DLV::atom_scalars::add_data(const string label)
{
  if (n_data_sets == size) {
    string *new_str = new string[size + block_size];
    for (int_g i = 0; i < size; i++)
      new_str[i] = labels[i];
    if (labels != 0)
      delete [] labels;
    labels = new_str;
    size += block_size;
  }
  labels[n_data_sets] = label;
  n_data_sets++;
}

void DLV::atom_integers::add_data(int_g values[], const string label,
				  const bool copy_data)
{
  int_g n = get_ndata_sets();
  int_g s = get_data_size();
  atom_scalars::add_data(label);
  if (n == s) {
    int_g **new_data = new int_g*[s + block_size];
    for (int_g i = 0; i < s; i++)
      new_data[i] = data[i];
    if (data != 0)
      delete [] data;
    data = new_data;
  }
  if (copy_data) {
    int_g l = get_data_length();
    data[n] = new int_g[l];
    for (int_g i = 0; i < l; i++)
      data[n][i] = values[i];
  } else
    data[n] = values;
}

void DLV::atom_reals::add_data(real_g values[], const string label,
			       const bool copy_data)
{
  int_g n = get_ndata_sets();
  int_g s = get_data_size();
  atom_scalars::add_data(label);
  if (n == s) {
    real_g **new_data = new real_g*[s + block_size];
    for (int_g i = 0; i < s; i++)
      new_data[i] = data[i];
    if (data != 0)
      delete [] data;
    data = new_data;
  }
  if (copy_data) {
    int_g l = get_data_length();
    data[n] = new real_g[l];
    for (int_g i = 0; i < l; i++)
      data[n][i] = values[i];
  } else
    data[n] = values;
}

void DLV::phonon_vectors::set_grid(real_g grid[][3], const int_g n,
				   const bool copy_data)
{
  natoms = n;
  if (copy_data) {
    coords = new real_g[n][3];
    for (int_g i = 0; i < n; i++) {
      coords[i][0] = grid[i][0];
      coords[i][1] = grid[i][1];
      coords[i][2] = grid[i][2];
    }
  } else
    coords = grid;
}

bool DLV::phonon_vectors::add_data(const int_g kp, const int_g fp,
				   const real_g ka, const real_g kb,
				   const real_g kc, const real_g freq,
				   real_g mags[][3], const int_g length,
				   const bool copy_data)
{
  if (kp < 0 or kp >= n_k_points)
    return false;
  if (fp < 0 or fp >= nfrequencies)
    return false;
  if (vectors[kp].phonons == 0) {
    vectors[kp].ka = ka;
    vectors[kp].kb = kb;
    vectors[kp].kc = kc;
    vectors[kp].nfrequencies = nfrequencies;
    vectors[kp].phonons = new phonon_frequency[nfrequencies];
  }
  if (vectors[kp].phonons == 0)
    return false;
  vectors[kp].phonons[fp].frequency = freq;
  if (natoms == 0)
    natoms = length;
  else if (natoms != length)
    return false;
  vectors[kp].phonons[fp].natoms = natoms;
  if (copy_data) {
    vectors[kp].phonons[fp].mags = new real_g[length][3];
    for (int_g i = 0; i < length; i++) {
      vectors[kp].phonons[fp].mags[i][0] = mags[i][0];
      vectors[kp].phonons[fp].mags[i][1] = mags[i][1];
      vectors[kp].phonons[fp].mags[i][2] = mags[i][2];
    }
  } else
    vectors[kp].phonons[fp].mags = mags;
  if (vectors[kp].has_phases) {
    vectors[kp].phonons[fp].phases = new real_g[length][3];
    for (int_g i = 0; i < length; i++) {
      vectors[kp].phonons[fp].phases[i][0] = 0.0;
      vectors[kp].phonons[fp].phases[i][1] = 0.0;
      vectors[kp].phonons[fp].phases[i][2] = 0.0;
    }
  }
  return true;
}

bool DLV::phonon_vectors::add_data(const int_g kp, const int_g fp,
				   const real_g ka, const real_g kb,
				   const real_g kc, const real_g freq,
				   const string sym, real_g mags[][3],
				   const int_g length, const bool copy_data)
{
  if (add_data(kp, fp, ka, kb, kc, freq, mags, length, copy_data)) {
    vectors[kp].phonons[fp].symbol = sym;
    return true;
  } else
    return false;
}

bool DLV::phonon_vectors::add_data(const int_g kp, const int_g fp,
				   const real_g ka, const real_g kb,
				   const real_g kc, const real_g freq,
				   real_g mags[][3], real_g phases[][3],
				   const int_g length, const bool copy_data)
{
  if (kp < 0 or kp >= n_k_points)
    return false;
  if (fp < 0 or fp >= nfrequencies)
    return false;
  if (vectors[kp].phonons == 0) {
    vectors[kp].ka = ka;
    vectors[kp].kb = kb;
    vectors[kp].kc = kc;
    vectors[kp].nfrequencies = nfrequencies;
    vectors[kp].phonons = new phonon_frequency[nfrequencies];
  }
  if (vectors[kp].phonons == 0)
    return false;
  vectors[kp].phonons[fp].frequency = freq;
  if (natoms == 0)
    natoms = length;
  else if (natoms != length)
    return false;
  vectors[kp].phonons[fp].natoms = natoms;
  if (copy_data) {
    vectors[kp].phonons[fp].mags = new real_g[length][3];
    vectors[kp].phonons[fp].phases = new real_g[length][3];
    for (int_g i = 0; i < length; i++) {
      vectors[kp].phonons[fp].mags[i][0] = mags[i][0];
      vectors[kp].phonons[fp].mags[i][1] = mags[i][1];
      vectors[kp].phonons[fp].mags[i][2] = mags[i][2];
      vectors[kp].phonons[fp].phases[i][0] = phases[i][0];
      vectors[kp].phonons[fp].phases[i][1] = phases[i][1];
      vectors[kp].phonons[fp].phases[i][2] = phases[i][2];
    }
  } else {
    vectors[kp].phonons[fp].mags = mags;
    vectors[kp].phonons[fp].phases = phases;
  }
  if (fp > 0 and !vectors[kp].has_phases) {
    for (int_g j = 0; j < fp - 1; j++) {
      vectors[kp].phonons[j].phases = new real_g[length][3];
      for (int_g i = 0; i < length; i++) {
	vectors[kp].phonons[j].phases[i][0] = 0.0;
	vectors[kp].phonons[j].phases[i][1] = 0.0;
	vectors[kp].phonons[j].phases[i][2] = 0.0;
      }
    }
  }
  vectors[kp].has_phases = true;
  return true;
}

bool DLV::phonon_vectors::add_data(const int_g kp, const int_g fp,
				   const real_g ka, const real_g kb,
				   const real_g kc, const real_g freq,
				   const string sym, real_g mags[][3],
				   real_g phases[][3], const int_g length,
				   const bool copy_data)
{
  if (add_data(kp, fp, ka, kb, kc, freq, mags, phases, length, copy_data)) {
    vectors[kp].phonons[fp].symbol = sym;
    return true;
  } else
    return false;
}

void DLV::phonon_vectors::set_intensities(const int_g kp, const int_g fp,
					  const real_g intensity,
					  const int_g ir, const int_g raman)
{
  if (ir or raman) {
    real_g i = intensity;
    if (intensity == 0.0)
      i = 1.0;
    if (ir)
      vectors[kp].phonons[fp].ir_intensity = i;
    if (raman)
      vectors[kp].phonons[fp].raman_intensity = i;
  }
}

bool DLV::vectors_and_intensity::add_data(const int_g kp, const int_g fp,
					  const real_g ka, const real_g kb,
					  const real_g kc, const real_g freq,
					  const real_g intensity,
					  const int_g ir, const int_g raman,
					  const string sym, real_g mags[][3],
					  const int_g length,
					  const bool copy_data)
{
  if (phonon_vectors::add_data(kp, fp, ka, kb, kc, freq, sym,
			       mags, length, copy_data)) {
    set_intensities(kp, fp, intensity, ir, raman);
    return true;
  } else
    return false;
}


void DLV::phonon_vectors::unload_data()
{
  fprintf(stderr, "phonon vectors unload not implemented - Todo\n");
}

void DLV::md_trajectory::set_data(real_g (*c)[3], const coord_type coords[][3],
				  const int_g n, const int_g s)
{
  // coords are used to generate offsets from the original structure
  natoms = n;
  atom_coords = new real_g[natoms][3];
  for (int_g j = 0; j < natoms; j++)
    for (int_g k = 0; k < 3; k++)
      atom_coords[j][k] = real_g(coords[j][k]);
  for (int_g i = 0; i < nsteps; i++) {
    trajectory[i] = new real_g[natoms][3];
    // c is [natoms][s][3]
    for (int_g j = 0; j < natoms; j++)
      for (int_g k = 0; k < 3; k++)
	trajectory[i][j][k] = c[j * s + i][k] - real_g(coords[j][k]);
  }
}

bool DLV::phonon_vectors::is_editable() const
{
  return true;
}

DLV::string DLV::phonon_vectors::get_data_label() const
{
  string name = "Phonon vectors";
  name += get_tags();
  return name;
}

DLV::string DLV::md_trajectory::get_data_label() const
{
  string name = "MD trajectory";
  name += get_tags();
  return name;
}

DLV::int_g DLV::phonon_vectors::get_number_data_sets() const
{
  return nfrequencies * n_k_points;
}

DLV::int_g DLV::phonon_vectors::get_edit_type() const
{
  return (int)edit_phonons;
}

DLV::string DLV::phonon_vectors::get_edit_label() const
{
  string name = "Phonon vectors";
  name += get_tags();
  return name;
}

DLV::int_g DLV::vectors_and_intensity::get_edit_type() const
{
  return (int)edit_intens;
}

DLV::string DLV::phonon_vectors::get_sub_data_label(const int_g n) const
{
  int_g kp = n / nfrequencies;
  int_g fp = n % nfrequencies;
  char buff[128];
  snprintf(buff, 128, "%s %.1f cm-1, k = %6.3f %6.3f %6.3f",
	   vectors[kp].phonons[fp].symbol.c_str(),
	   to_double(vectors[kp].phonons[fp].frequency),
	   to_double(vectors[kp].ka), to_double(vectors[kp].kb),
	   to_double(vectors[kp].kc));
  string name = buff;
  return name;
}

DLV::int_g DLV::md_trajectory::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::md_trajectory::get_sub_data_label(const int_g n) const
{
  string name = "";
  return name;
}

void DLV::md_trajectory::unload_data()
{
  fprintf(stderr, "MD trajectory unload not implemented - Todo\n");
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::atom_scalars::render_data(const class model *structure,
					      char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::phonon_vectors::render_data(const class model *structure,
						char message[],
						const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::md_trajectory::render_data(const class model *structure,
					       char message[],
					       const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLV::edit_phonon_trajectory::~edit_phonon_trajectory()
{
  for (int_g i = 0; i < number_of_frames; i++)
    delete [] vectors[i];
}

void DLV::atom_based_data::reset_data_sets()
{
  fprintf(stderr, "Todo - atom data and reset\n");
}

void DLV::atom_scalars::reset_data_sets()
{
  //clb first attempt
  n_data_sets = 0;
  size = 0;
}

void DLV::atom_integers::reset_data_sets()
{
  //clb first attempt
  for (int_g i = 0; i < get_ndata_sets(); i++)
    delete [] data[i];
  delete [] data;
  data = new int_g*[block_size];
  DLV::atom_scalars::reset_data_sets();
}

void DLV::atom_reals::reset_data_sets()
{
  // do nothing
}

void DLV::md_trajectory::reset_data_sets()
{
  // do nothing
}

void DLV::atom_integers::set_3D_display(const class model *structure)
{
   drawable_obj *obj = get_drawable();
   real_g (*map_grid)[3] = 0;
   int_g **map_data = 0;
   int_g n = 0;
   int_g mlen = 80;
   char message[80];
   structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			    get_data_length(), map_grid, map_data, n);
   obj->update_text_field(map_grid, map_data, get_labels(), get_ndata_sets(),
			  n, message, mlen);

   DLV::display_obj *obj_clb = get_display_obj();
   obj_clb->display_3D();
}

void DLV::atom_integers::unset_3D_display(const class model *structure)
{
  DLV::display_obj *obj_clb = get_display_obj();
  obj_clb->undisplay_3D();
}

DLVreturn_type DLV::atom_integers::render(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    real_g (*map_grid)[3] = 0;
    int_g **map_data = 0;
    int_g n = 0;
    structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			     get_data_length(), map_grid, map_data, n);
    obj = DLV::drawable_obj::create_text_field(parent, map_grid, map_data,
					       get_labels(), get_ndata_sets(),
					       n, message, mlen);
    for (int_g i = 0; i < get_ndata_sets(); i++)
      delete [] map_data[i];
    delete [] map_data;
    delete [] map_grid;
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_labels()[component];
  ptr = display_obj::create_atom_text(parent, obj, component, name, index,
				      message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::atom_reals::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index,
				       char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    real_g (*map_grid)[3] = 0;
    real_g **map_data = 0;
    int_g n = 0;
    structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			     get_data_length(), map_grid, map_data, n);
    obj = DLV::drawable_obj::create_text_field(parent, map_grid, map_data,
					       get_labels(), get_ndata_sets(),
					       n, message, mlen);
    for (int_g i = 0; i < get_ndata_sets(); i++)
      delete [] map_data[i];
    delete [] map_data;
    delete [] map_grid;
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_labels()[component];
  ptr = display_obj::create_atom_text(parent, obj, component, name, index,
				      message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

void DLV::phonon_vectors::reset_data_sets()
{
  // do nothing
}

DLVreturn_type DLV::phonon_vectors::render(const render_parent *parent,
					   class model *structure,
					   const int_g component,
					   const int_g method,
					   const int_g index,
					   char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    //int kp = component / nfrequencies;
    //int fp = component % nfrequencies;
    real_g (*map_grid)[3] = 0;
    real_g (*map_data)[3] = 0;
    int_g total = n_k_points * nfrequencies;
    // Todo - do we want to split top level across kpoints
    for (int_g kp = 0; kp < n_k_points; kp++) {
      // Todo - more efficient loops over atoms
      for (int_g fp = 0; fp < nfrequencies; fp++) {
	int_g n = 0;
	structure->map_atom_data(coords, vectors[kp].phonons[fp].mags,
				 vectors[kp].phonons[fp].phases, natoms,
				 map_grid, map_data, n, vectors[kp].ka,
				 vectors[kp].kb, vectors[kp].kc);
	if (obj == 0) {
	  obj = DLV::drawable_obj::create_vector_field(parent, map_grid,
						       map_data, n, total,
						       message, mlen);
	  if (obj == 0)
	    return DLV_ERROR;
	  else
	    set_drawable(obj);
	} else {
	  total = nfrequencies * kp + fp;
	  obj->update_vector_field(map_grid, map_data, n,
				   total, message, mlen);
	}
	delete [] map_data;
	delete [] map_grid;
      }
    }
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  ptr = display_obj::create_atom_vectors(parent, obj, component, name, index,
					 message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    name += " (vectors)";
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::md_trajectory::render(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  (void) reload_data(this, message, mlen);
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_phonon_anim(parent, 0, name, index,
					message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
    structure->set_atom_data(parent, this);
  }
  return DLV_OK;
}

bool DLV::atom_scalars::update_atom_selection_info(const model *m,
						   char message[],
						   const int_g mlen)
{
  drawable_obj *d = get_drawable();
  if (d != 0) {
    // should probably be if > 0 display_objs?
    int_g index = m->locate_selected_atom();
    if (index < 0)
      d->update_selected_atom(-1);
    else
      d->update_selected_atom(index);
  }
  return true;
}

bool DLV::atom_integers::update_cell_info(model *m, char message[],
					  const int_g mlen)
{
  drawable_obj *d = get_drawable();
  if (d != 0) {
    real_g (*map_grid)[3] = 0;
    int_g **map_data = 0;
    int_g n = 0;
    m->map_atom_data(get_grid(), data, get_ndata_sets(),
		     get_data_length(), map_grid, map_data, n);
    d->update_text_field(map_grid, map_data, get_labels(), get_ndata_sets(),
			 n, message, mlen);
    for (int_g i = 0; i < get_ndata_sets(); i++)
      delete [] map_data[i];
    delete [] map_data;
    delete [] map_grid;
  }
  return true;
}

bool DLV::atom_reals::update_cell_info(model *m, char message[],
				       const int_g mlen)
{
  drawable_obj *d = get_drawable();
  if (d != 0) {
    real_g (*map_grid)[3] = 0;
    real_g **map_data = 0;
    int_g n = 0;
    m->map_atom_data(get_grid(), data, get_ndata_sets(),
		     get_data_length(), map_grid, map_data, n);
    d->update_text_field(map_grid, map_data, get_labels(),
			 get_ndata_sets(), n, message, mlen);
    for (int_g i = 0; i < get_ndata_sets(); i++)
      delete [] map_data[i];
    delete [] map_data;
    delete [] map_grid;
  }
  return true;
}

bool DLV::phonon_vectors::update_cell_info(model *m, char message[],
					   const int_g mlen)
{
  drawable_obj *d = get_drawable();
  if (d != 0) {
    //int kp = 0;
    //int fp = 0;
    //d->get_vector_component(kp, fp);
    real_g (*map_grid)[3] = 0;
    real_g (*map_data)[3] = 0;
    int_g k = 0;
    for (int_g kp = 0; kp < n_k_points; kp++) {
      // Todo - more efficient loops over atoms
      for (int_g fp = 0; fp < nfrequencies; fp++) {
	int_g n = 0;
	m->map_atom_data(coords, vectors[kp].phonons[fp].mags,
			 vectors[kp].phonons[fp].phases, natoms, map_grid,
			 map_data, n, vectors[kp].ka, vectors[kp].kb,
			 vectors[kp].kc);
	d->update_vector_field(map_grid, map_data, n, k, message, mlen);
	delete [] map_data;
	delete [] map_grid;
	k++;
      }
    }
  }
  return true;
}

bool DLV::atom_integers::map_atom_selections(const model *m, int_g &n,
					     int_g * &labels,
					     int_g * &atom_types,
					     atom * &parents,
					     const bool all_atoms) const
{
  const model_base *mb = dynamic_cast<const model_base *>(m);
  if (mb == nullptr)
    return false;
  else
    return mb->map_atom_selections(n, labels, atom_types, parents, all_atoms,
				   get_grid(), data, get_ndata_sets(),
				   get_data_length());
}

bool DLV::atom_integers::map_selected_atoms(const model *m, int_g &n,
					    int_g * &labels,
					    int_g (* &shifts)[3]) const
{
  return m->map_selected_atoms(n, labels, shifts, get_grid(), data,
			       get_ndata_sets(), get_data_length());
}

bool DLV::atom_integers::swop_two_data_items(const int_g item1, 
					     const int_g item2,
					     const int_g dataset,
					     const class model *structure)
{
   if(dataset >= get_ndata_sets())
	return false;
    for(int_g i = 0; i < get_data_length(); i++){
	if(data[dataset][i] == item1) data[dataset][i] = item2;
	else if(data[dataset][i] == item2) data[dataset][i] = item1;
    }
   drawable_obj *obj = get_drawable();
   real_g (*map_grid)[3] = 0;
   int_g **map_data = 0;
   int_g n = 0;
   int_g mlen = 80;
   char message[80];
   structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			    get_data_length(), map_grid, map_data, n);
   obj->update_text_field(map_grid, map_data, get_labels(), get_ndata_sets(),
					       n, message, mlen);
    return true;
}

bool DLV::atom_integers::hide_render(const int_g object, const int_g visible,
				     const render_parent *parent,
				     class model *structure,
				     char message[], const int_g mlen)
{
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *d = get_drawable();
      real_g (*map_grid)[3] = 0;
      int_g **map_data = 0;
      int_g n = 0;
      structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			       get_data_length(), map_grid, map_data, n);
      d->update_text_field(map_grid, map_data, get_labels(), get_ndata_sets(),
			   n, message, mlen);
      for (int_g i = 0; i < get_ndata_sets(); i++)
	delete [] map_data[i];
      delete [] map_data;
      delete [] map_grid;
      return false;
    }
  }
  return true;
}

bool DLV::atom_reals::hide_render(const int_g object, const int_g visible,
				  const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen)
{
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *d = get_drawable();
      real_g (*map_grid)[3] = 0;
      real_g **map_data = 0;
      int_g n = 0;
      structure->map_atom_data(get_grid(), data, get_ndata_sets(),
			       get_data_length(), map_grid, map_data, n);
      d->update_text_field(map_grid, map_data, get_labels(), get_ndata_sets(),
			   n, message, mlen);
      for (int_g i = 0; i < get_ndata_sets(); i++)
	delete [] map_data[i];
      delete [] map_data;
      delete [] map_grid;
      return false;
    }
  }
  return true;
}

DLV::data_object *DLV::phonon_vectors::edit(const render_parent *parent,
					    class model *structure,
					    const int_g component,
					    const int_g method,
					    const int_g index,
					    char message[], const int_g mlen)
{
  message[0] = '\0';
  edit_atom_object *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  switch (method) {
  case 0:
    ptr = edit_phonon_trajectory::create(parent, this, structure, component,
					 name, index, nfrequencies,
					 message, mlen);
    // Todo - these need to be derived from data object we create.
    (void)ptr->update_phonon(parent, structure, 20, 0.2f, false, 0,
			     message, mlen);
    break;
  case 1:
    ptr = edit_atom_spectrum::create(parent, this, component,
				     name, index, message, mlen);
    // Todo - these need to be derived from data object we create.
    (void)ptr->update_spectrum(0, 0.0, 4000.0, 200, 1.0, 0.0, message, mlen);

    break;
  default:
    strncpy(message, "BUG: Unrecognised phonon edit method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::edit_atom_spectrum *
DLV::edit_atom_spectrum::create(const render_parent *parent,
				data_object *data, const int_g component,
				const string name, const int_g index,
				char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_spectrum(parent, index, message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit_atom_spectrum *edit = new edit_atom_spectrum(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit_atom_spectrum::is_displayable() const
{
  return true;
}

bool DLV::edit_atom_spectrum::is_editable() const
{
  return false;
}

DLV::string DLV::edit_atom_spectrum::get_data_label() const
{
  string name = "Phonon spectrum" + get_tags();
  return name;
}

DLV::string DLV::edit_atom_spectrum::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::edit_atom_spectrum::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit_atom_spectrum::get_sub_data_label(const int_g n) const
{
  return "";
}

DLV::int_g DLV::edit_atom_spectrum::get_display_type() const
{
  return (int)display_plot;
}

DLV::int_g DLV::edit_atom_spectrum::get_edit_type() const
{
  return (int)no_edit;
}

bool DLV::edit_atom_spectrum::get_sub_is_vector(const int_g n) const
{
  return false;
}

bool DLV::edit_atom_spectrum::hide_render(const int_g object,
					  const int_g visible,
					  const render_parent *parent,
					  class model *structure,
					  char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    set_drawable(get_edit_obj()->get_drawable());
    reattach_objects(parent);
    return false;
  }
  return true;
}

DLVreturn_type DLV::edit_atom_spectrum::render(const render_parent *parent,
					       class model *structure,
					       const int_g component,
					       const int_g method,
					       const int_g index,
					       char message[],
					       const int_g mlen)
{
  message[0] = '\0';
  // is this safe for reattaching if project loaded but wasn't drawn?
  reattach_objects(parent);
  drawable_obj *obj = get_edit_obj()->get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for spectrum", mlen);
    return DLV_ERROR;
  }
  set_drawable(obj);
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 0;  //dos_plot
  ptr = display_obj::create_dos_plot(parent, obj, name, index,
				     "Frequency (cm-1)", "Intensity", 1,
				     type, message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

void DLV::phonon_vectors::widen_lines(const int_g kp, real_g data[],
				      const int_g type, const real_g min_energy,
				      const real_g max_energy,
				      const real_g energy_step,
				      const real_g scale,
				      const int_g npoints)
/* Add the Lorentzian width to each line */
{
  const real_g tol = 0.0001f;
  const real_g cutoff = 10.0f; // cm-1
  for (int_g f = 0; f < nfrequencies; f++) {
    real_g delta_e = vectors[kp].phonons[f].frequency;
    if (delta_e > cutoff) {
      real_g intens;
      if (type == 0)
	intens = vectors[kp].phonons[f].ir_intensity;
      else
	intens = vectors[kp].phonons[f].raman_intensity;
      while (delta_e <= max_energy and intens > tol) {
	if (delta_e >= min_energy) {
	  //coeff = intens / (gamma * pi * (q * q - 1.0));
	  //real_g gamsq = gamma * gamma;
	  //real_g qgam = q * gamma;
	  real_g energy = min_energy - delta_e;
	  for (int_g k = 0; k < npoints; k++) {
	    if (std::abs(energy) < std::abs(energy + energy_step)) {
	      data[k] += intens;
	      break;
	    }
	    /* This is the intensity * fano(energy) of course. */
	    //data[k] += (coeff * (qgam + energy) * (qgam + energy) /
	    //		(gamsq + energy * energy));
	    energy += energy_step;
	  }
	}
	delta_e += vectors[kp].phonons[f].frequency;
	intens *= scale;
      }
    }
  }
}

void DLV::phonon_vectors::convolute(real_g data[], real_g temp[],
				    const real_g sigma, const int_g npoints)
/* Convolute all the Lorentzian widened lines with the Gaussian.
   Essentially unchanged from Theo's program */
{
  const real_g sqrt_two_pi = (real_g)std::sqrt(2.0 * pi);
  for (int_g i = 0; i < npoints; i++)
    temp[i] = 0.0;
  int_g iran = std::min(npoints - 1, (int)(10.0 * sigma));
  real_g sum = 0.0;
  for (int_g j = -iran; j <= iran; j++) {
    real_g f = std::exp(-((real_g)(j * j))/(2 * sigma * sigma)) /
      (sqrt_two_pi * sigma);
    sum += f;
    for (int_g i = std::max(1, 1 - j); i <= std::min(npoints, npoints - j); i++)
      temp[i - 1] += data[i + j - 1] * f;
  }
  for (int_g i = 0; i < npoints; i++)
    data[i] = temp[i] / sum;
}

bool DLV::phonon_vectors::generate_spectrum(real_g grid[], real_g data[],
					    const int_g type, const real_g emin,
					    const real_g emax, const int_g np,
					    const real_g w,
					    const real_g harmonic,
					    char message[], const int_g mlen)
{
  // find gamma point data
  const real_g tol = 0.0001f;
  int_g kp;
  for (kp = 0; kp < n_k_points; kp++)
    if (std::abs(vectors[kp].ka) < tol and std::abs(vectors[kp].kb) < tol and
	std::abs(vectors[kp].kc) < tol)
      break;
  if (kp == n_k_points) {
    strncpy(message, "Couldn't find gamma point data", mlen);
    return false;
  } else {
    // This comes from create_spectrum in spectra.c from my program (BGS)
    real_g energy_step = (emax - emin) / ((real_g)np);
    for (int_g i = 0; i < np; i++)
      data[i] = 0.0;
    // scan the frequencies and insert into the plot at 0 width
    widen_lines(kp, data, type, emin, emax, energy_step, harmonic, np);
    if (w > tol)
      convolute(data, grid, w / energy_step, np);
    for (int_g i = 0; i < np; i++)
      grid[i] = emin + (real_g)i * energy_step;
  }
  return true;
}

DLVreturn_type DLV::edit_atom_spectrum::update_spectrum(const int_g type,
							const real_g emin,
							const real_g emax,
							const int_g np,
							const real_g w,
							const real_g harmonic,
							char message[],
							const int_g mlen)
{
  set_values(type, emin, emax, np, w, harmonic);
  // This is currently safe.
  phonon_vectors *obj = static_cast<phonon_vectors *>(get_base_data());
  real_g *grid = new real_g[np];
  real_g *plot = new real_g[np];
  if (obj->generate_spectrum(grid, plot, type, emin, emax, np, w, harmonic,
			     message, mlen)) {
    edited_obj *eobj = get_edit_obj();
    string name;
    if (type == 0)
      name = "Infra-Red Spectrum";
    else
      name = "Raman Spectrum";
    bool ok = eobj->update_spectrum(grid, plot, np, name, message, mlen);
    delete [] plot;
    delete [] grid;
    if (ok)
      return DLV_OK;
    else
      return DLV_ERROR;
  } else
    return DLV_ERROR;
}

DLV::edit_phonon_trajectory *
DLV::edit_phonon_trajectory::create(const render_parent *parent,
				    data_object *data, model *structure,
				    const int_g component, const string name,
				    const int_g index, const int_g nfreq,
				    char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_phonon_traj(parent, index,
						   message, mlen);
  if (ptr == 0)
    return 0;
  else {
    int_g kp = component / nfreq;
    int_g fp = component % nfreq;
    edit_phonon_trajectory *edit =
      new edit_phonon_trajectory(data, ptr, index, name,
				 component, kp, fp, structure);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit_phonon_trajectory::is_displayable() const
{
  return true;
}

bool DLV::edit_phonon_trajectory::is_editable() const
{
  return false;
}

DLV::string DLV::edit_phonon_trajectory::get_data_label() const
{
  string name = "Phonon trajectory" + get_tags() + " ";
  name += get_base_data()->get_sub_data_label(component);
  return name;
}

DLV::string DLV::edit_phonon_trajectory::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::edit_phonon_trajectory::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit_phonon_trajectory::get_sub_data_label(const int_g n) const
{
  return "";
}

DLV::int_g DLV::edit_phonon_trajectory::get_display_type() const
{
  return (int)display_atoms;
}

DLV::int_g DLV::edit_phonon_trajectory::get_edit_type() const
{
  return (int)no_edit;
}

bool DLV::edit_phonon_trajectory::get_sub_is_vector(const int_g n) const
{
  return false;
}

DLVreturn_type
DLV::edit_phonon_trajectory::render(const render_parent *parent,
				    class model *structure,
				    const int_g component, const int_g method,
				    const int_g index, char message[],
				    const int_g mlen)
{
  message[0] = '\0';
  //drawable_obj *obj = get_drawable();
  //if (obj == 0) {
  //   strncpy(message, "Missing object for phonon trajectory", mlen);
  //  return DLV_ERROR;
  //}
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_phonon_anim(parent, 0, name, index,
					message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
    structure->set_atom_data(parent, this);
  }
  return DLV_OK;
}

bool DLV::phonon_vectors::generate_frames(std::vector<real_g (*)[3]> &map_data,
					  const class model *structure,
					  const int_g component,
					  const int_g nframes,
					  const real_g scale,
					  const bool use_temp,
					  const real_g temp,
					  char message[], const int_g mlen)
{
  int_g kp = component / nfrequencies;
  int_g fp = component % nfrequencies;
  const real_g cutoff = 10.0;
  int_g n = 0;
  // Find largest vector and set scale so it equals amp.
  real_g vlen, maxv = 0.0;
  for (int_g i = 0; i < natoms; i++) {
    vlen =
      vectors[kp].phonons[fp].mags[i][0] * vectors[kp].phonons[fp].mags[i][0]
      + vectors[kp].phonons[fp].mags[i][1] * vectors[kp].phonons[fp].mags[i][1]
      + vectors[kp].phonons[fp].mags[i][2]*vectors[kp].phonons[fp].mags[i][2];
    if (vlen > maxv)
      maxv = vlen;
  }
  real_g s = scale / std::sqrt(maxv);
  if (use_temp) {
    // scale number of frames.
    real_g max_freq = (real_g)(temp / inv_cm_to_K); // Todo - improve?
    real_g min_freq = 10000.0f;
    for (int_g i = 0; i < nfrequencies; i++) {
      if (vectors[kp].phonons[i].frequency >= cutoff and
	  (vectors[kp].phonons[i].frequency * inv_cm_to_K) <= temp) {
	//if (max_freq < vectors[kp].phonons[i].frequency)
	//  max_freq = vectors[kp].phonons[i].frequency;
	if (min_freq > vectors[kp].phonons[i].frequency)
	  min_freq = vectors[kp].phonons[i].frequency;
      }
    }
    // use nframes for the max_frequency -> ~ max / min frames for min_freq
    // Todo - smarter attempt to scale
    // Todo - should I have some sort of Botlzmann weighting?
    int_g max_copies = (int) round((real_l)(max_freq / min_freq));
    int_g totframes = max_copies * nframes;
    for (int_g i = 0; i < nfrequencies; i++) {
      real_g energy = (real_g)(vectors[kp].phonons[i].frequency * inv_cm_to_K);
      if (vectors[kp].phonons[i].frequency >= cutoff and energy <= temp) {
	int_g harmonics = (int) (temp / energy);
	if (harmonics < 1)
	  harmonics = 1;
	for (int_g j = 1; j <= harmonics; j++) { 
	  int_g count =
	    (int) round(real_l(j*vectors[kp].phonons[i].frequency / min_freq));
	  // Todo should s be based on the largest of all vectors, not fp?
	  structure->map_atom_data(coords, vectors[kp].phonons[i].mags,
				   vectors[kp].phonons[i].phases, natoms,
				   map_data, n, totframes, count,
				   vectors[kp].ka, vectors[kp].kb,
				   vectors[kp].kc, s);
	}
      }
    }
  } else    
    structure->map_atom_data(coords, vectors[kp].phonons[fp].mags,
			     vectors[kp].phonons[fp].phases, natoms,
			     map_data, n, nframes, 1, vectors[kp].ka,
			     vectors[kp].kb, vectors[kp].kc, s);
  return true;
}

DLVreturn_type
DLV::edit_phonon_trajectory::update_phonon(const render_parent *parent,
					   model *structure,
					   const int_g nframes,
					   const real_g scale,
					   const bool use_temp,
					   const real_g temp,
					   char message[], const int_g mlen)
{
  phonon_vectors *obj = static_cast<phonon_vectors *>(get_base_data());
  if (vectors.size() != 0) {
    for (int_g i = 0; i < number_of_frames; i++)
      delete [] vectors[i];
  }
  if (obj->generate_frames(vectors, geom, component, nframes, scale,
			   use_temp, temp, message, mlen)) {
    number_of_frames = nframes;
    magnitude = scale;
    use_temperature = use_temp;
    temperature = temp;
    structure->refresh_atom_data(parent);
    return DLV_OK;
  } else
    return DLV_ERROR;
}

DLV::int_g DLV::md_trajectory::get_nframes() const
{
  return nsteps;
}

DLV::int_g DLV::edit_phonon_trajectory::get_nframes() const
{
  return number_of_frames;
}

const std::vector<DLV::real_g (*)[3]> *
DLV::md_trajectory::get_trajectory() const
{
  return &vectors;
}

const std::vector<DLV::real_g (*)[3]> *
DLV::edit_phonon_trajectory::get_trajectory() const
{
  return &vectors;
}

bool DLV::phonon_vectors::test_gamma_point(const int_g kp) const
{
  const real_g tol = 0.0001f;
  return (std::abs(vectors[kp].ka) < tol and std::abs(vectors[kp].kb) < tol and
	  std::abs(vectors[kp].kc) < tol);
}

void DLV::phonon_vectors::get_kpoint_cell(int_g kc[3], const int_g kp) const
{
  kc[0] = 1;
  kc[1] = 1;
  kc[2] = 1;
  const int_g max_cell = 10;
  const real_l tol = 0.005;
  for (int_g j = 1; j <= max_cell; j++) {
    real_l sc = (real_l)j * vectors[kp].ka;
    if (std::abs(sc - round(sc)) < tol) {
      kc[0] = j;
      break;
    }
  }
  for (int_g j = 1; j <= max_cell; j++) {
    real_l sc = (real_l)j * vectors[kp].kb;
    if (std::abs(sc - round(sc)) < tol) {
      kc[1] = j;
      break;
    }
  }
  for (int_g j = 1; j <= max_cell; j++) {
    real_l sc = (real_l)j * vectors[kp].kc;
    if (std::abs(sc - round(sc)) < tol) {
      kc[2] = j;
      break;
    }
  }
  // Todo - improve error conditions
}

bool DLV::phonon_vectors::hide_render(const int_g object, const int_g visible,
				      const render_parent *parent,
				      class model *structure,
				      char message[], const int_g mlen)
{
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      // kind of a copy of render here - Todo?
      drawable_obj *obj = get_drawable();
      real_g (*map_grid)[3] = 0;
      real_g (*map_data)[3] = 0;
      int_g total = n_k_points * nfrequencies;
      // Todo - do we want to split top level across kpoints
      for (int_g kp = 0; kp < n_k_points; kp++) {
	// Todo - more efficient loops over atoms
	for (int_g fp = 0; fp < nfrequencies; fp++) {
	  int_g n = 0;
	  structure->map_atom_data(coords, vectors[kp].phonons[fp].mags,
				   vectors[kp].phonons[fp].phases, natoms,
				   map_grid, map_data, n, vectors[kp].ka,
				   vectors[kp].kb, vectors[kp].kc);
	  total = nfrequencies * kp + fp;
	  obj->update_vector_field(map_grid, map_data, n,
				   total, message, mlen);
	}
	delete [] map_data;
	delete [] map_grid;
      }
      return false;
    }
  }
  return true;
}

bool DLV::md_trajectory::is_gamma_point() const
{
  return true;
}

bool DLV::edit_phonon_trajectory::is_gamma_point() const
{
  phonon_vectors *obj = static_cast<phonon_vectors *>(get_base_data());
  return obj->test_gamma_point(kpoint);
}

void DLV::edit_phonon_trajectory::get_kpoint_cell(int_g kc[3]) const
{
  phonon_vectors *obj = static_cast<phonon_vectors *>(get_base_data());
  return obj->get_kpoint_cell(kc, kpoint);
}

void DLV::md_trajectory::update_trajectory(model *structure)
{
  structure->map_atom_data(atom_coords, natoms, trajectory, nsteps, vectors);
}

void DLV::edit_phonon_trajectory::update_trajectory(model *structure)
{
  // a lot like update_phonon
  phonon_vectors *obj = static_cast<phonon_vectors *>(get_base_data());
  if (vectors.size() != 0) {
    for (int_g i = 0; i < number_of_frames; i++)
      delete [] vectors[i];
  }
  char message[256];
  (void) obj->generate_frames(vectors, structure, component, number_of_frames,
			      magnitude, use_temperature, temperature,
			      message, 256);
}

bool DLV::md_trajectory::hide_render(const int_g object, const int_g visible,
				     const render_parent *parent,
				     class model *structure,
				     char message[], const int_g mlen)
{
  if (visible == 0)
    structure->set_atom_data(parent, 0, false);
  else {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      update_trajectory(structure);
      structure->set_atom_data(parent, this);
      return false;
    }
    structure->set_atom_data(parent, this);
  }
  return true;
}

bool DLV::edit_phonon_trajectory::hide_render(const int_g object,
					      const int_g visible,
					      const render_parent *parent,
					      class model *structure,
					      char message[], const int_g mlen)
{
  if (visible == 0)
    structure->set_atom_data(parent, 0, false);
  else
    structure->set_atom_data(parent, this);
  return true;
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_integers *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_integers("recover", "temp", 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_reals *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_reals("recover", "temp", 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::phonon_vectors *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::phonon_vectors("recover", "temp", 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::vectors_and_intensity *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::vectors_and_intensity("recover", "temp", 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::md_trajectory *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::md_trajectory("recover", "temp", 0, 0, 0.0);
    }
  }
}

template <class Archive>
void DLV::atom_based_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
}

template <class Archive>
void DLV::atom_scalars::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & name;
  ar & natoms;
  ar & n_data_sets;
  for (int i = 0; i < natoms; i++)
    ar & coords[i];
  for (int i = 0; i < n_data_sets; i++)
    ar & labels[i];
}

template <class Archive>
void DLV::atom_scalars::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & name;
  ar & natoms;
  ar & n_data_sets;
  size = n_data_sets;
  coords = new float[natoms][3];
  for (int i = 0; i < natoms; i++)
    ar & coords[i];
  labels = new string[size];
  for (int i = 0; i < n_data_sets; i++)
    ar & labels[i];
}

template <class Archive>
void DLV::atom_integers::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<atom_scalars>(*this);
  for (int i = 0; i < get_ndata_sets(); i++)
    for (int j = 0; j < get_data_length(); j++)
      ar & data[i][j];
}

template <class Archive>
void DLV::atom_integers::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_scalars>(*this);
  int nd = get_ndata_sets();
  data = new int *[nd];
  int na = get_data_length();
  for (int i = 0; i < nd; i++) {
    data[i] = new int[na];
    for (int j = 0; j < na; j++)
      ar & data[i][j];
  }
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::atom_reals::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<atom_scalars>(*this);
  for (int i = 0; i < get_ndata_sets(); i++)
    for (int j = 0; j < get_data_length(); j++)
      ar & data[i][j];
}

template <class Archive>
void DLV::atom_reals::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_scalars>(*this);
  int nd = get_ndata_sets();
  data = new float *[nd];
  int na = get_data_length();
  for (int i = 0; i < nd; i++) {
    data[i] = new float[na];
    for (int j = 0; j < na; j++)
      ar & data[i][j];
  }
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::phonon_frequency::save(Archive &ar, const unsigned int version) const
{
  ar & natoms;
  ar & frequency;
  ar & ir_intensity;
  ar & raman_intensity;
  ar & symbol;
  for (int_g i = 0; i < natoms; i++)
    ar & mags[i];
  bool has_phase = (phases != 0);
  ar & has_phase;
  if (has_phase) {
    for (int_g i = 0; i < natoms; i++)
      ar & phases[i];
  }
}

template <class Archive>
void DLV::phonon_frequency::load(Archive &ar, const unsigned int version)
{
  ar & natoms;
  ar & frequency;
  ar & ir_intensity;
  ar & raman_intensity;
  ar & symbol;
  mags = new real_g[natoms][3];
  for (int_g i = 0; i < natoms; i++)
    ar & mags[i];
  bool has_phase;
  ar & has_phase;
  if (has_phase) {
    phases = new real_g[natoms][3];
    for (int_g i = 0; i < natoms; i++)
      ar & phases[i];
  } else
    phases = 0;
}

template <class Archive>
void DLV::phonon_kpoint::save(Archive &ar, const unsigned int version) const
{
  ar & nfrequencies;
  ar & ka;
  ar & kb;
  ar & kc;
  ar & has_phases;
  for (int_g i = 0; i < nfrequencies; i++)
    ar & phonons[i];
}

template <class Archive>
void DLV::phonon_kpoint::load(Archive &ar, const unsigned int version)
{
  ar & nfrequencies;
  ar & ka;
  ar & kb;
  ar & kc;
  ar & has_phases;
  phonons = new phonon_frequency[nfrequencies];
  for (int_g i = 0; i < nfrequencies; i++)
    ar & phonons[i];
}

template <class Archive>
void DLV::phonon_vectors::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & nfrequencies;
  ar & n_k_points;
  ar & natoms;
  for (int_g i = 0; i < natoms; i++)
    ar & coords[i];
  for (int_g i = 0; i < n_k_points; i++)
    ar & vectors[i];
}

template <class Archive>
void DLV::phonon_vectors::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & nfrequencies;
  ar & n_k_points;
  ar & natoms;
  coords = new real_g[natoms][3];
  for (int_g i = 0; i < natoms; i++)
    ar & coords[i];
  vectors = new phonon_kpoint[n_k_points];
  for (int_g i = 0; i < n_k_points; i++)
    ar & vectors[i];
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
  // Todo - should I reload rather than saving phonons?
}

template <class Archive>
void DLV::vectors_and_intensity::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<phonon_vectors>(*this);
}

template <class Archive>
void DLV::md_trajectory::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & nsteps;
  ar & time_step;
  ar & natoms;
}

template <class Archive>
void DLV::md_trajectory::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<atom_based_data>(*this);
  ar & nsteps;
  ar & time_step;
  ar & natoms;
  trajectory = std::vector<real_g (*)[3]>(nsteps);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_integers)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_reals)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::phonon_vectors)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::vectors_and_intensity)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::md_trajectory)

DLV_SUPPRESS_TEMPLATES(DLV::reloadable_data)

DLV_SPLIT_EXPLICIT(DLV::atom_integers)
DLV_SPLIT_EXPLICIT(DLV::atom_reals)
DLV_SPLIT_EXPLICIT(DLV::phonon_vectors)
DLV_NORMAL_EXPLICIT(DLV::vectors_and_intensity)
DLV_SPLIT_EXPLICIT(DLV::md_trajectory)

#  ifdef ENABLE_DLV_GRAPHICS

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit_phonon_trajectory *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit_phonon_trajectory(0, 0, 0, "", 0, 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit_atom_spectrum *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit_atom_spectrum(0, 0, 0, "");
    }

  }
}

template <class Archive>
void DLV::edit_atom_object::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_object>(*this);
}

template <class Archive>
void DLV::edit_phonon_trajectory::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_atom_object>(*this);
  ar & component;
  ar & kpoint;
  ar & fpoint;
  ar & number_of_frames;
  ar & magnitude;
  ar & use_temperature;
  ar & temperature;
  ar & geom;
  char message[128];
  (void) update_phonon(render_parent::get_serialize_obj(),
		       geom, number_of_frames, magnitude,
		       use_temperature, temperature, message, 128);
  reattach_objects();
}

template <class Archive>
void DLV::edit_phonon_trajectory::save(Archive &ar,
				       const unsigned int version) const
{
  ar & boost::serialization::base_object<edit_atom_object>(*this);
  ar & component;
  ar & kpoint;
  ar & fpoint;
  ar & number_of_frames;
  ar & magnitude;
  ar & use_temperature;
  ar & temperature;
  ar & geom;
}

template <class Archive>
void DLV::edit_atom_spectrum::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_atom_object>(*this);
  ar & spectra_type;
  ar & min_energy;
  ar & max_energy;
  ar & npoints;
  ar & width;
  ar & harmonic_scale;
  char message[128];
  (void)update_spectrum(spectra_type, min_energy, max_energy, npoints,
			width, harmonic_scale, message, 128);
}

template <class Archive> void
DLV::edit_atom_spectrum::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edit_atom_object>(*this);
  ar & spectra_type;
  ar & min_energy;
  ar & max_energy;
  ar & npoints;
  ar & width;
  ar & harmonic_scale;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit_atom_spectrum)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit_phonon_trajectory)

DLV_SUPPRESS_TEMPLATES(DLV::edit_object)

DLV_SPLIT_EXPLICIT(DLV::edit_atom_spectrum)
DLV_SPLIT_EXPLICIT(DLV::edit_phonon_trajectory)

#  endif //ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
