
#ifndef DLV_DATA_OBJECT
#define DLV_DATA_OBJECT

#include <list>
#include <vector>

namespace DLV {

  enum display_info_type {
    display_text = 0, display_point = 1, display_atoms = 2,
    display_line = 3, display_plot = 4, display_plane = 5,
    display_2D = 6, display_2Dunstr = 7, display_3D = 8, display_3Dvr = 9,
    display_atoms_bonds = 10, display_direction = 11, display_leed = 12,
    display_real = 13, display_file = 14, display_box = 15
  };

  enum edit_info_type {
    no_edit = 0, edit_2D = 1, edit_3D = 2, edit_phonons = 3, edit_intens = 4,
    edit_energy = 5
  };

  // Todo where should this go?
  class wulff_data {
  public:
    int_g h;
    int_g k;
    int_g l;
    real_g energy;
    real_g red;
    real_g green;
    real_g blue;
    int_g termination;
    bool conventional;
#ifdef DLV_USES_SERIALIZE
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class data_object {
  public:
    virtual ~data_object();

    virtual string get_obj_label() const;
    virtual bool is_displayable() const = 0;
    virtual bool is_editable() const = 0;
    virtual bool is_edited() const;
    virtual string get_data_label() const = 0;
    virtual string get_edit_label() const = 0;
    virtual int_g get_number_data_sets() const = 0;
    virtual string get_sub_data_label(const int_g n) const = 0;
    virtual int_g get_display_type() const = 0;
    virtual int_g get_edit_type() const = 0;
    virtual bool get_sub_is_vector(const int_g n) const = 0;
    virtual bool is_atom_bond_data() const;
    bool match_type(const string type, const string program) const;
    virtual string get_name() const;
    string get_source() const;
    string get_program_name() const;

    virtual bool is_kspace() const;
    virtual bool is_periodic() const;

    virtual bool is_line() const;
    virtual bool is_plane() const;
    virtual bool is_box() const;
    virtual bool is_wulff() const;
    virtual bool is_real_scalar() const;
    virtual bool is_lattice_direction() const;

    virtual bool get_real_value(real_l &v) const;
    virtual const std::list<wulff_data> &get_wulff_data() const;

    virtual void unload_data() = 0;

#ifdef ENABLE_DLV_GRAPHICS
    // Todo - protect (problem with update_slice)
    class drawable_obj *get_drawable();
    virtual class drawable_obj *
    create_drawable(const render_parent *parent,
		    char message[], const int_g mlen);

    virtual bool hide_render(const int_g object, const int_g visible,
			     const render_parent *parent,
			     class model *structure,
			     char message[], const int_g mlen);
    virtual DLVreturn_type render(const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen) = 0;
    virtual DLVreturn_type transfer_and_render(class data_object *old_data,
					       const render_parent *parent,
					       char message[],
					       const int_g mlen);
    int_g count_display_objects() const;
    void list_display_objects(const render_parent *parent) const;
    bool check_display_index(const int_g id) const;
    bool show_panel() const;
    virtual void list_edit_object(const render_parent *parent) const;
    virtual bool check_edit_index(const int_g id) const;
    bool object_is_edited() const;

    virtual void update_streamlines(const int_g object, data_object *plane,
				    const render_parent *parent,
				    char message[], const int_g mlen);
    virtual void update_trajectory(class model *structure);
    virtual int_g get_nframes() const;
    virtual const std::vector<real_g (*)[3]> *get_trajectory() const;
    virtual bool is_gamma_point() const;
    virtual void get_kpoint_cell(int_g kc[3]) const;

    virtual DLVreturn_type render(const render_parent *parent,
				  class model *structure,
				  const int_g component, const int_g method,
				  const int_g index, char message[],
				  const int_g mlen) = 0;
    virtual DLVreturn_type select(char message[], const int_g mlen);
    DLVreturn_type select(const int_g object, char message[], const int_g mlen);
    DLVreturn_type remove(const int_g object, char message[], const int_g mlen);
    DLVreturn_type remove_display(const render_parent *parent,
				  class model *structure, int_g &first,
				  int_g &last, char message[],
				  const int_g mlen);
    void delete_drawable();

    void add_edit(class edited_obj *obj);
    virtual data_object *edit(const render_parent *parent,
			      class model *structure, const int_g component,
			      const int_g method, const int_g index,
			      char message[], const int_g mlen);

    virtual void attach_updatable();
    virtual bool update_atom_selection_info(const class model *m,
					    char message[], const int_g mlen);
    virtual bool update_cell_info(class model *m,
				  char message[], const int_g mlen);
    virtual DLVreturn_type use_view_editor(const render_parent *parent,
					   const bool v, char message[],
					   const int_g mlen);
    virtual DLVreturn_type update3D(const int_g method, const int_g h,
				    const int_g k, const int_g l,
				    const real_g x, const real_g y,
				    const real_g z, const int_g object,
				    const bool conv, const bool frac,
				    const bool p_obj, class model *const m,
				    char message[], const int_g mlen);
    virtual DLVreturn_type update3D(const int_g method, const real_g r,
				    class model *const m, char message[],
				    const int_g mlen);
    virtual DLVreturn_type update2D(const int_g method, const int_g h,
				    const int_g k, const int_g l,
				    const real_g x, const real_g y,
				    const real_g z, const int_g obj,
				    const bool conv, const bool frac,
				    const bool parent, class model *const m,
				    char message[], const int_g mlen);
    virtual DLVreturn_type update1D(const int_g method, const int_g h,
				    const int_g k, const int_g l,
				    const real_g x, const real_g y,
				    const real_g z, const int_g obj,
				    const bool conv, const bool frac,
				    const bool parent, class model *const m,
				    char message[], const int_g mlen);
    virtual DLVreturn_type update0D(const int_g method, const real_g x,
				    const real_g y, const real_g z,
				    const bool conv, model *const m,
				    char message[], const int_g mlen);
    virtual DLVreturn_type update_wulff(const int_g h, const int_g k,
					const int_g l, const real_g e,
					const real_g r, const real_g g,
					const real_g b, const bool conv,
					model *const m, char message[],
					const int_g mlen);
    virtual DLVreturn_type update_wulff(const char filename[], model *const m,
					char message[], const int_g mlen);
    virtual DLVreturn_type update_leed(const bool d, model *const m,
				       char message[], const int_g mlen);
    virtual DLVreturn_type update_spectrum(const int_g type, const real_g emin,
					   const real_g emax, const int_g np,
					   const real_g w,
					   const real_g harmonic,
					   char message[], const int_g mlen);
    virtual DLVreturn_type update_phonon(const render_parent *parent,
					 class model *structure,
					 const int_g nframes,
					 const real_g scale,
					 const bool use_temp,
					 const real_g temperature,
					 char message[], const int_g mlen);
    virtual DLVreturn_type update_slice(const render_parent *parent,
					class model *structure,
					data_object *plane,
					char message[], const int_g mlen);
    virtual DLVreturn_type update_cut(const render_parent *parent,
				      class model *structure,
				      data_object *plane,
				      char message[], const int_g mlen);
    virtual DLVreturn_type update_extension(const render_parent *parent,
					    const int_g na, const int_g nb,
					    const int_g nc, char message[],
					    const int_g mlen);
    virtual DLVreturn_type update_data_math(const render_parent *parent,
					    data_object *obj, const int_g cmpt,
					    const int_g index, char message[],
					    const int_g mlen);
    virtual DLVreturn_type update_shift(const real_g shift, char message[],
					const int_g mlen);
    virtual void undisplay() const;

    virtual void data_math_parent(const data_object * &obj,
				  int_g &component) const;
    virtual void match_data_math(const data_object *obj, const int_g selection,
				 const bool all, const int_g cmpt,
				 int_g &nmatch, int_g &n) const;
    virtual void list_data_math(const data_object *obj, const bool all,
				const int_g cmpt, string *names,
				int_g &n) const;
    void reattach_objects(const render_parent *parent = 0);

#endif // ENABLE_DLV_GRAPHICS

  protected:
    data_object(const string code, const string src, const bool show = true);
     
    string get_tags() const;
#ifdef ENABLE_DLV_GRAPHICS
    void set_drawable(class drawable_obj *obj);
    void add_display_obj(class display_obj *obj);
    display_obj *remove_display_obj();
    void set_panel(const bool value);
    bool object_is_displayed() const;
    class display_obj *get_display_obj();
#endif // ENABLE_DLV_GRAPHICS

  private:
    string program_name;
    string source;
#ifdef ENABLE_DLV_GRAPHICS
    class drawable_obj *draw;
    std::list<class display_obj *> display;
    std::list<class edited_obj *> modified;
    bool display_panel;
#endif // ENABLE_DLV_GRAPHICS

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class text_buffer : public data_object {
  public:
    text_buffer(const string code, const string src, const string name);

    void unload_data();

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    void expand_lines(const int_g n);
    void add_line(const char line[], const bool no_eol);
#endif // ENABLE_DLV_GRAPHICS

  private:
    string label;
    bool pending_eol;
    int_g nlines;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_and_bond_data : public data_object {
  public:
    atom_and_bond_data();

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    bool is_atom_bond_data() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
    bool update_atom_selection_info(const class model *m, char message[],
				    const int_g mlen);
    bool update_cell_info(class model *m, char message[], const int_g mlen);
    void attach_updatable();
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class lattice_direction_data : public data_object {
  public:
    lattice_direction_data();

    bool is_displayable() const;
    bool is_editable() const;
    bool is_lattice_direction() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
    bool update_atom_selection_info(const class model *m, char message[],
				    const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
#ifdef ENABLE_DLV_GRAPHICS
    bool get_lattice_info(string &text, const model *m, char message[],
			  const int_g mlen);
    static bool set_vector_info(const coord_type lattice[3][3],
				const coord_type p1[3], const coord_type p2[3],
				const int_g dim, string &text, const bool bulk);
    static bool set_plane_info(const coord_type lattice[3][3],
			       const coord_type p1[3], const coord_type p2[3],
			       const coord_type p3[3], const int_g dim,
			       string &text, const bool bulk);
    static void miller_to_integer(const coord_type fh, const coord_type fk,
				  const coord_type fl, int_g &h, int_g &k,
				  int_g &l, const int_g dim, bool &failed);
#endif // ENABLE_DLV_GRAPHICS

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::data_object)
BOOST_CLASS_EXPORT_KEY(DLV::text_buffer)
BOOST_CLASS_EXPORT_KEY(DLV::atom_and_bond_data)
BOOST_CLASS_EXPORT_KEY(DLV::lattice_direction_data)
#endif // DLV_USES_SERIALIZE

inline DLV::data_object::data_object(const string code, const string src,
				     const bool show)
  : program_name(code), source(src)
#ifdef ENABLE_DLV_GRAPHICS
  , draw(0), display_panel(show)
#endif // ENABLE_DLV_GRAPHICS
{
}

inline DLV::string DLV::data_object::get_source() const
{
  return source;
}

inline DLV::string DLV::data_object::get_tags() const
{
  return " (" + program_name + " - " + source + ")";
}

inline DLV::text_buffer::text_buffer(const string code, const string src,
				     const string name)
  : data_object(code, src), label(name), pending_eol(false), nlines(0)
{
}

inline DLV::atom_and_bond_data::atom_and_bond_data()
  : data_object("DLV", "user", false)
{
}

inline DLV::lattice_direction_data::lattice_direction_data()
  : data_object("DLV", "user", false)
{
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::int_g DLV::data_object::count_display_objects() const
{
  return (int)display.size();
}

inline void DLV::data_object::set_panel(const bool value)
{
  display_panel = value;
}

inline bool DLV::data_object::show_panel() const
{
  return display_panel;
}

inline bool DLV::data_object::object_is_displayed() const
{
  return (display.size() > 0);
}

inline bool DLV::data_object::object_is_edited() const
{
  return (modified.size() > 0);
}

inline class DLV::display_obj *DLV::data_object::get_display_obj()
{
  if (display.size() > 0)
    return display.front();
  else
    return 0;
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_DATA_OBJECT
