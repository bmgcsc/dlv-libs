
#include <cmath>
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "constants.hxx"
#include "math_fns.hxx"
#include "data_objs.hxx"
#include "model.hxx"
#include "data_simple.hxx"
#include "data_plots.hxx"
// for update in line
#include "utils.hxx"
#include "operation.hxx" //clb

DLV::plot_data::~plot_data()
{
  for (int_g i = 0; i < ngraphs; i++)
    delete [] plots[i];
  delete [] plots;
  delete [] labels;
  delete [] grid;
  delete [] xpoints;
  delete [] xlabels;
}

DLV::plot_multi_grids::~plot_multi_grids()
{
  for (int_g i = 0; i < ngrids; i++)
    delete [] multi_grids[i];
  delete [] multi_grids;
  delete [] grid_sizes;
  delete [] select_grid;
}

DLV::rod1d_plot::~rod1d_plot()
{
  // delete [] grid2;
}

DLV::panel_plot::~panel_plot()
{
  delete [] ypoints;
  delete [] ylabels;
}

bool DLV::line::is_line() const
{
  return true;
}

DLV::string DLV::line::get_name() const
{
  return name;
}

bool DLV::line::is_displayable() const
{
  return true;
}

bool DLV::line::is_editable() const
{
  return false;
}

bool DLV::line::is_edited() const
{
  return false;
}

DLV::string DLV::line::get_data_label() const
{
  return (name + " - line");
}

DLV::string DLV::line::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::line::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::line::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::line::get_display_type() const
{
  return display_line;
}

DLV::int_g DLV::line::get_edit_type() const
{
  return no_edit;
}

bool DLV::line::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::k_space_line::is_kspace() const
{
  return true;
}

void DLV::line::unload_data()
{
  // Do nothing
}

void DLV::plot_data::set_grid(real_g *data, const int_g n, const bool copy_data)
{
  npoints = n;
  if (copy_data) {
    grid = new real_g[n];
    for (int_g i = 0; i < n; i++)
        grid[i] = data[i];
  } else
    grid = data;
  // Assign the limits.
  xrange[0] = grid[0];
  xrange[1] = grid[npoints - 1];
}

void DLV::plot_data::add_plot(real_g *points, const int_g n,
			      const bool copy_data)
{
  if (plots == 0) {
    // Todo - indices?
    plots = new real_g *[ngraphs];
    labels = new string[ngraphs];
  }
  if (copy_data) {
    plots[n] = new real_g[npoints];
    for (int_g i = 0; i < npoints; i++)
      plots[n][i] = points[i];
  } else
    plots[n] = points;
  // Update the limits.
  for (int_g i = 0; i < npoints; i++) {
    if (points[i] < yrange[0])
      yrange[0] = points[i];
    if (points[i] > yrange[1])
      yrange[1] = points[i];
  }
}

void DLV::plot_data::add_plot(real_g *points, const int_g n, const char label[],
			      const bool copy_data)
{
  add_plot(points, n, copy_data);
  set_plot_label(label, n);
}

void DLV::plot_data::update_plot(real_g *points, const int_g n)
{
  if (plots != 0 && n < ngraphs){
    for (int_g i = 0; i < npoints; i++)
      plots[n][i] = points[i];
    // Update the limits.
    for (int_g i = 0; i < npoints; i++) {
      if (points[i] < yrange[0])
	yrange[0] = points[i];
      if (points[i] > yrange[1])
	yrange[1] = points[i];
    }
  }
}

void DLV::plot_data::set_plot_label(const char label[], const int_g index)
{
  if (plots == 0) {
    plots = new real_g *[ngraphs];
    labels = new string[ngraphs];
  }
  labels[index] = label;
}

void DLV::plot_data::set_xaxis_label(const char label[])
{
  x_axis_label = label;
}

void DLV::plot_data::set_xunits(const char label[])
{
  // Todo
}

void DLV::plot_data::set_yaxis_label(const char label[])
{
  y_axis_label = label;
}

void DLV::plot_data::set_yunits(const char label[])
{
  // Todo
}

void DLV::plot_data::set_title(const char label[])
{
  title = label;
}

DLV::string DLV::plot_data::get_data_label() const
{
  string name = title;
  if (name.length() == 0)
    name = "Plot";
  name += get_tags();
  return name;
}

bool DLV::plot_data::is_displayable() const
{
  return true;
}

bool DLV::plot_data::is_editable() const
{
  // Todo - treat plot select as 'edit' ?
  return false;
}

DLV::string DLV::plot_data::get_edit_label() const
{
  return get_data_label();
  //string name = "BUG: edit plot";
  //return name;
}

DLV::int_g DLV::plot_data::get_display_type() const
{
  return (int)display_plot;
}

DLV::int_g DLV::plot_data::get_edit_type() const
{
  return (int)no_edit;
}

bool DLV::plot_data::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::plot_data::set_xpoints(real_g *points, const int_g np,
				 const bool copy_data)
{
  num_xpoints = np;
  xlabels = new string[np];
  if (copy_data) {
    xpoints = new real_g[np];
    for (int_g i = 0; i < np; i++)
      xpoints[i] = points[i];
  } else
    xpoints = points;
}

void DLV::plot_data::set_xlabel(const char label[], const int_g index)
{
  if (index >= 0 and index < num_xpoints)
    xlabels[index] = label;
}

bool DLV::plot_multi_grids::add_plot(real_g *points, const int_g n, 
				     const int_g ng, const bool copy_data)
{
  plot_data::add_plot(points, n, copy_data);
  int_g ngraphs = get_ngraphs();
  if (select_grid == 0)
    select_grid = new int_g[ngraphs]; 
  if(n >= ngraphs)
    return false;
  if(ng >= ngrids)
    return false;
  select_grid[n] = ng;
  return true;
}

void DLV::plot_multi_grids::add_plot(real_g *points, const int_g n,
				     const int_g ng, const char label[],
				     const bool copy_data)
{
  add_plot(points, n, ng, copy_data);
  set_plot_label(label, n);
}

bool DLV::plot_multi_grids::add_grid(real_g *data, const int_g npts, 
					const int_g ng)
{
  if(ng >=  ngrids)
    return false;
  int_g npoints = get_npoints();
  if(npts >  npoints)
    return false;
  if (multi_grids == 0)
    multi_grids = new real_g *[ngrids];
  multi_grids[ng] = new real_g[npoints];
  for (int_g i = 0; i < npts; i++)
    multi_grids[ng][i] = data[i];
  // Assign grid size
  if (grid_sizes == 0)
    grid_sizes = new int_g[ngrids]; 
  grid_sizes[ng] = npts; 
  // Assign the limits.
  real_g *xrange = new real_g[2];
  const real_g *xr = get_xrange();
  xrange[0] = xr[0];
  xrange[1] = xr[1];
  if(xrange[0] > multi_grids[ng][0])
    xrange[0] = multi_grids[ng][0];
  if(xrange[1] < multi_grids[ng][npoints - 1])
    xrange[1] = multi_grids[ng][npoints - 1];
  set_xrange(xrange);
  return true;
}

void DLV::plot_multi_grids::set_grid(real_g *data, const int_g n,
				     const bool copy_data)
{
  std::cout << "ERROR SHOULD CALL ADD GRID - CAN WE DO THIS HERE???\n";
}



DLV::int_g DLV::plot_multi_grids::get_number_data_sets() const
{
  return get_ngraphs();
}

DLV::string DLV::plot_multi_grids::get_sub_data_label(const int_g n) const
{
  return get_labels()[n];
}

void DLV::panel_plot::set_ypoints(real_g *points, const int_g np,
				  const bool copy_data)
{
  num_ypoints = np;
  ylabels = new string[np];
  if (copy_data) {
    ypoints = new real_g[np];
    for (int_g i = 0; i < np; i++)
      ypoints[i] = points[i];
  } else
    ypoints = points;
}

void DLV::panel_plot::set_ylabel(const char label[], const int_g index)
{
  if (index >= 0 and index < num_ypoints)
    ylabels[index] = label;
}

void DLV::band_structure::set_kpoints(const real_g kp[][3],
				      const bool copy_data)
{
  // Todo
}

DLV::string DLV::phonon_dos::get_data_label() const
{
  string name = "Phonon Density of States";
  name += get_tags();
  return name;
}

bool DLV::electron_dos::is_editable() const
{
  return true;
}

DLV::int_g DLV::electron_dos::get_edit_type() const
{
  return (int)edit_energy;
}

DLV::string DLV::electron_dos::get_data_label() const
{
  string name = "Electronic Density of States";
  name += get_tags();
  return name;
}

DLV::string DLV::time_plot::get_data_label() const
{
  string name = get_labels()[0];
  name += " v t";
  name += get_tags();
  return name;
}

DLV::string DLV::phonon_bands::get_data_label() const
{
  string name = "Phonon Dispersion";
  name += get_tags();
  return name;
}

bool DLV::electron_bands::is_editable() const
{
  return true;
}

DLV::int_g DLV::electron_bands::get_edit_type() const
{
  return (int)edit_energy;
}

DLV::string DLV::electron_bands::get_data_label() const
{
  string name = "Electronic Bands";
  name += get_tags();
  return name;
}

bool DLV::electron_bdos::is_editable() const
{
  return true;
}

DLV::int_g DLV::electron_bdos::get_edit_type() const
{
  return (int)edit_energy;
}

DLV::string DLV::electron_bdos::get_data_label() const
{
  string name = "Electronic Bands+DOS";
  name += get_tags();
  return name;
}

DLV::int_g DLV::dos_plot::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::dos_plot::get_sub_data_label(const int_g) const
{
  string name = "DOS";
  return name;
}

DLV::int_g DLV::time_plot::get_number_data_sets() const
{
  return get_ngraphs();
}

DLV::string DLV::time_plot::get_sub_data_label(const int_g n) const
{
  return get_labels()[n];
}

DLV::int_g DLV::rod1d_plot::get_number_data_sets() const
{
  return get_ngraphs();
}

DLV::int_g DLV::rod2d_plot::get_number_data_sets() const
{
  return get_ngraphs();
}

DLV::string DLV::rod1d_plot::get_sub_data_label(const int_g n) const
{
  return get_labels()[n];
}

DLV::string DLV::rod1d_plot::get_data_label() const
{
  string name = "rod 1D plot"; //bit hacky should be ROD::rod_plot_label
  return name;
}

DLV::string DLV::rod2d_plot::get_sub_data_label(const int_g n) const
{
  return get_labels()[n];
}

DLV::string DLV::rod2d_plot::get_data_label() const
{
  string name = "rod 2D plot"; //bit hacky should be ROD::rod_ffac_label
  return name;
}

DLV::int_g DLV::band_structure::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::band_structure::get_sub_data_label(const int_g) const
{
  string name = "Bands";
  return name;
}

bool DLV::dos_plot::has_spin() const
{
  return false;
}

bool DLV::electron_dos::has_spin() const
{
  return spin;
}

bool DLV::panel_plot::has_spin() const
{
  return false;
}

bool DLV::electron_bands::has_spin() const
{
  return spin;
}

DLV::string DLV::leed_pattern::get_name() const
{
  return "LEED pattern";
}

bool DLV::leed_pattern::is_displayable() const
{
  return true;
}

bool DLV::leed_pattern::is_editable() const
{
  return false;
}

bool DLV::leed_pattern::is_edited() const
{
  return false;
}

DLV::string DLV::leed_pattern::get_data_label() const
{
  return get_name();
}

DLV::string DLV::leed_pattern::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::leed_pattern::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::leed_pattern::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::leed_pattern::get_display_type() const
{
  return display_leed;
}

DLV::int_g DLV::leed_pattern::get_edit_type() const
{
  return no_edit;
}

bool DLV::leed_pattern::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::leed_pattern::unload_data()
{
  // Do nothing
}

void DLV::plot_multi_grids::unload_data()
{
  fprintf(stderr, "plot multiplie grids unload not implemented - Todo\n");
}

void DLV::dos_plot::unload_data()
{
  fprintf(stderr, "DOS plot unload not implemented - Todo\n");
}

void DLV::time_plot::unload_data()
{
  fprintf(stderr, "Time plot unload not implemented - Todo\n");
}

void DLV::rod1d_plot::unload_data()
{
  fprintf(stderr, "rod1d plot unload not implemented - Todo\n");
}

void DLV::rod2d_plot::unload_data()
{
  fprintf(stderr, "rod plot unload not implemented - Todo\n");
}

void DLV::band_structure::unload_data()
{
  fprintf(stderr, "Band structure unload not implemented - Todo\n");
}

void DLV::leed_pattern::create(model *m, model *bm,
			       const int_g scell[2][2],
			       const bool show,
			       real_g (* &points)[2], int_g &npoints,
			       real_g (* &domains)[4], int_g &ndomains,
			       int_g * &colours)
{
  int_g sops = m->get_number_of_sym_ops();
  real_l (*srot)[3][3] = new_local_array3(real_l, sops, 3, 3);
  real_l (*strans)[3] = new_local_array2(real_l, sops, 3);
  m->get_cart_rotation_operators(srot, sops);
  m->get_cart_translation_operators(strans, sops);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  int_g nops = 0;
  real_l (*rotate)[3][3] = 0;
  real_l (*translate)[3] = 0;
  if (bm == 0) {
    nops = m->get_number_of_sym_ops();
    rotate = new_local_array3(real_l, nops, 3, 3);
    translate = new_local_array2(real_l, nops, 3);
    m->get_cart_rotation_operators(rotate, nops);
    m->get_cart_translation_operators(translate, nops);
    m->get_primitive_lattice(a, b, c);
  } else {
    nops = bm->get_number_of_sym_ops();
    rotate = new_local_array3(real_l, nops, 3, 3);
    translate = new_local_array2(real_l, nops, 3);
    bm->get_cart_rotation_operators(rotate, nops);
    bm->get_cart_translation_operators(translate, nops);
    // get the base lattice
    bm->get_primitive_lattice(a, b, c);
  }
  //int ndom = 0;
  real_l recipv[2][2];
  real_l (*domainv)[2][2] = 0;
  real_l v[3][3];
  for (int_g i = 0; i < 3; i++) {
    v[0][i] = a[i];
    v[1][i] = b[i];
    v[2][i] = 0.0;
  }
  calc(v, rotate, translate, nops, srot, strans, sops,
       supercell, recipv, domainv, ndomains);
  delete_local_array(translate);
  delete_local_array(rotate);
  bool glidex = false;
  bool glidey = false;
  find_glidelines(srot, strans, sops, v, glidex, glidey);
  delete_local_array(strans);
  delete_local_array(srot);
  // For simplicity in truncating the box, lets normalise the vectors
  real_l al = recipv[0][0] * recipv[0][0] + recipv[0][1] * recipv[0][1];
  real_l bl = recipv[1][0] * recipv[1][0] + recipv[1][1] * recipv[1][1];
  real_l len;
  if (al > bl)
    len = sqrt(al);
  else
    len = sqrt(bl);
  const real_l scale = 2.5;
  for (int_g i = 0; i < 2; i++)
    for (int_g j = 0; j < 2; j++)
      recipv[i][j] = recipv[i][j] * scale / len;
  for (int_g k = 0; k < ndomains; k++) {
    for (int_g i = 0; i < 2; i++)
      for (int_g j = 0; j < 2; j++)
	domainv[k][i][j] = domainv[k][i][j] * scale / len;
  }
  // Generate base points in domain. (-1, -1) to (1, 1) so max of 9?
  // Todo - Do we need to check for glide lines in the substrate? (if 1x1)
  int_g n = 0;
  real_l t[2];
  real_l p[9][2];
  const real_l limit = scale + 0.001;
  for (int_g i = -1; i <= 1; i++) {
    t[0] = real_l(i) * recipv[0][0];
    t[1] = real_l(i) * recipv[0][1];
    for (int_g j = -1; j <= 1; j++) {
      p[n][0] = t[0] + ((real_l) j) * recipv[1][0];
      p[n][1] = t[1] + ((real_l) j) * recipv[1][1];
      if (abs(p[n][0]) < limit and abs(p[n][1]) < limit)
	n++;
    }
  }
  points = new real_g[n][2];
  for (int_g i = 0; i < n; i++) {
    points[i][0] = real_g(p[i][0]);
    points[i][1] = real_g(p[i][1]);
  }
  npoints = n;
  const real_g diam = 0.05;
  if (bm == 0) {
    ndomains = n;
    domains = new real_g[n][4];
    colours = new int_g[n];
    for (int_g i = 0; i < n; i++) {
      domains[i][0] = points[i][0] + diam;
      domains[i][1] = points[i][1];
      domains[i][2] = points[i][0];
      domains[i][3] = points[i][1] + diam;
      colours[i] = 0;
    }
  } else {
    int_g nloop = ndomains;
    if (!show)
      nloop = 1;
    // Now do the domains - bit messy since we duplicate the loops by
    // getting the number of points first.
    int_g tot = 0;
    real_l lx = (domainv[0][0][0] * domainv[0][0][0]
		 + domainv[0][0][1] * domainv[0][0][1]);
    int_g smx = std::abs(scell[0][0]);
    int_g smy = std::abs(scell[1][1]);
    real_l u[2];
    const real_l tol = 0.0001;
    for (int_g k = 0; k < nloop; k++) {
      real_l dx = (domainv[k][0][0] * domainv[k][0][0]
		   + domainv[k][0][1] * domainv[k][0][1]);
      int_g sa;
      int_g sb;
      if (abs(dx - lx) > tol) {
	// vector interchange.
	sa = smy;
	sb = smx;
      } else {
	sa = smx;
	sb = smy;
      }
      for (int_g i = -sa; i <= sa; i++) {
	t[0] = real_l(i) * domainv[k][0][0];
	t[1] = real_l(i) * domainv[k][0][1];
	for (int_g j = -sb; j <= sb; j++) {
	  if ((!glidex or (glidex and ((j != 0) or ((i & 1) == 0)))) and
	      (!glidey or (glidey and ((i != 0) or ((j & 1) == 0))))) {
	    u[0] = t[0] + real_l(j) * domainv[k][1][0];
	    u[1] = t[1] + real_l(j) * domainv[k][1][1];
	    if (abs(u[0]) < limit and abs(u[1]) < limit)
	      tot++;
	  }
	}
      }
    }
    // Now do the domain points for real.
    ndomains = tot;
    domains = new real_g[tot][4];
    colours = new int_g[tot];
    tot = 0;
    for (int_g k = 0; k < nloop; k++) {
      real_l dx = (domainv[k][0][0] * domainv[k][0][0]
		   + domainv[k][0][1] * domainv[k][0][1]);
      int_g sa;
      int_g sb;
      if (abs(dx - lx) > tol) {
	// vector interchange.
	sa = smy;
	sb = smx;
      } else {
	sa = smx;
	sb = smy;
      }
      for (int_g i = -sa; i <= sa; i++) {
	t[0] = real_l(i) * domainv[k][0][0];
	t[1] = real_l(i) * domainv[k][0][1];
	for (int_g j = -sb; j <= sb; j++) {
	  if ((!glidex or (glidex and ((j != 0) or ((i & 1) == 0)))) and
	      (!glidey or (glidey and ((i != 0) or ((j & 1) == 0))))) {
	    u[0] = t[0] + real_l(j) * domainv[k][1][0];
	    u[1] = t[1] + real_l(j) * domainv[k][1][1];
	    if (abs(u[0]) < limit and abs(u[1]) < limit) {
	      // Set the points for the AGEllipse. - Todo, too AVS dependent.
	      domains[tot][0] = real_g(u[0]) + diam;
	      domains[tot][1] = real_g(u[1]);
	      domains[tot][2] = real_g(u[0]);
	      domains[tot][3] = real_g(u[1]) + diam;
	      colours[tot] = k;
	      tot++;
	    }
	  }
	}
      }
    }
  }
  // Finally free up the space for the list of domains.
  delete [] domainv;
}

void DLV::leed_pattern::find_glidelines(const real_l rotations[][3][3],
					const real_l translations[][3],
					const int_g nops,
					const real_l cell[3][3], bool &glidex,
					bool &glidey)
{

  // Todo - glide lines in xy or other non-base lattice vector directions.
  glidex = false;
  glidey = false;
  // loop over operators, test 2x2 part for reflection and
  // 1/2 lattice translation. Make assumptions about 2x2 nature of ops/cell.
  const real_l tol = 0.0001;
  for (int_g i = 0; i < nops; i++) {
    if (!glidex) {
      // a axis glide, 1/2 trans in a (1/2, 0, 0), reflect in a (-1 0 0 1).
      if (abs(rotations[i][0][0] + 1.0) < tol and
	  abs(rotations[i][0][1]) < tol and
	  abs(rotations[i][1][0]) < tol and
	  abs(rotations[i][1][1] - 1.0) < tol and
	  abs(translations[i][0] - (cell[0][0] / 2.0)) < tol and
	  abs(translations[i][1] - (cell[0][1] / 2.0)) < tol) {
	glidex = true;
	continue;
      }
    }
    if (!glidey) {
      // a axis glide, 1/2 trans in a (0, 1/2, 0), reflect in a (1 0 0 -1).
      if (abs(rotations[i][0][0] - 1.0) < tol and
	  abs(rotations[i][0][1]) < tol and
	  abs(rotations[i][1][0]) < tol and
	  abs(rotations[i][1][1] + 1.0) < tol and
	  abs(translations[i][0] - (cell[1][0] / 2.0)) < tol and
	  abs(translations[i][1] - (cell[1][1] / 2.0)) < tol) {
	glidey = true;
	continue;
      }
    }
    // Todo?
    /*if (abs(rotations[i][0][0]) < tol and
	abs(rotations[i][0][1] - 1.0) < tol and
	abs(rotations[i][1][0] - 1.0) < tol and
	abs(rotations[i][1][1]) < tol and
	abs(translations[i][0]-((cell[0][0]+cell[0][0]) / 2.0)) < tol and
	abs(translations[i][1]-((cell[0][1]+cell[1][1]) / 2.0)) < tol) {
      // (1/2, 1/2, 0), (0 1 1 0).
      DLVwarning("LEED pattern", "Possible xy glide line");
      }*/
  }
}

// Paolo's code, defined by Adrian.

void DLV::leed_pattern::calc(const real_l lattice[3][3],
			     const real_l rotations[][3][3],
			     const real_l translations[][3], const int_g nops,
			     const real_l scell_rotate[][3][3],
			     const real_l scell_trans[][3], const int_g sops,
			     const int_g supercell[2][2],
			     real_l base_domain[2][2],
			     real_l (* &scell_domains)[2][2], int_g &ndomains)
{
  const real_l ck = 0.001;
  int_g i, ii, j, jj, k, kk, n, sw, count, n_ops, n_sops;
  //int op_ind[48], sop_ind[48];
  real_l s[3][3], op[3][3], sn[3][3]; //, ssn[48][3][3];
  real_l subst[1][3][3], bdom[1][2][2];

  int_g max_ops = nops;
  if (sops > nops)
    max_ops = sops;
  int_g *op_ind = new_local_array1(int_g, max_ops);
  int_g *sop_ind = new_local_array1(int_g, max_ops);
  real_l (*ssn)[3][3] = new_local_array3(real_l, max_ops, 3, 3);

  count = 0;
  n_ops = 0;
  n_sops = 0;
  ndomains = 0;

  // Generation of overlayer
  s[0][0] = real_l(supercell[0][0])*lattice[0][0]
    +real_l(supercell[0][1])*lattice[1][0];
  s[0][1] = real_l(supercell[0][0])*lattice[0][1]
    +real_l(supercell[0][1])*lattice[1][1];
  s[0][2] = lattice[0][2];   // unimportant z-component. Set to lattice value.

  s[1][0] = real_l(supercell[1][0])*lattice[0][0]
    +real_l(supercell[1][1])*lattice[1][0];
  s[1][1] = real_l(supercell[1][0])*lattice[0][1]
    +real_l(supercell[1][1])*lattice[1][1];
  s[1][2] = lattice[1][2];   // unimportant z-component. Set to lattice value.

  // New filter overlayer ops that preserve the lattice, to avoid the
  // ones that are the old lattice vectors.
  real_l inv[3][3];
  matrix_invert(s, inv, 2);
  bool *safe_ops = new_local_array1(bool, sops);
  for (i=0; i<sops; ++i) {
    safe_ops[i] = true;
    for (j=0; j<3; ++j) {
      if (abs(scell_trans[i][j]) > ck) {
	safe_ops[i] = false;
	break;
      }
    }
    if (j == 3) {
      // Make sure that the lattice vectors are preserved by this op - New
      // a vector
      real_l vec[3] = { 0.0, 0.0, 0.0 };
      for (int_g ai = 0; ai < 3; ai++)
	for (int_g aj = 0; aj < 3; aj++)
	  vec[ai] += scell_rotate[i][ai][aj] * s[0][aj];
      real_l frac[3] = { 0.0, 0.0, 0.0 };
      for (int_g bi = 0; bi < 2; bi++)
	for (int_g bj = 0; bj < 2; bj++)
	  frac[bi] += inv[bi][bj] * vec[bj];
      for (int_g ai = 0; ai < 2; ai++) {
	if (abs(frac[ai] - nint(frac[ai])) > 0.001) {
	  safe_ops[i] = false;
	  break;
	}
      }
      if (safe_ops[i]) {
	// b vector
	for (int_g ai = 0; ai < 3; ai++)
	  vec[ai] = 0.0;
	for (int_g ai = 0; ai < 3; ai++)
	  for (int_g aj = 0; aj < 3; aj++)
	    vec[ai] += scell_rotate[i][ai][aj] * s[1][aj];
	real_l frac[3] = { 0.0, 0.0, 0.0 };
	for (int_g bi = 0; bi < 2; bi++)
	  for (int_g bj = 0; bj < 2; bj++)
	    frac[bi] += inv[bi][bj] * vec[bj];
	for (int_g ai = 0; ai < 2; ai++) {
	  if (abs(frac[ai] - nint(frac[ai])) > 0.001) {
	    safe_ops[i] = false;
	    break;
	  }
	}
      }
    }
  }

  // Find substrate symm ops not overlayer symm op. Exclude reflexions & trans.
  for (k=0; k<nops; ++k) {
    sw = 1;
    if (abs(rotations[k][2][2]+1.0) > ck) {   //valid rotation
      for (i=0; i<3; ++i) {
	if (abs(translations[k][i]) > ck) {
	  sw = 0;
	  break;
	}
      }
      if (i == 3) { //okay, no translation
        // Check if valid symm op is not an overlayer symm op
        for (kk=0; kk<sops; ++kk) {
	  if (safe_ops[kk]) {
	    count = 0;
	    for (i=0; i<2; ++i) {
	      for (j=0; j<2; ++j) {
		if (abs(rotations[k][i][j]-scell_rotate[kk][i][j])<ck)
		  ++count;
	      }
	    }
	    if (count == 4) {
	      for (j=0; j<2; ++j) {
		if (abs(translations[k][j]-scell_trans[kk][j])>ck) {
		  break;
		}
	      }
	      if (j == 2) {
		sw = 0;
		break;
	      }
	    }
	  }
        }
      }
      if (sw == 1) {
        op_ind[n_ops] = k;   // saving index of valid operator in array op_ind
        ++n_ops;
      }
    }
  }
  // Save overlayer ops with no translations. BGS beware z reflections!
  for (i=0; i<sops; ++i) {
    if (safe_ops[i]) {
      for (j=0; j<2; ++j) {
	if (abs(scell_trans[i][j]) > ck) {
	  break;
	}
      }
      if (j==2) {
	if (abs(scell_rotate[i][2][2] + 1.0) > ck) {
	  sop_ind[n_sops] = i;
	  ++n_sops;
	}
      }
    }
  }

  // Apply overlayer ops (with no translation part) to exclusive substrate ops
  // and eliminate if found in exclusive subrate ops list
  for (i=0; i<n_ops; ++i) {
    if (op_ind[i] != -1) {  // apply overlayer op to substrate op
      for (j=0; j<n_sops; ++j) {
	//reset array op[][]
	for (ii=0; ii<2; ++ii) {
	  for (jj=0; jj<2; ++jj) {
	    op[ii][jj]=0.0;
	  }
	}
	for (ii=0; ii<2; ++ii) {
	  for (jj=0; jj<2; ++jj) {
	    for (n=0; n<2; ++n) {
	      op[ii][jj]=op[ii][jj]+(scell_rotate[sop_ind[j]][ii][n]
				     *rotations[op_ind[i]][n][jj]);
	    }
	  }
	}
	for (k=0; k<n_ops; ++k) {  // scan list of substrate ops
	  if (k != i and op_ind[k] != -1) {
	    count = 0;
	    for (ii=0; ii<2; ++ii) {
	      for (jj=0; jj<2; ++jj) {
		if (abs(op[ii][jj]-rotations[op_ind[k]][ii][jj]) < ck)
		  ++count;
	      }
	    }
	    if (count == 4) {
	      op_ind[k] = -1;
	      break;
	    }
	  }
	}
      }
    }
  }

  // Add the supercell to ssn array (as it will give a valid domain!)
  for (i=0; i<2; ++i) {
    for (j=0; j<2; ++j) {
      ssn[0][i][j] = s[i][j];
    }
  }
  ndomains = 1;
  // Apply exclusive substrate symm ops to supercell
  for (k=0; k<n_ops; ++k) {
    if (op_ind[k] != -1) {
      sw = 1;
      //reset sn
      for (i=0; i<2; ++i) {
	for (j=0; j<2; ++j) {
	  sn[i][j] = 0.0;
	}
      }

      for (i=0; i<2; ++i) {
	for (j=0; j<2; ++j) {
	  sn[0][i] = sn[0][i]+rotations[op_ind[k]][i][j]*s[0][j];
	  sn[1][i] = sn[1][i]+rotations[op_ind[k]][i][j]*s[1][j];
	}
      }
      for (i=0; i<2; ++i) {
	sn[0][i] = sn[0][i]+translations[op_ind[k]][i];
	sn[1][i] = sn[1][i]+translations[op_ind[k]][i];
      }

      // Check if sn has been already generated. If not, add to list
      for (kk=0; kk<ndomains; ++kk) {
	count = 0;
	for (i=0; i<2; ++i) {
	  for (j=0; j<2; ++j) {
	    if (abs(sn[i][j]-ssn[kk][i][j])<ck)
	      ++count;
	  }
	}
	if (count==4) {
	  sw =0;
	  break;
	}
      }
      if (sw == 1) {  // Save it
	for (i=0; i<2; ++i) {
	  for (j=0; j<2; ++j) {
	    ssn[ndomains][i][j] = sn[i][j];
	  }
	}
	++ndomains;
      }
    }
  }
  // Create scell_domains array
  scell_domains = new real_l [ndomains][2][2];
  k_lat(ssn, scell_domains, ndomains);   // Obtain reciprocal space overlayers

  // Generation of reciprocal cell for the substrate
  for (i=0; i<3; ++i) {
    for (j=0; j<3; ++j) {
      subst[0][i][j] = lattice[i][j];
    }
  }

  k_lat(subst, bdom, 1);

  for (i=0; i<2; ++i) {
    for (j=0; j<2; ++j) {
      base_domain[i][j] = bdom[0][i][j];
    }
  }

  delete_local_array(safe_ops);
  delete_local_array(ssn);
  delete_local_array(sop_ind);
  delete_local_array(op_ind);
}

void DLV::leed_pattern::k_lat(const real_l lattice[][3][3],
			      real_l k_lattice[][2][2], const int_g ndomains)
{
  int_g k;
  real_l n[3] = {0.0, 0.0, 1.0};
  real_l d1_x, d1_y, d1_z, d2_x, d2_y, V;

  // Reciprocal space lattice vector calculation.
  for (k=0; k<ndomains; ++k) {
    // b x c
    d1_x = lattice[k][1][1]*n[2]-lattice[k][1][2]*n[1];
    d1_y = lattice[k][1][2]*n[0]-lattice[k][1][0]*n[2];
    //d1_z = lattice[k][1][0]*n[1]-lattice[k][1][1]*n[0];
    d1_z = 0.0;

    // c x a
    d2_x = n[1]*lattice[k][0][2]-n[2]*lattice[k][0][1];
    d2_y = n[2]*lattice[k][0][0]-n[0]*lattice[k][0][2];
    //    d2_z = n[0]*lattice[k][0][1]-n[1]*lattice[k][0][0];

    //Cell volume
    V = lattice[k][0][0]*d1_x+lattice[k][0][1]*d1_y+lattice[k][0][2]*d1_z;

    k_lattice[k][0][0] = (2*pi/V)*d1_x;
    k_lattice[k][0][1] = (2*pi/V)*d1_y;

    k_lattice[k][1][0] = (2*pi/V)*d2_x;
    k_lattice[k][1][1] = (2*pi/V)*d2_y;

  }
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::line::render(const render_parent *parent,
				 class model *structure,
				 char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::line::render(const render_parent *parent,
				 class model *structure,
				 const int_g component, const int_g method,
				 const int_g index, char message[],
				 const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_line_data(parent, is_kspace(),
					      message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (count_display_objects() == 0) {
    display_obj *ptr = 0;
    ptr = display_obj::create_line_obj(parent, obj, name, index, is_kspace(),
				       message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::line::update1D(const int_g method, const int_g h,
				   const int_g k, const int_g l,
				   const real_g x, const real_g y,
				   const real_g z, const int_g object,
				   const bool conv, const bool frac,
				   const bool parent, model *const m,
				   char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing drawable for line", mlen);
    ok = DLV_ERROR;
  } else {
    if (method < 2) {
      coord_type atom1[3];
      coord_type atom2[3];
      if (!m->get_selected_positions(atom1, atom2)) {
	strncpy(message, "At least two atoms must be selected", mlen);
	return DLV_WARNING;
      }
      if (method == 1) {
	// endpoint is vector between atoms added to origin
	endp[0] = startp[0] + (real_g)(atom1[0] - atom2[0]);
	endp[1] = startp[1] + (real_g)(atom1[1] - atom2[1]);
	endp[2] = startp[2] + (real_g)(atom1[2] - atom2[2]);
      } else {
	// endpoint is last atom selected
	//real_g offset[3];
	//for (int_g i = 0; i < 3; i++)
	//  offset[i] = (real_g)atom2[i] - startp[i];
	startp[0] = (real_g)atom2[0];
	startp[1] = (real_g)atom2[1];
	startp[2] = (real_g)atom2[2];
	if (method == 0) { // if seems redundant
	  // and origin is previous atom selected
	  endp[0] = (real_g)atom1[0];
	  endp[1] = (real_g)atom1[1];
	  endp[2] = (real_g)atom1[2];
	}
      }
    } else if (method == 2) {
      // line origin is moved to average atom position without changing vector
      // average atom positions and set origin
      coord_type position[3];
      m->get_average_selection_position(position);
      real_g offset[3];
      for (int_g i = 0; i < 3; i++)
	offset[i] = (real_g)position[i] - startp[i];
      startp[0] = (real_g)position[0];
      startp[1] = (real_g)position[1];
      startp[2] = (real_g)position[2];
      endp[0] += offset[0];
      endp[1] += offset[1];
      endp[2] += offset[2];
    } else if (method == 3) {
      if (h == 0 and k == 0 and l == 0) {
	strncpy(message, "At least one miller index must be non-zero", mlen);
	return DLV_WARNING;
      }
      coord_type a[3];
      coord_type b[3];
      coord_type c[3];
      int_g dim;
      bool has_rotation = true;
      if (parent) {
	if (is_kspace()) {
	  strncpy(message, "K space lattice mapping not implemented", mlen);
	  ok = DLV_WARNING;
	} else {
	  real_l r[3][3];
	  operation *op = operation::find_parent_geometry(r, has_rotation);
	  if (op == 0) {
	    strncpy(message, "Model doesn't have a parent lattice", mlen);
	    ok = DLV_WARNING;
	  } else if (!has_rotation) {
	    strncpy(message,
		    "Lattice mapping not implemented for geometry edit", mlen);
	    ok = DLV_WARNING;
	  } else {
	    coord_type aa[3];
	    coord_type bb[3];
	    coord_type cc[3];
	    if (is_kspace())
	      op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	    else if (conv)
	      op->get_model()->get_conventional_lattice(aa, bb, cc);
	    else
	      op->get_model()->get_primitive_lattice(aa, bb, cc);
	    dim = op->get_model()->get_number_of_periodic_dims();
	    for (int i = 0; i < 3; i++) {
	      a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
	      b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
	      c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	    }
	  }
	}
      } else {
	if (is_kspace())
	  m->get_reciprocal_lattice(a, b, c);
	else if (conv)
	  m->get_conventional_lattice(a, b, c);
	else
	  m->get_primitive_lattice(a, b, c);
	dim = m->get_number_of_periodic_dims();
      }
      if (ok == DLV_OK) {
	startp[0] = 0.0;
	startp[1] = 0.0;
	startp[2] = 0.0;
	for (int_g i = 0; i < 3; i++)
	  endp[i] = (real_g)(a[i] * (real_g)h);
	if (dim > 1) {
	  for (int_g i = 0; i < 3; i++)
	    endp[i] += (real_g)(b[i] * (real_g)k);
	  if (dim > 2) {
	    for (int_g i = 0; i < 3; i++)
	      endp[i] += (real_g)(c[i] * (real_g)l);
	  }
	}
      }
    } else if (method == 4) {
      // copy from object
      data_object *l = operation::find_line(object);
      if (l == 0) {
	strncpy(message, "Failed to find selected line object", mlen);
	return DLV_WARNING;
      } else {
	line *ldata = dynamic_cast<line *>(l);
	if (ldata == 0) {
	  strncpy(message, "Selected object isn't line", mlen);
	  return DLV_WARNING;
	} else {
	  startp[0] = ldata->startp[0];
	  startp[1] = ldata->startp[1];
	  startp[2] = ldata->startp[2];
	  endp[0] = ldata->endp[0];
	  endp[1] = ldata->endp[1];
	  endp[2] = ldata->endp[2];
	}
      }
    } else {
      coord_type position[3];
      position[0] = x;
      position[1] = y;
      position[2] = z;
      if (frac) {
	coord_type a[3];
	coord_type b[3];
	coord_type c[3];
	int_g dim;
	bool has_rotation = true;
	if (parent) {
	  if (is_kspace()) {
	    strncpy(message, "K space lattice mapping not implemented", mlen);
	    ok = DLV_WARNING;
	  } else {
	    real_l r[3][3];
	    operation *op = operation::find_parent_geometry(r, has_rotation);
	    if (op == 0) {
	      strncpy(message, "Model doesn't have a parent lattice", mlen);
	      ok = DLV_WARNING;
	    } else if (!has_rotation) {
	      strncpy(message,
		      "Lattice mapping not implemented for geometry edit",
		      mlen);
	      ok = DLV_WARNING;
	    } else {
	      coord_type aa[3];
	      coord_type bb[3];
	      coord_type cc[3];
	      if (is_kspace())
		op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	      else if (conv)
		op->get_model()->get_conventional_lattice(aa, bb, cc);
	      else
		op->get_model()->get_primitive_lattice(aa, bb, cc);
	      dim = op->get_model()->get_number_of_periodic_dims();
	      for (int i = 0; i < 3; i++) {
		a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
		b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
		c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	      }
	    }
	  }
	} else {
	  dim = m->get_number_of_periodic_dims();
	  if (is_kspace())
	    m->get_reciprocal_lattice(a, b, c);
	  else if (conv)
	    m->get_conventional_lattice(a, b, c);
	  else
	    m->get_primitive_lattice(a, b, c);
	}
	if (ok == DLV_OK) {
	  // loop to dim should mean that non-periodic ones shouldn't change
	  // from original value set.
	  for (int_g i = 0; i < dim; i++)
	    position[i] = a[i] * x;
	  if (dim > 1) {
	    for (int_g i = 0; i < dim; i++)
	      position[i] += b[i] * y;
	    if (dim > 2) {
	      for (int_g i = 0; i < dim; i++)
		position[i] += c[i] * z;
	    }
	  }
	}
      }
      if (method == 5) {
	// line endpoint is moved to xyz, changing vector
	endp[0] = (real_g)position[0];
	endp[1] = (real_g)position[1];
	endp[2] = (real_g)position[2];
      } else { // 6
	// line origin is moved to xyz position without changing vector
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - startp[i];
	startp[0] = (real_g)position[0];
	startp[1] = (real_g)position[1];
	startp[2] = (real_g)position[2];
	endp[0] += offset[0];
	endp[1] += offset[1];
	endp[2] += offset[2];
      }
    }
    obj->update_line(startp, endp);
  }
  return ok;
}

void DLV::plot_data::reset_data_sets()
{
  // Do nothing?
}

bool DLV::dos_plot::hide_render(const int_g object, const int_g visible,
				const render_parent *parent,
				class model *structure,
				char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		       get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_yrange(),
		       message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::dos_plot::render(const render_parent *parent,
				     class model *structure,
				     const int_g, const int_g,
				     const int_g index, char message[],
				     const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_dos_data(parent, get_grid(),
					     get_npoints(), get_data(),
					     get_labels(), get_ngraphs(),
					     has_spin(), get_xpoints(),
					     get_xlabels(), get_nxpoints(),
					     get_yrange(), message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 0;  //dos_plot
  ptr = display_obj::create_dos_plot(parent, obj, name, index, get_xlabel(),
				     get_ylabel(), get_ngraphs(), type,
				     message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::dos_plot::render_data(const class model *structure,
					  char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::time_plot::hide_render(const int_g object, const int_g visible,
				 const render_parent *parent,
				 class model *structure,
				 char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		       get_labels(), get_ngraphs(), false, get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_yrange(),
		       message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::time_plot::render(const render_parent *parent,
				      class model *structure,
				      const int_g component, const int_g method,
				      const int_g index, char message[],
				      const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_dos_data(parent, get_grid(),
					     get_npoints(), get_data(),
					     get_labels(), get_ngraphs(),
					     false, get_xpoints(),
					     get_xlabels(), get_nxpoints(),
					     get_yrange(), message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), false, get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 0;  //dos plot
  ptr = display_obj::create_dos_plot(parent, obj, name, index, get_xlabel(),
				     get_ylabel(), get_ngraphs(), type,
				     message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::time_plot::render_data(const class model *structure,
					   char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::rod1d_plot::hide_render(const int_g object, const int_g visible,
				const render_parent *parent,
				class model *structure,
				char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_multi_grids(), get_ngrids(), 
		       get_grid_sizes(), get_npoints(),
		       get_data(), get_select_grid(),
		       get_labels(), get_ngraphs(), false, get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_yrange(),
		       message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::rod1d_plot::render(const render_parent *parent,
				      class model *structure,
				      const int_g component, const int_g method,
				      const int_g index, 
				      char message[], const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_multi_grid_data(parent, get_multi_grids(),
						    get_ngrids(),
						    get_select_grid(),
						    get_grid_sizes(),
						    get_npoints(), get_data(),
						    get_labels(), get_ngraphs(),
						    false, get_xpoints(),
						    get_xlabels(),
						    get_nxpoints(),
						    get_yrange(), message,
						    mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_multi_grids(), get_ngrids(),  
		     get_grid_sizes(), get_npoints(),
		     get_data(), get_select_grid(),
		     get_labels(), get_ngraphs(), false, get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 1; //rod1d
  ptr = display_obj::create_dos_plot(parent, obj, name, index, get_xlabel(),
				     get_ylabel(), get_ngraphs(), type,
				     message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type 
DLV::rod1d_plot::transfer_and_render(class data_object *old_data,
				    const render_parent *parent,
				    char message[],const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_multi_grid_data(parent, get_multi_grids(),
						    get_ngrids(),
						    get_select_grid(),
						    get_grid_sizes(),
						    get_npoints(), get_data(),
						    get_labels(), get_ngraphs(),
						    false, get_xpoints(),
						    get_xlabels(),
						    get_nxpoints(),
						    get_yrange(),
						    message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_multi_grids(), get_ngrids(),  
		     get_grid_sizes(), get_npoints(),
		     get_data(), get_select_grid(),
		     get_labels(), get_ngraphs(), false, get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g visible = true;  
  rod1d_plot *old_data1=dynamic_cast<rod1d_plot*> (old_data);  
  ptr = old_data1->remove_display_obj();
  ptr->update_data(parent, obj, name, get_xlabel(), get_ylabel(), get_ngraphs(),
		   visible, message, mlen);  //is this ok?
  add_display_obj(ptr);
  ptr->update_display_list(parent, name);
  ptr->attach_params();
   return DLV_OK;
}

DLVreturn_type DLV::rod1d_plot::render_data(const class model *structure,
					   char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::rod2d_plot::hide_render(const int_g object, const int_g visible,
				  const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		       get_labels(), get_ngraphs(), false, get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_yrange(),
		       message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::rod2d_plot::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index, 
				       char message[], const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_dos_data(parent, get_grid(),
					     get_npoints(), get_data(),
					     get_labels(), get_ngraphs(),
					     false, get_xpoints(),
					     get_xlabels(), get_nxpoints(),
					     get_yrange(), message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), false, get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 2; //rod
  ptr = display_obj::create_dos_plot(parent, obj, name, index, get_xlabel(),
				     get_ylabel(), get_ngraphs(), type,
				     message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type 
DLV::rod2d_plot::transfer_and_render(class data_object *old_data,
				     const render_parent *parent,
				     char message[], const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_dos_data(parent, get_grid(),
					     get_npoints(), get_data(),
					     get_labels(), get_ngraphs(),
					     false, get_xpoints(),
					     get_xlabels(), get_nxpoints(),
					     get_yrange(), message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), false, get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_yrange(),
		     message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g visible = false;  
  rod2d_plot *old_data1=dynamic_cast<rod2d_plot*> (old_data);  
  ptr = old_data1->remove_display_obj();
  ptr->update_data(parent, obj, name, get_xlabel(), get_ylabel(), get_ngraphs(),
		   visible, message, mlen);
  add_display_obj(ptr);
  ptr->update_display_list(parent, name);
  ptr->attach_params();
  return DLV_OK;
}

DLVreturn_type DLV::rod2d_plot::render_data(const class model *structure,
					   char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::panel_plot::hide_render(const int_g object, const int_g visible,
				  const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		       get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_ypoints(),
		       get_ylabels(), get_nypoints(), get_xrange(),
		       get_yrange(), message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::panel_plot::render(const render_parent *parent,
				       class model *structure,
				       const int_g, const int_g method,
				       const int_g index, char message[],
				       const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_panel_data(parent, get_grid(),
					       get_npoints(), get_data(),
					       get_labels(), get_ngraphs(),
					       has_spin(), get_xpoints(),
					       get_xlabels(), get_nxpoints(),
					       get_ypoints(), get_ylabels(),
					       get_nypoints(), get_xrange(),
					       get_yrange(), message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_ypoints(),
		     get_ylabels(), get_nypoints(), get_xrange(),
		     get_yrange(), message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  if (method == 1 and has_spin())
    ptr = display_obj::create_panel_plot(parent, obj, name, index,
					 get_xlabel(), get_ylabel(),
					 get_ngraphs(), message, mlen, true);
  else
    ptr = display_obj::create_panel_plot(parent, obj, name, index,
					 get_xlabel(), get_ylabel(),
					 get_ngraphs(), message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

bool DLV::electron_bdos::hide_render(const int_g object, const int_g visible,
				     const render_parent *parent,
				     class model *structure,
				     char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		       get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		       get_xlabels(), get_nxpoints(), get_ypoints(),
		       get_ylabels(), get_nypoints(), get_xrange(),
		       get_yrange(), dos->get_grid(), dos->get_npoints(),
		       dos->get_data(), dos->get_ngraphs(), message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::electron_bdos::render(const render_parent *parent,
					  class model *structure,
					  const int_g, const int_g,
					  const int_g index, char message[],
					  const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_bdos_data(parent, get_grid(),
					      get_npoints(), get_data(),
					      get_labels(), get_ngraphs(),
					      has_spin(), get_xpoints(),
					      get_xlabels(), get_nxpoints(),
					      get_ypoints(), get_ylabels(),
					      get_nypoints(), get_xrange(),
					      get_yrange(), dos->get_grid(),
					      dos->get_npoints(),
					      dos->get_data(),
					      dos->get_ngraphs(),
					      message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, get_grid(), get_npoints(), get_data(),
		     get_labels(), get_ngraphs(), has_spin(), get_xpoints(),
		     get_xlabels(), get_nxpoints(), get_ypoints(),
		     get_ylabels(), get_nypoints(), get_xrange(),
		     get_yrange(), dos->get_grid(), dos->get_npoints(),
		     dos->get_data(), dos->get_ngraphs(), message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_panel_plot(parent, obj, name, index, get_xlabel(),
				       get_ylabel(), get_ngraphs(),
				       message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::phonon_bands::render_data(const class model *structure,
					      char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::electron_bands::render_data(const class model *structure,
						char message[],
						const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::leed_pattern::render(const render_parent *parent,
					 class model *structure,
					 char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::leed_pattern::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    real_g (*points)[2] = 0;
    int_g npoints = 0;
    real_g (*domains)[4] = 0;
    int_g ndomains = 0;
    int_g *colours = 0;
    create(structure, base, supercell, show_domains, points, npoints,
	   domains, ndomains, colours);
    obj = DLV::drawable_obj::create_leed_data(parent, points, npoints, domains,
					      ndomains, colours,message, mlen);
    //delete [] domains;
    //delete [] points;
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
    display_obj *ptr = 0;
    ptr = display_obj::create_leed_pattern(parent, obj, "LEED pattern", index,
					   message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, "LEED pattern");
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::leed_pattern::update_leed(const bool d, model *const m,
					      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing drawable for leed pattern", mlen);
    ok = DLV_ERROR;
  } else {
    real_g (*points)[2] = 0;
    int_g npoints = 0;
    real_g (*domains)[4] = 0;
    int_g ndomains = 0;
    int_g *colours = 0;
    create(m, base, supercell, d, points, npoints, domains, ndomains, colours);
    obj->update_leed(points, npoints, domains, ndomains, colours);
    delete [] colours;
    delete [] domains;
    delete [] points;
    show_domains = d;
  }
  return ok;
}

DLV::data_object *DLV::electron_dos::edit(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_dos_data(parent, get_grid(),
					     get_npoints(), get_data(),
					     get_labels(), get_ngraphs(),
					     has_spin(), get_xpoints(),
					     get_xlabels(), get_nxpoints(),
					     get_yrange(), message, mlen);
    if (obj == 0)
      return 0;
    else
      set_drawable(obj);
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Energy shift of ";
  name += get_data_label();
  name += " ";
  name += get_labels()[component];
  edit1D_object *ptr = edit1D_dos::create(parent, this, obj, false, name,
					  index, message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::data_object *DLV::electron_bands::edit(const render_parent *parent,
					    class model *structure,
					    const int_g component,
					    const int_g method,
					    const int_g index,
					    char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_panel_data(parent, get_grid(),
					       get_npoints(), get_data(),
					       get_labels(), get_ngraphs(),
					       has_spin(), get_xpoints(),
					       get_xlabels(), get_nxpoints(),
					       get_ypoints(), get_ylabels(),
					       get_nypoints(), get_xrange(),
					       get_yrange(), message, mlen);
    if (obj == 0)
      return 0;
    else
      set_drawable(obj);
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Energy shift of ";
  name += get_data_label();
  name += " ";
  name += get_labels()[component];
  edit1D_object *ptr = edit1D_bands::create(parent, this, obj, false, name,
					    index, message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::data_object *DLV::electron_bdos::edit(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_bdos_data(parent, get_grid(),
					      get_npoints(), get_data(),
					      get_labels(), get_ngraphs(),
					      has_spin(), get_xpoints(),
					      get_xlabels(), get_nxpoints(),
					      get_ypoints(), get_ylabels(),
					      get_nypoints(), get_xrange(),
					      get_yrange(), dos->get_grid(),
					      dos->get_npoints(),
					      dos->get_data(),
					      dos->get_ngraphs(),
					      message, mlen);
    if (obj == 0)
      return 0;
    else
      set_drawable(obj);
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Energy shift of ";
  name += get_data_label();
  name += " ";
  name += get_labels()[component];
  edit1D_object *ptr = edit1D_bdos::create(parent, this, obj, false, name,
					   index, message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

bool DLV::edit1D_object::is_displayable() const
{
  return true;
}

bool DLV::edit1D_object::is_editable() const
{
  return false;
}

DLV::string DLV::edit1D_object::get_data_label() const
{
  string name = "Energy shift of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit1D_object::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit1D_object::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit1D_object::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit1D_object::get_display_type() const
{
  return (int)display_plot;
}

DLV::int_g DLV::edit1D_object::get_edit_type() const
{
  return no_edit;
}

bool DLV::edit1D_object::get_sub_is_vector(const int_g n) const
{
  // Todo - no 1D vectors => false?
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit1D_dos *DLV::edit1D_dos::create(const render_parent *parent,
					 data_object *data,
					 const drawable_obj *obj,
					 const bool kspace,
					 const string name,
					 const int_g index, char message[],
					 const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_dos_shift(parent, obj, kspace,
						 message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit1D_dos *edit = new edit1D_dos(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

DLVreturn_type DLV::edit1D_dos::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index,
				       char message[], const int_g mlen)
{
  // basically a copy of dos::render
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing object for dos edit", mlen);
    return DLV_ERROR;
  }
  data_object *d = get_base_data();
  plot_data *p = static_cast<plot_data *>(d);
  display_obj *ptr = 0;
  string name = get_data_label();
  int_g type = 0;
  ptr = display_obj::create_dos_plot(parent, obj, name, index, p->get_xlabel(),
				     p->get_ylabel(), p->get_ngraphs(), type,
				     message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit1D_dos::update_shift(const real_g shift,
					     char message[], const int_g mlen)
{
  message[0] = '\0';
  edited_obj *myedit = get_edit_obj();
  if (myedit == 0) {
    strncpy(message, "Missing object for dos shift", mlen);
    return DLV_ERROR;
  }
  if (myedit->update_shift(shift, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

DLV::edit1D_bands *DLV::edit1D_bands::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const bool kspace,
					     const string name,
					     const int_g index, char message[],
					     const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_band_shift(parent, obj, kspace,
						  message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit1D_bands *edit = new edit1D_bands(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

DLVreturn_type DLV::edit1D_bands::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  // basically a copy of dos::render
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing object for band edit", mlen);
    return DLV_ERROR;
  }
  data_object *d = get_base_data();
  panel_plot *p = static_cast<panel_plot *>(d);
  display_obj *ptr = 0;
  string name = get_data_label();
  if (method == 1 and p->has_spin())
    ptr = display_obj::create_panel_plot(parent, obj, name, index,
					 p->get_xlabel(), p->get_ylabel(),
					 p->get_ngraphs(), message, mlen, true);
  else
    ptr = display_obj::create_panel_plot(parent, obj, name, index,
					 p->get_xlabel(), p->get_ylabel(),
					 p->get_ngraphs(), message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit1D_bands::update_shift(const real_g shift,
					       char message[], const int_g mlen)
{
  message[0] = '\0';
  edited_obj *myedit = get_edit_obj();
  if (myedit == 0) {
    strncpy(message, "Missing object for dos shift", mlen);
    return DLV_ERROR;
  }
  if (myedit->update_shift(shift, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

DLV::edit1D_bdos *DLV::edit1D_bdos::create(const render_parent *parent,
					  data_object *data,
					  const drawable_obj *obj,
					  const bool kspace,
					  const string name,
					  const int_g index, char message[],
					  const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_bdos_shift(parent, obj, kspace,
						 message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit1D_bdos *edit = new edit1D_bdos(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

DLVreturn_type DLV::edit1D_bdos::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  // basically a copy of dos::render
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing object for bands and dos edit", mlen);
    return DLV_ERROR;
  }
  data_object *d = get_base_data();
  plot_data *p = static_cast<plot_data *>(d);
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_panel_plot(parent, obj, name, index,
				       p->get_xlabel(), p->get_ylabel(),
				       p->get_ngraphs(), message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit1D_bdos::update_shift(const real_g shift,
					      char message[], const int_g mlen)
{
  message[0] = '\0';
  edited_obj *myedit = get_edit_obj();
  if (myedit == 0) {
    strncpy(message, "Missing object for dos shift", mlen);
    return DLV_ERROR;
  }
  if (myedit->update_shift(shift, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_space_line *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_space_line("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::k_space_line *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::k_space_line("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rod1d_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::rod1d_plot("recover", "temp", 0, 0, 0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rod2d_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::rod2d_plot("recover", "temp", 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::phonon_dos *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::phonon_dos("recover", "temp", 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::electron_dos *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::electron_dos("recover", "temp", 0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::phonon_bands *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::phonon_bands("recover", "temp", 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::electron_bands *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::electron_bands("recover", "temp", 0, 0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::electron_bdos *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::electron_bdos("recover", "temp", 0, 0, 0, false, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::leed_pattern *t,
				    const unsigned int file_version)
    {
      int data[1][2];
      ::new(t)DLV::leed_pattern(0, data);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::dos_plot *t,
				    const unsigned int file_version)
    {
      // Todo - will this result in lost pointers with bdos?
      ::new(t)DLV::dos_plot("recover", "temp", 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::time_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::time_plot("recover", "temp", 0, 0);
    }

  }
}

template <class Archive>
void DLV::line::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & startp;
  ar & endp;
}

template <class Archive>
void DLV::k_space_line::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<line>(*this);
}

template <class Archive>
void DLV::plot_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & title;
  ar & ngraphs;
  ar & npoints;
  ar & num_xpoints;
  ar & xrange[0];
  ar & xrange[1];
  ar & yrange[0];
  ar & yrange[1];
  ar & x_axis_label;
  ar & y_axis_label;
  for (int i = 0; i < ngraphs; i++)
    ar & labels[i];
  for (int i = 0; i < num_xpoints; i++)
    ar & xlabels[i];
  for (int i = 0; i < num_xpoints; i++)
    ar & xpoints[i];
}

template <class Archive>
void DLV::plot_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & title;
  ar & ngraphs;
  ar & npoints;
  ar & num_xpoints;
  ar & xrange[0];
  ar & xrange[1];
  ar & yrange[0];
  ar & yrange[1];
  ar & x_axis_label;
  ar & y_axis_label;
  labels = new string[ngraphs];
  for (int i = 0; i < ngraphs; i++)
    ar & labels[i];
  xlabels = new string[num_xpoints];
  for (int i = 0; i < num_xpoints; i++)
    ar & xlabels[i];
  xpoints = new float[num_xpoints];
  for (int i = 0; i < num_xpoints; i++)
    ar & xpoints[i];
  plots = new float *[ngraphs];
}

template <class Archive>
void DLV::plot_multi_grids::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<plot_data>(*this);
  if (version > 0) {
    ar & ngrids;
    for (int i = 0; i < ngrids; i++)
      ar & grid_sizes[i];
    for (int i = 0; i < ngrids; i++)
      ar & select_grid[i];
    // I don't think I've got a file to reload from in ROD so must save
    for (int i = 0; i < ngrids; i++) {
      for (int j = 0; j < get_npoints(); j++)
	ar & multi_grids[i][j];
    }
  }
}

template <class Archive>
void DLV::plot_multi_grids::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plot_data>(*this);
  if (version > 0) {
    ar & ngrids;
    grid_sizes = new int_g[ngrids];
    for (int i = 0; i < ngrids; i++)
      ar & grid_sizes[i];
    select_grid = new int_g[ngrids];
    for (int i = 0; i < ngrids; i++)
      ar & select_grid[i];
    // I don't think I've got a file to reload from in ROD so must load
    multi_grids = new real_g *[ngrids];
    for (int i = 0; i < ngrids; i++) {
      multi_grids[i] = new real_g[get_npoints()];
      for (int j = 0; j < get_npoints(); j++)
	ar & multi_grids[i][j];
    }
  }
}

template <class Archive>
void DLV::dos_plot::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plot_data>(*this);
}

template <class Archive>
void DLV::electron_dos::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<dos_plot>(*this);
  ar & spin;
}

template <class Archive>
void DLV::time_plot::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plot_data>(*this);
}

template <class Archive>
void DLV::rod1d_plot::save(Archive &ar, const unsigned int version) const
{
  if (version == 0)
    ar & boost::serialization::base_object<plot_data>(*this);
  else {
    ar & boost::serialization::base_object<plot_multi_grids>(*this);
  }
}

template <class Archive>
void DLV::rod1d_plot::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<plot_data>(*this);
  else {
    ar & boost::serialization::base_object<plot_multi_grids>(*this);
  }
}

template <class Archive>
void DLV::rod2d_plot::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plot_data>(*this);
}

template <class Archive>
void DLV::panel_plot::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<plot_data>(*this);
  ar & num_ypoints;
  for (int i = 0; i < num_ypoints; i++)
    ar & ylabels[i];
  for (int i = 0; i < num_ypoints; i++)
    ar & ypoints[i];
}

template <class Archive>
void DLV::panel_plot::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plot_data>(*this);
  ar & num_ypoints;
  ylabels = new string[num_ypoints];
  for (int i = 0; i < num_ypoints; i++)
    ar & ylabels[i];
  ypoints = new float[num_ypoints];
  for (int i = 0; i < num_ypoints; i++)
    ar & ypoints[i];
}

template <class Archive>
void DLV::band_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<panel_plot>(*this);
  ar & n_k_points;
}

template <class Archive>
void DLV::phonon_bands::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<band_structure>(*this);
}

template <class Archive>
void DLV::electron_bands::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<band_structure>(*this);
  ar & spin;
}

template <class Archive>
void DLV::electron_bdos::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<electron_bands>(*this);
  ar & dos;
}

template <class Archive>
void DLV::leed_pattern::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & base;
  ar & supercell;
  ar & show_domains;
}

BOOST_CLASS_VERSION(DLV::plot_multi_grids, 1)
BOOST_CLASS_VERSION(DLV::rod1d_plot, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::dos_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rod1d_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rod2d_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_line)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::k_space_line)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::phonon_dos)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::electron_dos)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::time_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::phonon_bands)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::electron_bands)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::electron_bdos)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::leed_pattern)

DLV_SUPPRESS_TEMPLATES(DLV::data_object)
DLV_SUPPRESS_TEMPLATES(DLV::reloadable_data)

DLV_NORMAL_EXPLICIT(DLV::dos_plot)
DLV_SPLIT_EXPLICIT(DLV::rod1d_plot)
DLV_NORMAL_EXPLICIT(DLV::rod2d_plot)
DLV_NORMAL_EXPLICIT(DLV::real_space_line)
DLV_NORMAL_EXPLICIT(DLV::k_space_line)
DLV_NORMAL_EXPLICIT(DLV::phonon_dos)
DLV_NORMAL_EXPLICIT(DLV::electron_dos)
DLV_NORMAL_EXPLICIT(DLV::time_plot)
DLV_NORMAL_EXPLICIT(DLV::phonon_bands)
DLV_NORMAL_EXPLICIT(DLV::electron_bands)
DLV_NORMAL_EXPLICIT(DLV::electron_bdos)
DLV_NORMAL_EXPLICIT(DLV::leed_pattern)

#  ifdef ENABLE_DLV_GRAPHICS

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit1D_dos *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit1D_dos(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit1D_bands *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit1D_bands(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit1D_bdos *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit1D_bdos(0, 0, 0, "");
    }
  }
}

template <class Archive>
void DLV::edit1D_object::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_object>(*this);
}

template <class Archive>
void DLV::edit1D_dos::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit1D_object>(*this);
}

template <class Archive>
void DLV::edit1D_bands::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit1D_object>(*this);
}

template <class Archive>
void DLV::edit1D_bdos::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit1D_object>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit1D_dos)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit1D_bands)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit1D_bdos)

DLV_SUPPRESS_TEMPLATES(DLV::edit_object)

DLV_NORMAL_EXPLICIT(DLV::edit1D_dos)
DLV_NORMAL_EXPLICIT(DLV::edit1D_bands)
DLV_NORMAL_EXPLICIT(DLV::edit1D_bdos)

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
