
#ifndef DLV_PLOT_DATA
#define DLV_PLOT_DATA

namespace DLV {

  class model;

  class line : public data_object {
  public:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update1D(const int_g method, const int_g h, const int_g k,
			    const int_g l, const real_g x, const real_g y,
			    const real_g z, const int_g obj, const bool conv,
			    const bool frac, const bool parent,
			    class model *const m, char message[],
			    const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    line(const char label[]);

    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

    bool is_line() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    real_g startp[3];
    real_g endp[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_space_line : public line {
  public:
    real_space_line(const char label[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<line>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class k_space_line : public line {
  public:
    k_space_line(const char label[]);
    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class plot_data : public reloadable_data {
  public:
    ~plot_data();

    void set_grid(real_g *data, const int_g n, const bool copy_data = true);
    void add_plot(real_g *points, const int_g n, const bool copy_data = true);
    void add_plot(real_g *points, const int_g n, const char label[],
		  const bool copy_data = true);
    void update_plot(real_g *points, const int_g n);
    void set_plot_label(const char label[], const int_g index);

    void set_xaxis_label(const char label[]);
    void set_xunits(const char label[]);
    void set_yaxis_label(const char label[]);
    void set_yunits(const char label[]);

    void set_xpoints(real_g *points, const int_g np,
		     const bool copy_data = true);
    void set_xlabel(const char label[], const int_g index);
    void set_title(const char label[]);

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    // for edit
    int_g get_ngraphs() const;
    string get_xlabel() const;
    string get_ylabel() const;

  protected:
    plot_data(const char code[], const string src,
	      operation *p, const int_g nplots, const bool show = true);

    real_g *get_grid() const;
    int_g get_npoints() const;
    void set_npoints(const int_g n);
    real_g **get_data() const;
    string *get_labels() const;
    const real_g *get_xrange() const;
    const real_g *get_yrange() const;
    void set_xrange(const real_g x[2]);
    void set_yrange(const real_g y[2]);
    real_g *get_xpoints() const;
    string *get_xlabels() const;
    int_g get_nxpoints() const;
#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
#endif // ENABLE_DLV_GRAPHICS

  private:
    string title;
    int_g ngraphs;
    real_g **plots;
    string *labels;
    real_g *grid;
    int_g npoints;
    real_g xrange[2];
    real_g yrange[2];
    string x_axis_label;
    string y_axis_label;
    real_g *xpoints;
    string *xlabels;
    int_g num_xpoints;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class plot_multi_grids : public plot_data {
  public:
    ~plot_multi_grids();

    void set_grid(real_g *data, const int_g n, const bool copy_data = true);
    bool add_plot(real_g *points, const int_g n, const int_g ng,
		  const bool copy_data = true);
    void add_plot(real_g *points, const int_g n, const int_g ng,
		  const char label[], const bool copy_data = true);
    plot_multi_grids(const char code[], const string src,
		     operation *p, const int_g nplots,
		     const int_g ngds, const int_g max_g_size,
		     const bool show = true);
    bool add_grid(real_g *data, const int_g npts, const int_g ng);
    void unload_data();
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;

  protected:
    real_g **get_multi_grids() const;
    int_g get_ngrids() const;
    int_g *get_select_grid() const;
    int_g *get_grid_sizes() const;

  private:
    int_g ngrids;
    int_g *grid_sizes;
    real_g **multi_grids;
    int_g *select_grid;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class dos_plot : public plot_data {
  public:
    dos_plot(const char code[], const string src,
	     operation *p, const int_g nplots);

    void unload_data();

    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    friend class electron_bdos;
    virtual bool has_spin() const;
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_dos : public dos_plot {
  public:
    phonon_dos(const char code[], const string src,
	       operation *p, const int_g nplots);

    string get_data_label() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<dos_plot>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class electron_dos : public dos_plot {
  public:
    electron_dos(const char code[], const string src, operation *p,
		 const int_g nplots, const bool s);

    string get_data_label() const;
    bool is_editable() const;
    int_g get_edit_type() const;
    void set_spin();

#ifdef ENABLE_DLV_GRAPHICS
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    bool has_spin() const;

  private:
    bool spin;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class time_plot : public plot_data {
  public:
    time_plot(const char code[], const string src,
	      operation *p, const int_g nplots);

    void unload_data();

    string get_data_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rod1d_plot : public plot_multi_grids {
  public:
    ~rod1d_plot();

    rod1d_plot(const char code[], const string src,
	       operation *p, const int_g nplots,
	       const int_g ngds, const int_g max_g_size,
	       const bool show = true);
    void unload_data();
    //    string get_data_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    string get_data_label() const;
#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type transfer_and_render(class data_object *old_data,
				       const render_parent *parent,
				       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    //real_g *grid2;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class rod2d_plot : public plot_data {
  public:
    rod2d_plot(const char code[], const string src,
	       operation *p, const int_g nplots, const bool show = true);
    void unload_data();
    //    string get_data_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    string get_data_label() const;
#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type transfer_and_render(class data_object *old_data,
				       const render_parent *parent,
				       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - is this a special type?
  class panel_plot : public plot_data {
  public:
    ~panel_plot();

    void set_ypoints(real_g *points, const int_g np,
		     const bool copy_data = true);
    void set_ylabel(const char label[], const int_g index);

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    virtual bool has_spin() const;

  protected:
    panel_plot(const char code[], const string src,
	       operation *p, const int_g nplots);

    real_g *get_ypoints() const;
    string *get_ylabels() const;
    int_g get_nypoints() const;

  private:
    real_g *ypoints;
    string *ylabels;
    int_g num_ypoints;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class band_structure : public panel_plot {
  public:
    void set_kpoints(const real_g kp[][3], const bool copy_data = true);

    void unload_data();

    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;

  protected:
    band_structure(const char code[], const string src, operation *p,
		   const int_g nplots, const int_g nk);

  private:
    int_g n_k_points;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_bands : public band_structure {
  public:
    phonon_bands(const char code[], const string src, operation *p,
		 const int_g nplots, const int_g nk);

    string get_data_label() const;

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class electron_bands : public band_structure {
  public:
    electron_bands(const char code[], const string src, operation *p,
		   const int_g nplots, const int_g nk, const bool s);

    string get_data_label() const;
    bool is_editable() const;
    int_g get_edit_type() const;

#ifdef ENABLE_DLV_GRAPHICS
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    bool has_spin() const;
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    bool spin;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - use virtual inheritence instead?
  class electron_bdos : public electron_bands {
  public:
    electron_bdos(const char code[], const string src, operation *p,
		  const int_g nplots, const int_g nk,
		  const bool s, dos_plot *d);

    string get_data_label() const;
    bool is_editable() const;
    int_g get_edit_type() const;
    dos_plot *get_dos();

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    dos_plot *dos;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class leed_pattern : public data_object {
  public:
    leed_pattern(class model *m, const int_g s[2][2]);

  protected:
    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_leed(const bool d, model *const m,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    model *base;
    int_g supercell[2][2];
    bool show_domains;

    void create(model *m, model *bm, const int_g scell[2][2],
		const bool show, real_g (* &points)[2], int_g &npoints,
		real_g (* &domains)[4], int_g &ndomains, int_g * &colours);
    static void find_glidelines(const real_l rotations[][3][3],
				const real_l translations[][3],
				const int_g nops, const real_l cell[3][3],
				bool &glidex, bool &glidey);
    static void calc(const real_l lattice[3][3],
		     const real_l rotations[][3][3],
		     const real_l translations[][3], const int_g nops,
		     const real_l scell_rotate[][3][3],
		     const real_l scell_trans[][3], const int_g sops,
		     const int_g supercell[2][2], real_l base_domain[2][2],
		     real_l (* &scell_domains)[2][2], int_g &ndomains);
    static void k_lat(const real_l lattice[][3][3], real_l k_lattice[][2][2],
		      const int_g ndomains);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS

  class edit1D_object : public edit_object {
  public:
    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

  protected:
    edit1D_object(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit1D_dos : public edit1D_object {
  public:
    static edit1D_dos *create(const render_parent *parent,
			      data_object *data, const drawable_obj *obj,
			      const bool kspace, const string name,
			      const int_g index, char message[],
			      const int_g mlen);

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_shift(const real_g shift,
				char message[], const int_g mlen);

    // Public for serialization
    edit1D_dos(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit1D_bands : public edit1D_object {
  public:
    static edit1D_bands *create(const render_parent *parent,
				data_object *data, const drawable_obj *obj,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_shift(const real_g shift,
				char message[], const int_g mlen);

    // Public for serialization
    edit1D_bands(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit1D_bdos : public edit1D_object {
  public:
    static edit1D_bdos *create(const render_parent *parent,
			       data_object *data, const drawable_obj *obj,
			       const bool kspace, const string name,
			       const int_g index, char message[],
			       const int_g mlen);

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_shift(const real_g shift,
				char message[], const int_g mlen);

    // Public for serialization
    edit1D_bdos(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#endif // ENABLE_DLV_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE

BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::plot_data)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::plot_multi_grids)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::band_structure)
BOOST_CLASS_EXPORT_KEY(DLV::dos_plot)
BOOST_CLASS_EXPORT_KEY(DLV::rod1d_plot)
BOOST_CLASS_EXPORT_KEY(DLV::rod2d_plot)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_line)
BOOST_CLASS_EXPORT_KEY(DLV::k_space_line)
BOOST_CLASS_EXPORT_KEY(DLV::phonon_dos)
BOOST_CLASS_EXPORT_KEY(DLV::electron_dos)
BOOST_CLASS_EXPORT_KEY(DLV::time_plot)
BOOST_CLASS_EXPORT_KEY(DLV::phonon_bands)
BOOST_CLASS_EXPORT_KEY(DLV::electron_bands)
BOOST_CLASS_EXPORT_KEY(DLV::electron_bdos)
BOOST_CLASS_EXPORT_KEY(DLV::leed_pattern)

#  ifdef ENABLE_DLV_GRAPHICS
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edit1D_object)
BOOST_CLASS_EXPORT_KEY(DLV::edit1D_dos)
BOOST_CLASS_EXPORT_KEY(DLV::edit1D_bands)
BOOST_CLASS_EXPORT_KEY(DLV::edit1D_bdos)
#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE

inline DLV::line::line(const char label[])
  : data_object("DLV", "user"), name(label)
{
  startp[0] = 0.0;
  startp[1] = 0.0;
  startp[2] = 0.0;
  endp[0] = 1.0;
  endp[1] = 0.0;
  endp[2] = 0.0;
}

inline DLV::real_space_line::real_space_line(const char label[]) : line(label)
{
}

inline DLV::k_space_line::k_space_line(const char label[]) : line(label)
{
}

inline DLV::plot_data::plot_data(const char code[], const string src,
				 operation *p, const int_g nplots,
				 const bool show)
  : reloadable_data(code, src, p, show), ngraphs(nplots), plots(0), labels(0),
    grid(0), npoints(0), xpoints(0), xlabels(0), num_xpoints(0)
{
  yrange[0] = 1e10;
  yrange[1] = -1e10;
}

inline DLV::plot_multi_grids::plot_multi_grids(const char code[],
					       const string src, operation *p,
					       const int_g nplots,
					       const int_g ngds,
					       const int_g max_g_size,
					       const bool show)
  : plot_data(code, src, p, nplots, show), ngrids(ngds), grid_sizes(0),
    multi_grids(0), select_grid(0)
{
  set_npoints(max_g_size);
}

inline DLV::dos_plot::dos_plot(const char code[], const string src,
			       operation *p, const int_g nplots)
  : plot_data(code, src, p, nplots)
{
}

inline DLV::phonon_dos::phonon_dos(const char code[], const string src,
				   operation *p, const int_g nplots)
  : dos_plot(code, src, p, nplots)
{
}

inline DLV::electron_dos::electron_dos(const char code[], const string src,
				       operation *p, const int_g nplots,
				       const bool s)
  : dos_plot(code, src, p, nplots), spin(s)
{
}

inline DLV::time_plot::time_plot(const char code[], const string src,
				 operation *p, const int_g nplots)
  : plot_data(code, src, p, nplots)
{
}

inline DLV::rod1d_plot::rod1d_plot(const char code[], const string src,
				 operation *p, const int_g nplots,
				 const int_g ngds, const int_g max_g_size,
				 const bool show)
  : plot_multi_grids(code, src, p, nplots, ngds, max_g_size, show)
{
}

inline DLV::rod2d_plot::rod2d_plot(const char code[], const string src,
				   operation *p, const int_g nplots,
				   const bool show)
  : plot_data(code, src, p, nplots, show)
{
}

inline DLV::panel_plot::panel_plot(const char code[], const string src,
				   operation *p, const int_g nplots)
  : plot_data(code, src, p, nplots), ypoints(0), ylabels(0), num_ypoints(0)
{
}

inline DLV::band_structure::band_structure(const char code[], const string src,
					   operation *p, const int_g nplots,
					   const int_g nk)
  : panel_plot(code, src, p, nplots), n_k_points(nk)
{
}

inline DLV::phonon_bands::phonon_bands(const char code[], const string src,
				       operation *p, const int_g nplots,
				       const int_g nk)
  : band_structure(code, src, p, nplots, nk)
{
}

inline DLV::electron_bands::electron_bands(const char code[], const string src,
					   operation *p, const int_g nplots,
					   const int_g nk, const bool s)
  : band_structure(code, src, p, nplots, nk), spin(s)
{
}

inline DLV::int_g DLV::plot_data::get_ngraphs() const
{
  return ngraphs;
}

inline DLV::real_g *DLV::plot_data::get_grid() const
{
  return grid;
}

inline DLV::int_g DLV::plot_data::get_npoints() const
{
  return npoints;
}

inline void DLV::plot_data::set_npoints(const int_g n)
{
  npoints = n;
}

inline DLV::real_g **DLV::plot_data::get_data() const
{
  return plots;
}

inline DLV::string *DLV::plot_data::get_labels() const
{
  return labels;
}

inline const DLV::real_g *DLV::plot_data::get_xrange() const
{
  return xrange;
}

inline const DLV::real_g *DLV::plot_data::get_yrange() const
{
  return yrange;
}

inline void DLV::plot_data::set_xrange(const real_g x[2])
{
  xrange[0] = x[0];
  xrange[1] = x[1];
}

inline void DLV::plot_data::set_yrange(const real_g y[2])
{
  yrange[0] = y[0];
  yrange[1] = y[1];
}

inline DLV::string DLV::plot_data::get_xlabel() const
{
  return x_axis_label;
}

inline DLV::string DLV::plot_data::get_ylabel() const
{
  return y_axis_label;
}

inline DLV::real_g *DLV::plot_data::get_xpoints() const
{
  return xpoints;
}

inline DLV::string *DLV::plot_data::get_xlabels() const
{
  return xlabels;
}

inline DLV::int_g DLV::plot_data::get_nxpoints() const
{
  return num_xpoints;
}

inline DLV::real_g **DLV::plot_multi_grids::get_multi_grids() const
{
  return multi_grids;
}

inline DLV::int_g DLV::plot_multi_grids::get_ngrids() const
{
  return ngrids;
}

inline DLV::int_g *DLV::plot_multi_grids::get_select_grid() const
{
  return select_grid;
}

inline DLV::int_g *DLV::plot_multi_grids::get_grid_sizes() const
{
  return grid_sizes;
}

inline DLV::real_g *DLV::panel_plot::get_ypoints() const
{
  return ypoints;
}

inline DLV::string *DLV::panel_plot::get_ylabels() const
{
  return ylabels;
}

inline DLV::int_g DLV::panel_plot::get_nypoints() const
{
  return num_ypoints;
}

inline void DLV::electron_dos::set_spin()
{
  spin = true;
}

inline DLV::electron_bdos::electron_bdos(const char code[], const string src,
					 operation *p, const int_g nplots,
					 const int_g nk, const bool s,
					 dos_plot *d)
  : electron_bands(code, src, p, nplots, nk, s), dos(d)
{
}

inline DLV::dos_plot *DLV::electron_bdos::get_dos()
{
  return dos;
}

inline DLV::leed_pattern::leed_pattern(class model *m, const int_g s[2][2])
  : data_object("DLV", "user"), base(m), show_domains(false)
{
  supercell[0][0] = s[0][0];
  supercell[0][1] = s[0][1];
  supercell[1][0] = s[1][0];
  supercell[1][1] = s[1][1];
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::edit1D_object::edit1D_object(data_object *d, edited_obj *e,
					 const int_g i, const string s)
  : edit_object(d, e, i, s)
{
}

inline DLV::edit1D_dos::edit1D_dos(data_object *d, edited_obj *e,
				   const int_g i, const string s)
  : edit1D_object(d, e, i, s)
{
}

inline DLV::edit1D_bands::edit1D_bands(data_object *d, edited_obj *e,
				       const int_g i, const string s)
  : edit1D_object(d, e, i, s)
{
}

inline DLV::edit1D_bdos::edit1D_bdos(data_object *d, edited_obj *e,
				     const int_g i, const string s)
  : edit1D_object(d, e, i, s)
{
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_PLOT_DATA
