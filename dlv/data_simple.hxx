
#ifndef DLV_DATA_SIMPLE
#define DLV_DATA_SIMPLE

namespace DLV {

  class scalar_data : public data_object {
  public:
    void unload_data();

    string get_obj_label() const;
    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

  protected:
    scalar_data(const string code, const string src, const char name[]);

  private:
    string label;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class boolean_data : public scalar_data {
  public:
    boolean_data(const string code, const string src, const char name[],
		 const bool i);

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
    bool is_displayable() const;
    int_g get_display_type() const;
    bool get_value() const;

  private:
    bool value;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class integer_data : public scalar_data {
  public:
    integer_data(const string code, const string src,
		 const char name[], const int_g i);

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
    int_g get_display_type() const;

  private:
    int_g value;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_data : public scalar_data {
  public:
    real_data(const string code, const string src, const char name[],
	      const real_l i, const bool viewable = true);

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
    bool is_displayable() const;
    bool is_real_scalar() const;
    int_g get_display_type() const;
    real_l get_value() const;
    bool get_real_value(real_l &v) const;

  private:
    real_l value;
    bool displayable;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class file_data : public data_object {
  public:
    file_data(const string file, const string code, const string name,
	      const bool bin = false);

    void unload_data();

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    string get_obj_label() const;
    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    string get_filename() const;
    bool is_binary() const;

  private:
    string filename;
    string label;
    bool binary;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class text_file : public data_object {
  public:
    text_file(const string file, const string code, const string src,
	      const string name);

    void unload_data();

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[],
			  const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    string get_filename() const;

  private:
    string filename;
    string label;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class reloadable_data : public data_object {
  public:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
    void set_loaded();

  protected:
    reloadable_data(const string code, const string src, class operation *p,
		    const bool show = true);

#ifdef ENABLE_DLV_GRAPHICS
    virtual DLVreturn_type render_data(const class model *structure,
				       char message[], const int_g mlen) = 0;
    virtual void reset_data_sets() = 0;
    bool reload_data(reloadable_data *data, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    bool loaded;
    class operation *parent_op;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS
  // Todo - is this inheritance correct? - reloadable_data or data_object?
  class edit_object : public data_object {
  public:
    ~edit_object();
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    bool is_edited() const;
    DLVreturn_type select(char message[], const int_g mlen);
    void list_edit_object(const render_parent *parent) const;
    void unload_data();

    bool is_kspace() const;
    bool is_periodic() const;

  protected:
    edit_object(data_object *d, class edited_obj *e, const int_g i,
		const string s);

    data_object *get_base_data() const;
    edited_obj *get_edit_obj();
    bool check_edit_index(const int_g id) const;

  private:
    data_object *parent;
    class edited_obj *edit;
    int_g index;
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };
#endif // ENABLE_DLV_GRAPHICS

  class point : public data_object {
  public:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update0D(const int_g method, const real_g x, const real_g y,
			    const real_g z, const bool conv, model *const m,
			    char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    point(const char label[]);

    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    real_g position[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_space_point : public point {
  public:
    real_space_point(const char label[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class k_space_point : public point {
  public:
    k_space_point(const char label[]);
    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::scalar_data)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::reloadable_data)
BOOST_CLASS_EXPORT_KEY(DLV::boolean_data)
BOOST_CLASS_EXPORT_KEY(DLV::integer_data)
BOOST_CLASS_EXPORT_KEY(DLV::real_data)
BOOST_CLASS_EXPORT_KEY(DLV::file_data)
BOOST_CLASS_EXPORT_KEY(DLV::text_file)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_point)
BOOST_CLASS_EXPORT_KEY(DLV::k_space_point)
#  ifdef ENABLE_DLV_GRAPHICS
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edit_object)
#  endif // ENABLE_DLV_GRAPHICS
#endif // DLV_USES_SERIALIZE

inline void DLV::reloadable_data::set_loaded()
{
  loaded = true;
}

inline DLV::scalar_data::scalar_data(const string code, const string src,
				     const char name[])
  : data_object(code, src), label(name)
{
}

inline DLV::boolean_data::boolean_data(const string code, const string src,
				       const char name[], const bool i)
  : scalar_data(code, src, name), value(i)
{
}

inline DLV::integer_data::integer_data(const string code, const string src,
				       const char name[], const int_g i)
  : scalar_data(code, src, name), value(i)
{
}

inline DLV::real_data::real_data(const string code, const string src,
				 const char name[], const real_l i,
				 const bool viewable)
  : scalar_data(code, src, name), value(i), displayable(viewable)
{
}

inline DLV::file_data::file_data(const string file, const string code,
				 const string name, const bool bin)
  : data_object(code, "internal", false), filename(file), label(name),
    binary(bin)
{
}

inline bool DLV::file_data::is_binary() const
{
  return binary;
}

inline DLV::text_file::text_file(const string file, const string code,
				 const string src, const string name)
  : data_object(code, src), filename(file), label(name)
{
}

inline DLV::string DLV::file_data::get_filename() const
{
  return filename;
}

inline DLV::string DLV::text_file::get_filename() const
{
  return filename;
}

inline bool DLV::boolean_data::get_value() const
{
  return value;
}

inline DLV::real_l DLV::real_data::get_value() const
{
  return value;
}

inline DLV::point::point(const char label[])
  : data_object("DLV", "user"), name(label)
{
  position[0] = 0.0;
  position[1] = 0.0;
  position[2] = 0.0;
}

inline DLV::real_space_point::real_space_point(const char label[])
  : point(label)
{
}

inline DLV::k_space_point::k_space_point(const char label[]) : point(label)
{
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::edit_object::edit_object(data_object *d, class edited_obj *e,
				     const int_g i, const string s)
  : data_object("DLV", "edit"), parent(d), edit(e), index(i), name(s)
{
}

inline DLV::data_object *DLV::edit_object::get_base_data() const
{
  return parent;
}

inline DLV::edited_obj *DLV::edit_object::get_edit_obj()
{
  return edit;
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_DATA_SIMPLE
