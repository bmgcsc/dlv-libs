
#ifndef DLV_SURFACE_DATA
#define DLV_SURFACE_DATA

namespace DLV {

  class plane : public data_object {
  public:
    void get_vertices(real_g o[3], real_g a[3], real_g b[3]) const;

    // public for serialization
    plane(const char label[]);

  protected:
    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

    bool is_plane() const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type use_view_editor(const render_parent *parent,
				   const bool v, char message[],
				   const int_g mlen);
    DLVreturn_type update2D(const int_g method, const int_g h, const int_g k,
			    const int_g l, const real_g x, const real_g y,
			    const real_g z, const int_g obj, const bool conv,
			    const bool frac, const bool parent,
			    class model *const m, char message[],
			    const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    real_g origin[3];
    real_g pointa[3];
    real_g pointb[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_space_plane : public plane {
  public:
    real_space_plane(const char label[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class k_space_plane : public plane {
  public:
    k_space_plane(const char label[]);
    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class surface_data : public reloadable_data {
  public:
    void add_data(real_g *array, const char label[],
		  const bool copy_data = true);
    surface_data(const char code[], const string src, operation *p,
		 const int_g na, const int_g nb, const real_g o[3],
		 const real_g as[3], const real_g bs[3]);
    ~surface_data();

    void unload_data();

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    class drawable_obj *create_drawable(const render_parent *parent,
					char message[], const int_g mlen);
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
    void match_data_math(const data_object *obj, const int_g selection,
			 const bool all, const int_g cmpt, int_g &nmatch,
			 int_g &n) const;
    void list_data_math(const data_object *obj, const bool all,
			const int_g cmpt, string *names, int_g &n) const;
#endif // ENABLE_DLV_GRAPHICS

  protected:
    bool is_kspace() const;
    bool is_periodic() const;
#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
#endif // ENABLE_DLV_GRAPHICS

  private:
    int_g nx;
    int_g ny;
    real_g origin[3];
    real_g a_step[3];
    real_g b_step[3];
    int_g n_data_sets;
    string *labels;
    real_g **data;
    int_g size;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class real_space_surface : public surface_data {
  public:
    real_space_surface(const char code[], const string src, operation *p,
		       const int_g na, const int_g nb, const real_g o[3],
		       const real_g as[3], const real_g bs[3]);

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rspace_periodic_surface : public real_space_surface {
  public:
    rspace_periodic_surface(const char code[], const string src, operation *p,
			    const int_g na, const int_g nb, const real_g o[3],
			    const real_g as[3], const real_g bs[3]);

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_periodic() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS

  class edit2D_object : public edit_object {
  protected:
    edit2D_object(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_ortho : public edit2D_object {
  public:
    static edit2D_ortho *create(const render_parent *parent,
				data_object *data, const drawable_obj *obj,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    // Public for serialization
    edit2D_ortho(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_slice : public edit2D_object {
  public:
    static edit2D_slice *create(const render_parent *parent,
				data_object *data,
				const drawable_obj *obj, const int_g component,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit2D_slice(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_cut : public edit2D_object {
  public:
    static edit2D_cut *create(const render_parent *parent,
			      data_object *data,
			      const drawable_obj *obj, const int_g component,
			      const bool kspace, const string name,
			      const int_g index, char message[],
			      const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit2D_cut(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_clamp : public edit2D_object {
  public:
    static edit2D_clamp *create(const render_parent *parent,
				data_object *data,
				const drawable_obj *obj, const int_g component,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit2D_clamp(data_object *d, edited_obj *e, const int_g i,
		 const string s, const int_g c);

  private:
    int_g component;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_crop : public edit2D_object {
  public:
    static edit2D_crop *create(const render_parent *parent,
			       data_object *data,
			       const drawable_obj *obj, const int_g component,
			       const bool kspace, const string name,
			       const int_g index, char message[],
			       const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit2D_crop(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_extend : public edit2D_object {
  public:
    static edit2D_extend *create(const render_parent *parent,
				 data_object *data,
				 const drawable_obj *obj, const int_g component,
				 const bool kspace, const string name,
				 const int_g index, char message[],
				 const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_extension(const render_parent *parent,
				    const int_g na, const int_g nb,
				    const int_g nc, char message[],
				    const int_g mlen);

    edit2D_extend(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_downsize : public edit2D_object {
  public:
    static edit2D_downsize *create(const render_parent *parent,
				   data_object *data,
				   const drawable_obj *obj,
				   const int_g component, const bool kspace,
				   const string name, const int_g index,
				   char message[], const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit2D_downsize(data_object *d, edited_obj *e, const int_g i,
		    const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit2D_math : public edit2D_object {
  public:
    static edit2D_math *create(const render_parent *parent,
			       data_object *data,
			       const drawable_obj *obj, const int_g component,
			       const bool kspace, const string name,
			       const int_g index, char message[],
			       const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    void data_math_parent(const data_object * &obj, int_g &component) const;
    DLVreturn_type update_data_math(const render_parent *parent,
				    data_object *obj, const int_g cmpt,
				    const int_g index, char message[],
				    const int_g mlen);

    edit2D_math(data_object *d, const int_g component,
		edited_obj *e, const int_g i, const string s);

  private:
    int_g obj_component;
    data_object *obj1;
    data_object *obj2;
    data_object *obj3;
    int_g component1;
    int_g component2;
    int_g component3;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
#endif // ENABLE_DLV_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE
#  ifdef ENABLE_DLV_GRAPHICS
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::surface_data)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edit2D_object)
#  endif // ENABLE_DLV_GRAPHICS
BOOST_CLASS_EXPORT_KEY(DLV::plane)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_plane)
BOOST_CLASS_EXPORT_KEY(DLV::k_space_plane)
BOOST_CLASS_EXPORT_KEY(DLV::surface_data)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_surface)
BOOST_CLASS_EXPORT_KEY(DLV::rspace_periodic_surface)

#  ifdef ENABLE_DLV_GRAPHICS
//BOOST_CLASS_EXPORT_KEY(DLV::edit2D_object)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_ortho)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_slice)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_cut)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_clamp)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_crop)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_extend)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_downsize)
BOOST_CLASS_EXPORT_KEY(DLV::edit2D_math)
#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE

inline DLV::plane::plane(const char label[])
  : data_object("DLV", "user"), name(label)
{
  origin[0] = 0.0;
  origin[1] = 0.0;
  origin[2] = 0.0;
  pointa[0] = 1.0;
  pointa[1] = 0.0;
  pointa[2] = 0.0;
  pointb[0] = 0.0;
  pointb[1] = 1.0;
  pointb[2] = 0.0;
}

inline DLV::real_space_plane::real_space_plane(const char label[])
  : plane(label)
{
}

inline DLV::k_space_plane::k_space_plane(const char label[]) : plane(label)
{
}

inline
DLV::real_space_surface::real_space_surface(const char code[],
					    const string src, operation *p,
					    const int_g na, const int_g nb,
					    const real_g o[3],
					    const real_g as[3],
					    const real_g bs[3])
  : surface_data(code, src, p, na, nb, o, as, bs)
{
}

inline
DLV::rspace_periodic_surface::rspace_periodic_surface(const char code[],
						      const string src,
						      operation *p,
						      const int_g na,
						      const int_g nb,
						      const real_g o[3],
						      const real_g as[3],
						      const real_g bs[3])
  : real_space_surface(code, src, p, na, nb, o, as, bs)
{
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::edit2D_object::edit2D_object(data_object *d, edited_obj *e,
					 const int_g i, const string s)
  : edit_object(d, e, i, s)
{
}

inline DLV::edit2D_ortho::edit2D_ortho(data_object *d, edited_obj *e,
				       const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_slice::edit2D_slice(data_object *d, edited_obj *e,
				       const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_cut::edit2D_cut(data_object *d, edited_obj *e,
				   const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_clamp::edit2D_clamp(data_object *d, edited_obj *e,
				       const int_g i, const string s,
				       const int_g c)
  : edit2D_object(d, e, i, s), component(c)
{
}

inline DLV::edit2D_crop::edit2D_crop(data_object *d, edited_obj *e,
				     const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_extend::edit2D_extend(data_object *d, edited_obj *e,
					 const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_downsize::edit2D_downsize(data_object *d, edited_obj *e,
					     const int_g i, const string s)
  : edit2D_object(d, e, i, s)
{
}

inline DLV::edit2D_math::edit2D_math(data_object *d, const int_g component,
				     edited_obj *e, const int_g i,
				     const string s)
  : edit2D_object(d, e, i, s), obj_component(component)
{
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_SURFACE_DATA
