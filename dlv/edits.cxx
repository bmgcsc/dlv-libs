
#include <list>
#include <map>
#include <cmath>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#  include "../graphics/display_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "math_fns.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "atom_prefs.hxx"
#include "data_objs.hxx"
//#include "data_vol.hxx" // wulff_plot
#include "model.hxx"
#include "outline.hxx" // fill_geometry
#include "model_atoms.hxx"
#include "operation.hxx"
#include "extern_model.hxx"

void DLV::model_base::copy_cartesian_atoms(const model_base *m)
{
  std::map<int, atom>::const_iterator x;
  for (x = m->atom_basis.begin(); x != m->atom_basis.end(); ++x ) {
    atom p(new atom_type());
    // copy all data
    *p = *(x->second);
    p->clear_edit_flag();
    //p->set_atomic_number(x->get_atomic_number);
    // Do this to invalidate fractional coords
    coord_type c[3];
    x->second->get_cartesian_coords(c);
    p->set_cartesian_coords(c);
    atom_basis[x->second->get_id()] = p;
  }
}

void DLV::model_base::copy_cartesian_atoms(const model_base *m,
					   atom *parents)
{
  int_g count = 0;
  std::map<int, atom>::const_iterator x;
  for (x = m->atom_basis.begin(); x != m->atom_basis.end(); ++x ) {
    atom p(new atom_type());
    // copy all data
    *p = *(x->second);
    p->clear_edit_flag();
    //p->set_atomic_number(x->get_atomic_number);
    // Do this to invalidate fractional coords
    coord_type c[3];
    x->second->get_cartesian_coords(c);
    p->set_cartesian_coords(c);
    atom_basis[x->second->get_id()] = p;
    parents[count] = p;
    count++;
  }
}

void DLV::model_base::copy_fractional_atoms(const model_base *m)
{
  std::map<int, atom>::const_iterator x;
  for (x = m->atom_basis.begin(); x != m->atom_basis.end(); ++x ) {
    atom p(new atom_type());
    // copy all data
    *p = *(x->second);
    p->clear_edit_flag();
    //p->set_atomic_number(x->get_atomic_number);
    // Do this to invalidate cartesian coords
    coord_type c[3];
    x->second->get_fractional_coords(c);
    p->set_fractional_coords(c);
    atom_basis[x->second->get_id()] = p;
  }
}

void DLV::model_base::reset_fractional_atoms(const model_base *m)
{
  std::map<int, atom>::const_iterator x;
  std::map<int, atom>::iterator ptr = atom_basis.begin();
  for (x = m->atom_basis.begin(); x != m->atom_basis.end(); ++x, ++ptr ) {
    // Do this to invalidate cartesian coords
    coord_type c[3];
    x->second->get_fractional_coords(c);
    ptr->second->set_fractional_coords(c);
  }
}

void DLV::model_base::shift_cartesian_atoms(const model_base *m,
					    const real_l z_shift)
{
  std::map<int, atom>::const_iterator p = atom_basis.begin();
  std::map<int, atom>::const_iterator x;
  for (x = m->atom_basis.begin(); x != m->atom_basis.end(); ++x ) {
    coord_type c[3];
    x->second->get_cartesian_coords(c);
    c[2] += z_shift;
    p->second->set_cartesian_coords(c);
    ++p;
  }
}

void DLV::model_base::shift_cartesian_atoms(const model_base *m,
					    const real_l x, const real_l y,
					    const real_l z)
{
  std::map<int, atom>::const_iterator p = atom_basis.begin();
  std::map<int, atom>::const_iterator ptr;
  for (ptr = m->atom_basis.begin(); ptr != m->atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    c[0] += x;
    c[1] += y;
    c[2] += z;
    p->second->set_cartesian_coords(c);
    ++p;
  }
}

void DLV::model_base::shift_cartesian_atoms(const real_l x, const real_l y,
					    const real_l z)
{
  std::map<int, atom>::const_iterator p;
  for (p = atom_basis.begin(); p != atom_basis.end(); ++p ) {
    coord_type c[3];
    p->second->get_cartesian_coords(c);
    c[0] += x;
    c[1] += y;
    c[2] += z;
    p->second->set_cartesian_coords(c);
  }
}

void DLV::model_base::shift_fractional_atoms(const model_base *m,
					     const real_l x, const real_l y,
					     const real_l z)
{
  std::map<int, atom>::const_iterator p = atom_basis.begin();
  std::map<int, atom>::const_iterator ptr;
  for (ptr = m->atom_basis.begin(); ptr != m->atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_fractional_coords(c);
    c[0] += x;
    c[1] += y;
    c[2] += z;
    p->second->set_fractional_coords(c);
    ++p;
  }
}

void DLV::model_base::rotate_cartesian_atoms(const model_base *m,
					     const real_l r[3][3])
{
  std::map<int, atom>::const_iterator p = atom_basis.begin();
  std::map<int, atom>::const_iterator ptr;
  for (ptr = m->atom_basis.begin(); ptr != m->atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    coord_type nc[3] = { 0.0, 0.0, 0.0 };
    for (int_g i = 0; i < 3; i++)
      for (int_g j = 0; j < 3; j++)
	nc[i] += r[i][j] * c[j];
    p->second->set_cartesian_coords(nc);
    ++p;
  }
}

void DLV::model_base::centre_slab_atoms(const coord_type vc[3],
					real_l shift[3])
{
  std::map<int, atom>::const_iterator ptr;
  coord_type z_min = 1e10;
  coord_type z_max = -1e10;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    if (z_min > c[2])
      z_min = c[2];
    if (z_max < c[2])
      z_max = c[2];
  }
  real_l scale = (z_max + z_min) / 2.0;
  shift[0] = vc[0] * scale / vc[2];
  shift[1] = vc[1] * scale / vc[2];
  shift[2] = scale;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    c[0] -= shift[0];
    c[1] -= shift[1];
    c[2] -= shift[2];
    ptr->second->set_cartesian_coords(c);
  }
}

void DLV::model_base::shift_surface_atoms(const coord_type vc[3],
					  real_l shift[3])
{
  std::map<int, atom>::const_iterator ptr;
  //coord_type z_min = 1e10;
  coord_type z_max = -1e10;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    //if (z_min > c[2])
    // z_min = c[2];
    if (z_max < c[2])
      z_max = c[2];
  }
  //real_l shift = z_max;
  shift[0] = vc[0] * z_max / vc[2];
  shift[1] = vc[1] * z_max / vc[2];
  shift[2] = z_max;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    c[0] -= shift[0];
    c[1] -= shift[1];
    c[2] -= shift[2];
    ptr->second->set_cartesian_coords(c);
  }
}

void DLV::model_base::build_bravais_lattice_from_slab(const int_g, const int_g)
{
  // Not a crystal - BUG
}

void DLV::model_base::copy_model(const model *m)
{
  const model_base *sym = static_cast<const model_base *>(m);
  copy_cartesian_atoms(sym);
  bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
  group_names = sym->group_names;
  copy_lattice(sym);
  copy_symmetry_ops(sym, false);
  copy_bravais_lattice(sym);
  complete(false);
}

DLV::model *DLV::model_base::duplicate_model(const string label) const
{
  model_base *m = duplicate(label);
  m->copy_cartesian_atoms(this);
  m->bonds.copy_pairs(m->atom_basis, bonds, atom_basis);
  m->group_names = group_names;
  m->copy_lattice(this);
  m->copy_symmetry_ops(this, false);
  m->copy_bravais_lattice(this);
  m->complete(false);
  return m;
}

void DLV::model_base::update_model(const model *m)
{
  const model_base *sym = static_cast<const model_base *>(m);
  atom_basis.clear();
  copy_cartesian_atoms(sym);
  bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
  group_names = sym->group_names;
  copy_lattice(sym);
  copy_symmetry_ops(sym, false);
  copy_bravais_lattice(sym);
  complete(false);
}

bool DLV::model_base::supercell(const model *m, const int_g matrix[3][3],
				const bool conv_cell)
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    copy_cartesian_atoms(sym);
    bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
    group_names = sym->group_names;
    redraw = false;
  }
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conv_cell)
    sym->get_conventional_lattice(a, b, c);
  else
    sym->get_primitive_lattice(a, b, c);
  coord_type conv[3][3];
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      conv[i][j] = real_l(matrix[i][0]) * a[j] + real_l(matrix[i][1]) * b[j]
	+ real_l(matrix[i][2]) * c[j];
  coord_type va[3];
  coord_type vb[3];
  coord_type vc[3];
  for (int_g i = 0; i < 3; i++) {
    va[i] = conv[0][i];
    vb[i] = conv[1][i];
    vc[i] = conv[2][i];
  }
  set_primitive_lattice(va, vb, vc);
  build_supercell_symmetry(m, a, b, c, va, vb, vc, conv_cell);
  complete(false);
  identify_bravais_lattice(sym->get_lattice_type(),
			   sym->get_lattice_centring());
  return redraw;
}

bool DLV::model_base::delete_symmetry(const model *m)
{
  const model_base *sym = static_cast<const model_base *>(m);
  group_names = sym->group_names;
  copy_lattice(sym);
  copy_bravais_lattice(sym);
  sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds, sym->bonds);
  complete(false);
  return false;
}

bool DLV::model_base::view_to_molecule(const model *m)
{
  const model_base *sym = static_cast<const model_base *>(m);
  group_names = sym->group_names;
  // no lattice
  atom *parents = 0;
  real_l shift[3] = { 0.0, 0.0, 0.0 };
  sym->display_atoms.copy_atoms_and_bonds(atom_basis, bonds, sym->bonds,
					  parents, shift);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  return true;
}

bool DLV::model_base::cell_to_molecule(const model *m, const int_g na,
				       const int_g nb, const int_g nc,
				       const bool conv, const bool centred,
				       const bool edges)
{
  const model_base *sym = static_cast<const model_base *>(m);
  group_names = sym->group_names;
  add_atom_group("Cluster shells");
  // from generate_atom_display_list
  bool centre[3] = { false, false, false };
  if (centred) {
    centre[0] = (na % 2) == 1;
    centre[1] = (nb % 2) == 1;
    centre[2] = (nc % 2) == 1;
  }
  atom_tree tree = atom_tree();
  if (conv)
    sym->conventional_atoms(primitive_atoms, tree, centre);
  else {
    if (centre[0] or centre[1] or centre[2])
      sym->centre_atoms(sym->primitive_atoms, tree, centre);
    else
      sym->primitive_atoms.copy(tree);
  }
  sym->update_display_atoms(tree, 0.0001, na, nb, nc, conv, centred, edges);
  atom *parents = 0;
  //real_l shift[3] = { 0.0, 0.0, 0.0 };
  //tree.copy_atoms_and_bonds(atom_basis, bonds, sym->bonds,
  //			    parents, shift);
  tree.build_cluster_atoms_and_bonds(atom_basis, bonds, sym->bonds, parents,
				     tree.size(), "Cluster shells", 0.0, 0.0, 0.0);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  return true;
}

bool DLV::model_base::update_lattice_parameters(const model *m,
						const coord_type params[6])
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    copy_fractional_atoms(sym);
    bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
    copy_bravais_lattice(sym);
    group_names = sym->group_names;
    redraw = false;
  }
  set_lattice_parameters(params[0], params[1], params[2], params[3],
			 params[4], params[5]);
  reset_fractional_atoms(sym);
  copy_symmetry_ops(sym, true);
  complete(false);
  return redraw;
}

bool DLV::model_base::update_origin(const model *m, const real_l x,
				    const real_l y, const real_l z,
				    const bool frac)
{
  bool redraw = true;
  const model_base *sym = static_cast<const model_base *>(m);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  sym->get_primitive_lattice(a, b, c);
  if (atom_basis.size() == 0) {
    set_primitive_lattice(a, b, c);
    copy_bravais_lattice(sym);
    group_names = sym->group_names;
    redraw = false;
  }
  atom_basis.clear();
  real_l shift[3] = { 0.0, 0.0, 0.0 };
  if (frac) {
    int_g dim = get_number_of_periodic_dims();
    for (int_g i = 0; i < 3; i++)
      shift[i] -= a[i] * x;
    if (dim == 1) {
      shift[1] -= y;
      shift[2] -= z;
    } else {
      for (int_g i = 0; i < 3; i++)
	shift[i] -= b[i] * y;
      if (dim == 2)
	shift[2] -= z;
      else {
	for (int_g i = 0; i < 3; i++)
	  shift[i] -= c[i] * z;
      }
    }
  } else {
    shift[0] = -x;
    shift[1] = -y;
    shift[2] = -z;
  }
  atom *parents = 0;
  sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds, sym->bonds,
					    parents, shift);
  copy_symmetry_ops(sym, true);
  if (frac) {
    //shift_fractional_atoms(sym, -x, -y, -z);
    shift_fractional_operators(sym, x, y, z, a, b, c);
  } else {
    //shift_cartesian_atoms(sym, -x, -y, -z);
    shift_cartesian_operators(sym, x, y, z, a, b, c);
  }
  complete_translators(a, b, c);
  check_symmetry(parents, a, b, c);
  delete [] parents;
  complete(false);
  return redraw;;
}

bool DLV::model_base::update_origin_symm(const model *m, real_l &x, real_l &y,
					 real_l &z, const bool frac)
{
  bool redraw = true;
  coord_type p[3];
  const model_base *sym = static_cast<const model_base *>(m);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  sym->get_primitive_lattice(a, b, c);
  if (sym->get_min_trans_origin(p, a, b, c,
				sym->get_number_of_periodic_dims())) {
    sym->get_primitive_lattice(a, b, c);
    if (atom_basis.size() == 0) {
      set_primitive_lattice(a, b, c);
      //copy_symmetry_ops(sym, true);
      copy_bravais_lattice(sym);
      redraw = false;
    }
    atom_basis.clear();
    real_l shift[3] = { 0.0, 0.0, 0.0 };
    shift[0] = -p[0];
    shift[1] = -p[1];
    shift[2] = -p[2];
    atom *parents = 0;
    sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds,
					      sym->bonds, parents, shift);
    copy_symmetry_ops(sym, true);
    shift_cartesian_operators(sym, p[0], p[1], p[2], a, b, c);
    complete_translators(a, b, c);
    check_symmetry(parents, a, b, c);
    delete [] parents;
    x = p[0];
    y = p[1];
    z = p[2];
    if (frac)
      cartesian_to_fractional_coords(x, y, z);
    complete(false);
  }
  return redraw;;
}

bool DLV::model_base::update_vacuum(const model *m, const real_l c,
				    const real_l o)
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    copy_cartesian_atoms(sym);
    bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
    group_names = sym->group_names;
    //copy_symmetry_ops(sym, false);
    build_bravais_lattice_from_slab(sym->get_lattice_type(),
				    sym->get_lattice_centring());
    redraw = false;
  }
  // only sym ops with a z reflection change - they acquire a translator
  int_g nops = sym->get_number_of_sym_ops();
  real_l (*r)[3][3] = new_local_array3(real_l, nops, 3, 3);
  sym->get_cart_rotation_operators(r, nops);
  real_l (*t)[3] = new_local_array2(real_l, nops, 3);
  sym->get_cart_translation_operators(t, nops);
  const real_l tol = 0.0001;
  for (int_g i = 0; i < nops; i++) {
    if ((r[i][2][2] > real_l(-1.0) - tol) and (r[i][2][2] < real_l(-1.0) + tol))
      t[i][2] = real_l(2.0) * o;
  }
  set_cartesian_sym_ops(r, t, nops);
  delete_local_array(t);
  delete_local_array(r);
  coord_type va[3];
  coord_type vb[3];
  coord_type vc[3];
  sym->get_primitive_lattice(va, vb, vc);
  vc[0] = 0.0;
  vc[1] = 0.0;
  vc[2] = c;
  set_primitive_lattice(va, vb, vc);
  shift_cartesian_atoms(sym, o);
  complete(false);
  return redraw;
}

bool DLV::model_base::update_cluster(model *m, const int_g n)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = (atom_basis.size() != 0);
  if (!redraw) {
    group_names = sym->group_names;
    add_atom_group("Cluster shells");
  }
  atom_basis.clear();
  // Try to guess how many cells we need for the number of shells
  // Assume number of shells is number of asym atoms
  // * 2 for cells centred both sides of origin
  int_g na = 2 * ((n / (int)sym->atom_basis.size()) + 1) + 2;
  int_g nb = na;
  int_g nc = na;
  atom_tree cluster;
  bool centre[3] = { true, true, true };
  sym->generate_primitive_atom_display(cluster, centre);
  sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
  // Todo - shift to cell 0, may be safest for na/nb/nc range
  // Use something like atom_position.set_coords() to shift
  real_l pos[3];
  sym->display_atoms.get_average_selection_position(pos);
  atom *parents = 0;
  cluster.build_cluster_atoms_and_bonds(atom_basis, bonds, sym->bonds, parents,
					n, "Cluster shells",
					pos[0], pos[1], pos[2]);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  return redraw;
}

bool DLV::model_base::update_cluster(model *m, const int_g n, const real_l x,
				     const real_l y, const real_l z)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = (atom_basis.size() != 0);
  if (!redraw) {
    group_names = sym->group_names;
    add_atom_group("Cluster shells");
  }
  atom_basis.clear();
  // Try to guess how many cells we need for the number of shells
  // Assume number of shells is number of asym atoms
  // * 2 for cells centred both sides of origin
  int_g na = 2 * ((n / (int)sym->atom_basis.size()) + 1) + 2;
  int_g nb = na;
  int_g nc = na;
  atom_tree cluster;
  bool centre[3] = { true, true, true };
  sym->generate_primitive_atom_display(cluster, centre);
  sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
  atom *parents = 0;
  // Todo - shift xyz to cell 0, may be safest for na/nb/nc range
  cluster.build_cluster_atoms_and_bonds(atom_basis, bonds, sym->bonds, parents,
					n, "Cluster shells", x, y, z);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  return redraw;
}

bool DLV::model_base::update_cluster(model *m, const real_l r)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = (atom_basis.size() != 0);
  if (!redraw) {
    group_names = sym->group_names;
    add_atom_group("Cluster shells");
  }
  atom_basis.clear();
  // Try to guess how many cells we need for the radius
  // * 2 for cells centred both sides of origin
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  sym->get_primitive_lattice(a, b, c);
  int_g na = 1;
  int_g nb = 1;
  int_g nc = 1;
  // Todo - shift to cell 0, may be safest for na/nb/nc range
  // hopefully current increase by pos at least gets the right result
  real_l pos[3];
  sym->display_atoms.get_average_selection_position(pos);
  real_l d;
  // periodic dims but need third coord for surface repeats
  switch (sym->get_model_type()) {
  case 4:
  case 3:
    d = sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
    nc = 2 * (to_integer((r + pos[2]) / d) + 1) + 3;
  case 2:
    d = sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
    nb = 2 * (to_integer((r + pos[1]) / d) + 1) + 3;
  case 1:
    d = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
    na = 2 * (to_integer((r + pos[0]) / d) + 1) + 3;
  default:
    break;
  }
  atom_tree cluster;
  bool centre[3] = { true, true, true };
  sym->generate_primitive_atom_display(cluster, centre);
  sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
  atom *parents = 0;
  cluster.build_cluster_atoms_and_bonds(atom_basis, bonds, sym->bonds, parents,
					r, "Cluster shells", pos[0],
					pos[1], pos[2]);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  return redraw;
}

bool DLV::model_base::edit_atom(model *m, const int_g atn, const bool all,
				const int_g index)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    bool keep = sym->keep_symmetry();
    if (all)
      keep = true;
    redraw = false;
    group_names = sym->group_names;
    copy_lattice(sym);
    //copy_symmetry_ops(sym, false);
    copy_bravais_lattice(sym);
    atom *parents = 0;
    // selections are only in display list - Ouch!
    atom_tree disp;
    bool centre[3] = {false, false, false};
    sym->generate_primitive_atom_display(disp, centre);
    if (keep)
      sym->display_atoms.copy_selected_atoms(disp, 0, "Atoms", true,
					     get_number_of_periodic_dims());
    else
      sym->display_atoms.copy_selected_atoms(disp, 1, "Atoms", true,
					     get_number_of_periodic_dims());
    disp.edit_atoms_and_bonds(atom_basis, bonds, sym->bonds,
			      parents, keep, false);
    if (all) {
      std::map<int, atom>::const_iterator x;
      for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
	if (x->second->get_atomic_number() == atn)
	  x->second->set_edit_flag();
      }
    }
    copy_symmetry_ops(sym, false);
    if (!keep) {
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      sym->get_primitive_lattice(va, vb, vc);
      check_symmetry(parents, va, vb, vc);
    }
    delete [] parents;
  } else {
    std::map<int, atom>::const_iterator x;
    for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
      if (x->second->get_edit_flag())
	x->second->set_atomic_number_and_props(atn);
    }
    complete(true);
  }
  return redraw;
}

bool DLV::model_base::edit_atom_props(model *m, const int_g charge,
				      real_g &radius, const real_g red,
				      const real_g green, const real_g blue,
				      const int_g spin, const bool use_charge,
				      const bool use_radius,
				      const bool use_colour,
				      const bool use_spin, const bool all,
				      const int_g index)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    redraw = false;
    group_names = sym->group_names;
    copy_lattice(sym);
    //copy_symmetry_ops(sym, false);
    copy_bravais_lattice(sym);
    atom *parents = 0;
    // selections are only in display list - Ouch!
    atom_tree disp;
    bool centre[3] = {false, false, false};
    sym->generate_primitive_atom_display(disp, centre);
    sym->display_atoms.copy_selected_atoms(disp, 0, "Atoms", true,
					   get_number_of_periodic_dims());
    disp.edit_atoms_and_bonds(atom_basis, bonds, sym->bonds,
			      parents, true, false);
    if (all) {
      std::map<int, atom>::iterator px = atom_basis.find(index);
      int_g atn = px->second->get_atomic_number();
      std::map<int, atom>::const_iterator x;
      for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
	if (x->second->get_atomic_number() == atn)
	  x->second->set_edit_flag();
      }
    }
    copy_symmetry_ops(sym, false);
    delete [] parents;
  } else {
    std::map<int, atom>::const_iterator x;
    for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
      if (x->second->get_edit_flag())
	x->second->set_properties(charge, radius, red, green, blue, spin,
				  use_charge, use_radius, use_colour, use_spin);
    }
    complete(true);
  }
  return redraw;
}

bool DLV::model_base::edit_atom_rod_props(const model *m, const int_g dw1,
					  const bool use_dw1, 
					  int_g dw2, const bool use_dw2,
					  int_g occ, const bool use_occ,
					  const bool all,
					  const int_g index)
{
  //const model_base *sym = static_cast<const model_base *>(m);
  std::map<int, atom>::iterator x = atom_basis.find(index);
  if (all) {
    int_g oldatn = x->second->get_atomic_number();
    for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
      if (x->second->get_atomic_number() == oldatn){
	if(use_dw1)
	  x->second->set_rod_dw1_id(dw1);
	if(use_dw2)
	  x->second->set_rod_dw2_id(dw2);
	if(use_occ)
	  x->second->set_rod_occ_id(occ);
      }
    }
  } else{
    if(use_dw1)
      x->second->set_rod_dw1_id(dw1);
    if(use_dw2)
      x->second->set_rod_dw2_id(dw2);
    if(use_occ)
      x->second->set_rod_occ_id(occ);
  }
  complete(true);
  return true;
}

bool DLV::model_base::update_slab(const model *m, model *threed, const int_g h,
				  const int_g k, const int_g l,
				  const bool conventional, const real_g tol,
				  int_g &layers, string * &labels, int_g &n,
				  real_l r[3][3])
{
  model_base *c3d = static_cast<model_base *>(threed);
  bool redraw = c3d->generate_surface(m, h, k, l, conventional,
				      tol, labels, n, r);
  if (!redraw) {
    group_names = c3d->group_names;
    add_atom_group("Slab layers");
  }
  generate_slab_layers(threed, tol, 0, 0, layers);
  complete(false);
  return redraw;
}

bool DLV::model_base::update_slab(const model *m, const real_g tol,
				  int_g &layers, string * &labels, int_g &n)
{
  bool redraw = (group_names.size() > 1);
  if (!redraw) {
    const model_base *c3d = static_cast<const model_base *>(m);
    group_names = c3d->group_names;
    add_atom_group("Slab layers");
  }
  generate_slab_layers(m, tol, 0, 0, layers);
  complete(false);
  return redraw;
}

bool DLV::model_base::update_slab(const model *m, model *threed,
				  const real_g tol, const int_g term,
				  const int_g nl)
{
  int_g layers = 0;
  generate_slab_layers(threed, tol, term, nl, layers);
  complete(false);
  return true;
}

bool DLV::model_base::surface_slab(const model *m, const real_g tol,
				   int_g &layers, string * &labels, int_g &n)
{
  bool redraw = (group_names.size() > 1);
  const model_base *sym = static_cast<const model_base *>(m);
  int_g count = sym->get_salvage_layers();
  if (!redraw) {
    group_names = sym->group_names;
    add_atom_group("Slab layers");
  }
  labels = 0;
  n = 0;
  if (count == 0) {
    // no salvage
    generate_slab_layers(m, tol, 0, layers);
  } else {
    // layers is salvage plus bulk
    add_to_slab(m, tol, 0, layers);
  }
  complete(false);
  return redraw;
}

bool DLV::model_base::surface_slab(const model *m, const real_g tol,
				   int_g layers)
{
  // Todo
  const model_base *sym = static_cast<const model_base *>(m);
  int_g count = sym->get_salvage_layers();
  if (sym->group_names.size() == group_names.size())
    add_atom_group("Slab layers");
  if (count == 0) {
    generate_slab_layers(m, tol, layers, count);
  } else {
    add_to_slab(m, tol, layers, count);
  }
  complete(false);
  return true;
}

void DLV::model_base::generate_slab_layers(const model *threed,
					   const real_g tol, const int_g term,
					   const int_g numl, int_g &layers)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  threed->get_primitive_lattice(a, b, c);
  if (numl == 0)
    set_primitive_lattice(a, b, c);
  const model_base *c3d = static_cast<const model_base *>(threed);
  atom_basis.clear();
  bonds.clear_pairs();
  // dummy's
  int_g natoms = c3d->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  real_l shift = 0.0;
  int_g nlayers = c3d->sort_layers(tol, real_g(c[2]), term, shift,
				   layer_index, order, zbuff, natoms);
  layers = nlayers;
  delete_local_array(zbuff);
  delete_local_array(order);
  int_g nl = numl;
  if (numl == 0)
    nl = nlayers;
  int_g ncells = nl / nlayers;
  if (nl % nlayers != 0)
    ncells++;
  int_g *layer_copies = new_local_array1(int_g, nlayers);
  for (int_g i = 0; i < nlayers; i++)
    layer_copies[i] = 0;
  int_g j = term;
  for (int_g i = 0; i < nl; i++) {
    layer_copies[j]++;
    j++;
    if (j == nlayers)
      j = 0;
  }
  atom *parents = 0;
  // are primitive atoms safe for being shifted into cell0?
  c3d->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  c3d->bonds, parents, c,
						  ncells, term, layer_copies,
						  layer_index, "Slab layers");
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  real_l trans[3];
  centre_slab_atoms(c, trans);
  // It is possible for symmetry ops to change
  copy_symmetry_ops(c3d, true);
  shift_cartesian_operators(c3d, trans[0], trans[1], trans[2], a, b, c, 3);
  complete_translators(a, b, c, 3);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

void DLV::model_base::generate_slab_layers(const model *threed,
					   const real_g tol, const int_g numl,
					   int_g &layers)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  threed->get_primitive_lattice(a, b, c);
  if (numl == 0)
    set_primitive_lattice(a, b, c);
  const model_base *c3d = static_cast<const model_base *>(threed);
  atom_basis.clear();
  bonds.clear_pairs();
  // dummy's
  int_g natoms = c3d->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  real_l shift = 0.0;
  int_g nlayers = c3d->sort_layers(tol, 0.0, 0, shift,
				 layer_index, order, zbuff, natoms);
  layers = nlayers;
  delete_local_array(zbuff);
  delete_local_array(order);
  int_g nl = numl;
  if (numl == 0)
    nl = nlayers;
  int_g ncells = nl / nlayers;
  if (nl % nlayers != 0)
    ncells++;
  int_g *layer_copies = new_local_array1(int_g, nlayers);
  for (int_g i = 0; i < nlayers; i++)
    layer_copies[i] = 0;
  int_g j = 0;
  for (int_g i = 0; i < nl; i++) {
    layer_copies[j]++;
    j++;
    if (j == nlayers)
      j = 0;
  }
  atom *parents = 0;
  // are primitive atoms safe for being shifted into cell0?
  c3d->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  c3d->bonds, parents, c,
						  ncells, 0, layer_copies,
						  layer_index, "Slab layers");
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  real_l trans[3];
  centre_slab_atoms(c, trans);
  // It is possible for symmetry ops to change
  copy_symmetry_ops(c3d, true);
  shift_cartesian_operators(c3d, trans[0], trans[1], trans[2], a, b, c, 3);
  complete_translators(a, b, c, 3);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

void DLV::model_base::add_to_slab(const model *threed, const real_g tol,
				  const int_g numl, int_g &layers)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  threed->get_primitive_lattice(a, b, c);
  if (numl == 0)
    set_primitive_lattice(a, b, c);
  const model_base *c3d = static_cast<const model_base *>(threed);
  atom_basis.clear();
  bonds.clear_pairs();
  // dummy's
  int_g natoms = c3d->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  int_g nlayers = c3d->sort_slab(tol, layer_index, order, zbuff, natoms);
  layers = nlayers;
  int_g salvage = 0;
  for (int_g i = 0; i < natoms; i++) {
    if (zbuff[i] > real_l(tol))
      if (layer_index[i] > salvage)
	salvage = layer_index[i];
  }
  // want count rather than max index
  salvage++;
  delete_local_array(zbuff);
  delete_local_array(order);
  int_g nl = numl;
  if (numl == 0)
    nl = nlayers;
  int_g repeat = nlayers - salvage;
  int_g ncells = (nl - salvage) / repeat;
  if ((nl - salvage) % repeat != 0)
    ncells++;
  int_g *layer_copies = new_local_array1(int_g, nlayers);
  for (int_g i = 0; i < nlayers; i++)
    layer_copies[i] = 0;
  for (int_g i = 0; (i < salvage and i < nl); i++)
    layer_copies[i] = 1;
  int_g j = salvage;
  for (int_g i = salvage; i < nl; i++) {
    layer_copies[j]++;
    j++;
    if (j == nlayers)
      j = salvage;
  }
  atom *parents = 0;
  // add to cell size based on number of extra layers
  ncells += salvage / repeat;
  if (salvage % repeat != 0)
    ncells++;
  // are primitive atoms safe for being shifted into cell0?
  c3d->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  c3d->bonds, parents, c,
						  ncells, 0, layer_copies,
						  layer_index, "Slab layers",
						  true, 1);
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  real_l shift[3];
  centre_slab_atoms(c, shift);
  // It is possible for symmetry ops to change
  copy_symmetry_ops(c3d, true);
  shift_cartesian_operators(c3d, shift[0], shift[1], shift[2], a, b, c, 3);
  complete_translators(a, b, c, 3);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

DLV::int_g DLV::model_base::sort_slab(const real_g tol, int_g layer_index[],
			       int_g order[], real_l zbuff[],
			       const int_g natoms) const
{
  coord_type (*coords)[3] = new_local_array2(coord_type, natoms, 3);
  get_primitive_atom_cart_coords(coords, natoms);
  // surface is in xy plane so sort z list of atoms
  for (int_g i = 0; i < natoms; i++)
    zbuff[i] = coords[i][2];
  delete_local_array(coords);
  atom_tree::order_distances(order, zbuff, natoms);
  int_g count = 1;
  int_g j = natoms - 1;
  layer_index[order[j]] = 0;
  for (int_g i = natoms - 2; i >= 0; i--) {
    if (zbuff[order[j]] - zbuff[order[i]] > real_l(tol)) {
      j = i;
      count++;
    }
    layer_index[order[i]] = count - 1;
  }
  return count;
}

bool DLV::model_base::update_surface(const model *m, model *threed,
				     const int_g h, const int_g k,
				     const int_g l, const bool conventional,
				     const real_g tol, string * &labels,
				     int_g &n, real_l r[3][3])
{
  model_base *c3d = static_cast<model_base *>(threed);
  bool redraw = c3d->generate_surface(m, h, k, l, conventional,
				      tol, labels, n, r);
  if (!redraw)
    group_names = c3d->group_names;
  generate_surface_layers(threed, tol, 0);
  std::map<int, atom>::iterator ptr;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr )
    ptr->second->set_special_periodicity();
  complete(false);
  return redraw;
}

bool DLV::model_base::generate_surface(const model *m, const int_g h,
				       const int_g k, const int_g l,
				       const bool conventional,
				       const real_g tol, string * &labels,
				       int_g &n, real_l r[3][3])
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    redraw = false;
    copy_cartesian_atoms(sym);
    bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
    group_names = sym->group_names;
    //add_atom_group("Bulk layers");
  }
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  surface_vectors(sym, h, k, l, conventional, a, b, c);
  rotate_lattice(a, b, c, r);
  set_primitive_lattice(a, b, c);
  rotate_cartesian_atoms(sym, r);
  rotate_symmetry_ops(sym, r);
  // Its supposed to be a det = +1 surface lattice transform so
  // shouldn't be any supercell ops due to vol change?
  //Todo find_bravais_lattice?
  complete(false);
  // index stuff after we've created the primitive atoms
  update_atom_list();
  real_l shift = 0.0;
  (void) identify_layers(tol, real_g(c[2]), 0, shift, labels, n);
  return redraw;
}

void DLV::model_base::generate_surface_layers(const model *threed,
					      const real_g tol,
					      const int_g term)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  threed->get_primitive_lattice(a, b, c);
  set_primitive_lattice(a, b, c);
  const model_base *c3d = static_cast<const model_base *>(threed);
  atom_basis.clear();
  bonds.clear_pairs();
  // Surface layer labels?
  // dummy's
  int_g natoms = c3d->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  real_l shift = 0.0;
  int_g nlayers = c3d->sort_layers(tol, real_g(c[2]), term, shift,
				   layer_index, order, zbuff, natoms);
  //layers = nlayers;
  delete_local_array(zbuff);
  delete_local_array(order);
  int_g ncells = 1;
  int_g *layer_copies = new_local_array1(int_g, nlayers);
  for (int_g i = 0; i < nlayers; i++)
    layer_copies[i] = 1;
  atom *parents = 0;
  // are primitive atoms safe for being shifted into cell0?
  c3d->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  c3d->bonds, parents, c,
						  ncells, term, layer_copies,
						  layer_index,
						  "Surface layers");
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  real_l trans[3];
  shift_surface_atoms(c, trans);
  // It is possible for symmetry ops to change
  copy_symmetry_ops(c3d, true);
  shift_cartesian_operators(c3d, trans[0], trans[1], trans[2], a, b, c, 3);
  complete_translators(a, b, c, 3);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

bool DLV::model_base::update_surface(const model *threed, const real_g tol,
				     const int_g term)
{
  generate_surface_layers(threed, tol, term); 
  std::map<int, atom>::iterator ptr;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr )
    ptr->second->set_special_periodicity();
  complete(false);
  return true;
}

bool DLV::model_base::update_salvage(const model *m, const real_g tol,
				     const int_g nlayers)
{
  const model_base *sym = static_cast<const model_base *>(m);
  int_g count = sym->get_salvage_layers();
  if (nlayers > 0) {
    if (count == 0)
      if (sym->group_names.size() == group_names.size())
	add_atom_group("Salvage");
    if (count == 0) {
      // no salvage
      if (nlayers > 0) {
	// create salvage
	// m has correct termination and number of layers will be 1 repeat.
	generate_salvage(m, tol, nlayers);
      }
    } else {
      // we are adding to salvage
      add_to_salvage(m, tol, nlayers, count);
    }
  }
  set_salvage_layers(count + nlayers);
  complete(false);
  return true;
}

// Todo - lots of common stuff here
void DLV::model_base::generate_salvage(const model *m, const real_g tol,
				       const int_g numl)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  const model_base *sym = static_cast<const model_base *>(m);
  atom_basis.clear();
  bonds.clear_pairs();
  // dummy's
  int_g natoms = sym->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  real_l shift = 0.0;
  int_g nlayers = sym->sort_layers(tol, 0.0, 0, shift,
				 layer_index, order, zbuff, natoms);
  // add an extra layer set for the final repeat group
  int_g nl = numl + nlayers;
  int_g ncells = nl / nlayers;
  if (nl % nlayers != 0)
    ncells++;
  int_g *layer_copies = new_local_array1(int_g, nlayers);
  for (int_g i = 0; i < nlayers; i++)
    layer_copies[i] = 0;
  int_g k = 0;
  int_g cell = 0;
  int_g j = 0;
  for (int_g i = 0; i < nl; i++) {
    layer_copies[j]++;
    if (k == numl) {
      // This is the first repeat layer, so find the shift
      int_g l;
      for (l = 0; l < natoms; l++)
	if (layer_index[l] == j)
	  break;
      shift = - real_l(cell) * c[2] + zbuff[l];
    }
    k++;
    j++;
    if (j == nlayers) {
      j = 0;
      cell++;
    }
  }
  delete_local_array(zbuff);
  delete_local_array(order);
  atom *parents = 0;
  // are primitive atoms safe for being shifted into cell0?
  sym->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  sym->bonds, parents, c,
						  ncells, 0, layer_copies,
						  layer_index, "Salvage",
						  false);
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  // need layer + 1 to be at 0.0
  shift_cartesian_atoms(0.0, 0.0, -shift);
  // It is possible for symmetry ops to change?
  copy_symmetry_ops(sym, false);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

void DLV::model_base::add_to_salvage(const model *m, const real_g tol,
				     const int_g numl, const int_g prevl)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  const model_base *sym = static_cast<const model_base *>(m);
  atom_basis.clear();
  bonds.clear_pairs();
  int_g existl = 1;
  // dummy's
  int_g natoms = sym->get_number_of_primitive_atoms();
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  real_l *zbuff = new_local_array1(real_l, natoms);
  real_l shift = 0.0;
  int_g totlayers = sym->sort_salvage(tol, layer_index, order, zbuff, natoms);
  int_g nlayers = totlayers - existl;
  // add an extra layer set for the final repeat group
  int_g nl = numl + nlayers;
  int_g ncells = nl / nlayers;
  if (nl % nlayers != 0)
    ncells++;
  int_g *layer_copies = new_local_array1(int_g, totlayers);
  for (int_g i = 0; i < totlayers; i++)
    layer_copies[i] = 0;
  int_g k = existl;
  int_g cell = 0;
  int_g j = existl;
  if (existl > 0)
    layer_copies[0] = 1;
  for (int_g i = existl; i < nl + existl; i++) {
    layer_copies[j]++;
    if (k == numl + existl) {
      // This is the first repeat layer, so find the shift
      int_g l;
      for (l = 0; l < natoms; l++)
	if (layer_index[l] == j)
	  break;
      shift = - real_l(cell) * c[2] + zbuff[l];
    }
    k++;
    j++;
    if (j == totlayers) {
      j = existl;
      cell++;
    }
  }
  delete_local_array(zbuff);
  delete_local_array(order);
  atom *parents = 0;
  // are primitive atoms safe for being shifted into cell0?
  sym->primitive_atoms.copy_atom_layers_and_bonds(atom_basis, bonds,
						  sym->bonds, parents, c,
						  ncells, 0, layer_copies,
						  layer_index, "Salvage",
						  false, prevl);
  delete_local_array(layer_copies);
  delete_local_array(layer_index);
  // need layer + 1 to be at 0.0
  shift_cartesian_atoms(0.0, 0.0, -shift);
  // It is possible for symmetry ops to change?
  copy_symmetry_ops(sym, false);
  coord_type d[3] = { 0.0, 0.0, 0.0 };
  check_symmetry_no_z(parents, a, b, d);
  delete [] parents;
}

DLV::int_g DLV::model_base::sort_salvage(const real_g tol, int_g layer_index[],
					 int_g order[], real_l zbuff[],
					 const int_g natoms) const
{
  coord_type (*coords)[3] = new_local_array2(coord_type, natoms, 3);
  get_primitive_atom_cart_coords(coords, natoms);
  // surface is in xy plane so sort z list of atoms
  for (int_g i = 0; i < natoms; i++)
    zbuff[i] = coords[i][2];
  delete_local_array(coords);
  atom_tree::order_distances(order, zbuff, natoms);
  // count number of layers - skip +z ones of existing salvage
  int_g count = 1;
  int_g i = natoms - 1;
  while (zbuff[order[i]] > real_l(tol)) {
    layer_index[order[i]] = 0;
    i--;
  }
  if (i != natoms - 1)
    count++;
  int_g j = i;
  //layer_index[order[i]] = 0;
  for (; i >= 0; i--) {
    if (zbuff[order[j]] - zbuff[order[i]] > real_l(tol)) {
      j = i;
      count++;
    }
    layer_index[order[i]] = count - 1;
  }
  return count;
}

DLV::int_g DLV::model_base::get_salvage_layers() const
{
  // BUG
  return 0;
}

void DLV::model_base::set_salvage_layers(const int_g l)
{
  // BUG
}
  
// see Surface Review and Letters (1997) v4 p 1063
void DLV::model_base::surface_vectors(const model_base *sym, const int_g h,
				      const int_g k, const int_g l,
				      const bool conventional,
				      coord_type rs1[3], coord_type rs2[3],
				      coord_type rs3[3]) const
{
  // primitive h, k, l values
  int_g ph = 0;
  int_g pk = 0;
  int_g pl = 0;
  if (conventional) {
    int_g t[3][3];
    sym->get_transform_to_prim_cell(t);
    // multiply by transformation to primitive, ignoring fractional factor
    // to keep as integer, if it ends up too large gcd removal will fix it.
    ph = t[0][0] * h + t[0][1] * k + t[0][2] * l;
    pk = t[1][0] * h + t[1][1] * k + t[1][2] * l;
    pl = t[2][0] * h + t[2][1] * k + t[2][2] * l;
  } else {
    ph = h;
    pk = k;
    pl = l;
  }
  // remove common divisor
  int_g d = 1;
  do {
    bool set = false;
    if (ph != 0) {
      d = std::abs(ph);
      set = true;
    }
    if (pk != 0 and (std::abs(pk) < d or !set)) {
      d = std::abs(pk);
      set = true;
    }
    if (pl != 0 and (std::abs(pl) < d or !set)) {
      d = std::abs(pl);
      set = true;
    }
    while (d > 1) {
      if (ph % d == 0 and pk % d == 0 and pl % d == 0) {
	ph /= d;
	pk /= d;
	pl /= d;
	break;
      }
      d--;
    }
  } while (d > 1);
  // Transformation lattice vectors into (hkl) oriented lattice
  int_g alpha = 0;
  int_g beta = 0;
  int_g gamma = 0;
  int_g eq = 0;
  if (ph == 0) {
    if (pk == 0) {
      // (0 0 1) - eq (6b)
      gamma = 1;
      eq = 1;
    } else if (pl == 0) {
      // (0 1 0) - eq (6c)
      beta = 1;
      eq = 2;
    } else {
      // (0 k l) - eq (6c)
      euclid(pk, beta, pl, gamma, 1);
      eq = 2;
    }
  } else if (pk == 0) {
    if (pl == 0) {
      // (1 0 0) - eq (6a)
      alpha = 1;
    } else {
      // (h 0 l) - eq (6b)
      euclid(ph, alpha, pl, gamma, 1);
      eq  = 1;
    }
  } else if (pl == 0) {
    // (h k 0) - eq (6a)
    euclid(ph, alpha, pk, beta, 1);
  } else {
    // (h k l), choose (6a) if possible, e.g. (2 2 1) has h k problems
    d = std::abs(ph);
    if (std::abs(pk) < d)
      d = std::abs(pk);
    while (d > 1) {
      if (ph % d == 0 and pk % d == 0)
	break;
      d--;
    }
    if (d == 1) {
      euclid(ph, alpha, pk, beta, 1);
      eq = 0;
    } else {
      d = std::abs(ph);
      if (std::abs(pl) < d)
	d = std::abs(pl);
      while (d > 1) {
	if (ph % d == 0 and pl % d == 0)
	  break;
	d--;
      }
      if (d == 1) {
	euclid(ph, alpha, pl, gamma, 1);
	eq = 1;
      } else {
	d = std::abs(pk);
	if (std::abs(pl) < d)
	  d = std::abs(pl);
	while (d > 1) {
	  if (pk % d == 0 and pl % d == 0)
	    break;
	  d--;
	}
	if (d == 1) {
	  euclid(pk, beta, pl, gamma, 1);
	  eq = 2;
	} else
	  fprintf(stderr, "Problem finding safe (h k l) for Euclid\n");
      }
    }
  }
  //fprintf(stderr, "h = %d, k = %d, l = %d\n", ph, pk, pl);
  //fprintf(stderr, "alpha = %d, beta = %d, gamma = %d, %d\n", alpha,
  //  beta, gamma, eq);
  int_g T_hkl[3][3];
  //int T_inv[3][3];
  if (eq == 1) {
    // (6b)
    T_hkl[0][0] = -pl;
    T_hkl[0][1] = 0;
    T_hkl[0][2] = ph;
    T_hkl[1][0] = pk * alpha;
    T_hkl[1][1] = -1;
    T_hkl[1][2] = pk * gamma;
    T_hkl[2][0] = alpha;
    T_hkl[2][1] = 0;
    T_hkl[2][2] = gamma;
    //T_inv[0][0] = -gamma;
    //T_inv[0][1] = 0;
    //T_inv[0][2] = ph;
    //T_inv[1][0] = 0;
    //T_inv[1][1] = -1;
    //T_inv[1][2] = pk;
    //T_inv[2][0] = alpha;
    //T_inv[2][1] = 0;
    //T_inv[2][2] = pl;
  } else if (eq == 2) {
    // (6c)
    T_hkl[0][0] = 0;
    T_hkl[0][1] = pl;
    T_hkl[0][2] = -pk;
    T_hkl[1][0] = -1;
    T_hkl[1][1] = ph * beta;
    T_hkl[1][2] = ph * gamma;
    T_hkl[2][0] = 0;
    T_hkl[2][1] = beta;
    T_hkl[2][2] = gamma;
    //T_inv[0][0] = 0;
    //T_inv[0][1] = -1;
    //T_inv[0][2] = ph;
    //T_inv[1][0] = gamma;
    //T_inv[1][1] = 0;
    //T_inv[1][2] = pk;
    //T_inv[2][0] = -beta;
    //T_inv[2][1] = 0;
    //T_inv[2][2] = gamma;
  } else {
    // (6a)
    T_hkl[0][0] = pk;
    T_hkl[0][1] = -ph;
    T_hkl[0][2] = 0;
    T_hkl[1][0] = pl * alpha;
    T_hkl[1][1] = pl * beta;
    T_hkl[1][2] = -1;
    T_hkl[2][0] = alpha;
    T_hkl[2][1] = beta;
    T_hkl[2][2] = 0;
    //T_inv[0][0] = beta;
    //T_inv[0][1] = 0;
    //T_inv[0][2] = ph;
    //T_inv[1][0] = -alpha;
    //T_inv[1][1] = 0;
    //T_inv[1][2] = pk;
    //T_inv[2][0] = 0;
    //T_inv[2][1] = -1;
    //T_inv[2][2] = pl;
  }
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  sym->get_primitive_lattice(a, b, c);
  //real_l vol1 = a[0] * b[1] * c[2] - a[2] * b[1] * c[0]
  //  + a[1] * b[2] * c[0] - a[1] * b[0] * c[2]
  //  + a[2] * b[0] * c[1] - a[2] * b[1] * c[0];
  // Not completely sure of this, but seems consistent with paper
  // 'matrix' of lattice has each row as a vector
  for (int_g i = 0; i < 3; i++) {
    rs1[i] = real_l(T_hkl[0][0]) * a[i] + real_l(T_hkl[0][1]) * b[i]
      + real_l(T_hkl[0][2]) * c[i];
    rs2[i] = real_l(T_hkl[1][0]) * a[i] + real_l(T_hkl[1][1]) * b[i]
      + real_l(T_hkl[1][2]) * c[i];
    rs3[i] = real_l(T_hkl[2][0]) * a[i] + real_l(T_hkl[2][1]) * b[i]
      + real_l(T_hkl[2][2]) * c[i];
  }
  //fprintf(stderr, "Base vectors\n");
  //fprintf(stderr, "%f %f %f\n", rs1[0], rs1[1], rs1[2]);
  //fprintf(stderr, "%f %f %f\n", rs2[0], rs2[1], rs2[2]);
  //fprintf(stderr, "%f %f %f\n", rs3[0], rs3[1], rs3[2]);
  // Minkowski reduce to smallest set of vectors.
  coord_type rs1p[3];
  coord_type rs2p[3];
  int_g q1 = 0;
  int_g q2 = 0;
  do {
    for (int_g i = 0; i < 3; i++) {
      rs1p[i] = rs1[i];
      rs2p[i] = rs2[i];
    }
    coord_type x1 =
      (rs1p[0] * rs2p[0] + rs1p[1] * rs2p[1] + rs1p[2] * rs2p[2]) /
      (rs1p[0] * rs1p[0] + rs1p[1] * rs1p[1] + rs1p[2] * rs1p[2]);
    q1 = to_integer(DLV::nint(x1));
    //fprintf(stderr, "q1 %d %f\n", q1, x1);
    for (int_g i = 0; i < 3; i++)
      rs2[i] = rs2p[i] - real_l(q1) * rs1p[i];
    coord_type x2 =
      (rs1p[0] * rs2[0] + rs1p[1] * rs2[1] + rs1p[2] * rs2[2]) /
      (rs2[0] * rs2[0] + rs2[1] * rs2[1] + rs2[2] * rs2[2]);
    q2 = to_integer(DLV::nint(x2));
    //fprintf(stderr, "q2 %d %f\n", q2, x2);
    for (int_g i = 0; i < 3; i++)
      rs1[i] = rs1p[i] - real_l(q2) * rs2[i];
  } while (q1 != 0 and q2 != 0);
  // Choose the vector angle >= 90
  real_l ctheta = rs1[0] * rs2[0] + rs1[1] * rs2[1] + rs1[2] * rs2[2];
  real_l len = (rs1[0] * rs1[0] + rs1[1] * rs1[1] + rs1[2] * rs1[2])
    * (rs2[0] * rs2[0] + rs2[1] * rs2[1] + rs2[2] * rs2[2]);
  ctheta /= sqrt(len);
  if (ctheta > 0.0001) {
    // a -> b, b -> -a preserve handedness of coord system
    len = rs1[0];
    rs1[0] = rs2[0];
    rs2[0] = -len;
    len = rs1[1];
    rs1[1] = rs2[1];
    rs2[1] = -len;
    len = rs1[2];
    rs1[2] = rs2[2];
    rs2[2] = -len;
  }
  // get 3rd vector
  coord_type r2sq = rs2[0] * rs2[0] + rs2[1] * rs2[1] + rs2[2] * rs2[2];
  coord_type r1sq = rs1[0] * rs1[0] + rs1[1] * rs1[1] + rs1[2] * rs1[2];
  coord_type r12 = rs1[0] * rs2[0] + rs1[1] * rs2[1] + rs1[2] * rs2[2];
  coord_type phi = r1sq * r2sq - r12 * r12;
  coord_type x1 =
    ((rs1[0] * rs3[0] + rs1[1] * rs3[1] + rs1[2] * rs3[2]) * r2sq
     - (rs2[0] * rs3[0] + rs2[1] * rs3[1] + rs2[2] * rs3[2]) * r12) / phi;
  int_g n1 = to_integer(DLV::truncate(x1));
  coord_type x2 =
    ((rs2[0] * rs3[0] + rs2[1] * rs3[1] + rs2[2] * rs3[2]) * r1sq
     - (rs1[0] * rs3[0] + rs1[1] * rs3[1] + rs1[2] * rs3[2]) * r12) / phi;
  int_g n2 = to_integer(DLV::truncate(x2));
  for (int_g i = 0; i < 3; i++)
    rs3[i] -= (real_l(n1) * rs1[i] + real_l(n2) * rs2[i]);
  //fprintf(stderr, "Simplify\n");
  //fprintf(stderr, "%f %f %f\n", rs1[0], rs1[1], rs1[2]);
  //fprintf(stderr, "%f %f %f\n", rs2[0], rs2[1], rs2[2]);
  //fprintf(stderr, "%f %f %f\n", rs3[0], rs3[1], rs3[2]);
  // worry about left or right handed system in rotate
  //real_l vol2 = a[0] * b[1] * c[2] - a[2] * b[1] * c[0]
  //  + a[1] * b[2] * c[0] - a[1] * b[0] * c[2]
  //  + a[2] * b[0] * c[1] - a[2] * b[1] * c[0];
  //fprintf(stderr, "volume change %f\n", vol2 / vol1);
}

void DLV::model_base::rotate_lattice(coord_type a[3], coord_type b[3],
				     coord_type c[3], real_l r[3][3])
{
  // rotate a into x axis, b into xy plane and then make sure its the
  // correct coordinate system with c in +z direction.
  // copied and modified from plane code
  // Equations for generalised rotation from Kim(1999), Cam UP, section 4.3
  // Theta is the angle between the 2 normalised vectors, s the normal to the
  // plane that contains them (via a cross product).
  // Rotate (1, 0, 0) into OA, then rotate the rotated (0, 1, 0) about
  // OA into OB.
  // Thats what the plane code did, we need the inverse.
  // get normalised a vector
  coord_type oa[3];
  coord_type len = 0.0;
  for (int_g i = 0; i < 3; i++)
    len += a[i] * a[i];
  len = sqrt(len);
  for (int_g i = 0; i < 3; i++)
    oa[i] = a[i] / len;
  // Now start setting up rotation
  coord_type diff1 = abs(real_l(1.0) - oa[0]) + abs(oa[1]) + abs(oa[2]);
  coord_type diff2 = abs(oa[0] + 1.0) + abs(oa[1]) + abs(oa[2]);
  const coord_type tol = 0.001;
  coord_type m1[3][3];
  if (diff1 < tol) {
    m1[0][0] = 1.0;
    m1[0][1] = 0.0;
    m1[0][2] = 0.0;
    m1[1][0] = 0.0;
    m1[1][1] = 1.0;
    m1[1][2] = 0.0;
    m1[2][0] = 0.0;
    m1[2][1] = 0.0;
    m1[2][2] = 1.0;
  } else if (diff2 < tol) {
    m1[0][0] = -1.0;
    m1[0][1] = 0.0;
    m1[0][2] = 0.0;
    m1[1][0] = 0.0;
    m1[1][1] = 1.0;
    m1[1][2] = 0.0;
    m1[2][0] = 0.0;
    m1[2][1] = 0.0;
    m1[2][2] = -1.0;
  } else {
    coord_type xyl = oa[1] * oa[1] + oa[2] * oa[2];
    coord_type l = sqrt(xyl + oa[0] * oa[0]);
    xyl = sqrt(xyl);
    coord_type ctheta = oa[0] / l;
    coord_type stheta = xyl / l;
    coord_type s[3];
    s[0] = 0.0;
    s[1] = -oa[2] / xyl;
    s[2] = oa[1] / xyl;
    m1[0][0] = ctheta;
    m1[0][1] = - stheta * s[2];
    m1[0][2] = stheta * s[1];
    m1[1][0] = stheta * s[2];
    m1[1][1] = (s[1] * s[1]) + ctheta * (real_l(1.0) - s[1] * s[1]);
    m1[1][2] = (real_l(1.0) - ctheta) * s[1] * s[2];
    m1[2][0] = - stheta * s[1];
    m1[2][1] = (real_l(1.0) - ctheta) * s[2] * s[1];
    m1[2][2] = s[2] * s[2] + ctheta * (real_l(1.0) - s[2] * s[2]);
    coord_type n[3];
    n[0] = 1.0;
    n[1] = 0.0;
    n[2] = 0.0;
    coord_type v[3];
    for (int_g i = 0; i < 3; i++)
      v[i] = m1[i][0] * n[0] + m1[i][1] * n[1] + m1[i][2] * n[2];
    diff2 = abs(oa[0] - v[0]) + abs(oa[1] - v[1]) + abs(oa[2] - v[2]);
    if (diff2 > tol) {
      // negate stheta
      stheta = - stheta;
      m1[0][0] = ctheta;
      m1[0][1] = - stheta * s[2];
      m1[0][2] = stheta * s[1];
      m1[1][0] = stheta * s[2];
      m1[1][1] = (s[1] * s[1]) + ctheta * (real_l(1.0) - s[1] * s[1]);
      m1[1][2] = (real_l(1.0) - ctheta) * s[1] * s[2];
      m1[2][0] = - stheta * s[1];
      m1[2][1] = (real_l(1.0) - ctheta) * s[2] * s[1];
      m1[2][2] = s[2] * s[2] + ctheta * (real_l(1.0) - s[2] * s[2]);
    }
  }
  coord_type n[3];
  n[0] = 0.0;
  n[1] = 1.0;
  n[2] = 0.0;
  // set up ob, which needs to be in ab plane but normal to a
  // So if n is the normal of the plane defined by OA and OX. Then OB is
  // normal to n and OA (and the angle between OB and OX < 90).
  coord_type ox[3];
  len = 0.0;
  for (int_g i = 0; i < 3; i++)
    len += b[i] * b[i];
  len = sqrt(len);
  for (int_g i = 0; i < 3; i++)
    ox[i] = b[i] / len;
  coord_type norm[3];
  norm[0] = oa[1] * ox[2] - oa[2] * ox[1];
  norm[1] = oa[2] * ox[0] - oa[0] * ox[2];
  norm[2] = oa[0] * ox[1] - oa[1] * ox[0];
  coord_type ob[3];
  ob[0] = norm[1] * oa[2] - norm[2] * oa[1];
  ob[1] = norm[2] * oa[0] - norm[0] * oa[2];
  ob[2] = norm[0] * oa[1] - norm[1] * oa[0];
  coord_type l = sqrt(ob[0] * ob[0] + ob[1] * ob[1] + ob[2] * ob[2]);
  ob[0] = ob[0] / l;
  ob[1] = ob[1] / l;
  ob[2] = ob[2] / l;
  // Make sure we got the angles right (use fact that they are unit vectors)
  coord_type ca = ob[0] * ox[0] + ob[1] * ox[1] + ob[2] * ox[2];
  if (ca < -tol) {
    ob[0] = - ob[0];
    ob[1] = - ob[1];
    ob[2] = - ob[2];
    ca = -ca;
  }
  // Next part of the rotation
  coord_type v[3];
  for (int_g i = 0; i < 3; i++)
    v[i] = m1[i][0] * n[0] + m1[i][1] * n[1] + m1[i][2] * n[2];
  diff2 = abs(ob[0] - v[0]) + abs(ob[1] - v[1]) + abs(ob[2] - v[2]);
  coord_type m2[3][3];
  if (diff2 > tol) {
    l = 1.0;
    coord_type xyl = v[0] * ob[0] + v[1] * ob[1] + v[2] * ob[2];
    coord_type ctheta = xyl / l;
    coord_type stheta = sqrt(real_l(1.0) - ctheta * ctheta);
    coord_type s[3];
    s[0] = oa[0];
    s[1] = oa[1];
    s[2] = oa[2];
    m2[0][0] = (s[0] * s[0]) + ctheta * (real_l(1.0) - s[0] * s[0]);
    m2[0][1] = (real_l(1.0) - ctheta) * s[0] * s[1] - stheta * s[2];
    m2[0][2] = (real_l(1.0) - ctheta) * s[0] * s[2] + stheta * s[1];
    m2[1][0] = (real_l(1.0) - ctheta) * s[1] * s[0] + stheta * s[2];
    m2[1][1] = (s[1] * s[1]) + ctheta * (real_l(1.0) - s[1] * s[1]);
    m2[1][2] = (real_l(1.0) - ctheta) * s[1] * s[2] - stheta * s[0];
    m2[2][0] = (real_l(1.0) - ctheta) * s[2] * s[0] - stheta * s[1];
    m2[2][1] = (real_l(1.0) - ctheta) * s[2] * s[1] + stheta * s[0];
    m2[2][2] = s[2] * s[2] + ctheta * (real_l(1.0) - s[2] * s[2]);
    for (int_g i = 0; i < 3; i++)
      n[i] = m2[i][0] * v[0] + m2[i][1] * v[1] + m2[i][2] * v[2];
    diff2 = abs(ob[0] - n[0]) + abs(ob[1] - n[1]) + abs(ob[2] - n[2]);
    if (diff2 > tol) {
      // negate stheta
      stheta = - stheta;
      m2[0][0] = (s[0] * s[0]) + ctheta * (real_l(1.0) - s[0] * s[0]);
      m2[0][1] = (real_l(1.0) - ctheta) * s[0] * s[1] - stheta * s[2];
      m2[0][2] = (real_l(1.0) - ctheta) * s[0] * s[2] + stheta * s[1];
      m2[1][0] = (real_l(1.0) - ctheta) * s[1] * s[0] + stheta * s[2];
      m2[1][1] = (s[1] * s[1]) + ctheta * (real_l(1.0) - s[1] * s[1]);
      m2[1][2] = (real_l(1.0) - ctheta) * s[1] * s[2] - stheta * s[0];
      m2[2][0] = (real_l(1.0) - ctheta) * s[2] * s[0] - stheta * s[1];
      m2[2][1] = (real_l(1.0) - ctheta) * s[2] * s[1] + stheta * s[0];
      m2[2][2] = s[2] * s[2] + ctheta * (real_l(1.0) - s[2] * s[2]);
    }
  } else {
    m2[0][0] = 1.0;
    m2[0][1] = 0.0;
    m2[0][2] = 0.0;
    m2[1][0] = 0.0;
    m2[1][1] = 1.0;
    m2[1][2] = 0.0;
    m2[2][0] = 0.0;
    m2[2][1] = 0.0;
    m2[2][2] = 1.0;
  }
  //DLV_MMmultnn(t, m2, m1, 3, true);
  coord_type t[3][3];
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      t[i][j] = 0.0;
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      for (int_g k = 0; k < 3; k++)
	t[i][j] += m2[i][k] * m1[k][j];
  // Transpose is the inverse?
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      r[i][j] = t[j][i];
  coord_type vec[3];
  for (int_g i = 0; i < 3; i++) {
    vec[i] = a[i];
    a[i] = 0.0;
  }
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      a[i] += r[i][j] * vec[j];
  for (int_g i = 0; i < 3; i++) {
    vec[i] = b[i];
    b[i] = 0.0;
  }
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      b[i] += r[i][j] * vec[j];
  for (int_g i = 0; i < 3; i++) {
    vec[i] = c[i];
    c[i] = 0.0;
  }
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      c[i] += r[i][j] * vec[j];
  // get correct type of coordinate system.
  if (c[2] < -tol) {
    for (int_g i = 0; i < 3; i++)
      c[i] = -c[i];
  }
  //fprintf(stderr, "Rotate\n");
  //fprintf(stderr, "%f %f %f\n", a[0], a[1], a[2]);
  //fprintf(stderr, "%f %f %f\n", b[0], b[1], b[2]);
  //fprintf(stderr, "%f %f %f\n", c[0], c[1], c[2]);
  //fprintf(stderr, "%f %f %f\n", r[0][0], r[0][1], r[0][2]);
  //fprintf(stderr, "%f %f %f\n", r[1][0], r[1][1], r[1][2]);
  //fprintf(stderr, "%f %f %f\n", r[2][0], r[2][1], r[2][2]);
}

DLV::int_g DLV::model_base::identify_layers(const real_g tol,
					    const real_g zshift,
					    const int_g term, real_l &shift,
					    string * &labels, int_g &n,
					    const bool str) const
{
  int_g natoms = get_number_of_primitive_atoms();
  real_l *zbuff = new_local_array1(real_l, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  int_g *layer_index = new_local_array1(int_g, natoms);
  int_g nlayers = sort_layers(tol, zshift, term, shift,
			      layer_index, order, zbuff, natoms);
  if (str) {
    //primitive_atoms.layer_groups(layer_index, "Bulk layers");
    // generate UI labels
    n = 0;
    int_g *atom_types = new_local_array1(int_g, natoms);
    get_primitive_atom_types(atom_types, natoms);
    labels = new string[nlayers];
    n = nlayers;
    if (nlayers == 1)
      labels[0] = "Only layer";
    else {
      int_g k = 0;
      int_g j = natoms - 1;
      labels[k] = atomic_prefs::get_symbol(atom_types[order[j]]);
      for (int_g i = natoms - 2; i >= 0; i--) {
	if (zbuff[order[j]] - zbuff[order[i]] > real_l(tol)) {
	  k++;
	  j = i;
	  labels[k] = atomic_prefs::get_symbol(atom_types[order[j]]);
	} else {
	  labels[k] += ", ";
	  labels[k] += atomic_prefs::get_symbol(atom_types[order[i]]);
	}
      }
    }
    delete_local_array(atom_types);
  }
  delete_local_array(layer_index);
  delete_local_array(order);
  delete_local_array(zbuff);
  return nlayers;
}

DLV::int_g
DLV::model_base::sort_layers(const real_g tol, const real_g zshift,
			     const int_g term, real_l &shift,
			     int_g layer_index[], int_g order[],
			     real_l zbuff[], const int_g natoms) const
{
  coord_type (*coords)[3] = new_local_array2(coord_type, natoms, 3);
  get_primitive_atom_cart_coords(coords, natoms);
  // surface is in xy plane so sort z list of atoms
  for (int_g i = 0; i < natoms; i++) {
    zbuff[i] = coords[i][2];
    // How safe is the use of tol here?
    if (zbuff[i] > real_l(tol))
      zbuff[i] -= real_l(zshift);
  }
  delete_local_array(coords);
  atom_tree::order_distances(order, zbuff, natoms);
  // count number of layers - largest, i.e ~ 0, is at end of list
  int_g count = 1;
  int_g j = natoms - 1;
  layer_index[order[natoms - 1]] = 0;
  shift = 0.0;
  for (int_g i = natoms - 2; i >= 0; i--) {
    if (zbuff[order[j]] - zbuff[order[i]] > real_l(tol)) {
      j = i;
      if (count == term)
	shift = zbuff[order[j]];
      count++;
    }
    layer_index[order[i]] = count - 1;
  }
  // Correct overlap of final layer with 0.0
  if (count > 1) {
    if (abs(zbuff[order[0]] - zbuff[order[natoms - 1]]) < real_l(tol)) {
      count--;
      for (int_g i = 0; i < natoms; i++)
	if (layer_index[order[i]] == count)
	  layer_index[order[i]] = 0;
    }
  }
  return count;
}

bool DLV::model_base::delete_atoms(model *m)
{
  model_base *sym = static_cast<model_base *>(m);
  if (sym->keep_symmetry()) {
    /*if (display_atoms.get_number_selected_atoms() == 0) {
      atom_basis.clear();
      bonds.clear_all();
      copy_cartesian_atoms(sym);
      bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
      } else*/
    if (sym->atom_basis.size() == atom_basis.size()) {
      int_g *atomic_types = new_local_array1(int_g, atom_basis.size());
      std::map<int, atom> basis;
      std::map<int, atom>::iterator ptr;
      for (ptr = sym->atom_basis.begin(); ptr != sym->atom_basis.end(); ++ptr) {
	if (sym->display_atoms.is_selected(ptr->second)) {
	  // sort out bonds
	  bool keep_atn = false;
	  int_g atn = ptr->second->get_atomic_number();
	  std::map<int, atom>::iterator x;
	  int_g i = 0;
	  int_g j = 0;
	  for (x = atom_basis.begin(); x != atom_basis.end(); ++x) {
	    atomic_types[i] = x->second->get_atomic_number();
	    if (bonds.is_set(ptr->second, x->second))
	      bonds.clear_pair(ptr->second, x->second);
	    if (x->first == ptr->first) { // Windows problem on iterator compare
	      atomic_types[i] = 0;
	      j = i;
	    } else {
	      if (atomic_types[i] == atn)
		keep_atn = true;
	    }
	    i++;
	  }
	  if (!keep_atn) {
	    for (int_g k = 0; k < (int)sym->atom_basis.size(); k++) {
	      if (k != j) {
		if (atomic_types[k] > 0 and bonds.is_set(atomic_types[k], atn))
		  bonds.clear_pair_type(atomic_types[k], atn);
	      }
	    }
	  }
	} else { // deleting active iterators is a problem
	  // since we're looping in order this should preserve ids
	  // up until the one we delete.
	  std::map<int, atom>::iterator nptr;
	  nptr = atom_basis.find(ptr->first);
	  //nptr->second->set_id(basis.size());
	  basis[basis.size()] = nptr->second;
	}
      }
      delete_local_array(atomic_types);
      atom_basis.clear();
      atom_basis = basis;
      // don't reindex atoms until we've been through all the bonds
      for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
	if (ptr->first != ptr->second->get_id()) {
	  bonds.reindex(ptr->second->get_id(), ptr->first,
			(int)sym->atom_basis.size());
	  ptr->second->set_id(ptr->first);
	}
      }
    } else {
      // should no longer be used!
      // deleteing different atoms from deleted list, seems simplest
      // to use the break sym technology since all copies are selected.
      // Use primitive to avoid cell issues,
      // selections are only in display list
      atom_tree disp;
      bool centre[3] = {false, false, false};
      sym->generate_primitive_atom_display(disp, centre);
      display_atoms.copy_selected_atoms(disp, 0, "Atoms", true);
      atom *parents = 0;
      atom_basis.clear();
      bonds.clear_all();
      disp.delete_selected_atoms(atom_basis, bonds, sym->bonds, parents);
      delete [] parents;
    }
    complete(true);
  } else {
    /*
    if (display_atoms.get_number_selected_atoms() == 0) {
      atom_basis.clear();
      bonds.clear_all();
      copy_cartesian_atoms(sym);
      bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
      copy_symmetry_ops(sym, false);
    } else {
    */
    // break sym
    // Use primitive to avoid cell issues
    atom_tree disp;
    bool centre[3] = {false, false, false};
    sym->generate_primitive_atom_display(disp, centre);
    sym->display_atoms.copy_selected_atoms(disp, 1, "Atoms", false,
					   get_number_of_periodic_dims());
    atom *parents = 0;
    atom_basis.clear();
    bonds.clear_all();
    disp.delete_selected_atoms(atom_basis, bonds, sym->bonds, parents);
    coord_type a[3];
    coord_type b[3];
    coord_type c[3];
    get_primitive_lattice(a, b, c);
    copy_symmetry_ops(sym, false);
    check_symmetry(parents, a, b, c);
    delete [] parents;
    //}
    complete(false);
  }
  char message[256];
  display_atoms.deselect(message, 256);
  return true;
}

bool DLV::model_base::insert_atom(const model *m, const int_g atn,
				  const real_l x, const real_l y,
				  const real_l z, const bool frac,
				  const bool keep)
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    group_names = sym->group_names;
    copy_lattice(sym);
    copy_bravais_lattice(sym);
    complete(false);
    redraw = false;
  } else {
    atom_basis.clear();
    bonds.clear_pairs();
  }
  if (keep) {
    copy_cartesian_atoms(sym);
    copy_symmetry_ops(sym, false);
    bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
    atom a(new atom_type());
    a->set_atomic_number(atn);
    a->set_edit_flag();
    coord_type c[3];
    c[0] = x;
    c[1] = y;
    c[2] = z;
    if (frac)
      a->set_fractional_coords(c);
    else
      a->set_cartesian_coords(c);
    a->set_id(atom_basis.size());
    atom_basis[atom_basis.size()] = a;
  } else {
    // Need an extra parent
    atom *parents = new atom[sym->primitive_atoms.size() + 1];
    real_l shift[3] = { 0.0, 0.0, 0.0 };
    sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds, sym->bonds,
					      parents, shift);
    atom a(new atom_type());
    a->set_atomic_number(atn);
    a->set_edit_flag();
    coord_type c[3];
    c[0] = x;
    c[1] = y;
    c[2] = z;
    if (frac)
      a->set_fractional_coords(c);
    else
      a->set_cartesian_coords(c);
    parents[atom_basis.size()] = a;
    a->set_id(atom_basis.size());
    atom_basis[atom_basis.size()] = a;
    coord_type va[3];
    coord_type vb[3];
    coord_type vc[3];
    sym->get_primitive_lattice(va, vb, vc);
    copy_symmetry_ops(sym, false);
    check_symmetry(parents, va, vb, vc);
    delete [] parents;
  }
  complete(false);
  return redraw;
}

bool DLV::model_base::insert_model(const model *m, const model *i,
				   const real_l x, const real_l y,
				   const real_l z, const bool frac,
				   const real_l c_x, const real_l c_y,
				   const real_l c_z, const bool keep)
{
  const model_base *sym = static_cast<const model_base *>(m);
  bool redraw = true;
  real_l shift[3] = {0.0, 0.0, 0.0};
  if (atom_basis.size() == 0) {
    group_names = sym->group_names;
    add_atom_group(i->get_model_name());
    copy_lattice(sym);
    copy_bravais_lattice(sym);
    copy_symmetry_ops(sym, false);
    const model_base *molecule = static_cast<const model_base *>(i);
    int_g size = 0;
    atom *parents = 0;
    if (keep) {
      parents = new atom[sym->atom_basis.size()
			 + molecule->primitive_atoms.size()];
      copy_cartesian_atoms(sym, parents);
      bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
      size = (int)sym->atom_basis.size();
      molecule->primitive_atoms.add_atoms_and_bonds(atom_basis, bonds,
						    molecule->bonds, parents,
						    size, shift, "Molecule",
						    i->get_model_name());
    } else {
      parents = new atom[sym->primitive_atoms.size()
			 + molecule->primitive_atoms.size()];
      sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds,
						sym->bonds, parents, shift);
      size = sym->primitive_atoms.size();
      molecule->primitive_atoms.add_atoms_and_bonds(atom_basis, bonds,
						    molecule->bonds, parents,
						    size, shift, "Molecule",
						    i->get_model_name());
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      sym->get_primitive_lattice(va, vb, vc);
      check_symmetry(parents, va, vb, vc);
    }
    delete [] parents;
    redraw = false;
  } else {
    coord_type c1 = x;
    coord_type c2 = y;
    coord_type c3 = z;
    if (frac)
      sym->fractional_to_cartesian_coords(c1, c2, c3);
    shift[0] = c1 - c_x;
    shift[1] = c2 - c_y;
    shift[2] = c3 - c_z;
    atom *parents = 0;
    if (keep) {
      // symmetry may have removed a molecule primitive which might be
      // needed at new position.
      const model_base *molecule = static_cast<const model_base *>(i);
      if (atom_basis.size() != (sym->atom_basis.size() +
				molecule->primitive_atoms.size())) {
	atom_basis.clear();
	parents = new atom[sym->atom_basis.size()
			   + molecule->primitive_atoms.size()];
	copy_cartesian_atoms(sym, parents);
	bonds.copy_pairs(atom_basis, sym->bonds, sym->atom_basis);
	int_g size = (int)sym->atom_basis.size();
	real_l move[3] = {0.0, 0.0, 0.0};
	molecule->primitive_atoms.add_atoms_and_bonds(atom_basis, bonds,
						      molecule->bonds,
						      parents, size,
						      move, "Molecule",
						      i->get_model_name());
      }
    } else { // !keep
      // Todo - don't really like this
      std::map<int, atom> basis = atom_basis;
      atom_basis.clear();
      bond_data old_bonds = bonds;
      bonds.clear_pairs();
      atom_tree disp;
      bool centre[3] = {false, false, false};
      generate_primitive_atom_display(disp, centre);
      // Can't use shift as it moves all the atoms.
      real_l trans[3] = {0.0, 0.0, 0.0};
      // By not using sym I should preserve the edit flags
      disp.copy_atoms_and_bonds(atom_basis, bonds, old_bonds,
				parents, trans);
    }
    std::map<int, atom>::iterator ptr;
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
      if (ptr->second->get_edit_flag()) {
	coord_type c[3];
	ptr->second->get_cartesian_coords(c);
	c[0] += shift[0];
	c[1] += shift[1];
	c[2] += shift[2];
	ptr->second->set_cartesian_coords(c);
      }
    }
    if (!keep) {
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      sym->get_primitive_lattice(va, vb, vc);
      check_symmetry(parents, va, vb, vc);
      delete [] parents;
    }
  }
  complete(false);
  return redraw;
}

bool DLV::model_base::move_atoms(model *m, const real_l x, const real_l y,
				 const real_l z, const real_l c_x,
				 const real_l c_y, const real_l c_z,
				 real_l shift[3], const bool keep)
{
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = true;
  if (atom_basis.size() == 0) {
    // shift is zero for setup call
    redraw = false;
    group_names = sym->group_names;
    copy_lattice(sym);
    //copy_symmetry_ops(sym, false);
    copy_bravais_lattice(sym);
    atom *parents = 0;
    // selections are only in display list - Ouch!
    atom_tree disp;
    bool centre[3] = {false, false, false};
    sym->generate_primitive_atom_display(disp, centre);
    if (keep)
      sym->display_atoms.copy_selected_atoms(disp, 0, "Atoms", false,
					     get_number_of_periodic_dims());
    else
      sym->display_atoms.copy_selected_atoms(disp, 1, "Atoms", false,
					     get_number_of_periodic_dims());
    disp.edit_atoms_and_bonds(atom_basis, bonds, sym->bonds,
			      parents, keep, true);
    copy_symmetry_ops(sym, false);
    if (!keep) {
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      sym->get_primitive_lattice(va, vb, vc);
      check_symmetry(parents, va, vb, vc);
    }
    delete [] parents;
    shift[0] = 0.0;
    shift[1] = 0.0;
    shift[2] = 0.0;
  } else {
    // new shift is x_p - x_c, so move shifted atoms by this - shift
    real_l move[3];
    move[0] = x - c_x - shift[0];
    move[1] = y - c_y - shift[1];
    move[2] = z - c_z - shift[2];
    shift[0] = x - c_x;
    shift[1] = y - c_y;
    shift[2] = z - c_z;
    std::map<int, atom>::iterator ptr;
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
      if (ptr->second->get_edit_flag()) {
	coord_type c[3];
	ptr->second->get_cartesian_coords(c);
	c[0] += move[0];
	c[1] += move[1];
	c[2] += move[2];
	ptr->second->set_cartesian_coords(c);
      }
    }
  }
  complete(false);
  return redraw;
}

void DLV::model_base::generate_wulff_data(const std::list<wulff_data> &planes,
					  string labels[], real_g rgb[][3],
					  real_l normals[][3],
					  int_g &nnormals,
					  real_l (* &myverts)[3],
					  int_g ** &face_vertices,
					  int_g * &nface_vertices,
					  int_g &nfaces,
					  int_g &nvertices) const
{
  // BUG
}

bool DLV::model_base::wulff(model *m, const real_l r, data_object *data)
{
  // Todo - if planes all have terminations specified then a version that
  // adjusts things to correctly terminate the surfaces?
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = (atom_basis.size() != 0);
  atom_basis.clear();
  // from generate_wulff_plot
  //wulff_plot *plot = dynamic_cast<wulff_plot *>(data);
  //const std::list<wulff_data> &planes = plot->get_wulff_data();
  const std::list<wulff_data> &planes = data->get_wulff_data();
  int_g count = 0;
  std::list<wulff_data>::const_iterator ptr;
  for (ptr = planes.begin(); ptr != planes.end(); ++ptr ) {
    // Todo - does the centring cause problems?
    count += 2;
  }
  int_g nnormals = 0;
  real_l (*normals)[3] = new_local_array2(real_l, count, 3);
  int_g nvertices = 0;
  int_g nfaces = 0;
  int_g **face_vertices = 0;
  int_g *nface_vertices = 0;
  real_l (*verts)[3] = 0;
  string *labels = 0;
  real_g (*rgb)[3] = 0;
  sym->generate_wulff_data(planes, labels, rgb, normals, nnormals, verts,
			   face_vertices, nface_vertices, nfaces, nvertices);
  // scale vertices so one furtherest from origin is distance r
  real_l len = 0.0;
  for (int i = 0; i < nvertices; i++) {
    real_l l = verts[i][0] * verts[i][0] + verts[i][1] * verts[i][1]
      + verts[i][2] * verts[i][2];
    if (l > len)
      len = l;
  }
  len = sqrt(len);
  real_l scale = r / len;
  for (int i = 0; i < nvertices; i++) {
    verts[i][0] *= scale;
    verts[i][1] *= scale;
    verts[i][2] *= scale;
  }
  //debug
  /*
  for (int i = 0; i < nfaces; i++) {
    if (nface_vertices[i] > 2) {
      for (int j = 0; j < nface_vertices[i]; j++) {
	fprintf(stderr, "face %d, vertex %f %f %f\n", i,
		verts[face_vertices[i][j]][0],
		verts[face_vertices[i][j]][1],
		verts[face_vertices[i][j]][2]);
      }
    }
  }
  */
  // Try to guess how many cells we need for the radius
  // * 2 for cells centred both sides of origin
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  sym->get_primitive_lattice(a, b, c);
  int_g na = 1;
  int_g nb = 1;
  int_g nc = 1;
  // convert each vertex to a fractional coord, use max fractional value
  // in each lattice vector as the number of cells from the origin.
  real_l cell[3][3];
  cell[0][0] = a[0];
  cell[0][1] = a[1];
  cell[0][2] = a[2];
  cell[1][0] = b[0];
  cell[1][1] = b[1];
  cell[1][2] = b[2];
  cell[2][0] = c[0];
  cell[2][1] = c[1];
  cell[2][2] = c[2];
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  real_l max_a = 1.0;
  real_l max_b = 1.0;
  real_l max_c = 1.0;
  for (int k = 0; k < nvertices; k++) {
    real_l frac_a = inverse[0][0] * verts[k][0] + inverse[0][1] * verts[k][1]
      + inverse[0][2] * verts[k][2];
    real_l frac_b = inverse[1][0] * verts[k][0] + inverse[1][1] * verts[k][1]
      + inverse[1][2] * verts[k][2];
    real_l frac_c = inverse[2][0] * verts[k][0] + inverse[2][1] * verts[k][1]
      + inverse[2][2] * verts[k][2];
    if (abs(frac_a) > max_a)
      max_a = abs(frac_a);
    if (abs(frac_b) > max_b)
      max_b = abs(frac_b);
    if (abs(frac_c) > max_c)
      max_c = abs(frac_c);
  }
  // also loop over all midpoint pairs, just to try to cover all options
  for (int k = 0; k < nvertices; k++) {
    for (int l = k + 1; l < nvertices; l++) {
      real_l va = (verts[k][0] + verts[l][0] ) / 2.0;
      real_l vb = (verts[k][1] + verts[l][1] ) / 2.0;
      real_l vc = (verts[k][2] + verts[l][2] ) / 2.0;
      real_l frac_a = inverse[0][0] * va + inverse[0][1] * vb
	+ inverse[0][2] * vc;
      real_l frac_b = inverse[1][0] * va + inverse[1][1] * vb
	+ inverse[1][2] * vc;
      real_l frac_c = inverse[2][0] * va + inverse[2][1] * vb
	+ inverse[2][2] * vc;
      if (abs(frac_a) > max_a)
	max_a = abs(frac_a);
      if (abs(frac_b) > max_b)
	max_b = abs(frac_b);
      if (abs(frac_c) > max_c)
	max_c = abs(frac_c);
    }
  }
  na = 2 * (to_integer(max_a) + 1) + 1;
  nb = 2 * (to_integer(max_b) + 1) + 1;
  nc = 2 * (to_integer(max_c) + 1) + 1;
  // Todo - a min size for a,b,c that guarantees to be inside the shape without
  // any testing necessary?
  // if termination specified try to work out offset (as distance from plane)
  // required to terminate on that layer
  real_g *offsets = new_local_array1(real_g, nfaces);
  // planes.size() == nfaces? Todo
  ptr = planes.begin();
  for (int face = 0; face < nfaces; face++) {
    if (ptr->termination == -1 or nface_vertices[face] < 3)
      offsets[face] = 0.0;
    else
      offsets[face] = calc_plane_offset(m, *ptr, verts, face_vertices[face],
					nface_vertices[face]);
    // faces come in h,k,l -h,-k,-l pairs from single data item
    if (face % 2 == 1)
      ++ptr;
  }
  atom_tree cluster;
  bool centre[3] = { true, true, true };
  sym->generate_primitive_atom_display(cluster, centre);
  sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
  atom *parents = 0;
  // cut from wulff plot
  cluster.build_wulff_shape(atom_basis, bonds, sym->bonds, parents,
			    verts, nvertices, face_vertices, nface_vertices,
			    nfaces, offsets);
  delete_local_array(offsets);
  check_point_symmetry(sym, parents);
  delete [] parents;
  complete(false);
  delete [] verts;
  for (int_g i = 0; i < nfaces; i++)
    if (face_vertices[i] != 0)
      delete [] face_vertices[i];
  delete [] nface_vertices;
  delete_local_array(normals);
  return redraw;
}

DLV::real_g DLV::model_base::calc_plane_offset(const model *m,
					       const wulff_data &data,
					       const real_l verts[][3],
					       const int_g face_vertices[],
					       const int_g nface_vertices) const
{
  // assume that termination 0 is at z = 0.0 in the cell of the plane
  // then work out the nearest distance to the plane that gives the
  // termination we want and set the offset to this
  real_g point[3];
  // this is related to the code in build_wulff_shape
  point[0] = verts[face_vertices[0]][0];
  point[1] = verts[face_vertices[0]][1];
  point[2] = verts[face_vertices[0]][2];
  real_g normal[3];
  real_g ba[3];
  ba[0] = verts[face_vertices[1]][0] - verts[face_vertices[0]][0];
  ba[1] = verts[face_vertices[1]][1] - verts[face_vertices[0]][1];
  ba[2] = verts[face_vertices[1]][2] - verts[face_vertices[0]][2];
  real_g ca[3];
  ca[0] = verts[face_vertices[2]][0] - verts[face_vertices[0]][0];
  ca[1] = verts[face_vertices[2]][1] - verts[face_vertices[0]][1];
  ca[2] = verts[face_vertices[2]][2] - verts[face_vertices[0]][2];
  normal[0] = ba[1] * ca[2] - ba[2] * ca[1];
  normal[1] = ba[2] * ca[0] - ba[0] * ca[2];
  normal[2] = ba[0] * ca[1] - ba[1] * ca[0];
  real_g len = sqrt(normal[0] * normal[0] + normal[1] * normal[1]
		    + normal[2] * normal[2]);
  // normalize
  normal[0] /= len;
  normal[1] /= len;
  normal[2] /= len;
  // find distance of origin
  real_g plane_d = point[0] * normal[0] + point[1] * normal[1]
    + point[2] * normal[2];
  // should be safe since wulff planes shouldn't go through origin
  if (plane_d > 0.0) {
    normal[0] = - normal[0];
    normal[1] = - normal[1];
    normal[2] = - normal[2];
  } else
    plane_d = - plane_d;
  // Now need to know how far the termination should be from the origin.
  model_base *c3d = static_cast<model_base *>(create_atoms("tmp", 3));
  string *labels = 0;
  int_g n = 0;
  real_l r[3][3];
  (void) c3d->generate_surface(m, data.h, data.k, data.l, data.conventional,
			       0.0001, labels, n, r);
  //(void) identify_layers(tol, real_g(c[2]), 0, shift, labels, n);
  // from identify_layers - Todo, duplicate gennerate_surface stuff to avoid
  // extra identify_layers call
  int_g natoms = c3d->get_number_of_primitive_atoms();
  real_l *zbuff = new_local_array1(real_l, natoms);
  int_g *order = new_local_array1(int_g, natoms);
  int_g *layer_index = new_local_array1(int_g, natoms);
  real_l shift = 0.0;
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  c3d->get_primitive_lattice(a, b, c);
  int nlayers = c3d->sort_layers(0.0001, real_g(c[2]), data.termination,
				 shift, layer_index, order, zbuff, natoms);
  if (nlayers == 1)
    return 0.0;
  // depth of cell is c[2], so n multiples of this plus the termination
  // is shift the termination depth?
  real_g depth = shift;
  int_g ncells = to_integer(plane_d / c[2]) + 1;
  for (int i = 1; i <= ncells; i++) {
    if (depth <= plane_d and depth + c[2] >= plane_d) {
      if (plane_d - depth > depth + c[2] - plane_d)
	depth += c[2];
      break;
    }
    depth += c[2];
  }
  //fprintf(stderr, "shift %f, %f %f %d %f %d\n", shift, c[2], plane_d, ncells,
  //	  depth, data.termination);
  delete_local_array(layer_index);
  delete_local_array(order);
  delete_local_array(zbuff);
  if (labels != 0)
    delete [] labels;
  delete c3d;
  //fprintf(stderr, "shift plane by %f percent\n",
  //	  100.0 * (depth - plane_d) / plane_d);
  return (depth - plane_d);
}

bool DLV::model_base::multi_layer(model *m, const real_l r, model *slab)
{
  model_base *sym = static_cast<model_base *>(m);
  model_base *layer = static_cast<model_base *>(slab);
  bool redraw = true;
  atom *parents = 0;
  if (atom_basis.size() == 0) {
    group_names = sym->group_names;
    add_atom_group("Multi Layer");
    copy_lattice(sym);
    copy_bravais_lattice(sym);
    redraw = false;
  }
  int_g size = 0;
  parents = new atom[sym->primitive_atoms.size()
		     + layer->primitive_atoms.size()];
  real_l zmin_sym = 0.0;
  real_l zmax_sym = 0.0;
  sym->primitive_atoms.get_z_range(zmin_sym, zmax_sym);
  real_l zmin_layer = 0.0;
  real_l zmax_layer = 0.0;
  layer->primitive_atoms.get_z_range(zmin_layer, zmax_layer);
  real_l zmin = zmin_sym;
  real_l zmax = zmax_layer + (zmax_sym + r - zmin_layer);
  real_l shift = (zmax + zmin) / 2.0;
  real_l dummy[3] = {0.0, 0.0, 0.0};
  dummy[2] = - shift;
  sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds,
					    sym->bonds, parents, dummy);
  // set atom group
  std::map<int, atom>::const_iterator aptr;
  for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr)
    aptr->second->set_group("Multi Layer", "slab1");
  size = sym->primitive_atoms.size();
  // shift of layer atoms is
  shift = (zmax_sym + r - zmin_layer) - shift;
  layer->primitive_atoms.add_atoms_and_bonds(atom_basis, bonds,
					     layer->bonds, parents,
					     size, shift, "slab2",
					     "Multi Layer");
  copy_symmetry_ops(sym, false);
  coord_type va[3];
  coord_type vb[3];
  coord_type vc[3];
  sym->get_primitive_lattice(va, vb, vc);
  // need to get atoms in cartesian coords
  for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr)
    aptr->second->complete(va, vb, vc, 2);
  check_symmetry(parents, va, vb, vc);
  delete [] parents;
  complete(false);
  return redraw;
}

bool DLV::model_base::fill_geometry(model *m, const model *shape)
{
  // Todo - smarter version building each part separately rather than a
  // single large box.
  model_base *sym = static_cast<model_base *>(m);
  bool redraw = (atom_basis.size() != 0);
  if (!redraw)
    group_names = sym->group_names;
  atom_basis.clear();
  const outline_model *geometry = static_cast<const outline_model *>(shape);
  const std::map<int, std::list<std::list<plane_vertex> > > &polyhedra =
    geometry->get_polyhedra();
  // Todo - cylinders and polyhedra together
  if (polyhedra.size() == 0) {
    const std::map<int, cylinder_data> &cylinders = geometry->get_cylinders();
    int ncylinders = int(cylinders.size());
    real_g *iradius = 0;
    real_g *oradius = 0;
    real_g *dmin = 0;
    real_g *dmax = 0;
    int *odir = 0;
    iradius = new real_g[ncylinders];
    oradius = new real_g[ncylinders];
    dmin = new real_g[ncylinders];
    dmax = new real_g[ncylinders];
    odir = new int[ncylinders];
    std::map<int, cylinder_data>::const_iterator cyl;
    int i = 0;
    for (cyl = cylinders.begin(); cyl != cylinders.end(); ++cyl) {
      iradius[i] = cyl->second.inner_radius;
      oradius[i] = cyl->second.outer_radius;
      dmin[i] = cyl->second.min_p;
      dmax[i] = cyl->second.max_p;
      odir[i] = cyl->second.orientation;
      i++;
    }
    // Try to guess how many cells we need for the cylinder
    // * 2 for cells centred both sides of origin
    coord_type a[3];
    coord_type b[3];
    coord_type c[3];
    sym->get_primitive_lattice(a, b, c);
    int_g na = 1;
    int_g nb = 1;
    int_g nc = 1;
    // biggest length should be (outer rad, 0, biggest z)
    // Todo - smarter version if dmin/dmax are on same side of origin?
    real_l len = 0.0;
    for (int i = 0; i < ncylinders; i++) {
      real_l vlen = oradius[i] * oradius[i] + dmin[i] * dmin[i];
      if (vlen > len)
	len = vlen;
      vlen = oradius[i] * oradius[i] + dmax[i] * dmax[i];
      if (vlen > len)
	len = vlen;
    }
    len = sqrt(len);
    // make all vectors long enough to fit it
    real_l d;
    d = sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
    nc = 2 * (to_integer((len) / d) + 1) + 1;
    d = sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
    nb = 2 * (to_integer((len) / d) + 1) + 1;
    d = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
    na = 2 * (to_integer((len) / d) + 1) + 1;
    atom_tree cluster;
    bool centre[3] = { true, true, true };
    sym->generate_primitive_atom_display(cluster, centre);
    sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
    // cut from geometry
    cluster.build_cylinders(atom_basis, bonds, sym->bonds, iradius, oradius,
			    dmin, dmax, odir, ncylinders);
    //check_point_symmetry(sym, parents);
    complete(false);
    delete [] odir;
    delete [] dmax;
    delete [] dmin;
    delete [] oradius;
    delete [] iradius;    
  } else {
    int_g nobjs = int(polyhedra.size());
    int_g nvertices = 0;
    int_g nplanes = 0;
    real_g (*verts)[3] = 0;
    int_g *planes = 0;
    int_g *planes_per_obj = new_local_array1(int_g, nobjs);
    int i = 0;
    std::map<int, std::list<std::list<plane_vertex> > >::const_iterator p;
    for (p = polyhedra.begin(); p != polyhedra.end(); ++p) {
      planes_per_obj[i] = int(p->second.size());
      nplanes += planes_per_obj[i];
      std::list<std::list<plane_vertex> >::const_iterator v;
      for (v = p->second.begin(); v != p->second.end(); ++v)
	nvertices += int(v->size());
      i++;
    }
    verts = new_local_array2(real_g, nvertices, 3);
    planes = new_local_array1(int_g, nplanes);
    i = 0;
    int j = 0;
    for (p = polyhedra.begin(); p != polyhedra.end(); ++p) {
      std::list<std::list<plane_vertex> >::const_iterator v;
      for (v = p->second.begin(); v != p->second.end(); ++v) {
	planes[i] = int(v->size());
	i++;
	std::list<plane_vertex>::const_iterator x;
	for (x = v->begin(); x != v->end(); ++x) {
	  verts[j][0] = x->x;
	  verts[j][1] = x->y;
	  verts[j][2] = x->z;
	  j++;
	}
      }
    }
    // Try to guess how many cells we need for the radius
    // * 2 for cells centred both sides of origin
    coord_type a[3];
    coord_type b[3];
    coord_type c[3];
    sym->get_primitive_lattice(a, b, c);
    int_g na = 1;
    int_g nb = 1;
    int_g nc = 1;
    // find the furtherest vertex
    real_l len = 0.0;
    for (int i = 0; i < nvertices; i++) {
      real_l vlen = verts[i][0] * verts[i][0] + verts[i][1] * verts[i][1]
	+ verts[i][2] * verts[i][2];
      if (vlen > len)
	len = vlen;
    }
    len = sqrt(len);
    // make all vectors long enough to fit it
    real_l d;
    d = sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
    nc = 2 * (to_integer((len) / d) + 1) + 1;
    d = sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
    nb = 2 * (to_integer((len) / d) + 1) + 1;
    d = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
    na = 2 * (to_integer((len) / d) + 1) + 1;
    // Todo - a min size for a,b,c that guarantees to be inside the shape
    // without any testing necessary?
    atom_tree cluster;
    //fprintf(stderr, "n %d %d %d\n", na, nb, nc);
    bool centre[3] = { true, true, true };
    sym->generate_primitive_atom_display(cluster, centre);
    sym->update_display_atoms(cluster, 0.0001, na, nb, nc, false, true, true);
    atom *parents = 0;
    // cut from geometry
    cluster.build_box(atom_basis, bonds, sym->bonds, parents, verts, nvertices,
		      planes, nplanes, planes_per_obj, nobjs);
    //check_point_symmetry(sym, parents);
    delete [] parents;
    complete(false);
    delete_local_array(planes);
    delete_local_array(verts);
    delete_local_array(planes_per_obj);
  }
  return redraw;
}

#ifdef ENABLE_DLV_GRAPHICS

bool DLV::model_base::update_origin_atoms(const model *m, real_l &x, real_l &y,
					  real_l &z, const bool frac)
{
  bool redraw = true;
  coord_type p[3];
  if (get_average_selection_position(p)) {
    const model_base *sym = static_cast<const model_base *>(m);
    coord_type a[3];
    coord_type b[3];
    coord_type c[3];
    sym->get_primitive_lattice(a, b, c);
    // include previous shift which has offset the selections
    if (frac) {
      int_g dim = get_number_of_periodic_dims();
      p[0] += a[0] * x;
      p[1] += a[1] * x;
      p[2] += a[2] * x;
      if (dim > 1) {
	p[0] += b[0] * y;
	p[1] += b[1] * y;
	p[2] += b[2] * y;
	if (dim > 2) {
	  p[0] += c[0] * z;
	  p[1] += c[1] * z;
	  p[2] += c[2] * z;
	}
      }
    } else {
      p[0] += x;
      p[1] += y;
      p[2] += z;
    }
    if (atom_basis.size() == 0) {
      set_primitive_lattice(a, b, c);
      copy_bravais_lattice(sym);
      redraw = false;
    }
    atom_basis.clear();
    real_l shift[3] = { 0.0, 0.0, 0.0 };
    shift[0] = -p[0];
    shift[1] = -p[1];
    shift[2] = -p[2];
    atom *parents = 0;
    sym->primitive_atoms.copy_atoms_and_bonds(atom_basis, bonds,
					      sym->bonds, parents, shift);
    copy_symmetry_ops(sym, true);
    shift_cartesian_operators(sym, p[0], p[1], p[2], a, b, c);
    complete_translators(a, b, c);
    check_symmetry(parents, a, b, c);
    delete [] parents;
    x = p[0];
    y = p[1];
    z = p[2];
    if (frac)
      cartesian_to_fractional_coords(x, y, z);
    complete(false);
  }
  return redraw;;
}

void DLV::model_base::transform_atom(const model *m, const int_g atn, real_l &x,
				     real_l &y, real_l &z, const bool keep,
				     const real_g matrix[4][4],
				     const real_g translate[3],
				     const real_g centre[3])
{
  real_g p[3];
  p[0] = x;
  p[1] = y;
  p[2] = z;
  display_obj::transform_point(p, matrix, translate, centre);
  x = p[0];
  y = p[1];
  z = p[2];
  (void) insert_atom(m, atn, x, y, z, false, keep);
}

void DLV::model_base::transform_model(const model *m, const model *i,
				      real_l &x, real_l &y, real_l &z,
				      real_l &c_x, real_l &c_y, real_l &c_z,
				      const bool keep,
				      const real_g matrix[4][4],
				      const real_g translate[3],
				      const real_g centre[3])
{
  const model_base *sym = static_cast<const model_base *>(m);
  real_g p[3];
  p[0] = x;
  p[1] = y;
  p[1] = z;
  display_obj::transform_point(p, matrix, translate, centre);
  x = p[0];
  y = p[1];
  z = p[2];
  p[0] = c_x;
  p[1] = c_y;
  p[1] = c_z;
  display_obj::transform_point(p, matrix, translate, centre);
  c_x = p[0];
  c_y = p[1];
  c_z = p[2];
  atom *parents = 0;
  if (!keep) {
    // Todo - don't really like this
    std::map<int, atom> basis = atom_basis;
    atom_basis.clear();
    bond_data old_bonds = bonds;
    bonds.clear_pairs();
    atom_tree disp;
    bool centre[3] = {false, false, false};
    generate_primitive_atom_display(disp, centre);
    // Can't use shift as it moves all the atoms.
    real_l trans[3] = {0.0, 0.0, 0.0};
    // By not using sym I should preserve the edit flags
    disp.copy_atoms_and_bonds(atom_basis, bonds, old_bonds, parents, trans);
  }
  std::map<int, atom>::iterator ptr;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
    if (ptr->second->get_edit_flag()) {
      coord_type c[3];
      ptr->second->get_cartesian_coords(c);
      p[0] = c[0];
      p[1] = c[2];
      p[1] = c[2];
      display_obj::transform_point(p, matrix, translate, centre);
      c[0] = p[0];
      c[1] = p[1];
      c[2] = p[2];
      ptr->second->set_cartesian_coords(c);
    }
  }
  if (!keep) {
    coord_type va[3];
    coord_type vb[3];
    coord_type vc[3];
    sym->get_primitive_lattice(va, vb, vc);
    check_symmetry(parents, va, vb, vc);
    delete [] parents;
  }
  complete(false);
}

void DLV::model_base::transform_move(model *m, real_l &x, real_l &y,
				     real_l &z, real_l &c_x,
				     real_l &c_y, real_l &c_z,
				     real_l shift[3], const bool keep,
				     const real_g matrix[4][4],
				     const real_g translate[3],
				     const real_g centre[3])
{
  //model_base *sym = static_cast<model_base *>(m);
  // Don't think atom_basis can be zero
  real_g p[3];
  p[0] = x;
  p[1] = y;
  p[1] = z;
  display_obj::transform_point(p, matrix, translate, centre);
  x = p[0];
  y = p[1];
  z = p[2];
  std::map<int, atom>::iterator ptr;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
    if (ptr->second->get_edit_flag()) {
      coord_type c[3];
      ptr->second->get_cartesian_coords(c);
      p[0] = c[0] - c_x;
      p[1] = c[2] - c_y;
      p[1] = c[2] - c_z;
      display_obj::transform_point(p, matrix, translate, centre);
      c[0] = p[0] + c_x;
      c[1] = p[1] + c_y;
      c[2] = p[2] + c_z;
      ptr->second->set_cartesian_coords(c);
    }
  }
  p[0] = c_x;
  p[1] = c_y;
  p[1] = c_z;
  display_obj::transform_point(p, matrix, translate, centre);
  c_x = p[0];
  c_y = p[1];
  c_z = p[2];
  shift[0] = x - c_x;
  shift[1] = y - c_y;
  shift[2] = z - c_z;
  complete(false);
}

#endif // ENABLE_DLV_GRAPHICS
