
#ifndef DLV_BASE_EXPORTS
#define DLV_BASE_EXPORTS

namespace DLV {

  bool initialise(const char prefs_file[]);
  bool init_project(const char prefs_file[]);

}

#endif // DLV_BASE_EXPORTS
