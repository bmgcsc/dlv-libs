
#include <iostream>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
//#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
//#  include "../graphics/calculations.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
#include "op_objs.hxx"
#include "extern_data.hxx"

bool DLV::create_volume(const char name[], const bool kspace)
{
  char message[256];
  if (op_volume::create(name, kspace, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_sphere(const char name[], const bool kspace)
{
  char message[256];
  if (op_sphere::create(name, kspace, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_plane(const char name[], const bool kspace)
{
  char message[256];
  if (op_plane::create(name, kspace, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_line(const char name[], const bool kspace)
{
  char message[256];
  if (op_line::create(name, kspace, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_point(const char name[], const bool kspace)
{
  char message[256];
  if (op_point::create(name, kspace, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_wulff(const char name[])
{
  char message[256];
  if (op_wulff::create(name, message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

bool DLV::create_leed()
{
  char message[256];
  if (op_leed::create(message, 256) == nullptr) {
    std::cout << message << std::endl;
    return false;
  } else
    return true;
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::update_current_slice(const int_g object, const int_g plane,
					 char message[], const int_g mlen)
{
  return operation::update_current_slice(object, plane, message, mlen);
}

DLVreturn_type DLV::update_current_cut(const int_g object, const int_g plane,
				       char message[], const int_g mlen)
{
  return operation::update_current_cut(object, plane, message, mlen);
}

DLVreturn_type DLV::update_current_extension(const int_g object, const int_g na,
					     const int_g nb, const int_g nc,
					     char message[], const int_g mlen)
{
  return operation::update_current_extension(object, na, nb, nc, message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS
