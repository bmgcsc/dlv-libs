
#include <list>
#include <map>
#include <iostream>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "math_fns.hxx"
#include "atom_model.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "op_model.hxx"
#include "op_atoms.hxx"
#include "data_objs.hxx"
#include "symmetry.hxx"
#include "atom_pairs.hxx"
#include "model_atoms.hxx"
#include "extern_model.hxx"

DLV::model *DLV::create_atoms(const string model_name, const int_g dim)
{
  model *structure = 0;
  switch (dim) {
  case 0:
    structure = create_molecule(model_name);
    break;
  case 1:
    structure = create_polymer(model_name);
    break;
  case 2:
    structure = create_slab(model_name);
    break;
  case 3:
    structure = create_crystal(model_name);
    break;
  case 4:
    structure = create_surface(model_name);
    break;
  }
  structure->add_atom_group("Atoms");
  structure->add_atom_group("Atom Types");
  return structure;
}

static DLVreturn_type render_rspace_model(DLV::operation *op, char message[], const int mlen)
{
  DLVreturn_type ok = DLV_OK;
#ifdef ENABLE_DLV_GRAPHICS
  ok = op->render_r(message, mlen);
#endif // GRAPHICS
  return ok;
}

DLV::operation *DLV::load_cif(const char name[], const char file_name[],
			      char message[], const int mlen)
{
  DLV::operation *op = DLV::load_cif_file::create(name, file_name, true,
						  message, mlen);
  if (op != nullptr) {
    if (render_rspace_model(op, message, mlen) == DLV_OK)
      return op;
  }
  return nullptr;
}

DLVreturn_type DLV::create_supercell(const char name[], int_g &na, int_g &nb,
				     int_g &nc, bool &conv, char message[],
				     const int_g mlen)
{
  return supercell::create(name, na, nb, nc, conv, message, mlen);
}

DLVreturn_type DLV::update_supercell(const int_g matrix[3][3],
				     const bool conv_cell, char message[],
				     const int_g mlen)
{
  return DLV::operation::update_supercell(matrix, conv_cell, message, mlen);
}

DLVreturn_type DLV::delete_symmetry(const char name[], char message[],
				    const int_g mlen)
{
  return del_symmetry::create(name, message, mlen);
}

DLVreturn_type DLV::view_to_molecule(const char name[], char message[],
				     const int_g mlen)
{
  return molecule_view::create(name, message, mlen);
}

DLVreturn_type DLV::cell_to_lattice(const char name[], const int_g na,
				     const int_g nb, const int_g nc,
				    const bool conv, const bool centred,
				    const bool edges,
				    char message[], const int_g mlen)
{
  return remove_lattice::create(name, na, nb, nc, conv, centred, edges,
				message, mlen);
}

DLVreturn_type DLV::edit_lattice(const char name[], real_l values[6],
				 bool usage[6], char message[],
				 const int_g mlen)
{
  return alter_lattice::create(name, values, usage, message, mlen);
}

DLVreturn_type DLV::update_edit_lattice(const real_l values[6], char message[],
					const int_g mlen)
{
  return operation::update_edit_lattice(values, message, mlen);
}

DLVreturn_type DLV::shift_origin(const char name[], char message[],
				 const int_g mlen)
{
  return origin_shift::create(name, message, mlen);
}

DLVreturn_type DLV::update_origin(const real_l x, const real_l y,
				  const real_l z, const bool frac,
				  char message[], const int_g mlen)
{
  return operation::update_origin(x, y, z, frac, message, mlen);
}

DLVreturn_type DLV::update_origin_symm(real_l &x, real_l &y, real_l &z,
				       const bool frac, char message[],
				       const int_g mlen)
{
  return operation::update_origin_symm(x, y, z, frac, message, mlen);
}

DLVreturn_type DLV::vacuum_gap(const char name[], const real_l c, const real_l origin,
			       char message[], const int_g mlen)
{
  return vacuum_cell::create(name, c, origin, message, mlen);
}

DLVreturn_type DLV::update_vacuum(const real_l c, const real_l origin,
				  char message[], const int mlen)
{
  return operation::update_vacuum(c, origin, message, mlen);
}

DLVreturn_type DLV::cut_cluster(const char name[], int_g &n,
				char message[], const int_g mlen)
{
  return cluster::create(name, n, message, mlen);
}

DLVreturn_type DLV::cut_cluster(const char name[], int_g &n,
				const real_l x, const real_l y,
				const real_l z, char message[],
				const int_g mlen)
{
  return cluster::create(name, n, x, y, z, message, mlen);
}

DLVreturn_type DLV::update_cluster(const int_g n, char message[],
				   const int_g mlen)
{
  return operation::update_cluster(n, message, mlen);
}

/*
DLVreturn_type DLV::operation::update_cluster(const int_g n, const real_l x,
					      const real_l y, const real_l z,
					      char message[], const int_g mlen)
*/

DLVreturn_type DLV::cut_slab(const char name[], int_g &nlayers, string * &names,
			     int_g &n, int_g &w3D, char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (operation::get_current_model()->is_crystal()) {
    w3D = 1;
    ok = slab_from_bulk::create(name, nlayers, names, n, message, mlen);
  } else {
    w3D = 0;
    ok = slab_from_surface::create(name, nlayers, names, n, message, mlen);
  }
  return ok;
}

DLVreturn_type DLV::cut_slab(const char name[], const int_g h, const int_g k,
			     const int_g l, const bool conventional,
			     const real_g tol, int_g &nlayers, string * &names,
			     int_g &n, char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (operation::get_current_model()->is_crystal()) {
    ok = slab_from_bulk::create(name, h, k, l, conventional, tol, nlayers,
				names, n, message, mlen);
  } else {
    ok = DLV_ERROR;
    strncpy(message, "slab (hkl) already specified by surface", mlen);
  }
  return ok;
}

DLVreturn_type DLV::update_slab(const int_g h, const int_g k, const int_g l,
				const bool conventional, const real_g tol,
				int_g &nlayers, string * &labels, int_g &n,
				char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  ok = operation::update_slab(h, k, l, conventional, tol, nlayers,
			      labels, n, message, mlen);
  return ok;
}

DLVreturn_type DLV::update_slab(const real_g tol, int_g &nlayers,
				string * &labels, int_g &n,
				char message[], const int_g mlen)
{
  return operation::update_slab(tol, nlayers, labels, n, message, mlen);
}

DLVreturn_type DLV::update_slab(const int_g nlayers, char message[], const int_g mlen)
{
  return operation::update_slab(nlayers, message, mlen);
}

DLVreturn_type DLV::update_slab_term(const int_g term, char message[], const int_g mlen)
{
  return operation::update_slab_term(term, message, mlen);
}

DLVreturn_type DLV::cut_surface(const char name[], string * &names, int_g &n,
				char message[], const int_g mlen)
{
  return surface_edit::create(name, names, n, message, mlen);
}

DLVreturn_type DLV::cut_surface(const char name[], const int_g h,
				const int_g k, const int_g l,
				const bool conventional, const real_g tol,
				string * &names, int_g &n, char message[],
				const int_g mlen)
{
  return surface_edit::create(name, h, k, l, conventional, tol, names, n,
			      message, mlen);
}

DLVreturn_type DLV::update_surface(const int_g h, const int_g k, const int_g l,
				   const bool conventional, const real_g tol,
				   string * &labels, int_g &n,
				   char message[], const int_g mlen)
{
  return operation::update_surface(h, k, l, conventional, tol, labels, n,
				   message, mlen);
}

DLVreturn_type DLV::update_surface(const real_g tol, string * &labels, int_g &n,
				   char message[], const int_g mlen)
{
  return operation::update_surface(tol, labels, n, message, mlen);
}

DLVreturn_type DLV::update_surface(const int_g term, char message[],
				   const int_g mlen)
{
  return operation::update_surface(term, message, mlen);
}

DLVreturn_type DLV::cut_salvage(const char name[], int_g &nlayers,
				int_g &min_layers, char message[],
				const int_g mlen)
{
  return salvage::create(name, nlayers, min_layers, message, mlen);
}

DLVreturn_type DLV::update_salvage(const real_g tol, char message[],
				   const int_g mlen)
{
  return operation::update_salvage(tol, message, mlen);
}

DLVreturn_type DLV::update_salvage(const int_g nlayers, char message[],
				   const int_g mlen)
{
  return operation::update_salvage(nlayers, message, mlen);
}

DLVreturn_type DLV::multilayer_create(const char name[], const real_l r,
				      const int selection, char message[],
				      const int_g mlen)
{
  return multi_layer::create(name, r, selection, message, mlen);
}

DLVreturn_type DLV::multilayer_update(const real_l r, char message[],
				      const int_g mlen)
{
  return operation::multilayer_update(r, message, mlen);
}

DLVreturn_type DLV::fill_geometry_obj(const char name[], const int selection,
				      char message[], const int_g mlen)
{
  return fill_geometry::create(name, selection, message, mlen);
}

#ifndef ONLY_ATOM_MODELS

#include "outline.hxx"
#include "shells.hxx"

DLV::model *DLV::create_shells(const string model_name)
{
  if (model_name.length() > 0)
    return new shell_model(model_name);
  else
    return new shell_model("Shells");
}

DLV::model *DLV::create_outline(const string model_name, int dim, float a1,
				float a2, float a3, int dir, int Lat_type)
{
  model *structure = 0;
  switch (dim) {
  case 0:
    structure = new outline_molecule(model_name);
    break;
  case 1:
    structure = new outline_oneD(model_name, a1, dir);
    break;
  case 2:
     structure = new outline_twoD(model_name, a1, a2, dir, Lat_type);
    break;
  case 3: 
    structure = new outline_threeD(model_name, a1, a2, a3, dir, Lat_type);
    break;
  }
  return structure;
}

#endif // ONLY__ATOM_MODELS

#ifdef ENABLE_DLV_GRAPHICS

std::string DLV::update_current_cell()
{
  if (operation::get_current() != nullptr) {
    char message[128];
    if (operation::get_current()->update_cell(message, 128) == DLV_OK)
      return "";
    else
      return message;
  } else
    return "No current model to update";
}

std::string DLV::update_current_lattice(const bool kspace)
{
  if (operation::get_current() != nullptr) {
    char message[128];
    if (operation::get_current()->update_lattice(kspace, message, 128) == DLV_OK)
      return "";
    else
      return message;
  } else
    return "No current model to update";
}

std::string DLV::update_current_atoms()
{
  if (operation::get_current() != nullptr) {
    char message[128];
    if (operation::get_current()->update_atoms(message, 128) == DLV_OK)
      return "";
    else
      return message;
  } else
    return "No current model to update";
}

std::string DLV::update_current_bonds()
{
  if (operation::get_current() != nullptr) {
    char message[128];
    if (operation::get_current()->update_bonds(message, 128) == DLV_OK)
      return "";
    else
      return message;
  } else
    return "No current model to update";
}

DLVreturn_type DLV::update_origin_atoms(real_l &x, real_l &y, real_l &z,
					const bool frac, char message[],
					const int_g mlen)
{
  return operation::update_origin_atoms(x, y, z, frac, message, mlen);
}

DLVreturn_type DLV::edit_atom(const char name[], int_g &atn, const bool all,
			      char message[],const int_g mlen)
{
  return atom_edit::create(name, atn, all, message, mlen);
}

DLVreturn_type DLV::update_atom(const int_g atn, const bool all,
				char message[], const int_g mlen)
{
  return operation::update_atom(atn, all, message, mlen);
}

DLVreturn_type DLV::edit_props(const char name[], int_g &charge,
			       real_g &radius, real_g &red, real_g &green,
			       real_g &blue, int_g &spin, bool &use_charge,
			       bool &use_radius, bool &use_colour,
			       bool &use_spin, const bool all,
			       char message[], const int_g mlen)
{
  return atom_props::create(name, charge, radius, red, green, blue, spin,
			    use_charge, use_radius, use_colour, use_spin, all,
			    message, mlen);
}

DLVreturn_type DLV::update_props(const int_g charge, real_g &radius,
				 const real_g red, const real_g green,
				 const real_g blue, const int_g spin,
				 const bool use_charge, const bool use_radius,
				 const bool use_colour, const bool use_spin,
				 const bool all, char message[],
				 const int_g mlen)
{
  return operation::update_props(charge, radius, red, green, blue, spin,
				 use_charge, use_radius, use_colour, use_spin,
				 all, message, mlen);
}

// update_rod_props???

DLVreturn_type DLV::delete_atoms(const char name[], char message[],
				 const int_g mlen)
{
  return atom_delete::create(name, message, mlen);
}

// update_deleted_atoms?

DLVreturn_type DLV::insert_atom(const char name[], char message[],
				const int_g mlen)
{
  return atom_insert::create(name, message, mlen);
}

DLVreturn_type DLV::update_inserted_atom(const int_g atn, const bool frac,
					 const real_l x, const real_l y,
					 const real_l z, char message[],
					 const int_g mlen)
{
  return operation::update_inserted_atom(atn, frac, x, y, z, message, mlen);
}

DLVreturn_type DLV::switch_atom_coords(const bool frac, const bool displace,
				       real_l &x, real_l &y, real_l &z,
				       char message[], const int_g mlen)
{
  return operation::switch_atom_coords(frac, displace, x, y, z, message, mlen);
}

DLVreturn_type DLV::update_from_selections(real_l &x, real_l &y, real_l &z,
					   char message[], const int_g mlen)
{
  return operation::update_from_selections(x, y, z, message, mlen);
}

DLVreturn_type DLV::set_edit_transform(const bool v, real_l &x, real_l &y,
				       real_l &z, char message[],
				       const int_g mlen)
{
  return operation::set_edit_transform(v, x, y, z, message, mlen);
}

DLVreturn_type DLV::insert_model(const char name[], const int_g selection,
				 char message[], const int_g mlen)
{
  return model_insert::create(name, selection, message, mlen);
}

DLVreturn_type DLV::update_inserted_model(const bool frac, const real_l x,
					  const real_l y, const real_l z,
					  char message[], const int_g mlen)
{
  return operation::update_inserted_model(frac, x, y, z, message, mlen);
}

DLVreturn_type DLV::position_from_selections(real_l &x, real_l &y, real_l &z,
					     char message[], const int_g mlen)
{
  return operation::position_from_selections(x, y, z, message, mlen);
}

DLVreturn_type DLV::move_atoms(const char name[], real_l &x, real_l &y,
			       real_l &z, char message[], const int_g mlen)
{
  return atom_move::create(name, x, y, z, message, mlen);
}

DLVreturn_type DLV::update_move_atoms(const bool frac, const bool displace,
				      const real_l x, const real_l y,
				      const real_l z, char message[],
				      const int_g mlen)
{
  return operation::update_move_atoms(frac, displace, x, y, z, message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS
