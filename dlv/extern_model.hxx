
#ifndef DLV_MODEL_EXPORTS
#define DLV_MODEL_EXPORTS

namespace DLV {

  model *create_atoms(const string model_name, const int_g dim);
#ifndef ONLY_ATOM_MODELS
  model *create_outline(const string model_name, int dim, float a1,
			float a2, float a3, int dir, int LatType);
  model *create_shells(const string model_name);
#endif // ONLY__ATOM_MODELS

  operation *load_cif(const char name[], const char file_name[],
		      char message[], const int_g mlen);

  DLVreturn_type create_supercell(const char name[], int_g &na, int_g &nb,
				  int_g &nc, bool &conv, char message[],
				  const int_g mlen);
  DLVreturn_type update_supercell(const int_g matrix[3][3],const bool conv_cell,
				  char message[], const int_g mlen);
  DLVreturn_type delete_symmetry(const char name[], char message[],
				 const int_g mlen);
  DLVreturn_type view_to_molecule(const char name[], char message[],
				  const int_g mlen);
  DLVreturn_type cell_to_lattice(const char name[], const int_g na,
				 const int_g nb, const int_g nc,
				 const bool conv, const bool centred,
				 const bool edges,
				 char message[], const int_g mlen);
  DLVreturn_type edit_lattice(const char name[], real_l values[6],
			      bool usage[6], char message[], const int_g mlen);
  DLVreturn_type update_edit_lattice(const real_l values[6], char message[],
				     const int_g mlen);
  DLVreturn_type shift_origin(const char name[], char message[],
			      const int_g mlen);
  DLVreturn_type update_origin(const real_l x, const real_l y, const real_l z,
			       const bool frac, char message[],
			       const int_g mlen);
  DLVreturn_type update_origin_symm(real_l &x, real_l &y, real_l &z,
				    const bool frac, char message[],
				    const int_g mlen);
  DLVreturn_type vacuum_gap(const char name[], const real_l c,
			    const real_l origin, char message[], const int_g mlen);
  DLVreturn_type update_vacuum(const real_l c, const real_l origin,
			       char message[], const int_g mlen);
  DLVreturn_type cut_cluster(const char name[], int_g &n, char message[],
			     const int_g mlen);
  DLVreturn_type cut_cluster(const char name[], int_g &n, const real_l x,
			     const real_l y, const real_l z, char message[],
			     const int_g mlen);
  DLVreturn_type update_cluster(const int_g n, char message[],
				const int_g mlen);
  DLVreturn_type cut_slab(const char name[], int_g &nlayers, string * &names,
			  int_g &n, int_g &w3D, char message[], const int_g mlen);
  DLVreturn_type cut_slab(const char name[], const int_g h, const int_g k,
			  const int_g l, const bool conventional,
			  const real_g tol, int_g &nlayers, string * &names,
			  int_g &n, char message[], const int_g mlen);
  DLVreturn_type update_slab(const int_g h, const int_g k, const int_g l,
			     const bool conventional, const real_g tol,
			     int_g &nlayers, string * &labels, int_g &n,
			     char message[], const int_g mlen);
  DLVreturn_type update_slab(const real_g tol, int_g &nlayers, string * &labels,
			     int_g &n, char message[], const int_g mlen);
  DLVreturn_type update_slab(const int_g nlayers, char message[], const int_g mlen);
  DLVreturn_type update_slab_term(const int_g term, char message[], const int_g mlen);
  DLVreturn_type cut_surface(const char name[], string * &names, int_g &n,
			     char message[], const int_g mlen);
  DLVreturn_type cut_surface(const char name[], const int_g h, const int_g k,
			     const int_g l, const bool conventional,
			     const real_g tol, string * &names, int_g &n,
			     char message[], const int_g mlen);
  DLVreturn_type update_surface(const int_g h, const int_g k, const int_g l,
				const bool conventional, const real_g tol,
				string * &labels, int_g &n, char message[],
				const int_g mlen);
  DLVreturn_type update_surface(const real_g tol, string * &labels, int_g &n,
				char message[], const int_g mlen);
  DLVreturn_type update_surface(const int_g term, char message[],
				const int_g mlen);
  DLVreturn_type cut_salvage(const char name[], int_g &nlayers,
			     int_g &min_layers, char message[],
			     const int_g mlen);
  DLVreturn_type update_salvage(const real_g tol, char message[],
				const int_g mlen);
  DLVreturn_type update_salvage(const int_g nlayers, char message[],
				const int_g mlen);
  DLVreturn_type multilayer_create(const char name[], const real_l r,
				   const int selection, char message[],
				   const int_g mlen);
  DLVreturn_type multilayer_update(const real_l r, char message[],
				   const int_g mlen);
  DLVreturn_type fill_geometry_obj(const char name[], const int selection,
				   char message[], const int_g mlen);

#ifdef ENABLE_DLV_GRAPHICS

  std::string update_current_cell();
  std::string update_current_lattice(const bool kspace);
  std::string update_current_atoms();
  std::string update_current_bonds();

  DLVreturn_type update_origin_atoms(real_l &x, real_l &y, real_l &z,
				     const bool frac, char message[],
				     const int_g mlen);
  DLVreturn_type edit_atom(const char name[], int_g &atn, const bool all,
			   char message[],const int_g mlen);
  DLVreturn_type update_atom(const int_g atn, const bool all,
			     char message[], const int_g mlen);
  DLVreturn_type edit_props(const char name[], int_g &charge, real_g &radius,
			    real_g &red, real_g &green, real_g &blue,
			    int_g &spin, bool &use_charge, bool &use_radius,
			    bool &use_colour, bool &use_spin, const bool all,
			    char message[], const int_g mlen);
  DLVreturn_type update_props(const int_g charge, real_g &radius,
			      const real_g red, const real_g green,
			      const real_g blue, const int_g spin,
			      const bool use_charge, const bool use_radius,
			      const bool use_colour, const bool use_spin,
			      const bool all, char message[], const int_g mlen);
  DLVreturn_type delete_atoms(const char name[], char message[],
			      const int_g mlen);
  DLVreturn_type insert_atom(const char name[], char message[],
			     const int_g mlen);
  DLVreturn_type update_inserted_atom(const int_g atn, const bool frac,
				      const real_l x, const real_l y,
				      const real_l z, char message[],
				      const int_g mlen);
  DLVreturn_type switch_atom_coords(const bool frac, const bool displace,
				    real_l &x, real_l &y, real_l &z,
				    char message[], const int_g mlen);
  DLVreturn_type update_from_selections(real_l &x, real_l &y, real_l &z,
					char message[], const int_g mlen);
  DLVreturn_type set_edit_transform(const bool v, real_l &x, real_l &y,
				    real_l &z, char message[],
				    const int_g mlen);
  DLVreturn_type insert_model(const char name[], const int_g selection,
			      char message[], const int_g mlen);
  DLVreturn_type update_inserted_model(const bool frac, const real_l x,
				       const real_l y, const real_l z,
				       char message[], const int_g mlen);
  DLVreturn_type position_from_selections(real_l &x, real_l &y, real_l &z,
					  char message[], const int_g mlen);
  DLVreturn_type move_atoms(const char name[], real_l &x, real_l &y,
			    real_l &z, char message[], const int_g mlen);
  DLVreturn_type update_move_atoms(const bool frac, const bool displace,
				   const real_l x, const real_l y,
				   const real_l z, char message[],
				   const int_g mlen);

#endif // ENABLE_DLV_GRAPHICS

}

#endif // DLV_MODEL_EXPORTS
