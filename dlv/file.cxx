
#include "types.hxx"
#include "boost_lib.hxx"
#include "file.hxx"
#include "file_impl.hxx"

DLV::file_obj *DLV::file_obj::create(const string dir, const string local_name,
				     const bool is_local, const bool link)
{
  if (is_local)
    return new local_file(dir, local_name, local_name, link);
  else
    return new remote_file(dir, local_name, local_name, link);
}

DLV::file_obj *DLV::file_obj::create(const string dir, const string local_name,
				     const string other_name,
				     const bool is_local, const bool link)
{
  if (is_local)
    return new local_file(dir, local_name, other_name, link);
  else
    return new remote_file(dir, local_name, other_name, link);
}

DLV::file_obj::~file_obj()
{
  // Todo?
}

void DLV::file_obj::set_run_directory(const string dir, const string)
{
  assign_run_dir(dir);
}

void DLV::remote_file::set_run_directory(const string dir, const string host)
{
  assign_run_dir(dir);
  hostname = host;
}

DLV::string DLV::file_obj::get_filename() const
{
  return (directory + filename);
}

DLV::string DLV::file_obj::get_runname() const
{
  return (run_dir + run_as_name);
}

DLV::string DLV::remote_file::get_filename() const
{
  return file_obj::get_filename();
}

DLV::string DLV::remote_file::get_runname() const
{
  // Todo - windows dir sep issues
  string name = hostname;
  name += ":";
  name += file_obj::get_runname();
  return name;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::file_obj *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::file_obj("recover", "", "", false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::local_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::local_file("recover", "", "", false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::remote_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::remote_file("recover", "", "", false);
    }

  }
}

template <class Archive>
void DLV::file_obj::serialize(Archive &ar, const unsigned int version)
{
  ar & directory;
  ar & filename;
  ar & run_as_name;
  ar & run_dir;
  ar & fast_copy;
}

template <class Archive>
void DLV::local_file::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<file_obj>(*this);
  loaded = false;
}

template <class Archive>
void DLV::local_file::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<file_obj>(*this);
}

template <class Archive>
void DLV::remote_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<file_obj>(*this);
  ar & hostname;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::file_obj)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::local_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::remote_file)

DLV_SPLIT_EXPLICIT(DLV::local_file)
DLV_NORMAL_EXPLICIT(DLV::remote_file)
DLV_NORMAL_EXPLICIT(DLV::file_obj)

#endif // DLV_USES_SERIALIZE
