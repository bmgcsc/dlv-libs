
#ifndef DLV_FILE_OBJECTS
#define DLV_FILE_OBJECTS

namespace DLV {

  class file_obj {
  public:
    static file_obj *create(const string dir, const string local_name,
			    const bool is_local, const bool link = false);
    static file_obj *create(const string dir, const string local_name,
			    const string other_name, const bool is_local,
			    const bool link = false);

    virtual ~file_obj();

    virtual void set_run_directory(const string dir, const string host);
    virtual string get_filename() const;
    virtual string get_runname() const;
    string get_runbase() const;
    bool use_fast_copy() const;

    // public for serialization
    file_obj(const string dir, const string name, const string run_name,
	     const bool link);

  protected:
    void assign_run_dir(const string dir);

  private:
    string directory;
    string filename;
    string run_as_name;
    string run_dir;
    bool fast_copy;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::file_obj)

#endif // DLV_USES_SERIALIZE

inline DLV::file_obj::file_obj(const string dir, const string name,
			       const string run_name, const bool link)
  : directory(dir), filename(name), run_as_name(run_name), fast_copy(link)
{
}

inline void DLV::file_obj::assign_run_dir(const string dir)
{
  run_dir = dir;
}

inline DLV::string DLV::file_obj::get_runbase() const
{
  return run_as_name;
}

inline bool DLV::file_obj::use_fast_copy() const
{
  return fast_copy;
}

#endif // DLV_FILE_OBJECTS
