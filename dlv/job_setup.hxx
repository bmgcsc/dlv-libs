
#ifndef DLV_JOB_INFO
#define DLV_JOB_INFO

namespace DLV {

  class job_setup_data {
  public:
    job_setup_data(const char *host, const char *wd, const char *sd,
		   const int_g cpus, const int_g nodes, const int_g hours,
		   const int_g memory, const char *project,
		   const char *queue, const bool parallel);

    bool is_parallel() const;
    bool is_local() const;
    string get_scratch_dir() const;
    string get_host() const;
    string get_project() const;
    string get_queue() const;
    int_g get_time() const;
    int_g get_mem() const;
    int_g get_cpus() const;
    int_g get_nodes() const;

  private:
    string hostname;
    string work_directory;
    string scratch_directory;
    int_g n_processors;
    int_g n_nodes;
    int_g walltime;
    int_g memory_per_proc;
    string project_id;
    string queue;
    bool local;
  };

}

inline bool DLV::job_setup_data::is_parallel() const
{
  return (n_processors != 0);
}

inline bool DLV::job_setup_data::is_local() const
{
  return local;
}

inline DLV::string DLV::job_setup_data::get_scratch_dir() const
{
  return scratch_directory;
}

inline DLV::string DLV::job_setup_data::get_host() const
{
  return hostname;
}

inline DLV::string DLV::job_setup_data::get_project() const
{
  return project_id;
}

inline DLV::string DLV::job_setup_data::get_queue() const
{
  return queue;
}

inline DLV::int_g DLV::job_setup_data::get_time() const
{
  return walltime;
}

inline DLV::int_g DLV::job_setup_data::get_mem() const
{
  return memory_per_proc;
}

inline DLV::int_g DLV::job_setup_data::get_cpus() const
{
  return n_processors;
}

inline DLV::int_g DLV::job_setup_data::get_nodes() const
{
  return n_nodes;
}

#endif // DLV_JOB_INFO
