
#include <cstdio>
#include <cstdlib>
#include "types.hxx"
#include "math_fns.hxx"

void DLV::matrix_invert(const real_l matrix[3][3], real_l inverse[3][3],
			const int_g dim)
{
  if (dim == 1)
    inverse[0][0] = 1.0 / matrix[0][0];
  else if (dim == 2) {
    real_l det = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    real_l deti = 1.0 / (det);
    inverse[0][0] = matrix[1][1] * deti;
    inverse[1][0] = -matrix[1][0] * deti;
    inverse[0][1] = -matrix[0][1] * deti;
    inverse[1][1] = matrix[0][0] * deti;
  } else {
    real_l f1 = matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1];
    real_l f2 = matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2];
    real_l f3 = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
    real_l det = matrix[0][0] * f1 + matrix[0][1] * f2 + matrix[0][2] * f3;
    real_l deti = 1.0 / det;
    inverse[0][0] = f1 * deti;
    inverse[1][0] = f2 * deti;
    inverse[2][0] = f3 * deti;
    f1 = matrix[0][0] * deti;
    f2 = matrix[0][1] * deti;
    f3 = matrix[0][2] * deti;
    inverse[0][1] = f3 * matrix[2][1] - f2 * matrix[2][2];
    inverse[1][1] = f1 * matrix[2][2] - f3 * matrix[2][0];
    inverse[2][1] = f2 * matrix[2][0] - f1 * matrix[2][1];
    inverse[0][2] = f2 * matrix[1][2] - f3 * matrix[1][1];
    inverse[1][2] = f3 * matrix[1][0] - f1 * matrix[1][2];
    inverse[2][2] = f1 * matrix[1][1] - f2 * matrix[1][0];
  }
}

DLV::real_l DLV::determinant(const real_l matrix[3][3])
{
  real_l f1 = matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1];
  real_l f2 = matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2];
  real_l f3 = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
  real_l det = matrix[0][0] * f1 + matrix[0][1] * f2 + matrix[0][2] * f3;
  return det;
}

// Gauss-Jordan elimination from Numerical Recipes, n = 2 or 3
bool DLV::linear_solve(real_l a[3][3], real_l b[3], const int_g n)
{
  //int indxc[3];
  //int indxr[3];
  int_g ipiv[3] = { 0, 0, 0 };
  int_g icol;
  int_g irow;
  for (int_g i = 0; i < n; i++) {
    real_l big = 0.0;
    for (int_g j = 0; j < n; j++) {
      if (ipiv[j] != 1) {
	for (int_g k = 0; k < n; k++) {
	  if (ipiv[k] == 0) {
	    if (abs(a[j][k]) >= big) {
	      big = abs(a[j][k]);
	      irow = j;
	      icol = k;
	    }
	  } else if (ipiv[k] > 1)
	    return false; // error
	}
      }
    }
    ipiv[icol]++;
    if (irow != icol) {
      real_l t;
      for (int_g k = 0; k < n; k++) {
	t = a[irow][k];
	a[irow][k] = a[icol][k];
	a[icol][k] = t;
      }
      t = b[irow];
      b[irow] = b[icol];
      b[icol] = t;
    }
    //indxr[i] = irow;
    //indxc[i] = icol;
    if (abs(a[icol][icol]) == 0.0)
      return false;
    real_l pivinv = 1.0 / a[icol][icol];
    a[icol][icol] = 1.0;
    for (int_g k = 0; k < n; k++)
      a[icol][k] *= pivinv;
    b[icol] *= pivinv;
    for (int_g l = 0; l < n; l++) {
      if (l != icol) {
	real_l dum = a[l][icol];
	a[l][icol] = 0.0;
	for (int_g k = 0; k < n; k++)
	  a[l][k] -= a[icol][l] * dum;
	b[l] -= b[icol] * dum;
      }
    }
  }
  // Unscramble inverse matrix in a - I don't bother.
  return true;
}

// Wikipedia - extended Euclids algorithm
// My mods to attempt to get signs correct for ax + by = 1
void DLV::euclid(const int_g a, int_g &x, const int_g b, int_g &y,
		 const int_g gcd)
{
  int_g cur_x = 0;
  int_g cur_y = 1;
  int_g last_x = 1;
  int_g last_y = 0;
  int_g cur_a = std::abs(a);
  int_g cur_b = std::abs(b);
  while (cur_b != 0) {
    int_g t = cur_b;
    int_g q = cur_a / cur_b;
    cur_b = cur_a % cur_b;
    cur_a = t;
    t = cur_x;
    cur_x = last_x - q * cur_x;
    last_x = t;
    t = cur_y;
    cur_y = last_y - q * cur_y;
    last_y = t;
  }
  if (a < 0)
    x = -last_x;
  else
    x = last_x;
  if (b < 0)
    y = -last_y;
  else
    y = last_y;
  if (cur_a != gcd)
    fprintf(stderr, "Error in Euclids algorithm\n");
}
