
#include <list>
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "atom_prefs.hxx"
//#include "symmetry.hxx"
//#include "atom_model.hxx"
//#include "atom_pairs.hxx"
//#include "data_objs.hxx"
#include "model.hxx"
//#include "model_impl.hxx"
// for create
//#include "shells.hxx"
//#include "outline.hxx"
//#include "molecule.hxx"
//#include "polymer.hxx"
//#include "slab.hxx"
//#include "crystal.hxx"
//#include "surface.hxx"

DLV::model::model(const string n) : name(n)
{
  // Todo?
}

DLV::model::~model()
{
}

DLVreturn_type DLV::model::configure_display_prefs(const char [],
						   char message[],
						   const int_g mlen)
{
  strcpy(message, "Todo - model display prefs not implemented");
  return DLV_WARNING;
}

bool DLV::model::is_shell() const
{
  return false;
}

bool DLV::model::is_crystal() const
{
  return false;
}

bool DLV::model::has_salvage() const
{
  return false;
}

bool DLV::model::keep_symmetry() const
{
  return true;
}

void DLV::model::add_atom_group(const string)
{
}

DLVreturn_type DLV::model::create_atom_group(const char [], const char [],
					     const bool, const char [],
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: model doesn't contain atoms", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::model::update_cluster_region(const real_g [], const int_g,
						 const char [], char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: model doesn't contain atoms", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::model::update_cluster_region(const int_g shells[],
						 const int_g nshells,
						 const char group[],
						 char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: model doesn't contain atoms", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::model::clear_cluster_region(const char group[],
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: model doesn't contain atoms", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::model::set_supercell_region(const model *parent,
						const int_g shells[],
						const int_g nshells,
						const char group[],
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: model doesn't contain atoms", mlen);
  return DLV_ERROR;
}

void DLV::model::add_polyhedra()
{
  // BUG - should really have message etc to report this
}

bool DLV::model::add_polygon(coord_type [][3], const int_g,
			     char message[], const int_g mlen)
{
  strncpy(message, "BUG: adding polygon to wrong model type", mlen);
  return false;
}

void DLV::model::add_cylinder(const coord_type, const coord_type,
			      const coord_type, const coord_type,
			      const int_g)
{
  // BUG - should really be able to report it
}

void DLV::model::add_sphere(const coord_type radius)
{
  // BUG
}

void DLV::model::convert_coords(const bool frac, real_l &x,
				real_l &y, real_l &z) const
{
  // Dummy
}

void DLV::model::get_lattice_parameters(coord_type &a, coord_type &b,
					coord_type &c, coord_type &alpha,
					coord_type &beta, coord_type &gamma,
					const bool conventional) const
{
  // BUG
  a = 0.0;
  b = 0.0;
  c = 0.0;
  alpha = 0.0;
  beta = 0.0;
  gamma = 0.0;
}

void DLV::model::set_lattice_and_centre(const int_g lattice, const int_g centre)
{
  // BUG
}

bool DLV::model::set_operators(const string ops[], const int_g nops,
			       const int_g gp, const bool rhomb, const bool)
{
  return false;
}

bool DLV::model::set_fractional_trans_ops(const real_l rotations[][3][3],
					  const real_l translations[][3],
					  const int_g nops)
{
  return false;
}

bool DLV::model::add_cartesian_ghost_atom(const coord_type coords[3])
{
  return false;
}

bool DLV::model::add_cartesian_point_charge(const coord_type coords[3])
{
  return false;
}

void DLV::model::replace_atom_cart_coords(const coord_type coords[][3])
{
  // BUG
}

bool DLV::model::set_atom_charge(const string symbol, const int_g charge,
				 char message[], const int_g mlen)
{
  strncpy(message, "BUG: set_charge called", mlen);
  return false;
}

bool DLV::model::set_core_electrons(const string symbol, const int_g core,
				    char message[], const int_g mlen)
{
  strncpy(message, "BUG: set_core_electrons called", mlen);
  return false;
}

void DLV::model::set_point_charge(const int_g index, const real_l charge,
				  char message[], const int_g mlen)
{
  strncpy(message, "BUG: set_point_charge called", mlen);
}

void DLV::model::set_isotope(const int_g index, const int_g isotope,
			     char message[], const int_g mlen)
{
  strncpy(message, "BUG: set_isotope called", mlen);
}

void DLV::model::add_bond(const int_g atom1, const int_g atom2)
{
  // BUG
}

void DLV::model::group_atoms(const string gp, const string label,
			     const bool atoms[])
{
  // BUG
}

void DLV::model::set_atom_charge(const int_g index, const int_g charge)
{
  // BUG
}

void DLV::model::set_atom_radius(const int_g index, const real_g r)
{
  fprintf(stderr, "Fix - shell model might be setting atom radius\n");
}

void DLV::model::set_atom_crystal03_id(const int_g index, const int_g id)
{
  // BUG
}

void DLV::model::set_atoms_group(const int_g index, const string gpname,
				 const string label)
{
}

void DLV::model::add_shells(const int_g types[], const real_g numbers[],
			    const real_g radii[], const int_g nshells)
{
  // BUG
}

void DLV::model::update_shells(const int_g types[], const real_g numbers[],
			       const real_g radii[], const int_g nshells)
{
  // BUG
}


DLV::string DLV::model::get_formula()
{
  return "";
}

void DLV::model::count_primitive_atom_types(int_g count[],
					    const int_g len) const
{
  return;
}

void DLV::model::count_crystal03_ids(int_g count[], const int_g len) const
{
  return;
}

void DLV::model::set_indexed_asym_atom_type(const int_g id,
					    const int_g i) const
{
  return;
}

void DLV::model::use_which_lattice_parameters(bool usage[6]) const
{
  usage[0] = false;
  usage[1] = false;
  usage[2] = false;
  usage[3] = false;
  usage[4] = false;
  usage[5] = false;
}

DLV::int_g DLV::model::get_atomic_number(const int_g index) const
{
  return 0;
}

void DLV::model::get_atom_properties(const int_g, int_g &, real_g &,
				     real_g &, real_g &, real_g &,
				     int_g &, bool &, bool &,
				     bool &, bool &) const
{
}

void DLV::model::get_asym_atom_spins(int_g atom_spins[], const int_g n) const
{
  for (int_g i = 0; i < n; i++)
    atom_spins[i] = 0;
}

void DLV::model::get_asym_atom_regions(const char group[],
				       int_g regions[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_occupancy(real_g atom_occ[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_crystal03_ids(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_occupancy(real_g atom_occ[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_core_electrons(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_point_charge(real_l charge[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_isotope(int_g isotope[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_debye_waller1(real_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_debye_waller2(real_g atom_ids[],
					const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_debye_waller(real_g atom_dw1[],
				       real_g atom_dw2[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_displacements(real_g atom_disps[][3],
					const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_displacements(real_g atom_disps[][3],
					const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_dw1(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_dw2(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_noccup(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_rod_dw1(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_rod_dw2(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::set_asym_rod_noccup(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_xconst(real_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_nxdis(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_x2const(real_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_nx2dis(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_yconst(real_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_nydis(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_y2const(real_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_ny2dis(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_asym_rod_nzdis(int_g atom_ids[], const int_g n) const
{
  // BUG - dummy
}

void DLV::model::get_primitive_atom_regions(const char group[], int_g regions[],
					    const int_g n) const
{
  // BUG
}

void DLV::model::get_primitive_asym_indices(int_g indices[],
					    const int_g n) const
{
  // BUG
}

void DLV::model::get_asym_to_primitive_indices(int_g indices[],
					       const int_g n) const
{
  // BUG
}

void DLV::model::get_asym_atom_charges(int_g charges[], const int_g n) const
{
  if (n > 0)
    charges[0] = 0;
}

void DLV::model::has_asym_atom_special_periodicity(bool special_periodicity[], 
						   const int_g n) const
{
  if (n > 0)
    special_periodicity[0] = 0;
}

void DLV::model::set_indexed_asym_cart_coords(const coord_type coords[3], 
				  const int_g n) const
{
} 

void DLV::model::get_atom_shifts(const coord_type coords[][3],
				 int_g shifts[][3], int_g prim_id[],
				 int_g asym_id[], const int_g n) const
{
  for (int_g i = 0; i < n; i++) {
    shifts[i][0] = 0;
    shifts[i][1] = 0;
    shifts[i][2] = 0;
    prim_id[i] = -1;
    asym_id[i] = -1;
  }
}

void DLV::model::find_asym_pos(const int_g, const coord_type[3], int_g &,
			       coord_type[3], const coord_type[][3],
			       const int_g[], coord_type [][3], int_g[3],
			       const int_g) const
{
  // BUG
}

void DLV::model::map_atom_data(const real_g [][3], int_g **const,
			       const int_g, const int_g ngrid,
			       real_g (* &map_grid)[3], int_g ** &new_data,
			       int_g &n) const
{
  n = 0;
  map_grid = 0;
  new_data = 0;
}

void DLV::model::map_atom_data(const real_g [][3], real_g **const,
			       const int_g, const int_g ngrid,
			       real_g (* &map_grid)[3], real_g ** &new_data,
			       int_g &n) const
{
  n = 0;
  map_grid = 0;
  new_data = 0;
}

void DLV::model::map_atom_data(const real_g [][3], const real_g [][3],
			       const real_g [][3], const int_g,
			       real_g (* &map_grid)[3], real_g (* &new_data)[3],
			       int_g &n, const real_g, const real_g,
			       const real_g) const
{
  n = 0;
  map_grid = 0;
  new_data = 0;
}

void DLV::model::map_atom_data(const real_g [][3], const real_g [][3],
			       const real_g [][3], const int_g,
			       std::vector<real_g (*)[3]> &, int_g &n,
			       const int_g, const int_g, const real_g,
			       const real_g, const real_g, const real_g) const
{
  n = 0;
}

void DLV::model::map_atom_data(const real_g [][3], const int_g,
			       const std::vector<real_g (*)[3]> &, const int_g,
			       std::vector<real_g (*)[3]> &) const
{
}

bool DLV::model::set_point_group(const char [])
{
  return false;
}

void DLV::model::update_atom_list()
{
  // dummy
}

void DLV::model::set_bond_all()
{
  // Dummy
}

void DLV::model::copy_model(const model *)
{
  // BUG
}

DLV::model *DLV::model::copy_model(const string) const
{
  // BUG
  return nullptr;
}

void DLV::model::update_model(const model *)
{
  // BUG
}

bool DLV::model::supercell(const model *, const int_g [3][3], const bool)
{
  // BUG
  return false;
}

bool DLV::model::delete_symmetry(const model *)
{
  // BUG
  return false;
}

bool DLV::model::view_to_molecule(const model *)
{
  // BUG
  return false;
}

bool DLV::model::view_to_molecule(const model *, const bool, const bool,
				  const bool)
{
  // BUG
  return false;
}

bool DLV::model::cell_to_molecule(const model *, const int_g, const int_g,
				  const int_g, const bool, const bool,
				  const bool)
{
  // BUG
  return false;
}

bool DLV::model::update_lattice_parameters(const model *, const coord_type [6])
{
  // BUG
  return false;
}

bool DLV::model::update_origin(const model *, const real_l, const real_l,
			       const real_l, const bool)
{
  // BUG
  return false;
}

bool DLV::model::update_origin_symm(const model *, real_l &, real_l &,
				    real_l &, const bool)
{
  // BUG
  return false;
}

bool DLV::model::update_vacuum(const model *, const real_l, const real_l)
{
  // BUG
  return false;
}

bool DLV::model::update_cluster(model *, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::update_cluster(model *, const int_g, const real_l,
				const real_l, const real_l)
{
  // BUG
  return false;
}

bool DLV::model::update_cluster(model *, const real_l)
{
  // BUG
  return false;
}

bool DLV::model::edit_atom(model *, const int_g, const bool, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::edit_atom_props(model *, const int_g, real_g &,
				 const real_g, const real_g, const real_g,
				 const int_g, const bool, const bool,
				 const bool, const bool, const bool,
				 const int_g)
{
  // BUG
  return false;
}

bool DLV::model::edit_atom_rod_props(const model *, const int_g, const bool,
				     const int_g, const bool,
				     const int_g, const bool,
				     const bool, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::update_slab(const model *, model *, const int_g, const int_g,
			     const int_g, const bool, const real_g, int_g &,
			     string * &, int_g &, real_l [3][3])
{
  // BUG
  return false;
}

bool DLV::model::update_slab(const model *, const real_g,
			     int_g &, string * &, int_g &)
{
  // BUG
  return false;
}

bool DLV::model::update_slab(const model *, model *, const real_g,
			     const int_g, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::surface_slab(const model *, const real_g, int_g &,
			      string * &, int_g &)
{
  return false;
}

bool DLV::model::surface_slab(const model *, const real_g, int_g)
{
  return false;
}

bool DLV::model::update_surface(const model *, model *, const int_g,
				const int_g, const int_g, const bool,
				const real_g tol, string * &, int_g &,
				real_l [3][3])
{
  // BUG
  return false;
}

bool DLV::model::update_surface(const model *, const real_g, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::update_salvage(const model *, const real_g, const int_g)
{
  // BUG
  return false;
}

bool DLV::model::delete_atoms(model *)
{
  // BUG
  return false;
}

bool DLV::model::insert_atom(const model *, const int_g, const real_l,
			     const real_l, const real_l, const bool,
			     const bool)
{
  // BUG
  return false;
}

bool DLV::model::insert_model(const model *, const model *, const real_l,
			      const real_l, const real_l, const bool,
			      const real_l, const real_l, const real_l,
			      const bool)
{
  // BUG
  return false;
}

bool DLV::model::move_atoms(model *, const real_l, const real_l, const real_l,
			    const real_l, const real_l, const real_l,
			    real_l [3], const bool)
{
  // BUG
  return false;
}

bool DLV::model::wulff(model *m, const real_l r, data_object *data)
{
  // BUG
  return false;
}

bool DLV::model::multi_layer(model *m, const real_l r, model *slab)
{
  // BUG
  return false;
}

bool DLV::model::fill_geometry(model *m, const model *shape)
{
  // BUG
  return false;
}

#ifdef ENABLE_DLV_GRAPHICS

bool DLV::model::has_selected_atoms() const
{
  return false;
}

void DLV::model::list_atom_groupings(render_parent *) const
{
}

DLV::int_g DLV::model::get_atom_selection_info(real_l &x, real_l &y, real_l &z,
					real_l &length, real_l &x2, real_l &y2,
					real_l &z2, real_l &angle, char sym1[],
					char sym2[], char sym3[], real_g &r1,
					real_g &r2, real_g &r3, string &gp,
					string &label) const
{
  x = 0.0;
  y = 0.0;
  z = 0.0;
  length = 0.0;
  x2 = 0.0;
  y2 = 0.0;
  z2 = 0.0;
  angle = 0.0;
  sym1[0] = '\0';
  sym2[0] = '\0';
  sym3[0] = '\0';
  r1 = 0.0;
  r2 = 0.0;
  r3 = 0.0;
  return 0;
}

DLV::int_g DLV::model::get_number_selected_atoms() const
{
  return -1;
}

bool DLV::model::get_selected_positions(coord_type p[3]) const
{
  return false;
}

bool DLV::model::get_selected_positions(coord_type p1[3],
					coord_type p2[3]) const
{
  return false;
}

bool DLV::model::get_selected_positions(coord_type p1[3], coord_type p2[3],
					coord_type p3[3]) const
{
  return false;
}

bool DLV::model::get_average_selection_position(coord_type p[3]) const
{
  p[0] = 0.0;
  p[1] = 0.0;
  p[2] = 0.0;
  return false;
}

DLV::int_g DLV::model::locate_selected_atom() const
{
  return -1;
}

DLV::int_g DLV::model::find_primitive_selection() const
{
  return -1;
}

class DLV::toolkit_obj DLV::model::get_k_ui_obj() const
{
  // BUG
  return toolkit_obj();
}

DLV::int_g DLV::model::get_symmetry_selector() const
{
  return 0;
}

void DLV::model::fix_symmetry_selector(const int_g)
{
}

DLVreturn_type DLV::model::update_outline(render_parent *p,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: update_outline called for incorrect model type",
	  mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::model::update_shells(render_parent *p,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: update_shells called for incorrect model type",
	  mlen);
  return DLV_ERROR;
}

void DLV::model::get_displayed_cell(render_parent *p, int_g &na,
				    int_g &nb, int_g &nc, bool &conv,
				    bool &centre, bool &edges) const
{
  na = 1;
  nb = 1;
  nc = 1;
  conv = false;
  centre = false;
  edges = true;
}

void DLV::model::set_edit_transform(const render_parent *parent, const bool v,
				    real_g m[4][4], real_g t[3], real_g c[3],
				    char message[], const int_g mlen)
{
  // Dummy
}

void DLV::model::deactivate_atom_flags()
{
  // Dummy
}

void DLV::model::activate_atom_flags(const string label, 
				     const bool reset_flags)
{
  // Dummy
}

void DLV::model::toggle_atom_flags(const string label, 
				   const bool reset_flags)
{
  // Dummy
}

void DLV::model::set_atom_flag(const int_g index, const atom_flag_type aft)
{
  // Dummy
}

bool DLV::model::set_atom_index_and_flags(int_g indices[], const int_g value,
					  const bool set_def, char message[],
					  const int_g mlen)
{
  // Dummy
  strncpy(message, "BUG", mlen);
  return false;
}

void DLV::model::set_selected_atom_flags(const bool done)
{
  // Dummy
}

/*
bool DLV::model::map_atom_selections(int_g &n, int_g * &labels,
				     int_g * &atom_types, class atom * &parents,
				     const bool all_atoms,
				     const real_g grid[][3], int_g **const data,
				     const int_g ndata, const int_g ngrid) const
{
  n = 0;
  labels = 0;
  atom_types = 0;
  parents = nullptr;
  return false;
}
*/

bool DLV::model::map_selected_atoms(int_g &n, int_g * &labels,
				    int_g (* &shifts)[3],
				    const real_g grid[][3],
				    int_g **const data, const int_g ndata,
				    const int_g ngrid) const
{
  // Dummy
  return false;
}

void DLV::model::set_atom_data(const render_parent *p, class data_object *d,
			       const bool undisplay)
{
  // Dummy
}

void DLV::model::refresh_atom_data(const render_parent *p)
{
  // Dummy
}

bool DLV::model::generate_wulff_plot(std::list<wulff_data> &, real_g (* &)[3],
				     int_g &, real_g (* &)[3], real_g (* &)[3],
				     real_g (* &)[3], string * &, int_l * &,
				     int_l &, int_g * &, int_g &,
				     real_g (* &)[3], int_g &, int_l * &,
				     int_l &) const
{
  // BUG
  return false;
}

bool DLV::model::update_origin_atoms(const model *m, real_l &x, real_l &y,
				     real_l &z, const bool frac)
{
  // BUG
  return false;
}

void DLV::model::transform_atom(const model *, const int_g, real_l &,
				real_l &, real_l &, const bool,
				const real_g[4][4], const real_g[3],
				const real_g[3])
{
  // BUG
}

void DLV::model::transform_model(const model *, const model *, real_l &,
				 real_l &, real_l &, real_l &, real_l &,
				 real_l &, const bool, const real_g[4][4],
				 const real_g[3], const real_g[3])
{
  // BUG
}

void DLV::model::transform_move(model *, real_l &, real_l &, real_l &,
				real_l &, real_l &, real_l &, real_l [3],
				const bool, const real_g[4][4],
				const real_g [3], const real_g [3])
{
  // BUG
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::model::serialize(Archive &ar, const unsigned int version)
{
}

DLV_EXPORT_EXPLICIT(DLV::model)

#endif // DLV_USES_SERIALIZE
