
#include <list>
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "atom_prefs.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "data_objs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "operation.hxx"
#include "extern_model.hxx"
// for create
//#include "shells.hxx"
//#include "outline.hxx"
//#include "molecule.hxx"
//#include "polymer.hxx"
//#include "slab.hxx"
//#include "crystal.hxx"
//#include "surface.hxx"

class poly_centre {
public:
  DLV::real_g pos[3];

  bool operator < (const poly_centre &y) const;
  //bool operator == (const poly_centre &y) const;

private:
  DLV::int_g compare(const poly_centre &y) const;
};

inline bool poly_centre::operator < (const poly_centre &y) const
{
  return (compare(y) < 0);
}

DLV::int_g poly_centre::compare(const poly_centre &y) const
{
  const DLV::real_g tol = 0.0001;
  for (DLV::int_g i = 0; i < 3; i++) {
    if (pos[i] < (y.pos[i] - tol))
      return -1;
    else if (pos[i] > (y.pos[i] + tol))
      return 1;
  }
  return 0;
}

DLV::model_base::~model_base()
{
#ifdef ENABLE_DLV_GRAPHICS
  if (render_rspace != 0)
    delete render_rspace;
  if (render_kspace != 0)
    delete render_kspace;
#endif // ENABLE_DLV_GRAPHICS
}

DLV::model *DLV::model_base::copy_model(const string name) const
{
  model *m = create_atoms(name, get_number_of_periodic_dims());
  m->copy_model(this);
  return m;
}

bool DLV::model_base::has_salvage() const
{
  if (get_model_type() == 4) {
    std::map<int, atom>::const_iterator p;
    for (p = atom_basis.begin(); p != atom_basis.end(); ++p )
      if (!p->second->has_special_periodicity())
	return true;
  }
  return false;
}

bool DLV::model_base::keep_symmetry() const
{
#ifdef ENABLE_DLV_GRAPHICS
  return (render_rspace->get_selection_symmetry() == 0);
#else
  return true;
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::model_base::add_atom_group(const string name)
{
  group_names.push_back(name);
}

DLVreturn_type DLV::model_base::create_atom_group(const char label[],
						  const char group[],
						  const bool all,
						  const char remainder[],
						  char message[],
						  const int_g mlen)
{
  if (strcmp(label, "Atoms") == 0) {
    strncpy(message, "Atoms is a reserved group", mlen);
    return DLV_ERROR;
  } else if (strcmp(label, "Atom Types") == 0) {
    strncpy(message, "Atom Types is a reserved group", mlen);
    return DLV_ERROR;
  } else {
    bool exists = false;
    std::list<string>::const_iterator ptr;
    for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
      if (*ptr == label) {
	exists = true;
	break;
      }
    }
    if (!exists)
      group_names.push_back(label);
    if (display_atoms.group_atoms_from_selections(atom_basis, label,
						  group, all, remainder))
      return DLV_OK;
    else {
      strncpy(message, "Ignoring atoms already in a group", mlen);
      return DLV_WARNING;
    }
  }
}

DLVreturn_type DLV::model_base::update_cluster_region(const real_g radii[],
						      const int_g nradii,
						      const char group[],
						      char message[],
						      const int_g mlen)
{
  if (strcmp(group, "Atoms") == 0) {
    strncpy(message, "Atoms is a reserved group", mlen);
    return DLV_ERROR;
  } else if (strcmp(group, "Atom Types") == 0) {
    strncpy(message, "Atom Types is a reserved group", mlen);
    return DLV_ERROR;
  } else {
    bool exists = false;
    std::list<string>::const_iterator ptr;
    for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
      if (*ptr == group) {
	exists = true;
	break;
      }
    }
    if (!exists)
      group_names.push_back(group);
    std::map<int, atom>::const_iterator aptr;
    for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr) {
      coord_type c[3];
      aptr->second->get_cartesian_coords(c);
      real_l l = sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
      int_g i;
      for (i = 1; i < nradii; i++)
	if (l < radii[i])
	  break;
      char buff[20];
      snprintf(buff, 20, "region %d", i);
      aptr->second->set_group(group, buff);
      if (i < nradii)
	aptr->second->set_atom_flag(flag_done);
      else
	aptr->second->clear_flag();
    }
    return DLV_OK;
  }
}

DLVreturn_type DLV::model_base::update_cluster_region(const int_g shells[],
						      const int_g nshells,
						      const char group[],
						      char message[],
						      const int_g mlen)
{
  if (strcmp(group, "Atoms") == 0) {
    strncpy(message, "Atoms is a reserved group", mlen);
    return DLV_ERROR;
  } else if (strcmp(group, "Atom Types") == 0) {
    strncpy(message, "Atom Types is a reserved group", mlen);
    return DLV_ERROR;
  } else {
    bool exists = false;
    bool has_shells = false;
    std::list<string>::const_iterator ptr;
    for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
      if (*ptr == group)
	exists = true;
      if (*ptr == "Cluster shells")
	has_shells = true;
    }
    if (!has_shells) {
      strncpy(message, "Unable to find cluster shells group", mlen);
      return DLV_ERROR;
    }
    if (!exists)
      group_names.push_back(group);
    std::map<int, atom>::const_iterator aptr;
    for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr) {
      int_g index;
      char buff[64];
      if (sscanf(aptr->second->get_group("Cluster shells").c_str(),
		 "%s %d", buff, &index) != 2)
	index = 0;
      int_g i;
      for (i = 0; i < nshells; i++)
	if (index <= shells[i])
	  break;
      snprintf(buff, 64, "region %d", i + 1);
      aptr->second->set_group(group, buff);
      if (i < nshells)
	aptr->second->set_atom_flag(flag_done);
      else
	aptr->second->clear_flag();
    }
    return DLV_OK;
  }
}

DLVreturn_type DLV::model_base::clear_cluster_region(const char group[],
						     char message[],
						     const int_g mlen)
{
  if (strcmp(group, "Atoms") == 0) {
    strncpy(message, "Atoms is a reserved group", mlen);
    return DLV_ERROR;
  } else if (strcmp(group, "Atom Types") == 0) {
    strncpy(message, "Atom Types is a reserved group", mlen);
    return DLV_ERROR;
  } else {
    bool exists = false;
    std::list<string>::const_iterator ptr;
    for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
      if (*ptr == group) {
	exists = true;
	break;
      }
    }
    if (exists) {
      std::map<int, atom>::const_iterator aptr;
      for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr) {
	aptr->second->clear_group(group);
	aptr->second->clear_flag();
      }
      group_names.pop_back();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::model_base::set_supercell_region(const model *parent,
						     const int_g shells[],
						     const int_g nshells,
						     const char group[],
						     char message[],
						     const int_g mlen)
{
  if (strcmp(group, "Atoms") == 0) {
    strncpy(message, "Atoms is a reserved group", mlen);
    return DLV_ERROR;
  } else if (strcmp(group, "Atom Types") == 0) {
    strncpy(message, "Atom Types is a reserved group", mlen);
    return DLV_ERROR;
  } else {
    bool exists = false;
    std::list<string>::const_iterator ptr;
    for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
      if (*ptr == group)
	exists = true;
    }
    if (exists) {
      strncpy(message, "Supercell region group already exists", mlen);
      return DLV_ERROR;
    } else
      group_names.push_back(group);
    //int region1_size = (shells[1] - shells[0]) / 2;
    coord_type va[3];
    coord_type vb[3];
    coord_type vc[3];
    parent->get_primitive_lattice(va, vb, vc);
    int dim = parent->get_number_of_periodic_dims();
    if (dim == 0) {
      strncpy(message, "Supercell region for molecule", mlen);
      return DLV_ERROR;
    }
    // from atom_type::complete
    real_l cell[3][3] = { {1.0, 0.0, 0.0},
			  {0.0, 1.0, 0.0},
			  {0.0, 0.0, 1.0} };
    cell[0][0] = va[0];
    cell[1][0] = va[1];
    cell[2][0] = va[2];
    if (dim > 1) {
      cell[0][1] = vb[0];
      cell[1][1] = vb[1];
      cell[2][1] = vb[2];
      if (dim > 2) {
	cell[0][2] = vc[0];
	cell[1][2] = vc[1];
	cell[2][2] = vc[2];
      }
    }
    coord_type upper0 = coord_type(shells[0] * 1.0001 / 2);
    coord_type lower0 = -upper0;
    coord_type upper1 = coord_type(shells[1] * 1.0001 / 2);
    coord_type lower1 = -upper1;
    real_l inverse[3][3];
    matrix_invert(cell, inverse, 3);
    std::map<int, atom>::const_iterator aptr;
    for (aptr = atom_basis.begin(); aptr != atom_basis.end(); ++aptr) {
      coord_type c[3];
      aptr->second->get_cartesian_coords(c);
      // convert to fractional for parent cell to estimate which copy it is
      // from atom_type::complete
      coord_type f[3];
      for (int_g i = 0; i < dim; i++) {
	f[i] = 1e-5;
	for (int_g j = 0; j < dim; j++)
	  f[i] += inverse[i][j] * c[j];
      }
      //int fa = int(DLV::round(f[0]));
      //int fb = int(DLV::round(f[1]));
      //int fc = int(DLV::round(f[2]));
      int i = -1;
      if (dim == 1) {
	if (f[0] >= lower0 and f[0] <= upper0)
	  i = 0;
	else if (f[0] >= lower1 and f[0] <= upper1)
	  i = 1;
	else
	  i = 2;
      } else if (dim == 2) {
	if (f[0] >= lower0 and f[0] <= upper0 and
	    f[1] >= lower0 and f[1] <= upper0)
	  i = 0;
	else if (f[0] >= lower1 and f[0] <= upper1 and
		 f[1] >= lower1 and f[1] <= upper1)
	  i = 1;
	else
	  i = 2;
      } else if (dim == 3) {
	if (f[0] >= lower0 and f[0] <= upper0 and
	    f[1] >= lower0 and f[1] <= upper0 and
	    f[2] >= lower0 and f[2] <= upper0) {
	  i = 0;
	} else if (f[0] >= lower1 and f[0] <= upper1 and
		   f[1] >= lower1 and f[1] <= upper1 and
		   f[2] >= lower1 and f[2] <= upper1)
	  i = 1;
	else
	  i = 2;
      }	
      char buff[64];
      snprintf(buff, 64, "region %d", i + 1);
      aptr->second->set_group(group, buff);
      aptr->second->set_atom_flag(flag_done);
    }
    return DLV_OK;
  }
}

void DLV::model_base::convert_coords(const bool frac, real_l &x,
				     real_l &y, real_l &z) const
{
  if (frac)
    cartesian_to_fractional_coords(x, y, z);
  else
    fractional_to_cartesian_coords(x, y, z);
}

bool DLV::model_base::add_cartesian_atom(int_g &index, const int_g atomic_number,
					 const coord_type coords[3])
{
  if (atomic_number > 0 and atomic_number <= max_atomic_number) {
    atom p(new atom_type());
    p->set_atomic_number(atomic_number);
    p->set_cartesian_coords(coords);
    int_g n = (int)atom_basis.size();
    p->set_id(n);
    atom_basis[n] = p;
    index = n;
    return true;
  } else {
    return false;
  }
}

bool DLV::model_base::add_cartesian_ghost_atom(const coord_type coords[3])
{
  atom p(new atom_type());
  p->set_atomic_number(ghost_index);
  p->set_cartesian_coords(coords);
  int_g n = (int)atom_basis.size();
  p->set_id(n);
  atom_basis[n] = p;
  return true;
}

bool DLV::model_base::add_cartesian_point_charge(const coord_type coords[3])
{
  atom p(new atom_type());
  p->set_atomic_number(point_charge_index);
  p->set_cartesian_coords(coords);
  int_g n = (int)atom_basis.size();
  p->set_id(n);
  atom_basis[n] = p;
  return true;
}

bool DLV::model_base::add_fractional_atom(int_g &index,
					  const int_g atomic_number,
					  const coord_type coords[3],
					  const bool rhomb, const bool prim)
{
  if (atomic_number > 0 and atomic_number <= max_atomic_number) {
    atom p(new atom_type());
    p->set_atomic_number(atomic_number);
    //    int_g dims = get_number_of_periodic_dims();
    int_g dims = get_model_type();
    if (dims > 0) {
      // fractional coords are usually for the conventional cell.
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      if (rhomb or prim)
	get_primitive_lattice(va, vb, vc);
      else
	get_conventional_lattice(va, vb, vc);
      coord_type c[3] = {0.0, 0.0, 0.0};
      switch (dims) {
      case 1:
	c[0] = va[0] * coords[0];
	c[1] = coords[1];
	c[2] = coords[2];
	break;
      case 2:
	c[0] = va[0] * coords[0] + vb[0] * coords[1];
	c[1] = va[1] * coords[0] + vb[1] * coords[1];
	c[2] = coords[2];
	break;
      case 3:
      case 4: 
	c[0] = va[0] * coords[0] + vb[0] * coords[1] + vc[0] * coords[2];
	c[1] = va[1] * coords[0] + vb[1] * coords[1] + vc[1] * coords[2];
	c[2] = va[2] * coords[0] + vb[2] * coords[1] + vc[2] * coords[2];
	break;
      default:
	break;
      }
      p->set_cartesian_coords(c);
    } else
      p->set_cartesian_coords(coords);
    int_g n = atom_basis.size();
    p->set_id(n);
    atom_basis[n] = p;
    index = n;
    return true;
  } else {
    return false;
  }
}

bool DLV::model_base::add_cartesian_atom(int_g &index, const char symbol[],
					 const coord_type coords[3])
{
  int_g atomic_number = atom_type::check_atomic_symbol(symbol);
  if (atomic_number > 0 and atomic_number <= max_atomic_number) {
    atom p(new atom_type());
    p->set_atomic_number(atomic_number);
    p->set_cartesian_coords(coords);
    int_g n = atom_basis.size();
    p->set_id(n);
    atom_basis[n] = p;
    index = n;
    return true;
  } else {
    return false;
  }
}

bool DLV::model_base::add_fractional_atom(int_g &index, const char symbol[],
					  const coord_type coords[3],
					  const bool rhomb, const bool prim)
{
  int_g atomic_number = atom_type::check_atomic_symbol(symbol);
  if (atomic_number > 0 and atomic_number <= max_atomic_number)
    // Todo - rhomb issues?
    return add_fractional_atom(index, atomic_number, coords, rhomb, prim);
  else
    return false;
}

void DLV::model_base::replace_atom_cart_coords(const coord_type coords[][3])
{
  std::map<int, atom>::iterator ptr;
  int_g i = 0;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    coord_type c[3];
    c[0] = coords[i][0];
    c[1] = coords[i][1];
    c[2] = coords[i][2];
    ptr->second->set_cartesian_coords(c);
    i++;
  }
}

bool DLV::model_base::set_atom_charge(const string symbol, const int_g charge,
				      char message[], const int_g mlen)
{
  bool set_all = false;
  int_g atn = 0;
  if (symbol == "all")
    set_all = true;
  else
    atn = atomic_prefs::lookup_symbol(symbol.c_str());
  if (!set_all and atn == 0) {
    strncpy(message, "Couldn't identify atomic symbol", mlen);
    return false;
  } else {
    std::map<int, atom>::iterator ptr;
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
      if (set_all or ptr->second->get_atomic_number() == atn)
	ptr->second->set_charge(charge);
    }
    return true;
  }
}

bool DLV::model_base::set_core_electrons(const string symbol, const int_g core,
					 char message[], const int_g mlen)
{
  bool set_all = false;
  int_g atn = 0;
  if (symbol == "all")
    set_all = true;
  else
    atn = atomic_prefs::lookup_symbol(symbol.c_str());
  if (!set_all and atn == 0) {
    strncpy(message, "Couldn't identify atomic symbol", mlen);
    return false;
  } else {
    std::map<int, atom>::iterator ptr;
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
      if (set_all or ptr->second->get_atomic_number() == atn)
	ptr->second->set_core_electrons(core);
    }
    return true;
  }
}

void DLV::model_base::set_point_charge(const int_g index, const real_l charge,
				       char message[], const int_g mlen)
{
  std::map<int, atom>::const_iterator p = atom_basis.find(index);
  if (p == atom_basis.end())
    strncpy(message, "set_point_charge with invalid index", mlen);
  else if (p->second->get_atomic_number() == point_charge_index)
    p->second->set_point_charge(charge);
}

void DLV::model_base::set_isotope(const int_g index, const int_g isotope,
				  char message[], const int_g mlen)
{
  std::map<int, atom>::const_iterator p = atom_basis.find(index);
  if (p == atom_basis.end())
    strncpy(message, "set_isotope with invalid index", mlen);
  else
    p->second->set_isotope(isotope);
}

void DLV::model_base::set_atom_charge(const int_g index, const int_g charge)
{
  if (index >= 0 and index < (int_g)atom_basis.size())
    atom_basis[index]->set_charge(charge);
}

void DLV::model_base::set_atom_radius(const int_g index, const real_g r)
{
  if (index >= 0 and index < (int_g)atom_basis.size())
    atom_basis[index]->set_radius(r);
}

void DLV::model_base::set_atom_crystal03_id(const int_g index, const int_g id)
{
  if (index >= 0 and index < (int_g)atom_basis.size())
    atom_basis[index]->set_crystal03_id(id);
}

void DLV::model_base::set_atoms_group(const int_g index, const string gpname,
				      const string label)
{
  if (index >= 0 and index < (int_g)atom_basis.size())
    atom_basis[index]->set_group(gpname, label);
}

void DLV::model_base::add_bond(const int_g atom1, const int_g atom2)
{
  int_g n = (int)atom_basis.size();
  if (atom1 > -1 and atom1 < n and atom2 > -1 and atom2 < n) {
    atom a1 = atom_basis.find(atom1)->second;
    atom a2 = atom_basis.find(atom2)->second;
    bonds.set_pair(a1, a2, true, atom_basis);
  }
}

void DLV::model_base::group_atoms(const string gp, const string label,
				  const bool atoms[])
{
  bool exists = false;
  std::list<string>::const_iterator lptr;
  for (lptr = group_names.begin(); lptr != group_names.end(); ++lptr ) {
    if (*lptr == gp) {
      exists = true;
      break;
    }
  }
  if (!exists)
    group_names.push_back(gp);
  std::map<int, atom>::iterator ptr;
  int_g i = 0;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    if (atoms[i])
      ptr->second->add_group(gp, label);
    i++;
  }
}

void DLV::model_base::update_shells(const int_g types[], const real_g [],
				    const real_g radii[], const int_g nshells)
{
  if (get_number_of_periodic_dims() > 0)
    return; // BUG
  int_g n = (int)atom_basis.size();
  int_g *atn = new_local_array1(int_g, n);
  real_l *lengths = new_local_array1(real_l, n);
  coord_type (*coords)[3] = new_local_array2(coord_type, n, 3);
  std::map<int, atom>::const_iterator ptr;
  int_g i = 0;
  real_l min_len = 1e10;
  int_g centre = 0;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    ptr->second->get_cartesian_coords(coords[i]);
    atn[i] = ptr->second->get_atomic_number();
    lengths[i] = (coords[i][0] * coords[i][0] + coords[i][1] * coords[i][1]
		  + coords[i][2] * coords[i][2]);
    if (min_len > lengths[i]) {
      min_len = lengths[i];
      centre = i;
    }
    i++;
  }
  for (int_g k = 0; k < n; k++) {
    if (k != centre) {
      coords[k][0] -= coords[centre][0];
      coords[k][1] -= coords[centre][1];
      coords[k][2] -= coords[centre][2];
      lengths[k] = (coords[k][0] * coords[k][0] + coords[k][1] * coords[k][1]
		    + coords[k][2] * coords[k][2]);
    }
  }
  // order them to match types order
  int_g *indices = new_local_array1(int_g, n);
  for (int_g k = 0; k < n; k++)
    indices[k] = -1;
  // need a better sort
  for (int_g k = 0; k < nshells; k++) {
    min_len = 1e10;
    int_g pos = 0;
    for (int_g j = 0; j < n; j++) {
      if (j != centre and indices[j] == -1 and atn[j] == types[k]) {
	if (min_len > lengths[j]) {
	  min_len = lengths[j];
	  pos = j;
	}
      }
    }
    indices[pos] = k;
  }
  // then update coords
  i = 0;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr ) {
    if (i != centre) {
      real_l scale = radii[indices[i]] / sqrt(lengths[i]);
      coords[i][0] = scale * coords[i][0] + coords[centre][0];
      coords[i][1] = scale * coords[i][1] + coords[centre][1];
      coords[i][2] = scale * coords[i][2] + coords[centre][2];
      ptr->second->set_cartesian_coords(coords[i]);
    }
  }
  delete_local_array(indices);
  delete_local_array(coords);
  delete_local_array(lengths);
  delete_local_array(atn);
}

DLV::string DLV::model_base::get_formula()
{
  update_atom_list();
  int_g count[150];
  for (int_g i = 0; i < 150; i++)
    count[i] = 0;
  primitive_atoms.count_atom_types(count, 150);
  int_g divisor = 1;
  if (get_number_of_periodic_dims() > 0) {
    int_g max_count = 0;
    for (int_g i = 0; i < 150; i++)
      if (count[i] > max_count)
	max_count = count[i];
    int_g gcd = 1;
    do {
      // find smallest atom count
      int_g min_count = max_count;
      for (int_g i = 0; i < 150; i++)
	if (count[i] > 0 and count[i] < min_count)
	  min_count = count[i];
      gcd = min_count;
      if (min_count > 1) {
	// find common divisor for counts
	bool found;
	do {
	  found = true;
	  for (int_g i = 0; i < 150; i++) {
	    if (count[i] > 0) {
	      if (count[i] % gcd != 0) {
		gcd--;
		found = false;
		break;
	      }
	    }
	  }
	  if (gcd == 1)
	    break;
	} while (!found);
	if (gcd > 1) {
	  // apply divisor
	  for (int_g i = 0; i < 150; i++)
	    count[i] /= gcd;
	  divisor *= gcd;
	}
      }
    } while (gcd > 1);
  }
  string formula;
  for (int_g i = 0; i < 150; i++) {
    // if formula.length() > 0 formula += " "; ?
    if (count[i] > 0) {
      formula += atomic_prefs::get_symbol(i);
      if (count[i] > 1) {
	char buff[8];
	snprintf(buff, 8, "%1d", count[i]);
	formula += buff;
      }
    }
  }
  if (divisor > 1) {
    char unit[32];
    snprintf(unit, 32, " (%1d units/cell)", divisor);
    formula += unit;
  }
  return formula;
}

void DLV::model_base::count_primitive_atom_types(int_g count[],
						 const int_g len) const
{
    for (int_g i = 0; i < len; i++)
	count[i] = 0;
    primitive_atoms.count_atom_types(count, len);
    return;
}

void DLV::model_base::count_crystal03_ids(int_g count[], const int_g len) const
{
    for (int_g i = 0; i < len; i++)
      count[i] = 0;
    primitive_atoms.count_crystal03_ids(count);
    return;
}

void DLV::model_base::set_indexed_asym_atom_type(const int_g id,
						 const int_g i) const
{
  std::map<int, atom>::const_iterator p;
  p = atom_basis.find(i);
  p->second->set_atomic_number(id);
}

void DLV::model_base::get_primitive_lattice(coord_type a[3], coord_type b[3],
					    coord_type c[3]) const
{
  a[0] = 0.0;
  a[1] = 0.0;
  a[2] = 0.0;
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::model_base::get_conventional_lattice(coord_type a[3],
					       coord_type b[3],
					       coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

DLV::int_g DLV::model_base::get_hermann_mauguin_group(string &name) const
{
  // Todo
  name = "";
  return 1;
}

DLV::int_g DLV::model_base::get_number_of_asym_atoms() const
{
  return (int)atom_basis.size();
}

DLV::int_g DLV::model_base::get_number_of_primitive_atoms() const
{
  return primitive_atoms.size();
}

DLV::int_g DLV::model_base::get_atomic_number(const int_g index) const
{
  if (index >= 0 and index < (int_g)atom_basis.size()) {
    std::map<int, atom>::const_iterator atom = atom_basis.find(index);
    return atom->second->get_atomic_number();
  } else
    return 0;
}

void DLV::model_base::get_atom_properties(const int_g index, int_g &c,
					  real_g &r, real_g &red, real_g &green,
					  real_g &blue, int_g &s,
					  bool &use_charge, bool &use_radius,
					  bool &use_colour,
					  bool &use_spin) const
{
  if (index >= 0 and index < (int_g)atom_basis.size()) {
    std::map<int, atom>::const_iterator atom = atom_basis.find(index);
    atom->second->get_properties(c, r, red, green, blue, s, use_charge,
				 use_radius, use_colour, use_spin);
  }
}

void DLV::model_base::get_asym_atom_types(int_g atom_types[],
					  const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_types[i] = p->second->get_atomic_number();
}

void DLV::model_base::get_asym_atom_regions(const char group[], int_g regions[],
					    const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i) {
    char buff[64];
    if (sscanf(p->second->get_group(group).c_str(),
	       "%s %d", buff, &regions[i]) != 2)
      regions[i] = 0;
  }
}

void DLV::model_base::get_asym_occupancy(real_g atom_occ[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_occ[i] = p->second->get_occupancy();
}

void DLV::model_base::set_asym_occupancy(real_g atom_occ[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_occupancy(atom_occ[i]);
}


void DLV::model_base::get_asym_crystal03_ids(int_g atom_ids[],
					     const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_crystal03_id();
}

void DLV::model_base::get_asym_core_electrons(int_g atom_ids[],
					      const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_core_electrons();
}

void DLV::model_base::get_asym_point_charge(real_l charge[],
					    const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    charge[i] = p->second->get_point_charge();
}

void DLV::model_base::get_asym_isotope(int_g isotope[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    isotope[i] = p->second->get_isotope();
}

void DLV::model_base::get_asym_debye_waller1(real_g atom_ids[],
					     const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_debye_waller1();
}

void DLV::model_base::get_asym_debye_waller2(real_g atom_ids[],
					     const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_debye_waller2();
}

void DLV::model_base::set_asym_debye_waller(real_g atom_dw1[],
					    real_g atom_dw2[],
					    const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_debye_waller(atom_dw1[i], atom_dw2[i]);
}

void DLV::model_base::get_asym_displacements(real_g atom_disps[][3],
					     const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->get_displacements(atom_disps[i]);
}

void DLV::model_base::set_asym_displacements(real_g atom_disps[][3],
					     const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_displacements(atom_disps[i]);
}

void DLV::model_base::get_asym_rod_dw1(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_dw1();
}

void DLV::model_base::get_asym_rod_dw2(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_dw2();
}

void DLV::model_base::get_asym_rod_noccup(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_noccup();
}

void DLV::model_base::set_asym_rod_dw1(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_rod_dw1_id(atom_ids[i]);
}

void DLV::model_base::set_asym_rod_dw2(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_rod_dw2_id(atom_ids[i]);
}

void DLV::model_base::set_asym_rod_noccup(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    p->second->set_rod_occ_id(atom_ids[i]);
}

void DLV::model_base::get_asym_rod_xconst(real_g atom_ids[],
					  const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_xconst();
}

void DLV::model_base::get_asym_rod_nxdis(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_nxdis();
}

void DLV::model_base::get_asym_rod_x2const(real_g atom_ids[],
					   const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_x2const();
}

void DLV::model_base::get_asym_rod_nx2dis(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_nx2dis();
}

void DLV::model_base::get_asym_rod_yconst(real_g atom_ids[],
					  const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_yconst();
}

void DLV::model_base::get_asym_rod_nydis(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_nydis();
}

void DLV::model_base::get_asym_rod_y2const(real_g atom_ids[],
					   const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_y2const();
}

void DLV::model_base::get_asym_rod_ny2dis(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_ny2dis();
}

void DLV::model_base::get_asym_rod_nzdis(int_g atom_ids[], const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    atom_ids[i] = p->second->get_rod_nzdis();
}

void DLV::model_base::get_asym_atom_spins(int_g atom_spins[],
					  const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i) {
    uhf_spin_type j = p->second->get_uhf_spin();
    if (j == alpha_spin)
      atom_spins[i] = 1;
    else if (j == beta_spin)
      atom_spins[i] = -1;
    else
      atom_spins[i] = 0;
  }
}

void DLV::model_base::get_primitive_atom_types(int_g atom_types[],
					       const int_g n) const
{
  primitive_atoms.get_atom_types(atom_types, n);
}

void DLV::model_base::get_primitive_atom_regions(const char group[],
						 int_g regions[],
						 const int_g n) const
{
  primitive_atoms.get_atom_regions(group, regions, n);
}

void DLV::model_base::get_primitive_asym_indices(int_g indices[],
						 const int_g n) const
{
  primitive_atoms.get_asym_indices(indices, n, atom_basis);
}

void DLV::model_base::get_asym_to_primitive_indices(int_g indices[],
						    const int_g n) const
{
  int_g *idx = new_local_array1(int_g, n);
  primitive_atoms.get_asym_indices(idx, n, atom_basis);
  for (int_g i = 0; i < n; i++)
    indices[idx[i]] = i;
  delete_local_array(idx);
}

void DLV::model_base::get_asym_atom_charges(int_g charges[],
					    const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    charges[i] = p->second->get_charge();
}

void 
DLV::model_base::has_asym_atom_special_periodicity(bool special_periodicity[],
						   const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i)
    special_periodicity[i] = p->second->has_special_periodicity();
}

void DLV::model_base::get_asym_atom_cart_coords(coord_type coords[][3],
						const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i) {
    coord_type c[3];
    p->second->get_cartesian_coords(c);
    coords[i][0] = c[0];
    coords[i][1] = c[1];
    coords[i][2] = c[2];
  }
}

void DLV::model_base::get_asym_atom_frac_coords(coord_type coords[][3],
						const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  int_g i = 0;
  for (p = atom_basis.begin(); (p != atom_basis.end() and i < n); ++p, ++i) {
    coord_type c[3];
    p->second->get_fractional_coords(c);
    coords[i][0] = c[0];
    coords[i][1] = c[1];
    coords[i][2] = c[2];
  }
}

void DLV::model_base::get_primitive_atom_cart_coords(coord_type coords[][3],
						     const int_g n) const
{
  primitive_atoms.get_cartesian_coords(coords, n);
}

void DLV::model_base::get_primitive_atom_frac_coords(coord_type coords[][3],
						     const int_g n) const
{
  primitive_atoms.get_fractional_coords(coords, n);
}

void DLV::model_base::set_indexed_asym_cart_coords(const coord_type coords[3], 
				  const int_g n) const
{
  std::map<int, atom>::const_iterator p;
  p = atom_basis.find(n);
  p->second->set_cartesian_coords(coords);
} 

void DLV::model_base::get_atom_shifts(const coord_type coords[][3],
				      int_g shifts[][3], int_g prim_id[],
				      int_g asym_id[], const int_g n) const
{
  get_atom_shifts(primitive_atoms, coords, shifts, prim_id, n);
  int_g nprim = (int)primitive_atoms.size();
  int_g *indices = new_local_array1(int_g, nprim);
  primitive_atoms.get_asym_indices(indices, nprim, atom_basis);
  for (int_g i = 0; i < n; i++)
    asym_id[i] = indices[prim_id[i]];
  delete_local_array(indices);
}

void DLV::model_base::map_asym_pos(const int_g asym_idx, const coord_type a[3],
				   int_g &op, coord_type a_coords[3],
				   const coord_type b[][3],
				   const int_g b_asym_idx[],
				   coord_type b_coords[][3], int_g ab_shift[3],
				   const int_g nbatoms,
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops) const
{
  std::map<int, atom>::const_iterator p;
  p = atom_basis.find(asym_idx);
  atom_position a_atom(p->second);
  p->second->get_cartesian_coords(a_coords);
  a_atom.set_coords(a_coords);
  atom_position tatom(p->second);
  atom b_atoms[3];
  for (int_g j = 0; j < nbatoms; j++) {
    p = atom_basis.find(b_asym_idx[j]);
    b_atoms[j] = p->second;
  }
  ab_shift[0] = 0;
  ab_shift[1] = 0;
  ab_shift[2] = 0;
  coord_type coords[3][3];
  int_g indices[3];
  int_g nx = get_number_of_primitive_atoms();
  for (int_g i = 0; i < nbatoms; i++)
    indices[i] = nx;
  for (int_g i = 0; i < nops; i++) {
    coord_type pos[3];
    transform_coordinates(pos, a, rotations[i], translations[i]);
    tatom.set_coords(pos);
    if (a_atom.compare(tatom) == 0) {
      // Todo - lrint?
      bool best = true;
      for (int_g j = 0; j < nbatoms; j++) {
	atom_position patom(b_atoms[j]);
	coord_type px[3];
	transform_coordinates(px, b[j], rotations[i], translations[i]);
	coords[j][0] = px[0];
	coords[j][1] = px[1];
	coords[j][2] = px[2];
	patom.set_coords(px);
	int_g index = primitive_atoms.locate_atom(patom);
	if (index > indices[j]) {
	  best = false;
	  break;
	} else {
	  bool update = false;
	  if (index < indices[j])
	    update = true;
	  else {
	    const real_l tol = 0.001;
	    if (px[0] > b_coords[j][0] + tol)
	      update = true;
	    else if (px[0] > b_coords[j][0] - tol) {
	      if (px[1] > b_coords[j][1] + tol)
		update = true;
	      else if (px[1] > b_coords[j][1] - tol) {
		if (px[2] > b_coords[j][2] + tol)
		  update = true;
	      }
	    }
	  }
	  if (update) {
	    for (int_g k = j + 1; k < nbatoms; k++)
	      indices[k] = nx;
	    indices[j] = index;
	  } else
	    best = false;
	}
      }
      if (best) {
	op = i;
	for (int_g j = 0; j < nbatoms; j++) {
	  b_coords[j][0] = coords[j][0];
	  b_coords[j][1] = coords[j][1];
	  b_coords[j][2] = coords[j][2];
	}
      }
    }
  }
}

void DLV::model_base::map_asym_pos(const int_g asym_idx, const coord_type a[3],
				   int_g &op, coord_type a_coords[3],
				   const coord_type b[][3],
				   const int_g b_asym_idx[],
				   coord_type b_coords[][3], int_g ab_shift[3],
				   const int_g nbatoms, const coord_type la[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops) const
{
  real_l cell[3][3];
  cell[0][0] = a[0];
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 1);
  std::map<int, atom>::const_iterator p = atom_basis.find(asym_idx);
  atom_position a_atom(p->second);
  p->second->get_cartesian_coords(a_coords);
  a_atom.set_coords(a_coords, cell, inverse, 1);
  coord_type a_shift[3];
  a_atom.get_shift(a_shift);
  atom_position tatom(p->second);
  atom b_atoms[3];
  for (int_g j = 0; j < nbatoms; j++) {
    p = atom_basis.find(b_asym_idx[j]);
    b_atoms[j] = p->second;
  }
  coord_type coords[3][3];
  int_g b_shifts[3][3];
  int_g indices[3];
  int_g nx = get_number_of_primitive_atoms();
  for (int_g i = 0; i < nbatoms; i++)
    indices[i] = nx;
  for (int_g i = 0; i < nops; i++) {
    coord_type pos[3];
    transform_coordinates(pos, a, rotations[i], translations[i]);
    tatom.set_coords(pos, cell, inverse, 1);
    if (a_atom.compare(tatom) == 0) {
      coord_type t_shift[3];
      tatom.get_shift(t_shift);
      // Todo - lrint?
      int_g shift[3];
      shift[0] = to_integer(DLV::round(t_shift[0] - a_shift[0]));
      shift[1] = 0;
      shift[2] = 0;
      bool best = true;
      for (int_g j = 0; j < nbatoms; j++) {
	atom_position patom(b_atoms[j]);
	coord_type px[3];
	transform_coordinates(px, b[j], rotations[i], translations[i]);
	// apply -ve shift to move it to where it would be
	// if a is at asym position.
	px[0] += real_l(shift[1]) * cell[0][1];
	coords[j][0] = px[0];
	coords[j][1] = px[1];
	coords[j][2] = px[2];
	patom.set_coords(px, cell, inverse, 1);
	int_g index = primitive_atoms.locate_atom(patom);
	if (index > indices[j]) {
	  best = false;
	  break;
	} else {
	  coord_type dbshift[3];
	  patom.get_shift(dbshift);
	  int_g bshift[3];
	  bshift[0] = to_integer(DLV::round(dbshift[0]));
	  bshift[1] = 0;
	  bshift[2] = 0;
	  bool update = false;
	  if (index < indices[j])
	    update = true;
	  else {
	    int_g curr = std::abs(b_shifts[j][0]);
	    int_g newb = std::abs(bshift[0]);
	    if (newb < curr)
	      update = true;
	    else if (curr == newb) {
	      const real_l tol = 0.001;
	      if (px[0] > b_coords[j][0] + tol)
		update = true;
	      else if (px[0] > b_coords[j][0] - tol) {
		if (px[1] > b_coords[j][1] + tol)
		  update = true;
		else if (px[1] > b_coords[j][1] - tol) {
		  if (px[2] > b_coords[j][2] + tol)
		    update = true;
		}
	      }
	    }
	  }
	  if (update) {
	    for (int_g k = j + 1; k < nbatoms; k++)
	      indices[k] = nx;
	    indices[j] = index;
	    b_shifts[j][0] = bshift[0];
	    b_shifts[j][1] = bshift[1];
	    b_shifts[j][2] = bshift[2];
	  } else
	    best = false;
	}
      }
      if (best) {
	op = i;
	for (int_g j = 0; j < nbatoms; j++) {
	  b_coords[j][0] = coords[j][0];
	  b_coords[j][1] = coords[j][1];
	  b_coords[j][2] = coords[j][2];
	}
	ab_shift[0] = shift[0];
	ab_shift[1] = shift[1];
	ab_shift[2] = shift[2];
      }
    }
  }
}

void DLV::model_base::map_asym_pos(const int_g asym_idx, const coord_type a[3],
				   int_g &op, coord_type a_coords[3],
				   const coord_type b[][3],
				   const int_g b_asym_idx[],
				   coord_type b_coords[][3], int_g ab_shift[3],
				   const int_g nbatoms, const coord_type la[3],
				   const coord_type lb[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = la[i];
    cell[i][1] = lb[i];
    cell[i][2] = 0.0;
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 2);
  std::map<int, atom>::const_iterator p = atom_basis.find(asym_idx);
  atom_position a_atom(p->second);
  p->second->get_cartesian_coords(a_coords);
  a_atom.set_coords(a_coords, cell, inverse, 2);
  coord_type a_shift[3];
  a_atom.get_shift(a_shift);
  atom_position tatom(p->second);
  atom b_atoms[3];
  for (int_g j = 0; j < nbatoms; j++) {
    p = atom_basis.find(b_asym_idx[j]);
    b_atoms[j] = p->second;
  }
  coord_type coords[3][3];
  int_g b_shifts[3][3];
  int_g indices[3];
  int_g nx = get_number_of_primitive_atoms();
  for (int_g i = 0; i < nbatoms; i++)
    indices[i] = nx;
  for (int_g i = 0; i < nops; i++) {
    coord_type pos[3];
    transform_coordinates(pos, a, rotations[i], translations[i]);
    tatom.set_coords(pos, cell, inverse, 2);
    if (a_atom.compare(tatom) == 0) {
      coord_type t_shift[3];
      tatom.get_shift(t_shift);
      // Todo - lrint?
      int_g shift[3];
      shift[0] = to_integer(DLV::round(t_shift[0] - a_shift[0]));
      shift[1] = to_integer(DLV::round(t_shift[1] - a_shift[1]));
      shift[2] = 0;
      bool best = true;
      for (int_g j = 0; j < nbatoms; j++) {
	atom_position patom(b_atoms[j]);
	//patom.set_coords(b[j], cell, inverse, 2);
	//patom.get_shift(o_shift);
	coord_type px[3];
	transform_coordinates(px, b[j], rotations[i], translations[i]);
	// apply -ve shift to move it to where it would be
	// if a is at asym position.
	for (int_g k = 0; k < 2; k++) {
	  px[0] += real_l(shift[k]) * cell[0][k];
	  px[1] += real_l(shift[k]) * cell[1][k];
	}
	coords[j][0] = px[0];
	coords[j][1] = px[1];
	coords[j][2] = px[2];
	patom.set_coords(px, cell, inverse, 2);
	int_g index = primitive_atoms.locate_atom(patom);
	if (index > indices[j]) {
	  best = false;
	  break;
	} else {
	  coord_type dbshift[3];
	  patom.get_shift(dbshift);
	  int_g bshift[3];
	  bshift[0] = to_integer(DLV::round(dbshift[0]));
	  bshift[1] = to_integer(DLV::round(dbshift[1]));
	  bshift[2] = 0;
	  bool update = false;
	  if (index < indices[j])
	    update = true;
	  else {
	    int_g curr = std::abs(b_shifts[j][0]) + std::abs(b_shifts[j][1]);
	    int_g newb = std::abs(bshift[0]) + std::abs(bshift[1]);
	    if (newb < curr)
	      update = true;
	    else if (curr == newb) {
	      const real_l tol = 0.001;
	      if (px[0] > b_coords[j][0] + tol)
		update = true;
	      else if (px[0] > b_coords[j][0] - tol) {
		if (px[1] > b_coords[j][1] + tol)
		  update = true;
		else if (px[1] > b_coords[j][1] - tol) {
		  if (px[2] > b_coords[j][2] + tol)
		    update = true;
		}
	      }
	    }
	  }
	  if (update) {
	    for (int_g k = j + 1; k < nbatoms; k++)
	      indices[k] = nx;
	    indices[j] = index;
	    b_shifts[j][0] = bshift[0];
	    b_shifts[j][1] = bshift[1];
	    b_shifts[j][2] = bshift[2];
	  } else
	    best = false;
	}
      }
      if (best) {
	op = i;
	for (int_g j = 0; j < nbatoms; j++) {
	  b_coords[j][0] = coords[j][0];
	  b_coords[j][1] = coords[j][1];
	  b_coords[j][2] = coords[j][2];
	}
	ab_shift[0] = shift[0];
	ab_shift[1] = shift[1];
	ab_shift[2] = shift[2];
      }
    }
  }
}

void DLV::model_base::map_asym_pos(const int_g asym_idx, const coord_type a[3],
				   int_g &op, coord_type a_coords[3],
				   const coord_type b[][3],
				   const int_g b_asym_idx[],
				   coord_type b_coords[][3], int_g ab_shift[3],
				   const int_g nbatoms, const coord_type la[3],
				   const coord_type lb[3],
				   const coord_type lc[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = la[i];
    cell[i][1] = lb[i];
    cell[i][2] = lc[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  std::map<int, atom>::const_iterator p = atom_basis.find(asym_idx);
  atom_position a_atom(p->second);
  p->second->get_cartesian_coords(a_coords);
  a_atom.set_coords(a_coords, cell, inverse, 3);
  coord_type a_shift[3];
  a_atom.get_shift(a_shift);
  atom_position tatom(p->second);
  atom b_atoms[3];
  for (int_g j = 0; j < nbatoms; j++) {
    p = atom_basis.find(b_asym_idx[j]);
    b_atoms[j] = p->second;
  }
  coord_type coords[3][3];
  int_g b_shifts[3][3];
  int_g indices[3];
  int_g nx = get_number_of_primitive_atoms();
  for (int_g i = 0; i < nbatoms; i++)
    indices[i] = nx;
  for (int_g i = 0; i < nops; i++) {
    coord_type pos[3];
    transform_coordinates(pos, a, rotations[i], translations[i]);
    tatom.set_coords(pos, cell, inverse, 3);
    if (a_atom.compare(tatom) == 0) {
      coord_type t_shift[3];
      tatom.get_shift(t_shift);
      // Todo - lrint?
      int_g shift[3];
      shift[0] = to_integer(DLV::round(t_shift[0] - a_shift[0]));
      shift[1] = to_integer(DLV::round(t_shift[1] - a_shift[1]));
      shift[2] = to_integer(DLV::round(t_shift[2] - a_shift[2]));
      bool best = true;
      for (int_g j = 0; j < nbatoms; j++) {
	atom_position patom(b_atoms[j]);
	coord_type px[3];
	transform_coordinates(px, b[j], rotations[i], translations[i]);
	// apply -ve shift to move it to where it would be
	// if a is at asym position.
	for (int_g k = 0; k < 3; k++) {
	  px[0] += real_l(shift[k]) * cell[0][k];
	  px[1] += real_l(shift[k]) * cell[1][k];
	  px[2] += real_l(shift[k]) * cell[2][k];
	}
	coords[j][0] = px[0];
	coords[j][1] = px[1];
	coords[j][2] = px[2];
	patom.set_coords(px, cell, inverse, 3);
	int_g index = primitive_atoms.locate_atom(patom);
	if (index > indices[j]) {
	  best = false;
	  break;
	} else {
	  coord_type dbshift[3];
	  patom.get_shift(dbshift);
	  int_g bshift[3];
	  bshift[0] = to_integer(DLV::round(dbshift[0]));
	  bshift[1] = to_integer(DLV::round(dbshift[1]));
	  bshift[2] = to_integer(DLV::round(dbshift[2]));
	  bool update = false;
	  if (index < indices[j])
	    update = true;
	  else {
	    int_g curr = std::abs(b_shifts[j][0]) + std::abs(b_shifts[j][1])
	      + std::abs(b_shifts[j][2]);
	    int_g newb = std::abs(bshift[0]) + std::abs(bshift[1])
	      + std::abs(bshift[2]);
	    if (newb < curr)
	      update = true;
	    else if (curr == newb) {
	      const real_l tol = 0.001;
	      if (px[0] > b_coords[j][0] + tol)
		update = true;
	      else if (px[0] > b_coords[j][0] - tol) {
		if (px[1] > b_coords[j][1] + tol)
		  update = true;
		else if (px[1] > b_coords[j][1] - tol) {
		  if (px[2] > b_coords[j][2] + tol)
		    update = true;
		}
	      }
	    }
	  }
	  if (update) {
	    for (int_g k = j + 1; k < nbatoms; k++)
	      indices[k] = nx;
	    indices[j] = index;
	    b_shifts[j][0] = bshift[0];
	    b_shifts[j][1] = bshift[1];
	    b_shifts[j][2] = bshift[2];
	  } else
	    best = false;
	}
      }
      if (best) {
	op = i;
	for (int_g j = 0; j < nbatoms; j++) {
	  b_coords[j][0] = coords[j][0];
	  b_coords[j][1] = coords[j][1];
	  b_coords[j][2] = coords[j][2];
	}
	ab_shift[0] = shift[0];
	ab_shift[1] = shift[1];
	ab_shift[2] = shift[2];
      }
    }
  }
}

void DLV::model_base::map_asym_pos(const int_g asym_idx, const coord_type a[3],
				   int_g &op, coord_type a_coords[3],
				   const coord_type b[][3],
				   const int_g b_asym_idx[],
				   coord_type b_coords[][3], int_g ab_shift[3],
				   const int_g nbatoms, const coord_type la[3],
				   const coord_type lb[3],
				   const coord_type lc[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops, const bool surface) const
{
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = la[i];
    cell[i][1] = lb[i];
    cell[i][2] = lc[i];
  }
  std::map<int, atom>::const_iterator p = atom_basis.find(asym_idx);
  bool periodic = p->second->has_special_periodicity();
  int_g dim = 2;
  if (periodic)
    dim = 3;
  real_l inverse[3][3];
  matrix_invert(cell, inverse, dim);
  atom_position a_atom(p->second);
  p->second->get_cartesian_coords(a_coords);
  a_atom.set_coords(a_coords, cell, inverse, dim);
  coord_type a_shift[3];
  a_atom.get_shift(a_shift);
  atom_position tatom(p->second);
  atom b_atoms[3];
  for (int_g j = 0; j < nbatoms; j++) {
    p = atom_basis.find(b_asym_idx[j]);
    b_atoms[j] = p->second;
  }
  coord_type coords[3][3];
  int_g b_shifts[3][3];
  int_g indices[3];
  int_g nx = get_number_of_primitive_atoms();
  for (int_g i = 0; i < nbatoms; i++)
    indices[i] = nx;
  for (int_g i = 0; i < nops; i++) {
    coord_type pos[3];
    transform_coordinates(pos, a, rotations[i], translations[i]);
    tatom.set_coords(pos, cell, inverse, dim);
    tatom.get_cartesian_coords(pos);
    if (a_atom.compare(tatom) == 0) {
      coord_type t_shift[3];
      tatom.get_shift(t_shift);
      // Todo - lrint?
      int_g shift[3];
      shift[0] = to_integer(DLV::round(t_shift[0] - a_shift[0]));
      shift[1] = to_integer(DLV::round(t_shift[1] - a_shift[1]));
      if (periodic)
	shift[2] = to_integer(DLV::round(t_shift[2] - a_shift[2]));
      else
	shift[2] = 0;
      bool best = true;
      for (int_g j = 0; j < nbatoms; j++) {
	int_g bdim = 2;
	if (b_atoms[j]->has_special_periodicity())
	  bdim = 3;
	atom_position patom(b_atoms[j]);
	coord_type px[3];
	transform_coordinates(px, b[j], rotations[i], translations[i]);
	// apply -ve shift to move it to where it would be
	// if a is at asym position.
	for (int_g k = 0; k < bdim; k++) {
	  px[0] += real_l(shift[k]) * cell[0][k];
	  px[1] += real_l(shift[k]) * cell[1][k];
	  px[2] += real_l(shift[k]) * cell[2][k];
	}
	coords[j][0] = px[0];
	coords[j][1] = px[1];
	coords[j][2] = px[2];
	patom.set_coords(px, cell, inverse, bdim);
	int_g index = primitive_atoms.locate_atom(patom);
	if (index > indices[j]) {
	  best = false;
	  break;
	} else {
	  coord_type dbshift[3];
	  patom.get_shift(dbshift);
	  int_g bshift[3];
	  bshift[0] = to_integer(DLV::round(dbshift[0]));
	  bshift[1] = to_integer(DLV::round(dbshift[1]));
	  if (periodic)
	    bshift[2] = to_integer(DLV::round(dbshift[2]));
	  else
	    bshift[2] = 0;
	  bool update = false;
	  if (index < indices[j])
	    update = true;
	  else {
	    int_g curr = std::abs(b_shifts[j][0]) + std::abs(b_shifts[j][1])
	      + std::abs(b_shifts[j][2]);
	    int_g newb = std::abs(bshift[0]) + std::abs(bshift[1])
	      + std::abs(bshift[2]);
	    if (newb < curr)
	      update = true;
	    else if (curr == newb) {
	      const real_l tol = 0.001;
	      if (px[0] > b_coords[j][0] + tol)
		update = true;
	      else if (px[0] > b_coords[j][0] - tol) {
		if (px[1] > b_coords[j][1] + tol)
		  update = true;
		else if (px[1] > b_coords[j][1] - tol) {
		  if (px[2] > b_coords[j][2] + tol)
		    update = true;
		}
	      }
	    }
	  }
	  if (update) {
	    for (int_g k = j + 1; k < nbatoms; k++)
	      indices[k] = nx;
	    indices[j] = index;
	    b_shifts[j][0] = bshift[0];
	    b_shifts[j][1] = bshift[1];
	    b_shifts[j][2] = bshift[2];
	  } else
	    best = false;
	}
      }
      if (best) {
	op = i;
	for (int_g j = 0; j < nbatoms; j++) {
	  b_coords[j][0] = coords[j][0];
	  b_coords[j][1] = coords[j][1];
	  b_coords[j][2] = coords[j][2];
	}
	ab_shift[0] = shift[0];
	ab_shift[1] = shift[1];
	ab_shift[2] = shift[2];
      }
    }
  }
}

void DLV::model_base::map_atom_data(const real_g grid[][3], int_g **const data,
				    const int_g ndata, const int_g ngrid,
				    real_g (* &map_grid)[3], int_g ** &new_data,
				    int_g &n) const
{
  n = display_atoms.size();
  map_grid = new real_g [n][3];
  if (map_grid == 0) {
    n = 0;
    return;
  }
  new_data = new int_g *[ndata];
  if (new_data == 0) {
    delete [] map_grid;
    n = 0;
    return;
  }
  for (int_g i = 0; i < ndata; i++)
    new_data[i] = new int_g[n];
  map_data(display_atoms, grid, data, ndata, ngrid, map_grid, new_data, n);
}

void DLV::model_base::map_atom_data(const real_g grid[][3], real_g **const data,
				    const int_g ndata, const int_g ngrid,
				    real_g (* &map_grid)[3],
				    real_g ** &new_data, int_g &n) const
{
  n = display_atoms.size();
  map_grid = new real_g [n][3];
  if (map_grid == 0) {
    n = 0;
    return;
  }
  new_data = new real_g *[ndata];
  if (new_data == 0) {
    delete [] map_grid;
    n = 0;
    return;
  }
  for (int_g i = 0; i < ndata; i++)
    new_data[i] = new real_g[n];
  map_data(display_atoms, grid, data, ndata, ngrid, map_grid, new_data, n);
}

void DLV::model_base::map_atom_data(const real_g grid[][3],
				    const real_g mags[][3],
				    const real_g phases[][3], const int_g ngrid,
				    real_g (* &map_grid)[3],
				    real_g (* &new_data)[3], int_g &n,
				    const real_g ka, const real_g kb,
				    const real_g kc) const
{
  map_data(display_atoms, grid, mags, phases, ngrid,
	   map_grid, new_data, n, ka, kb, kc);
}

void DLV::model_base::map_atom_data(const real_g grid[][3],
				    const real_g mags[][3],
				    const real_g phases[][3], const int_g ngrid,
				    std::vector<real_g (*)[3]> &new_data,
				    int_g &n, const int_g nframes,
				    const int_g ncopies, const real_g ka,
				    const real_g kb, const real_g kc,
				    const real_g scale) const
{
  n = display_atoms.size();
  if (new_data.size() == 0)
    new_data = std::vector<real_g (*)[3]> (nframes);
  else if (new_data.size() < (nat_g) nframes)
    new_data.resize(nframes);
  for (int_g i = 0; i < nframes; i++) {
    new_data[i] = new real_g[n][3];
    for (int_g j = 0; j < n; j++) {
      new_data[i][j][0] = 0.0;
      new_data[i][j][1] = 0.0;
      new_data[i][j][2] = 0.0;
    }
  }
  map_data(display_atoms, grid, mags, phases, ngrid, new_data, n,
  	   nframes, ncopies, ka, kb, kc, scale);
}

void DLV::model_base::map_atom_data(const real_g grid[][3], const int_g ngrid,
				    const std::vector<real_g (*)[3]> &traj,
				    const int_g nframes,
				    std::vector<real_g (*)[3]> &new_data) const
{
  int_g n = display_atoms.size();
  if (new_data.size() == 0)
    new_data = std::vector<real_g (*)[3]> (nframes);
  else if (new_data.size() < (nat_g) nframes)
    new_data.resize(nframes);
  for (int_g i = 0; i < nframes; i++) {
    if (new_data[i] != 0)
      delete [] new_data[i];
    new_data[i] = new real_g[n][3];
  }
  map_data(display_atoms, grid, ngrid, traj, nframes, new_data);
}

void DLV::model_base::shift_cartesian_operators(const model_base *,
						const real_l, const real_l,
						const real_l,
						const coord_type[3],
						const coord_type [3],
						const coord_type [3],
						const int_g)
{
  // BUG
  fprintf(stderr, "Illegal call to shift cartesian operators\n");
}

void DLV::model_base::check_point_symmetry(const model_base *p,
					   const atom parent[])
{
  // create searchable list, primitive is display as no lattice.
  atom_tree cluster;
  std::map<int, atom>::const_iterator ptr;
  for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
    atom_position pos(ptr->second);
    coord_type c[3];
    ptr->second->get_cartesian_coords(c);
    pos.set_coords(c);
    cluster.insert(pos);
  }
  int_g nops = p->get_number_no_trans_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  p->get_cart_no_trans_ops(rotations, nops);
  bool *valid_op = new_local_array1(bool, nops);
  for (int_g i = 0; i < nops; i++)
    valid_op[i] = true;
  // op[0] Should be the identity.
  for (int_g i = 1; i < nops; i++) {
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
      atom_position pos(ptr->second);
      coord_type c1[3];
      ptr->second->get_cartesian_coords(c1);
      pos.set_coords(c1);
      // No lattice, so we're safe avoiding mappings back into the cell.
      pos.rotate_cartesian(rotations[i]);
      if (cluster.locate_atom(pos) == (int_g)atom_basis.size()) {
	valid_op[i] = false;
	break;
      }
    }
  }
  // pack operators
  int_g j = 1;
  for (int_g i = 1; i < nops; i++) {
    if (valid_op[i]) {
      if (j < i) {
	rotations[j][0][0] = rotations[i][0][0];
	rotations[j][0][1] = rotations[i][0][1];
	rotations[j][0][2] = rotations[i][0][2];
	rotations[j][1][0] = rotations[i][1][0];
	rotations[j][1][1] = rotations[i][1][1];
	rotations[j][1][2] = rotations[i][1][2];
	rotations[j][2][0] = rotations[i][2][0];
	rotations[j][2][1] = rotations[i][2][1];
	rotations[j][2][2] = rotations[i][2][2];
      }
      j++;
    }
  }
  delete_local_array(valid_op);
  real_l (*t)[3] = new_local_array2(real_l, j, 3);
  for (int_g i = 0; i < j; i++) {
    t[i][0] = 0.0;
    t[i][1] = 0.0;
    t[i][2] = 0.0;
  }
  set_cartesian_sym_ops(rotations, t, j);
  delete_local_array(t);
  delete_local_array(rotations);
}

void DLV::model_base::check_symmetry(const atom parent[],
				     const coord_type a[3],
				     const coord_type b[3],
				     const coord_type c[3])
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  get_cart_rotation_operators(rotations, nops);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cart_translation_operators(translations, nops);
  bool *valid_op = new_local_array1(bool, nops);
  for (int_g i = 0; i < nops; i++)
    valid_op[i] = true;
  real_l cell[3][3];
  int_g dim = get_number_of_periodic_dims();
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, dim);
  // op[0] Should be the identity.
  for (int_g i = 1; i < nops; i++) {
    std::map<int, atom>::const_iterator ptr;
    int_g j = 0;
    for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
      atom_position pos(ptr->second);
      coord_type coords[3];
      ptr->second->get_cartesian_coords(coords);
      coord_type c1[3];
      transform_coordinates(c1, coords, rotations[i], translations[i]);
      pos.set_coords(c1, cell, inverse, dim);
      pos.get_cartesian_coords(c1);
      // Now try to find it - Todo, is there a better way?
      bool ok = false;
      std::map<int, atom>::const_iterator x;
      int_g k = 0;
      for (x = atom_basis.begin(); x != atom_basis.end(); ++x) {
	// If the atoms came from the same atom in the base structure
	if (parent[j] == parent[k]) {
	  atom_position data(x->second);
	  coord_type c2[3];
	  x->second->get_cartesian_coords(c2);
	  data.set_coords(c2, cell, inverse, dim);
	  data.get_cartesian_coords(c2);
	  // and if there is an atom at the transformed position
	  if (data.compare(pos) == 0) {
	    ok = (x->second->get_edit_flag() == ptr->second->get_edit_flag());
	    break;
	  }
	}
	k++;
      }
      if (!ok) {
	valid_op[i] = false;
	break;
      }
      j++;
    }
  }
  // pack operators
  int_g j = 1;
  for (int_g i = 1; i < nops; i++) {
    if (valid_op[i]) {
      if (j < i) {
	rotations[j][0][0] = rotations[i][0][0];
	rotations[j][0][1] = rotations[i][0][1];
	rotations[j][0][2] = rotations[i][0][2];
	rotations[j][1][0] = rotations[i][1][0];
	rotations[j][1][1] = rotations[i][1][1];
	rotations[j][1][2] = rotations[i][1][2];
	rotations[j][2][0] = rotations[i][2][0];
	rotations[j][2][1] = rotations[i][2][1];
	rotations[j][2][2] = rotations[i][2][2];
	translations[j][0] = translations[i][0];
	translations[j][1] = translations[i][1];
	translations[j][2] = translations[i][2];
      }
      j++;
    }
  }
  delete_local_array(valid_op);
  set_cartesian_sym_ops(rotations, translations, j);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::model_base::check_symmetry_no_z(const atom parent[],
					  const coord_type a[3],
					  const coord_type b[3],
					  const coord_type c[3])
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  get_cart_rotation_operators(rotations, nops);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cart_translation_operators(translations, nops);
  bool *valid_op = new_local_array1(bool, nops);
  for (int_g i = 0; i < nops; i++)
    valid_op[i] = true;
  real_l cell[3][3];
  int_g dim = get_number_of_periodic_dims();
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, dim);
  // op[0] Should be the identity.
  const real_l tol = 0.001;
  for (int_g i = 1; i < nops; i++) {
    // get rid of anything that isn't just a z reflection
    if (abs(translations[i][2]) > tol)
      valid_op[i] = false;
    else if (abs(rotations[i][0][2]) > tol or
	     abs(rotations[i][1][2]) > tol or
	     abs(rotations[i][2][0]) > tol or
	     abs(rotations[i][2][1]) > tol)
      valid_op[i] = false;
    else {
      std::map<int, atom>::const_iterator ptr;
      int_g j = 0;
      for (ptr = atom_basis.begin(); ptr != atom_basis.end(); ++ptr) {
	atom_position pos(ptr->second);
	coord_type coords[3];
	ptr->second->get_cartesian_coords(coords);
	coord_type c1[3];
	transform_coordinates(c1, coords, rotations[i], translations[i]);
	pos.set_coords(c1, cell, inverse, dim);
	pos.get_cartesian_coords(c1);
	// Now try to find it - Todo, is there a better way?
	bool ok = false;
	std::map<int, atom>::const_iterator x;
	int_g k = 0;
	for (x = atom_basis.begin(); x != atom_basis.end(); ++x) {
	  // If the atoms came from the same atom in the base structure
	  if (parent[j] == parent[k]) {
	    //if ((*x)->get_atomic_number() == (*ptr)->get_atomic_number()) {
	    atom_position data(x->second);
	    coord_type c2[3];
	    x->second->get_cartesian_coords(c2);
	    data.set_coords(c2, cell, inverse, dim);
	    data.get_cartesian_coords(c2);
	    // and if there is an atom at the transformed position
	    if (data.compare(pos) == 0) {
	      if (ptr->second->has_special_periodicity() ==
		  x->second->has_special_periodicity())
		ok = true;
	      break;
	    }
	  }
	  k++;
	}
	if (!ok) {
	  valid_op[i] = false;
	  break;
	}
	j++;
      }
    }
  }
  // pack operators
  int_g j = 1;
  for (int_g i = 1; i < nops; i++) {
    if (valid_op[i]) {
      if (j < i) {
	rotations[j][0][0] = rotations[i][0][0];
	rotations[j][0][1] = rotations[i][0][1];
	rotations[j][0][2] = rotations[i][0][2];
	rotations[j][1][0] = rotations[i][1][0];
	rotations[j][1][1] = rotations[i][1][1];
	rotations[j][1][2] = rotations[i][1][2];
	rotations[j][2][0] = rotations[i][2][0];
	rotations[j][2][1] = rotations[i][2][1];
	rotations[j][2][2] = rotations[i][2][2];
	translations[j][0] = translations[i][0];
	translations[j][1] = translations[i][1];
	translations[j][2] = translations[i][2];
      }
      j++;
    }
  }
  delete_local_array(valid_op);
  set_cartesian_sym_ops(rotations, translations, j);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::model_base::complete(const bool only_atoms)
{
  coord_type va[3];
  coord_type vb[3];
  coord_type vc[3];
  if (!only_atoms) {
    complete_symmetry();
    complete_lattice(va, vb, vc);
    complete_translators(va, vb, vc);
  } else
    get_primitive_lattice(va, vb, vc);
  int_g dim = get_number_of_periodic_dims();
  std::map<int, atom>::iterator x;
  for (x = atom_basis.begin(); x != atom_basis.end(); ++x )
    x->second->complete(va, vb, vc, dim);
  generate_primitive_atoms(true);
  // We may have tidied up special positions, so remove list
  primitive_atoms.clear();
}

void DLV::model_base::identify_bravais_lattice(const int_g old_l,
					       const int old_c)
{
  set_lattice_and_centre(0, 0);
}

void DLV::model_base::update_atom_list()
{
  if (primitive_atoms.size() == 0)
    generate_primitive_atoms();
}

void DLV::model_base::transform_coordinates(coord_type cnew[3],
					    const coord_type cold[3],
					    const real_l r[3][3],
					    const real_l t[3])
{
  // R * x + t
  for (int_g i = 0; i < 3; i++)
    cnew[i] = r[i][0] * cold[0] + r[i][1] * cold[1] + r[i][2] * cold[2] + t[i];
}

void
DLV::model_base::generate_primitive_copies(const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops, const bool tidy)
{
  if (primitive_atoms.size() > 0)
    primitive_atoms.clear();
  std::map<int, atom>::const_iterator x;
  int_g *equiv_atoms = 0;
  if (tidy)
    equiv_atoms = new_local_array1(int_g, atom_basis.size());
  int_g nequiv = 0;
  for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
    int_g count = 0;
    coord_type base[3] = { 0.0, 0.0, 0.0 };
    coord_type special[3] = { 0.0, 0.0, 0.0 };
    atom_position atom(x->second);
    coord_type coords[3];
    x->second->get_cartesian_coords(coords);
    bool added = false;
    for (int_g i = 0; i < nops; i++) {
      coord_type pos[3];
      transform_coordinates(pos, coords, rotations[i], translations[i]);
      atom.set_coords(pos);
      if (tidy) {
	atom.get_cartesian_coords(pos);
	if (i == 0) {
	  base[0] = pos[0];
	  base[1] = pos[1];
	  base[2] = pos[2];
	}
      }
      int_g size = primitive_atoms.size();
      primitive_atoms.insert(atom, true);
      if (primitive_atoms.size() > size) {
	added = true;
	if (tidy) {
	  if (atom.is_near_position(base)) {
	    special[0] += pos[0];
	    special[1] += pos[1];
	    special[2] += pos[2];
	    count++;
	  }
	}
      }
    }
    if (tidy) {
      if (added) {
	// Tidy position
	if (count > 1) {
	  coords[0] = special[0] / (coord_type)count;
	  coords[1] = special[1] / (coord_type)count;
	  coords[2] = special[2] / (coord_type)count;
	  x->second->set_cartesian_coords(coords);
	  coord_type a[3] = { 0.0, 0.0, 0.0 };
	  coord_type b[3] = { 0.0, 0.0, 0.0 };
	  coord_type c[3] = { 0.0, 0.0, 0.0 };
	  x->second->complete(a, b, c, 0);
	}
      } else { // record info to delete it.
	equiv_atoms[nequiv] = x->first;
	nequiv++;
      }
    }
  }
  if (nequiv > 0) {
    int_g natoms = int(atom_basis.size());
    for (int_g i = 0; i < nequiv; i++) {
      if (bonds.has_single_pairs()) {
	std::map<int,atom>::const_iterator z = atom_basis.find(equiv_atoms[i]);
	// Remove any bonds relating to this atom
	std::map<int,atom>::const_iterator y;
	for (y = atom_basis.begin(); y != atom_basis.end(); ++y ) {
	  if (bonds.is_pair_set(z->second, y->second))
	    bonds.clear_pair(z->second, y->second);
	}
      }
      atom_basis.erase(equiv_atoms[i]);
    }
    int_g k = 0;
    int_g j = 0;
    // reset atom ids
    for (int_g i = 0; i < natoms; i++) {
      if (equiv_atoms[k] == i)
	k++;
      else {
	if (i != j) {
	  std::map<int, atom>::const_iterator z = atom_basis.find(i);
	  atom p = z->second;
	  atom_basis.erase(i);
	  p->set_id(j);
	  atom_basis[j] = p;
	  bonds.reindex(i, j, natoms);
	}
	j++;
      }
    }
  }
  if (tidy)
    delete_local_array(equiv_atoms);
}

void
DLV::model_base::generate_primitive_copies(const real_l a[3],
					   const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops, const bool tidy)
{
  if (primitive_atoms.size() > 0)
    primitive_atoms.clear();
  int_g *equiv_atoms = 0;
  if (tidy)
    equiv_atoms = new_local_array1(int_g, atom_basis.size());
  int_g nequiv = 0;
  std::map<int, atom>::const_iterator x;
  real_l cell[3][3];
  cell[0][0] = a[0];
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 1);
  for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
    int_g count = 0;
    coord_type base[3] = { 0.0, 0.0, 0.0 };
    coord_type special[3] = { 0.0, 0.0, 0.0 };
    coord_type shift[3] = { 0.0, 0.0, 0.0 };
    atom_position atom(x->second);
    coord_type coords[3];
    x->second->get_cartesian_coords(coords);
    bool added = false;
    for (int_g i = 0; i < nops; i++) {
      coord_type pos[3];
      transform_coordinates(pos, coords, rotations[i], translations[i]);
      atom.set_coords(pos, cell, inverse, 1);
      if (tidy) {
	atom.get_fractional_coords(pos);
	if (i == 0) {
	  base[0] = pos[0];
	  base[1] = pos[1];
	  base[2] = pos[2];
	  atom.get_shift(shift);
	}
      }
      int_g size = primitive_atoms.size();
      primitive_atoms.insert(atom, true);
      if (primitive_atoms.size() > size) {
	added = true;
	if (tidy) {
	  if (atom.is_near_position(base)) {
	    special[0] += pos[0];
	    special[1] += pos[1];
	    special[2] += pos[2];
	    count++;
	  }
	}
      }
    }
    if (tidy) {
      if (added) {
	// Tidy position
	if (count > 1) {
	  coords[0] = special[0] / (coord_type)count - shift[0];
	  coords[1] = special[1] / (coord_type)count;
	  coords[2] = special[2] / (coord_type)count;
	  x->second->set_fractional_coords(coords);
	  coord_type b[3] = { 0.0, 0.0, 0.0 };
	  coord_type c[3] = { 0.0, 0.0, 0.0 };
	  x->second->complete(a, b, c, 1);
	}
      } else { // record info to delete it.
	equiv_atoms[nequiv] = x->first;
	nequiv++;
      }
    }
  }
  if (nequiv > 0) {
    int_g natoms = (int) atom_basis.size();
    for (int_g i = 0; i < nequiv; i++) {
      if (bonds.has_single_pairs()) {
	std::map<int,atom>::const_iterator z = atom_basis.find(equiv_atoms[i]);
	// Remove any bonds relating to this atom
	std::map<int,atom>::const_iterator y;
	for (y = atom_basis.begin(); y != atom_basis.end(); ++y ) {
	  if (bonds.is_pair_set(z->second, y->second))
	    bonds.clear_pair(z->second, y->second);
	}
      }
      atom_basis.erase(equiv_atoms[i]);
    }
    int_g k = 0;
    int_g j = 0;
    // reset atom ids
    for (int_g i = 0; i < natoms; i++) {
      if (equiv_atoms[k] == i)
	k++;
      else {
	if (i != j) {
	  std::map<int, atom>::const_iterator z = atom_basis.find(i);
	  atom p = z->second;
	  atom_basis.erase(i);
	  p->set_id(j);
	  atom_basis[j] = p;
	  bonds.reindex(i, j, natoms);
	}
	j++;
      }
    }
  }
  if (tidy)
    delete_local_array(equiv_atoms);
}

void
DLV::model_base::generate_primitive_copies(const real_l a[3],
					   const real_l b[3],
					   const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops, const bool tidy)
{
  if (primitive_atoms.size() > 0)
    primitive_atoms.clear();
  int_g *equiv_atoms = 0;
  if (tidy)
    equiv_atoms = new_local_array1(int_g, atom_basis.size());
  int_g nequiv = 0;
  std::map<int, atom>::const_iterator x;
  real_l cell[3][3];
  for (int_g i = 0; i < 2; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 2);
  for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
    int_g count = 0;
    coord_type base[3] = { 0.0, 0.0, 0.0 };
    coord_type special[3] = { 0.0, 0.0, 0.0 };
    coord_type shift[3] = { 0.0, 0.0, 0.0 };
    atom_position atom(x->second);
    coord_type coords[3];
    x->second->get_cartesian_coords(coords);
    bool added = false;
    for (int_g i = 0; i < nops; i++) {
      coord_type pos[3];
      transform_coordinates(pos, coords, rotations[i], translations[i]);
      atom.set_coords(pos, cell, inverse, 2);
      if (tidy) {
	atom.get_fractional_coords(pos);
	if (i == 0) {
	  base[0] = pos[0];
	  base[1] = pos[1];
	  base[2] = pos[2];
	  atom.get_shift(shift);
	}
      }
      int_g size = primitive_atoms.size();
      primitive_atoms.insert(atom, true);
      if (primitive_atoms.size() > size) {
	added = true;
	if (tidy) {
	  if (atom.is_near_position(base)) {
	    special[0] += pos[0];
	    special[1] += pos[1];
	    special[2] += pos[2];
	    count++;
	  }
	}
      }
    }
    if (tidy) {
      if (added) {
	// Tidy position
	if (count > 1) {
	  coords[0] = special[0] / (coord_type)count - shift[0];
	  coords[1] = special[1] / (coord_type)count - shift[1];
	  coords[2] = special[2] / (coord_type)count;
	  x->second->set_fractional_coords(coords);
	  coord_type c[3] = { 0.0, 0.0, 0.0 };
	  x->second->complete(a, b, c, 2);
	}
      } else { // record info to delete it.
	equiv_atoms[nequiv] = x->first;
	nequiv++;
      }
    }
  }
  if (nequiv > 0) {
    int_g natoms = (int) atom_basis.size();
    for (int_g i = 0; i < nequiv; i++) {
      if (bonds.has_single_pairs()) {
	std::map<int,atom>::const_iterator z = atom_basis.find(equiv_atoms[i]);
	// Remove any bonds relating to this atom
	std::map<int,atom>::const_iterator y;
	for (y = atom_basis.begin(); y != atom_basis.end(); ++y ) {
	  if (bonds.is_pair_set(z->second, y->second))
	    bonds.clear_pair(z->second, y->second);
	}
      }
      atom_basis.erase(equiv_atoms[i]);
    }
    int_g k = 0;
    int_g j = 0;
    // reset atom ids
    for (int_g i = 0; i < natoms; i++) {
      if (equiv_atoms[k] == i)
	k++;
      else {
	if (i != j) {
	  std::map<int, atom>::const_iterator z = atom_basis.find(i);
	  atom p = z->second;
	  atom_basis.erase(i);
	  p->set_id(j);
	  atom_basis[j] = p;
	  bonds.reindex(i, j, natoms);
	}
	j++;
      }
    }
  }
  if (tidy)
    delete_local_array(equiv_atoms);
}

void
DLV::model_base::generate_primitive_copies(const real_l a[3],
					   const real_l b[3],
					   const real_l c[3],
					   const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops, const bool tidy)
{
  if (primitive_atoms.size() > 0)
    primitive_atoms.clear();
  int_g *equiv_atoms = 0;
  if (tidy)
    equiv_atoms = new_local_array1(int_g, atom_basis.size());
  int_g nequiv = 0;
  std::map<int, atom>::const_iterator x;
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = c[i];
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, 3);
  for (x = atom_basis.begin(); x != atom_basis.end(); ++x ) {
    int_g count = 0;
    coord_type base[3] = { 0.0, 0.0, 0.0 };
    coord_type special[3] = { 0.0, 0.0, 0.0 };
    coord_type shift[3] = { 0.0, 0.0, 0.0 };
    atom_position atom(x->second);
    coord_type coords[3];
    x->second->get_cartesian_coords(coords);
    bool added = false;
    for (int_g i = 0; i < nops; i++) {
      coord_type pos[3];
      transform_coordinates(pos, coords, rotations[i], translations[i]);
      atom.set_coords(pos, cell, inverse, 3);
      if (tidy) {
	atom.get_fractional_coords(pos);
	if (i == 0) {
	  base[0] = pos[0];
	  base[1] = pos[1];
	  base[2] = pos[2];
	  atom.get_shift(shift);
	}
      }
      int_g size = primitive_atoms.size();
      primitive_atoms.insert(atom, true);
      if (primitive_atoms.size() > size) {
	added = true;
	if (tidy) {
	  if (atom.is_near_position(base)) {
	    special[0] += pos[0];
	    special[1] += pos[1];
	    special[2] += pos[2];
	    count++;
	  }
	}
      }
    }
    if (tidy) {
      if (added) {
	// Tidy position
	if (count > 1) {
	  coords[0] = special[0] / (coord_type)count - shift[0];
	  coords[1] = special[1] / (coord_type)count - shift[1];
	  coords[2] = special[2] / (coord_type)count - shift[2];
	  x->second->set_fractional_coords(coords);
	  x->second->complete(a, b, c, 3);
	}
      } else { // record info to delete it.
	equiv_atoms[nequiv] = x->first;
	nequiv++;
      }
    }
  }
  if (nequiv > 0) {
    int_g natoms = (int) atom_basis.size();
    for (int_g i = 0; i < nequiv; i++) {
      if (bonds.has_single_pairs()) {
	std::map<int,atom>::const_iterator z = atom_basis.find(equiv_atoms[i]);
	// Remove any bonds relating to this atom
	std::map<int,atom>::const_iterator y;
	for (y = atom_basis.begin(); y != atom_basis.end(); ++y ) {
	  if (bonds.is_pair_set(z->second, y->second))
	    bonds.clear_pair(z->second, y->second);
	}
      }
      atom_basis.erase(equiv_atoms[i]);
    }
    int_g k = 0;
    int_g j = 0;
    // reset atom ids
    for (int_g i = 0; i < natoms; i++) {
      if (equiv_atoms[k] == i)
	k++;
      else {
	if (i != j) {
	  std::map<int, atom>::const_iterator z = atom_basis.find(i);
	  atom p = z->second;
	  atom_basis.erase(i);
	  p->set_id(j);
	  atom_basis[j] = p;
	  bonds.reindex(i, j, natoms);
	}
	j++;
      }
    }
  }
  if (tidy)
    delete_local_array(equiv_atoms);
}

void DLV::model_base::generate_conventional_atom_display(atom_tree &tree,
							 const bool centre[3])
{
  if (primitive_atoms.size() == 0)
    generate_primitive_atoms();
  //primitive_atoms.copy(tree);
  conventional_atoms(primitive_atoms, tree, centre);
}

void DLV::model_base::generate_primitive_atom_display(atom_tree &tree,
						      const bool centre[3])
{
  if (primitive_atoms.size() == 0)
    generate_primitive_atoms();
  if (centre[0] or centre[1] or centre[2])
    centre_atoms(primitive_atoms, tree, centre);
  else
    primitive_atoms.copy(tree);
}

void DLV::model_base::count_atom_display_list(int_g &natoms1, int_g &natoms2,
					      int_g &natoms3,
					      const int_g select_type,
					      const bool use_flags,
					      const int_g flag_type) const
{
  bool transparent_flags = (use_flags and (flag_type == 2));
  switch (select_type) {
  case 0:
    natoms1 = display_atoms.count_atoms(false, true, transparent_flags,
					use_edit_flags, false);
    if (transparent_flags)
      natoms2 = display_atoms.count_atoms(false, true, true,
					  use_edit_flags, false);
    else
      natoms2 = 0;
    break;
  case 1:
    natoms1 = display_atoms.count_atoms(false, false, transparent_flags,
					use_edit_flags, false);
    natoms2 = display_atoms.count_atoms(true, false, transparent_flags,
					use_edit_flags, false);
    break;
  case 2:
    natoms1 = display_atoms.count_atoms(true, false, transparent_flags,
					use_edit_flags, false);
    natoms2 = display_atoms.count_atoms(false, false, transparent_flags,
					use_edit_flags, false);
    break;
  case 3:
    natoms1 = display_atoms.count_atoms(false, false, transparent_flags,
					use_edit_flags, false);
    if (transparent_flags) // Todo - is this correct?
      natoms2 = display_atoms.count_atoms(false, false, true,
					  use_edit_flags, false);
    else
      natoms2 = 0;
    break;
  }
  if (use_edit_flags)
    natoms3 = display_atoms.count_atoms(true, false, transparent_flags,
					use_edit_flags, true);
  else
    natoms3 = 0;
}

void DLV::model_base::get_opaque_atoms(real_g (* &coords)[3],
				       real_g (* &colours)[3], real_g * &radii,
				       const real_g scale, const int_g natoms,
				       const int_g select_type,
				       const bool use_flags,
				       const int_g flag_type,
				       const std::vector<real_g (*)[3]> *traj,
				       const int_g nframes)
{
  coords = new real_g[natoms * nframes][3];
  colours = new real_g[natoms][3];
  radii = new real_g[natoms];
  switch (select_type) {
  case 0:
    display_atoms.fill_arrays(coords, colours, radii, false, true,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 1:
    display_atoms.fill_arrays(coords, colours, radii, false, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 2:
    display_atoms.fill_arrays(coords, colours, radii, true, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 3:
    display_atoms.fill_arrays(coords, colours, radii, false, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  default:
    break;
  }
  for (int_g i = 0; i < natoms; i++)
    radii[i] *= scale;
}

void DLV::model_base::get_transparent_atoms(real_g (* &coords)[3],
					    real_g (* &colours)[3],
					    real_g * &radii, const real_g scale,
					    const int_g natoms,
					    const int_g select_type,
					    const bool use_flags,
					    const int_g flag_type,
					const std::vector<real_g (*)[3]> *traj,
					    const int_g nframes)
{
  // Todo - is it more efficient to do a single fill of all values?
  coords = new real_g[natoms * nframes][3];
  colours = new real_g[natoms][3];
  radii = new real_g[natoms];
  switch (select_type) {
  case 0:
    display_atoms.fill_arrays(coords, colours, radii, false, true,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 1:
    display_atoms.fill_arrays(coords, colours, radii, true, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 2:
    display_atoms.fill_arrays(coords, colours, radii, false, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  case 3: // Todo - is this correct?
    display_atoms.fill_arrays(coords, colours, radii, false, false,
			      use_flags, flag_type, traj, nframes, natoms,
			      use_edit_flags, false);
    break;
  default:
    break;
  }
  for (int_g i = 0; i < natoms; i++)
    radii[i] *= scale;
}

void DLV::model_base::get_edit_op_atoms(real_g (* &coords)[3],
					real_g (* &colours)[3], real_g * &radii,
					const real_g scale, const int_g natoms,
					const std::vector<real_g (*)[3]> *traj,
					const int_g nframes)
{
  if (use_edit_flags) {
    coords = new real_g[natoms * nframes][3];
    colours = new real_g[natoms][3];
    radii = new real_g[natoms];
    // select type -> 0
    display_atoms.fill_arrays(coords, colours, radii, false, true,
			      false, 0, traj, nframes, natoms,
			      use_edit_flags, true);
    for (int_g i = 0; i < natoms; i++)
      radii[i] *= scale;
  }
}

void DLV::model_base::count_bond_list(const std::list<bond_list> &info,
				      int_l &nbonds1, int_l &nbonds2,
				      int_l &nbonds3, int_l &nvertices,
				      const bool polyh) const
{
  nbonds1 = 0;
  nbonds2 = 0;
  nbonds3 = 0;
  nvertices = 0;
  std::list<bond_list>::const_iterator x;
  if (polyh) {
    for (x = info.begin(); x != info.end(); ++x) {
      if (x->type1 == 2) {
	if (x->type2 != 2)
	  nvertices++;
      } else if (x->type2 == 2) { // type1 != 2
	nvertices++;
      } else {
	switch (x->type1) {
	case 3:
	  nbonds3++;
	  break;
	case 1:
	  nbonds1++;
	  break;
	default:
	  break;
	}
	switch (x->type2) {
	case 3:
	  nbonds3++;
	  break;
	case 1:
	  nbonds1++;
	  break;
	default:
	  break;
	}
      }
    }
  } else {
    for (x = info.begin(); x != info.end(); ++x) {
      // 0 is outside cell (type2 only), 1 normal, 2 select, 3 edit
      switch (x->type1) {
      case 2:
	nbonds2++;
	break;
      case 3:
	nbonds3++;
	break;
      case 1:
	nbonds1++;
	break;
      default:
	break;
      }
      switch (x->type2) {
      case 2:
	nbonds2++;
	break;
      case 3:
	nbonds3++;
	break;
      case 1:
	nbonds1++;
	break;
      default:
	break;
      }
    }
  }
}

void DLV::model_base::generate_line_bonds(const std::list<bond_list> &info,
					  real_g (* &pos)[3],
					  real_g (* &rgb)[3], int_l &nbonds,
					  int_l * &connects, int_l &nconnects,
					  const int_g btype) const
{
  int_g count = 0;
  std::list<bond_list>::const_iterator x;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == btype)
      count += 2;
    if (x->type2 == btype)
      count += 2;
  }
  pos = new real_g[count][3];
  rgb = new real_g[count][3];
  connects = new int_l[count];
  int_l j = 0;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == btype) {
      pos[j][0] = x->start[0];
      pos[j][1] = x->start[1];
      pos[j][2] = x->start[2];
      pos[j + 1][0] = x->middle[0];
      pos[j + 1][1] = x->middle[1];
      pos[j + 1][2] = x->middle[2];
      rgb[j][0] = x->colour1.red;
      rgb[j][1] = x->colour1.green;
      rgb[j][2] = x->colour1.blue;
      rgb[j + 1][0] = x->colour1.red;
      rgb[j + 1][1] = x->colour1.green;
      rgb[j + 1][2] = x->colour1.blue;
      connects[j] = j;
      connects[j + 1] = j + 1;
      j += 2;
    }
    if (x->type2 == btype) {
      pos[j][0] = x->end[0];
      pos[j][1] = x->end[1];
      pos[j][2] = x->end[2];
      pos[j + 1][0] = x->middle[0];
      pos[j + 1][1] = x->middle[1];
      pos[j + 1][2] = x->middle[2];
      rgb[j][0] = x->colour2.red;
      rgb[j][1] = x->colour2.green;
      rgb[j][2] = x->colour2.blue;
      rgb[j + 1][0] = x->colour2.red;
      rgb[j + 1][1] = x->colour2.green;
      rgb[j + 1][2] = x->colour2.blue;
      connects[j] = j;
      connects[j + 1] = j + 1;
      j += 2;
    }
  }
  nconnects = count;
  nbonds = count;
}

void DLV::model_base::generate_tube_bonds(const std::list<bond_list> &info,
					  real_g (* &pos)[3],
					  real_g (* &rgb)[3], int_l &nbonds,
					  int_l * &connects, int_l &nconnects,
					  const int_g btype,
					  const real_g radius,
					  const int_g subdiv) const
{
  int_g count = 0;
  std::list<bond_list>::const_iterator x;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == btype)
      count += 2;
     if (x->type2 == btype)
       count += 2;
  }
  int_g steps = subdiv + 1;
  real_g (*segments)[3] = new_local_array2(real_g, steps, 3);
  real_g resolution = real_g((2.0 * pi / (double)(steps - 1)));
  for (int_g i = 0; i < steps; i++) {
    real_g angle = ((real_g) i) * resolution;
    // Make circle in yz plane
    segments[i][0] = 0.0;
    segments[i][1] = radius * cos(angle);
    segments[i][2] = radius * sin(angle);
  }
  // Total vertices are nbonds * 4 * steps, 4 * nbonds polymesh connections.
  nbonds = count * steps;
  pos = new real_g[count * steps][3];
  rgb = new real_g[count * steps][3];
  int_l c = 0;
  const real_g tol = 1e-5f;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == btype) {
      // There are 'steps' polygons, n vertices and colours ([3] of course).
      real_g bdir[3];
      for (int_g k = 0; k < 3; k++)
	bdir[k] = x->middle[k] - x->start[k];
      real_g bxyl = bdir[0] * bdir[0] + bdir[1] * bdir[1];
      real_g bl = sqrt(bxyl + bdir[2] * bdir[2]);
      bxyl = sqrt(bxyl);
      // create rotation, which rotates (b,0,0) into bdir, so it will correctly
      // rotate the circle coords into the face of the bond. We rotate to the
      // correct angle in xz then rotate in xy using spherical coord angles.
      real_g cphi;
      real_g sphi;
      if (bxyl < tol) {
	cphi = 1.0;
	sphi = 0.0;
      } else {
	cphi = bdir[0] / bxyl;
	sphi = bdir[1] / bxyl;
      }
      real_g ctheta = bdir[2] / bl;
      real_g stheta = bxyl / bl;
      real_g r[3][3];
      r[0][0] = cphi * stheta;
      r[0][1] = - sphi;
      r[0][2] = - cphi * ctheta;
      r[1][0] = sphi * stheta;
      r[1][1] = cphi;
      r[1][2] = - sphi * ctheta;
      r[2][0] = ctheta;
      r[2][1] = 0.0;
      r[2][2] = stheta;
      for (int_g k = 0; k < steps; k++) {
	// So rotate segments coords and add to the end points.
	real_g y[3];
	for (int_g ii = 0; ii < 3; ii++) {
	  y[ii] = 0.0;
	  for (int_g jj = 0; jj < 3; jj++)
	    y[ii] += r[ii][jj] * segments[k][jj];
	}
	for (int_g l = 0; l < 3; l++) {
	  pos[c + 2 * k][l] = x->start[l] + y[l];
	  pos[c + 2 * k + 1][l] = x->middle[l] + y[l];
	}
	rgb[c + 2 * k][0] = x->colour1.red;
	rgb[c + 2 * k][1] = x->colour1.green;
	rgb[c + 2 * k][2] = x->colour1.blue;
	rgb[c + 2 * k + 1][0] = x->colour1.red;
	rgb[c + 2 * k + 1][1] = x->colour1.green;
	rgb[c + 2 * k + 1][2] = x->colour1.blue;
      }
      c += 2 * steps;
    }
    // Todo - subroutine to avoid duplication
    if (x->type2 == btype) {
      // There are 'steps' polygons, n vertices and colours ([3] of course).
      real_g bdir[3];
      for (int_g k = 0; k < 3; k++)
	bdir[k] = x->middle[k] - x->end[k];
      real_g bxyl = bdir[0] * bdir[0] + bdir[1] * bdir[1];
      real_g bl = sqrt(bxyl + bdir[2] * bdir[2]);
      bxyl = sqrt(bxyl);
      // create rotation, which rotates (b,0,0) into bdir, so it will correctly
      // rotate the circle coords into the face of the bond. We rotate to the
      // correct angle in xz then rotate in xy using spherical coord angles.
      real_g cphi;
      real_g sphi;
      if (bxyl < tol) {
	cphi = 1.0;
	sphi = 0.0;
      } else {
	cphi = bdir[0] / bxyl;
	sphi = bdir[1] / bxyl;
      }
      real_g ctheta = bdir[2] / bl;
      real_g stheta = bxyl / bl;
      real_g r[3][3];
      r[0][0] = cphi * stheta;
      r[0][1] = - sphi;
      r[0][2] = - cphi * ctheta;
      r[1][0] = sphi * stheta;
      r[1][1] = cphi;
      r[1][2] = - sphi * ctheta;
      r[2][0] = ctheta;
      r[2][1] = 0.0;
      r[2][2] = stheta;
      for (int_g k = 0; k < steps; k++) {
	// So rotate segments coords and add to the end points.
	real_g y[3];
	for (int_g ii = 0; ii < 3; ii++) {
	  y[ii] = 0.0;
	  for (int_g jj = 0; jj < 3; jj++)
	    y[ii] += r[ii][jj] * segments[k][jj];
	}
	for (int_g l = 0; l < 3; l++) {
	  pos[c + 2 * k][l] = x->end[l] + y[l];
	  pos[c + 2 * k + 1][l] = x->middle[l] + y[l];
	}
	rgb[c + 2 * k][0] = x->colour2.red;
	rgb[c + 2 * k][1] = x->colour2.green;
	rgb[c + 2 * k][2] = x->colour2.blue;
	rgb[c + 2 * k + 1][0] = x->colour2.red;
	rgb[c + 2 * k + 1][1] = x->colour2.green;
	rgb[c + 2 * k + 1][2] = x->colour2.blue;
      }
      c += 2 * steps;
    }
  }
  connects = new int_l[count];
  int_l j = 0;
  for (int_l k = 0; k < count; k += 2) {
    connects[k] = j;
    connects[k + 1] = j + 2 * steps - 1;
    j += 2 * steps;
  }
  nconnects = count;
  delete_local_array(segments);
}

void DLV::model_base::build_triangles(polyhedron &data, real_g pos[][3],
				      const real_g centre[3])
{
  // generate all triples
  int_g size = data.indices.size();
  std::list<int_l>::const_iterator aptr = data.indices.begin();
  for (int_g i = 0; i < size - 2; i++) {
    int_l a = *aptr;
    std::list<int_l>::const_iterator bptr = aptr;
    for (int_g j = i + 1; j < size - 1; j++) {
      bptr++;
      int_l b = *bptr;
      std::list<int_l>::const_iterator cptr = bptr;
      for (int_g k = j + 1; k < size; k++) {
	cptr++;
	int_l c = *cptr;
	// compute plane normal
	real_g v21[3];
	v21[0] = pos[b][0] - pos[a][0];
	v21[1] = pos[b][1] - pos[a][1];
	v21[2] = pos[b][2] - pos[a][2];
	real_g v31[3];
	v31[0] = pos[c][0] - pos[a][0];
	v31[1] = pos[c][1] - pos[a][1];
	v31[2] = pos[c][2] - pos[a][2];
	// cross product
	real_g vc[3];
	vc[0] = v21[1] * v31[2] - v21[2] * v31[1];
	vc[1] = v21[2] * v31[0] - v21[0] * v31[2];
	vc[2] = v21[0] * v31[1] - v21[1] * v31[0];
	// normalize
	real_g l = vc[0] * vc[0] + vc[1] * vc[1] + vc[2] * vc[2];
	l = sqrt(l);
	vc[0] /= l;
	vc[1] /= l;
	vc[2] /= l;
	// line from plane centre to central atom
	real_g mid[3];
	mid[0] = (pos[a][0] + pos[b][0] + pos[c][0]) / real_g(3.0);
	mid[1] = (pos[a][1] + pos[b][1] + pos[c][1]) / real_g(3.0);
	mid[2] = (pos[a][2] + pos[b][2] + pos[c][2]) / real_g(3.0);
	real_g c_m[3];
	c_m[0] = centre[0] - mid[0];
	c_m[1] = centre[1] - mid[1];
	c_m[2] = centre[2] - mid[2];
	real_g l_c_m = c_m[0] * c_m[0] + c_m[1] * c_m[1] + c_m[2] * c_m[2];
	// angle between normal and line to central atom
	real_g cos_c_m = (c_m[0] * vc[0] + c_m[1] * vc[1] + c_m[2] * vc[2]);
	cos_c_m /= sqrt(l_c_m);
	// make sure its not almost on to of the centre
	int_g flag = 0;
	if (abs(l_c_m) > 0.01) {
	  // loop over atoms, if any other atom is on the otherside of this plane
	  // and within the extent of the plane then its probably not a boundary
	  std::list<int_l>::const_iterator dptr = data.indices.begin();
	  for (int_g m = 0; m < size; m++) {
	    if (m != i and m != j and m != k) {
	      int_l d = *dptr;
	      // See which side its on
	      real_g a_m[3];
	      a_m[0] = pos[d][0] - mid[0];
	      a_m[1] = pos[d][1] - mid[1];
	      a_m[2] = pos[d][2] - mid[2];
	      real_g l_a_m = a_m[0] * a_m[0] + a_m[1] * a_m[1] + a_m[2] * a_m[2];
	      // angle between normal and line to central atom
	      real_g cos_a_m = (a_m[0] * vc[0] + a_m[1] * vc[1] + a_m[2] * vc[2]);
	      cos_a_m /= sqrt(l_a_m);
	      if (((cos_a_m > 0.0 and cos_c_m < 0.0) or
		   (cos_c_m > 0.0 and cos_a_m < 0.0)) and abs(cos_a_m) > 0.001) {
		flag = 1;
		break;
	      } else {
		// How to project plane to atom to define 'within'?
		// It may be needed for 'convex' issues with truncated objects
		// at surfaces etc
	      }
	    }
	    ++dptr;
	  }
	} else
	  flag = 1;
	/*
	// compute midpoints
	real_g v12[3];
	v12[0] = (pos[a][0] + pos[b][0]) / real_g(2.0);
	v12[1] = (pos[a][1] + pos[b][1]) / real_g(2.0);
	v12[2] = (pos[a][2] + pos[b][2]) / real_g(2.0);
	real_g v13[3];
	v13[0] = (pos[a][0] + pos[c][0]) / real_g(2.0);
	v13[1] = (pos[a][1] + pos[c][1]) / real_g(2.0);
	v13[2] = (pos[a][2] + pos[c][2]) / real_g(2.0);
	real_g v23[3];
	v23[0] = (pos[b][0] + pos[c][0]) / real_g(2.0);
	v23[1] = (pos[b][1] + pos[c][1]) / real_g(2.0);
	v23[2] = (pos[b][2] + pos[c][2]) / real_g(2.0);
	// compare to central atom
	v12[0] -= centre[0];
	v12[1] -= centre[1];
	v12[2] -= centre[2];
	v13[0] -= centre[0];
	v13[1] -= centre[1];
	v13[2] -= centre[2];
	v23[0] -= centre[0];
	v23[1] -= centre[1];
	v23[2] -= centre[2];
	// 0.5 was fractional lattice coord, but only valid for crystal!
	// Todo - what should this number be?
	int_g flag = 0;
	if ((v12[0] * v12[0] + v12[1] * v12[1] + v12[2] * v12[2]) < real_g(0.5))
	  flag++;
	if ((v13[0] * v13[0] + v13[1] * v13[1] + v13[2] * v13[2]) < real_g(0.5))
	  flag++;
	if ((v23[0] * v23[0] + v23[1] * v23[1] + v23[2] * v23[2]) < real_g(0.5))
	  flag++;
	*/
	if (flag == 0) {
	  // compute clockwise normal - does this work always, see wulff
	  real_g v21[3];
	  v21[0] = pos[b][0] - pos[a][0];
	  v21[1] = pos[b][1] - pos[a][1];
	  v21[2] = pos[b][2] - pos[a][2];
	  real_g v31[3];
	  v31[0] = pos[c][0] - pos[a][0];
	  v31[1] = pos[c][1] - pos[a][1];
	  v31[2] = pos[c][2] - pos[a][2];
	  // cross product
	  real_g vc[3];
	  vc[0] = v21[1] * v31[2] - v21[2] * v31[1];
	  vc[1] = v21[2] * v31[0] - v21[0] * v31[2];
	  vc[2] = v21[0] * v31[1] - v21[1] * v31[0];
	  // normalize
	  real_g l = vc[0] * vc[0] + vc[1] * vc[1] + vc[2] * vc[2];
	  l = sqrt(l);
	  vc[0] /= l;
	  vc[1] /= l;
	  vc[2] /= l;
	  // Don't think I believe this - vector from poly centre to face
	  // shouldn't it be pos[c][0]?
	  v21[0] = pos[a][0] - v21[0];
	  v21[1] = pos[a][1] - v21[1];
	  v21[2] = pos[a][2] - v21[2];
	  // dot product
	  real_g angle = v21[0] * vc[0] + v21[1] * vc[1] + v21[2] * vc[2];
	  angle /= sqrt(v21[0] * v21[0] + v21[1] * v21[1]
			+ v21[2] * v21[2]); // vc already norm
	  angle = acos(angle);
	  // NOT reversed since AVS uses counter-clockwise normal
	  if (angle < real_g(DLV::pi / 2.0)) {
	    triple xx;
	    xx.i = a;
	    xx.j = b;
	    xx.k = c;
	    data.triangles.push_back(xx);
	  } else {
	    triple xx;
	    xx.i = a;
	    xx.j = c;
	    xx.k = b;
	    data.triangles.push_back(xx);
	  }
	}
	/*
	if (data.triangles.size() == 16) {
	  fprintf(stderr, "Triangle generation problem\n");
	  break;
	}
	*/
      }
      /*
      if (data.triangles.size() == 16)
	break;
      */
    }
    /*
    if (data.triangles.size() == 16)
      break;
    */
    aptr++;
  }
}

bool DLV::model_base::generate_polyhedra(const std::list<bond_list> &info,
					 real_g (* &pos)[3], real_g (* &rgb)[3],
					 int_l &nverts, int_l * &connects,
					 int_l &nconnects) const
{
  int_l nvertices = 0;
  std::list<bond_list>::const_iterator x;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == 2) {
      if (x->type2 != 2)
	nvertices++;
    } else if (x->type2 == 2) { // type1 != 2
      nvertices++;
    }
  }
  nverts = nvertices;
  pos = new real_g[nvertices][3];
  rgb = new real_g[nvertices][3];
  std::map<poly_centre, polyhedron> poly_data;
  int_g j = 0;
  poly_centre centre;
  std::map<poly_centre, polyhedron>::iterator ptr;
  for (x = info.begin(); x != info.end(); ++x) {
    if (x->type1 == 2) {
      if (x->type2 != 2) {
	centre.pos[0] = x->start[0];
	centre.pos[1] = x->start[1];
	centre.pos[2] = x->start[2];
	pos[j][0] = x->end[0];
	pos[j][1] = x->end[1];
	pos[j][2] = x->end[2];
	rgb[j][0] = x->colour1.red;
	rgb[j][1] = x->colour1.green;
	rgb[j][2] = x->colour1.blue;
	ptr = poly_data.find(centre);
	if (ptr != poly_data.end())
	  ptr->second.indices.push_back(j);
	else
	  poly_data[centre].indices.push_back(j);
	j++;
      }
    } else if (x->type2 == 2) { // type1 != 2
      centre.pos[0] = x->end[0];
      centre.pos[1] = x->end[1];
      centre.pos[2] = x->end[2];
      pos[j][0] = x->start[0];
      pos[j][1] = x->start[1];
      pos[j][2] = x->start[2];
      rgb[j][0] = x->colour2.red;
      rgb[j][1] = x->colour2.green;
      rgb[j][2] = x->colour2.blue;
      ptr = poly_data.find(centre);
      if (ptr != poly_data.end())
	ptr->second.indices.push_back(j);
      else
	poly_data[centre].indices.push_back(j);
      j++;
    }
  }
  // Build triangle list (and connections) from vertices around each centre
  int_l ntriangles = 0;
  for (ptr = poly_data.begin(); ptr != poly_data.end(); ++ptr) {
    nvertices = ptr->second.indices.size();
    if (nvertices > 3) {
      build_triangles(ptr->second, pos, ptr->first.pos);
      ntriangles += ptr->second.triangles.size();
    }
  }
  nconnects = 3 * ntriangles;
  connects = new int_l[3 * ntriangles];
  int_g count = 0;
  bool ok = true;
  for (ptr = poly_data.begin(); ptr != poly_data.end(); ++ptr) {
    if (ptr->second.triangles.size() > 0) {
      std::list<triple>::const_iterator tp;
      for (tp = ptr->second.triangles.begin();
	   tp != ptr->second.triangles.end(); ++tp) {
	connects[count + 0] = tp->i;
	connects[count + 1] = tp->j;
	connects[count + 2] = tp->k;
	count += 3;
      }
    } else
      ok = false;
  }
  return ok;
}

void DLV::model_base::set_bond_all()
{
  (void) bonds.set_all(true, atom_basis);
}

void DLV::model_base::generate_atom_display_list(const int_g na, const int_g nb,
						 const int_g nc,
						 const bool centre_cell,
						 const bool conv_cell,
						 const bool edges,
						 const real_l tol,
						 int_g &natoms1, int_g &natoms2,
						 int_g &natoms3,
						 const int_g select_type,
						 const int_g sym_select,
						 const bool use_flags,
						 const int_g flag_type)
{
  // Todo - ideally we should update the existing list if possible.
  atom_tree previous_tree = display_atoms;
  display_atoms = atom_tree();
  bool centre[3] = { false, false, false };
  if (centre_cell) {
    centre[0] = (na % 2) == 1;
    centre[1] = (nb % 2) == 1;
    centre[2] = (nc % 2) == 1;
  }
  if (conv_cell)
    generate_conventional_atom_display(display_atoms, centre);
  else
    generate_primitive_atom_display(display_atoms, centre);
  update_display_atoms(display_atoms, tol, na, nb, nc, conv_cell,
		       centre_cell, edges);
#ifdef ENABLE_DLV_GRAPHICS
  int_g atom_gp = render_rspace->get_atom_group_index();
  string gp = find_atom_group(atom_gp);
  previous_tree.copy_selected_atoms(display_atoms, sym_select, gp);
#endif // ENABLE DLV_GRAPHICS
  // 1 is opaque atoms list, 2 is transparent list, 3 is detached set for
  // editing via transforms.
  count_atom_display_list(natoms1, natoms2, natoms3, select_type,
			  use_flags, flag_type);
  // previous_tree should destruct on exit.
}

#ifdef ENABLE_DLV_GRAPHICS

bool DLV::model_base::has_selected_atoms() const
{
  int_g i = display_atoms.get_number_selected_atoms();
  return (i > 0);
}

void DLV::model_base::list_atom_groupings(render_parent *p) const
{
  p->set_atom_group_size((int)group_names.size());
  std::list<string>::const_iterator x;
  int_g i = 0;
  for (x = group_names.begin(); x != group_names.end(); ++x ) {
    p->set_atom_group_name(*x, i);
    i++;
  }
}

DLV::string DLV::model_base::find_atom_group(const int_g index) const
{
  int_g i = 0;
  std::list<string>::const_iterator ptr;
  for (ptr = group_names.begin(); ptr != group_names.end(); ++ptr ) {
    if (index == i) {
      return *ptr;
      break;
    }
    i++;
  }
  return "";
}

DLV::int_g
DLV::model_base::get_atom_selection_info(real_l &x, real_l &y, real_l &z,
					 real_l &length, real_l &x2,
					 real_l &y2, real_l &z2,
					 real_l &angle, char sym1[],
					 char sym2[], char sym3[],
					 real_g &r1, real_g &r2,
					 real_g &r3, string &gp,
					 string &label) const
{
  int_g index = render_rspace->get_atom_group_index();
  gp = find_atom_group(index);
  return display_atoms.get_atom_selection_info(x, y, z, length, x2, y2, z2,
					       angle, sym1, sym2, sym3,
					       r1, r2, r3, gp, label);
}

DLV::int_g DLV::model_base::get_number_selected_atoms() const
{
  return display_atoms.get_number_selected_atoms();
}

bool DLV::model_base::get_selected_positions(coord_type p[3]) const
{
  return display_atoms.get_selected_positions(p);
}

bool DLV::model_base::get_selected_positions(coord_type p1[3],
					     coord_type p2[3]) const
{
  return display_atoms.get_selected_positions(p1, p2);
}

bool DLV::model_base::get_selected_positions(coord_type p1[3],
					     coord_type p2[3],
					     coord_type p3[3]) const
{
  return display_atoms.get_selected_positions(p1, p2, p3);
}

bool DLV::model_base::get_average_selection_position(coord_type p[3]) const
{
  return display_atoms.get_average_selection_position(p);
}

DLV::int_g DLV::model_base::locate_selected_atom() const
{
  return display_atoms.locate_selected_atom();
}

DLV::int_g DLV::model_base::find_primitive_selection() const
{
  return display_atoms.find_primitive_selection(atom_basis);
}

DLV::render_parent *
DLV::model_base::create_render_parent(render_parent *p, const bool copy_cell,
				      const bool copy_wavefn)
{
  render_parent *rp = new render_atoms(get_model_name().c_str());
  if (p != 0) {
    rp->copy_settings(p, copy_cell, copy_wavefn);
  }
  return rp;
}

DLVreturn_type DLV::model_base::render_r(render_parent *p,
					 const model *m,
					 char message[], const int_g mlen)
{
  if (render_rspace == 0) {
    render_rspace = new model_display_r(p);
    if (m != 0) {
      const model_base *obj = static_cast<const model_base *>(m);
      render_rspace->copy_settings(obj->render_rspace);
    }
  }
  return update_cell(p, message, mlen);
}

DLVreturn_type DLV::model_base::render_k(render_parent *p,
					 char message[], const int_g mlen)
{
  if (render_kspace == 0)
    render_kspace = new model_display_k(p);
  return update_lattice(p, true, message, mlen);
}

class DLV::toolkit_obj DLV::model_base::get_r_ui_obj() const
{
  if (render_rspace == 0)
    return toolkit_obj();
  else
    return render_rspace->get_ui_obj();
}

class DLV::toolkit_obj DLV::model_base::get_k_ui_obj() const
{
  if (render_kspace == 0)
    return toolkit_obj();
  else
    return render_kspace->get_ui_obj();
}

DLV::int_g DLV::model_base::get_symmetry_selector() const
{
  return render_rspace->get_selection_symmetry();
}

void DLV::model_base::fix_symmetry_selector(const int_g s)
{
  render_rspace->set_selection_symmetry(s);
}

DLVreturn_type DLV::model_base::update_cell(render_parent *p,
					    char message[], const int_g mlen)
{
  int_g na;
  int_g nb;
  int_g nc;
  bool centre_cell;
  bool conventional;
  bool edges;
  real_l tol;
  p->get_cell_data(na, nb, nc, centre_cell, conventional, edges, tol);
  DLVreturn_type ok = redraw_atoms(p, message, mlen);
  if (ok == DLV_OK) {
    ok = update_bonds(p, message, mlen);
    if (ok == DLV_OK)
      ok = update_lattice(p, false, message, mlen);
  }
  int_g ntransforms = 1;
  real_g (*transforms)[3];
  generate_transforms(na, nb, nc, 1, 1, 1, centre_cell, conventional,
		      ntransforms, transforms);
  p->set_transforms(ntransforms, transforms);
  delete [] transforms;
  return ok;
}

DLVreturn_type DLV::model_base::update_lattice(render_parent *p,
					       const bool kspace,
					       char message[], const int_g mlen)
{
  // need a better way to copy settings that avoids draw for molecules.
  if (get_number_of_periodic_dims() == 0)
    return DLV_OK;
  int_g nlines = 1;
  if (kspace) {
    bool draw;
    bool labels;
    if (render_kspace == 0)
      render_kspace = new model_display_k(p);
    // Todo - need to render and attach_ui if user switches to kspace
    // before this routine, otherwise the next call isn't any use.
    render_kspace->get_lattice_data(draw, labels);
    if (draw) {
      real_g (*vertices)[3];
      int_l *connects;
      int_l nconnects;
      generate_k_space_vertices(vertices, nlines, connects, nconnects,
				message, mlen);
      if (nlines > 0) {
	render_kspace->set_lattice(vertices, nlines, connects, nconnects);
	delete [] connects;
	delete [] vertices;
      }
    } else
      render_kspace->detach_lattice();
  } else {
    bool draw;
    bool labels;
    render_rspace->get_lattice_data(draw, labels);
    if (draw) {
      int_g na;
      int_g nb;
      int_g nc;
      bool centre_cell;
      bool conv_cell;
      bool edges;
      real_l tol;
      p->get_cell_data(na, nb, nc, centre_cell, conv_cell, edges, tol);
      bool conventional = conv_cell;
      real_g (*ipoints)[3];
      real_g (*fpoints)[3];
      generate_real_space_lattice(ipoints, fpoints, nlines, na, nb, nc,
				  centre_cell, conventional, message, mlen);
      if (nlines > 0) {
	render_rspace->set_lattice(ipoints, fpoints, nlines);
	delete [] fpoints;
	delete [] ipoints;
	if (labels) {
	  real_g positions[4][3];
	  static const char *ids[4] = { "O", "A", "B", "C" };
	  int_g np = get_real_space_lattice_labels(positions, na, nb, nc,
						   centre_cell, conventional);
	  if (np > 0)
	    render_rspace->set_lattice_labels(positions, ids, np);
	  else
	    render_rspace->detach_lattice_labels();
	} else
	  render_rspace->detach_lattice_labels();
      }
    } else
      render_rspace->detach_lattice();
  }
  if (nlines == 0)
    return DLV_ERROR;
  else
    return DLV_OK;
}

DLVreturn_type DLV::model_base::redraw_atoms(const render_parent *p,
					     char message[], const int_g mlen)
{
  int_g na;
  int_g nb;
  int_g nc;
  bool centre_cell;
  bool conv_cell;
  real_l tol;
  bool edges;
  p->get_cell_data(na, nb, nc, centre_cell, conv_cell, edges, tol);
  bool draw;
  real_g scale;
  int_g select_type;
  bool use_flags;
  int_g flag_type;
  render_rspace->get_atom_data(draw, scale, select_type, use_flags, flag_type);
  if (draw) {
    bool draw_b;
    real_g overlap;
    bool tubes;
    real_g radius;
    int_g subdiv;
    bool polyhedra;
    render_rspace->get_bond_data(draw_b, polyhedra, overlap,
				 tubes, radius, subdiv);
    if (polyhedra)
      select_type = 3;
    real_g (*coords)[3];
    real_g (*colours)[3];
    real_g *radii;
    int_g natoms1 = 0;
    int_g natoms2 = 0;
    int_g natoms3 = 0;
    int_g nframes = 1;
    int_g sym_select = render_rspace->get_selection_symmetry();
    if (atom_data == 0)
      nframes = 1;
    else {
      if (!atom_data->is_gamma_point()) {
	int_g kc[3];
	atom_data->get_kpoint_cell(kc);
	na = (na - 1) / kc[0] + 1;
	nb = (nb - 1) / kc[1] + 1;
	nc = (nc - 1) / kc[2] + 1;
	na *= kc[0];
	nb *= kc[1];
	nc *= kc[2];
      }
    }
    generate_atom_display_list(na, nb, nc, centre_cell, conv_cell, edges,
			       tol, natoms1, natoms2, natoms3, select_type,
			       sym_select, use_flags, flag_type);
    const std::vector<real_g (*)[3]> *traj = 0;
    if (atom_data != 0) {
      atom_data->update_trajectory(this);
      nframes = atom_data->get_nframes();
      traj = atom_data->get_trajectory();
    }
    if (natoms1 > 0) {
      get_opaque_atoms(coords, colours, radii, scale, natoms1, select_type,
		       use_flags, flag_type, traj, nframes);
      render_rspace->draw_opaque_atoms(coords, colours, radii,
				       natoms1, nframes);
      delete [] radii;
      delete [] colours;
      delete [] coords;
    } else
      render_rspace->detach_opaque_atoms();
    if (natoms2 > 0) {
      get_transparent_atoms(coords, colours, radii, scale, natoms2,
			    select_type, use_flags, flag_type, traj, nframes);
      render_rspace->draw_transparent_atoms(coords, colours, radii,
					    natoms2, nframes);
      delete [] radii;
      delete [] colours;
      delete [] coords;
    } else
      render_rspace->detach_transparent_atoms();
    if (natoms3 > 0) {
      get_edit_op_atoms(coords, colours, radii, scale, natoms3, traj, nframes);
      render_rspace->draw_edit_atoms(coords, colours, radii,
				     natoms3, nframes);
      delete [] radii;
      delete [] colours;
      delete [] coords;
    } else
      render_rspace->detach_edit_atoms();
  } else
    render_rspace->detach_all_atoms();
  return DLV_OK;
}

DLVreturn_type DLV::model_base::update_atoms(const render_parent *p,
					     char message[], const int_g mlen)
{
  bool draw;
  real_g scale;
  int_g select_type;
  bool use_flags;
  int_g flag_type;
  render_rspace->get_atom_data(draw, scale, select_type, use_flags, flag_type);
  if (draw) {
    bool draw_b;
    real_g overlap;
    bool tubes;
    real_g radius;
    int_g subdiv;
    bool polyhedra;
    render_rspace->get_bond_data(draw_b, polyhedra, overlap,
				 tubes, radius, subdiv);
    if (polyhedra)
      select_type = 3;
    real_g (*coords)[3];
    real_g (*colours)[3];
    real_g *radii;
    int_g natoms1 = 0;
    int_g natoms2 = 0;
    int_g natoms3 = 0;
    int_g nframes = 1;
    // selections may have changed
    int_g sym_select = render_rspace->get_selection_symmetry();
    int_g atom_gp = render_rspace->get_atom_group_index();
    string gp = find_atom_group(atom_gp);
    const std::vector<real_g (*)[3]> *traj = 0;
    if (atom_data == 0)
      nframes = 1;
    else {
      nframes = atom_data->get_nframes();
      traj = atom_data->get_trajectory();
      /*if (!atom_data->is_gamma_point()) {
	int_g kc[3];
	atom_data->get_kpoint_cell(kc);
	na = (na - 1) / kc[0] + 1;
	nb = (nb - 1) / kc[1] + 1;
	nc = (nc - 1) / kc[2] + 1;
	na *= kc[0];
	nb *= kc[1];
	nc *= kc[2];
	}*/
    }
    display_atoms.update_selected_atoms(sym_select, gp);
    count_atom_display_list(natoms1, natoms2, natoms3, select_type,
			    use_flags, flag_type);
    if (natoms1 > 0) {
      get_opaque_atoms(coords, colours, radii, scale, natoms1,
		       select_type, use_flags, flag_type, traj, nframes);
      render_rspace->draw_opaque_atoms(coords, colours, radii,
				       natoms1, nframes);
    } else
      render_rspace->detach_opaque_atoms();
    if (natoms2 > 0) {
      get_transparent_atoms(coords, colours, radii, scale, natoms2,
			    select_type, use_flags, flag_type, traj, nframes);
      render_rspace->draw_transparent_atoms(coords, colours, radii,
					    natoms2, nframes);
    } else
      render_rspace->detach_transparent_atoms();
    if (natoms3 > 0) {
      get_edit_op_atoms(coords, colours, radii, scale, natoms3, traj, nframes);
      render_rspace->draw_edit_atoms(coords, colours, radii,
				     natoms3, nframes);
    } else
      render_rspace->detach_edit_atoms();
  } else
    render_rspace->detach_all_atoms();
  // Todo - be smarter as to when we redraw bonds (select_type change etc)
  return update_bonds(p, message, mlen);
}

DLVreturn_type DLV::model_base::update_bonds(const render_parent *p,
					     char message[],
					     const int_g mlen) const
{
  DLVreturn_type ok = DLV_OK;
  int_g na;
  int_g nb;
  int_g nc;
  bool centre_cell;
  bool conv_cell;
  bool edges;
  real_l tol;
  p->get_cell_data(na, nb, nc, centre_cell, conv_cell, edges, tol);
  bool conventional = conv_cell;
  bool draw;
  real_g overlap;
  bool tubes;
  real_g radius;
  int_g subdiv;
  bool polyhedra;
  render_rspace->get_bond_data(draw, polyhedra, overlap,
			       tubes, radius, subdiv);
  if (draw or polyhedra) {
    bool draw;
    real_g scale;
    int_g select_type;
    bool use_flags;
    int_g flag_type;
    render_rspace->get_atom_data(draw, scale, select_type,
				 use_flags, flag_type);
    if (polyhedra)
      select_type = 3;
    int_l nbonds1 = 0;
    int_l nbonds2 = 0;
    int_l nbonds3 = 0;
    int_l nvertices = 0;
    int_g nframes = 1;
    int_g btype;
    bool is_gamma = true;
    const std::vector<real_g (*)[3]> *traj = 0;
    if (atom_data == 0)
      nframes = 1;
    else {
      nframes = atom_data->get_nframes();
      traj = atom_data->get_trajectory();
      is_gamma = atom_data->is_gamma_point();
      if (!atom_data->is_gamma_point()) {
	int_g kc[3];
	atom_data->get_kpoint_cell(kc);
	na = (na - 1) / kc[0] + 1;
	nb = (nb - 1) / kc[1] + 1;
	nc = (nc - 1) / kc[2] + 1;
	na *= kc[0];
	nb *= kc[1];
	nc *= kc[2];
      }
    }
    std::list<bond_list> *data = new std::list<bond_list>[nframes];
    // Todo - use_flags, flag_type?
    generate_bonds(display_atoms, na, nb, nc, conventional, data,
		   overlap, centre_cell, (select_type == 0), bonds, tol,
		   traj, nframes, is_gamma, use_edit_flags);
    int_g maxb1 = 0;
    int_g maxb2 = 0;
    int_g maxb3 = 0;
    int_g maxverts = 0;
    for (int_g i = 0; i < nframes; i++) {
      count_bond_list(data[i], nbonds1, nbonds2, nbonds3,
		      nvertices, polyhedra);
      if (maxb1 < nbonds1)
	maxb1 = nbonds1;
      if (maxb2 < nbonds2)
	maxb2 = nbonds2;
      if (maxb3 < nbonds3)
	maxb3 = nbonds3;
      if (maxverts < nvertices)
	maxverts = nvertices;
    }
    if (select_type == 2) {
      btype = maxb1;
      maxb1 = maxb2;
      maxb2 = btype;
    }
    if (maxb1 == 0)
      render_rspace->detach_opaque_bonds();
    if (maxb2 == 0)
      render_rspace->detach_transparent_bonds();
    if (maxb3 == 0)
      render_rspace->detach_edit_bonds();
    if (maxverts == 0)
      render_rspace->detach_polyhedra();
    for (int_g i = 0; i < nframes; i++) {
      count_bond_list(data[i], nbonds1, nbonds2, nbonds3,
		      nvertices, polyhedra);
      if (select_type == 2) {
	btype = nbonds1;
	nbonds1 = nbonds2;
	nbonds2 = btype;
      }
      if (maxb1 > 0) {
	int_l *connects;
	int_l nconnects = 0;
	real_g (*bond_pos)[3];
	real_g (*bond_rgb)[3];
	btype = 1;
	if (select_type == 2)
	  btype = 2;
	int_l nbonds = 0;
	if (tubes)
	  generate_tube_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype, radius, subdiv);
	else
	  generate_line_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype);
	render_rspace->draw_opaque_bonds(bond_pos, bond_rgb, nbonds, connects,
					 nconnects, nframes, tubes, i);
	delete [] connects;
	delete [] bond_rgb;
	delete [] bond_pos;
      }
      if (maxb2 > 0) {
	int_l *connects;
	int_l nconnects = 0;
	real_g (*bond_pos)[3];
	real_g (*bond_rgb)[3];
	int_l nbonds = 0;
	btype = 2;
	if (select_type == 2)
	  btype = 1;
	if (tubes)
	  generate_tube_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype, radius, subdiv);
	else
	  generate_line_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype);
	render_rspace->draw_transparent_bonds(bond_pos, bond_rgb, nbonds,
					      connects, nconnects, nframes,
					      tubes, i);
	delete [] connects;
	delete [] bond_rgb;
	delete [] bond_pos;
      }
      if (maxb3 > 0) {
	int_l *connects;
	int_l nconnects = 0;
	real_g (*bond_pos)[3];
	real_g (*bond_rgb)[3];
	int_l nbonds = 0;
	btype = 3;
	if (tubes)
	  generate_tube_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype, radius, subdiv);
	else
	  generate_line_bonds(data[i], bond_pos, bond_rgb, nbonds, connects,
			      nconnects, btype);
	render_rspace->draw_edit_bonds(bond_pos, bond_rgb, nbonds, connects,
				       nconnects, nframes, tubes, i);
	delete [] connects;
	delete [] bond_rgb;
	delete [] bond_pos;
      }
      bool missing = false;
      if (maxverts > 0) {
	int_l *connects;
	int_l nconnects = 0;
	real_g (*vpos)[3];
	real_g (*vrgb)[3];
	int_l nverts = 0;
	if (!generate_polyhedra(data[i], vpos, vrgb, nverts,
				connects, nconnects))
	  missing = true;
	render_rspace->draw_polyhedra(vpos, vrgb, nverts, connects,
				      nconnects, nframes, i);
	delete [] connects;
	delete [] vrgb;
	delete [] vpos;
      } else if (polyhedra and display_atoms.get_number_selected_atoms() > 0)
	missing = true;
      if (missing) {
	ok = DLV_WARNING;
	strncpy(message,
		"Some atoms had no bonds from which to draw polyhedra",
		mlen);
      }
    }
    delete [] data;
  } else {
    render_rspace->detach_all_bonds();
    render_rspace->detach_polyhedra();
  }
  return ok;
}

DLVreturn_type DLV::model_base::add_bond(render_parent *p,
					 char message[], const int_g mlen)
{
  atom atom1;
  atom atom2;
  bool ok = display_atoms.get_selected_pair(atom1, atom2);
  if (ok) {
    if (bonds.set_pair(atom1, atom2, true, atom_basis))
      return update_bonds(p, message, mlen);
    return DLV_OK;
  } else {
    strncpy(message, "Two atoms must be selected to define a bond", mlen);
    return DLV_WARNING;
  }
}

DLVreturn_type DLV::model_base::del_bond(render_parent *p,
					 char message[], const int_g mlen)
{
  atom atom1;
  atom atom2;
  bool ok = display_atoms.get_selected_pair(atom1, atom2);
  if (ok) {
    if (bonds.clear_pair(atom1, atom2, atom_basis))
      return update_bonds(p, message, mlen);
    return DLV_OK;
  } else {
    strncpy(message, "Two atoms must be selected to define a bond", mlen);
    return DLV_WARNING;
  }
}

DLVreturn_type DLV::model_base::add_bond_type(render_parent *p,
					      char message[], const int_g mlen)
{
  atom atom1;
  atom atom2;
  bool ok = display_atoms.get_selected_pair(atom1, atom2);
  if (ok) {
    if (bonds.set_pair_type(atom1, atom2, atom_basis, true))
      return update_bonds(p, message, mlen);
    return DLV_OK;
  } else {
    strncpy(message, "Two atoms must be selected to define a bond", mlen);
    return DLV_WARNING;
  }
}

DLVreturn_type DLV::model_base::del_bond_type(render_parent *p,
					      char message[], const int_g mlen)
{
  atom atom1;
  atom atom2;
  bool ok = display_atoms.get_selected_pair(atom1, atom2);
  if (ok) {
    if (bonds.clear_pair_type(atom1, atom2, atom_basis))
      return update_bonds(p, message, mlen);
    return DLV_OK;
  } else {
    strncpy(message, "Two atoms must be selected to define a bond", mlen);
    return DLV_WARNING;
  }
}

DLVreturn_type DLV::model_base::add_bond_all(render_parent *p,
					     char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (bonds.set_all(true, atom_basis))
    ok = update_bonds(p, message, mlen);
  return ok;
}

DLVreturn_type DLV::model_base::del_bond_all(render_parent *p,
					     char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (bonds.clear_all())
    ok = update_bonds(p, message, mlen);
  return ok;
}

DLVreturn_type DLV::model_base::select_atom(render_parent *p,
					    const real_g coords[3],
					    const int_g status, int_g &nselects,
					    char message[], const int_g mlen)
{
  nselects = 0;
  static real_g origin[3];
  message[0] = '\0';
  const real_g offset = 0.001f;
  int_g sym_select = render_rspace->get_selection_symmetry();
  if (status == 1) {
    origin[0] = coords[0];
    origin[1] = coords[1];
    origin[2] = coords[2];
    display_atoms.select(origin, offset, true, sym_select, false,
			 message, mlen);
  } else {
    real_g distance = (coords[0] - origin[0]) * (coords[0] - origin[0])
      + (coords[1] - origin[1]) * (coords[1] - origin[1])
      + (coords[2] - origin[2]) * (coords[2] - origin[2]);
    if (distance < offset)
      distance = offset;
    display_atoms.select(origin, distance, (status == 2), sym_select, false,
			 message, mlen);
  }
  if (strlen(message) > 0)
    return DLV_ERROR;
  else {
    DLVreturn_type ok = DLV_OK;
    if (status != 1) {
      ok = update_atoms(p, message, mlen);
      nselects = display_atoms.get_number_selected_atoms();
    //if (ok == DLV_OK) {
      // Todo - if we kept bonds then no need to recalulate list, just draw.
      //ok = update_bonds(p, message, mlen);
    //}
    }
    return ok;
  }
}

DLVreturn_type DLV::model_base::deselect_all_atoms(render_parent *p,
						   char message[],
						   const int_g mlen)
{
  message[0] = '\0';
  display_atoms.deselect(message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  else {
    DLVreturn_type ok = update_atoms(p, message, mlen);
    //if (ok == DLV_OK)
      // Todo - if we kept bonds then no need to recalulate list, just draw.
      //ok = update_bonds(p, message, mlen);
    return ok;
  }
}

void DLV::model_base::get_displayed_cell(render_parent *p, int_g &na,
					 int_g &nb, int_g &nc, bool &conv,
					 bool &centre, bool &edges) const
{
  if (p != 0) {
    bool centre;
    real_l tol;
    p->get_cell_data(na, nb, nc, centre, conv, edges, tol);
  } else {
    na = 1;
    nb = 1;
    nc = 1;
    conv = false;
    centre = false;
  }
}

void DLV::model_base::set_edit_transform(const render_parent *parent,
					 const bool v, real_g m[4][4],
					 real_g t[3], real_g c[3],
					 char message[], const int_g mlen)
{
  if (parent != 0 and render_rspace != 0) {
    use_edit_flags = v;
    render_rspace->attach_editor(v, parent->get_redit(), m, t, c,
				 message, mlen);
  }
}   

void DLV::model_base::deactivate_atom_flags()
{
  render_rspace->deactivate_atom_flags();
  std::map<int, atom>::iterator p;
  for (p = atom_basis.begin(); p != atom_basis.end(); ++p)
    p->second->clear_flag();
}

void DLV::model_base::activate_atom_flags(const string label, 
					  const bool reset_flags)
{
  render_rspace->activate_atom_flags(label);
  if (reset_flags){
    std::map<int, atom>::iterator p;
    for (p = atom_basis.begin(); p != atom_basis.end(); ++p)
      p->second->clear_flag();
  }
}

void DLV::model_base::toggle_atom_flags(const string label, 
					const bool reset_flags)
{
  render_rspace->toggle_atom_flags(label);
  if (reset_flags){
    std::map<int, atom>::iterator p;
    for (p = atom_basis.begin(); p != atom_basis.end(); ++p)
      p->second->clear_flag();
  }
}

void DLV::model_base::set_atom_flag(const int_g index, 
				    const atom_flag_type aft)
{
    std::map<int, atom>::iterator p;
//    DLV::atom_flag_type flag = flag_done;
    int_g i = 0;
    //clb to do - speed up
    for (p = atom_basis.begin(); p != atom_basis.end(); ++p){
	if(i==index) p->second->set_atom_flag(aft);
	i++;
    }
}

bool DLV::model_base::set_atom_index_and_flags(int_g indices[],
					       const int_g value,
					       const bool set_def,
					       char message[], const int_g mlen)
{
  return display_atoms.set_selected_index_and_flags(indices, value, set_def,
						    atom_basis, message, mlen);
}

void DLV::model_base::set_selected_atom_flags(const bool done)
{
  display_atoms.set_selected_atom_flags(done);
}

bool DLV::model_base::map_atom_selections(int_g &n, int_g * &labels,
					  int_g * &atom_types, atom * &parents,
					  const bool all_atoms,
					  const real_g grid[][3],
					  int_g **const data, const int_g ndata,
					  const int_g ngrid) const
{
  return map_atom_selections(display_atoms, primitive_atoms, grid, data, ndata,
			     ngrid, labels, atom_types, parents, n, all_atoms);
}

bool DLV::model_base::map_selected_atoms(int_g &n, int_g * &labels,
					 int_g (* &shifts)[3],
					 const real_g grid[][3],
					 int_g **const data, const int_g ndata,
					 const int_g ngrid) const
{
  return map_selected_atoms(display_atoms, n, labels, shifts,
			    grid, data, ndata, ngrid);
}

void DLV::model_base::set_atom_data(const render_parent *p,
				    class data_object *d, const bool undisplay)
{
  // might be too many refreshes here?
  if (atom_data != 0 and undisplay)
    atom_data->undisplay();
  bool changed = (atom_data != d);
  atom_data = d;
  if (changed)
    refresh_atom_data(p);
  if (d == 0)
    render_parent::stop_animation();
  else
    render_parent::show_animate_panel();
}

void DLV::model_base::refresh_atom_data(const render_parent *p)
{
  //if (atom_data != 0) {
    char message[256];
    (void) redraw_atoms(p, message, 256);
    (void) update_bonds(p, message, 256);
    //}
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::model_base::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<model>(*this);
  ar & atom_basis;
  ar & primitive_atoms;
  ar & bonds;
  ar & group_names;
#ifdef ENABLE_DLV_GRAPHICS
  ar & render_rspace;
  ar & render_kspace;
  ar & atom_data;
  atom_data = 0; // Todo - don't load/save atom_data and avoid split?
#endif // ENABLE_DLV_GRAPHICS
  ar & use_edit_flags;
}

template <class Archive>
void DLV::model_base::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<model>(*this);
  ar & atom_basis;
  ar & primitive_atoms;
  ar & bonds;
  ar & group_names;
#ifdef ENABLE_DLV_GRAPHICS
  ar & render_rspace;
  ar & render_kspace;
  ar & atom_data;
#endif // ENABLE_DLV_GRAPHICS
  ar & use_edit_flags;
}

template <class sym> template <class Archive>
void DLV::model_impl<sym>::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<model_base>(*this);
  ar & symmetry;
}

template <class sym> template <class Archive>
void DLV::periodic_model<sym>::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<model_impl<sym> >(*this);
  ar & primitive_lattice;
  ar & param_a;
  ar & param_b;
  ar & param_c;
  ar & param_alpha;
  ar & param_beta;
  ar & param_gamma;
  ar & valid_vectors;
  ar & valid_parameters;
}

//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_impl<DLV::point_symmetry>)
//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::periodic_model<DLV::line_symmetry>)
//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::periodic_model<DLV::plane_symmetry>)
//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::periodic_model<DLV::volume_symmetry>)

DLV_SUPPRESS_TEMPLATES(DLV::symmetry_data)
DLV_SUPPRESS_TEMPLATES(DLV::point_symmetry)
DLV_SUPPRESS_TEMPLATES(DLV::line_symmetry)
DLV_SUPPRESS_TEMPLATES(DLV::plane_symmetry)
DLV_SUPPRESS_TEMPLATES(DLV::volume_symmetry)
#ifdef ENABLE_DLV_GRAPHICS
DLV_SUPPRESS_TEMPLATES(DLV::model_display_r)
DLV_SUPPRESS_TEMPLATES(DLV::model_display_k)
#endif // ENABLE_DLV_GRAPHICS

DLV_NORMAL_EXPLICIT(DLV::model_impl<DLV::point_symmetry>)
DLV_EXPORT_EXPLICIT(DLV::model_impl<DLV::point_symmetry>)
DLV_NORMAL_EXPLICIT(DLV::periodic_model<DLV::line_symmetry>)
DLV_EXPORT_EXPLICIT(DLV::periodic_model<DLV::line_symmetry>)
DLV_NORMAL_EXPLICIT(DLV::periodic_model<DLV::plane_symmetry>)
DLV_EXPORT_EXPLICIT(DLV::periodic_model<DLV::plane_symmetry>)
DLV_NORMAL_EXPLICIT(DLV::periodic_model<DLV::volume_symmetry>)
DLV_EXPORT_EXPLICIT(DLV::periodic_model<DLV::volume_symmetry>)

#endif // DLV_USES_SERIALIZE
