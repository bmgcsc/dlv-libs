
#ifndef DLV_OP_ADMIN
#define DLV_OP_ADMIN

namespace DLV {

  class subproject : public operation {
  public:
    static operation *create(const char label[], const char dir[],
			     char message[], const int_g mlen);
    // public for serialize
    subproject(const char label[], const char dir[]);

    bool is_subproject() const;
    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

    void add_path(string &dir) const;

  private:
    string name;
    string directory;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // An operation with no side effects - e.g. just writing a file.
  class empty_operation : public operation {
  protected:
    empty_operation();
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<operation>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  // Do we really need the ones derived from this? Todo
  class save_file_op : public empty_operation {
  public:
    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    save_file_op(const char file[]);

    string get_filename() const;

  private:
    string filename;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class save_input_op : public save_file_op {
  protected:
    save_input_op(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class save_model_op : public save_file_op {
  public:
    // public for serialization
    save_model_op(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_data_op : public load_op {
  public:
    bool is_data_load() const;

    // public for serialization
    load_data_op(const char file[]);

  protected:
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
  BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::save_model_op)
  BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::load_data_op)
  BOOST_CLASS_EXPORT_KEY(DLV::subproject)
  BOOST_CLASS_EXPORT_KEY(DLV::empty_operation)
  BOOST_CLASS_EXPORT_KEY(DLV::save_file_op)
  BOOST_CLASS_EXPORT_KEY(DLV::save_input_op)
  BOOST_CLASS_EXPORT_KEY(DLV::save_model_op)
  BOOST_CLASS_EXPORT_KEY(DLV::load_data_op)
#endif // DLV_USES_SERIALIZE

inline DLV::subproject::subproject(const char label[], const char dir[])
  : name(label), directory(dir)
{
}

inline DLV::empty_operation::empty_operation()
{
}

inline DLV::save_file_op::save_file_op(const char file[]) : filename(file)
{
}

inline DLV::save_input_op::save_input_op(const char file[])
  : save_file_op(file)
{
}

inline DLV::save_model_op::save_model_op(const char file[])
  : save_file_op(file)
{
}

inline DLV::load_data_op::load_data_op(const char file[]) : load_op(file)
{
}

inline DLV::string DLV::save_file_op::get_filename() const
{
  return filename;
}

#endif // DLV_OP_ADMIN
