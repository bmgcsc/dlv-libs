
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "math_fns.hxx"
#include "atom_model.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "op_model.hxx"
#include "op_atoms.hxx"
#include "data_objs.hxx"
#include "extern_model.hxx"

DLVreturn_type DLV::supercell::create(const char name[], int_g &na, int_g &nb,
				      int_g &nc, bool &conv, char message[],
				      const int_g mlen)
{
  na = 1;
  nb = 1;
  nc = 1;
  conv = false;
  const model *parent = get_current_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    bool edges = true;
#ifdef ENABLE_DLV_GRAPHICS
    bool centre;
    parent->get_displayed_cell(get_current()->get_display_obj(),
			       na, nb, nc, conv, centre, edges);
#endif // ENABLE_DLV_GRAPHICS
    supercell *op = new supercell(m, na, nb, nc, conv);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create supercell", mlen);
      ok = DLV_ERROR;
    } else {
      int_g matrix[3][3];
      matrix[0][0] = na;
      matrix[0][1] = 0;
      matrix[0][2] = 0;
      matrix[1][0] = 0;
      matrix[1][1] = nb;
      matrix[1][2] = 0;
      matrix[2][0] = 0;
      matrix[2][1] = 0;
      matrix[2][2] = nc;
      if ((ok = op->update_supercell(parent, matrix, conv,
				     message, mlen)) == DLV_OK) {
	op->support_create();
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::supercell::update_supercell(const model *parent,
						const int_g m[3][3],
						const bool conv, char message[],
						const int_g mlen)
{
  const real_l tol = 0.0001;
  // check its not singular
  real_l mat[3][3];
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      mat[i][j] = (real_l) m[i][j];
  if (abs(determinant(mat)) < tol) {
    strncpy(message, "Ignoring singular supercell matrix", mlen);
    return DLV_WARNING;
  } else {
    bool redraw = get_model()->supercell(parent, m, conv);
    for (int_g i = 0; i < 3; i++)
      for (int_g j = 0; j < 3; j++)
	matrix[i][j] = m[i][j];
    conventional_cell = conv;
    if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
      (void) update_cell_info(message, mlen);
      return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return DLV_OK;
}

DLV::string DLV::supercell::get_name() const
{
  return (get_model_name() + " - supercell");
}

bool DLV::supercell::is_supercell() const
{
  return true;
}

bool DLV::supercell::copy_cell_replicate() const
{
  return false;
}

void DLV::supercell::get_supercell(int_g supercell[2][2]) const
{
  supercell[0][0] = matrix[0][0];
  supercell[0][1] = matrix[0][1];
  supercell[1][0] = matrix[1][0];
  supercell[1][1] = matrix[1][1];
}

DLVreturn_type DLV::del_symmetry::create(const char name[], char message[],
					 const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    del_symmetry *op = new del_symmetry(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to delete symmetry", mlen);
      ok = DLV_ERROR;
    } else {
      (void) m->delete_symmetry(parent);
      op->support_create();
      op->add_standard_data_objects();
    }
    return ok;
  }
}

DLV::string DLV::del_symmetry::get_name() const
{
  return (get_model_name() + " - del symmetry");
}

DLVreturn_type DLV::molecule_view::create(const char name[], char message[],
					  const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, 0);
  if (m == 0)
    return DLV_ERROR;
  else {
    int_g na = 1;
    int_g nb = 1;
    int_g nc = 1;
    bool conv = false;
    bool centred = false;
    bool edges = true;
#ifdef ENABLE_DLV_GRAPHICS
    parent->get_displayed_cell(get_current()->get_display_obj(),
			       na, nb, nc, conv, centred, edges);
#endif // ENABLE_DLV_GRAPHICS
    molecule_view *op = new molecule_view(m, na, nb, nc, conv, centred, edges);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create molecule", mlen);
      ok = DLV_ERROR;
    } else {
      (void) m->view_to_molecule(parent);
      op->support_create();
      op->add_standard_data_objects();
    }
    return ok;
  }
}

DLVreturn_type DLV::remove_lattice::create(const char name[], const int_g na,
					   const int_g nb, const int_g nc,
					   const bool conv, const bool centred,
					   const bool edges, char message[],
					   const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, 0);
  if (m == 0)
    return DLV_ERROR;
  else {
    remove_lattice *op = new remove_lattice(m, na, nb, nc, conv, centred, edges);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create molecule", mlen);
      ok = DLV_ERROR;
    } else {
      (void) m->cell_to_molecule(parent, na, nb, nc, conv, centred, edges);
      op->support_create();
      op->add_standard_data_objects();
    }
    return ok;
  }
}

DLV::string DLV::molecule_view::get_name() const
{
  return (get_model_name() + " - to molecule");
}

DLV::string DLV::remove_lattice::get_name() const
{
  return (get_model_name() + " - to molecule");
}

DLVreturn_type DLV::alter_lattice::create(const char name[], real_l values[6],
					  bool usage[6], char message[],
					  const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    coord_type a;
    coord_type b;
    coord_type c;
    coord_type alpha;
    coord_type beta;
    coord_type gamma;
    parent->get_lattice_parameters(a, b, c, alpha, beta, gamma);
    alter_lattice *op = new alter_lattice(m, a, b, c, alpha, beta, gamma);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create lattice edit", mlen);
      ok = DLV_ERROR;
    } else {
      m->copy_model(parent);
      op->support_create();
      //is_pending = true;
      //pending = current;
      //editing = op;
      //current = op;
      op->add_standard_data_objects();
      values[0] = a;
      values[1] = b;
      values[2] = c;
      values[3] = alpha;
      values[4] = beta;
      values[5] = gamma;
      m->use_which_lattice_parameters(usage);
    }
    return ok;
  }
}

DLVreturn_type DLV::alter_lattice::update_lattice(const model *parent,
						  const real_l values[6],
						  char message[],
						  const int_g mlen)
{
  bool redraw = get_model()->update_lattice_parameters(parent, values);
  a = values[0];
  b = values[1];
  c = values[2];
  alpha = values[3];
  beta = values[4];
  gamma = values[5];
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::alter_lattice::get_name() const
{
  return (get_model_name() + " - edit lattice");
}

DLVreturn_type DLV::origin_shift::create(const char name[], char message[],
					 const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    origin_shift *op = new origin_shift(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create origin shift", mlen);
      ok = DLV_ERROR;
    } else {
      m->copy_model(parent);
      op->support_create();
      //is_pending = true;
      //pending = current;
      //editing = op;
      //current = op;
      op->add_standard_data_objects();
    }
    return ok;
  }
}

DLVreturn_type DLV::origin_shift::update_origin(const model *parent,
						const real_l x, const real_l y,
						const real_l z,
						const bool frac, char message[],
						const int_g mlen)
{
  bool redraw = get_model()->update_origin(parent, x, y, z, frac);
  sx = x;
  sy = y;
  sz = z;
  fractional = frac;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::origin_shift::update_origin_symm(const model *parent,
						     real_l &x, real_l &y,
						     real_l &z,
						     const bool frac,
						     char message[],
						     const int_g mlen)
{
  bool redraw = get_model()->update_origin_symm(parent, x, y, z, frac);
  sx = x;
  sy = y;
  sz = z;
  fractional = frac;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::origin_shift::get_name() const
{
  return (get_model_name() + " - shift origin");
}

DLVreturn_type DLV::vacuum_cell::create(const char name[], const real_l c,
					const real_l origin, char message[],
					const int_g mlen)
{
  const model *parent = get_current_model();
  model *m = create_atoms(name, 3);
  if (m == 0)
    return DLV_ERROR;
  else {
    vacuum_cell *op = new vacuum_cell(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create vacuum gap", mlen);
      ok = DLV_ERROR;
    } else {
      if ((ok = op->update_vacuum(parent, c, origin,
				  message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::vacuum_cell::update_vacuum(const model *parent,
					       const real_l c,
					       const real_l origin,
					       char message[], const int_g mlen)
{
  bool redraw = get_model()->update_vacuum(parent, c, origin);
  c_axis = c;
  slab_origin = origin;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::vacuum_cell::get_name() const
{
  return (get_model_name() + " - slab to 3D");
}

DLVreturn_type DLV::cluster::create(const char name[], int_g &n,
				    char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 0);
  if (m == 0)
    return DLV_ERROR;
  else {
    n = 1;
    cluster *op = new cluster(m, 1);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create cluster", mlen);
      ok = DLV_ERROR;
    } else {
      if ((ok = op->update_cluster(parent, 1, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::cluster::create(const char name[], int_g &n, const real_l x,
				    const real_l y, const real_l z,
				    char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 0);
  if (m == 0)
    return DLV_ERROR;
  else {
    n = 1;
    cluster *op = new cluster(m, 1);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create cluster", mlen);
      ok = DLV_ERROR;
    } else {
      if ((ok = op->update_cluster(parent, 1, x, y, z,
				   message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::cluster::update_cluster(model *parent, const int_g n,
					    char message[], const int_g mlen)
{
  bool redraw = get_model()->update_cluster(parent, n);
  neighbours = n;
  radius = 0.0;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::cluster::update_cluster(model *parent, const int_g n,
					    const real_l x, const real_l y,
					    const real_l z, char message[],
					    const int_g mlen)
{
  bool redraw = get_model()->update_cluster(parent, n, x, y, z);
  neighbours = n;
  radius = 0.0;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::cluster::update_cluster(model *parent,
					    const real_l r,
					    char message[], const int_g mlen)
{
  bool redraw = get_model()->update_cluster(parent, r);
  neighbours = 0;
  radius = r;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::cluster::get_name() const
{
  return (get_model_name() + " - cut cluster");
}

bool DLV::cluster::get_lattice_rotation(real_l r[3][3]) const
{
  // I don't think any rotation happens
  r[0][0] = 1.0;
  r[0][1] = 0.0;
  r[0][2] = 0.0;
  r[1][0] = 0.0;
  r[1][1] = 1.0;
  r[1][2] = 0.0;
  r[2][0] = 0.0;
  r[2][1] = 0.0;
  r[2][2] = 1.0;
  return true;
}

DLVreturn_type DLV::slab_from_bulk::create(const char name[], int_g &nlayers,
					   string * &names, int_g &n,
					   char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 2);
  if (m == 0)
    return DLV_ERROR;
  else {
    slab_from_bulk *op = new slab_from_bulk(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create slab", mlen);
      ok = DLV_ERROR;
    } else {
      names = 0;
      n = 0;
      nlayers = 1;
      if ((ok = op->update_slab(parent, 0, 0, 1, true, 0.001, nlayers,
				names, n, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::slab_from_bulk::create(const char name[], const int_g h,
					   const int_g k, const int_g l,
					   const bool conventional,
					   const real_g tol,int_g &nlayers,
					   string * &names, int_g &n,
					   char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 2);
  if (m == 0)
    return DLV_ERROR;
  else {
    slab_from_bulk *op = new slab_from_bulk(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create slab", mlen);
      ok = DLV_ERROR;
    } else {
      names = 0;
      n = 0;
      nlayers = 1;
      if ((ok = op->update_slab(parent, h, k, l, conventional, tol, nlayers,
				names, n, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::slab_from_bulk::update_slab(model *parent, const int_g h,
						const int_g k, const int_g l,
						const bool conventional,
						const real_g tol,
						int_g &nlayers,
						string * &labels, int_g &n,
						char message[],
						const int_g mlen)
{
  if (crystal3d == 0)
    crystal3d = create_atoms("internal", 3);
  bool redraw = get_model()->update_slab(parent, crystal3d, h, k, l,
					 conventional, tol, nlayers,
					 labels, n, r);
  miller_h = h;
  miller_k = k;
  miller_l = l;
  conventional_cell = conventional;
  layer_tolerance = tol;
  termination = 0;
  number_of_layers = nlayers;
  hkl_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::slab_from_bulk::update_slab(model *parent, const real_g tol,
						int_g &nlayers,
						string * &labels,
						int_g &n, char message[],
						const int_g mlen)
{
  bool redraw = get_model()->update_slab(parent, crystal3d, miller_h, miller_k,
					 miller_l, conventional_cell, tol,
					 nlayers, labels, n, r);
  layer_tolerance = tol;
  hkl_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type
DLV::slab_from_bulk::update_slab_term(model *parent, const int_g term,
				      char message[], const int_g mlen)
{
  // trap for termination outside range of terminations
  int_g t = term % hkl_layers;
  if (t < 0)
    t += hkl_layers;
  bool redraw = get_model()->update_slab(parent, crystal3d, layer_tolerance,
					 t, number_of_layers);
  termination = t;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type
DLV::slab_from_bulk::update_slab(model *parent, const int_g nlayers,
				 char message[], const int_g mlen)
{
  if (nlayers < 1) {
    strncpy(message, "Must be at least 1 slab layer", mlen);
    return DLV_ERROR;
  } else {
    bool redraw = get_model()->update_slab(parent, crystal3d, layer_tolerance,
					   termination, nlayers);
    number_of_layers = nlayers;
    if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
      (void) update_cell_info(message, mlen);
      // should really be redraw_atoms
      return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
    }
    return DLV_OK;
  }
}

DLV::string DLV::slab_from_bulk::get_name() const
{
  return (get_model_name() + " - cut slab");
}

bool DLV::slab_from_bulk::get_lattice_rotation(real_l r[3][3]) const
{
  r[0][0] = this->r[0][0];
  r[0][1] = this->r[0][1];
  r[0][2] = this->r[0][2];
  r[1][0] = this->r[1][0];
  r[1][1] = this->r[1][1];
  r[1][2] = this->r[1][2];
  r[2][0] = this->r[2][0];
  r[2][1] = this->r[2][1];
  r[2][2] = this->r[2][2];
  return true;
}

DLVreturn_type DLV::slab_from_surface::create(const char name[], int_g &nlayers,
					      string * &names, int_g &n,
					      char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 2);
  if (m == 0)
    return DLV_ERROR;
  else {
    slab_from_surface *op = new slab_from_surface(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create slab", mlen);
      ok = DLV_ERROR;
    } else {
      names = 0;
      n = 0;
      nlayers = 1;
      if ((ok = op->update_slab(parent, 0, 0, 1, true, 0.001, nlayers,
				names, n, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type
DLV::slab_from_surface::update_slab(model *parent, const int_g h, const int_g,
				    const int_g l, const bool conventional,
				    const real_g tol, int_g &nlayers,
				    string * &labels, int_g &n,
				    char message[], const int_g mlen)
{
  bool redraw = get_model()->surface_slab(parent, tol, nlayers, labels, n);
  layer_tolerance = tol;
  number_of_layers = nlayers;
  hkl_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type
DLV::slab_from_surface::update_slab(model *parent, const real_g tol,
				    int_g &nlayers, string * &labels, int_g &n,
				    char message[], const int_g mlen)
{
  bool redraw = get_model()->surface_slab(parent, tol, nlayers, labels, n);
  layer_tolerance = tol;
  hkl_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type
DLV::slab_from_surface::update_slab(model *parent, const int_g nlayers,
				    char message[], const int_g mlen)
{
  bool redraw = get_model()->surface_slab(parent, layer_tolerance, nlayers);
  number_of_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::slab_from_surface::get_name() const
{
  return (get_model_name() + " - cut slab");
}

DLVreturn_type DLV::surface_edit::create(const char name[], string * &names,
					 int_g &n, char message[],
					 const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 4);
  if (m == 0)
    return DLV_ERROR;
  else {
    surface_edit *op = new surface_edit(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create surface", mlen);
      ok = DLV_ERROR;
    } else {
      names = 0;
      n = 0;
      if ((ok = op->update_surface(parent, 0, 0, 1, true, 0.001,
				   names, n, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::surface_edit::create(const char name[], const int_g h,
					 const int_g k, const int_g l,
					 const bool conventional,
					 const real_g tol, string * &names,
					 int_g &n, char message[],
					 const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 4);
  if (m == 0)
    return DLV_ERROR;
  else {
    surface_edit *op = new surface_edit(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create surface", mlen);
      ok = DLV_ERROR;
    } else {
      names = 0;
      n = 0;
      if ((ok = op->update_surface(parent, h, k, l, conventional, tol,
				   names, n, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::surface_edit::update_surface(model *parent, const int_g h,
						 const int_g k, const int_g l,
						 const bool conventional,
						 const real_g tol,
						 string * &labels, int_g &n,
						 char message[],
						 const int_g mlen)
{
  if (crystal3d == 0)
    crystal3d = create_atoms("internal", 3);
  bool redraw = get_model()->update_surface(parent, crystal3d, h, k, l,
					    conventional, tol, labels, n, r);
  miller_h = h;
  miller_k = k;
  miller_l = l;
  conventional_cell = conventional;
  layer_tolerance = tol;
  termination = 0;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::surface_edit::update_surface(model *parent,
						 const real_g tol,
						 string * &labels, int_g &n,
						 char message[],
						 const int_g mlen)
{
  bool redraw = get_model()->update_surface(parent, crystal3d, miller_h,
					    miller_k, miller_l,
					    conventional_cell, tol,
					    labels, n, r);
  layer_tolerance = tol;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::surface_edit::update_surface(model *parent,
						 const int_g term,
						 char message[],
						 const int_g mlen)
{
  bool redraw = get_model()->update_surface(crystal3d, layer_tolerance, term);
  termination = term;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::surface_edit::get_name() const
{
  return (get_model_name() + " - cut surface");
}

bool DLV::surface_edit::get_lattice_rotation(real_l r[3][3]) const
{
  r[0][0] = this->r[0][0];
  r[0][1] = this->r[0][1];
  r[0][2] = this->r[0][2];
  r[1][0] = this->r[1][0];
  r[1][1] = this->r[1][1];
  r[1][2] = this->r[1][2];
  r[2][0] = this->r[2][0];
  r[2][1] = this->r[2][1];
  r[2][2] = this->r[2][2];
  return true;
}

DLVreturn_type DLV::salvage::create(const char name[], int_g &nlayers,
				    int_g &min_layers, char message[],
				    const int_g mlen)
{
  model *m = get_current_model()->duplicate_model(name);
  if (m == 0)
    return DLV_ERROR;
  else {
    salvage *op = new salvage(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create salvage", mlen);
      ok = DLV_ERROR;
    } else {
      nlayers = 0;
      min_layers = 0;
      op->support_create();
      //is_pending = true;
      //pending = current;
      //editing = op;
      //current = op;
      op->add_standard_data_objects();
    }
    return ok;
  }
}

DLVreturn_type DLV::salvage::update_salvage(model *parent, const real_g tol,
					    char message[], const int_g mlen)
{
  bool redraw = get_model()->update_salvage(parent, tol, number_of_layers);
  layer_tolerance = tol;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    get_model()->list_atom_groupings(get_display_obj());
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLVreturn_type DLV::salvage::update_salvage(model *parent, const int_g nlayers,
					    char message[], const int_g mlen)
{
  bool redraw = get_model()->update_salvage(parent, layer_tolerance, nlayers);
  number_of_layers = nlayers;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    get_model()->list_atom_groupings(get_display_obj());
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::salvage::get_name() const
{
  return (get_model_name() + " - create salvage");
}

DLVreturn_type DLV::wulff::create(const char name[], const real_l r,
				  const int selection, char message[],
				  const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, 0);
  data_object *data = find_wulff(selection);
  if (m == 0 or data == 0)
    return DLV_ERROR;
  else {
    wulff *op = new wulff(m, r, data);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create wulff nanocrystal", mlen);
      ok = DLV_ERROR;
    } else {
      if ((ok = op->wulff_update(parent, r, message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::wulff::wulff_update(model *parent, const real_l r,
					char message[], const int_g mlen)
{
  bool redraw = get_model()->wulff(parent, r, crystal);
  scale = r;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::wulff::get_name() const
{
  return (get_model_name() + " - wulff crystal");
}

bool DLV::wulff::get_lattice_rotation(real_l r[3][3]) const
{
  // I don't think any rotation happens
  r[0][0] = 1.0;
  r[0][1] = 0.0;
  r[0][2] = 0.0;
  r[1][0] = 0.0;
  r[1][1] = 1.0;
  r[1][2] = 0.0;
  r[2][0] = 0.0;
  r[2][1] = 0.0;
  r[2][2] = 1.0;
  return true;
}

DLVreturn_type DLV::multi_layer::create(const char name[], const real_l r,
					const int selection, char message[],
					const int_g mlen)
{
  const operation *ins = select_model(selection, 2, message, mlen);
  if (ins == 0)
    return DLV_ERROR;
  else {
    model *parent = get_current_model();
    model *m = create_atoms(name, 2);
    if (m == 0)
      return DLV_ERROR;
    else {
      multi_layer *op = new multi_layer(m, r, ins->get_model());
      DLVreturn_type ok = DLV_OK;
      if (op == 0) {
	delete m;
	strncpy(message, "Failed to create multi_layer", mlen);
	ok = DLV_ERROR;
      } else {
	if ((ok = op->multilayer_update(parent, r, message, mlen)) == DLV_OK) {
	  op->support_create();
	  //is_pending = true;
	  //pending = current;
	  //editing = op;
	  //current = op;
	  op->add_standard_data_objects();
	} else
	  delete op;
      }
      return ok;
    }
  }
}

DLVreturn_type DLV::multi_layer::multilayer_update(model *parent,
						   const real_l r,
						   char message[],
						   const int_g mlen)
{
  bool redraw = get_model()->multi_layer(parent, r, slab);
  space = r;
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::multi_layer::get_name() const
{
  return (get_model_name() + " - multi_layer");
}

DLVreturn_type DLV::fill_geometry::create(const char name[],
					  const int selection, char message[],
					  const int_g mlen)
{
  const operation *ins = select_model(selection, 8, message, mlen);
  if (ins == 0)
    return DLV_ERROR;
  else {
    model *parent = get_current_model();
    model *m = create_atoms(name, 0);
    if (m == 0)
      return DLV_ERROR;
    else {
      fill_geometry *op = new fill_geometry(m, ins->get_model());
      DLVreturn_type ok = DLV_OK;
      if (op == 0) {
	delete m;
	strncpy(message, "Failed to create fill_geometry", mlen);
	ok = DLV_ERROR;
      } else {
	if ((ok = op->geometry_update(parent, message, mlen)) == DLV_OK) {
	  op->support_create();
	  //is_pending = true;
	  //pending = current;
	  //editing = op;
	  //current = op;
	  op->add_standard_data_objects();
	} else
	  delete op;
      }
      return ok;
    }
  }
}

DLVreturn_type DLV::fill_geometry::geometry_update(model *parent,
						   char message[],
						   const int_g mlen)
{
  bool redraw = get_model()->fill_geometry(parent, geometry);
  if (redraw) {
#ifdef ENABLE_DLV_GRAPHICS
    (void) update_cell_info(message, mlen);
    // should really be redraw_atoms
    return get_model()->update_cell(get_display_obj(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
  }
  return DLV_OK;
}

DLV::string DLV::fill_geometry::get_name() const
{
  return (get_model_name() + " - fill_geometry");
}

bool DLV::fill_geometry::get_lattice_rotation(real_l r[3][3]) const
{
  // I don't think any rotation happens
  r[0][0] = 1.0;
  r[0][1] = 0.0;
  r[0][2] = 0.0;
  r[1][0] = 0.0;
  r[1][1] = 1.0;
  r[1][2] = 0.0;
  r[2][0] = 0.0;
  r[2][1] = 0.0;
  r[2][2] = 1.0;
  return true;
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::origin_shift::update_origin_atoms(const model *parent,
						      real_l &x, real_l &y,
						      real_l &z,
						      const bool frac,
						      char message[],
						      const int_g mlen)
{
  x = sx;
  y = sy;
  z = sz;
  bool redraw = get_model()->update_origin_atoms(parent, x, y, z, frac);
  sx = x;
  sy = y;
  sz = z;
  fractional = frac;
  if (redraw) {
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLVreturn_type DLV::atom_edit::create(const char name[], int_g &atn,
				      const bool all, char message[],
				      const int_g mlen)
{
  model *parent = get_continue()->get_model();
  //model *m = parent->duplicate_model(name);
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    atn = 0;
    int_g index = parent->find_primitive_selection();
    atom_edit *op = new atom_edit(m, index);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create atom edit", mlen);
      ok = DLV_ERROR;
    } else {
      atn = m->get_atomic_number(index);
      if (op->update_atom(parent, atn, all, message, mlen) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::atom_edit::update_atom(model *parent, const int_g atn,
					   const bool all, char message[],
					   const int_g mlen)
{
  if (get_model()->edit_atom(parent, atn, all, index)) {
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLV::string DLV::atom_edit::get_name() const
{
  return (get_model_name() + " - atom edit");
}

DLVreturn_type
DLV::atom_props::create(const char name[], int_g &charge, real_g &radius,
			real_g &red, real_g &green, real_g &blue,
			int_g &spin, bool &use_charge, bool &use_radius,
			bool &use_colour, bool &use_spin, const bool all,
			char message[], const int_g mlen)
{
  model *parent = get_current_model();
  model *m = create_atoms(name, parent->get_model_type());
  //model *m = parent->duplicate_model(name);
  if (m == 0)
    return DLV_ERROR;
  else {
    int_g index = parent->find_primitive_selection();
    atom_props *op = new atom_props(m, index);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create edit atom properties", mlen);
      ok = DLV_ERROR;
    } else {
      m->get_atom_properties(index, charge, radius, red, green, blue, spin,
			     use_charge, use_radius, use_colour, use_spin);
      if (op->update_props(parent, charge, radius, red, green, blue, spin,
			   use_charge, use_radius, use_colour, use_spin,
			   all, message, mlen) == DLV_OK) {
	op->add_standard_data_objects();
	// inherit before change current
	op->inherit_non_std_data();
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
      } else
	delete op;
    }
    return ok;
  }
}


DLVreturn_type DLV::atom_props::update_rod_props(const int_g dw1,
						 const bool use_dw1,
						 const int_g dw2,
						 const bool use_dw2,
						 const int_g occ,
						 const bool use_occ,
						 const bool all, char message[],
						 const int_g mlen)
{
  //clb note this differs to other types of update
  //either accept or cancel - no going back!
  model *parent = get_current_model();
  model *m = parent->duplicate_model("update rod props");
  if (m == 0)
    return DLV_ERROR;
  else {
    atom_props *op = new atom_props(m, parent->find_primitive_selection());
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create edit atom properties", mlen);
      ok = DLV_ERROR;
    } else {
      op->add_standard_data_objects();
      op->update_rod_props(parent, dw1, use_dw1, dw2, use_dw2, occ, use_occ,
			   all, message, mlen);
    }
    return ok;
  }
}

DLVreturn_type
DLV::atom_props::update_props(model *parent, const int_g charge,
			      real_g &radius, const real_g red,
			      const real_g green, const real_g blue,
			      const int_g spin, const bool use_charge,
			      const bool use_radius, const bool use_colour,
			      const bool use_spin, const bool all,
			      char message[], const int_g mlen)
{
  if (get_model()->edit_atom_props(parent, charge, radius, red,
				   green, blue, spin, use_charge, use_radius,
				   use_colour, use_spin, all, index)) {
    (void) update_cell_info(message, mlen);
    return get_model()->update_atoms(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLVreturn_type
DLV::atom_props::update_rod_props(model *parent, 
				  const int_g dw1, const bool use_dw1,
				  const int_g dw2, const bool use_dw2,
				  const int_g occ, const bool use_occ,
				  const bool all,
				  char message[], const int_g mlen)
{
  if (get_model()->edit_atom_rod_props(parent, dw1, use_dw1, dw2, use_dw2, 
				       occ, use_occ, all, index)) { //need ot do
    //(void) update_cell_info(message, mlen);
    //return get_model()->update_atoms(get_display_obj(), message, mlen);
    //necessary?
    return DLV_OK; 
  }
  return DLV_OK;
}

DLV::string DLV::atom_props::get_name() const
{
  return (get_model_name() + " - edit atom properties");
}

bool DLV::atom_props::is_geometry() const
{
  return false;
}

bool DLV::atom_props::is_property_change() const
{
  return true;
}

DLVreturn_type DLV::atom_delete::create(const char name[], char message[],
					const int_g mlen)
{
  model *parent = get_continue()->get_model();
  model *m = parent->duplicate_model(name);
  if (m == 0)
    return DLV_ERROR;
  else {
    atom_delete *op = new atom_delete(m);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create atom delete", mlen);
      ok = DLV_ERROR;
    } else if ((ok = op->delete_atoms(parent, message, mlen)) == DLV_OK) {
      op->support_create();
      //is_pending = true;
      //pending = current;
      //editing = op;
      //current = op;
      op->add_standard_data_objects();
    } else
      delete op;
    return ok;
  }
}

DLVreturn_type DLV::atom_delete::delete_atoms(model *parent,
					      char message[],
					      const int_g mlen)
{
  if (get_model()->delete_atoms(parent)) {
    // this is no longer consistent with modified behaviour
    //(void) update_cell_info(message, mlen);
    //return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLV::string DLV::atom_delete::get_name() const
{
  return (get_model_name() + " - atom delete");
}

DLVreturn_type DLV::atom_insert::create(const char name[], char message[],
					const int_g mlen)
{
  model *parent = get_continue()->get_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    atom_insert *op = new atom_insert(m, parent->keep_symmetry());
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create atom insert", mlen);
      ok = DLV_ERROR;
    } else {
      if ((ok = op->insert_atom(parent, 1, 0.0, 0.0, 0.0, false,
				message, mlen)) == DLV_OK) {
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::atom_insert::insert_atom(const model *parent,
					     const int_g atn,
					     const real_l x, const real_l y,
					     const real_l z, const bool frac,
					     char message[], const int_g mlen)
{
  bool redraw = get_model()->insert_atom(parent, atn, x, y, z, frac, keep_sym);
  x_p = x;
  y_p = y;
  z_p = z;
  atomic_number = atn;
  if (redraw) {
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLVreturn_type DLV::atom_insert::switch_coords(const bool frac, const bool,
					       real_l &x, real_l &y, real_l &z,
					       char message[], const int_g mlen)
{
  if (frac != fractional) {
    get_model()->convert_coords(frac, x_p, y_p, z_p);
    fractional = frac;
  }
  x = x_p;
  y = y_p;
  z = z_p;
  return DLV_OK;
}

DLVreturn_type DLV::atom_insert::use_selections(model *parent, real_l &x,
						real_l &y, real_l &z,
						char message[],
						const int_g mlen)
{
  coord_type p[3];
  get_model()->get_average_selection_position(p);
  x_p = p[0];
  y_p = p[1];
  z_p = p[2];
  if (fractional)
    get_model()->convert_coords(fractional, x_p, y_p, z_p);
  x = x_p;
  y = y_p;
  z = z_p;
  return insert_atom(parent, atomic_number, x_p, y_p, z_p, fractional,
		     message, mlen);
}

DLVreturn_type DLV::atom_insert::transform_editor(model *parent,
						  const bool v, real_l &x,
						  real_l &y, real_l &z,
						  char message[],
						  const int_g mlen)
{
  real_g matrix[4][4];
  real_g translate[3];
  real_g centre[3];
  if (v) {
    editor = true;
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
  } else {
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
    if (editor) {
      if (fractional)
	get_model()->convert_coords(!fractional, x_p, y_p, z_p);
      get_model()->transform_atom(parent, atomic_number, x_p, y_p, z_p,
				  keep_sym, matrix, translate, centre);
      if (fractional)
	get_model()->convert_coords(fractional, x_p, y_p, z_p);
      editor = false;
    }
  }
  x = x_p;
  y = y_p;
  z = z_p;
  (void) update_cell_info(message, mlen);
  return get_model()->update_cell(get_display_obj(), message, mlen);
}

DLV::string DLV::atom_insert::get_name() const
{
  return (get_model_name() + " - atom insert");
}

DLVreturn_type DLV::model_insert::create(const char name[],
					 const int_g selection, char message[],
					 const int_g mlen)
{
  const operation *ins = select_model(selection, 0, message, mlen);
  if (ins == 0)
    return DLV_ERROR;
  else {
    model *parent = get_current_model();
    model *m = create_atoms(name, parent->get_model_type());
    if (m == 0)
      return DLV_ERROR;
    else {
      model_insert *op = new model_insert(m, ins->get_model(),
				       parent->keep_symmetry());
      DLVreturn_type ok = DLV_OK;
      if (op == 0) {
	delete m;
	strncpy(message, "Failed to create model insert", mlen);
	ok = DLV_ERROR;
      } else {
	// Its possible after a project load that the model hasn't been drawn
	ins->get_model()->update_atom_list();
	if ((ok = op->insert_model(parent, 0.0, 0.0, 0.0,
				   false, message, mlen)) == DLV_OK) {
	  op->support_create();
	  //is_pending = true;
	  //pending = current;
	  //editing = op;
	  //current = op;
	  op->add_standard_data_objects();
	} else
	  delete op;
      }
      return ok;
    }
  }
}

DLVreturn_type DLV::model_insert::insert_model(const model *parent,
					       const real_l x, const real_l y,
					       const real_l z, const bool frac,
					       char message[], const int_g mlen)
{
  if (get_model()->insert_model(parent, insert, x, y, z, frac,
				x_c, y_c, z_c, keep_sym)) {
    x_c = x_p;
    y_c = y_p;
    z_c = z_p;
    if (frac)
      get_model()->convert_coords(false, x_c, y_c, z_c);
    x_p = x;
    y_p = y;
    z_p = z;
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLVreturn_type DLV::model_insert::switch_coords(const bool frac, const bool,
						real_l &x, real_l &y, real_l &z,
						char message[],
						const int_g mlen)
{
  if (frac != fractional) {
    get_model()->convert_coords(frac, x_p, y_p, z_p);
    fractional = frac;
  }
  x = x_p;
  y = y_p;
  z = z_p;
  return DLV_OK;
}

DLVreturn_type DLV::model_insert::use_selections(model *parent,
						 real_l &x, real_l &y,
						 real_l &z, char message[],
						 const int_g mlen)
{
  coord_type p[3];
  get_model()->get_average_selection_position(p);
  x_p = p[0];
  y_p = p[1];
  z_p = p[2];
  if (fractional)
    get_model()->convert_coords(fractional, x_p, y_p, z_p);
  x = x_p;
  y = y_p;
  z = z_p;
  return insert_model(parent, x_p, y_p, z_p, fractional,
		      message, mlen);
}

DLVreturn_type DLV::model_insert::centre_from_selects(const model *parent,
						      real_l &x, real_l &y,
						      real_l &z,
						      char message[],
						      const int_g mlen)
{
  coord_type p[3];
  get_model()->get_average_selection_position(p);
  x_c = p[0];
  y_c = p[1];
  z_c = p[2];
  x = x_p;
  y = y_p;
  z = z_p;
  //return insert_model(parent, x_p, y_p, z_p, fractional,
  //	      message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::model_insert::transform_editor(model *parent,
						   const bool v, real_l &x,
						   real_l &y, real_l &z,
						   char message[],
						   const int_g mlen)
{
  real_g matrix[4][4];
  real_g translate[3];
  real_g centre[3];
  if (v) {
    editor = true;
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
  } else {
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
    if (editor) {
      if (fractional)
	get_model()->convert_coords(!fractional, x_p, y_p, z_p);
      get_model()->transform_model(parent, insert, x_p, y_p, z_p, x_c, y_c, z_c,
				   keep_sym, matrix, translate, centre);
      if (fractional)
	get_model()->convert_coords(fractional, x_p, y_p, z_p);
      editor = false;
    }
  }
  x = x_p;
  y = y_p;
  z = z_p;
  (void) update_cell_info(message, mlen);
  return get_model()->update_cell(get_display_obj(), message, mlen);
}

DLV::string DLV::model_insert::get_name() const
{
  return (get_model_name() + " - model insert");
}

DLVreturn_type DLV::atom_move::create(const char name[], real_l &x, real_l &y,
				      real_l &z, char message[],
				      const int_g mlen)
{
  model *parent = get_continue()->get_model();
  model *m = create_atoms(name, parent->get_model_type());
  if (m == 0)
    return DLV_ERROR;
  else {
    coord_type p[3];
    parent->get_average_selection_position(p);
    atom_move *op = new atom_move(m, parent->keep_symmetry(), p[0], p[1], p[2]);
    DLVreturn_type ok = DLV_OK;
    if (op == 0) {
      delete m;
      strncpy(message, "Failed to create atom move", mlen);
      ok = DLV_ERROR;
    } else {
      if (op->move_atoms(parent, p[0], p[1], p[2], false, false,
			 message, mlen) == DLV_OK) {
	x = p[0];
	y = p[1];
	z = p[2];
	op->support_create();
	//is_pending = true;
	//pending = current;
	//editing = op;
	//current = op;
	op->add_standard_data_objects();
      } else
	delete op;
    }
    return ok;
  }
}

DLVreturn_type DLV::atom_move::move_atoms(model *parent, const real_l x,
					  const real_l y, const real_l z,
					  const bool frac, const bool displace,
					  char message[], const int_g mlen)
{
  real_l fx = x;
  real_l fy = y;
  real_l fz = z;
  if (frac)
    get_model()->convert_coords(false, fx, fy, fz);
  if (displace) {
    x_p += (fx - shift[0]);
    y_p += (fy - shift[1]);
    z_p += (fz - shift[2]);
  } else {
    x_p = fx;
    y_p = fy;
    z_p = fz;
  }
  if (get_model()->move_atoms(parent, x_p, y_p, z_p,
			      x_c, y_c, z_c, shift, keep_sym)) {
    (void) update_cell_info(message, mlen);
    return get_model()->update_cell(get_display_obj(), message, mlen);
  }
  return DLV_OK;
}

DLVreturn_type DLV::atom_move::switch_coords(const bool frac,
					     const bool displace, real_l &x,
					     real_l &y, real_l &z,
					     char message[], const int_g mlen)
{
  if (displace) {
    x = shift[0];
    y = shift[1];
    z = shift[2];
  } else {
    x = x_p;
    y = y_p;
    z = z_p;
  }
  fractional = frac;
  displacement = displace;
  if (frac)
    get_model()->convert_coords(frac, x, y, z);
  return DLV_OK;
}

DLVreturn_type DLV::atom_move::use_selections(model *parent,
					      real_l &x, real_l &y,
					      real_l &z, char message[],
					      const int_g mlen)
{
  coord_type p[3];
  get_model()->get_average_selection_position(p);
  x_p = p[0];
  y_p = p[1];
  z_p = p[2];
  x = x_p;
  y = y_p;
  z = z_p;
  if (fractional)
    get_model()->convert_coords(true, x, y, z);
  return move_atoms(parent, x_p, y_p, z_p, false, false, message, mlen);
}

DLVreturn_type DLV::atom_move::centre_from_selects(const model *parent,
						   real_l &x, real_l &y,
						   real_l &z,
						   char message[],
						   const int_g mlen)
{
  coord_type p[3];
  get_model()->get_average_selection_position(p);
  x_c = p[0];
  y_c = p[1];
  z_c = p[2];
  shift[0] = x_p - x_c;
  shift[1] = y_p - y_c;
  shift[2] = z_p - z_c;
  if (displacement) {
    x = shift[0];
    y = shift[0];
    z = shift[0];
  } else {
    x = x_p;
    y = y_p;
    z = z_p;
  }
  //return insert_model(parent, x_p, y_p, z_p, fractional,
  //message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::atom_move::transform_editor(model *parent,
						const bool v, real_l &x,
						real_l &y, real_l &z,
						char message[],
						const int_g mlen)
{
  real_g matrix[4][4];
  real_g translate[3];
  real_g centre[3];
  if (v) {
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
    editor = true;
  } else {
    get_model()->set_edit_transform(get_display_obj(), v, matrix, translate,
				    centre, message, mlen);
    if (editor) {
      if (fractional)
	get_model()->convert_coords(!fractional, x_p, y_p, z_p);
      (void) get_model()->transform_move(parent, x_p, y_p, z_p, x_c, y_c, z_c,
					 shift, keep_sym, matrix, translate,
					 centre);
      if (fractional)
	get_model()->convert_coords(fractional, x_p, y_p, z_p);
      editor = false;
    }
  }
  x = x_p;
  y = y_p;
  z = z_p;
  (void) update_cell_info(message, mlen);
  return get_model()->update_cell(get_display_obj(), message, mlen);
}

DLV::string DLV::atom_move::get_name() const
{
  return (get_model_name() + " - atom move");
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::supercell *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::supercell(0, 0, 0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::del_symmetry *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::del_symmetry(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::molecule_view *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::molecule_view(0, 0, 0, 0, false, false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::remove_lattice *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::remove_lattice(0, 0, 0, 0, false, false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::alter_lattice *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::alter_lattice(0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::origin_shift *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::origin_shift(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::vacuum_cell *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::vacuum_cell(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::cluster *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::cluster(0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::slab_from_bulk *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::slab_from_bulk(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::slab_from_surface *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::slab_from_surface(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::surface_edit *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::surface_edit(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::salvage *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::salvage(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::wulff *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::wulff(0, 10.0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::multi_layer *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::multi_layer(0, 10.0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::fill_geometry *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::fill_geometry(0, 0);
    }

  }
}

template <class Archive>
void DLV::supercell::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & matrix;
  ar & conventional_cell;
}

template <class Archive>
void DLV::del_symmetry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
}

template <class Archive>
void DLV::molecule_view::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & na;
  ar & nb;
  ar & nc;
  ar & conventional;
  ar & centred;
  ar &edges;
}

template <class Archive>
void DLV::remove_lattice::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & na;
  ar & nb;
  ar & nc;
  ar & conventional;
  ar & centred;
  ar &edges;
}

template <class Archive>
void DLV::alter_lattice::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & a;
  ar & b;
  ar & c;
  ar & alpha;
  ar & beta;
  ar & gamma;
}

template <class Archive>
void DLV::origin_shift::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & sx;
  ar & sy;
  ar & sz;
  ar & fractional;
}

template <class Archive>
void DLV::vacuum_cell::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & c_axis;
  ar & slab_origin;
}

template <class Archive>
void DLV::cluster::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & neighbours;
  ar & radius;
}

template <class Archive>
void DLV::slab_from_bulk::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & crystal3d;
  ar & miller_h;
  ar & miller_k;
  ar & miller_l;
  ar & conventional_cell;
  ar & termination;
  ar & layer_tolerance;
  ar & number_of_layers;
  ar & r;
}

template <class Archive>
void DLV::slab_from_surface::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & layer_tolerance;
  ar & number_of_layers;
}

template <class Archive>
void DLV::surface_edit::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & crystal3d;
  ar & miller_h;
  ar & miller_k;
  ar & miller_l;
  ar & conventional_cell;
  ar & termination;
  ar & layer_tolerance;
  ar & r;
}

template <class Archive>
void DLV::salvage::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & layer_tolerance;
  ar & number_of_layers;
}

template <class Archive>
void DLV::wulff::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & scale;
  ar & crystal;
}

template <class Archive>
void DLV::multi_layer::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & space;
  ar & slab;
}

template <class Archive>
void DLV::fill_geometry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & geometry;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::supercell)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::del_symmetry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::molecule_view)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::remove_lattice)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::alter_lattice)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::origin_shift)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::vacuum_cell)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::cluster)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::slab_from_bulk)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::slab_from_surface)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::surface_edit)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::salvage)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::wulff)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::multi_layer)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::fill_geometry)

DLV_SUPPRESS_TEMPLATES(DLV::edit_geometry)

DLV_NORMAL_EXPLICIT(DLV::supercell)
DLV_NORMAL_EXPLICIT(DLV::del_symmetry)
DLV_NORMAL_EXPLICIT(DLV::molecule_view)
DLV_NORMAL_EXPLICIT(DLV::remove_lattice)
DLV_NORMAL_EXPLICIT(DLV::alter_lattice)
DLV_NORMAL_EXPLICIT(DLV::origin_shift)
DLV_NORMAL_EXPLICIT(DLV::vacuum_cell)
DLV_NORMAL_EXPLICIT(DLV::cluster)
DLV_NORMAL_EXPLICIT(DLV::slab_from_bulk)
DLV_NORMAL_EXPLICIT(DLV::slab_from_surface)
DLV_NORMAL_EXPLICIT(DLV::surface_edit)
DLV_NORMAL_EXPLICIT(DLV::salvage)
DLV_NORMAL_EXPLICIT(DLV::wulff)
DLV_NORMAL_EXPLICIT(DLV::multi_layer)
DLV_NORMAL_EXPLICIT(DLV::fill_geometry)

#  ifdef ENABLE_DLV_GRAPHICS

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_edit *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_edit(0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_props *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_props(0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_delete *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_delete(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_insert *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_insert(0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_insert *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_insert(0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::atom_move *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::atom_move(0, false, 0.0, 0.0, 0.0);
    }
  }
}

template <class Archive>
void DLV::atom_edit::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & index;
}

template <class Archive>
void DLV::atom_props::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & index;
}

template <class Archive>
void DLV::atom_delete::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
}

template <class Archive>
void DLV::atom_insert::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & keep_sym;
  ar & atomic_number;
  ar & x_p;
  ar & y_p;
  ar & z_p;
  ar & fractional;
}

template <class Archive>
void DLV::model_insert::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & insert;
  ar & keep_sym;
  ar & x_p;
  ar & y_p;
  ar & z_p;
  ar & x_c;
  ar & y_c;
  ar & z_c;
  ar & fractional;
}

template <class Archive>
void DLV::atom_move::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_geometry>(*this);
  ar & keep_sym;
  ar & x_p;
  ar & y_p;
  ar & z_p;
  ar & x_c;
  ar & y_c;
  ar & z_c;
  ar & shift;
  ar & fractional;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_edit)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_props)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_delete)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_insert)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_insert)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_move)

DLV_NORMAL_EXPLICIT(DLV::atom_edit)
DLV_NORMAL_EXPLICIT(DLV::atom_props)
DLV_NORMAL_EXPLICIT(DLV::atom_delete)
DLV_NORMAL_EXPLICIT(DLV::atom_insert)
DLV_NORMAL_EXPLICIT(DLV::model_insert)
DLV_NORMAL_EXPLICIT(DLV::atom_move)

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
