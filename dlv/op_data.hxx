
#ifndef DLV_DATA_OPS
#define DLV_DATA_OPS

namespace DLV {

  class real_data;
  class md_trajectory;
  class surface_data;
  class volume_data;

  class cube_file {
  public:
    volume_data *read_data(operation *op, volume_data *v,
			   const char filename[], const string id,
			   const string tag, char message[],
			   const int_g mlen);
    static bool read_model(model *structure, const char filename[],
			   char message[], const int_g mlen);

    virtual ~cube_file();

  protected:
    virtual string get_label(std::ifstream &input, const string tag,
			     const string id) = 0;
    virtual volume_data *create_data(const string label, operation *op,
				     const int_g nx, const int_g ny,
				     const int_g nz, const real_g origin[3],
				     const real_g astep[3],
				     const real_g bstep[3],
				     const real_g cstep[3]) = 0;
  };

  class load_cube_file : public cube_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool load_model, const bool set_bonds,
			     char message[], const int_g mlen);

  protected:
    string get_label(std::ifstream &input, const string tag,
		     const string id);
    volume_data *create_data(const string label, operation *op,
			     const int_g nx, const int_g ny, const int_g nz,
			     const real_g origin[3], const real_g astep[3],
			     const real_g bstep[3], const real_g cstep[3]);
  };

  class load_cube_model : public load_atom_model_op,
			  public load_cube_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_bonds, char message[],
			     const int_g mlen);
    // public for serialization
    load_cube_model(model *m, const char file[]);
    
    bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_cube_data : public load_data_op, public load_cube_file {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    // public for serialization
    load_cube_data(const char file[]);

    bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class xsf_file {
  public:
    bool read_data(operation *op, model *m, md_trajectory * &md,
		   volume_data * &v1, volume_data * &v2, surface_data * &s1,
		   real_data * &fermi, const bool reload, const char filename[],
		   const string id, char message[], const int_g mlen);
    static model *read_model(const string name, const char filename[],
			     char message[], const int_g mlen);

  private:
    static volume_data *create_bz(const char name[], const string tag,
				  operation *op, const int nx, const int ny,
				  const int nz, const real_g origin[3],
				  const real_g astep[3], const real_g bstep[3],
				  const real_g cstep[3]);
    static void map_bz(real_g * &array, const int nx, const int ny,
		       const int nz, const real_g origin[3],
		       const real_g astep[3], const real_g bstep[3],
		       const real_g cstep[3], const model *structure);
    static void shift_origin(const int nx, const int ny, const int nz,
			     const real_g origin[3],const real_g astep[3],
			     const real_g bstep[3],const real_g cstep[3],
			     int &mx, int &my, int &mz, real_g o[3]);
  };

  class load_xsf_model : public load_atom_model_op, public xsf_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_bonds, bool &new_model,
			     char message[], const int_g mlen);
    // public for serialization
    load_xsf_model(model *m, const char file[]);
    
    bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    md_trajectory *md;
    volume_data *v1;
    volume_data *v2;
    surface_data *s1;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - common stuff with xsf_model
  class load_xsf_data : public load_data_op, public xsf_file {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    // public for serialization
    load_xsf_data(const char file[]);

    bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    md_trajectory *md;
    volume_data *v1;
    volume_data *v2;
    surface_data *s1;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#  ifdef USE_ESCDF
  // Todo - Needs data read, or simplify if only ever read models
  class escdf_file {
  public:
    //volume_data *read_data(operation *op, volume_data *v,
    //		   const char filename[], const string id,
    //		   const string tag, char message[],
    //		   const int_g mlen);
    static DLV::model *read_model(const string name, const char filename[],
				  char message[], const int_g mlen);

    virtual ~escdf_file();

  protected:
    virtual string get_label(std::ifstream &input, const string tag,
			     const string id) = 0;
    /*
    virtual volume_data *create_data(const string label, operation *op,
				     const int_g nx, const int_g ny,
				     const int_g nz, const real_g origin[3],
				     const real_g astep[3],
				     const real_g bstep[3],
				     const real_g cstep[3]) = 0;
    */
  };

  class load_escdf_file : public escdf_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool load_model, const bool set_bonds,
			     char message[], const int_g mlen);

  protected:
    string get_label(std::ifstream &input, const string tag,
		     const string id);
    /*
    volume_data *create_data(const string label, operation *op,
			     const int_g nx, const int_g ny, const int_g nz,
			     const real_g origin[3], const real_g astep[3],
			     const real_g bstep[3], const real_g cstep[3]);
    */
  };

  class load_escdf_model : public load_atom_model_op,
			   public load_escdf_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_bonds, char message[],
			     const int_g mlen);
    // public for serialization
    load_escdf_model(model *m, const char file[]);
    
    //bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
#  endif // ESCDF

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::load_cube_model)
BOOST_CLASS_EXPORT_KEY(DLV::load_cube_data)
BOOST_CLASS_EXPORT_KEY(DLV::load_xsf_model)
BOOST_CLASS_EXPORT_KEY(DLV::load_xsf_data)

#  ifdef USE_ESCDF
BOOST_CLASS_EXPORT_KEY(DLV::load_escdf_model)
#  endif // ESCDF

#endif // DLV_USES_SERIALIZE

inline DLV::load_cube_model::load_cube_model(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::load_cube_data::load_cube_data(const char file[])
  : load_data_op(file)
{
}

inline DLV::load_xsf_model::load_xsf_model(model *m, const char file[])
  : load_atom_model_op(m, file), md(0), v1(0), v2(0), s1(0)
{
}

inline DLV::load_xsf_data::load_xsf_data(const char file[])
  : load_data_op(file), md(0), v1(0), v2(0), s1(0)
{
}

#  ifdef USE_ESCDF
inline DLV::load_escdf_model::load_escdf_model(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}
#  endif // USE_ESCDF

#endif // DLV_DATA_OPS
