
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
//#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
//#  include "../graphics/calculations.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
//#include "atom_model.hxx"
#include "model.hxx"
#include "file.hxx"
#include "data_objs.hxx"
#include "operation.hxx"
//#include "op_model.hxx"
//#include "op_atoms.hxx"
#include "project.hxx"
//#include "job.hxx"

DLVreturn_type DLV::operation::update_supercell(const int_g matrix[3][3],
						const bool conv_cell,
						char message[],
						const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_supercell(pending->get_model(), matrix, conv_cell,
				     message, mlen);
}

DLVreturn_type DLV::operation::update_supercell(const model *,
						const int_g [3][3], const bool,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: not a supercell operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_edit_lattice(const real_l values[6],
						   char message[],
						   const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_lattice(pending->get_model(), values,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_lattice(const model *, const real_l [6],
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an edit lattice operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_origin(const real_l x, const real_l y,
					     const real_l z, const bool frac,
					     char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_origin(pending->get_model(), x, y, z, frac,
				  message, mlen);
}

DLVreturn_type DLV::operation::update_origin(const model *,
					     const real_l, const real_l,
					     const real_l, const bool,
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a shift origin operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_origin_symm(real_l &x, real_l &y,
						  real_l &z, const bool frac,
						  char message[],
						  const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_origin_symm(pending->get_model(), x, y, z, frac,
				       message, mlen);
}

DLVreturn_type DLV::operation::update_origin_symm(const model *,
						  real_l &, real_l &,
						  real_l &, const bool,
						  char message[],
						  const int_g mlen)
{
  strncpy(message, "BUG: not a shift origin operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_vacuum(const real_l c,
					     const real_l origin,
					     char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_vacuum(pending->get_model(), c, origin,
				  message, mlen);
}

DLVreturn_type DLV::operation::update_vacuum(const model *,
					     const real_l, const real_l,
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a vacuum gap operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_cluster(const int_g n,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_cluster(pending->get_model(), n,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_cluster(const int_g n, const real_l x,
					      const real_l y, const real_l z,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_cluster(pending->get_model(), n, x, y, z,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_cluster(model *, const int_g,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut cluster operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_cluster(model *, const int_g,
					      const real_l, const real_l,
					      const real_l, char message[],
					      const int_g mlen)
{
  strncpy(message, "BUG: not a cut cluster operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_cluster(const real_l r,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_cluster(pending->get_model(), r,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_cluster(model *, const real_l,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut cluster operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type
DLV::operation::update_slab(const int_g h, const int_g k, const int_g l,
			    const bool conventional, const real_g tol,
			    int_g &nlayers, string * &labels, int_g &n,
			    char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_slab(pending->get_model(), h, k, l, conventional,
				tol, nlayers, labels, n, message, mlen);
}

DLVreturn_type DLV::operation::update_slab(model *, const int_g, const int_g,
					   const int_g, const bool,
					   const real_g, int_g &, string * &,
					   int_g &, char message[],
					   const int_g mlen)
{
  strncpy(message, "BUG: not a cut slab operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_slab(const real_g tol, int_g &nlayers,
					   string * &labels, int_g &n,
					   char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_slab(pending->get_model(), tol, nlayers,
				labels, n, message, mlen);
}

DLVreturn_type DLV::operation::update_slab(model *, const real_g, int_g &,
					   string * &, int_g &, char message[],
					   const int_g mlen)
{
  strncpy(message, "BUG: not a cut slab operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_slab_term(const int_g term,
						char message[],
						const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_slab_term(pending->get_model(), term,
				     message, mlen);
}

DLVreturn_type DLV::operation::update_slab_term(model *, const int_g,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: not a cut slab operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_slab(const int_g nlayers, char message[],
					   const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_slab(pending->get_model(), nlayers, message, mlen);
}

DLVreturn_type DLV::operation::update_slab(model *, const int_g,
					   char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut slab operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_surface(const int_g h, const int_g k,
					      const int_g l,
					      const bool conventional,
					      const real_g tol,
					      string * &labels, int_g &n,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_surface(pending->get_model(), h, k, l, conventional,
				   tol, labels, n, message, mlen);
}

DLVreturn_type DLV::operation::update_surface(model *, const int_g, const int_g,
					      const int_g, const bool,
					      const real_g, string * &, int_g &,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut surface operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_surface(const real_g tol,
					      string * &labels, int_g &n,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_surface(pending->get_model(), tol,
				   labels, n, message, mlen);
}

DLVreturn_type DLV::operation::update_surface(model *parent, const real_g tol,
					      string * &labels, int_g &n,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut surface operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_surface(const int_g term, char message[],
					      const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_surface(pending->get_model(), term, message, mlen);
}

DLVreturn_type DLV::operation::update_surface(model *parent, const int_g term,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut surface operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_salvage(const real_g tol,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_salvage(pending->get_model(), tol, message, mlen);
}

DLVreturn_type DLV::operation::update_salvage(model *, const real_g,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut salvage operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_salvage(const int_g nlayers,
					      char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_salvage(pending->get_model(), nlayers,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_salvage(model *, const int_g,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a cut salvage operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::wulff_update(const real_l r,
					    char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->wulff_update(pending->get_model(), r, message, mlen);
}

DLVreturn_type DLV::operation::wulff_update(model *, const real_l r,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a wulff update operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::multilayer_update(const real_l r, char message[],
						 const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->multilayer_update(pending->get_model(), r, message, mlen);
}

DLVreturn_type DLV::operation::multilayer_update(model *, const real_l r,
						 char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: not a multilayer update operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::geometry_update(char message[],
					       const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->geometry_update(pending->get_model(), message, mlen);
}

DLVreturn_type DLV::operation::geometry_update(model *, char message[],
					       const int_g mlen)
{
  strncpy(message, "BUG: not a geometry fill update operation", mlen);
  return DLV_ERROR;
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::operation::update_origin_atoms(real_l &x, real_l &y,
						   real_l &z, const bool frac,
						   char message[],
						   const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_origin_atoms(pending->get_model(), x, y, z, frac,
					message, mlen);
}

DLVreturn_type DLV::operation::update_origin_atoms(const model *parent,
						   real_l &x, real_l &y,
						   real_l &z, const bool frac,
						   char message[],
						   const int_g mlen)
{
  strncpy(message, "BUG: not a shift origin operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_atom(const int_g atn, const bool all,
					   char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_atom(pending->get_model(), atn, all,
				message, mlen);
}

DLVreturn_type DLV::operation::update_atom(model *, const int_g, const bool,
					   char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom edit operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type
DLV::operation::update_props(const int_g charge, real_g &radius,
			     const real_g red, const real_g green,
			     const real_g blue, const int_g spin,
			     const bool use_charge, const bool use_radius,
			     const bool use_colour, const bool use_spin,
			     const bool all, char message[], const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->update_props(pending->get_model(), charge, radius, red,
				 green, blue, spin, use_charge, use_radius,
				 use_colour, use_spin, all, message, mlen);
}

DLVreturn_type DLV::operation::update_props(model *, const int_g, real_g &,
					    const real_g, const real_g,
					    const real_g, const int_g,
					    const bool, const bool, const bool,
					    const bool, const bool,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an edit atom properties operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_rod_props(model *, const int_g,
						const bool, const int_g,
						const bool, const int_g,
						const bool, const bool,
						char message[],const int_g mlen)
{
  strncpy(message, "BUG: not an edit atom properties operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_deleted_atoms(char message[],
						    const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->delete_atoms(pending->get_model(), message, mlen);
}

DLVreturn_type DLV::operation::delete_atoms(model *,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom delete operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_inserted_atom(const int_g atn,
						    const bool frac,
						    const real_l x,
						    const real_l y,
						    const real_l z,
						    char message[],
						    const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->insert_atom(pending->get_model(), atn, x, y, z, frac,
				message, mlen);
}

DLVreturn_type DLV::operation::switch_atom_coords(const bool frac,
						  const bool displace,
						  real_l &x, real_l &y,
						  real_l &z, char message[],
						  const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->switch_coords(frac, displace, x, y, z, message, mlen);
}

DLVreturn_type DLV::operation::update_from_selections(real_l &x,
						      real_l &y, real_l &z,
						      char message[],
						      const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->use_selections(pending->get_model(), x, y, z,
				   message, mlen);
}

DLVreturn_type DLV::operation::set_edit_transform(const bool v, real_l &x,
						  real_l &y, real_l &z,
						  char message[],
						  const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->transform_editor(pending->get_model(), v, x, y, z,
				     message, mlen);
}

DLVreturn_type DLV::operation::insert_atom(const model *parent, const int_g atn,
					   const real_l x, const real_l y,
					   const real_l z, const bool frac,
					   char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom insert operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::switch_coords(const bool, const bool, real_l &,
					     real_l &, real_l &,
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom coordinate operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::use_selections(model *, real_l &,
					      real_l &, real_l &,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom coordinate operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::transform_editor(model *, const bool,
						real_l &, real_l &, real_l &,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: not using transform editor", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_inserted_model(const bool frac,
						     const real_l x,
						     const real_l y,
						     const real_l z,
						     char message[],
						     const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->insert_model(pending->get_model(), x, y, z, frac,
				 message, mlen);
}

DLVreturn_type DLV::operation::insert_model(const model *parent,
					   const real_l x, const real_l y,
					   const real_l z, const bool frac,
					   char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a model insert operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::position_from_selections(real_l &x, real_l &y,
							real_l &z,
							char message[],
							const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->centre_from_selects(pending->get_model(), x, y, z,
					message, mlen);
}

DLVreturn_type DLV::operation::centre_from_selects(const model *, real_l &,
						   real_l &, real_l &,
						   char message[],
						   const int_g mlen)
{
  strncpy(message, "BUG: not a model centre operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::update_move_atoms(const bool frac,
						 const bool displace,
						 const real_l x,
						 const real_l y,
						 const real_l z,
						 char message[],
						 const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->move_atoms(pending->get_model(), x, y, z, frac, displace,
			       message, mlen);
}

DLVreturn_type DLV::operation::move_atoms(model *parent, const real_l x,
					  const real_l y, const real_l z,
					  const bool frac, const bool displace,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: not an atom move operation", mlen);
  return DLV_ERROR;
}

#endif // ENABLE_DLV_GRAPHICS
