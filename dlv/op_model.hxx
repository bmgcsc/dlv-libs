
#ifndef DLV_OP_MODEL
#define DLV_OP_MODEL

#ifdef WIN32
#  include <winsock2.h>
#  undef min
#  undef max
#endif // WIN32

namespace DLV {

  class change_geometry : public operation {
  public:
    bool is_geometry() const;

  protected:
    change_geometry(model *m);
    void inherit_model();
    void inherit_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit_geometry : public change_geometry {
  public:
    bool is_geom_edit() const;
    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    edit_geometry(model *m);
    void add_standard_data_objects();

    void map_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_model_op : public load_op {
  public:
    bool is_geometry() const;
    bool is_geom_load() const;
    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    load_model_op(model *m, const char file[]);

    static string name_from_file(const char name[], const char filename[]);
    static string name_from_file(const char name[], const char filename[], 
				 const char filename2[]);
    void inherit_model();
    void inherit_data();
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_atom_model_op : public load_model_op {
  protected:
    load_atom_model_op(model *m, const char file[]);

    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_cif_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool bonds, char message[],
			     const int_g mlen);

    // public for serialization
    load_cif_file(model *m, const char file[]);

  protected:
    string get_name() const;
    static model *read(std::istream &input, string def_name, bool &has_bonds,
		       const bool cds, char message[], const int_g mlen);

  private:
    static void expand_symbol(string &symbol);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS

  class cds_cif_socket : public load_cif_file {
  public:
    static operation *create(const bool bonds, char message[], const int_g len);

#ifdef WIN32
    static SOCKET start_listening();
#else
    static int_g start_listening();
#endif // WIN32
    static void stop_listening();
    static bool read(char message[], const int_g len);

    cds_cif_socket(model *m);

  protected:
    string get_name() const;

  private:
    static const int_g socket_number = 8023;
#ifdef WIN32
    static SOCKET socket_fd;
#else
    static int_g socket_fd;
#endif // WIN32
    static DLV::string data;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#endif // ENABLE_DLV_GRAPHICS

  class load_pdb_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const int_g model_index, const int_g conformer,
			     const int_g nconformers, const bool bonds,
			     char message[], const int_g mlen);
    static int_g count_models(const char filename[], char message[],
			    const int_g mlen);
    static int_g count_conformers(const char filename[], const int_g model,
				char message[], const int_g mlen);

    // public for serialization
    load_pdb_file(model *m, const char file[]);

  protected:
    string get_name() const;

    static model *read(const string def_name, const int_g model_index,
		       const int_g conformer, const int_g nconformers,
		       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class create_model_op : public change_geometry {
  public:
    create_model_op(model *m);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;

    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class create_atom_model_op : public create_model_op {
  public:
    create_atom_model_op(model *m);

  protected:
    DLVreturn_type add_atom(const int_g atn, const bool set_charge,
			    const int_g charge, const bool use_rad,
			    const real_g radius, const real_l x,
			    const real_l y, const real_l z, const bool rhomb,
			    char message[], const int_g mlen);
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::load_atom_model_op)
BOOST_CLASS_EXPORT_KEY(DLV::change_geometry)
BOOST_CLASS_EXPORT_KEY(DLV::edit_geometry)
BOOST_CLASS_EXPORT_KEY(DLV::load_model_op)
BOOST_CLASS_EXPORT_KEY(DLV::load_atom_model_op)
BOOST_CLASS_EXPORT_KEY(DLV::load_cif_file)
BOOST_CLASS_EXPORT_KEY(DLV::load_pdb_file)
BOOST_CLASS_EXPORT_KEY(DLV::create_model_op)
BOOST_CLASS_EXPORT_KEY(DLV::create_atom_model_op)
#endif // DLV_USES_SERIALIZE

inline DLV::change_geometry::change_geometry(model *m)
  : operation(m)
{
}

inline DLV::edit_geometry::edit_geometry(model *m)
  : change_geometry(m)
{
}

inline DLV::load_model_op::load_model_op(model *m, const char file[])
  : load_op(m, file)
{
}

inline DLV::load_atom_model_op::load_atom_model_op(model *m, const char file[])
  : load_model_op(m, file)
{
}

inline DLV::load_cif_file::load_cif_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::load_pdb_file::load_pdb_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::create_model_op::create_model_op(model *m) : change_geometry(m)
{
}

inline DLV::create_atom_model_op::create_atom_model_op(model *m)
  : create_model_op(m)
{
}

#  ifdef ENABLE_DLV_GRAPHICS

inline DLV::cds_cif_socket::cds_cif_socket(model *m)
  : load_cif_file(m, "")
{
}

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_OP_MODEL
