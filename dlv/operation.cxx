
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/calculations.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
//#include "atom_model.hxx"
#include "model.hxx"
#include "file.hxx"
#include "data_objs.hxx"
#include "operation.hxx"
//#include "op_model.hxx"
#include "project.hxx"
#include "job.hxx"

DLV::operation *DLV::operation::current = 0;
DLV::operation *DLV::operation::pending = 0;
DLV::operation *DLV::operation::editing = 0;
DLV::operation *DLV::operation::continue_edit = 0;
bool DLV::operation::is_pending = false;
bool DLV::operation::data_pending = false;
DLV::int_g DLV::operation::counter = 0;
DLV::int_g DLV::operation::size = 0;
DLV::op_list DLV::operation::top;

#ifdef ENABLE_DLV_JOB_THREADS

void DLV::operation::check_job()
{
  job *cur_job = get_job();
  if (cur_job != 0) {
    if (cur_job->status_has_changed()) {
      job_status_type status = cur_job->get_current_status();
#ifdef ENABLE_DLV_GRAPHICS
      update_job_status(job::get_status_info(status), get_job_number());
#endif // ENABLE_DLV_GRAPHICS
      // Todo - update status label widgets.
      cur_job->clear_status();
    }
  }
  op_list::const_iterator x;
  for (x = children.begin(); x != children.end(); ++x )
    x->second->check_job();
}

void DLV::operation::check_job_list()
{
  boost::mutex::scoped_lock *lock = DLV::job::acquire_status_lock();
  if (DLV::job::get_global_status()) {
    // search through job list
    op_list::iterator x;
    for (x = top.begin(); x != top.end(); ++x )
      x->second->check_job();
    DLV::job::clear_global_status();
  }
  delete lock;
}

void DLV::check_job_list(char *)
{
  operation::check_job_list();
}

#endif // ENABLE_DLV_JOB_THREADS

DLV::operation::operation()
  : date_stamp(current_date_time()), parent(0), rspace_view(false),
    kspace_view(false), display_index(0), edit_index(0), reload(false),
    dont_update(false)
{
}

DLV::operation::~operation()
{
  // Should call destructors for children/data objects.
  //#ifdef ENABLE_DLV_GRAPHICS
  //if (display_parent != 0)
  //  delete display_parent;
  //#endif // ENABLE_DLV_GRAPHICS
}

DLV::operation::operation(model *m)
  : date_stamp(current_date_time()), parent(0),
    structure(m), rspace_view(false), kspace_view(false),
    display_index(0), edit_index(0), reload(false), dont_update(false)
{
}

DLV::operation *DLV::operation::get_parent() const
{
  if (is_pending)
    return pending;
  else
    return parent;
}

void DLV::operation::clear_all()
{
  // Deletes everything.
  //top.~op_list();
  top.clear();
  counter = 0;
  size = 0;
  current = 0;
}

// Todo - combine common bits of these three methods.
void DLV::operation::attach_base(operation *op)
{
  op->index = counter;
  counter++;
  size++;
  top[op->index] = op;
  op->set_current();
  op->add_standard_data_objects();
}

void DLV::operation::attach_base_no_current()
{
  index = counter;
  counter++;
  size++;
  top[index] = this;
  //op->set_current();
  add_standard_data_objects();
}

void DLV::operation::attach_tree(operation *op)
{
  op->parent = current;
  op->index = counter;
  counter++;
  size++;
  current->children[op->index] = op;
  op->set_current();
}

void DLV::operation::attach_pending(const bool data)
{
  inherit_model();
  inherit_data();
  map_data();
  display_index = current->display_index;
  edit_index = current->edit_index;
  is_pending = true;
  pending = current;
  data_pending = data;
  current = this;
  editing = current;
  add_standard_data_objects();
}

void DLV::operation::attach_pending_copy_model(const string tag)
{
  // copy_model();
  const model *parent = current->get_model();
  string name = parent->get_model_name();
  name += " ";
  name += tag;
  /*
  model *m = model::create_atoms(name, parent->get_number_of_periodic_dims());
  m->copy_model(parent);
  */
  model *m = parent->copy_model(name);
  structure = shared_ptr<model>(m);
  map_data();
  //display_index = current->display_index;
  //edit_index = current->edit_index;
  is_pending = true;
  pending = current;
  data_pending = false;
  current = this;
  editing = current;
  add_standard_data_objects();
}

void DLV::operation::attach()
{
  parent = current;
  index = counter;
  counter++;
  size++;
  current->children[index] = this;
  inherit_model();
  inherit_data();
  map_data();
  display_index = current->display_index;
  edit_index = current->edit_index;
  set_current();
  add_standard_data_objects();
}

void DLV::operation::attach_no_inherit_model()
{
  parent = current;
  index = counter;
  counter++;
  size++;
  current->children[index] = this;
  //  inherit_model();
  //inherit_data();
  map_data();
  display_index = current->display_index;
  edit_index = current->edit_index;
  set_current();
  add_standard_data_objects();
}

void DLV::operation::attach_no_current()
{
  parent = current;
  index = counter;
  counter++;
  size++;
  current->children[index] = this;
  //inherit_model();
  inherit_data();
  map_data();
  display_index = current->display_index;
  edit_index = current->edit_index;
  //set_current();
  add_standard_data_objects();
}

void DLV::operation::replace_model(class model *m)
{
  structure->update_model(m);
  delete m;
#ifdef ENABLE_DLV_GRAPHICS
  char message[256];
  update_cell_info(message, 256);
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::operation::attach_model(class model *m)
{
  shared_ptr<model> p(m);
  structure = p;
}

void DLV::operation::inherit_model()
{
  structure = current->structure;
#ifdef ENABLE_DLV_GRAPHICS
  display_parent = current->display_parent;
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::operation::inherit_data()
{
  // inherit relevant data structures!
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      inherited_data.push_back(*x);
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      inherited_data.push_back(*x);
    }
  }
}

void DLV::operation::inherit_non_std_data()
{
  // inherit relevant data structures!
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      if (!(*x)->is_atom_bond_data() and !(*x)->is_lattice_direction())
	inherited_data.push_back(*x);
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      if (!(*x)->is_atom_bond_data() and !(*x)->is_lattice_direction())
	inherited_data.push_back(*x);
    }
  }
}

void DLV::operation::map_data()
{
  // Null op, as only need to map if the geometry changes.
}

void DLV::operation::attach_data(class data_object *data)
{
  shared_ptr<data_object> p(data);
  calculated_data.push_back(p);
#ifdef ENABLE_DLV_GRAPHICS
  if (display_parent != 0) {
    list_displayable_data(true);
    list_editable_data();
    }
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::operation::attach_data(shared_ptr<data_object> data)
{
  calculated_data.push_back(data);
#ifdef ENABLE_DLV_GRAPHICS
  if (display_parent != 0) {
    list_displayable_data(true);
    list_editable_data();
    }
#endif // ENABLE_DLV_GRAPHICS
}

void DLV::operation::detach_data(shared_ptr<data_object> data)
{
  calculated_data.remove(data);
}

DLVreturn_type DLV::operation::accept_pending(const bool is_edit,
					      const bool no_parent,
					      const bool set_cont)
{
  operation *op = editing;
#ifdef ENABLE_DLV_GRAPHICS
  op->deactivate_atom_flags();
  // make sure its an edit and not a new model
  if (is_edit and pending != 0) {
    char message[256];
    real_l x;
    real_l y;
    real_l z;
    op->transform_editor(pending->get_model(), false, x, y, z, message, 256);
  }
#endif // ENABLE_DLV_GRAPHICS
  if (set_cont)
    continue_edit = editing;
  current = pending;
  is_pending = false;
  pending = 0;
  editing = 0;
  data_pending = false;
  if (no_parent)
    attach_base(op);
  else
    attach_tree(op);
  return DLV_OK;
}

DLVreturn_type DLV::operation::accept_pending_no_attach(const bool set_cont)
{
  //operation *op = editing;
#ifdef ENABLE_DLV_GRAPHICS
  editing->deactivate_atom_flags();
#endif // ENABLE_DLV_GRAPHICS
  if (set_cont)
    continue_edit = editing;
  is_pending = false;
  pending = 0;
  editing = 0;
  data_pending = false;
  return DLV_OK;
}

void DLV::operation::detach_pending()
{
  if (is_pending) {
#ifdef ENABLE_DLV_GRAPHICS
    editing->deactivate_atom_flags();
#endif // ENABLE_DLV_GRAPHICS
    if (current == editing)
      current = pending;
    pending = 0;
  }
  continue_edit = 0;
}

DLVreturn_type DLV::operation::delete_pending()
{
  if (is_pending) {
    is_pending = false;
#ifdef ENABLE_DLV_GRAPHICS
    if (data_pending) {
      char stuff[64];
      editing->delete_display(editing->display_index, stuff, 64);
      current->list_displayable_data(true);
      current->list_editable_data();
    }
#endif // ENABLE_DLV_GRAPHICS
    data_pending = false;
    delete editing;
    editing = 0;
  }
  continue_edit = 0;
  return DLV_OK;
}

DLVreturn_type DLV::operation::cancel_pending()
{
  if (is_pending) {
#ifdef ENABLE_DLV_GRAPHICS
    editing->structure->deactivate_atom_flags();
    if (data_pending) {
      char stuff[64];
      editing->delete_display(editing->display_index, stuff, 64);
      pending->list_displayable_data(true);
      pending->list_editable_data();
    }
#endif // ENABLE_DLV_GRAPHICS
    data_pending = false;
    if (current == editing)
      current = pending;
    delete editing;
    is_pending = false;
    pending = 0;
    editing = 0;
  }
  continue_edit = 0;
  return DLV_OK;
}

bool DLV::operation::process_socket_data(char [], const bool)
{
  // BUG
  return false;
}

bool DLV::operation::will_process_socket() const
{
  return false;
}

bool DLV::operation::last_model()
{
  if (is_pending and pending == 0)
    return true;
  else
    return false;
}

void DLV::operation::add_path(string &) const
{
  // Do nothing
}

DLV::int_g DLV::operation::get_model_type() const
{
  return structure->get_model_type();
}

void DLV::operation::get_model_name(char name[], const int_g n) const
{
  structure->get_model_name(name, n);
}

DLV::string DLV::operation::get_model_name() const
{
  if (structure == 0)
    return "";
  else
    return structure->get_model_name();
}

DLV::string DLV::operation::get_sub_project_dir() const
{
  string dir = project::get_project_dir();
  const operation *op = this;
  while (op->parent != 0) {
    op = op->parent;
    if (op->is_subproject())
      op->add_path(dir);
  }
  return dir;
}

void DLV::operation::list_tree_all(string *names, int_g *ids, const int_g n,
				   int_g &count, const string indent)
{
  ids[count] = index;
  names[count] = indent + get_name();
  if (job_failed())
    names[count] += " (Failed)";
  count++;
  if (count == n)
    return;
  op_list::const_iterator op;
  for (op = children.begin(); op != children.end(); op++)
    op->second->list_tree_all(names, ids, n, count, indent + "  ");
}

void DLV::operation::list_tree_op(op_test test, string *names, int_g *ids,
				  const int_g n, const int_g selection,
				  int_g &count, const string indent)
{
  bool all = false;
  if ((this->*test)()) {
    ids[count] = index;
    names[count] = indent + get_name();
    if (job_failed())
      names[count] += " (Failed)";
    all = (count == selection);
    count++;
    if (count == n)
      return;
  }
  op_list::const_iterator op;
  if (all) {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_all(names, ids, n, count, indent + "  ");
  } else {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_op(test, names, ids, n, selection,
			       count, indent + "  ");
  }
}

void DLV::operation::list_tree_op1_or_op2(op_test test1, op_test test2,
					  string *names, int_g *ids,
					  const int_g n, const int_g selection,
					  int_g &count, const string indent)
{
  bool all = false;
  if ((this->*test1)() or (this->*test2)()) {
    ids[count] = index;
    names[count] = indent + get_name();
    if (job_failed())
      names[count] += " (Failed)";
    all = (count == selection);
    count++;
    if (count == n)
      return;
  }
  op_list::const_iterator op;
  if (all) {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_all(names, ids, n, count, indent + "  ");
  } else {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_op1_or_op2(test1, test2, names, ids, n,
				       selection, count, indent + "  ");
  }
}

void DLV::operation::list_tree_op1_and_op2(op_test test1, op_test test2,
					   string *names, int_g *ids,
					   const int_g n, const int_g selection,
					   int_g &count, const string indent)
{
  bool all = false;
  if ((this->*test1)() and (this->*test2)()) {
    ids[count] = index;
    names[count] = indent + get_name();
    if (job_failed())
      names[count] += " (Failed)";
    all = (count == selection);
    count++;
    if (count == n)
      return;
  }
  op_list::const_iterator op;
  if (all) {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_all(names, ids, n, count, indent + "  ");
  } else {
    for (op = children.begin(); op != children.end(); op++)
      op->second->list_tree_op1_and_op2(test1, test2, names, ids, n,
					selection, count, indent + "  ");
  }
}

DLV::int_g DLV::operation::list_all(string *names, int_g *ids, const int_g n)
{
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_all(names, ids, n, count, "");
  return count;
}

DLV::int_g DLV::operation::list_op(op_test test, string *names, int_g *ids,
				   const int_g n, const int_g selection)
{
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_op(test, names, ids, n, selection, count, "");
  return count;
}

DLV::int_g DLV::operation::list_op1_or_op2(op_test test1, op_test test2,
					   string *names, int_g *ids,
					   const int_g n, const int_g selection)
{
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_op1_or_op2(test1, test2, names, ids, n, selection,
				     count, "");
  return count;
}

DLV::int_g DLV::operation::list_op1_and_op2(op_test test1, op_test test2,
					    string *names, int_g *ids,
					    const int_g n,
					    const int_g selection)
{
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_op1_and_op2(test1, test2, names, ids, n,
				      selection, count, "");
  return count;
}

void DLV::operation::list_tree_model(string *names, const int model_type,
				     int_g &count)
{
  if (is_geometry()) {
    if (get_model_type() == model_type) {
      if (names != 0)
	names[count] = get_name();
      count++;
    }
  }
  op_list::const_iterator op;
  for (op = children.begin(); op != children.end(); op++)
    op->second->list_tree_model(names, model_type, count);
}

DLV::int_g DLV::operation::list_models(string * &names, const int model_type)
{
  names = 0;
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_model(names, model_type, count);
  if (count > 0) {
    names = new string[count];
    count = 0;
    for (op = top.begin(); op != top.end(); op++)
      op->second->list_tree_model(names, model_type, count);
  }
  return count;
}

DLV::int_g DLV::operation::compare_neb_structures(int_g limit)
{
    operation* op;
    if(limit==9999){
	op = current;
	if(structure == pending->structure) return 2;
    }
    else {
	op = editing;
    }
    if (get_model_type() != op->get_model_type()) return 0;
    if(structure->get_number_of_primitive_atoms()
       !=op->structure->get_number_of_primitive_atoms()) return 0;
    if(structure->get_number_of_sym_ops()
       != op->structure->get_number_of_sym_ops()) return 0;
    DLV::coord_type a1, b1, c1, alpha1, beta1, gamma1;
    DLV::coord_type a2, b2, c2, alpha2, beta2, gamma2;
    structure->get_lattice_parameters(a1, b1, c1, alpha1, beta1, gamma1);
    op->structure->get_lattice_parameters(a2, b2, c2, alpha2, beta2, gamma2);
    if((a1!=a2)||(b1!=b2)||(c1!=c2)||(alpha1!=alpha2)
       ||(beta1!=beta2)||(gamma1!=gamma2)) return 0;
    const int_g len = 300;
    int_g count1[len];
    int_g count2[len];
    op->structure->count_crystal03_ids(count2, len);
    structure->count_crystal03_ids(count1, len);
    for(int_g i=0;i<len;i++) if(count1[i]!=count2[i]) return 0;
    return 1;
}

void DLV::operation::list_tree_neb(string *names, int_g &count, int_g limit)
{
    if (is_geom_edit() or is_geometry()){
	if(this->compare_neb_structures(limit)==1){
	  if(count>limit) return;
	  if (names != 0)
	      names[count] = get_name();
	  count++;
      }
  }
  op_list::const_iterator op;
  for (op = children.begin(); op != children.end(); op++)
    op->second->list_tree_neb(names, count, limit);
}

DLV::int_g DLV::operation::get_neb_first_model()
{
  int_g count = 0;
  bool success = false;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++){
      op->second->list_tree_neb_first_model(count, success);
      if(success) return count;
  }
  return -1;
}

void DLV::operation::list_tree_neb_first_model(int_g &count, bool &success)
{
   if(success==true) return;
   if (is_geom_edit() or is_geometry()){
	if(compare_neb_structures(9999)== 1) count++;
	if(compare_neb_structures(9999)== 2) {
	    success = true;
	    return;
	}
    }
   op_list::const_iterator op;
   for (op = children.begin(); op != children.end(); op++)
       op->second->list_tree_neb_first_model(count, success);
  return;
}

DLV::int_g DLV::operation::list_neb_models(string * &names, char message[],
					   const int_g len)
{
  names = 0;
  int_g count = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++)
    op->second->list_tree_neb(names, count);
  if (count > 0) {
    names = new string[count];
    count = 0;
    for (op = top.begin(); op != top.end(); op++)
      op->second->list_tree_neb(names, count);
  }
  else strncpy(message, "No suitable models available", len);
  return count;
}

DLV::operation *DLV::operation::get_neb_op(int_g nmodel)
{
  int_g count = 0;
  DLV::operation *nebop = 0;
  op_list::const_iterator op;
  for (op = top.begin(); op != top.end(); op++) {
      op->second->list_tree_neb_op(count, nmodel, nebop);
  }
  return nebop;
}

void DLV::operation::list_tree_neb_op(int_g &count, int_g limit,
				      DLV::operation* &nebop)
{
  if(count>limit) return;
  if (is_geom_edit() or is_geometry()){
    if(this->compare_neb_structures(limit)==1){
      count++;
      if(count>limit) {
	  nebop = this;
	return;
      }
    }
  }
  op_list::const_iterator op;
  for (op = children.begin(); op != children.end(); op++)
    op->second->list_tree_neb_op(count, limit, nebop);
}

bool DLV::operation::is_all() const
{
  return true;
}

bool DLV::operation::is_subproject() const
{
  return false;
}

bool DLV::operation::is_geometry() const
{
  return false;
}

bool DLV::operation::is_geom_edit() const
{
  return false;
}

bool DLV::operation::is_geom_load() const
{
  return false;
}

bool DLV::operation::is_data_load() const
{
  return false;
}

bool DLV::operation::is_calc() const
{
  return false;
}

bool DLV::operation::copy_cell_replicate() const
{
  return true;
}

bool DLV::operation::is_supercell() const
{
  return false;
}

bool DLV::operation::is_property_change() const
{
  return false;
}

bool DLV::operation::open_new_view() const
{
  return false;
}

bool DLV::operation::job_failed() const
{
  return false;
}

DLV::operation *DLV::operation::find_bottom_child()
{
  // We need the lowest non-geometry operation for the selection so
  // that the data/display lists in the model will make sense.
  // What happens/is it possible to have more than 1 non-geom child op?
  op_list::const_iterator op;
  // Todo - currently all is_geom_load => is_geometry so I think its ok
  // extra is_geom_edit is due to atom_props.
  for (op = children.begin(); op != children.end(); op++) {
    if (!(op->second->is_subproject() or op->second->is_geometry() or
	  op->second->is_geom_edit())) {
      if (op->second->children.size() > 0)
	return op->second->find_bottom_child();
      else
	return op->second;
    }
  }
  return this;
}

// Todo - if the tree gets really big this won't be very efficient
DLV::operation *DLV::operation::search_tree(const int_g n) const
{
  operation *ptr = 0;
  op_list::const_iterator op = children.find(n);
  if (op == children.end()) {
    for (op = children.begin(); op != children.end(); op++) {
      ptr = op->second->search_tree(n);
      if (ptr != 0)
	break;
    }
  } else
    ptr = op->second;
  return ptr;
}

DLV::operation *DLV::operation::select(const int_g n)
{
  operation *ptr = 0;
  op_list::const_iterator op = top.find(n);
  if (op == top.end()) {
    for (op = top.begin(); op != top.end(); op++) {
      ptr = op->second->search_tree(n);
      if (ptr != 0)
	break;
    }
  } else
    ptr = op->second;
  if (ptr != 0) {
    if (ptr->children.size() > 0)
      ptr = ptr->find_bottom_child();
    ptr->set_current();
  }
  return ptr;
}

const DLV::operation *DLV::operation::search_model(const int_g n,
						   const int_g model_type,
						   int_g &count) const
{
  if (is_geom_edit() or is_geometry()) {
    if (get_model_type() == model_type) {
      if (count == n)
	return this;
      else
	count++;
    }
  }
  op_list::const_iterator op;
  const operation *ptr = 0;
  for (op = children.begin(); op != children.end(); op++) {
    ptr = op->second->search_model(n, model_type, count);
    if (ptr != 0)
      return ptr;
  }
  return 0;
}

const DLV::operation *DLV::operation::select_model(const int_g n,
						   const int_g model_type,
						   char message[],
						   const int_g mlen)
{
  int_g count = 0;
  op_list::const_iterator op;
  const operation *ptr = 0;
  for (op = top.begin(); op != top.end(); op++) {
    ptr = op->second->search_model(n, model_type, count);
    if (ptr != 0)
      return ptr;
  }
  strncpy(message, "Failed to locate model", mlen);
  return 0;
}

void DLV::operation::remove_data(shared_ptr<data_object> data)
{
  std::list< shared_ptr<data_object> >::iterator p;
  bool found = false;
  for (p = calculated_data.begin(); p != calculated_data.end(); p++) {
    if (*p == data) {
      calculated_data.erase(p);
      found = true;
      break;
    }
  }
  if (!found) {
    for (p = mappable_data.begin(); p != mappable_data.end(); p++) {
      if (*p == data) {
	mappable_data.erase(p);
	found = true;
	break;
      }
    }
  }
  // Can't be in children if it wasn't in this one
  if (found) {
    op_list::iterator child;
    for (child = children.begin(); child != children.end(); child++)
      child->second->remove_data(data);
  }
}

bool DLV::operation::remove_tree(const int_g n, const bool all_refs)
{
  op_list::iterator op = children.find(n);
  if (op == children.end()) {
    for (op = children.begin(); op != children.end(); op++) {
      if (op->second->remove_tree(n, all_refs))
	return true;
    }
  } else {
    operation *ptr = op->second;
    op_list::iterator child;
    if (all_refs) {
      // remove all references to calculation_data in children...
      std::list< shared_ptr<data_object> >::iterator p;
      for (p = calculated_data.begin(); p != calculated_data.end(); p++) {
	for (child = ptr->children.begin();
	     child != ptr->children.end(); child++)
	  child->second->remove_data((*p));
	calculated_data.erase(p);
      }
    }
    // Add its children to tree it was part of
    for (child = ptr->children.begin();
	 child != ptr->children.end(); child++) {
      // I think this is ok - It should just destroy the map object, rather
      // than destroying the object pointed to.
      children[child->second->index] = child->second;
      child->second->parent = ptr->parent;
      ptr->children.erase(child);
    }
    children.erase(op);
    size--;
    return true;
  }
  return false;
}

void DLV::operation::remove(const int_g n, const bool all_refs)
{
  op_list::iterator op = top.find(n);
  if (op == top.end()) {
    for (op = top.begin(); op != top.end(); op++) {
      if (op->second->remove_tree(n, all_refs))
	break;
    }
  } else {
    operation *ptr = op->second;
    op_list::iterator child;
    if (all_refs) {
      // remove all references to calculation_data in children...
      std::list< shared_ptr<data_object> >::iterator p;
      for (p = ptr->calculated_data.begin();
	   p != ptr->calculated_data.end(); p++) {
	for (child = ptr->children.begin();
	     child != ptr->children.end(); child++)
	  child->second->remove_data((*p));
	ptr->calculated_data.erase(p);
      }
    }
    // Add its children to tree it was part of
    for (child = ptr->children.begin();
	 child != ptr->children.end(); child++) {
      // I think this is ok - It should just destroy the map object, rather
      // than destroying the object pointed to.
      top[child->second->index] = child->second;
      child->second->parent = ptr->parent;
      ptr->children.erase(child);
    }
    top.erase(op);
    size--;
  }
}

void DLV::operation::get_current_lattice(int_g &c, int_g &l)
{
  c = current->get_model()->get_lattice_centring();
  l = current->get_model()->get_lattice_type();
}

#ifdef DLV_USES_SERIALIZE

void DLV::operation::load(std::ifstream &input)
{
  int_g n;
  input >> n;
  input >> counter;
  input >> size;
  char buff[128];
  input.getline(buff, 128);
  boost::archive::text_iarchive ar(input);
  ar & top;
  select(n);
  // Todo - reindex from size?
}

void DLV::operation::save(std::ofstream &output)
{
  output << current->index << ' ';
  output << counter << ' ';
  output << size << ' ';
  output << '\n';
  boost::archive::text_oarchive ar(output);
  ar & top;
}

#endif // DLV_USES_SERIALIZE

DLV::edit_data_op::edit_data_op()
{
}

bool DLV::edit_data_op::reload_data(class data_object *, char message[],
				    const int_g mlen)
{
  strncpy(message, "edit_data reload not implemented - Todo", mlen);
  return false;
}

DLV::string DLV::edit_data_op::get_name() const
{
  return "Edit data";
}

void DLV::edit_data_op::add_standard_data_objects()
{
  // Do nothing
}

// Todo what should this be
// These 3 are inlinable, at the cost of operation.hxx depending on file.hxx
DLV::load_op::load_op(const char file[])
 : file(file_obj::create("", file, true))
{
}

DLV::load_op::load_op(model *m, const char file[])
  : operation(m), file(file_obj::create("", file, true))
{
}

DLV::string DLV::load_op::get_filename() const
{
  return file->get_filename();
}

DLVreturn_type DLV::operation::add_atom(const int, const bool,
					const int, const bool,
					const real_g, const real_l,
					const real_l, const real_l,
					const bool, char message[],
					const int_g mlen)
{
  strncpy(message, "BUG: incorrect op type for add atom", mlen);
  return DLV_ERROR;
}

DLV::int_g DLV::operation::get_job_number() const
{
  return -1;
}

DLV::job *DLV::operation::get_job() const
{
  return 0;
}

bool DLV::operation::find_job(job * &cur_job, operation * &op,
			      const int_g index)
{
  if (get_job_number() == index) {
    cur_job = get_job();
    op = this;
    return true;
  } else {
    op_list::const_iterator x;
    for (x = children.begin(); x != children.end(); ++x ) {
      if (x->second->find_job(cur_job, op, index))
	return true;
    }
  }
  return false;
}

DLV::job *DLV::operation::find_job(const int_g index, operation * &op)
{
  op_list::iterator x;
  job *cur_job = 0;
  for (x = top.begin(); x != top.end(); ++x ) {
    if (x->second->find_job(cur_job, op, index))
      break;
  }
  return cur_job;
}

DLVreturn_type DLV::operation::recover_job(char message[], const int_g mlen)
{
  strncpy(message, "BUG: killing job for non-calc operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::remove_job(char message[], const int_g mlen)
{
  strncpy(message, "BUG: removing job from non-calc operation", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::operation::kill_job(char message[], const int_g mlen)
{
  strncpy(message, "BUG: killing job for non-calc operation", mlen);
  return DLV_ERROR;
}

DLV::operation *DLV::operation::find_parent(const string datatype,
					    const string program)
{
  DLV::operation *op = this;
  //while (op->parent != 0) {
  while (op != 0) {
    //op = op->parent;
    std::list< shared_ptr<data_object> >::const_iterator x;
    if (op->calculated_data.size() > 0) {
      for (x = op->calculated_data.begin();
	   x != op->calculated_data.end(); x++ ) {
	if ((*x)->match_type(datatype, program))
	  return op;
      }
    }
    op = op->parent;
  }
  return 0;
}

DLV::operation *DLV::operation::find_parent_geometry(real_l r[3][3],
						     bool &ok)
{
  operation *op = current;
  if (pending != 0)
    op = pending;
  while (op != 0) {
    if (op->is_geom_edit()) {
      ok = op->get_lattice_rotation(r);
      return op->parent;
    }
    op = op->parent;
  }
  return 0;
}

DLV::operation *
DLV::operation::find_current_surface_parent(int_g supercell[2][2])
{
  const operation *op = current;
  while (op != 0) {
    if (op->is_supercell()) {
      op->get_supercell(supercell);
      return op->parent;
    }
    op = op->parent;
  }
  return 0;
}

/*
DLV::operation *DLV::operation::find_surface_parent(int_g supercell[2][2]) const
{
  const operation *op = this;
  //bool ok = false;
  while (op != 0) {
    if (op->is_supercell()) {
      op->get_supercell(supercell);
      return op->parent;
    }
    op = op->parent;
  }
  return 0;
}
*/

void DLV::operation::get_supercell(int_g supercell[2][2]) const
{
  // BUG
}

bool DLV::operation::get_lattice_rotation(real_l r[3][3]) const
{
  return false;
}

void DLV::operation::generate_transforms(const int_g na, const int_g nb,
					 const int_g nc, const int_g sa,
					 const int_g sb, const int_g sc,
					 int_g &n, real_g (* &transforms)[3])
{
  const DLV::model *m = get_current_model();
  m->generate_transforms(na, nb, nc, sa, sb, sc, false, false, n, transforms);
}

void DLV::operation::set_bond_all()
{
  structure->set_bond_all();
}

const DLV::data_object *DLV::operation::find_data(const string datatype) const
{
  // Todo
  return 0;
}

DLV::data_object *DLV::operation::find_data(const string datatype,
					    const string program) const
{
  data_object *data = 0;
  std::list< shared_ptr<data_object> >::const_iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      // by not breaking we find the most recent data item.
      if ((*x)->match_type(datatype, program))
	data = x->get();
    }
  }
  if (data == 0) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	// by not breaking we find the most recent data item.
	if ((*x)->match_type(datatype, program))
	  data = x->get();
      }
    }
  }
  return data;
}

DLV::shared_ptr<DLV::data_object>
DLV::operation::find_shared_data(const string datatype,
				 const string program) const
{
  shared_ptr<data_object> data;
  std::list< shared_ptr<data_object> >::const_iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      // by not breaking we find the most recent data item.
      if ((*x)->match_type(datatype, program))
	data = *x;
    }
  }
  if (data == 0) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	// by not breaking we find the most recent data item.
	if ((*x)->match_type(datatype, program))
	  data = *x;
      }
    }
  }
  return data;
}

DLV::data_object *DLV::operation::find_wulff(const int_g selection)
{
  int_g n = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      if ((*x)->is_wulff()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      if ((*x)->is_wulff()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  return 0;
}

DLVreturn_type DLV::operation::recover_current_job(const int_g index,
						   const bool use_list,
						   operation * &op,
						   char message[],
						   const int_g mlen)
{
  // Todo use_list
  op = 0;
  operation *my_op = 0;
  job *cur_job = find_job(index, my_op);
  if (cur_job == 0) {
    strncpy(message, "Unable to locate job", mlen);
    return DLV_ERROR;
  } else {
    cur_job->reset_user_status();
    if (my_op->is_geometry())
      op = my_op;
    return my_op->recover_job(message, mlen);
  }
}

DLVreturn_type DLV::operation::delete_current_job(const int_g index,
						  char message[],
						  const int_g mlen)
{
  operation *op = 0;
  job *cur_job = find_job(index, op);
  if (cur_job == 0) {
    strncpy(message, "Unable to locate job", mlen);
    return DLV_ERROR;
  } else
    return op->remove_job(message, mlen);
}

#ifdef ENABLE_DLV_GRAPHICS

bool DLV::operation::continue_has_selections()
{
  if (continue_edit == 0)
    return current->structure->has_selected_atoms();
  else
    return continue_edit->structure->has_selected_atoms();
}

DLVreturn_type DLV::operation::create_atom_group(const char label[],
						 const char group[],
						 const bool all,
						 const char remainder[],
						 char message[],
						 const int_g mlen)
{
  DLV::model *m = get_current_model();
  DLVreturn_type ok = m->create_atom_group(label, group, all,
					   remainder, message, mlen);
  if (ok != DLV_ERROR)
    m->list_atom_groupings(current->display_parent.get());
  return ok;
}

DLVreturn_type DLV::operation::update_cluster_region(const real_g radii[],
						     const int_g nradii,
						     const char label[],
						     char message[],
						     const int_g mlen)
{
  DLV::model *m = get_current_model();
  DLVreturn_type ok = m->update_cluster_region(radii, nradii, label,
					       message, mlen);
  if (ok != DLV_ERROR) {
    m->activate_atom_flags("Cluster Regions", false);
    m->list_atom_groupings(current->display_parent.get());
  }
  return ok;
}

DLVreturn_type DLV::operation::accept_cluster_region(char message[],
						     const int_g mlen)
{
  current->deactivate_atom_flags();
  return DLV_OK;
}

DLVreturn_type DLV::operation::cancel_cluster_region(const char label[],
						     char message[],
						     const int_g mlen)
{
  current->deactivate_atom_flags();
  DLV::model *m = get_current_model();
  m->clear_cluster_region(label, message, mlen);
  m->list_atom_groupings(current->display_parent.get());
  return DLV_OK;
}

void DLV::operation::render_data(class data_object *data)
{
  if (display_parent != 0) {
    char message[256];
    display_index++;
    data->render(display_parent.get(), structure.get(), 0, 0, display_index,
		 message, 256);
  }
}

void DLV::operation::transfer_and_render_data(DLV::operation *op,
					     class data_object *data)
{
  if (display_parent != 0) {
    char message[256];
    display_index++;
    class data_object *old_data = op->find_data(data->get_data_label(),
    						data->get_program_name());
    if(old_data != 0)
      data->transfer_and_render(old_data, display_parent.get(),
				message, 256);
  }
}

DLVreturn_type DLV::operation::render_r(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  bool inherit = false;
  if (display_parent == 0) {
    render_parent *rp = 0;
    if (is_pending and is_geom_edit()) {
      inherit = true;
      rp = structure->create_render_parent(pending->display_parent.get(),
					   copy_cell_replicate(),
					   is_property_change());
    } else
      rp = structure->create_render_parent();
    shared_ptr<render_parent> bp(rp);
    display_parent = bp;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin();
	 x != inherited_data.end(); x++ ) {
      if (!(ok = (*x)->render(display_parent.get(), structure.get(),
			      message, mlen)))
	break;
    }
  }
  if (ok) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin();
	   x != calculated_data.end(); x++ ) {
	if (!(ok = (*x)->render(display_parent.get(), structure.get(),
				message, mlen)))
	  break;
      }
    }
    if (ok) {
      if (inherit)
	ok = structure->render_r(display_parent.get(),
				 pending->structure.get(), message, mlen);
      else
	ok = structure->render_r(display_parent.get(), 0, message, mlen);
    }
  }
  return ok;
}

DLVreturn_type DLV::operation::render_k(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (display_parent == 0) {
    render_parent *rp = 0;
    if (is_pending and is_geom_edit())
      rp = structure->create_render_parent(pending->display_parent.get());
    else
      rp = structure->create_render_parent();
    shared_ptr<render_parent> bp(rp);
    display_parent = bp;
  }
  ok = structure->render_k(display_parent.get(), message, mlen);
  return ok;
}

void DLV::operation::atom_selections_changed()
{
}

DLVreturn_type DLV::operation::update_atom_selection_info(char message[],
							  const int_g mlen)
{
  // update objects
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if (!(*x)->update_atom_selection_info(structure.get(), message, mlen))
	return DLV_ERROR;
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if (!(*x)->update_atom_selection_info(structure.get(), message, mlen))
	return DLV_ERROR;
    }
  }
  if (is_pending)
    atom_selections_changed();
  return DLV_OK;
}

DLVreturn_type DLV::operation::update_cell_info(char message[],
						const int_g mlen)
{
  // update objects
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if (!(*x)->update_cell_info(structure.get(), message, mlen))
	return DLV_ERROR;
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if (!(*x)->update_cell_info(structure.get(), message, mlen))
	return DLV_ERROR;
    }
  }
  return DLV_OK;
}

DLV::operation *DLV::operation::edit_data(const int_g object,
					  const int_g component,
					  const int_g method,
					  char message[], const int_g mlen)
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  bool found = false;
  message[0] = '\0';
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_editable()) {
	if (count == object) {
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_editable()) {
	  if (count == object) {
	    found = true;
	    break;
	  }
	  count++;
	}
      }
    }
  }
  operation *op = 0;
  if (found) {
    edit_index++;
    data_object *ptr = (*x)->edit(display_parent.get(), structure.get(),
				  component, method, edit_index,
				  message, mlen);
    if (ptr != 0) {
      op = new edit_data_op;
      op->attach();
      op->attach_data(ptr);
    }
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "BUG: (edit) data object not found", mlen);
  }
  return op;
}

DLV::data_object *DLV::operation::find_edit(const int_g object)
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_edited()) {
	if ((*x)->check_edit_index(object)) {
	  return x->get();
	  break;
	}
	count++;
      }
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_edited()) {
	if ((*x)->check_edit_index(object)) {
	  return x->get();
	  break;
	}
	count++;
      }
    }
  }
  return 0;
}

DLVreturn_type DLV::operation::select_edited(const int_g object,
					     char message[], const int_g mlen)
{
  /*int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  bool found = false;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_edited()) {
	if ((*x)->check_edit_index(object)) {
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_edited()) {
	  if ((*x)->check_edit_index(object)) {
	    found = true;
	    break;
	  }
	  count++;
	}
      }
    }
  }
  if (found)
    return (*x)->select(message, mlen);
  else {
    strncpy(message, "BUG: edit object not found", mlen);
    return DLV_ERROR;
    }*/
  DLV::data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: edit object not found", mlen);
    return DLV_ERROR;
  } else
    return obj->select(message, mlen);
}

DLV::operation *DLV::operation::edit_current_data(const int_g object,
						  const int_g component,
						  const int_g method,
						  char message[],
						  const int_g mlen)
{
  return current->edit_data(object, component, method, message, mlen);
}

DLVreturn_type DLV::operation::select_current_edit(const int_g object,
						   char message[],
						   const int_g mlen)
{
  return current->select_edited(object, message, mlen);
}

#  if defined(DLV_USES_AVS_GRAPHICS) || defined(DLV_USES_VTK_GRAPHICS)

DLVreturn_type DLV::operation::detach_from_rviewer(class VIEWER3D *view)
{
  rspace_view = false;
  if (display_parent == 0)
    return DLV_ERROR;
  else
    return display_parent->detach_r3D(view);
}

DLVreturn_type DLV::operation::detach_from_kviewer(class VIEWER3D *view)
{
  kspace_view = false;
  if (display_parent == 0)
    return DLV_ERROR;
  else
    return display_parent->detach_k3D(view);
}

DLVreturn_type DLV::operation::attach_to_kviewer(class VIEWER3D *view)
{
  kspace_view = true;
  if (display_parent == 0)
    return DLV_ERROR;
  else
    return display_parent->attach_k3D(view);
}

DLVreturn_type DLV::operation::attach_to_rviewer(class VIEWER3D *view)
{
  rspace_view = true;
  if (display_parent == 0)
    return DLV_ERROR;
  else
    return display_parent->attach_r3D(view);
}

#  endif //GRAPHICS

DLVreturn_type DLV::operation::attach_rui()
{
  list_displayable_data();
  list_editable_data();
  structure->list_atom_groupings(display_parent.get());
  if (reload) {
    list_edited_data();
    list_displayed_data();
    reload = false;
  }
  if (display_parent == 0)
    return DLV_ERROR;
  else {
    int_g i = structure->get_symmetry_selector();
    toolkit_obj obj = structure->get_r_ui_obj();
    dont_update = true;
    DLVreturn_type ok = display_parent->attach_rui(obj);
    structure->fix_symmetry_selector(i);
    dont_update = false;
    attach_updatable_data();
    return ok;
  }
}

DLVreturn_type DLV::operation::attach_kui()
{
  list_displayable_data();
  list_editable_data();
  if (reload) {
    list_edited_data();
    list_displayed_data();
    reload = false;
  }
  if (display_parent == 0)
    return DLV_ERROR;
  else {
    toolkit_obj obj = structure->get_k_ui_obj();
    return display_parent->attach_kui(obj);
  }
}

void DLV::operation::attach_updatable_data()
{
  // attach UI for objects that may update due to selections
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ )
      (*x)->attach_updatable();
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ )
      (*x)->attach_updatable();
  }
}

void DLV::operation::list_displayable_data(const bool set_data)
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ )
      if ((*x)->is_displayable())
	count++;
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ )
      if ((*x)->is_displayable())
	count++;
  }
  bool show = false;
  if (count > 0) {
    display_parent->set_data_size(count);
    count = 0;
    if (inherited_data.size() > 0) {
      for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
	if ((*x)->is_displayable()) {
	  display_parent->set_data_label((*x)->get_data_label(), count);
	  display_parent->set_display_type((*x)->get_display_type(), count);
	  int_g n = (*x)->get_number_data_sets();
	  display_parent->set_sub_data_size(n, count);
	  for (int_g i = 0; i < n; i++) {
	    display_parent->set_sub_data_label((*x)->get_sub_data_label(i),
					       i, count);
	    display_parent->set_sub_data_vector((*x)->get_sub_is_vector(i),
						i, count);
	  }
	  count++;
	  show = show or (*x)->show_panel();
	}
      }
    }
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_displayable()) {
	  display_parent->set_data_label((*x)->get_data_label(), count);
	  display_parent->set_display_type((*x)->get_display_type(), count);
	  int_g n = (*x)->get_number_data_sets();
	  display_parent->set_sub_data_size(n, count);
	  for (int_g i = 0; i < n; i++) {
	    display_parent->set_sub_data_label((*x)->get_sub_data_label(i),
					       i, count);
	    display_parent->set_sub_data_vector((*x)->get_sub_is_vector(i),
						i, count);
	  }
	  count++;
	  show = show or (*x)->show_panel();
	}
      }
    }
    if (show) {
      display_parent->show_data_panel();
      if (set_data)
	display_parent->select_data_object(count - 1);
    }
  }
}

void DLV::operation::list_editable_data()
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ )
      if ((*x)->is_editable())
	count++;
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ )
      if ((*x)->is_editable())
	count++;
  }
  display_parent->set_edit_size(count);
  count = 0;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_editable()) {
	display_parent->set_edit_label((*x)->get_edit_label(), count);
	display_parent->set_edit_type((*x)->get_edit_type(), count);
	int_g n = (*x)->get_number_data_sets();
	display_parent->set_sub_edit_size(n, count);
	for (int_g i = 0; i < n; i++) {
	  display_parent->set_sub_edit_label((*x)->get_sub_data_label(i),
					     i, count);
	  display_parent->set_sub_edit_vector((*x)->get_sub_is_vector(i),
					      i, count);
	}
	count++;
      }
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_editable()) {
	display_parent->set_edit_label((*x)->get_data_label(), count);
	display_parent->set_edit_type((*x)->get_edit_type(), count);
	int_g n = (*x)->get_number_data_sets();
	display_parent->set_sub_edit_size(n, count);
	for (int_g i = 0; i < n; i++) {
	  display_parent->set_sub_edit_label((*x)->get_sub_data_label(i),
					     i, count);
	  display_parent->set_sub_edit_vector((*x)->get_sub_is_vector(i),
					      i, count);
	}
	count++;
      }
    }
  }
}

void DLV::operation::list_edited_data()
{
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_edited())
	(*x)->list_edit_object(display_parent.get());
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_edited())
	(*x)->list_edit_object(display_parent.get());
    }
  }
}

void DLV::operation::list_displayed_data()
{
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_displayable())
	(*x)->list_display_objects(display_parent.get());
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_displayable())
	(*x)->list_display_objects(display_parent.get());
    }
  }
}

DLVreturn_type DLV::operation::display_data(const int_g object,
					    const int_g component,
					    const int_g method, bool &kspace,
					    char message[], const int_g mlen)
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  bool found = false;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_displayable()) {
	if (count == object) {
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_displayable()) {
	  if (count == object) {
	    found = true;
	    break;
	  }
	  count++;
	}
      }
    }
  }
  if (found) {
    //int_g n = count_display_objects();
    display_index++;
    //kspace = (*x)->is_kspace();
    return (*x)->render(display_parent.get(), structure.get(), component,
			method, display_index, message, mlen);
  } else {
    strncpy(message, "BUG: data object not found", mlen);
    return DLV_ERROR;
  }
}

DLV::operation *DLV::operation::find_calc_data(shared_ptr<data_object> p,
					       const bool erase)
{
  // Todo - it shouldn't be possible for parent to be null?
  bool found = false;
  operation *op = this;
  std::list< shared_ptr<data_object> >::iterator x;
  while (!found) {
    op = op->parent;
    if (calculated_data.size() > 0) {
      for (x = op->calculated_data.begin();
	   x != op->calculated_data.end(); x++ ) {
	if (*x == p) {
	  found = true;
	  break;
	}
      }
    }
  }
  if (erase)
    op->calculated_data.erase(x);
  return op;  
}

void DLV::operation::delete_child_data(shared_ptr<data_object> p)
{
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if (*x == p) {
	inherited_data.erase(x);
	break;
      }
    }
  }
  std::map<int, operation *>::iterator ptr;
  for (ptr = children.begin(); ptr != children.end(); ++ptr)
    ptr->second->delete_child_data(p);
}

DLVreturn_type DLV::operation::delete_data(const int_g object, bool &kspace,
					   char message[], const int_g mlen)
{
  // Todo - should we also try to erase the op?
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  bool found = false;
  bool calc = false;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_displayable()) {
	if (count == object) {
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found) {
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_displayable()) {
	  if (count == object) {
	    found = true;
	    calc = true;
	    break;
	  }
	  count++;
	}
      }
    }
  }
  if (found) {
    // delete display objects
    int_g first = 0;
    int_g last = 0;
    DLVreturn_type ok = (*x)->remove_display(display_parent.get(),
					     structure.get(), first, last,
					     message, mlen);
    if (count_display_objects() == 0)
      display_parent->empty_display_list();
    else if (first >= 0)
      display_parent->reset_display_list(first, last);
    // delete drawable - if not used elsewhere by edit?
    // probably not needed as destructor should do this
    bool edited = (*x)->object_is_edited();
    if (edited) {
      strncpy(message, "Data is edited, not deleting completely", mlen);
      ok = DLV_WARNING;
    } else
      (*x)->delete_drawable();
    operation *op = this;
    // retain a link to the shared pointer to avoid destruction,
    // this wouldn't be needed if edited_obj used a shared_ptr
    // rather than an actual pointer (which is a flaw/bug).
    if (calc) {
      if (!edited)
	calculated_data.erase(x);
    } else
      op = find_calc_data(*x, !edited);
    // delete inherited data from children
    std::map<int, operation *>::iterator optr;
    for (optr = op->children.begin(); optr != op->children.end(); ++optr)
      optr->second->delete_child_data(*x);
    list_displayable_data(true);
    list_editable_data();
    // no need to delete data_object since if all refs to shared_ptr are
    // gone it is automatically destroyed.
    //if (ok == DLV_OK)
    //  delete ptr;
    return ok;
  } else {
    strncpy(message, "BUG: data object not found", mlen);
    return DLV_ERROR;
  }
}

DLV::data_object *DLV::operation::find_display(const int_g object)
{
  std::list< shared_ptr<data_object> >::iterator x;
  //bool found = false;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_displayable()) {
	if ((*x)->check_display_index(object)) {
	  return x->get();
	  break;
	}
      }
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_displayable()) {
	if ((*x)->check_display_index(object)) {
	  return x->get();
	  break;
	}
      }
    }
  }
  return 0;
}

DLVreturn_type DLV::operation::select_display(const int_g object,
					      char message[], const int_g mlen)
{
  data_object *obj = find_display(object);
  if (obj == 0) {
    strncpy(message, "BUG: display object not found 1", mlen);
    return DLV_ERROR;
  } else
    //if (found) {
    return obj->select(object, message, mlen);
  //} else {
  // strncpy(message, "BUG: display object not found", mlen);
  // return DLV_ERROR;
  //}
}

DLVreturn_type DLV::operation::delete_display(const int_g object,
					      char message[], const int_g mlen)
{
  data_object *obj = find_display(object);
  if (obj == 0) {
    strncpy(message, "BUG: display object not found", mlen);
    return DLV_ERROR;
  } else {
    //if (found) {
    obj->hide_render(object, 0, display_parent.get(), structure.get(),
		     message, mlen);
    DLVreturn_type ok = obj->remove(object, message, mlen);
    if (count_display_objects() == 0)
      display_parent->empty_display_list();
    else
      display_parent->reset_display_list(object);
    return ok;
  } //else {
    //strncpy(message, "BUG: display object not found", mlen);
    //return DLV_ERROR;
  //}
}

DLV::int_g DLV::operation::count_display_objects() const
{
  int_g count = 0;
  std::list< shared_ptr<data_object> >::const_iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      count += (*x)->count_display_objects();
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      count += (*x)->count_display_objects();
    }
  }
  return count;
}

DLVreturn_type DLV::operation::display_current_data(const int_g object,
						    const int_g component,
						    const int_g method,
						    bool &kspace,
						    char message[],
						    const int_g mlen)
{
  return current->display_data(object, component, method, kspace,
			       message, mlen);
}

DLVreturn_type DLV::operation::delete_current_data(const int_g object,
						   bool &kspace,
						   char message[],
						   const int_g mlen)
{
  return current->delete_data(object, kspace, message, mlen);
}

DLVreturn_type DLV::operation::select_current_display(const int_g object,
						      char message[],
						      const int_g mlen)
{
  return current->select_display(object, message, mlen);
}

DLVreturn_type DLV::operation::delete_current_display(const int_g object,
						      char message[],
						      const int_g mlen)
{
  return current->delete_display(object, message, mlen);
}

DLVreturn_type DLV::operation::update_cell(char message[], const int_g mlen)
{
  if (dont_update)
    return DLV_OK;
  DLVreturn_type ok = structure->update_cell(display_parent.get(),
					     message, mlen);
  if (ok == DLV_OK)
    ok = update_cell_info(message, mlen);
  return ok;
}

DLVreturn_type DLV::operation::update_lattice(const bool kspace,
					      char message[], const int_g mlen)
{
  if (dont_update)
    return DLV_OK;
  return structure->update_lattice(display_parent.get(), kspace,
				   message, mlen);
}

DLVreturn_type DLV::operation::update_atoms(char message[], const int_g mlen)
{
  if (dont_update)
    return DLV_OK;
  return structure->update_atoms(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::update_bonds(char message[], const int_g mlen)
{
  if (dont_update)
    return DLV_OK;
  // polyhedra means we may need to also change atoms
  //return structure->update_bonds(display_parent.get(), message, mlen);
  return structure->update_atoms(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::add_bond(char message[], const int_g mlen)
{
  return structure->add_bond(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::del_bond(char message[], const int_g mlen)
{
  return structure->del_bond(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::add_bond_type(char message[], const int_g mlen)
{
  return structure->add_bond_type(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::del_bond_type(char message[], const int_g mlen)
{
  return structure->del_bond_type(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::add_bond_all(char message[], const int_g mlen)
{
  return structure->add_bond_all(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::del_bond_all(char message[], const int_g mlen)
{
  return structure->del_bond_all(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::select_atom(const real_g coords[3],
					   const int_g status,
					   int_g &nselects,
					   char message[],
					   const int_g mlen)
{
  DLVreturn_type ok = structure->select_atom(display_parent.get(), coords,
					     status, nselects, message, mlen);
  if (ok == DLV_OK)
    ok = update_atom_selection_info(message, mlen);
  return ok;
}

void DLV::operation::set_selected_atom_flags(const bool done)
{
  structure->set_selected_atom_flags(done);
}

bool DLV::operation::set_atom_index_and_flags(int_g indices[],
					      const int_g value,
					      const bool set_def,
					      char message[], const int_g mlen)
{
  return structure->set_atom_index_and_flags(indices, value, set_def,
					     message, mlen);
}

DLVreturn_type DLV::operation::deselect_atoms(char message[],
					      const int_g mlen)
{
  DLVreturn_type ok = structure->deselect_all_atoms(display_parent.get(),
						    message, mlen);
  if (ok == DLV_OK)
    ok = update_atom_selection_info(message, mlen);
  return ok;
}

DLVreturn_type DLV::operation::update_outline(char message[],
					      const int_g mlen)
{
  return structure->update_outline(display_parent.get(), message, mlen);
}

DLVreturn_type DLV::operation::add_current_bond(char message[],
						const int_g mlen)
{
  if (current != 0)
    return current->add_bond(message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::del_current_bond(char message[],
						const int_g mlen)
{
  if (current != 0)
    return current->del_bond(message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::add_current_bond_type(char message[],
						     const int_g mlen)
{
  if (current != 0)
    return current->add_bond_type(message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::del_current_bond_type(char message[],
						     const int_g mlen)
{
  if (current != 0)
    return current->del_bond_type(message, mlen);
  else
    return DLV_OK;
}

void DLV::operation::set_current_bond_all()
{
  if (current != 0)
    current->set_bond_all();
}

DLVreturn_type DLV::operation::add_current_bond_all(char message[],
						    const int_g mlen)
{
  if (current != 0)
    return current->add_bond_all(message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::del_current_bond_all(char message[],
						    const int_g mlen)
{
  if (current != 0)
    return current->del_bond_all(message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::select_current_atom(const real_g coords[3],
						   const int_g status,
						   int_g &nselects,
						   char message[],
						   const int_g mlen)
{
  if (current != 0)
    return current->select_atom(coords, status, nselects, message, mlen);
  else
    return DLV_OK;
}

DLVreturn_type DLV::operation::deselect_all_atoms(char message[],
						  const int_g mlen)
{
  if (current != 0)
    return current->deselect_atoms(message, mlen);
  else
    return DLV_OK;
}

// Todo - call current->structure->... directly?
DLVreturn_type DLV::operation::update_current_outline(char message[],
						      const int_g mlen)
{
  if (current != 0)
    return current->update_outline(message, mlen);
  else
    return DLV_OK;
}

bool DLV::operation::select_current_job(const int_g index, char status[],
					bool &list_files, bool &recover,
					bool &log, bool &run, char message[],
					const int_g mlen)
{
  // Todo - list files? - extra entry in DLV job_data group?
  operation *op = 0;
  job *cur_job = find_job(index, op);
  status[0] = '\0';
  if (cur_job == 0) {
    strncpy(message, "Unable to locate job", mlen);
    return false;
  } else {
#ifdef ENABLE_DLV_JOB_THREADS
    boost::mutex::scoped_lock *lock = DLV::job::acquire_status_lock();
#endif // ENABLE_DLV_JOB_THREADS
    job_status_type s = cur_job->get_status();
    string state = job::get_status_info(s);
    update_job_status(state, index);
    strcpy(status, state.c_str());
    switch (s) {
    case run_and_completed:
      //strcpy(status, state.c_str());
      //list_files = false;
      //files.nullify();
      recover = true;
      //log = job->is_log_available();
      run = false;
      break;
    case run_and_failed:
      //strcpy(status, "Job failed");
      //list_files = false;
      //files.nullify();
      recover = true;
      //log = false;
      run = false;
      break;
    case run_and_recovered:
      //strcpy(status, "Job has been recovered");
      //list_files = false;
      recover = false;
      //log = true;
      run = false;
      break;
    case still_running:
      //strcpy(status, "Job is still running");
      //list_files = false;
      //files.nullify();
      recover = false;
      //log = job->is_log_available();
      run = true;
      break;
    case running_and_updated:
      //strcpy(status, "Job is still running - files have been updated");
      //list_files = true;
      //job->list_updated_files(files);
      recover = true;
      //log = job->is_log_available();
      run = true;
      break;
    case not_started:
      //strcpy(status, "Job didn't start");
      //list_files = false;
      //files.nullify();
      recover = false;
      //log = false;
      run = false;
      break;
    case recovered_with_errors:
      //strcpy(status, "Job has been recovered with errors");
      //list_files = false;
      recover = false;
      // Might be dangerous with multi-file local job copy.
      //log = job->is_log_available();
      run = false;
      break;
    case in_queue:
      //strcpy(status, "Job is queued");
      //list_files = false;
      //files.nullify();
      recover = false;
      //log = false;
      run = true; // For kill button?
      break;
    case being_submitted:
      //strcpy(status, "Globus is submitting job");
      //list_files = false;
      //files.nullify();
      recover = false;
      //log = false;
      run = true; // For kill button?
      break;
    case suspended:
      //strcpy(status, "Job is suspended");
      //list_files = false;
      //files.nullify();
      recover = false;
      //log = false;
      run = true; // For kill button?
      break;
    case nonDLV_job:
      recover = true;
      run = false;
      break;
    default:
      //strcpy(status, "Job status unknown");
      recover = false;
      run = true; // For kill button?
      break;
    }
#ifdef ENABLE_DLV_JOB_THREADS
    delete lock;
#endif // ENABLE_DLV_JOB_THREADS
  }
  return true;
}

DLVreturn_type DLV::operation::kill_current_job(const int_g index,
						char message[],
						const int_g mlen)
{
  operation *op = 0;
  job *cur_job = find_job(index, op);
  if (cur_job == 0) {
    strncpy(message, "Unable to locate job", mlen);
    return DLV_ERROR;
  } else
    return op->kill_job(message, mlen);
}

DLV::render_parent *DLV::operation::get_display_obj()
{
  // Todo - this could be very dangerous. Shouldn't need to inherit though
  if (display_parent == 0) {
    render_parent *rp = structure->create_render_parent();
    shared_ptr<render_parent> bp(rp);
    display_parent = bp;
  }
  return display_parent.get();
}

DLVreturn_type DLV::operation::update_point(const int_g method, const real_g x,
					    const real_g y, const real_g z,
					    const bool conv, char message[],
					    const int_g mlen)
{
  // Should only be a single calc item in the create point op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify point", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator ptr;
  ptr = editing->calculated_data.begin();
  return (*ptr)->update0D(method, x, y, z, conv, editing->get_model(),
			  message, mlen);
}

DLVreturn_type DLV::operation::update_line(const int_g method,
					   const int_g h, const int_g k,
					   const int_g l, const real_g x,
					   const real_g y, const real_g z,
					   const int_g obj, const bool conv,
					   const bool frac, const bool parent,
					   char message[], const int_g mlen)
{
  // Should only be a single calc item in the create line op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify line", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator ptr;
  ptr = editing->calculated_data.begin();
  return (*ptr)->update1D(method, h, k, l, x, y, z, obj, conv, frac, parent,
			  editing->get_model(), message, mlen);
}

DLVreturn_type DLV::operation::update_plane(const int_g method,
					    const int_g h, const int_g k,
					    const int_g l, const real_g x,
					    const real_g y, const real_g z,
					    const int_g obj, const bool conv,
					    const bool frac, const bool parent,
					    char message[], const int_g mlen)
{
  // Should only be a single calc item in the create plane op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify plane", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator p;
  p = editing->calculated_data.begin();
  return (*p)->update2D(method, h, k, l, x, y, z, obj, conv, frac, parent,
			editing->get_model(), message, mlen);
}

DLVreturn_type DLV::operation::update_3D_region(const int_g method,
						const int_g h, const int_g k,
						const int_g l, const real_g x,
						const real_g y, const real_g z,
						const int_g object,
						const bool conv,
						const bool frac,
						const bool p_obj,
						char message[],
						const int_g mlen)
{
  // Should only be a single calc item in the create 3D region op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify 3D region", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator p;
  p = editing->calculated_data.begin();
  return (*p)->update3D(method, h, k, l, x, y, z, object, conv, frac, p_obj,
			editing->get_model(), message, mlen);
}

DLVreturn_type DLV::operation::update_sphere(const int_g method,
					     const real_g radius,
					     char message[],
					     const int_g mlen)
{
  // avoid slider firing when radius attaches to panel at startup.
  if (editing == 0)
    return DLV_OK;
  // Should only be a single calc item in the create sphere op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify sphere", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  x = editing->calculated_data.begin();
  return (*x)->update3D(method, radius, editing->get_model(),
			message, mlen);
}

DLV::int_g DLV::operation::list_lines(string * &labels, char message[],
				      const int_g mlen)
{
  int_g n = 0;
  labels = 0;
  if (current != 0) {
    std::list< shared_ptr<data_object> >::iterator x;
    if (current->inherited_data.size() > 0) {
      for (x = current->inherited_data.begin();
	   x != current->inherited_data.end(); x++ )
	if ((*x)->is_line())
	  n++;
    }
    if (current->calculated_data.size() > 0) {
      for (x = current->calculated_data.begin();
	   x != current->calculated_data.end(); x++ )
	if ((*x)->is_line())
	  n++;
    }
    if (n > 0) {
      int_g count = 0;
      labels = new string[n];
      if (current->inherited_data.size() > 0) {
	for (x = current->inherited_data.begin();
	     x != current->inherited_data.end(); x++ ) {
	  if ((*x)->is_line()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
      if (current->calculated_data.size() > 0) {
	for (x = current->calculated_data.begin();
	     x != current->calculated_data.end(); x++ ) {
	  if ((*x)->is_line()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
    }
  }
  return n;
}

DLV::int_g DLV::operation::list_planes(string * &labels, char message[],
				       const int_g mlen)
{
  int_g n = 0;
  labels = 0;
  if (current != 0) {
    std::list< shared_ptr<data_object> >::iterator x;
    if (current->inherited_data.size() > 0) {
      for (x = current->inherited_data.begin();
	   x != current->inherited_data.end(); x++ )
	if ((*x)->is_plane())
	  n++;
    }
    if (current->calculated_data.size() > 0) {
      for (x = current->calculated_data.begin();
	   x != current->calculated_data.end(); x++ )
	if ((*x)->is_plane())
	  n++;
    }
    if (n > 0) {
      int_g count = 0;
      labels = new string[n];
      if (current->inherited_data.size() > 0) {
	for (x = current->inherited_data.begin();
	     x != current->inherited_data.end(); x++ ) {
	  if ((*x)->is_plane()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
      if (current->calculated_data.size() > 0) {
	for (x = current->calculated_data.begin();
	     x != current->calculated_data.end(); x++ ) {
	  if ((*x)->is_plane()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
    }
  }
  return n;
}

DLV::int_g DLV::operation::list_3D_regions(string * &labels, char message[],
					   const int_g mlen)
{
  int_g n = 0;
  labels = 0;
  if (current != 0) {
    std::list< shared_ptr<data_object> >::iterator x;
    if (current->inherited_data.size() > 0) {
      for (x = current->inherited_data.begin();
	   x != current->inherited_data.end(); x++ )
	if ((*x)->is_box())
	  n++;
    }
    if (current->calculated_data.size() > 0) {
      for (x = current->calculated_data.begin();
	   x != current->calculated_data.end(); x++ )
	if ((*x)->is_box())
	  n++;
    }
    if (n > 0) {
      int_g count = 0;
      labels = new string[n];
      if (current->inherited_data.size() > 0) {
	for (x = current->inherited_data.begin();
	     x != current->inherited_data.end(); x++ ) {
	  if ((*x)->is_box()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
      if (current->calculated_data.size() > 0) {
	for (x = current->calculated_data.begin();
	     x != current->calculated_data.end(); x++ ) {
	  if ((*x)->is_box()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
    }
  }
  return n;
}

DLV::int_g DLV::operation::list_wulff(string * &labels, char message[],
				      const int_g mlen)
{
  int_g n = 0;
  labels = 0;
  if (current != 0) {
    std::list< shared_ptr<data_object> >::iterator x;
    if (current->inherited_data.size() > 0) {
      for (x = current->inherited_data.begin();
	   x != current->inherited_data.end(); x++ )
	if ((*x)->is_wulff())
	  n++;
    }
    if (current->calculated_data.size() > 0) {
      for (x = current->calculated_data.begin();
	   x != current->calculated_data.end(); x++ )
	if ((*x)->is_wulff())
	  n++;
    }
    if (n > 0) {
      int_g count = 0;
      labels = new string[n];
      if (current->inherited_data.size() > 0) {
	for (x = current->inherited_data.begin();
	     x != current->inherited_data.end(); x++ ) {
	  if ((*x)->is_wulff()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
      if (current->calculated_data.size() > 0) {
	for (x = current->calculated_data.begin();
	     x != current->calculated_data.end(); x++ ) {
	  if ((*x)->is_wulff()) {
	    labels[count] = (*x)->get_name();
	    count++;
	  }
	}
      }
    }
  }
  return n;
}

DLV::data_object *DLV::operation::find_line(const int_g selection)
{
  int_g n = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      if ((*x)->is_line()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      if ((*x)->is_line()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  return 0;
}

DLV::data_object *DLV::operation::find_plane(const int_g selection)
{
  int_g n = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      if ((*x)->is_plane()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      if ((*x)->is_plane()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  return 0;
}

DLV::data_object *DLV::operation::find_3D_region(const int_g selection)
{
  int_g n = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (current->inherited_data.size() > 0) {
    for (x = current->inherited_data.begin();
	 x != current->inherited_data.end(); x++ ) {
      if ((*x)->is_box()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  if (current->calculated_data.size() > 0) {
    for (x = current->calculated_data.begin();
	 x != current->calculated_data.end(); x++ ) {
      if ((*x)->is_box()) {
	if (n == selection)
	  return x->get();
	n++;
      }
    }
  }
  return 0;
}

DLVreturn_type DLV::operation::set_data_transform(const bool v, char message[],
						  const int_g mlen)

{
  // Should only be a single calc item in the create 3D region op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify data object", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  x = editing->calculated_data.begin();
  return (*x)->use_view_editor(editing->display_parent.get(),
			       v, message, mlen);
}

DLVreturn_type DLV::operation::update_wulff(const int_g h, const int_g k,
					    const int_g l, const real_g e,
					    const real_g r, const real_g g,
					    const real_g b, const bool conv,
					    char message[], const int_g mlen)
{
  // Should only be a single calc item in the create plane op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify wulff plot", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  x = editing->calculated_data.begin();
  return (*x)->update_wulff(h, k, l, e, r, g, b, conv, editing->get_model(),
			    message, mlen);
}

DLVreturn_type DLV::operation::update_wulff(const char filename[],
					    char message[], const int_g mlen)
{
  // Should only be a single calc item in the create plane op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify wulff plot", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  x = editing->calculated_data.begin();
  return (*x)->update_wulff(filename, editing->get_model(),
			    message, mlen);
}

DLVreturn_type DLV::operation::update_leed(const bool domains, char message[],
					   const int_g mlen)
{
  // Should only be a single calc item in the create leed op.
  if (editing->calculated_data.size() != 1) {
    strncpy(message, "BUG: couldn't identify leed pattern", mlen);
    return DLV_ERROR;
  }
  std::list< shared_ptr<data_object> >::iterator x;
  x = editing->calculated_data.begin();
  return (*x)->update_leed(domains, editing->get_model(), message, mlen);
}

void DLV::operation::deactivate_atom_flags()
{
  structure->deactivate_atom_flags();
}

void DLV::operation::toggle_atom_flags(const string label)
{
  structure->toggle_atom_flags(label);
}

DLVreturn_type DLV::operation::update_spectrum(const int_g object,
					       const int_g type,
					       const real_g emin,
					       const real_g emax,
					       const int_g np, const real_g w,
					       const real_g harmonics,
					       char message[], const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not spectrum", mlen);
    return DLV_ERROR;
  } else
    return obj->update_spectrum(type, emin, emax, np, w, harmonics,
				message, mlen);
}

DLVreturn_type DLV::operation::update_current_spectrum(const int_g object,
						       const int_g type,
						       const real_g emin,
						       const real_g emax,
						       const int_g np,
						       const real_g w,
						       const real_g harmonics,
						       char message[],
						       const int_g mlen)
{
  return current->update_spectrum(object, type, emin, emax, np, w, harmonics,
				  message, mlen);
}

DLVreturn_type DLV::operation::update_phonon(const int_g object,
					     const int_g nframes,
					     const real_g scale,
					     const bool use_temp,
					     const real_g temperature,
					     char message[], const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not phonon trajectory", mlen);
    return DLV_ERROR;
  } else
    return obj->update_phonon(display_parent.get(), structure.get(), nframes,
			      scale, use_temp, temperature, message, mlen);
}

DLVreturn_type DLV::operation::update_current_phonon(const int_g object,
						     const int_g nframes,
						     const real_g scale,
						     const bool use_temp,
						     const real_g temperature,
						     char message[],
						     const int_g mlen)
{
  return current->update_phonon(object, nframes, scale, use_temp, temperature,
				message, mlen);
}

DLVreturn_type DLV::operation::hide_render(const int_g object,
					   const int_g visible, bool &kspace,
					   char message[], const int_g mlen)
{
  data_object *obj = find_display(object);
  if (obj == 0) {
    strncpy(message, "BUG: display object not found", mlen);
    return DLV_ERROR;
  } else {
    obj->hide_render(object, visible, display_parent.get(), structure.get(),
		     message, mlen);
    kspace = obj->is_kspace();
    return DLV_OK;
  }
}

DLVreturn_type DLV::operation::hide_current_render(const int_g object,
						   const int_g visible,
						   bool &kspace,
						   char message[],
						   const int_g mlen)
{
  return current->hide_render(object, visible, kspace, message, mlen);
}

DLVreturn_type DLV::operation::update_streamlines(const int_g object,
						  const int_g plane,
						  char message[],
						  const int_g mlen)
{
  data_object *obj = find_display(object);
  if (obj == 0) {
    strncpy(message, "BUG: display object not found", mlen);
    return DLV_ERROR;
  } else {
    obj->update_streamlines(object, find_plane(plane), display_parent.get(),
			    message, mlen);
    return DLV_OK;
  }
}

DLVreturn_type DLV::operation::update_current_streamlines(const int_g object,
							  const int_g plane,
							  char message[],
							  const int_g mlen)
{
  return current->update_streamlines(object, plane, message, mlen);
}

DLVreturn_type DLV::operation::update_slice(const int_g object,
					    const int_g plane,
					    char message[], const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not a slice", mlen);
    return DLV_ERROR;
  } else
    return obj->update_slice(display_parent.get(), structure.get(),
			     find_plane(plane), message, mlen);
}

DLVreturn_type DLV::operation::update_current_slice(const int_g object,
						    const int_g plane,
						    char message[],
						    const int_g mlen)
{
  return current->update_slice(object, plane, message, mlen);
}

DLVreturn_type DLV::operation::update_cut(const int_g object, const int_g plane,
					  char message[], const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not a cut", mlen);
    return DLV_ERROR;
  } else
    return obj->update_cut(display_parent.get(), structure.get(),
			   find_plane(plane), message, mlen);
}

DLVreturn_type DLV::operation::update_current_cut(const int_g object,
						  const int_g plane,
						  char message[],
						  const int_g mlen)
{
  return current->update_cut(object, plane, message, mlen);
}

DLVreturn_type DLV::operation::update_extension(const int_g object,
						const int_g na, const int_g nb,
						const int_g nc, char message[],
						const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not an extension", mlen);
    return DLV_ERROR;
  } else
    return obj->update_extension(display_parent.get(),
				 na, nb, nc, message, mlen);
}

DLVreturn_type DLV::operation::update_current_extension(const int_g object,
							const int_g na,
							const int_g nb,
							const int_g nc,
							char message[],
							const int_g mlen)
{
  return current->update_extension(object, na, nb, nc, message, mlen);
}

// Todo - common stuff with update?
DLVreturn_type DLV::operation::list_data_math(const int_g object,
					      const bool all, string * &names,
					      int_g &n, char message[],
					      const int_g mlen)
{
  n = 0;
  names = 0;
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not data math", mlen);
    return DLV_ERROR;
  } else {
    n = 0;
    names = 0;
    const data_object *data = 0;
    int_g cmpt;
    obj->data_math_parent(data, cmpt);
    if (data == 0) {
      strncpy(message, "BUG: data math object has no parent", mlen);
      return DLV_ERROR;
    } else {
      // Todo - search parent models for data?
      int_g nmatch;
      std::list< shared_ptr<data_object> >::iterator x;
      if (inherited_data.size() > 0) {
	for (x = inherited_data.begin(); x != inherited_data.end(); x++ )
	  (*x)->match_data_math(data, -1, all, cmpt, nmatch, n);
      }
      if (calculated_data.size() > 0) {
	for (x = calculated_data.begin(); x != calculated_data.end(); x++ )
	  (*x)->match_data_math(data, -1, all, cmpt, nmatch, n);
      }
      if (n > 0) {
	names = new string[n];
	int_g count = 0;
	if (inherited_data.size() > 0) {
	  for (x = inherited_data.begin(); x != inherited_data.end(); x++ )
	    (*x)->list_data_math(data, all, cmpt, names, count);
	}
	if (calculated_data.size() > 0) {
	  for (x = calculated_data.begin(); x != calculated_data.end(); x++ )
	    (*x)->list_data_math(data, all, cmpt, names, count);
	}
      }
      return DLV_OK;
    }
  }
}

// version of list_displayable
DLVreturn_type DLV::operation::list_reals(string * &names, int_g &n,
					  char message[], const int_g mlen)
{
  names = 0;
  n = 0;
  int_g count = 0;
  std::list< shared_ptr<data_object> >::iterator x;
  if (inherited_data.size() > 0) {
    for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
      if ((*x)->is_real_scalar())
	count++;
    }
  }
  if (calculated_data.size() > 0) {
    for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
      if ((*x)->is_real_scalar())
	count++;
    }
  }
  if (count > 0) {
    n = count;
    names = new string[count];
    count = 0;
    if (inherited_data.size() > 0) {
      for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
	if ((*x)->is_real_scalar()) {
	  names[count] = (*x)->get_data_label();
	  count++;
	}
      }
    }
    if (calculated_data.size() > 0) {
      for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	if ((*x)->is_real_scalar()) {
	  names[count] = (*x)->get_data_label();
	  count++;
	}
      }
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::operation::update_data_math(const int_g object,
						const int_g selection,
						const bool all,
						const int_g index,
						char message[],
						const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not data math", mlen);
    return DLV_ERROR;
  } else {
    int_g n = 0;
    const data_object *data = 0;
    int_g cmpt;
    obj->data_math_parent(data, cmpt);
    if (data == 0) {
      strncpy(message, "BUG: data math object has no parent", mlen);
      return DLV_ERROR;
    } else {
      // Todo - search parent models for data?
      bool found = false;
      int_g nmatch;
      std::list< shared_ptr<data_object> >::iterator x;
      if (inherited_data.size() > 0) {
	for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
	  (*x)->match_data_math(data, selection, all, cmpt, nmatch, n);
	  if (n > selection) {
	    found = true;
	    break;
	  }
	}
      }
      if (!found and calculated_data.size() > 0) {
	for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	  (*x)->match_data_math(data, selection, all, cmpt, nmatch, n);
	  if (n > selection) {
	    found = true;
	    break;
	  }
	}
      }
      if (found)
	return obj->update_data_math(display_parent.get(), x->get(), nmatch,
				     index, message, mlen);
      else {
	strncpy(message, "failed finding data math selection", mlen);
	return DLV_ERROR;
      }
    }
  }
}

DLVreturn_type DLV::operation::list_current_data_math(const int_g object,
						      const bool all,
						      string * &names, int_g &n,
						      char message[],
						      const int_g mlen)
{
  return current->list_data_math(object, all, names, n, message, mlen);
}

DLVreturn_type DLV::operation::update_current_data_math(const int_g object,
							const int_g selection,
							const bool all,
							const int_g index,
							char message[],
							const int_g mlen)
{
  return current->update_data_math(object, selection, all, index,
				   message, mlen);
}

DLVreturn_type DLV::operation::list_current_reals(string * &names, int_g &n,
						  char message[],
						  const int_g mlen)
{
  return current->list_reals(names, n, message, mlen);
}

DLVreturn_type DLV::operation::update_shift(const int_g object,
					    const real_g shift,
					    const int_g selection,
					    const bool use_select,
					    char message[], const int_g mlen)
{
  data_object *obj = find_edit(object);
  if (obj == 0) {
    strncpy(message, "BUG: object is not plot shift", mlen);
    return DLV_ERROR;
  } else {
    real_l eshift = shift;
    if (use_select) {
      int_g count = 0;
      std::list< shared_ptr<data_object> >::iterator x;
      bool found = false;
      if (inherited_data.size() > 0) {
	for (x = inherited_data.begin(); x != inherited_data.end(); x++ ) {
	  if ((*x)->is_real_scalar()) {
	    if (count == selection) {
	      found = true;
	      break;
	    }
	    count++;
	  }
	}
      }
      if (!found and calculated_data.size() > 0) {
	for (x = calculated_data.begin(); x != calculated_data.end(); x++ ) {
	  if ((*x)->is_real_scalar()) {
	    if (count == selection) {
	      found = true;
	      break;
	    }
	    count++;
	  }
	}
      }
      if (found) {
	x->get()->get_real_value(eshift);
	//real_data *d = static_cast<real_data *>(x->get());
	//eshift = - d->get_value();
	eshift = - eshift;
      } else {
	strncpy(message, "Failed to find real value", mlen);
	return DLV_ERROR;
      }
    }
    return obj->update_shift(eshift, message, mlen);
  }
}

DLVreturn_type DLV::operation::update_current_shift(const int_g object,
						    const real_g shift,
						    const int_g selection,
						    const bool use_select,
						    char message[],
						    const int_g mlen)
{
  return current->update_shift(object, shift, selection, use_select,
			       message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::operation::save(Archive &ar, const unsigned int version) const
{
  ar & date_stamp;
  ar & rspace_view;
  ar & kspace_view;
  ar & index;
  ar & display_index;
  ar & edit_index;
#ifdef ENABLE_DLV_GRAPHICS
  ar & display_parent;
#endif // ENABLE_DLV_GRAPHICS
  ar & parent;
  ar & structure;
  ar & inherited_data;
  ar & mappable_data;
  ar & calculated_data;
  ar & children;
}

template <class Archive>
void DLV::operation::load(Archive &ar, const unsigned int version)
{
  ar & date_stamp;
  ar & rspace_view;
  ar & kspace_view;
  ar & index;
  ar & display_index;
  ar & edit_index;
#ifdef ENABLE_DLV_GRAPHICS
  ar & display_parent;
  if (display_parent != 0)
    display_parent->set_serialize_obj();
#endif // ENABLE_DLV_GRAPHICS
  ar & parent;
  ar & structure;
  ar & inherited_data;
  ar & mappable_data;
  ar & calculated_data;
  ar & children;
  reload = true;
}

template <class Archive>
void DLV::load_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & file;
}

template <class Archive>
void DLV::edit_data_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::operation)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit_data_op)

DLV_SUPPRESS_TEMPLATES(DLV::file_obj)

DLV_SPLIT_EXPLICIT(DLV::operation)
DLV_NORMAL_EXPLICIT(DLV::load_op)
DLV_NORMAL_EXPLICIT(DLV::edit_data_op)
DLV_EXPORT_EXPLICIT(DLV::operation)

#endif // DLV_USES_SERIALIZE
