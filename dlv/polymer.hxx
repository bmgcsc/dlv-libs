
#ifndef DLV_MODEL_POLYMER
#define DLV_MODEL_POLYMER

namespace DLV {

  class polymer : public periodic_model<line_symmetry> {
  public:
    polymer(const string model_name);
    int_g get_number_of_periodic_dims() const;
    int_g get_lattice_type() const;
    int_g get_lattice_centring() const;
    int_g get_model_type() const;
    void set_crystal03_lattice_type(const int_g lattice, const int_g centre);

    void use_which_lattice_parameters(bool usage[6]) const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;
    void get_conventional_lattice(coord_type a[3], coord_type b[3],
				  coord_type c[3]) const;
    void get_lattice_parameters(coord_type &a, coord_type &b,
				coord_type &c, coord_type &alpha,
				coord_type &beta, coord_type &gamma,
				const bool conventional) const;
    void get_frac_rotation_operators(real_l r[][3][3], const int_g nops) const;

    void find_asym_pos(const int_g asym_idx, const coord_type a[3],
		       int_g &op, coord_type a_coords[3],
		       const coord_type b[][3], const int_g b_asym_idx[],
		       coord_type b_coords[][3], int_g ab_shift[3],
		       const int_g nbatoms) const;
    void get_reciprocal_lattice(coord_type a[3], coord_type b[3],
				coord_type c[3]) const;

  protected:
    model_base *duplicate(const string label) const;

    bool set_primitive_lattice(const coord_type a[3], const coord_type [3],
			       const coord_type [3]);
    bool set_lattice_parameters(const coord_type a, const coord_type,
				const coord_type, const coord_type,
				const coord_type, const coord_type);
    bool set_fractional_sym_ops(const real_l rotations[][3][3],
				const real_l translations[][3],
				const int_g nops);
    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;
    void generate_real_space_lattice(real_g (* &ipoints)[3],
				     real_g (* &fpoints)[3],
				     int_g &nlines, const int_g na,
				     const int_g nb, const int_g nc,
				     const bool centre_cell,
				     const bool conventional,
				     char message[], const int_g mlen) const;
    int_g get_real_space_lattice_labels(real_g points[][3], const int_g na,
				      const int_g nb, const int_g nc,
				      const bool centre_cell,
				      const bool conventional) const;
    void generate_k_space_vertices(real_g (* &vertices)[3], int_g &nlines,
				   int_l * &connects, int_l &nconnects,
				   char message[], const int_g mlen) const;
    void generate_primitive_atoms(const bool tidy = false);
    void conventional_atoms(const atom_tree &primitive, atom_tree &tree,
			    const bool centre[3]) const;
    void centre_atoms(const atom_tree &primitive, atom_tree &tree,
		      const bool centre[3]) const;
    void update_display_atoms(atom_tree &tree, const real_l tol, const int_g na,
			      const int_g nb, const int_g nc,
			      const bool conventional, const bool centre,
			      const bool edges) const;
    void generate_bonds(const atom_tree &tree, const int_g na,
			const int_g nb, const int_g nc, const bool conventional,
			std::list<bond_list> info[], const real_g overlap,
			const bool centre, const bool invert_colours,
			const bond_data &bond_info, const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const int_g nframes, const bool gamma,
			const bool edit) const;
    void complete_lattice(coord_type va[3], coord_type vb[3],
			  coord_type vc[3]);
    void get_atom_shifts(const atom_tree &tree, const coord_type coords[][3],
			 int_g shifts[][3], int_g prim_id[],
			 const int_g n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  int_g **const data, const int_g ndata, const int_g ngrid,
		  real_g (* &map_grid)[3], int_g ** &map_data, int_g &n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  real_g **const data, const int_g ndata, const int_g ngrid,
		  real_g (* &map_grid)[3], real_g ** &map_data, int_g &n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  const real_g mag[][3], const real_g phases[][3],
		  const int_g ngrid, real_g (* &map_grid)[3],
		  real_g (* &map_data)[3], int_g &n, const real_g ka,
		  const real_g kb, const real_g kc) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  const real_g mags[][3], const real_g phases[][3],
		  const int_g ngrid, std::vector<real_g (*)[3]> &new_data,
		  int_g &n, const int_g nframes, const int_g ncopies,
		  const real_g ka, const real_g kb, const real_g kc,
		  const real_g scale) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  const int_g ngrid, const std::vector<real_g (*)[3]> &traj,
		  const int_g nframes,
		  std::vector<real_g (*)[3]> &new_data) const;
    bool map_atom_selections(const atom_tree &display, const atom_tree &prim,
			     const real_g grid[][3], int_g **const data,
			     const int_g ndata, const int_g ngrid,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, int_g &n,
			     const bool all_atoms) const;
    bool map_selected_atoms(const atom_tree &display, int_g &n, int_g * &labels,
			    int_g (* &shifts)[3], const real_g grid[][3],
			    int_g **const data, const int_g ndata,
			    const int_g ngrid) const;

    void copy_lattice(const model_base *m);
    void copy_bravais_lattice(const model_base *m);
    void copy_symmetry_ops(const model_base *m, const bool frac);
    void rotate_symmetry_ops(const model_base *m, const real_l r[3][3]);
    void build_supercell_symmetry(const model *m, const coord_type a[3],
				  const coord_type b[3], const coord_type c[3],
				  const coord_type va[3],
				  const coord_type vb[3],
				  const coord_type vc[3], const bool conv);
    void cartesian_to_fractional_coords(real_l &x, real_l &y, real_l &z) const;
    void fractional_to_cartesian_coords(real_l &x, real_l &y, real_l &z) const;
    void shift_cartesian_operators(const model_base *m, const real_l x,
				   const real_l y, const real_l z,
				   const coord_type a[3],
				   const coord_type b[3],
				   const coord_type c[3]);
    void shift_fractional_operators(const model_base *m, const real_l x,
				    const real_l y, const real_l z,
				    const coord_type a[3],
				    const coord_type b[3],
				    const coord_type c[3]);

  private:
    void get_reciprocal_lattice(coord_type &l) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::polymer)
#endif // DLV_USES_SERIALIZE

inline DLV::polymer::polymer(const string model_name)
  : periodic_model<line_symmetry> (model_name)
{
}

#endif // DLV_MODEL_POLYMER
