
#include <list>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
//#  ifndef DLV_USES_SERIALIZE
//#    warning "Full DLV project support requires defining DLV_USES_SERIALIZE"
//#  endif // serialize - warning is C++23, MSVC++ may not support it
#  include "../graphics/calculations.hxx"
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "operation.hxx"
#include "model.hxx"
#include "atom_prefs.hxx"
#include "project.hxx"

// Todo - is a separate project needed? Most of this could be static
// within the operation class.
namespace DLV {

  namespace project {
    string name = "";
    string directory = "";
    string base_dir = "";
    string creation_date = "";
    string last_saved = "not saved";
    int_g job_number = 0;
    int_g count_items = 0;
    bool unnamed = true;
    bool initialised = false;
  }

}

DLVreturn_type DLV::project::initialise(const char atoms_def[],
					const char prefs[], const char name[],
					char message[], const int_g mlen)
{
  /*
  project::name = name;
  directory = get_current_directory().c_str();
  creation_date = current_date_time();
  job_number = 0;*/
  base_dir = get_current_directory().c_str();
  base_dir += DIR_SEP_CHR;
  char title[256];
  create("Unnamed", "", "", "", false, title, message, mlen);
  unnamed = true;
  change_directory(base_dir.c_str());
  atomic_data = new DLV::atomic_prefs;
  DLVreturn_type ok = atomic_data->read(atoms_def, message, mlen);
  //if (ok == DLV_OK)
  //  ok = model::configure_display_prefs(prefs, message, mlen);
  initialised = true;
  return ok;
}

DLVreturn_type DLV::project::create(const char pname[], const char dir[],
				    const char atoms_def[], const char prefs[],
				    const int_g inherit, char title[],
				    char message[], const int_g mlen)
{
  char dir_name[1024];
  if (strlen(dir) == 0) {
    strcpy(dir_name, base_dir.c_str());
    strncat(dir_name, pname, 64);
    //dir_name[63] = '\0';
  } else {
    if (is_absolute_path(dir))
      dir_name[0] = '\0';
    else
      strcpy(dir_name, base_dir.c_str());
    strncat(dir_name, dir, 256);
    //dir_name[255] = '\0';
  }
  if (!use_directory(dir_name, true, message, mlen)) {
    return DLV_ERROR;
  } else {
    if (change_directory(dir_name)) {
      if (initialised and unnamed)
	delete_directory(directory.c_str());
      operation::clear_all();
      name = pname;
      directory = dir_name;
      creation_date = current_date_time();
      last_saved = "not saved";
      job_number = 0;
#ifdef ENABLE_DLV_GRAPHICS
      clear_job_list();
#endif // ENABLE_DLV_GRAPHICS
      DLVreturn_type ok = DLV_OK;
      if (!unnamed) {
	if (inherit == 0) {
	  delete atomic_data;
	  atomic_data = new DLV::atomic_prefs;
	  ok = atomic_data->read(atoms_def, message, mlen);
	  //if (ok == DLV_OK) Todo
	  //  ok = model::configure_display_prefs(prefs, message, mlen);
	}
      }
      count_items = 0;
      unnamed = false;
      strncpy(title, name.c_str(), mlen);
      return ok;
    } else
      strncpy(message, "Failed to change directory", mlen);
  }
  return DLV_ERROR;
}

DLVreturn_type DLV::project::load(const char dir[], char title[],
				  char message[], const int_g mlen)
{
  if (!file_exists(dir)) {
    strncpy(message, "directory doesn't exist", mlen);
    return DLV_ERROR;
  } else {
    if (change_directory(dir)) {
      if (unnamed)
	delete_directory(directory.c_str());
      if (file_exists("project.dlv") and file_exists("archive.dlv")) {
	operation::clear_all();
	std::ifstream input;
	if (open_file_read(input, "project.dlv", message, mlen)) {
	  char line[MAX_PATH_LEN];
	  input.getline(line, MAX_PATH_LEN);
	  if (line[strlen(line) - 1] == '\n')
	    line[strlen(line) - 1] = '\0';
	  name = line;
	  strncpy(title, name.c_str(), mlen);
	  input.getline(line, MAX_PATH_LEN);
	  directory = dir;
	  input.getline(line, MAX_PATH_LEN);
	  if (line[strlen(line) - 1] != '\n') {
	    line[strlen(line)] = '\n';
	    line[strlen(line) + 1] = '\0';
	  }
	  creation_date = line;
	  input.getline(line, MAX_PATH_LEN);
	  if (line[strlen(line) - 1] != '\n') {
	    line[strlen(line)] = '\n';
	    line[strlen(line) + 1] = '\0';
	  }
	  last_saved = line;
	  unnamed = false;
	  input >> job_number;
	  input.getline(line, MAX_PATH_LEN);
	  input.close();
	  if (open_file_read(input, "archive.dlv", message, mlen)) {
#ifdef DLV_USES_SERIALIZE
	    DLV::operation::load(input);
#endif // DLV_USES_SERIALIZE
	    count_items = DLV::operation::max_items();
	    input.close();
	    if (open_file_read(input, "atoms.dlv", message, mlen)) {
	      atomic_data = DLV::atomic_prefs::load(input);
	      input.close();
	      //strncpy(message, "Project::Load not fully implemented", mlen);
	      // Todo - load display_prefs and viewer settings.
	      //ok = model::configure_display_prefs(prefs, message, mlen);
	      //return DLV_WARNING;
	      return DLV_OK;
	    }
	  }
	}
      } else
	strncpy(message, "Missing DLV project files - "
		"is this really a DLV project directory?", mlen);
    } else
      strncpy(message, "Failed to change directory", mlen);
  }
  return DLV_ERROR;
}

DLVreturn_type DLV::project::save(char message[], const int mlen)
{
  std::ofstream output;
  if (open_file_write(output, "project.dlv", message, mlen)) {
    last_saved = current_date_time();
    output << name << '\n';
    output << directory << '\n';
    output << creation_date;
    output << last_saved;
    output << job_number << '\n';
    output.close();
    if (open_file_writeb(output, "archive.dlv", message, mlen)) {
#ifdef DLV_USES_SERIALIZE
      DLV::operation::save(output);
#endif // DLV_USES_SERIALIZE
      count_items = DLV::operation::max_items();
      output.close();
      if (open_file_writeb(output, "atoms.dlv", message, mlen)) {
	atomic_data->save(output);
	output.close();
	//strncpy(message, "Project::Save not fully implemented", mlen);
	// Todo - display_prefs and viewer settings.
	//	return DLV_WARNING;
	return DLV_OK;
      }
    }
  }
  return DLV_ERROR;
}

DLVreturn_type DLV::project::save_all(const char pname[], const char dir[],
				      char title[], char message[],
				      const int mlen)
{
  char dir_name[256];
  if (strlen(dir) == 0) {
    strcpy(dir_name, base_dir.c_str());
    strncat(dir_name, pname, 64);
    //dir_name[63] = '\0';
  } else {
    if (is_absolute_path(dir))
      dir_name[0] = '\0';
    else
      strcpy(dir_name, base_dir.c_str());
    strncat(dir_name, dir, 255);
    dir_name[255] = '\0';
  }
  bool ok = true;
  if (use_directory(dir_name, !unnamed, message, mlen)) {
    if (unnamed)
      ok = rename_directory(directory.c_str(), dir_name);
    //else
    //  ok = make_directory(dir_name);
    if (!ok) {
      strncpy(message, "directory already exists", mlen);
      return DLV_ERROR;
    } else {
      if (change_directory(dir_name)) {
	name = pname;
	directory = dir_name;
	unnamed = false;
	//job_number = 0;
	strncpy(title, name.c_str(), mlen);
	DLVreturn_type result = save(message, mlen);
	if (result == DLV_OK and job_number > 0) {
	  strncpy(message, "Existing jobs may not reload properly for project",
		  mlen);
	  result = DLV_WARNING;
	}
	return result;
      } else
	strncpy(message, "Failed to change directory", mlen);
    }
  }
  return DLV_ERROR;
}

void DLV::project::delete_unnamed()
{
  if (unnamed) // should be true
    delete_directory(directory.c_str());
}

bool DLV::project::has_been_saved()
{
  if (unnamed)
    return (DLV::operation::max_items() == 0);
  else {
    // since UI changes for display items don't track op changes this is tricky
    // Todo
    return (DLV::operation::max_items() == count_items);
    //fprintf(stderr, "Project saving not sorted yet");
    //return false;
  }
}

int DLV::project::get_job_number()
{
  return job_number;
}

void DLV::project::inc_job_number()
{
  job_number++;
}

DLV::string DLV::project::get_project_dir()
{
  return directory;
}
