
#ifndef DLV_PROJECT
#define DLV_PROJECT

namespace DLV {

  namespace project {
    DLVreturn_type initialise(const char atoms_def[],
			      const char prefs[], const char name[],
			      char message[], const int_g mlen);
    DLVreturn_type create(const char pname[], const char dir[],
			  const char atoms_def[], const char prefs[],
			  const int_g inherit, char title[],
			  char message[], const int_g mlen);
    DLVreturn_type load(const char dir[], char title[], char message[],
			const int_g mlen);
    DLVreturn_type save(char message[], const int_g mlen);
    DLVreturn_type save_all(const char pname[], const char dir[], char title[],
			    char message[], const int_g mlen);
    void delete_unnamed();
    bool has_been_saved();
    int_g get_job_number();
    void inc_job_number();

    string get_project_dir();
  }

}

#endif // DLV_PROJECT
