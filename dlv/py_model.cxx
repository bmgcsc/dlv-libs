
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
//#include "model.hxx"
#include "extern_model.hxx"
#include "boost/python.hpp"

#define DLV_MODULE_NAME model

BOOST_PYTHON_OPAQUE_SPECIALIZED_TYPE_ID(DLV::operation)

namespace {

  /*
  DLVreturn_type
  (*update_slab_10args)(const DLV::int_g h, const DLV::int_g k,
			const DLV::int_g l, const bool conventional,
			const DLV::real_g tol, DLV::int_g &nlayers,
			DLV::string * &labels, DLV::int_g &n, char message[],
			const DLV::int_g mlen) = &DLV::update_slab;
  DLVreturn_type
  (*update_slab_6args)(const DLV::real_g tol, DLV::int_g &nlayers,
		       DLV::string * &labels, DLV::int_g &n, char message[],
		       const DLV::int_g mlen) = &DLV::update_slab;
  DLVreturn_type
  (*update_slab_2args)(const DLV::int_g nlayers,
		       std::string message) = &DLV::update_slab;
  */
  /*
  DLVreturn_type
  (*update_surface_9args)(const DLV::int_g h, const DLV::int_g k,
			  const DLV::int_g l, const bool conventional,
			  const DLV::real_g tol, DLV::string * &labels,
			  DLV::int_g &n, char message[],
			  const DLV::int_g mlen) = &DLV::update_surface;
  DLVreturn_type
  (*update_surface_5args)(const DLV::real_g tol, DLV::string * &labels,
			  DLV::int_g &n, char message[],
			  const DLV::int_g mlen) = &DLV::update_surface;
  DLVreturn_type
  (*update_surface_3args)(const DLV::int_g term, char message[],
			  const DLV::int_g mlen) = &DLV::update_surface;
  DLVreturn_type
  (*update_salvage_tol)(const DLV::real_g tol, char message[],
			const DLV::int_g mlen) = &DLV::update_salvage;
  DLVreturn_type
  (*update_salvage_layers)(const DLV::int_g nlayers, char message[],
			   const DLV::int_g mlen) = &DLV::update_salvage;
  */

}

namespace DLVPY_model {

  struct op_result_t {
    DLV::operation *op;
    std::string message;
  };

  op_result_t *load_cif(const char name[], const char file_name[]);

  struct slab_data {
    int h;
    int k;
    int l;
    int nlayers;
    boost::python::list labels;
    bool failed;
    std::string message;
  };

  slab_data *cut_slab(const char name[], const int h, const int k, const int l,
		      const bool conventional, const double tol);
  slab_data *update_slab_hkl(const int h, const int k, const int l,
			     const bool conventional, const double tol);
  std::string update_slab_termination(const int term);
  std::string update_slab_layers(const int nlayers);
  std::string add_vacuum_gap(const char name[], const double c_axis, const double origin);
  std::string update_vacuum_gap(const double c_axis, const double origin);

}

DLVPY_model::op_result_t *DLVPY_model::load_cif(const char name[], const char file_name[])
{
  op_result_t *cif = new op_result_t;
  char message[256];
  cif->op = DLV::load_cif(name, file_name, message, 256);
  if (cif->op == nullptr)
    cif->message = message;
  else
    cif->message = "";
  return cif;
}

DLVPY_model::slab_data *DLVPY_model::cut_slab(const char name[], const int h, const int k,
					      const int l, const bool conv, const double tol)
{
  DLV::string *labels;
  int nlabels;
  slab_data *data = new slab_data;
  char message[256];
  DLVreturn_type ok = DLV::cut_slab(name, h, k, l, conv, tol, data->nlayers, labels,
				    nlabels, message, 256);
  if (ok == DLV_OK) {
    data->h = h;
    data->k = k;
    data->l = l;
    data->failed = false;
    data->message = "";
    for (int i = 0; i < nlabels; i++)
      data->labels.append(labels[i]);
    delete [] labels;
  } else {
    data->failed = true;
    data->message = message;
  }
  return data;
}

DLVPY_model::slab_data *DLVPY_model::update_slab_hkl(const int h, const int k, const int l,
						     const bool conventional, const double tol)
{
  DLVreturn_type ok;
  DLV::string *labels;
  int nlabels;
  char message[256];
  slab_data *data = new slab_data;
  ok = DLV::update_slab(h, k, l, conventional, tol, data->nlayers,
			labels, nlabels, message, 256);
  if (ok == DLV_OK) {
    data->failed = false;
    data->h = h;
    data->k = k;
    data->l = l;
    data->message = "";
    for (int i = 0; i < nlabels; i++)
      data->labels.append(labels[i]);
    delete [] labels;
  } else {
    data->failed = true;
    data->message = message;
  }
  return data;
}

std::string DLVPY_model::update_slab_termination(const int term)
{
  char message[256];
  if (DLV::update_slab_term(term, message, 256) == DLV_OK)
    return "";
  else if (strlen(message) > 0)
    return message;
  else
    return "Update slab termination failed";
}

std::string DLVPY_model::update_slab_layers(const int nlayers)
{
  char message[256];
  if (DLV::update_slab(nlayers, message, 256) == DLV_OK)
    return "";
  else if (strlen(message) > 0)
    return message;
  else
    return "Update slab layers failed";
}

std::string DLVPY_model::add_vacuum_gap(const char name[], const double c_axis, const double origin)
{
  char message[256];
  if (DLV::vacuum_gap(name, c_axis, origin, message, 256) == DLV_OK)
    return "";
  else if (strlen(message) > 0)
    return message;
  else
    return "Add vacuum gap failed";
}

std::string DLVPY_model::update_vacuum_gap(const double c_axis, const double origin)
{
  char message[256];
  if (DLV::update_vacuum(c_axis, origin, message, 256) == DLV_OK)
    return "";
  else if (strlen(message) > 0)
    return message;
  else
    return "Update vacuum gap failed";
}

BOOST_PYTHON_MODULE(DLV_MODULE_NAME)
{
  using namespace boost::python;

  class_<DLVPY_model::op_result_t>("op_result_type")
    .def_readonly("op", &DLVPY_model::op_result_t::op)
    .def_readwrite("message", &DLVPY_model::op_result_t::message);

  def("load_cif", DLVPY_model::load_cif,
      boost::python::return_value_policy<boost::python::manage_new_object>());

  class_<DLVPY_model::slab_data>("slab_data")
    .def_readonly("h", &DLVPY_model::slab_data::h)
    .def_readonly("k", &DLVPY_model::slab_data::k)
    .def_readonly("l", &DLVPY_model::slab_data::l)
    .def_readonly("nlayers", &DLVPY_model::slab_data::nlayers)
    .def_readonly("labels", &DLVPY_model::slab_data::labels)
    .def_readonly("failed", &DLVPY_model::slab_data::failed)
    .def_readonly("message", &DLVPY_model::slab_data::message);

  def("cut_slab", DLVPY_model::cut_slab, boost::python::return_value_policy<boost::python::manage_new_object>());
  def("update_slab_hkl", DLVPY_model::update_slab_hkl, boost::python::return_value_policy<boost::python::manage_new_object>());
  //def("update_slab", update_slab_6args);
  def("update_slab_layers", DLVPY_model::update_slab_layers);
  def("update_slab_termination", DLVPY_model::update_slab_termination);

  def("add_vacuum", DLVPY_model::add_vacuum_gap);
  def("update_vacuum", DLVPY_model::update_vacuum_gap);

  /*
  def("create_supercell", DLV::create_supercell);
  def("update_supercell", DLV::update_supercell);
  def("delete_symmetry", DLV::delete_symmetry);
  def("view_to_molecule", DLV::view_to_molecule);
  def("edit_lattice", DLV::edit_lattice);
  def("update_edit_lattice", DLV::update_edit_lattice);
  def("shift_origin", DLV::shift_origin);
  def("update_origin", DLV::update_origin);
  def("update_origin_symm", DLV::update_origin_symm);
  def("cut_cluster", DLV::cut_cluster);
  def("update_cluster", DLV::update_cluster);
  def("cut_surface", DLV::cut_surface);
  def("update_surface", update_surface_9args);
  def("update_surface", update_surface_5args);
  def("update_surface", update_surface_3args);
  def("cut_salvage", DLV::cut_salvage);
  def("update_salvage", update_salvage_tol);
  def("update_salvage", update_salvage_layers);
  def("multilayer_create", DLV::multilayer_create);
  def("multilayer_update", DLV::multilayer_update);
  def("fill_geometry_obj", DLV::fill_geometry_obj);
  //def("", DLV::);
  */

#ifdef ENABLE_DLV_GRAPHICS
  
  def("update_current_cell", DLV::update_current_cell);
  def("update_current_lattice", DLV::update_current_lattice);
  def("update_current_atoms", DLV::update_current_atoms);
  def("update_current_bonds", DLV::update_current_bonds);

  /*
  def("update_origin_atoms", DLV::update_origin_atoms);
  def("edit_atom", DLV::edit_atom);
  def("update_atom", DLV::update_atom);
  def("edit_props", DLV::edit_props);
  def("update_props", DLV::update_props);
  def("delete_atoms", DLV::delete_atoms);
  def("insert_atom", DLV::insert_atom);
  def("update_inserted_atom", DLV::update_inserted_atom);
  def("switch_atom_coords", DLV::switch_atom_coords);
  def("update_from_selections", DLV::update_from_selections);
  def("set_edit_transform", DLV::set_edit_transform);
  def("insert_model", DLV::insert_model);
  def("update_inserted_model", DLV::update_inserted_model);
  def("position_from_selections", DLV::position_from_selections);
  def("move_atoms", DLV::move_atoms);
  def("update_move_atoms", DLV::update_move_atoms);
  //def("", DLV::);
  */
  
#endif // ENABLE_DLV_GRAPHICS
}
