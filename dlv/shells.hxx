
#ifndef DLV_MODEL_SHELL
#define DLV_MODEL_SHELL

namespace DLV {

  struct shell_data {
    int_g atomic_number;
    real_g number_of_atoms;
    real_g radius;
    //real_g debye_waller;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class shell_model : public model {
  public:
    shell_model(const string model_name);
    ~shell_model();

    bool is_shell() const;

    void add_shells(const int_g types[], const real_g numbers[],
		    const real_g radii[], const int_g nshells);
    void update_shells(const int_g types[], const real_g numbers[],
		       const real_g radii[], const int_g nshells);
    void complete(const bool only_atoms = false);
    model *duplicate_model(const string label) const;
    int_g get_number_of_periodic_dims() const;
    int_g get_lattice_type() const;
    int_g get_lattice_centring() const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;
    void get_conventional_lattice(coord_type a[3], coord_type b[3],
				  coord_type c[3]) const;
    int_g get_hermann_mauguin_group(string &name) const;
    void get_point_group(string &base, int_g &n, string &tail) const;
    int_g get_number_of_sym_ops() const;
    void get_cart_rotation_operators(real_l r[][3][3], const int_g nops) const;
    void get_frac_rotation_operators(real_l r[][3][3], const int_g nops) const;
    void get_cart_translation_operators(real_l t[][3], const int_g nops) const;
    void get_frac_translation_operators(real_l t[][3], const int_g nops) const;
    int_g get_number_of_asym_atoms() const;
    int_g get_number_of_primitive_atoms() const;
    void get_asym_atom_types(int_g atom_types[], const int_g n) const;
    void get_primitive_atom_types(int_g atom_types[], const int_g n) const;
    void get_asym_atom_cart_coords(coord_type coords[][3], const int_g n) const;
    void get_asym_atom_frac_coords(coord_type coords[][3], const int_g n) const;
    void get_primitive_atom_cart_coords(coord_type coords[][3],
					const int_g n) const;
    void get_primitive_atom_frac_coords(coord_type coords[][3],
					const int_g n) const;

    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;

    int_g get_model_type() const;
    void set_crystal03_lattice_type(const int_g lattice, const int_g centre);

    void get_reciprocal_lattice(coord_type a[3], coord_type b[3],
				coord_type c[3]) const;

#ifdef ENABLE_DLV_GRAPHICS
    render_parent *create_render_parent(render_parent *p, const bool copy_cell,
					const bool copy_wavefn);
    DLVreturn_type render_r(render_parent *p, const model *m,
			    char message[], const int_g mlen);
    DLVreturn_type update_shells(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type render_k(render_parent *p, char message[], const int_g mlen);
    class toolkit_obj get_r_ui_obj() const;
    DLVreturn_type update_cell(render_parent *p,
			       char message[], const int_g mlen);
    DLVreturn_type update_atoms(const render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type update_lattice(render_parent *p, const bool kspace,
				  char message[], const int_g mlen);
    DLVreturn_type update_bonds(const render_parent *p,
				char message[], const int_g mlen) const;
    DLVreturn_type add_bond(render_parent *p, char message[], const int_g mlen);
    DLVreturn_type del_bond(render_parent *p, char message[], const int_g mlen);
    DLVreturn_type add_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type del_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type add_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type del_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type select_atom(render_parent *p, const real_g coords[3],
			       const int_g status, int_g &nselects,
			       char message[], const int_g mlen);
    DLVreturn_type deselect_all_atoms(render_parent *p,
				      char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    bool set_group(const int_g gp);
    bool set_group(const char gp[], const int_g setting = 0,
		   const int_g nsettings = 1, const bool hex = false);
    bool set_cartesian_sym_ops(const real_l rotations[][3][3],
			       const real_l translations[][3],
			       const int_g nops);
    bool set_fractional_sym_ops(const real_l rotations[][3][3],
				const real_l translations[][3],
				const int_g nops);
    bool set_primitive_lattice(const coord_type [3], const coord_type [3],
			       const coord_type [3]);
    bool set_lattice_parameters(const coord_type a, const coord_type b,
				const coord_type c, const coord_type alpha,
				const coord_type beta, const coord_type gamma);
    bool add_cartesian_atom(int_g &index, const int_g atomic_number,
			    const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const int_g atomic_number,
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);
    bool add_cartesian_atom(int_g &index, const char symbol[],
			    const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const char symbol[],
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);

  private:
    atom central_atom;
    std::list<shell_data> shells;
#ifdef ENABLE_DLV_GRAPHICS
    model_shell_r *render_object;
#endif // ENABLE_DLV_GRAPHICS

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  model *create_shells(const string model_name);

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::shell_model)
#endif // DLV_USES_SERIALIZE

inline DLV::shell_model::shell_model(const string model_name)
  : model(model_name)
#ifdef ENABLE_DLV_GRAPHICS
  , render_object(0)
#endif // ENABLE_DLV_GRAPHICS
{
}

#endif // DLV_MODEL_SHELL
