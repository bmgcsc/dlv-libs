
#ifndef DLV_SYMMETRY
#define DLV_SYMMETRY

namespace DLV {

  enum rotation_type {
    identity, R180, R120, R90, R72, R60, unknown_rotation
  };

  enum reflection_type {
    no_reflection, inversion, mirror, improper_rotation, unknown_mirror
  };

  // Todo - is this correct
  enum surface_class {
    parallelpiped = 0, rectangular, triangular, square, six_fold
  };

  enum crystal_class {
    triclinic = 0, monoclinic, orthorhombic, tetragonal, trigonal, hexagonal,
    cubic
  };

  enum lattice_centering {
    primitive = 0, a_face, b_face, c_face, face_centred, body_centred,
    rhombohedral
  };

  class symmetry_data {
  public:
    symmetry_data();
    virtual ~symmetry_data();

    virtual void set_crystal03_lattice(const int_g l, const int_g c);
    virtual void set_lattice_and_centre(const int_g l, const int_g c);
    virtual bool set_group(const int_g gp) = 0;
    virtual bool set_group(const char gp[]) = 0;
    int_g get_number_of_sym_ops() const;
    bool set_cartesian_ops(const real_l rotations[][3][3],
			   const real_l translations[][3], const int_g nops);
    bool set_fractional_trans_ops(const real_l rotations[][3][3],
				  const real_l translations[][3],
				  const int_g nops);
    void get_cartesian_ops(real_l rotations[][3][3],
			   real_l translations[][3], const int_g nops) const;
    int_g get_number_no_trans_ops() const;
    void get_cartesian_no_trans_ops(real_l r[][3][3], const int_g nops) const;
    void copy_cartesian_ops(const symmetry_data &sym);
    void copy_fractional_ops(const symmetry_data &sym);
    void rotate_cartesian_ops(const symmetry_data &sym, const real_l r[3][3]);
    void add_conv_cell_ops(const symmetry_data &sym, const coord_type va[3],
			   const coord_type vb[3], const coord_type vc[3],
			   const int_g dim);
    void build_supercell_ops(const coord_type opa[3], const coord_type opb[3],
			     const coord_type opc[3], const coord_type va[3],
			     const coord_type vb[3], const coord_type vc[3],
			     const int_g dim);
    void shift_translations(const symmetry_data &sym, const real_l x,
			    const real_l y, const real_l z,
			    const coord_type a[3], const coord_type b[3],
			    const coord_type c[3], const int_g dim,
			    const bool fractional);
    void get_cartesian_rotations(real_l rotations[][3][3],
				 const int_g nops) const;
    void get_cartesian_translations(real_l translations[][3],
				    const int_g nops) const;
    void get_fractional_translations(real_l translations[][3],
				     const int_g nops) const;
    virtual void get_transform_to_prim_cell(coord_type t[3][3]) const;
    virtual void get_transform_to_prim_cell(int_g t[3][3]) const;
    virtual void get_transform_to_conv_cell(coord_type t[3][3]) const;
    virtual int_g get_number_conv_cell_translators() const;
    virtual void get_conv_cell_translators(real_l ops[][3]) const;
    virtual int_g get_lattice_type() const;
    virtual int_g get_centre_type() const;
    bool minimise_translation_ops(real_l p[3], const coord_type a[3],
				  const coord_type b[3], const coord_type c[3],
				  const int_g dim) const;
    void complete();
    virtual void complete_translators(const coord_type va[3],
				      const coord_type vb[3],
				      const coord_type vc[3]) = 0;

    void find_point_group(string &basename, int_g &n, string &suffix) const;
    virtual bool parse_operators(const string ops[], const int_g nops,
				 const int_g gp, const bool rhomb,
				 const bool prim) = 0;

    // Todo - hide?
    void generate_translations(const coord_type va[3], const coord_type vb[3],
			       const coord_type vc[3], const int_g dim);

  protected:
    void init_number_of_ops(const int_g nops);
    void set_cartesian(const real_l r[3][3], const rotation_type rot_id,
		       const reflection_type ref_id, const int_g i);
    void mult_cartesian(const real_l r[3][3], const rotation_type rot_id,
			const reflection_type ref_id, const int_g i,
			const int_g j);
    void set_fractional_operator(const real_l r[3][3], const real_l t[3],
				 const rotation_type rot_id,
				 const reflection_type ref_id,
				 const int_g index);
    bool set_space_group(const string gp, int_g &number);
    void build_table(const int_g n, const int_g size,
		     const bool translate = false);
    bool parse_vol_operators(const string ops[], const int_g nops,
			     const int_g gp, const bool rhomb,
			     const bool prim, const int_g prim_ops);

  private:
    class symmetry_operator *sym_ops;
    int_g number_of_sym_ops;
    bool cartesian_translators;
    bool fractional_translators;
    // Todo - group id and setting
    bool hm_ok;
    //string hermann_maguin;
    bool sh_ok;
    //string schoenflies;

    static bool check_ops(const symmetry_operator ops[], const int_g start,
			  const int_g nops, const int_g dim);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class point_symmetry : public symmetry_data {
  public:
    bool set_group(const int_g gp);
    bool set_group(const char gp[]);
    bool set_point_group(const char gp[]);
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3]);
    bool parse_operators(const string ops[], const int_g nops,
			 const int_g gp, const bool rhomb, const bool prim);

  private:
    void add_5fold_axis(const int_g start, const real_l reflect[3][3],
			const reflection_type ref_id);
    void add_6fold_axis(const int_g start, const real_l reflect[3][3],
			const reflection_type ref_id);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class line_symmetry : public symmetry_data {
  public:
    bool set_group(const int_g gp);
    bool set_group(const char gp[]);
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3]);
    bool parse_operators(const string ops[], const int_g nops,
			 const int_g gp, const bool rhomb, const bool prim);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class plane_symmetry : public symmetry_data {
  public:
    plane_symmetry();
    void set_crystal03_lattice(const int_g l, const int_g c);
    void set_lattice_and_centre(const int_g l, const int_g c);
    bool set_group(const int_g gp);
    bool set_group(const char gp[]);
    void copy_lattice_type(const symmetry_data &sym);
    void get_transform_to_prim_cell(coord_type t[3][3]) const;
    void get_transform_to_prim_cell(int_g t[3][3]) const;
    void get_transform_to_conv_cell(coord_type t[3][3]) const;
    int_g get_number_conv_cell_translators() const;
    void get_conv_cell_translators(real_l ops[][3]) const;
    int_g get_lattice_type() const;
    int_g get_centre_type() const;
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3]);
    bool parse_operators(const string ops[], const int_g nops,
			 const int_g gp, const bool rhomb, const bool prim);

  protected:
    void set_lattice_data(const int_g num, const char c1, const char c2);

  private:
    surface_class lattice;
    lattice_centering centre;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class volume_symmetry : public symmetry_data {
  public:
    volume_symmetry();
    void set_crystal03_lattice(const int_g l, const int_g c);
    void set_lattice_and_centre(const int_g l, const int_g c);
    bool set_group(const int_g gp);
    bool set_group(const char gp[]);
    void copy_lattice_type(const symmetry_data &sym);
    void get_transform_to_prim_cell(coord_type t[3][3]) const;
    void get_transform_to_prim_cell(int_g t[3][3]) const;
    void get_transform_to_conv_cell(coord_type t[3][3]) const;
    int_g get_number_conv_cell_translators() const;
    void get_conv_cell_translators(real_l ops[][3]) const;
    int_g get_lattice_type() const;
    int_g get_centre_type() const;
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3]);
    bool parse_operators(const string ops[], const int_g nops,
			 const int_g gp, const bool rhomb, const bool prim);

  protected:
    void set_lattice_data(const int_g num, const char c);

  private:
    crystal_class lattice;
    lattice_centering centre;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::symmetry_data)
BOOST_CLASS_EXPORT_KEY(DLV::symmetry_data)
#endif // DLV_USES_SERIALIZE

inline DLV::symmetry_data::symmetry_data() : sym_ops(0), number_of_sym_ops(0),
					     cartesian_translators(false),
					     fractional_translators(false),
					     hm_ok(false), sh_ok(false)
{
}

inline DLV::plane_symmetry::plane_symmetry()
  : lattice(parallelpiped), centre(primitive)
{
}

inline DLV::volume_symmetry::volume_symmetry()
  : lattice(triclinic), centre(primitive)
{
}

#endif // DLV_SYMMETRY
