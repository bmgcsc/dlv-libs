
#include <ctime>
#include <climits> // PATH_MAX
#ifdef WIN32
#  include <windows.h>
#  include <direct.h>
#  include <io.h>
#  include <sys/types.h>
#  define chdir _chdir
#  define PATH_MAX 1024
#else
#  include <unistd.h>
#endif // WIN32
#include <sys/stat.h>
#include "types.hxx"
#include "boost_lib.hxx"
#include "utils.hxx"

DLV::string DLV::current_date_time()
{
  time_t date;
  date = time(0);
  return ctime(&date);
}

DLV::string DLV::get_current_directory()
{
  char buff[PATH_MAX];
  (void) getcwd(buff, PATH_MAX);
  return buff;
}

DLV::string DLV::get_directory_path(const string filename,
				    const bool terminate)
{
  int_g i = (int)filename.length() - 1;
  while (i > 0 and filename[i] != DIR_SEP_CHR)
    i--;
  string name = filename.substr(0, i);
  if (terminate and name.length() > 0) {
    if (name[name.length() - 1] != DIR_SEP_CHR)
      name += DIR_SEP_CHR;
  }
  return name;
}

DLV::string DLV::get_file_name(const string filename)
{
  int_g i = (int)filename.length() - 1;
  while (i > 0 and filename[i] != DIR_SEP_CHR)
    i--;
  if (filename[i] == DIR_SEP_CHR)
    i++;
  string name = filename.substr(i);
  return name;
}

DLV::string DLV::base_file_name(const char filename[])
{
  int_g i = (int)strlen(filename) - 1;
  while (i > 0 and filename[i] != DIR_SEP_CHR)
    i--;
  if (filename[i] == DIR_SEP_CHR)
    i++;
  string basename = &filename[i];
  return basename;
}

bool DLV::change_directory(const char directory[])
{
  return (chdir(directory) == 0);
}

bool DLV::rename_directory(const char old_dir[], const char new_dir[])
{
  boost::filesystem::path odir(old_dir);
  boost::filesystem::path ndir(new_dir);
  try {
    boost::filesystem::rename(odir, ndir);
  }
  catch (boost::filesystem::filesystem_error &) {
    return false;
  }
  return true;
}

bool DLV::use_directory(const string name, const bool keep,
			char message[], const int_g len)
{
  boost::filesystem::path dname(name);
  message[0] = '\0';
  bool ok = false;
  if (boost::filesystem::exists(dname)) {
    if (boost::filesystem::is_directory(dname)) {
      if (boost::filesystem::is_empty(dname)) {
	if (keep)
	  ok = true;
	else {
#if BOOST_VERSION < 103800
	  // Don't use this for a code release!!!
	  ok = true;
	  boost::system::error_code ec;
	  boost::filesystem::remove(dname, ec);
#else
	  try {
	    ok = boost::filesystem::remove(dname);
	  }
	  catch (boost::filesystem::filesystem_error &) {
	    ok = false;
	    strncpy(message, "Directory tidyup failed", len);
	  }
#endif // BOOST_VERSION
	}
      } else
	strncpy(message, "Directory must be empty", len);
    } else
      strncpy(message, "Not a directory", len);
  } else {
    if (keep) {
      if (boost::filesystem::create_directory(dname))
	ok = true;
      else
	strncpy(message, "Directory creation failed", len);
    } else
      ok = true;
  }
  return ok;
}

bool DLV::delete_directory(const char directory[])
{
  boost::filesystem::path dir(directory);
  return (boost::filesystem::remove_all(dir) != 0);
}

void DLV::symlink(const string oldname, const string newname)
{
  boost::filesystem::path oname(oldname);
  boost::filesystem::path nname(newname);
  boost::filesystem::create_symlink(oname, nname);
}

void DLV::copy_file(const string oldname, const string newname)
{
  boost::filesystem::path oname(oldname);
  boost::filesystem::path nname(newname);
  boost::filesystem::copy_file(oname, nname);
}

bool DLV::check_filename(const char filename[], char message[],
			 const int_g mlen)
{
  if (filename[0] == '\0') {
    strncpy(message, "empty filename", mlen - 1);
    message[mlen - 1] = '\0';
    return false;
  } else
    return true;
}

bool DLV::open_file_read(std::ifstream &input, const char filename[],
			 char message[], const int_g mlen)
{
  input.clear();
  input.open(filename);
  if (input.fail()) {
    strncpy(message, "open file failed", mlen - 1);
    message[mlen - 1] = '\0';
    return false;
  }
  if (input.eof()) {
    strncpy(message, "empty file", mlen - 1);
    message[mlen - 1] = '\0';
    return false;
  }
  return true;
}

bool DLV::open_file_write(std::ofstream &output, const char filename[],
			  char message[], const int_g mlen)
{
  output.open(filename);
  if (output.fail()) {
    strncpy(message, "open file failed", mlen - 1);
    message[mlen - 1] = '\0';
    return false;
  }
  return true;
}

bool DLV::open_file_writeb(std::ofstream &output, const char filename[],
			   char message[], const int_g mlen)
{
  output.open(filename, std::ios::binary);
  if (output.fail()) {
    strncpy(message, "open file failed", mlen - 1);
    message[mlen - 1] = '\0';
    return false;
  }
  return true;
}

DLV::string DLV::add_full_path(const string filename)
{
  string name = filename;
  if (!is_absolute_path(name)) {
    name = get_current_directory();
    name += DIR_SEP_CHR;
    name += filename;
  }
  return name;
}

#ifdef __SUNPRO_CC

bool DLV::setenviron(const char var[], const char val[])
{
  char buff[1024];
  strcpy(buff, var);
  strcat(buff, "=");
  strcat(buff, val);
  return (putenv(buff) == 0);
}

#endif // SUNPRO

#ifdef WIN32

bool DLV::file_exists(const char filename[])
{
  return (_access(filename, 0) == 0);
}

bool DLV::setenviron(const char var[], const char val[])
{
  LPCSTR varA = var;
  LPCSTR valA = val;
  return (SetEnvironmentVariableA(varA, valA) != 0);
}

bool DLV::make_directory(const char directory[], const bool exist_fails)
{
  if (_mkdir(directory) == -1) {
    if (exist_fails)
      return false;
    else {
      struct _stat sbuf;
      _stat(directory, &sbuf);
      return ((bool)(sbuf.st_mode & _S_IFDIR));
    }
  } else
    return true;
}
DLV::string DLV::get_user_id()
{
  // Todo
  return "uid";
}

DLV::string DLV::get_host_name()
{
  // Todo
  return "MS-Windows";
}

DLV::int_g DLV::system(const char command[])
{
  return std::system(command);
}

#else

bool DLV::file_exists(const char filename[])
{
  return (access(filename, F_OK) == 0);
}

bool DLV::make_directory(const char directory[], const bool exist_fails)
{
  if (mkdir(directory, 0750) == -1) {
    if (exist_fails)
      return false;
    else {
      struct stat sbuf;
      stat(directory, &sbuf);
      return ((bool)(S_ISDIR(sbuf.st_mode)));
    }
  } else
    return true;
}

DLV::string DLV::get_user_id()
{
  string uid = getenv("USER");
  return uid;
}

DLV::string DLV::get_host_name()
{
  char name[256];
  gethostname(name, 256);
  return name;
}

DLV::int_g DLV::system(const char command[])
{
  return std::system(command);
}

#endif // WIN32

#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::colour_data::serialize(Archive &ar, const unsigned int version)
{
  ar & red;
  ar & green;
  ar & blue;
}

DLV_NORMAL_EXPLICIT(DLV::colour_data)

#endif // DLV_USES_SERIALIZE
