
#ifndef DLV_MISC_TYPES

#include <cstring>

// Todo - may not be true with MS-VCC 7
#ifdef WIN32
#  include <fstream>
#  define DIR_SEP_CHR '\\'
#  define DIR_SEP_STR "\\"
#  define REMOTE_DIR_CHAR '/'

#  define snprintf _snprintf

  // Todo - must be a better way
#  define MAX_PATH_LEN 1024
#else
#  include <unistd.h>
#  include <iostream>
#  include <fstream>
#  define DIR_SEP_CHR '/'
#  define DIR_SEP_STR "/"
#  define REMOTE_DIR_CHAR '/'

  // Todo - must be a better way
#  define MAX_PATH_LEN 1024
#endif // WIN32

namespace DLV {

  class colour_data {
  public:
    real_g red, green, blue;

    colour_data(const real_g r = 0.0, const real_g g = 0.0,
		const real_g b = 0.0);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  extern string current_date_time();
  extern string get_current_directory();
  extern string get_directory_path(const string filename,
				   const bool terminate = false);
  extern string get_file_name(const string filename);
  extern string base_file_name(const char filename[]);
  extern string add_full_path(const string filename);
  extern bool file_exists(const char filename[]);
  extern bool make_directory(const char directory[],
			     const bool exist_fails = true);
  extern bool change_directory(const char directory[]);
  extern bool rename_directory(const char old_dir[], const char new_dir[]);
  extern bool use_directory(const string name, const bool keep,
			    char message[], const int_g len);
  extern bool delete_directory(const char directory[]);
  extern void symlink(const string oldname, const string newname);
  extern void copy_file(const string oldname, const string newname);
  extern bool check_filename(const char filename[], char message[],
			     const int_g mlen);
  extern bool open_file_read(std::ifstream &input, const char filename[],
			     char message[], const int_g mlen);
  extern bool open_file_write(std::ofstream &output, const char filename[],
			      char message[], const int_g mlen);
  extern bool open_file_writeb(std::ofstream &output, const char filename[],
			       char message[], const int_g mlen);
  extern bool setenviron(const char var[], const char val[]);
  extern string get_user_id();
  extern string get_host_name();
  extern bool is_absolute_path(const string name);

#ifndef WIN32
  extern bool check_exec_access(const string filename);
  extern int_g system(const char command[]);
#else
  extern int_g system(const char command[]);
#endif // !WIN32

}

inline DLV::colour_data::colour_data(const real_g r, const real_g g,
				     const real_g b)
  : red(r), green(g), blue(b)
{
}

#ifdef WIN32

inline bool DLV::is_absolute_path(const string name)
{
  return (name[0] == DIR_SEP_CHR or name[1] == ':');
}

#else

inline bool DLV::check_exec_access(const string filename)
{
  return (access(filename.c_str(), X_OK) == 0);
}

inline bool DLV::is_absolute_path(const string name)
{
  return (name[0] == DIR_SEP_CHR);
}

#endif // WIN32

#if !defined(__SUNPRO_CC) && !defined(WIN32)

inline bool DLV::setenviron(const char var[], const char val[])
{
  return (setenv(var, val, 1) == 0);
}

#endif // !SUNPRO_CC

#endif // DLV_MISC_TYPES
