
#include "../dlv/types.hxx"
#include "../graphics/calculations.hxx"

DLV::string DLV::get_code_path(const char code[])
{
  return "";
}

void DLV::show_job_panel()
{
}

void DLV::clear_job_list()
{
}

void DLV::add_to_job_list(const string name, const string state,
			  const int index)
{
}

void DLV::update_job_status(const string state, const int index)
{
}

void DLV::remove_from_job_list(const int index)
{
}
