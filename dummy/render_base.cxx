
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_AVS_GRAPHICS) || defined(DLV_USES_VTK_GRAPHICS)
#    error "This dummy graphics interface should only be used when no graphics library is defined"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS should be defined when building this dummy graphics interface"
#endif // GRAPHICS check

#include <cmath>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"

template <> DLV::toolkit_obj *DLV::render_parent::parent = nullptr;
template <> DLV::render_parent *DLV::render_parent::serialize_obj = nullptr;

template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::render_parent_templ()
{
}

template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::~render_parent_templ()
{
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::~render_atoms_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::~render_outline_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::~render_shells_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::render_atoms_templ(const char name[])
  : obj(nullptr)
{
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::render_outline_templ(const char name[])
  : obj(nullptr)
{
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::render_shells_templ(const char name[])
  : obj(nullptr)
{
}

template <class scene_t, class parent_t>
const char *DLV::render_parent_templ<scene_t, parent_t>::get_name() const
{
  return "";
}

template <class scene_t, class parent_t>
DLV::toolkit_obj *DLV::render_parent_templ<scene_t, parent_t>::get_parent() const
{
  return parent;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_background(float c[3]) const
{
  c[0] = 1.0;
  c[1] = 1.0;
  c[2] = 1.0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_inverted_background(float c[3]) const
{
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

template <class scene_t, class parent_t, class atoms_t>
parent_t *DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class outline_t>
parent_t *DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class shells_t>
parent_t *DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::attach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_k3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::detach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_k3D(scene_t *view)
{
  return DLV_OK;
}

// Todo - some of this is common to render_atoms
template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::detach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::detach_r3D(scene_t *view)
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_rspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_rspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_rspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::attach_kui(toolkit_obj &k_ui) const
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_rui(toolkit_obj &r_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
DLVreturn_type DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_kui(toolkit_obj &k_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class outline_t>
DLVreturn_type DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_rui(toolkit_obj &r_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t>
DLVreturn_type DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_rui(toolkit_obj &r_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
				      const bool copy_wavefn)
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
					const bool)
{
}

template <class scene_t, class parent_t, class shells_t>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool, const bool)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_size(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::select_data_object(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_size(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_label(const string label, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_label(const string label, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_size(const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_size(const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_label(const string label,
					    const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_label(const string label,
					    const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_display_type(const int v, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_type(const int v, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_vector(const bool v,
					     const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_vector(const bool v,
					     const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_display_list(const string name,
					  const int id, const int index) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int object) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int first,
					    const int last) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_display_list() const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_data_panel()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::stop_animation()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_animate_panel()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_edit_list(const string name,
				       const int id, const int index) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_edit_list(const int object) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_edit_list() const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_size(const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_size(const int size)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_name(const string, const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_name(const string name,
					       const int index)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_cell_data(int &na, int &nb, int &nc,
								bool &centre_cell, bool &conv_cell,
								bool &edges, double &tol) const
{
  // a bug
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_transforms(const int n,
					const float transforms[][3])
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_cell_data(int &na, int &nb, int &nc,
									bool &centre_cell,
									bool &conv_cell,
									bool &edges,
									double &tol) const
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_transforms(const int n,
				       const float transforms[][3])
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_cell_data(int &na, int &nb, int &nc,
				      bool &centre_cell,
				      int &cell_type, double &tol) const
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::set_transforms(const int n,
				       const float transforms[][3])
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_wavefn(const int valence_min,
					    const int valence_max,
					    const int nbands,
					    const int lattice,
					    const int centre, const bool spin,
					    const int kpoints[][3],
					    const int nkpoints, const int sa,
					    const int sb, const int sc)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_scf()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_tddft()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_ONETEP_scf()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_wavefn(const int valence_min,
					   const int valence_max,
					   const int nbands,
					   const int lattice,
					   const int centre, const bool spin,
					   const int kpoints[][3],
					   const int nkpoints, const int sa,
					   const int sb, const int sc)
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_scf()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_tddft()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_ONETEP_scf()
{
}

template class DLV::render_parent_templ<void, void>;
template class DLV::render_atoms_templ<void, void, void>;
template class DLV::render_outline_templ<void, void, void>;
template class DLV::render_shells_templ<void, void, void>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_atoms(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_outline(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_shells(name.c_str());
      t->set_serialize_obj();
    }

  }
}

template <class scene_t, class parent_t> template <class Archive>
void DLV::render_parent_templ<scene_t, parent_t>::serialize(Archive &ar, const unsigned int version)
{
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_parent)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_atoms)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_outline)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_shells)

#endif // DLV_USES_SERIALIZE
