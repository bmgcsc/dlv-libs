
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#  include "../graphics/calculations.hxx"
#  include "../graphics/excurv.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/job.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "calcs.hxx"

DLV::string EXCURV::calculation::binary_name = "dl_excurv" LOCAL_EXEC_SUFFIX;
DLV::string EXCURV::calculation::def_name = "excurve.def";
DLV::string EXCURV::calculation::executable;
DLV::string EXCURV::calculation::def_file;
// Allows us to ignore calls when panel is opened and data re-initialised
EXCURV::calculation *EXCURV::calculation::base_calc = 0;
DLV::text_buffer *EXCURV::calculation::viewer = 0;

namespace {

  EXCURV::int_g my_counter = 0;

}

DLV::text_buffer *EXCURV::calculation::get_viewer()
{
  return viewer;
}

DLV::string EXCURV::calculation::get_path()
{
#ifdef ENABLE_DLV_GRAPHICS
  return DLV::get_code_path("EXCURV");
  #else
  return "";
#endif // ENABLE_DLV_GRAPHICS
}

bool EXCURV::calculation::process_socket_data(char data[], const bool suppress)
{
  if (strlen(data) > 0) {
#ifdef ENABLE_DLV_GRAPHICS
    if (!suppress)
      set_error("Possible EXCURV bug", false, false);
#endif // ENABLE_DLV_GRAPHICS
    return false;
  } else {
#ifdef ENABLE_DLV_GRAPHICS
    clear_error(false, false);
#endif // ENABLE_DLV_GRAPHICS
    return true;
  }
}

DLVreturn_type EXCURV::calculation::open(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  DLV::model *model = get_current_model();
  DLV::string excurv_path;
  if (model != 0) {
    excurv_path = get_path();
    if (excurv_path.length() > 0) {
      if (excurv_path[excurv_path.length() - 1] != DIR_SEP_CHR)
	excurv_path += DIR_SEP_STR;
      executable = excurv_path + binary_name;
      def_file = excurv_path + def_name;
    } else {
      executable = binary_name;
      def_file = def_name;
    }
    add_data_file(0, def_file, def_name, true, true);
    add_job_error_file("", "err", "dlv_errors", true, true);
    add_output_file(0, "lis", "exout98.lis", true, true);
    add_output_file(1, "par", "recover.par", true, true);
    DLV::text_buffer *data = new DLV::text_buffer("EXCURV", "EXCURV",
						  "output log");
    if (execute(executable, data, message, mlen)) {
      my_counter = 0;
      base_calc = this;
      viewer = data;
      attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      render_data(data);
#endif // ENABLE_DLV_GRAPHICS
      char buff[1280];
      strcpy(buff, "DLV\n");
      ok = write_connection(buff);
      if (ok == DLV_OK) {
	read_connection(0);
	snprintf(buff, 1280, "PATH\n%s\n", excurv_path.c_str());
	ok = write_connection(buff);
	if (ok == DLV_OK)
	  read_connection(0);
      }
    } else
      ok = DLV_ERROR;
  }
  return ok;
}

DLVreturn_type EXCURV::calculation::close()
{
  DLVreturn_type ok = DLV_OK;
  if (base_calc != 0) {
    ok = base_calc->write_connection("END\n");
    if (ok == DLV_OK) {
      base_calc->read_connection(0, false);
    }
    // Todo - anything else?
    base_calc->close_connection();
    base_calc = 0;
    viewer = 0;
  }
  return ok;
}

bool EXCURV::calculation::reload_data(DLV::data_object *data,
				      char message[], const int_g mlen)
{
  strncpy(message, "BUG: attempt to reload EXCURV data", mlen);
  return false;
}

void EXCURV::calculation::recover()
{
  // Dummy
}

DLV::operation *EXCURV::read_experimental_data::create(const char filename[],
						       const int_g atom,
						       const char edge[],
						       const int_g freq,
						       const int_g xcol,
						       const int_g ycol,
						       const int_g kweight,
						       bool &started,
						 const DLV::job_setup_data &j,
						       char message[],
						       const int_g mlen)
{
  read_experimental_data *op = new read_experimental_data(filename, atom, edge,
							  freq, xcol, ycol,
							  kweight);
  if (op == 0)
    strncpy(message, "Failed to allocate EXCURV::read_expt operation", mlen);
  else {
    // May be it should be when the central atom is selected?
    op->attach();
    DLVreturn_type ok = DLV_OK;
    bool init_k = false;
    if (!op->has_connection()) {
      ok = op->open(message, mlen);
      init_k = true;
      started = (ok == DLV_OK);
    } else
      started = true;
    if (ok == DLV_OK) {
      char buff[1024];
      if (init_k) {
	snprintf(buff, 1024, "SET WEIGHT %d\n", kweight + 1);
	ok = op->write_connection(buff);
	if (ok == DLV_OK)
	  op->read_connection(0);
      }
      snprintf(buff, 1024, "READ EXP\n%s\n%d\n%1d%1d\n%s %s\n0\n1\n",
	       filename, freq, xcol, ycol,
	       DLV::atom_type::get_atomic_symbol(atom).c_str(), edge);
      ok = op->write_connection(buff);
      //cmd_type = dlexcurv_expt;
      if (ok == DLV_OK)
	op->read_connection(op);
    }
  }
  return op;
}

DLV::string EXCURV::read_experimental_data::get_name() const
{
  return ("EXCURV read experimental data");
}

bool
EXCURV::read_experimental_data::process_socket_data(char data[],
						    const bool suppress)
{
  if (strlen(data) > 0) {
#ifdef ENABLE_DLV_GRAPHICS
    if (!suppress)
      set_error("Unexpected data return by read_expt", true, false);
#endif // ENABLE_DLV_GRAPHICS
    return false;
  } else {
#ifdef ENABLE_DLV_GRAPHICS
    clear_error(true, false);
#endif // ENABLE_DLV_GRAPHICS
    return true;
  }
}

DLV::operation *EXCURV::potential_and_phase::create(const int_g central_atom,
						    const int_g central_neighb,
						    const int_g natoms,
						    const int_g atoms[],
						    const int_g neighbours[],
						    const bool constant_V0,
						    const int_g calc_method,
						    char message[],
						    const int_g mlen)
{
  if (is_connected()) {
    potential_and_phase *op = new potential_and_phase(central_atom,
						      central_neighb, natoms,
						      atoms, neighbours,
						      constant_V0,
						      calc_method);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::potentials_and_phase operation",
	      mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[1024];
	// Set up the list of atoms - excited central atom!
	snprintf(buff, 1024, "CHANGE ATOM1 %s\n",
		 DLV::atom_type::get_atomic_symbol(central_atom).c_str());
	ok = op->write_connection(buff);
	if (ok == DLV_OK)
	  op->read_connection(0);
	if (ok != DLV_OK)
	  return op;
	int_g i, j;
	int_g n = natoms;
	for (i = 0; i < n; i++) {
	  j = atoms[i];
	  snprintf(buff, 1024, "CHANGE ATOM%1d %s\n", i + 2,
		   DLV::atom_type::get_atomic_symbol(j).c_str());
	  ok = op->write_connection(buff);
	  if (ok == DLV_OK)
	    op->read_connection(0);
	  if (ok != DLV_OK)
	    return op;
	}
	// Set constant V0 or not
	if (constant_V0)
	  strcpy(buff, "SET CONSTANT V\n");
	else
	  strcpy(buff, "SET CONSTANT NONE\n");
	ok = op->write_connection(buff);
	if (ok == DLV_OK)
	  op->read_connection(0);
	if (ok != DLV_OK)
	  return op;
	// Now issue the command to calculate the potentials.
	snprintf(buff, 1024, "CAL POT\nc\n%d\n%d\n",
		 central_neighb + 1, calc_method);
	char text[128];
	for (i = 0; i < n; i++) {
	  //j = neighbours.get_array_val(i);
	  j = neighbours[i];
	  j = j + 1;
	  snprintf(text, 128, "%d\n", j);
	  strcat(buff, text);
	}
	// Send command list
	ok = op->write_connection(buff);
	//cmd_type = dlexcurv_pot_phase;
	if (ok == DLV_OK) {
	  op->read_connection(0);
	  // Set up phase shifts - accept the 2 default inputs.
	  strcpy(buff, "CAL PHASE\n\n\n");
	  ok = op->write_connection(buff);
	  //cmd_type = dlexcurv_pot_phase;
	  if (ok == DLV_OK)
	    op->read_connection(op);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::potential_and_phase::get_name() const
{
  return ("EXCURV calculate potentials/phase shifts");
}

bool EXCURV::potential_and_phase::process_socket_data(char data[],
						      const bool suppress)
{
  if (strlen(data) > 0) {
#ifdef ENABLE_DLV_GRAPHICS
    if (!suppress)
      set_error("Unexpected data return by potentials", false, true);
#endif // ENABLE_DLV_GRAPHICS
    return false;
  } else {
#ifdef ENABLE_DLV_GRAPHICS
    clear_error(false, true);
#endif // ENABLE_DLV_GRAPHICS
    return true;
  }
}

DLV::operation *EXCURV::plot::create(const int_g x_value, const int_g y[],
				     const int_g ylen, char message[],
				     const int_g mlen)
{
  if (is_connected()) {
    plot *op = new plot(x_value);
    if (op == 0)
      strncpy(message, "Failed to allocate EXCURV::plot operation", mlen);
    else {
      op->attach();
      (void) op->inherit_connections();
      op->send(x_value, y, ylen, "", false, message, mlen);
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::plot::get_name() const
{
  return ("EXCURV plot graph");
}

void EXCURV::plot::send(const int_g x_value, const int_g y[], const int_g ylen,
			const char filename[], const bool print,
			char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (has_connection()) {
    char buff[1024];
    int_g n = ylen;
    int_g count = 0;
    for (int_g i = 0; i < n; i++) {
      if (y[i] == 1)
	count++;
    }
    if (count == 0) {
      strncpy(message, "No y axis values selected", mlen);
      ok = DLV_WARNING;
    } else if (count > 3) {
      strncpy(message, "More than 3 y values not currently supported", mlen);
      ok = DLV_WARNING;
    } else {
      if (print)
	snprintf(buff, 1024, "PRINT\n%s\n%d\n", filename, x_value + 1);
      else
	snprintf(buff, 1024, "PDLV\n%d\n%d\n", x_value + 1, count);
      char text[64];
      for (int_g i = 0; i < n; i++) {
	if (y[i] == 1) {
	  snprintf(text, 64, "%d\n", i + 1);
	  strcat(buff, text);
	}
      }
      if (print) {
	if (count < 3)
	  strcat(buff, "0\n");
      }
      ok = write_connection(buff);
      if (ok == DLV_OK) {
	if (!print) {
	  //cmd_type = dlexcurv_expt_plot;
	  columns = count;
	}
	if (print)
	  read_connection(0);
	else
	  read_connection(this);
      }
    }
  } else {
    strncpy(message, "EXCURV not running", mlen);
    ok = DLV_ERROR;
  }
}

bool EXCURV::plot::will_process_socket() const
{
  //fprintf(stderr, "Im a plot\n");
  return true;
}

bool EXCURV::print::will_process_socket() const
{
  return false;
}

// Reconstruct complete lines from the various read_socket calls,
// which could break the output anywhere.
bool EXCURV::plot::create_line(char *line, const DLV::string buff,
			       int_g &bufptr, bool &cont)
{
  int_g j = 0;
  if (cont) {
    j = (int)strlen(line);
    cont = false;
  }
  int_g len = (int)buff.length();
  while (bufptr < len and buff[bufptr] != '\n') {
    if (buff[bufptr] != 13) {
      line[j] = buff[bufptr];
      j++;
    }
    bufptr++;
  }
  line[j] = '\0';
  if (buff[bufptr] == '\n') {
    bufptr++;
    return true;
  } else {
    cont = true;
    return false;
  }
}

bool EXCURV::plot::process_socket_data(char data[], const bool suppress)
{
  static char linebuf[256];
  static bool incomplete_line = false;
  int_g bufptr = 0;
  int_g xptr = 0;
  //fprintf(stderr, "Do stuff\n%s\n", data);
  if (!got_lines) {
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      // Get number of info lines
      sscanf(linebuf, "%d", &nlines);
      got_lines = true;
    } else {
      data[0] = '\0';
      return false;
    }
  }
  if (nlines > 0 and clines != nlines) {
    while (clines < nlines) {
      if (create_line(linebuf, data, bufptr, incomplete_line)) {
	clines++;
      } else
	break;
    }
    if (clines != nlines)
      return false;
    xptr = bufptr;
  }
  if (!got_title) {
    // Title and headings.
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      int_g tptr = (int)strlen(linebuf);
      while ((tptr > 0) and (linebuf[tptr - 1] == ' '))
	tptr--;
      linebuf[tptr] = '\0';
      title = linebuf;
      got_title = true;
    } else {
      data[xptr] = '\0';
      return false;
    }
  }
  if (!got_x) {
    // x axis
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      int_g tptr = (int)strlen(linebuf);
      while ((tptr > 0) and (linebuf[tptr - 1] == ' '))
	tptr--;
      linebuf[tptr] = '\0';
      got_x = true;
      xaxis = linebuf;
    } else {
      data[xptr] = '\0';
      return false;
    }
  }
  if (!got_y) {
    // x axis
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      int_g tptr = (int)strlen(linebuf);
      while ((tptr > 0) and (linebuf[tptr - 1] == ' '))
	tptr--;
      linebuf[tptr] = '\0';
      got_y = true;
      yaxis = linebuf;
    } else {
      data[xptr] = '\0';
      return false;
    }
  }
  int_g i;
  for (i = 0; i < columns; i++) {
    if (!got_labels[i]) {
      if (create_line(linebuf, data, bufptr, incomplete_line)) {
	int_g tptr = (int)strlen(linebuf);
	while ((tptr > 0) and (linebuf[tptr - 1] == ' '))
	  tptr--;
	linebuf[tptr] = '\0';
	got_labels[i] = true;
	labels[i] = linebuf;
      } else
	break;
    }
  }
  if (i != columns) {
    data[xptr] = '\0';
    return false;
  }
  if (npoints == 0) {
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      if (sscanf(linebuf, "%d", &npoints) != 1)
	npoints = 0;
      if (npoints == 0) {
	//DLVerror("Read EXCURV Plot", "Problem reading number of points");
	//return DLV_STAT_FAILED;
	return true;
      }
      point = 0;
      x = new real_g[npoints];
      for (i = 0; i < columns; i++)
	y[i] = new real_g[npoints];
    } else {
      data[xptr] = '\0';
      return false;
    }
  }
  // Read points
  while (point < npoints) {
    if (create_line(linebuf, data, bufptr, incomplete_line)) {
      switch (columns) {
      case 1:
	sscanf(linebuf, "%f %f", &x[point], &y[0][point]);
	break;
      case 2:
	sscanf(linebuf, "%f %f %f", &x[point], &y[0][point],
	       &y[1][point]);
	break;
      default:
	sscanf(linebuf, "%f %f %f %f", &x[point], &y[0][point],
	       &y[1][point], &y[2][point]);
	break;
      }
      point++;
    } else
      break;
  }
  complete = (point == npoints);
  if (!complete) {
    data[xptr] = '\0';
    return false;
  }
  while ((nat_g)bufptr < strlen(data)) {
    data[xptr] = data[bufptr];
    bufptr++;
    xptr++;
  }
  data[xptr] = '\0';
  // We should have read all the data - now set up the plot.
  DLV::dos_plot *obj = new DLV::dos_plot("EXCURV", "EXCURV", this, columns);
  obj->set_grid(x, npoints, false);
  for (int_g i = 0; i < columns; i++)
    obj->add_plot(y[i], i, labels[i].c_str(), false);
  obj->set_xaxis_label(xaxis.c_str());
  obj->set_yaxis_label(yaxis.c_str());
  obj->set_title(title.c_str());
  attach_data(obj);
#ifdef ENABLE_DLV_GRAPHICS
  render_data(obj);
#endif // ENABLE_DLV_GRAPHICS
  return true;
}

DLV::operation *EXCURV::print::create(const int_g x_value, const int_g y[],
				      const int_g ylen, const char filename[],
				      char message[], const int_g mlen)
{
  if (is_connected()) {
    print *op = new print(x_value, filename);
    if (op == 0)
      strncpy(message, "Failed to allocate EXCURV::print operation", mlen);
    else {
      op->attach();
      (void) op->inherit_connections();
      op->send(x_value, y, ylen, filename, true, message, mlen);
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::print::get_name() const
{
  return ("EXCURV print graph");
}

DLVreturn_type EXCURV::refinement_data::update()
{
  DLVreturn_type ok = DLV_OK;
  if (has_connection()) {
    char buff[1024];
    buff[0] = '\0';
    snprintf(buff, 1024, "CHANGE A0 %f\n", data.central_dw);
    ok = write_connection(buff);
    if (ok == DLV_OK)
      read_connection(0);
    if (ok != DLV_OK)
      return ok;
    if (shells) {
      snprintf(buff, 1024, "CHANGE NS %d\n", (int)data.shells.size());
      ok = write_connection(buff);
      if (ok == DLV_OK)
	read_connection(0);
    }
    if (ok != DLV_OK)
      return ok;
    std::list<shell_data>::const_iterator ptr;
    int_g i = 0;
    for (ptr = data.shells.begin(); ptr != data.shells.end(); ++ptr ) {
      snprintf(buff, 1024, "CHANGE T%1d %d\n", i + 1, ptr->atom_type + 2);
      ok = write_connection(buff);
      if (ok == DLV_OK) {
	read_connection(0);
	snprintf(buff, 1024, "CHANGE N%1d %f\n", i + 1, ptr->number_of_atoms);
	ok = write_connection(buff);
	if (ok == DLV_OK) {
	  read_connection(0);
	  snprintf(buff, 1024, "CHANGE R%1d %f\n", i + 1, ptr->radius);
	  ok = write_connection(buff);
	  if (ok == DLV_OK) {
	    read_connection(0);
	    snprintf(buff, 1024, "CHANGE A%1d %f\n", i + 1, ptr->debye_waller);
	    ok = write_connection(buff);
	    if (ok == DLV_OK) {
	      read_connection(0);
	      if (data.multiple_scat) {
		snprintf(buff, 1024, "CHANGE TH%1d %f\n", i + 1, ptr->theta);
		ok = write_connection(buff);
		if (ok == DLV_OK) {
		  read_connection(0);
		  snprintf(buff, 1024, "CHANGE PHI%1d %f\n", i + 1, ptr->phi);
		  ok = write_connection(buff);
		  if (ok == DLV_OK)
		    read_connection(0);
		}
	      }
	    }
	  }
	}
      }
      if (ok != DLV_OK)
	break;
      i++;
    }
    if (ok == DLV_OK) {
      snprintf(buff, 1024, "CHANGE EF %f\n", data.fermi_energy);
      ok = write_connection(buff);
      if (ok == DLV_OK)
	read_connection(0);
      if (data.multiple_scat and ok == DLV_OK) {
	snprintf(buff, 1024, "CHANGE ATMAX %d\n", data.max_atom_path);
	ok = write_connection(buff);
	if (ok == DLV_OK)
	  read_connection(0);
	if (ok == DLV_OK) {
	  snprintf(buff, 1024, "CHANGE PLMAX %f\n", data.max_path_length);
	  ok = write_connection(buff);
	  if (ok == DLV_OK) {
	    read_connection(0);
	    if (ok == DLV_OK) {
	      snprintf(buff, 1024, "CHANGE PLMIN %f\n", data.real_g_plmin);
	      ok = write_connection(buff);
	      if (ok == DLV_OK) {
		read_connection(0);
		if (ok == DLV_OK) {
		  snprintf(buff, 1024, "CHANGE MINANG %f\n",
			   data.real_g_minang);
		  ok = write_connection(buff);
		  if (ok == DLV_OK) {
		    read_connection(0);
		    if (ok == DLV_OK) {
		      snprintf(buff, 1024, "CHANGE MINMAG %f\n",
			       data.real_g_minmag);
		      ok = write_connection(buff);
		      if (ok == DLV_OK) {
			read_connection(0);
			if (ok == DLV_OK) {
			  snprintf(buff, 1024, "CHANGE DLMAX %d\n",
				   data.int_dlmax);
			  ok = write_connection(buff);
			  if (ok == DLV_OK) {
			    read_connection(0);
			    if (ok == DLV_OK) {
			      snprintf(buff, 1024, "CHANGE TLMAX %d\n",
				       data.int_tlmax);
			      ok = write_connection(buff);
			      if (ok == DLV_OK) {
				read_connection(0);
				if (ok == DLV_OK) {
				  snprintf(buff, 1024, "CHANGE NUMAX %d\n",
					  data.int_numax);
				  ok = write_connection(buff);
				  if (ok == DLV_OK) {
				    read_connection(0);
				    if (ok == DLV_OK) {
				      snprintf(buff, 1024, "CHANGE OMIN %d\n",
					       data.int_omin);
				      ok = write_connection(buff);
				      if (ok == DLV_OK) {
					read_connection(0);
					if (ok == DLV_OK) {
					  snprintf(buff, 1024,
						   "CHANGE OMAX %d\n",
						   data.int_omax);
					  ok = write_connection(buff);
					  if (ok == DLV_OK) {
					    read_connection(0);
					    if (ok == DLV_OK) {
					      snprintf(buff, 1024,
						       "CHANGE OUTPUT %f\n",
						       data.real_g_output);
					      ok = write_connection(buff);
					      if (ok == DLV_OK) {
						read_connection(0);
						if (data.theory == 0)
						  strcpy(buff,
							 "SET THEORY CURV\n");
						else
						  strcpy(buff,
							 "SET THEORY SMALL\n");
						ok = write_connection(buff);
						if (ok == DLV_OK) {
						  read_connection(0);
						}
					      }
					    }
					  }
					}
				      }
				    }
				  }
				}
			      }
			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	    if (data.multiple_scat) {
	      snprintf(buff, 1024, "SYM %s\n", data.symmetry.c_str());
	      ok = write_connection(buff);
	      if (ok == DLV_OK)
		read_connection(0);
	    }
	  }
	}
      }
    }
  } else {
    //strncpy(message, "EXCURV not running", mlen);
    ok = DLV_ERROR;
  }
  return ok;
}

bool EXCURV::refinement_data::process_socket_data(char data[],
						  const bool suppress)
{
  if (strlen(data) > 0 or suppress) {
#ifdef ENABLE_DLV_GRAPHICS
    if (!suppress)
      set_error(data, false, false);
#endif // ENABLE_DLV_GRAPHICS
    return false;
  } else {
#ifdef ENABLE_DLV_GRAPHICS
    clear_error(false, false);
#endif // ENABLE_DLV_GRAPHICS
    // process within process could be interesting?
    char message[256];
    (void) list_parameters::create(0, message, 256);
    return true;
  }
}

DLV::operation *EXCURV::update_refine_params::create(const refine_data &rdata,
						     const bool update_shells,
						     char message[],
						     const int_g mlen)
{
  if (is_connected()) {
    update_refine_params *op = new update_refine_params(rdata, update_shells);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::update_refine_params operation",
	      mlen);
    else {
      op->attach();
      (void) op->inherit_connections();
      (void) op->update();
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::update_refine_params::get_name() const
{
  return ("EXCURV update refinement data");
}

DLV::operation *EXCURV::resume_refinement::create(const refine_data &data,
						  char message[],
						  const int_g mlen)
{
  if (is_connected()) {
    resume_refinement *op = new resume_refinement(data.step_size,
						  data.number_of_iterations,
						  data.correlations);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::resume_refinement operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[1024], text[128];
	// RESUME doesn't update what is being refined (I think).
	snprintf(buff, 1024, "REFINE RESUME %d\n", data.step_size);
	// Shouldn't need CONTINUE
	snprintf(text, 128, "SKIP %d\n", data.number_of_iterations);
	strcat(buff, text);
	if (data.correlations)
	  strcat(buff, "Y\n");
	else
	  strcat(buff, "N\n");
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  //cmd_type = dlexcurv_refine;
	  op->read_connection(op);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::resume_refinement::get_name() const
{
  return ("EXCURV resume refinement");
}

DLV::operation *EXCURV::refine_structure::create(const refine_data &data,
						 char message[],
						 const int_g mlen)
{
  if (is_connected()) {
    refine_structure *op = new refine_structure(data);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::refine_structure operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	if (!data.user_defined_ref and !data.refine_all_dw and
	    !data.refine_all_radii and !data.refine_all_atom_num) {
	  strncpy(message, "Refinement method not set", mlen);
	  ok = DLV_WARNING;
	} else
	  ok = op->update();
	if (ok == DLV_OK) {
	  char buff[1024], text[128];
	  // I could probably do this better
	  if (data.user_defined_ref) {
	    snprintf(buff, 1024, "REFINE LSR %d\n", data.step_size);
	    if (data.refine_cdw)
	      strcat(buff, "A0\n");
	    std::list<shell_data>::const_iterator ptr;
	    int_g i = 0;
	    for (ptr = data.shells.begin(); ptr != data.shells.end(); ++ptr ) {
	      if (ptr->refine_number) {
		snprintf(text, 128, "N%1d\n", i + 1);
		strcat(buff, text);
	      }
	      if (ptr->refine_radius) {
		snprintf(text, 128, "R%1d\n", i + 1);
		strcat(buff, text);
	      }
	      if (ptr->refine_dw) {
		snprintf(text, 128, "A%1d\n", i + 1);
		strcat(buff, text);
	      }
	      i++;
	    }
	    // Terminate parameter list
	    strcat(buff, "=\n");
	  } else if (data.refine_all_dw) {
	    if (data.refine_all_atom_num) {
	      if (data.refine_all_radii) {
		if (data.refine_efermi)
		  snprintf(buff, 1024, "REFINE ANRE %d\n", data.step_size);
		else
		  snprintf(buff, 1024, "REFINE ANR %d\n", data.step_size);
	      } else {
		if (data.refine_efermi)
		  snprintf(buff, 1024, "REFINE ANE %d\n", data.step_size);
		else
		  snprintf(buff, 1024, "REFINE AN %d\n", data.step_size);
	      }
	    } else if (data.refine_all_radii) {
	      if (data.refine_efermi)
		snprintf(buff, 1024, "REFINE ARE %d\n", data.step_size);
	      else
		snprintf(buff, 1024, "REFINE AR %d\n", data.step_size);
	    } else {
	      if (data.refine_efermi)
		snprintf(buff, 1024, "REFINE AE %d\n", data.step_size);
	      else
		snprintf(buff, 1024, "REFINE A %d\n", data.step_size);
	    }
	  } else if (data.refine_all_atom_num) {
	    if (data.refine_all_radii) {
	      if (data.refine_efermi)
		snprintf(buff, 1024, "REFINE NRE %d\n", data.step_size);
	      else
		snprintf(buff, 1024, "REFINE NR %d\n", data.step_size);
	    } else {
	      if (data.refine_efermi)
		snprintf(buff, 1024, "REFINE NE %d\n", data.step_size);
	      else
		snprintf(buff, 1024, "REFINE N %d\n", data.step_size);
	    }
	  } else if (data.refine_all_radii) {
	    if (data.refine_efermi)
	      snprintf(buff, 1024, "REFINE RE %d\n", data.step_size);
	    else
	      snprintf(buff, 1024, "REFINE R %d\n", data.step_size);
	  }
	  // Shouldn't need CONTINUE
	  snprintf(text, 128, "SKIP %d\n", data.number_of_iterations);
	  strcat(buff, text);
	  if (data.correlations)
	    strcat(buff, "Y\n");
	  else
	    strcat(buff, "N\n");
	  ok = op->write_connection(buff);
	  if (ok == DLV_OK) {
	    //cmd_type = dlexcurv_refine;
	    op->read_connection(op);
	  }
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::refine_structure::get_name() const
{
  return ("EXCURV refine structure");
}

DLV::operation *EXCURV::list_parameters::create(const int_g nshells,
						char message[],
						const int_g mlen)
{
  if (is_connected()) {
    list_parameters *op = new list_parameters(nshells);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::list_parameters operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	if (nshells > 0)
	  snprintf(buff, 128, "CHANGE NS %d\nLDLV\n", nshells);
	else
	  strcpy(buff, "LDLV\n");
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  //cmd_type = dlexcurv_list;
	  op->read_connection(op);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::list_parameters::get_name() const
{
  return ("EXCURV list parameters");
}

bool EXCURV::list_parameters::process_socket_data(char data[],
						  const bool suppress)
{
  if (suppress) {
    if (strlen(data) > 0) {
#ifdef ENABLE_DLV_GRAPHICS
      set_parameters(data);
#endif // ENABLE_DLV_GRAPHICS
      data[0] = '\0';
      return true;
    }
  }
  return false;
}

DLV::operation *EXCURV::set_kweight::create(const int_g k,
					    char message[], const int_g mlen)
{
  message[0] = '\0';
  if (is_connected()) {
    set_kweight *op = new set_kweight(k);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::set_kweight operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	snprintf(buff, 128, "SET WEIGHT %d\n", k + 1);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else
    return 0;
}

DLV::string EXCURV::set_kweight::get_name() const
{
  return ("EXCURV set kweight");
}

DLV::operation *EXCURV::set_emin::create(const real_g value,
					 char message[], const int_g mlen)
{
  if (is_connected()) {
    set_emin *op = new set_emin(value);
    if (op == 0)
      strncpy(message, "Failed to allocate EXCURV::set_emin operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	snprintf(buff, 128, "CHANGE EMIN %f\n", value);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::set_emin::get_name() const
{
  return ("EXCURV set energy minimum");
}

DLV::operation *EXCURV::set_emax::create(const real_g value,
					 char message[], const int_g mlen)
{
  if (is_connected()) {
    set_emax *op = new set_emax(value);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::set_emax operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	snprintf(buff, 128, "CHANGE EMAX %f\n", value);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::set_emax::get_name() const
{
  return ("EXCURV set energy maximum");
}

DLV::operation *EXCURV::set_kmin::create(const real_g value,
					 char message[], const int_g mlen)
{
  if (is_connected()) {
    set_kmin *op = new set_kmin(value);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::set_kmin operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	snprintf(buff, 128, "CHANGE KMIN %f\n", value);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::set_kmin::get_name() const
{
  return ("EXCURV set k minimum");
}

DLV::operation *EXCURV::set_kmax::create(const real_g value,
					 char message[], const int_g mlen)
{
  if (is_connected()) {
    set_kmax *op = new set_kmax(value);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::set_kmax operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	snprintf(buff, 128, "CHANGE KMAX %f\n", value);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::set_kmax::get_name() const
{
  return ("EXCURV set k maximum");
}

DLV::operation *EXCURV::multiple_scattering::create(const int_g ms,
						    char message[],
						    const int_g mlen)
{
  message[0] = '\0';
  multiple_scattering *op = 0;
  if (is_connected()) {
    bool bms = (ms != 0);
    op = new multiple_scattering(bms);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::multiple_scattering operation",
	      mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[128];
	if (ms == 0) {
	  snprintf(buff, 128, "SET MS OFF\n");
	  ok = op->write_connection(buff);
	  if (ok == DLV_OK) {
	    op->read_connection(0);
	  }
	} else {
	  snprintf(buff, 128, "SET MS ALL\n");
	  ok = op->write_connection(buff);
	  if (ok == DLV_OK) {
	    op->read_connection(0);
	  }
	}
      }
    }
  }
  return op;
}

DLV::string EXCURV::multiple_scattering::get_name() const
{
  return ("EXCURV set multiple scattering");
}

DLV::operation *EXCURV::save_data::create(const char name[],
					  char message[], const int_g mlen)
{
  if (is_connected()) {
    save_data *op = new save_data(name);
    if (op == 0)
      strncpy(message,
	      "Failed to allocate EXCURV::save_params operation", mlen);
    else {
      op->attach();
      DLVreturn_type ok = DLV_OK;
      if (op->inherit_connections()) {
	char buff[256];
	snprintf(buff, 128, "PRINT PAR\n%s\n", name);
	ok = op->write_connection(buff);
	if (ok == DLV_OK) {
	  op->read_connection(0);
	}
      }
    }
    return op;
  } else {
    strncpy(message, "EXCURV not running", mlen);
    return 0;
  }
}

DLV::string EXCURV::save_data::get_name() const
{
  return ("EXCURV save parameters");
}

DLV::operation *EXCURV::model::create(const int_g atomic_number,
				      char message[], const int_g mlen)
{
  // Create model
  DLV::model *m = DLV::model::create_shells("EXCURV shells");
  model *op = 0;
  if (m != 0) {
    DLV::coord_type c[3] = {0.0, 0.0, 0.0};
    DLV::atom a = m->add_cartesian_atom(atomic_number, c);
    if (a == 0)
      strncpy(message, "Error adding atom to model", mlen);
    else {
      op = new model(m);
      if (op == 0) {
	strncpy(message,
		"Failed to allocate EXCURV::create_model operation", mlen);
	delete m;
      } else {
	attach_base(op);
	m->complete();
      }
    }
  }
  return op;
}

DLV::string EXCURV::model::get_name() const
{
  return ("EXCURV create shell model");
}

bool EXCURV::calc_geom::is_geometry() const
{
  return true;
}

bool EXCURV::calc_geom::is_geom_edit() const
{
  return true;
}

void EXCURV::calc_geom::inherit_model()
{
  // Null op, we have a new structure
}

void EXCURV::calc_geom::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}

void EXCURV::calc_geom::map_data()
{
  // Load or create are disconnected from other and can't map data,
  // but a structural edit possibly can. Tricky - Todo
  // This may need to be implemented separately for each edit op.
}

DLV::operation *
EXCURV::convert_model::create(const int_g atomic_number, const int_g types[],
			      const real_g numbers[], const real_g radii[],
			      const int_g nshells, char message[],
			      const int_g mlen)
{
  DLV::string label = DLV::operation::get_current()->get_model_name();
  label += " - shells";
  char buff[8];
  snprintf(buff, 8, " %1d", my_counter);
  label += buff;
  DLV::model *m = DLV::model::create_shells(label);
  convert_model *op = 0;
  if (m != 0) {
    DLV::coord_type c[3] = {0.0, 0.0, 0.0};
    DLV::atom a = m->add_cartesian_atom(atomic_number, c);
    if (a == 0)
      strncpy(message, "Error converting to shell model", mlen);
    else {
      op = new convert_model(m);
      if (op == 0) {
	strncpy(message,
		"Failed to allocate EXCURV::convert_model operation", mlen);
	delete m;
      } else {
	op->attach();
	(void) op->inherit_connections();
	op->attach_data(calculation::get_viewer());
	m->add_shells(types, numbers, radii, nshells);
	m->complete();
      }
    }
  }
  return op;
}

DLV::string EXCURV::convert_model::get_name() const
{
  return ("EXCURV convert cluster to shells");
}

void EXCURV::convert_model::add_standard_data_objects()
{
  // Do nothing
}

DLV::operation *
EXCURV::update_model::update(const int_g types[], const real_g numbers[],
			     const real_g radii[], const int_g nshells,
			     bool &changed, char message[], const int_g mlen)
{
  DLV::model *m = 0;
  DLV::operation *op = get_current();
  update_model *edit_op = dynamic_cast<update_model *>(op);
  if (edit_op == 0) {
    changed = true;
    const DLV::model *parent = op->get_model();
    DLV::string label = op->get_model_name();
    char buff[8];
    if (my_counter > 0) {
      snprintf(buff, 8, " %1d", my_counter);
      label = op->get_model_name().substr(0, label.length() - strlen(buff));
    }
    my_counter++;
    snprintf(buff, 8, " %1d", my_counter);
    label += buff;
    m = parent->duplicate_model(label);
    if (m != 0) {
      edit_op = new update_model(m);
      if (edit_op == 0) {
	strncpy(message,
		"Failed to allocate EXCURV::update_model operation", mlen);
	delete m;
	return 0;
      } else {
	edit_op->attach();
	(void) edit_op->inherit_connections();
	edit_op->attach_data(calculation::get_viewer());
      }
    }
  } else {
    changed = false;
    m = edit_op->get_model();
  }
#ifdef ENABLE_DLV_GRAPHICS
  m->update_shells(types, numbers, radii, nshells);
  m->complete();
  if (!changed) {
    if (m->is_shell())
      m->update_shells(edit_op->get_display_obj(), message, mlen);
    else
      m->update_atoms(edit_op->get_display_obj(), message, mlen);
  }
#endif // ENABLE_DLV_GRAPHICS
  return edit_op;
}

DLV::string EXCURV::update_model::get_name() const
{
  return ("EXCURV update model");
}

void EXCURV::update_model::add_standard_data_objects()
{
  if (!get_model()->is_shell()) {
    // copied from edit_geometry
    DLV::data_object *data = new DLV::atom_and_bond_data();
    if (data != 0) {
      attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
    // Todo - isn't this false with EXCURV?
    if (get_model()->get_number_of_periodic_dims() > 1) {
      data = new DLV::lattice_direction_data();
      if (data != 0)
	attach_data(data);
    }
  }
}

bool EXCURV::convert_model::reload_data(DLV::data_object *data, char message[],
					const int_g mlen)
{
  // dummy, keep virtual inheritence happy since we need to inherit_conns
  return true;
}

bool EXCURV::update_model::reload_data(DLV::data_object *data, char message[],
				       const int_g mlen)
{
  // dummy, keep virtual inheritence happy since we need to inherit_conns
  return true;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    EXCURV::read_experimental_data *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::read_experimental_data("recover", 0, "", 0, 0, 0, 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::plot *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::plot(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::print *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::print(0, "recover");
    }

    /*
    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::refinement_data *t,
				    const unsigned int file_version)
    {
      EXCURV::refine_data data;
      ::new(t)EXCURV::refinement_data(data, true);
    }
    */

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    EXCURV::update_refine_params *t,
				    const unsigned int file_version)
    {
      EXCURV::refine_data data;
      ::new(t)EXCURV::update_refine_params(data, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::resume_refinement *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::resume_refinement(0, 0, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::refine_structure *t,
				    const unsigned int file_version)
    {
      EXCURV::refine_data data;
      ::new(t)EXCURV::refine_structure(data);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::list_parameters *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::list_parameters(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::set_kweight *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::set_kweight(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::set_emin *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::set_emin(0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::set_emax *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::set_emax(0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::set_kmin *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::set_kmin(0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::set_kmax *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::set_kmax(0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    EXCURV::multiple_scattering *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::multiple_scattering(false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::save_data *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::save_data("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::model *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::model(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::convert_model *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::convert_model(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, EXCURV::update_model *t,
				    const unsigned int file_version)
    {
      ::new(t)EXCURV::update_model(0);
    }

  }
}

template <class Archive>
void EXCURV::shell_data::serialize(Archive &ar, const unsigned int version)
{
  ar & atom_type;
  ar & number_of_atoms;
  ar & refine_number;
  ar & radius;
  ar & refine_radius;
  ar & debye_waller;
  ar & refine_dw;
  ar & theta;
  ar & phi;
}

template <class Archive>
void EXCURV::refine_data::serialize(Archive &ar, const unsigned int version)
{
  ar & number_of_shells;
  ar & step_size;
  ar & number_of_iterations;
  ar & central_dw;
  ar & fermi_energy;
  ar & refine_cdw;
  ar & user_defined_ref;
  ar & refine_all_atom_num;
  ar & refine_all_radii;
  ar & refine_all_dw;
  ar & refine_efermi;
  ar & correlations;
  ar & multiple_scat;
  ar & max_atom_path;
  ar & max_path_length;
  ar & real_g_plmin;
  ar & real_g_minang;
  ar & real_g_minmag;
  ar & int_dlmax;
  ar & int_tlmax;
  ar & int_numax;
  ar & int_omin;
  ar & int_omax;
  ar & real_g_output;
  ar & theory;
  ar & symmetry;
  ar & shells;
}

template <class Archive>
void EXCURV::calculation::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::socket_calc>(*this);
  ar & binary_name;
  ar & def_name;
  ar & executable;
  ar & def_file;
}

template <class Archive>
void EXCURV::read_experimental_data::serialize(Archive &ar,
					       const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & datafile;
  ar & atom_type;
  ar & xray_edge;
  ar & frequency;
  ar & x_index;
  ar & y_index;
  ar & k_weighting;
}

template <class Archive>
void EXCURV::potential_and_phase::save(Archive &ar,
				       const unsigned int version) const
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & centre_atom;
  ar & centre_neighbour;
  ar & number_shell_atoms;
  for (int_g i = 0; i < number_shell_atoms; i++) {
    ar & shell_atoms[i];
    ar & shell_neighbours[i];
  }
  ar & fix_v0;
  ar & method;
}

template <class Archive>
void EXCURV::potential_and_phase::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & centre_atom;
  ar & centre_neighbour;
  ar & number_shell_atoms;
  shell_atoms = new int_g[number_shell_atoms];
  shell_neighbours = new int_g[number_shell_atoms];
  for (int_g i = 0; i < number_shell_atoms; i++) {
    ar & shell_atoms[i];
    ar & shell_neighbours[i];
  }
  ar & fix_v0;
  ar & method;
}

template <class Archive>
void EXCURV::plot::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & value;
  ar & columns;
  ar & npoints;
  ar & title;
  ar & xaxis;
  ar & yaxis;
  for (int_g i = 0; i < columns; i++) {
    ar & labels[i];
    y[i] = new real_g[npoints];
  }
  x = new real_g[npoints];
  for (int_g i = 0; i < npoints; i++) {
    ar & x[i];
    for (int_g j = 0; i < columns; j++)
      ar & y[j][i];
  }
}

template <class Archive>
void EXCURV::plot::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & value;
  ar & columns;
  ar & npoints;
  ar & title;
  ar & xaxis;
  ar & yaxis;
  for (int_g i = 0; i < columns; i++)
    ar & labels[i];
  for (int_g i = 0; i < npoints; i++) {
    ar & x[i];
    for (int_g j = 0; i < columns; j++)
      ar & y[j][i];
  }
}

template <class Archive>
void EXCURV::print::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::plot>(*this);
  ar & outfile;
}

template <class Archive>
void EXCURV::refinement_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & data;
  ar & shells;
}

template <class Archive>
void EXCURV::update_refine_params::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::refinement_data>(*this);
}

template <class Archive>
void EXCURV::resume_refinement::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & step_size;
  ar & niterations;
  ar & correlations;
}

template <class Archive>
void EXCURV::refine_structure::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::refinement_data>(*this);
}

template <class Archive>
void EXCURV::list_parameters::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & number_of_shells;
}

template <class Archive>
void EXCURV::set_kweight::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & k;
}

template <class Archive>
void EXCURV::set_emin::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & energy;
}

template <class Archive>
void EXCURV::set_emax::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & energy;
}

template <class Archive>
void EXCURV::set_kmin::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & k;
}

template <class Archive>
void EXCURV::set_kmax::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & k;
}

template <class Archive>
void EXCURV::multiple_scattering::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & multiple_scat;
}

template <class Archive>
void EXCURV::save_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calculation>(*this);
  ar & filename;
}

template <class Archive>
void EXCURV::model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::create_model_op>(*this);
}

template <class Archive>
void EXCURV::calc_geom::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<calculation>(*this);
}

template <class Archive>
void EXCURV::convert_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calc_geom>(*this);
}

template <class Archive>
void EXCURV::update_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<EXCURV::calc_geom>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::calculation)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::read_experimental_data)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::potential_and_phase)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::plot)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::print)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::refinement_data)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::update_refine_params)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::resume_refinement)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::refine_structure)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::list_parameters)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::set_kweight)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::set_emin)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::set_emax)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::set_kmin)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::set_kmax)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::multiple_scattering)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::save_data)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::model)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::calc_geom)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::convert_model)
BOOST_CLASS_EXPORT_IMPLEMENT(EXCURV::update_model)

DLV_SUPPRESS_TEMPLATES(DLV::socket_calc)
DLV_SUPPRESS_TEMPLATES(DLV::create_model_op)

DLV_NORMAL_EXPLICIT(EXCURV::calculation)
DLV_NORMAL_EXPLICIT(EXCURV::read_experimental_data)
DLV_NORMAL_EXPLICIT(EXCURV::potential_and_phase)
DLV_NORMAL_EXPLICIT(EXCURV::plot)
DLV_NORMAL_EXPLICIT(EXCURV::print)
DLV_NORMAL_EXPLICIT(EXCURV::refinement_data)
DLV_NORMAL_EXPLICIT(EXCURV::update_refine_params)
DLV_NORMAL_EXPLICIT(EXCURV::resume_refinement)
DLV_NORMAL_EXPLICIT(EXCURV::refine_structure)
DLV_NORMAL_EXPLICIT(EXCURV::list_parameters)
DLV_NORMAL_EXPLICIT(EXCURV::set_kweight)
DLV_NORMAL_EXPLICIT(EXCURV::set_emin)
DLV_NORMAL_EXPLICIT(EXCURV::set_emax)
DLV_NORMAL_EXPLICIT(EXCURV::set_kmin)
DLV_NORMAL_EXPLICIT(EXCURV::set_kmax)
DLV_NORMAL_EXPLICIT(EXCURV::multiple_scattering)
DLV_NORMAL_EXPLICIT(EXCURV::save_data)
DLV_NORMAL_EXPLICIT(EXCURV::model)
DLV_NORMAL_EXPLICIT(EXCURV::calc_geom)
DLV_NORMAL_EXPLICIT(EXCURV::convert_model)
DLV_NORMAL_EXPLICIT(EXCURV::update_model)

#endif // DLV_USES_SERIALIZE
