
#ifndef DLV_CALC_PATHS
#define DLV_CALC_PATHS

namespace DLV {

  string get_code_path(const char code[]);

  void show_job_panel();
  void clear_job_list();
  void add_to_job_list(const string name, const string state, const int index);
  void update_job_status(const string state, const int index);
  void remove_from_job_list(const int index);

}

#endif // DLV_CALC_PATHS
