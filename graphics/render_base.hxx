
#ifndef DLV_RENDER_BASE
#define DLV_RENDER_BASE

#if defined(DLV_USES_AVS_GRAPHICS)
// Forward declare to avoid namespace issues.
class CCP3_Renderers_Base_parent;
class CCP3_Renderers_Base_atoms;
class CCP3_Renderers_Base_outline;
class CCP3_Renderers_Base_shells;
class CCP3_Viewers_Scenes_DLV3Dscene;

#  define VIEWER3D CCP3_Viewers_Scenes_DLV3Dscene

#elif defined(DLV_USES_VTK_GRAPHICS)
// Forward declare - or just include vtk/render_base?
namespace CCP3 {

  class render_base;
  class render_atoms;
  class render_outline;
  class render_shells;

}

class DLVview;

#  define VIEWER3D DLVview

#else
#  define VIEWER3D dummy_viewer
#endif // DLV_USES_*_GRAPHICS

namespace DLV {

  template <class scene_t, class parent_t> class render_parent_templ {
  public:
    render_parent_templ();
    virtual ~render_parent_templ();

    virtual DLVreturn_type
    attach_r3D(scene_t *view) = 0;
    virtual DLVreturn_type
    attach_k3D(scene_t *view);
    virtual DLVreturn_type
    detach_r3D(scene_t *view) = 0;
    virtual DLVreturn_type
    detach_k3D(scene_t *view);
    virtual class toolkit_obj get_rspace() const = 0;
    virtual class toolkit_obj get_kspace() const = 0;
    virtual class toolkit_obj get_redit() const = 0;
    virtual class toolkit_obj get_kedit() const = 0;
    const char *get_name() const;
    virtual DLVreturn_type attach_rui(class toolkit_obj &r_ui) const = 0;
    virtual DLVreturn_type attach_kui(class toolkit_obj &k_ui) const;

    virtual void copy_settings(render_parent_templ *p, const bool copy_cell = true,
			       const bool copy_wavefn = false) = 0;
    void set_data_size(const int n);
    void select_data_object(const int n);
    void set_edit_size(const int n);
    void set_data_label(const string label, const int n);
    void set_edit_label(const string label, const int n);
    void set_sub_data_size(const int s, const int n);
    void set_sub_edit_size(const int s, const int n);
    void set_sub_data_label(const string label, const int s, const int n);
    void set_sub_edit_label(const string label, const int s, const int n);
    void set_display_type(const int v, const int n);
    void set_edit_type(const int v, const int n);
    void set_sub_data_vector(const bool v, const int s, const int n);
    void set_sub_edit_vector(const bool v, const int s, const int n);

    void add_display_list(const string name, const int id,
			  const int index) const;
    void reset_display_list(const int object) const;
    void reset_display_list(const int first, const int last) const;
    void empty_display_list() const;
    void add_edit_list(const string name, const int id, const int index) const;
    void reset_edit_list(const int object) const;
    void empty_edit_list() const;

    virtual void set_atom_group_size(const int size);
    virtual void set_atom_group_name(const string name, const int index);
    virtual void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
			       bool &conv_cell, bool &edges, double &tol) const;
    virtual void set_transforms(const int n, const float transforms[][3]);

    virtual void set_CRYSTAL_wavefn(const int valence_min,
				    const int valence_max, const int nbands,
				    const int lattice, const int centre,
				    const bool spin, const int kpoints[][3],
				    const int nkpoints, const int sa,
				    const int sb, const int sc);
    virtual void set_CRYSTAL_scf();
    virtual void set_CRYSTAL_tddft();
    virtual void set_ONETEP_scf();

    void get_background(float c[3]) const;
    void get_inverted_background(float c[3]) const;
    static void show_data_panel();
    static void stop_animation();
    static void show_animate_panel();
    static render_parent_templ *get_serialize_obj();

    // public for serialize
    void set_serialize_obj();

  protected:
    class toolkit_obj *get_parent() const;
    virtual parent_t *get_object() const = 0;

  private:
    static class toolkit_obj *parent;
    static render_parent_templ *serialize_obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  template <class scene_t, class parent_t, class atoms_t>
  class render_atoms_templ : public render_parent_templ<scene_t, parent_t> {
  public:
    render_atoms_templ(const char name[]);
    ~render_atoms_templ();

    DLVreturn_type attach_r3D(scene_t *view);
    DLVreturn_type attach_k3D(scene_t *view);
    DLVreturn_type detach_r3D(scene_t *view);
    DLVreturn_type detach_k3D(scene_t *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;
    DLVreturn_type attach_kui(class toolkit_obj &r_ui) const;

    void copy_settings(render_parent_templ<scene_t, parent_t> *p,
		       const bool copy_cell, const bool copy_wavefn);

    void set_atom_group_size(const int size);
    void set_atom_group_name(const string name, const int index);
    void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
		       bool &conv_cell, bool &edges, double &tol) const;
    void set_transforms(const int n, const float transforms[][3]);

    void set_CRYSTAL_wavefn(const int valence_min, const int valence_max,
			    const int nbands, const int lattice,
			    const int centre, const bool spin,
			    const int kpoints[][3], const int nkpoints,
			    const int sa, const int sb, const int sc);
    void set_CRYSTAL_scf();
    void set_CRYSTAL_tddft();
    void set_ONETEP_scf();

  protected:
    template<class render_t> friend class rspace_model_templ;
    atoms_t *get_atom_obj() const;
    parent_t *get_object() const;

  private:
    atoms_t *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    // Todo - these may need to be moved to inherited ones.
    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class scene_t, class parent_t, class outline_t>
  class render_outline_templ : public render_parent_templ<scene_t, parent_t> {
  public:
    render_outline_templ(const char name[]);
    ~render_outline_templ();

    DLVreturn_type attach_r3D(scene_t *view);
    DLVreturn_type detach_r3D(scene_t *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;
    void get_cell_data(int &na, int &nb, int &nc, bool &centre_cell,
		       int &cell_type, double &tol) const;
    void set_transforms(const int n, const float transforms[][3]);

    void copy_settings(render_parent_templ<scene_t, parent_t> *p,
		       const bool copy_cell, const bool copy_wavefn);

  protected:
    parent_t *get_object() const;

  private:
    outline_t *obj;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class scene_t, class parent_t, class shells_t>
  class render_shells_templ : public render_parent_templ<scene_t, parent_t> {
  public:
    render_shells_templ(const char name[]);
    ~render_shells_templ();

    DLVreturn_type attach_r3D(scene_t *view);
    DLVreturn_type detach_r3D(scene_t *view);
    class toolkit_obj get_rspace() const;
    class toolkit_obj get_kspace() const;
    class toolkit_obj get_redit() const;
    class toolkit_obj get_kedit() const;
    DLVreturn_type attach_rui(class toolkit_obj &r_ui) const;

    void copy_settings(render_parent_templ<scene_t, parent_t> *p,
		       const bool copy_cell, const bool copy_wavefn);

  protected:
    parent_t *get_object() const;

  private:
    shells_t *obj;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

#ifdef DLV_USES_AVS_GRAPHICS

  typedef render_parent_templ<CCP3_Viewers_Scenes_DLV3Dscene,
			      CCP3_Renderers_Base_parent> render_parent;
  typedef render_atoms_templ<CCP3_Viewers_Scenes_DLV3Dscene,
			     CCP3_Renderers_Base_parent,
			     CCP3_Renderers_Base_atoms> render_atoms;
  typedef render_outline_templ<CCP3_Viewers_Scenes_DLV3Dscene,
			       CCP3_Renderers_Base_parent,
			       CCP3_Renderers_Base_outline> render_outline;
  typedef render_shells_templ<CCP3_Viewers_Scenes_DLV3Dscene,
			      CCP3_Renderers_Base_parent,
			      CCP3_Renderers_Base_shells> render_shells;

#elif defined(DLV_USES_VTK_GRAPHICS)

  typedef render_parent_templ<DLVview, CCP3::render_base> render_parent;
  typedef render_atoms_templ<DLVview, CCP3::render_base, CCP3::render_atoms> render_atoms;
  typedef render_outline_templ<DLVview, CCP3::render_base, CCP3::render_outline> render_outline;
  typedef render_shells_templ<DLVview, CCP3::render_base, CCP3::render_shells> render_shells;

#else

  typedef render_parent_templ<void, void> render_parent;
  typedef render_atoms_templ<void, void, void> render_atoms;
  typedef render_outline_templ<void, void, void> render_outline;
  typedef render_shells_templ<void, void, void> render_shells;

#endif // DLV_USES_AVS_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::render_parent)
BOOST_CLASS_EXPORT_KEY(DLV::render_atoms)
BOOST_CLASS_EXPORT_KEY(DLV::render_outline)
BOOST_CLASS_EXPORT_KEY(DLV::render_shells)
#endif // DLV_USES_SERIALIZE

template <class scene_t, class parent_t>
inline DLV::render_parent_templ<scene_t, parent_t> *
DLV::render_parent_templ<scene_t, parent_t>::get_serialize_obj()
{
  return serialize_obj;
}

template <class scene_t, class parent_t>
inline void DLV::render_parent_templ<scene_t, parent_t>::set_serialize_obj()
{
  serialize_obj = this;
}

#endif // DLV_RENDER_BASE
