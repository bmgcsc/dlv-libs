
#include <cmath>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "calcs.hxx"

DLV::model *GULP::structure_file::read(const DLV::string name,
				       const char filename[],
				       char message[], const int_g mlen)
{
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[256];
      input.getline(line, 256);
      int_g dim, centre, lattice;
      if (sscanf(line, "%d %d %d", &dim, &centre, &lattice) != 3) {
	strncpy(message, "Incorrect file header - is this a GULP str file?",
		mlen - 1);
	ok = DLV_ERROR;
	message[mlen - 1] = '\0';
      }
      if (dim < 0 or dim > 3) {
	strncpy(message, "Invalid model dimension - is this a GULP str file?",
		mlen - 1);
	ok = DLV_ERROR;
      }
      if ((centre < 1 or centre > 7) or (lattice < 1 or lattice > 6)) {
	strncpy(message, "Invalid lattice/centre - is this a GULP str file?",
		mlen - 1);
	ok = DLV_ERROR;
      }
      if (ok == DLV_OK) {
	structure = DLV::model::create_atoms(name, dim);
	if (structure == 0) {
	  strncpy(message, "Create model failed", mlen - 1);
	  ok = DLV_ERROR;
	} else {
	  // Get lattice
	  input.getline(line, 256);
	  real_l a[3];
	  if (sscanf(line, "%lf %lf %lf", &a[0], &a[1], &a[2]) != 3) {
	    strncpy(message, "Error reading a lattice vector", mlen - 1);
	    ok = DLV_ERROR;
	  } else {
	    input.getline(line, 256);
	    real_l b[3];
	    if (sscanf(line, "%lf %lf %lf", &b[0], &b[1], &b[2]) != 3) {
	      strncpy(message, "Error reading b lattice vector", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      input.getline(line, 256);
	      real_l c[3];
	      if (sscanf(line, "%lf %lf %lf", &c[0], &c[1], &c[2]) != 3) {
		strncpy(message, "Error reading c lattice vector", mlen - 1);
		ok = DLV_ERROR;
	      } else {
		if (dim > 0) {
		  if (!structure->set_primitive_lattice(a, b, c)) {
		    strncpy(message, "Error setting lattice vectors",
			    mlen - 1);
		    ok = DLV_ERROR;
		  }
		}
	      }
	    }
	  }
	  if (ok == DLV_OK) {
	    // Get Symmetry
	    input.getline(line, 256);
	    int_g nops;
	    sscanf(line, "%d", &nops);
	    if (nops < 1) {
	      strncpy(message, "Error reading number of symmetry operators",
		      mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      structure->set_crystal03_lattice_type(lattice, centre);
	      real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
	      real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
	      for (int_g i = 0; i < nops; i++) {
		for (int_g j = 0; j < 3; j++) {
		  input.getline(line, 256);
		  // These appear to be transposes, at least for Al2O3
		  if (sscanf(line, "%lf %lf %lf", &rotations[i][0][j],
			     &rotations[i][1][j], &rotations[i][2][j]) != 3) {
		    ok = DLV_ERROR;
		    break;
		  }
		}
		if (!ok)
		  break;
		input.getline(line, 256);
		if (sscanf(line, "%lf %lf %lf", &translations[i][0],
			   &translations[i][1], &translations[i][2]) != 3) {
		  ok = DLV_ERROR;
		  break;
		}
	      }
	      if (ok == DLV_OK) {
		if (!structure->set_fractional_sym_ops(rotations,
						       translations, nops)) {
		  strncpy(message, "Error setting symmetry operators",
			  mlen - 1);
		  ok = DLV_ERROR;
		}
	      } else
		strncpy(message, "Error reading symmetry operators",
			mlen - 1);
	      delete_local_array(translations);
	      delete_local_array(rotations);
	    }
	  }
	  if (ok == DLV_OK) {
	    // Get atoms
	    input.getline(line, 256);
	    int_g natoms;
	    if (sscanf(line, "%d", &natoms) != 1) {
	      strncpy(message, "Error reading number of atoms", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      // Todo - can we be sure that line didn't wrap?
	      for (int_g i = 0; i < natoms; i++) {
		input.getline(line, 256);
		int_g id;
		DLV::coord_type coords[3];
		if (sscanf(line, "%d %lf %lf %lf", &id, &coords[0],
			   &coords[1], &coords[2]) != 4) {
		  strncpy(message, "Error reading atoms", mlen - 1);
		  ok = DLV_ERROR;
		  break;
		} else {
		  DLV::atom my_atom = structure->add_cartesian_atom(id,
								    coords);
		  if (my_atom == 0) {
		    strncpy(message, "Error adding atom to model", mlen - 1);
		    ok = DLV_ERROR;
		    break;
		  }
		}
	      }
	    }
	  }
	  if (ok != DLV_OK) {
	    delete structure;
	    structure = 0;
	  } else
	    structure->complete();
	}
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}

void GULP::structure_file::write(const char filename[],
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename, message, mlen)) {
    write_data(output, structure, false);
    output.close();
  }
}

void GULP::structure_file::write_data(std::ofstream &output,
				      const DLV::model *const structure,
				      const bool put_shells)
{
  DLV::coord_type a[3];
  DLV::coord_type b[3];
  DLV::coord_type c[3];
  structure->get_primitive_lattice(a, b, c);
  int dim = structure->get_number_of_periodic_dims();
  if (dim == 2) {
    output << "svectors\n";
    output.width(18);
    output.precision(8);
    output << a[0];
    output.precision(8);
    output.width(18);
    output << a[1];
    output << '\n';
    output.precision(8);
    output.width(18);
    output << b[0];
    output.precision(8);
    output.width(18);
    output << b[1];
    output << '\n';
  } else {
    output << "vectors\n";
    output.width(18);
    output.precision(8);
    output << a[0];
    output.precision(8);
    output.width(18);
    output << a[1];
    output.precision(8);
    output.width(18);
    output << a[2];
    output << '\n';
    output.precision(8);
    output.width(18);
    output << b[0];
    output.precision(8);
    output.width(18);
    output << b[1];
    output.precision(8);
    output.width(18);
    output << b[2];
    output << '\n';
    output.precision(8);
    output.width(18);
    output << c[0];
    output.precision(8);
    output.width(18);
    output << c[1];
    output.precision(8);
    output.width(18);
    output << c[2];
    output << '\n';
  }
  /* Todo
  DLV::string symbol;
  int_g group = structure->get_hermann_mauguin_group(symbol);
  convert_group_symbol(group, symbol);
  if (group == 0)
    output << "space " << group << '\n';
  else
    output << "space " << symbol << '\n';
  int_g natoms = structure->get_number_of_asym_atoms();
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type, natoms, 3);
  structure->get_asym_atom_types(atom_types, natoms);
  structure->get_asym_atom_cart_coords(coords, natoms);
  output << "cartesian" << '\n';
  for (int_g i = 0; i < natoms; i++) {
    output << DLV::atom_type::get_atomic_symbol(atom_types[i]) << " core ";
    output.precision(8);
    output.width(18);
    output << coords[i][0];
    output.precision(8);
    output.width(18);
    output << coords[i][1];
    output.precision(8);
    output.width(18);
    output << coords[i][2];
    output << '\n';
    if (put_shells) {
      output << DLV::atom_type::get_atomic_symbol(atom_types[i]);
      output << " shell ";
      output.precision(8);
      output.width(18);
      output << coords[i][0];
      output.precision(8);
      output.width(18);
      output << coords[i][1];
      output.precision(8);
      output.width(18);
      output << coords[i][2];
      output << '\n';
    }
  }
  */
  output << "space\n1\n";
  int_g natoms = structure->get_number_of_primitive_atoms();
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type, natoms, 3);
  structure->get_primitive_atom_types(atom_types, natoms);
  structure->get_primitive_atom_cart_coords(coords, natoms);
  output << "cartesian ";
  if (put_shells)
    output << 2 * natoms << '\n';
  else
    output << natoms << '\n';    
  for (int_g i = 0; i < natoms; i++) {
    output << DLV::atom_type::get_atomic_symbol(atom_types[i]) << " core ";
    output.precision(8);
    output.width(18);
    output << coords[i][0];
    output.precision(8);
    output.width(18);
    output << coords[i][1];
    output.precision(8);
    output.width(18);
    output << coords[i][2];
    output << '\n';
    if (put_shells) {
      output << DLV::atom_type::get_atomic_symbol(atom_types[i]);
      output << " shell ";
      output.precision(8);
      output.width(18);
      output << coords[i][0];
      output.precision(8);
      output.width(18);
      output << coords[i][1];
      output.precision(8);
      output.width(18);
      output << coords[i][2];
      output << '\n';
    }
  }
  delete_local_array(coords);
  delete_local_array(atom_types);
}

void GULP::structure_file::convert_group_symbol(int_g &gp, DLV::string &name)
{
  // Todo
}

DLV::data_object *
GULP::property_files::read_phonon_bands(DLV::operation *op,
					DLV::phonon_bands *plot,
					const char filename[],
					const DLV::string id,
					const int_g nkp,
					const DLV::string klabels[],
					char message[], const int_g mlen)
{
  DLV::phonon_bands *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      const int_g buff_size = 5000;
      int_g panels[buff_size];
      for (int_g i = 0; i < buff_size; i++)
	panels[i] = -1;
      panels[0] = 0;
      int_g npanels = 0;
      int_g nplots = 0;
      const bool calc = false;
      const int_g max_kp = 32;
      int_g local_nkp = 1;
      real_g local_kp[max_kp][3];
      char line[256];
      int_g n = 0;
      int_g x[buff_size];
      real_g y[buff_size];
      do {
	input.getline(line, 256);
	if (strlen(line) == 0 || input.eof())
	  break;
	// Only read single data set
	if (line[0] == '#') {
	  if ((calc && npanels == local_nkp - 1) ||
	      (!calc && npanels == max_kp - 1))
	    break;
	  else {
	    // Start point overlaps end of previous
	    if (npanels != 0)
	      n -= nplots;
	    //start_pt = n;
	    panels[n] = npanels;
	    npanels++;
	    // Get start and end kpoint comments
	    if (calc) {
	      input.getline(line, 128);
	      input.getline(line, 128);
	    } else {
	      // Need to read the kpoint info
	      input.getline(line, 128);
	      int_g i = 0;
	      while (line[i] != '=')
		i++;
	      sscanf(&line[i + 1], "%f %f %f", &local_kp[npanels - 1][0],
		     &local_kp[npanels - 1][1], &local_kp[npanels - 1][2]);
	      input.getline(line, 128);
	      i = 0;
	      while (line[i] != '=')
		i++;
	      sscanf(&line[i + 1], "%f %f %f", &local_kp[npanels][0],
		     &local_kp[npanels][1], &local_kp[npanels][2]);
	      local_nkp++;
	    }
	  }
	} else {
	  if (n >= buff_size) {
	    strncpy(message, "Buffer size exceeded", mlen);
	    break;
	  }
	  sscanf(line, "%d %f\n", &x[n], &y[n]);
	  if (npanels == 1) {
	    if (x[n] == x[0])
	      nplots++;
	  }
	  n++;
	}
      } while (!input.eof());
      panels[n - nplots] = local_nkp - 1;
      input.close();
      n = n / nplots;
      if (plot == 0)
	data = new DLV::phonon_bands("GULP", id, op, nplots, local_nkp);
      else
	data = plot;
      real_g *xspecial = new real_g[local_nkp];
      // Set up x grid
      real_g *points = new_local_array1(real_g, n);
      int_g k = 0;
      int_g kp = 0;
      real_g stepx = 1.0;
      real_g stepy = 1.0;
      real_g stepz = 1.0;
      real_g ka = 0.0;
      real_g kb = 0.0;
      real_g kc = 0.0;
      real_g base = 1.0;
      int_g ref_point = 0;
      int_g kinc = 0;
      real_g scale = 1.0;
      real_g ref_step = 1.0;
      real_g kpts[buff_size][3];
      for (int_g i = 0; i < n; i++) {
	points[i] = base + ((real_g)(i - ref_point)) * scale;
	if (panels[k] > -1) {
	  // Find panel end
	  int_g j;
	  for (j = k + nplots; j < n * nplots; j += nplots)
	    if (panels[j] > -1)
	      break;
	  real_g len = 0.0;
	  for (int_g m = 0; m < 3; m++) {
	    real_g diff = (local_kp[panels[j]][m] - local_kp[panels[k]][m]);
	    len += diff * diff;
	  }
	  len = std::sqrt(len);
	  int_g nsteps = (j - k) / nplots;
	  scale = len / (real_g) nsteps;
	  if (i == 0) {
	    ref_step = scale;
	    scale = 1.0;
	  } else {
	    ref_point = i;
	    base = points[i];
	    scale = scale / ref_step;
	    kp++;
	  }
	  xspecial[kp] = points[i];
	  ka = local_kp[panels[k]][0];
	  kb = local_kp[panels[k]][1];
	  kc = local_kp[panels[k]][2];
	  stepx = (local_kp[panels[j]][0] - ka) / nsteps;
	  stepy = (local_kp[panels[j]][1] - kb) / nsteps;
	  stepz = (local_kp[panels[j]][2] - kc) / nsteps;
	  kinc = 0;
	}
	kpts[i][0] = ka + (real_g)kinc * stepx;
	kpts[i][1] = kb + (real_g)kinc * stepy;
	kpts[i][2] = kc + (real_g)kinc * stepz;
	kinc++;
	k += nplots;
      }
      data->set_grid(points, n);
      xspecial[local_nkp - 1] = points[n - 1];
      // Todo - check locl_nkp == nkp if != 0?
      data->set_xpoints(xspecial, local_nkp, false);
      if (nkp > 0) {
	for (int_g i = 0; i < nkp; i++) {
	  if (klabels[i] == "G") {
	    DLV::string gamma = "\r2029";
	    data->set_xlabel(gamma.c_str(), i);
	  } else
	    data->set_xlabel(klabels[i].c_str(), i);
	}
      } else {
	const real_g tol = 0.01;
	for (int_g i = 0; i < local_nkp; i++) {
	  if (std::abs(local_kp[i][0]) < tol and std::abs(local_kp[i][1]) < tol
	      and std::abs(local_kp[i][2]) < tol) {
	    DLV::string gamma = "\r2029";
	    data->set_xlabel(gamma.c_str(), i);
	  } else {
	    char label_buff[128];
	    snprintf(label_buff, 128, "(%3.1f %3.1f %3.1f)", local_kp[i][0],
		     local_kp[i][1], local_kp[i][2]);
	    data->set_xlabel(label_buff, i);
	  }
	}
      }
      //data->set_kpoints(local_kp, true);
      // Now generate the dispersion curves.
      for (int_g i = 0; i < nplots; i++) {
	k = i;
	for (int_g j = 0; j < n; j++) {
	  points[j] = y[k];
	  k += nplots;
	}
	data->add_plot(points, i);
	char label_buff[32];
	snprintf(label_buff, 32, "Phonon %d", i + 1); 
	data->set_plot_label(label_buff, i);
      }
      // Model name problems due to it being a new model thats not yet read?
      data->set_yaxis_label("Frequency (cm-1)");
      //data->special_ypoints(xspecial, NULL, 0, true, true);
      data->set_yunits("cm-1");
      //data->set_band_base(1);
      //data->set_kpoints(kpts); - Todo? is this necessary?
      delete_local_array(points);
      input.close();
    }
  }
  return data;
}

DLV::data_object *GULP::property_files::read_phonon_DOS(DLV::operation *op,
							DLV::phonon_dos *plot,
							const char filename[],
							const DLV::string id,
							char message[],
							const int_g mlen)
{
  DLV::phonon_dos *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      const int_g buff_size = 5000;
      real_g x[buff_size];
      real_g y[buff_size];
      int_g n = 0;
      char line[256];
      do {
	input.getline(line, 256);
	if (strlen(line) == 0)
	  break;
	// Only read single DOS - Todo, change this?
	if (line[0] == '#') {
	  if (n != 0)
	    break;
	} else {
	  if (n >= buff_size) {
	    strncpy(message, "Buffer size exceeded - truncating data", mlen);
	    break;
	  }
	  sscanf(line, "%f %f\n", &x[n], &y[n]);
	  n++;
	}
      } while (!input.eof());
      input.close();
      if (plot == 0)
	data = new DLV::phonon_dos("GULP", id, op, 1);
      else
	data = plot;
      data->set_grid(x, n);
      data->add_plot(y, 0);
      data->set_plot_label("DOS", 0);
      data->set_xaxis_label("Frequency (cm-1)");
      data->set_xunits("cm-1");
    }
  }
  return data;
}

DLV::data_object *
GULP::property_files::read_phonon_vectors(DLV::operation *op,
					  const char filename[],
					  const DLV::string id,
					  char message[], const int_g mlen)
{
  DLV::phonon_vectors *glyph = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g i, atn, n, nkp, nmodes;
      char line[256];
      bool ok = true;
      input >> n;
      if (n > 0) {
	// Read the atoms
	real_g (*coords)[3] = new real_g[n][3];
	for (i = 0; i < n; i++) {
	  // Ignore atomic number
	  // Todo - really should use this to check that the cell settings
	  // are the same, so the same atom types are at the coords.
	  input >> atn;
	  input >> coords[i][0];
	  input >> coords[i][1];
	  input >> coords[i][2];
	}
	input >> nkp;
	input >> nmodes;
	// Now read the eigenvectors.
	real_g (*vecs)[3] = new_local_array2(real_g, n, 3);
	int_g nk, nm;
	real_g freq, ka, kb, kc;
	for (nk = 0; nk < nkp; nk++) {
	  // clear to next line
	  input.getline(line, 256);
	  input >> line; // "K"
	  input >> line; // "point"
	  input >> line; // "at"
	  input >> ka;
	  input >> kb;
	  input >> kc;
	  for (nm = 0; nm < nmodes; nm++) {
	    // clear to next line
	    input.getline(line, 256);
	    input >> line; // "Mode"
	    input >> atn;
	    input >> freq;
	    // Read eigenvectors
	    for (i = 0; i < n; i++) {
	      input >> vecs[i][0];
	      input >> vecs[i][1];
	      input >> vecs[i][2];
	    }
	    if (glyph == 0) {
	      glyph = new DLV::phonon_vectors("GULP", id, op, nmodes, nkp);
	      glyph->set_grid(coords, n, false);
	    }
	    ok = glyph->add_data(nk, nm, ka, kb, kc, freq, vecs, n);
	    if (!ok)
	      break;
	  }
	  if (!ok)
	    break;
	}
	delete_local_array(vecs);
	//delete [] coords;
      }
      input.close();
    }
  }
  return glyph;
}

bool GULP::property_files::read_MD_trajectory(DLV::operation *op,
					      DLV::time_plot * &data1,
					      DLV::time_plot * &data2,
					      DLV::time_plot * &data3,
					      DLV::md_trajectory * &data4,
					      const char filename[],
					      const DLV::string id,
					      DLV::model *m,
					      const bool read_model,
					      char message[], const int_g mlen)
{
  bool ok = true;
  if (m->get_number_of_sym_ops() > 1) {
    // must have asym basis == primitive list
    strncpy(message, "Problem mapping MD trajectory symmetry", mlen);
    return false;
  }
  if ((ok = DLV::check_filename(filename, message, mlen))) {
    std::ifstream input;
    if ((ok = DLV::open_file_read(input, filename, message, mlen))) {
      const int_g size = 5000;
      int_g i, cores_and_shells, dim;
      real_g time[size], ke[size], pe[size], temp[size];
      real_g (*v)[size][3], (*p)[size][3];
      real_g t1, t2, t3, t4, ca1, ca2, ca3;
      char line[128];
      int_g natoms = m->get_number_of_asym_atoms();
      int_g n = 0;
      // skip version number
      input.getline(line, 128);
      input >> cores_and_shells;
      input >> dim;
      // complete line, skip header comment
      input.getline(line, 128);
      if (line[0] != '#')
	input.getline(line, 128);
      p = new real_g[natoms][size][3];
      v = new real_g[natoms][size][3];
      while (!input.eof()) {
	// Read time/ek/e/temp
	input.getline(line, 128);
	if (sscanf(line, "%f %f %f %f", &t1, &t2, &t3, &t4) < 4) {
	  // Not ideal, assumes only 1 number wrapped line
	  input.getline(line, 128);
	  sscanf(line, "%f", &t4);
	}
	time[n] = t1;
	ke[n] = t2;
	pe[n] = t3;
	temp[n] = t4;
	// Skip comment header
	input.getline(line, 128);
	// Read atom positions for vector field.
	for (i = 0; i < natoms; i++) {
	  input.getline(line, 128);
	  sscanf(line, "%f %f %f", &ca1, &ca2, &ca3);
	  p[i][n][0] = ca1;
	  p[i][n][1] = ca2;
	  p[i][n][2] = ca3;
	}
	for (i = natoms; i < cores_and_shells; i++)
	  input.getline(line, 128);
	input.getline(line, 128);
	// Read atom velocities
	for (i = 0; i < natoms; i++) {
	  input.getline(line, 128);
	  sscanf(line, "%f %f %f", &t1, &t2, &t3);
	  v[i][n][0] = t1;
	  v[i][n][1] = t2;
	  v[i][n][2] = t3;
	}
	// Skip shells
	for (i = natoms; i < cores_and_shells; i++)
	  input.getline(line, 128);
	input.getline(line, 128);
	if (strncmp(line, "#  Cell", 7) == 0) {
	  // Todo - is there a cell written for molecules?
	  // Skip cell vectors - structure read deals with this
	  for (i = 0; i < dim; i++)
	    input.getline(line, 128);
	  // Skip cell strains - don't know what to do with this
	  if (dim > 0) {
	    // Header
	    input.getline(line, 128);
	    // is this correct?
	    input.getline(line, 128);
	    if (dim == 3)
	      input.getline(line, 128);
	    input.getline(line, 128);
	  }
	}
	// additions for v4.0
	if (strncmp(line, "#  Derivatives", 14) == 0) {
	  for (i = 0; i < natoms; i++)
	    input.getline(line, 128);
	  input.getline(line, 128);
	}
	if (strncmp(line, "#  Site", 7) == 0) {
	  for (i = 0; i < natoms; i++)
	    input.getline(line, 128);
	  input.getline(line, 128);
	}
	n++;
	if (line[0] != '#')
	  break;
	if (n == size) {
	  strncpy(message, "buffer size excceded - truncating", mlen);
	  break;
	}
      }
      DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,
						      natoms, 3);
      for (int_g ip = 0; ip < natoms; ip++)
	for (int_g kp = 0; kp < 3; kp++)
	  coords[ip][kp] = p[ip][0][kp];
      if (read_model) {
	m->replace_atom_cart_coords(coords);
	m->update_atom_list();
      }
      real_g tstep = (time[1] - time[0]) * 1e-12;
      DLV::time_plot *plot = 0;
      if (data1 == 0) {
	plot = new DLV::time_plot("GULP", id, op, 1);
	data1 = plot;
      } else {
	plot = data1;
	data1->set_loaded();
      }
      plot->set_grid(time, n);
      plot->add_plot(ke, 0);
      plot->set_plot_label("KE", 0);
      plot->set_xaxis_label("Time (ps)");
      plot->set_yaxis_label("Energy (eV)");
      if (data2 == 0) {
	plot = new DLV::time_plot("GULP", id, op, 1);
	data2 = plot;
      } else {
	plot = data2;
	data2->set_loaded();
      }
      plot->set_grid(time, n);
      plot->add_plot(pe, 0);
      plot->set_plot_label("PE", 0);
      plot->set_xaxis_label("Time (ps)");
      plot->set_yaxis_label("Energy (eV)");
      if (data3 == 0) {
	plot = new DLV::time_plot("GULP", id, op, 1);
	data3 = plot;
      } else {
	plot = data3;
	data3->set_loaded();
      }
      plot->set_grid(time, n);
      plot->add_plot(temp, 0);
      plot->set_plot_label("T", 0);
      plot->set_xaxis_label("Time (ps)");
      plot->set_yaxis_label("Temperature (K)");
      DLV::md_trajectory *data = 0;
      if (data4 == 0) {
	data = new DLV::md_trajectory("GULP", id, op, n, tstep);
	data4 = data;
      } else {
	data = data4;
	data4->set_loaded();
      }
      //data->set_data(&(p[0][0]), &(v[0][0]), natoms, false);
      // we need to map the trajectory on to the primitive atom list
      //int *indices = new_local_array1(int_g, natoms);
      //m->get_asym_to_primitive_indices(indices, natoms);
      data->set_data(&(p[0][0]), coords, /*indices,*/ natoms, size);
      //delete_local_array(indices);
      delete_local_array(coords);
      delete [] v;
      delete [] p;
      input.close();
    }
  }
  return ok;
}
