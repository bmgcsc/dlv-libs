
#ifndef GULP_CALCS
#define GULP_CALCS

namespace DLV {

  class phonon_bands;
  class phonon_dos;
  class time_plot;
  class md_trajectory;

}

namespace GULP {

  using DLV::int_g;
  using DLV::nat_g;
  using DLV::real_g;
  using DLV::real_l;

  class structure_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);
    void write(const char filename[], const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_data(std::ofstream &output, const DLV::model *const structure,
		    const bool put_shells);

  private:
    void convert_group_symbol(int_g &gp, DLV::string &name);
  };

  class property_files {
  protected:
    static DLV::data_object *read_phonon_bands(DLV::operation *op,
					       DLV::phonon_bands *plot,
					       const char filename[],
					       const DLV::string id,
					       const int_g nkp,
					       const DLV::string klabels[],
					       char message[],
					       const int_g mlen);
    static DLV::data_object *read_phonon_DOS(DLV::operation *op,
					     DLV::phonon_dos *plot,
					     const char filename[],
					     const DLV::string id,
					     char message[], const int_g mlen);
    static DLV::data_object *read_phonon_vectors(DLV::operation *op,
						 const char filename[],
						 const DLV::string id,
						 char message[],
						 const int_g mlen);
    static bool read_MD_trajectory(DLV::operation *op,
				   DLV::time_plot * &data1,
				   DLV::time_plot * &data2,
				   DLV::time_plot * &data3,
				   DLV::md_trajectory * &data4,
				   const char filename[], const DLV::string id,
				   DLV::model *m, const bool read_model,
				   char message[],
				   const int_g mlen);
  };

}

#endif // GULP_CALCS
