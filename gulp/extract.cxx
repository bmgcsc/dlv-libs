
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "calcs.hxx"
#include "extract.hxx"

// Todo - should we run gulp? It requires us to transform any gulp input
// into a simple calc (from opt, md etc) which could be tricky, but is
// potentially better for older space group symbols and origin etc.

DLV::operation *GULP::extract_structure::create(const char name[],
						const char filename[],
						const bool set_bonds,
						char message[],
						const int_g mlen)
{
  DLV::string model_name = name_from_file(name, filename);
  DLV::model *structure = read(model_name, filename, message, mlen);
  extract_structure *op = 0;
  if (structure != 0) {
    op = new extract_structure(structure, filename);
    attach_base(op);
    if (set_bonds)
      op->set_bond_all();
  }
  return op;
}

DLV::string GULP::extract_structure::get_name() const
{
  return (get_model_name() + " - Extract GULP structure");
}

DLV::model *GULP::extract_structure::read(const DLV::string name,
					  const char filename[],
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[256];
      input.getline(line, 256);
      // My version of GULP seems to assume its a cluster if 'cell' doesn't
      // come before space/cart, so this should be safe. If not need
      // to use older still v2 code which reads all atoms before setting any.
      int_g natoms = 0;
      bool in_atoms = false;
      bool is_frac = false;
      bool started = false;
      bool cell_params = false;
      real_l a, b, c, alpha, beta, gamma;
      while (!input.eof()) {
	input >> line;
	if (input.eof() && !started) {
	  strncpy(message,
		  "No data in file - is this really a GULP input file?", mlen);
	  ok = DLV_ERROR;
	  break;
	}
	started = true;
	if (strcmp(line, "cell") == 0) {
	  structure = DLV::model::create_atoms(name, 3);
	  input >> a;
	  input >> b;
	  input >> c;
	  input >> alpha;
	  input >> beta;
	  input >> gamma;
	  /*if (sscanf(line, "%lf %lf %lf %lf %lf %lf", &a, &b, &c,
		     &alpha, &beta, &gamma) != 6) {
	    strncpy(message, "Error reading lattice length parameters",
		    mlen - 1);
	    ok = DLV_ERROR;
	    } else {*/
	  if (!structure->set_lattice_parameters(a, b, c, alpha,
						 beta, gamma)) {
	    strncpy(message, "Error setting lattice parameters",
		    mlen - 1);
	    ok = DLV_ERROR;
	    //}
	  }
	  cell_params = true;
	  input.getline(line, 256);
	} else if (strcmp(line, "vectors") == 0) {
	  // clear the line
	  input.getline(line, 256);
	  structure = DLV::model::create_atoms(name, 3);
	  input.getline(line, 256);
	  real_l a[3];
	  if (sscanf(line, "%lf %lf %lf", &a[0], &a[1], &a[2]) != 3) {
	    strncpy(message, "Error reading a lattice vector", mlen - 1);
	    ok = DLV_ERROR;
	  } else {
	    input.getline(line, 256);
	    real_l b[3];
	    if (sscanf(line, "%lf %lf %lf", &b[0], &b[1], &b[2]) != 3) {
	      strncpy(message, "Error reading b lattice vector", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      input.getline(line, 256);
	      real_l c[3];
	      if (sscanf(line, "%lf %lf %lf", &c[0], &c[1], &c[2]) != 3) {
		strncpy(message, "Error reading c lattice vector", mlen - 1);
		ok = DLV_ERROR;
	      } else {
		if (!structure->set_primitive_lattice(a, b, c)) {
		  strncpy(message, "Error setting lattice vectors",
			  mlen - 1);
		  ok = DLV_ERROR;
		}
	      }
	    }
	  }
	} else if (strcmp(line, "svectors") == 0) {
	  // clear the line
	  input.getline(line, 256);	  
	  structure = DLV::model::create_atoms(name, 2);
	  input.getline(line, 256);
	  real_l a[3];
	  a[2] = 0.0;
	  if (sscanf(line, "%lf %lf", &a[0], &a[1]) != 2) {
	    strncpy(message, "Error reading a lattice vector", mlen - 1);
	    ok = DLV_ERROR;
	  } else {
	    input.getline(line, 256);
	    real_l b[3];
	    b[2] = 0.0;
	    if (sscanf(line, "%lf %lf", &b[0], &b[1]) != 2) {
	      strncpy(message, "Error reading b lattice vector", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      real_l c[3];
	      if (!structure->set_primitive_lattice(a, b, c)) {
		strncpy(message, "Error setting lattice vectors",
			mlen - 1);
		ok = DLV_ERROR;
	      }
	    }
	  }
	} else if (strncmp(line, "space", 5) == 0) {
	  if (structure == 0)
	    structure = DLV::model::create_atoms(name, 3);
	  input >> line;
	  if (isdigit(line[0]))
	    (void) structure->set_group(atoi(line));
	  else {
	    char label[128];
	    strcpy(label, line);
	    // Do we need to add another space?
	    strcat(label, " ");
	    input.getline(line, 256);
	    strcat(label, line);
	    if (line[strlen(line) - 1] == '\n')
	      line[strlen(line) - 1] = '\0';
	    if (strcmp(label, "P M 3 M") == 0 or strcmp(label, "P m 3 m") == 0)
	      strcpy(label, "P m -3 m");
	    else if (strcmp(label, "P N 3 N") == 0 or
		     strcmp(label, "P n 3 n") == 0)
	      strcpy(label, "P n -3 n");
	    else if (strcmp(label, "P M 3 N") == 0 or
		     strcmp(label, "P m 3 n") == 0)
	      strcpy(label, "P m -3 n");
	    else if (strcmp(label, "P N 3 M") == 0 or
		     strcmp(label, "P n 3 m") == 0)
	      strcpy(label, "P n -3 m");
	    else if (strcmp(label, "F M 3 M") == 0 or
		     strcmp(label, "F m 3 m") == 0)
	      strcpy(label, "F m -3 m");
	    else if (strcmp(label, "F M 3 C") == 0 or
		     strcmp(label, "F m 3 c") == 0)
	      strcpy(label, "F m -3 c");
	    else if (strcmp(label, "F D 3 M") == 0 or
		     strcmp(label, "F d 3 m") == 0)
	      strcpy(label, "F d -3 m");
	    else if (strcmp(label, "F D 3 C") == 0 or
		     strcmp(label, "F d 3 c") == 0)
	      strcpy(label, "F d -3 c");
	    else if (strcmp(label, "I M 3 M") == 0 or
		     strcmp(label, "I m 3 m") == 0)
	      strcpy(label, "I m -3 m");
	    else if (strcmp(label, "I A 3 D") == 0 or
		     strcmp(label, "I a 3 d") == 0)
	      strcpy(label, "I a -3 d");
	    (void) structure->set_group(label);
	  }
	  input.getline(line, 256);
	} else if (strcmp(line, "origin") == 0) {
	  strncpy(message, "Origin keyword not supported", mlen);
	  input.getline(line, 256);
	  if (line[0] == ' ' || line[0] == '\n')
	    input.getline(line, 256);
	  // Todo
	} else if (strcmp(line, "scale") == 0) {
	  strncpy(message, "Scale keyword not supported", 256);
	  input.getline(line, 256);
	  if (line[0] == ' ' || line[0] == '\n')
	    input.getline(line, 256);
	  // Todo
	} else if (strncmp(line, "frac", 4) == 0) {
	  if (structure == 0) {
	    strncpy(message,
		    "Invalid GULP file - no cell for fractional coords", mlen);
	    ok = DLV_ERROR;
	    break;
	  }
	  // complete structure to make sure we have lattice vectors
	  structure->complete(false);
	  in_atoms = true;
	  is_frac = true;
	  input.getline(line, 256);
	} else if (strncmp(line, "cart", 4) == 0) {
	  in_atoms = true;
	  is_frac = false;
	  if (structure == 0)
	    structure = DLV::model::create_atoms(name, 0);
	  input.getline(line, 256);
	} else if (in_atoms) {
	  char symbol[3];
	  strncpy(symbol, line, 2);
	  symbol[2] = '\0';
	  if (DLV::atom_type::check_atomic_symbol(symbol) == 0)
	    break;
	  // get rest of line
	  input.getline(line, 256);
	  char buff[64];
	  DLV::coord_type coords[3];
	  // Skip 'core' etc in string
	  if (sscanf(line, "%s %lf %lf %lf", buff, &coords[0],
		     &coords[1], &coords[2]) != 4) {
	    strncpy(message, "Error reading atoms", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  } else if (strcmp(buff, "core") == 0) {
	    DLV::atom my_atom;
	    if (is_frac)
	      my_atom = structure->add_fractional_atom(symbol, coords);
	    else
	      my_atom = structure->add_cartesian_atom(symbol, coords);
	    if (my_atom == 0) {
	      strncpy(message, "Error adding atom to model", mlen - 1);
	      ok = DLV_ERROR;
	      break;
	    } else
	      natoms++;
	  }
	} // else ignore.
      }
      input.close();
      if (natoms == 0) {
	strncpy(message, "No atoms found in file", mlen);
	ok = DLV_ERROR;
      }
      if (structure != 0) {
	if (ok == DLV_OK) {
	  if (cell_params)
	    (void)structure->set_lattice_parameters(a, b, c, alpha,
						    beta, gamma);
	  structure->complete();
	} else {
	  delete structure;
	  structure = 0;
	}
      }
    }
  }
  return structure;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::extract_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)GULP::extract_structure(0, "recover");
    }

  }
}

template <class Archive>
void GULP::extract_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(GULP::extract_structure)

DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)

DLV_NORMAL_EXPLICIT(GULP::extract_structure)

#endif // DLV_USES_SERIALIZE
