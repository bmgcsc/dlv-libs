
#ifndef GULP_EXTRACT_STRUCTURE
#define GULP_EXTRACT_STRUCTURE

namespace GULP {

  class extract_structure : public DLV::load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_bonds, char message[],
			     const int_g mlen);

    // for serialization
    extract_structure(DLV::model *m, const char file[]);

  protected:
    DLV::string get_name() const;

  private:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(GULP::extract_structure)
#endif // DLV_USES_SERIALIZE

inline GULP::extract_structure::extract_structure(DLV::model *m,
						  const char file[])
  : load_atom_model_op(m, file)
{
}

#endif // GULP_EXTRACT_STRUCTURE
