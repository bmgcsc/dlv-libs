
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "load.hxx"

DLV::operation *GULP::load_phonon_bands::create(const char filename[],
						char message[],
						const int_g mlen)
{
  load_phonon_bands *op = new load_phonon_bands(filename);
  DLV::data_object *data = read_phonon_bands(op, 0, filename, filename,
					     0, 0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string GULP::load_phonon_bands::get_name() const
{
  return ("Load GULP Phonon dispersion - " + get_filename());
}

bool GULP::load_phonon_bands::reload_data(DLV::data_object *data,
					  char message[], const int_g mlen)
{
  DLV::phonon_bands *v = dynamic_cast<DLV::phonon_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload dispersion", mlen);
    return false;
  } else
    return (read_phonon_bands(this, v, get_filename().c_str(),
			      get_filename().c_str(),
			      0, 0, message, mlen) != 0);
  return false;
}

DLV::operation *GULP::load_phonon_DOS::create(const char filename[],
					      char message[], const int_g mlen)
{
  load_phonon_DOS *op = new load_phonon_DOS(filename);
  DLV::data_object *data = read_phonon_DOS(op, 0, filename, filename,
					   message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string GULP::load_phonon_DOS::get_name() const
{
  return ("Load GULP Phonon DOS - " + get_filename());
}

bool GULP::load_phonon_DOS::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::phonon_dos *v = dynamic_cast<DLV::phonon_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_phonon_DOS(this, v, get_filename().c_str(),
			    get_filename().c_str(), message, mlen) != 0);
  return false;
}

DLV::operation *GULP::load_phonon_vectors::create(const char filename[],
						  char message[],
						  const int_g mlen)
{
  load_phonon_vectors *op = new load_phonon_vectors(filename);
  DLV::data_object *data = read_phonon_vectors(op, filename, filename,
					       message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string GULP::load_phonon_vectors::get_name() const
{
  return ("Load GULP Phonon vectors - " + get_filename());
}

bool GULP::load_phonon_vectors::reload_data(DLV::data_object *data,
					    char message[], const int_g mlen)
{
  // they're stored, so don't do anything
  return true;
}

DLV::operation *GULP::load_MD_trajectory::create(const char filename[],
						 char message[],
						 const int_g mlen)
{
  const DLV::model *current = DLV::operation::get_current_model();
  DLV::model *m = current->duplicate_model(current->get_model_name() + " MD");
  DLV::time_plot *t1 = 0;
  DLV::time_plot *t2 = 0;
  DLV::time_plot *t3 = 0;
  DLV::md_trajectory *md = 0;
  load_MD_trajectory *op = new load_MD_trajectory(m, filename);
  if (read_MD_trajectory(op, t1, t2, t3, md, filename, filename,
			 m, true, message, mlen)) {
    op->set_data(t1, t2, t3, md);
    op->attach();
    op->attach_data(t1);
    op->attach_data(t2);
    op->attach_data(t3);
    op->attach_data(md);
  } else {
    delete op;
    delete m;
  }
  return op;
}

DLV::string GULP::load_MD_trajectory::get_name() const
{
  return ("Load GULP MD trajectory - " + get_filename());
}

bool GULP::load_MD_trajectory::reload_data(DLV::data_object *data,
					   char message[], const int_g mlen)
{
  return (read_MD_trajectory(this, t1, t2, t3, md, get_filename().c_str(),
			     get_filename().c_str(), get_current_model(),
			     false, message, mlen) != 0);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::load_phonon_bands *t,
				    const unsigned int file_version)
    {
      ::new(t)GULP::load_phonon_bands("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::load_phonon_DOS *t,
				    const unsigned int file_version)
    {
      ::new(t)GULP::load_phonon_DOS("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::load_phonon_vectors *t,
				    const unsigned int file_version)
    {
      ::new(t)GULP::load_phonon_vectors("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::load_MD_trajectory *t,
				    const unsigned int file_version)
    {
      ::new(t)GULP::load_MD_trajectory(0, "recover");
    }

  }
}

template <class Archive>
void GULP::load_phonon_bands::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void GULP::load_phonon_DOS::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void GULP::load_phonon_vectors::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void GULP::load_MD_trajectory::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
  ar & t1;
  ar & t2;
  ar & t3;
  ar & md;
}

BOOST_CLASS_EXPORT_IMPLEMENT(GULP::load_phonon_bands)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::load_phonon_DOS)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::load_phonon_vectors)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::load_MD_trajectory)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)
DLV_SUPPRESS_TEMPLATES(DLV::md_trajectory)
DLV_SUPPRESS_TEMPLATES(DLV::time_plot)

DLV_NORMAL_EXPLICIT(GULP::load_phonon_bands)
DLV_NORMAL_EXPLICIT(GULP::load_phonon_DOS)
DLV_NORMAL_EXPLICIT(GULP::load_phonon_vectors)
DLV_NORMAL_EXPLICIT(GULP::load_MD_trajectory)

#endif // DLV_USES_SERIALIZE
