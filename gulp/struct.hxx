
#ifndef GULP_LOAD_STRUCTURE
#define GULP_LOAD_STRUCTURE

namespace GULP {

  class load_structure : public DLV::load_atom_model_op,
			 public structure_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_vbonds, char message[],
			     const int_g mlen);

    // for serialization
    load_structure(DLV::model *m, const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(GULP::load_structure)
#endif // DLV_USES_SERIALIZE

inline GULP::load_structure::load_structure(DLV::model *m,
					    const char file[])
  : load_atom_model_op(m, file)
{
}

#endif // GULP_LOAD_STRUCTURE
