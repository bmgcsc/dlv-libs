
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
#include "calcs.hxx"
#include "grid3d.hxx"

DLV::operation *IMAGE::load_3d_data::create(const char filename[],
					    char message[], const int_g mlen)
{
  DLV::string model_name = name_from_file("", filename);
  DLV::model *structure = DLV::model::create_outline(model_name, 0, 0, 0, 0, 0, 0);
  if (structure == 0)
    strncpy(message, "Create model failed", mlen - 1);
  else {
    load_3d_data *op = new load_3d_data(structure, filename);
    DLV::data_object *data = op->read_data(op, structure, filename,
					   message, mlen);
    if (data == 0) {
      delete op;
      op = 0;
    } else {
      attach_base(op);
      op->attach_data(data);
    }
    return op;
  }
  return 0;
}

DLV::string IMAGE::load_3d_data::get_name() const
{
  return ("Load image 3D data - " + get_filename());
}

bool IMAGE::load_3d_data::reload_data(DLV::data_object *data,
				      char message[], const int_g mlen)
{
  strncpy(message, "Reload image grid3d not implemented", mlen);
  return false;
}

DLV::volume_data *IMAGE::grid3D::read_data(DLV::operation *op, DLV::model *m,
					   const char filename[],
					   char message[], const int_g mlen)
{
  DLV::volume_data *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::FILE *input = fopen(filename, "r");
    if (input != 0) {
      char label[128];
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];
      if (get_grid(input, nx, ny, nz, origin, astep, bstep, cstep,
		   label, message, mlen)) {
	data = new DLV::real_space_volume("image", filename, op, nx, ny, nz,
					  origin, astep, bstep, cstep);
	if (!get_data(input, data, nx, ny, nz, label, message, mlen)) {
	  delete data;
	  data = 0;
	} else {
	  // shift values to actual vector lengths rather than step;
	  astep[0] = nx * astep[0];
	  astep[1] = nx * astep[1];
	  astep[2] = nx * astep[2];
	  bstep[0] = ny * bstep[0];
	  bstep[1] = ny * bstep[1];
	  bstep[2] = ny * bstep[2];
	  cstep[0] = nz * cstep[0];
	  cstep[1] = nz * cstep[1];
	  cstep[2] = nz * cstep[2];
	  // make the outline box. (OAB)
	  DLV::coord_type coords[4][3];
	  coords[0][0] = origin[0];
	  coords[0][1] = origin[1];
	  coords[0][2] = origin[2];
	  coords[1][0] = origin[0] + astep[0];
	  coords[1][1] = origin[1] + astep[1];
	  coords[1][2] = origin[2] + astep[2];
	  coords[2][0] = origin[0] + astep[0] + bstep[0];
	  coords[2][1] = origin[1] + astep[1] + bstep[1];
	  coords[2][2] = origin[2] + astep[2] + bstep[2];
	  coords[3][0] = origin[0] + bstep[0];
	  coords[3][1] = origin[1] + bstep[1];
	  coords[3][2] = origin[2] + bstep[2];
	  m->add_polygon(coords, 4, message, mlen);
	  for (int i = 0; i < 4; i++)
	    for (int j = 0; j < 3; j++)
	      coords[i][j] += cstep[j];
	  m->add_polygon(coords, 4, message, mlen);
	  // OAC
	  coords[0][0] = origin[0];
	  coords[0][1] = origin[1];
	  coords[0][2] = origin[2];
	  coords[1][0] = origin[0] + astep[0];
	  coords[1][1] = origin[1] + astep[1];
	  coords[1][2] = origin[2] + astep[2];
	  coords[2][0] = origin[0] + astep[0] + cstep[0];
	  coords[2][1] = origin[1] + astep[1] + cstep[1];
	  coords[2][2] = origin[2] + astep[2] + cstep[2];
	  coords[3][0] = origin[0] + cstep[0];
	  coords[3][1] = origin[1] + cstep[1];
	  coords[3][2] = origin[2] + cstep[2];
	  m->add_polygon(coords, 4, message, mlen);
	  for (int i = 0; i < 4; i++)
	    for (int j = 0; j < 3; j++)
	      coords[i][j] += bstep[j];
	  m->add_polygon(coords, 4, message, mlen);
	  // OBC
	  coords[0][0] = origin[0];
	  coords[0][1] = origin[1];
	  coords[0][2] = origin[2];
	  coords[1][0] = origin[0] + bstep[0];
	  coords[1][1] = origin[1] + bstep[1];
	  coords[1][2] = origin[2] + bstep[2];
	  coords[2][0] = origin[0] + bstep[0] + cstep[0];
	  coords[2][1] = origin[1] + bstep[1] + cstep[1];
	  coords[2][2] = origin[2] + bstep[2] + cstep[2];
	  coords[3][0] = origin[0] + cstep[0];
	  coords[3][1] = origin[1] + cstep[1];
	  coords[3][2] = origin[2] + cstep[2];
	  m->add_polygon(coords, 4, message, mlen);
	  for (int i = 0; i < 4; i++)
	    for (int j = 0; j < 3; j++)
	      coords[i][j] += astep[j];
	  m->add_polygon(coords, 4, message, mlen);
	  m->complete();
	}
      }
      fclose(input);
    } else
      strncpy(message, "Failed to open file", mlen);
  }
  return data;
}

bool IMAGE::grid3D::get_grid(std::FILE *input, int_g &nx, int_g &ny,
			     int_g &nz, real_g origin[3], real_g astep[3],
			     real_g bstep[3], real_g cstep[3], char label[],
			     char message[], const int_g mlen)
{
  char text[128];
  char *buff = text;
  size_t s = 128;
  fgets(buff, s, input);
  sscanf(text, "%d %d %d", &nx, &ny, &nz);
  if (nx < 1 or ny < 1 or nz < 1) {
    strncpy(message,
	    "Empty grid - is this really a image volume file?", mlen);
    return false;
  } else
    return read_grid(input, origin, astep, bstep, cstep, message, mlen);
}

bool IMAGE::grid3D::read_grid(std::FILE *input, real_g origin[3],
			      real_g astep[3], real_g bstep[3], real_g cstep[3],
			      char message[], const int_g mlen)
{
  char text[128];
  char *buff = text;
  size_t s = 128;
  fgets(buff, s, input);
  sscanf(buff, "%f %f %f", &origin[0], &origin[1], &origin[2]);
  fgets(buff, s, input);
  sscanf(buff, "%f %f %f", &astep[0], &astep[1], &astep[2]);
  fgets(buff, s, input);
  sscanf(buff, "%f %f %f", &bstep[0], &bstep[1], &bstep[2]);
  fgets(buff, s, input);
  sscanf(buff, "%f %f %f", &cstep[0], &cstep[1], &cstep[2]);
  return true;
}

bool IMAGE::grid3D::get_data(std::FILE *input, DLV::volume_data *data,
			     const int_g nx, const int_g ny, const int_g nz,
			     const char label[], char message[],
			     const int_g mlen)
{
  char line[128];
  char *buff = line;
  size_t s = 128;
  fgets(buff, s, input);
  line[strlen(line) - 1] = '\0';
  return read_data(input, data, nx, ny, nz, 1, line, message, mlen);
}

bool IMAGE::grid3D::read_data(std::FILE *input, DLV::volume_data *data,
			      const int_g nx, const int_g ny, const int_g nz,
			      const int_g vec, const char label[],
			      char message[], const int_g mlen)
{
  int_l n = (int_l)nx * (int_l)ny * (int_l)nz * (int_l)vec;
  real_g *array = new real_g[n];
  if (array == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    (void) fread(array, sizeof(real_g), n, input);
    data->add_data(array, vec, label, false);
    return true;
  }
}
