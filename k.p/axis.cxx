
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "calcs.hxx"
#include "axis.hxx"
#include "../dlv/op_objs.hxx"

DLV::operation *K_P::axis::create(char message[], const int_g mlen){

  axis *op = new axis();
  DLV::data_object *data = op->generate_data(message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string K_P::axis::get_name() const
{
  return ("Load k.p Axis");
}

void K_P::axis::add_standard_data_objects()
{
  // Do nothing
}

bool K_P::axis::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload k.p Axis not implemented", mlen);
  return false;
}

DLV::data_object *K_P::axis::generate_data(char message[], const int_g mlen){


  for(int i = 0; i <= 2; i++){

    if( i == 0 ){
// x coordinates

real_g kpoints[2][3];
DLV::string labels[2];

labels[0] = "";
labels[1] = "x";

kpoints[0][0] = 0;
kpoints[1][0] = 100;

kpoints[0][1] = 0;
kpoints[1][1] = 0;

kpoints[0][2] = 0;
kpoints[1][2] = 0;


 DLV::op_line *op = new DLV::op_line("x");
 op->create_path(kpoints, labels, 2, true, message, mlen);
 //return data;
    }

    if( i == 1){	 

// y coordinates

real_g kpoints[2][3];
DLV::string labels[1];

labels[0] = "";
labels[1] = "y";

kpoints[0][0] = 0;
kpoints[1][0] = 00;

kpoints[0][1] = 0;
kpoints[1][1] = 100;

kpoints[0][2] = 0;
kpoints[1][2] = 0;


 DLV::op_line *op = new DLV::op_line("x");
 op->create_path(kpoints, labels, 2, true, message, mlen);
 //return data;
    }
	 
    if( i == 2 ){
// z coordinates

real_g kpoints[2][3];
DLV::string labels[1];

labels[0] = "";
labels[1] = "z";

kpoints[0][0] = 0;
kpoints[1][0] = 0;

kpoints[0][1] = 0;
kpoints[1][1] = 0;

kpoints[0][2] = 0;
kpoints[1][2] = 100;


 DLV::op_line *op = new DLV::op_line("x");
 op->create_path(kpoints, labels, 2, true, message, mlen);
 // Todo - what to return?
 //return data;
    }
  }
    return 0;

}
