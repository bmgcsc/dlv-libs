
#ifndef K_P_AXIS
#define K_P_AXIS

namespace K_P {

  class axis : public DLV::operation {
  public:
    static DLV::operation *create(char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);
    DLV::data_object *generate_data(char message[], const int_g mlen);

  protected:
    axis();

    DLV::string get_name() const;
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::operation>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline K_P::axis::axis()
{
}

#endif // K_P_AXIS
