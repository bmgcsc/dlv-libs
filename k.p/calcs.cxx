
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"

DLV::model *K_P::structure_file::read(const DLV::string name,
				      const char filename[],
				      char message[], const int_g mlen)
{
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      structure = DLV::model::create_outline(name, 0, 0, 0, 0, 0, 0);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	char line[256];
	DLV::coord_type coords[128][3];
	input.getline(line, 256);
	while (!input.eof()) {
	  int_g n;
	  if (sscanf(line, "%d", &n) != 1) {
	    // Should be end of line
	    break;
	  }
	  if (n == 0) {
	    // cylinder
	    DLV::coord_type r1;
	    DLV::coord_type r2;
	    DLV::coord_type z1;
	    DLV::coord_type z2;
	    DLV::int_g o;
	    input.getline(line, 256);
	    if (sscanf(line, "%lf %lf %lf %lf %d",
		       &r1, &r2, &z1, &z2, &o) != 5) {
	      strncpy(message, "Incorrect amount of cylinder data", mlen);
	      ok = DLV_ERROR;
	      break;
	    } else
	      structure->add_cylinder(r1, r2, z1, z2, o);
	  } else {
	    if (n < 3 and n > -3) {
	      strncpy(message, "Not enough sides to form polyhedra", mlen);
	      ok = DLV_ERROR;
	      break;
	    } else if (n >= 128 or n <= -128) {
	      strncpy(message, "Buffer overflow in polyhedra", mlen);
	      ok = DLV_ERROR;
	      break;
	    }	      
	    if (n < 0) {
	      structure->add_polyhedra();
	      n = -n;
	    }
	    for (int_g i = 0; i < n; i++) {
	      input.getline(line, 256);
	      if (sscanf(line, "%lf %lf %lf", &coords[i][0],
			 &coords[i][1], &coords[i][2]) != 3) {
		strncpy(message, "Incorrect number of coordinates", mlen);
		ok = DLV_ERROR;
		break;
	      }
	    }
	    if (ok != DLV_OK)
	      break;
	    if (!structure->add_polygon(coords, n, message, mlen)) {
	      ok = DLV_ERROR;
	      break;
	    }
	  }
	  input.getline(line, 256);
	}
	if (ok != DLV_OK) {
	  delete structure;
	  structure = 0;
	} else
	structure->complete();
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}
