
#ifndef K_P_VOLUME_DATA
#define K_P_VOLUME_DATA

namespace K_P {

  // interface class for 3D grids
  class grid3D {
  protected:
    DLV::volume_data *read_data(DLV::operation *op, const char filename[],int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z,
					 char message[], const int_g mlen);
    bool get_grid(std::FILE *input, DLV::volume_data *data,
			    const int_g N_x, const int_g N_y, const int_g N_z,const
                            real_g L_x, const real_g L_y, const real_g L_z, int_g &nx, int_g &ny, int_g &nz,
		  real_g origin[3], real_g astep[3], real_g bstep[3],
		  real_g cstep[3], char label[], char message[],
		  const int_g mlen);
    bool get_data(std::FILE *input, DLV::volume_data *data,const int_g N_x, const int_g N_y, const int_g N_z,const
		  real_g L_x, const real_g L_y, const real_g L_z,
		  const int_g nx, const int_g ny, const int_g nz,
		  const char label[],
		  char message[], const int_g mlen);
    bool read_grid(std::FILE *input, DLV::volume_data *data,
			    const int_g N_x, const int_g N_y, const int_g N_z,const real_g L_x, const real_g L_y, const real_g L_z, real_g origin[3], real_g astep[3],
		   real_g bstep[3], real_g cstep[3], char message[],
		   const int_g mlen);
    bool read_data(std::FILE *input, DLV::volume_data *data,const int_g N_x, const int_g N_y, const int_g N_z,const
                            real_g L_x, const real_g L_y, const real_g L_z,
			    const int_g vec, const char label[], char message[], const int_g mlen);
  };

  class load_3d_data : public DLV::load_data_op, public grid3D {
  public:
    static DLV::operation *create(const char filename[], int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z,
					  char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    load_3d_data(const char file[],int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::load_data_op>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline K_P::load_3d_data::load_3d_data(const char file[],int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z)
  : load_data_op(file)
{
}

#endif // K_P_VOLUME_DATA
