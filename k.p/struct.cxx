
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "struct.hxx"

DLV::operation *K_P::load_structure::create(const char name[],
					    const char filename[],
					    char message[], const int_g mlen)
{
  DLV::string model_name = name_from_file(name, filename);
  DLV::model *structure = read(model_name, filename, message, mlen);
  load_structure *op = 0;
  if (structure != 0) {
    op = new load_structure(structure, filename);
    attach_base(op);
  }
  return op;
}

DLV::string K_P::load_structure::get_name() const
{
  return (get_model_name() + " - Load k.p structure file");
}
