
CXX = g++
PIC = -fPIC
CXXFLAGS = -g -std=c++11 -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DDLV_USES_SERIALIZE -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API
# -DENABLE_DLV_GRAPHICS
# -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API
# -DDLV_USES_SERIALIZE
# -DENABLE_ROD
# release code
#CXXFLAGS = -g -O2 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -DBOOST_DISABLE_ASSERTS
CXXTEMPLATES = $(CXXFLAGS)
SHARED = -shared
LD = $(CXX)
LDFLAGS = $(CXXFLAGS) $(SHARED)
SO = so

TOOLDIR = avs
TOOLSRC = $(SRCDIR)/$(TOOLDIR)
TOOLLIB = $(LIBDIR)/libDLVgraphics_$(TOOLDIR).$(SO)

BOOSTDIR =
BOOSTLIB =

PYHDR = /usr/include/python3.10

CCTBXDIR = /home/bgs/projects/cctbx/include
CCTBXLIB = /home/bgs/projects/cctbx/libcctbx.a

RODDIR = ../../rod/src
RODLIB = ../../rod/librod.a

# Only for DLV_DL
#BABELDIR = ../../babel/include/openbabel-2.0
#BABELLIB = /home/bgs/express/3.0/babel/lib/libopenbabel.a
#else
BABELDIR =
BABELLIB =

GUILIB = 

INCDIRS = -I$(CCTBXDIR) -I$(RODDIR) -I$(RODDIR)/include

GRAPHICSLIBS = $(GUILIB)
BASETYPELIBS = -lDLVgraphics_$(TOOLDIR) -lboost_serialization -lboost_filesystem -lboost_thread
WULFFLIBS =
SYMMETRYLIBS = $(CCTBXLIB)
MODELLIBS = $(PDBIOLIB) -lboost_serialization
DATALIBS = -lboost_serialization
DATAMODELLIBS = -lboost_serialization
