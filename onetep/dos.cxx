
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/atom_prefs.hxx"
#include "calcs.hxx"
#include "dos.hxx"

DLV::operation *ONETEP::load_dos_data::create(const char filename[],
					      char message[], const int_g mlen)
{
  load_dos_data *op = new load_dos_data(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 1, false, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string ONETEP::load_dos_data::get_name() const
{
  return ("Load ONETEP Density of States - " + get_filename());
}

DLV::electron_dos *ONETEP::dos_file::read_data(DLV::operation *op,
					       DLV::electron_dos *plot,
					       const char filename[],
					       const DLV::string id,
					       const int_g nproj,
					       const bool next,
					       char message[],
					       const int_g mlen)
{
  DLV::electron_dos *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[128];
      input.getline(line, 128);
      if (line[0] != '#')
	strncpy(message, "Invalid header to DOS file", mlen);
      else {
	if (plot == 0)
	  data = new DLV::electron_dos("ONETEP", id, op, nproj, (nproj == 2));
	else
	  data = plot;
	// # Energy (units) / DOS
	const int max_data = 20000;
	real_g grid[max_data];
	real_g dos_data[max_data];
	int_g i = 0;
	while (!input.eof()) {
	  input >> grid[i];
	  // Todo - is this safe?
	  if (i > 0) {
	    if (grid[i] < grid[i - 1]) {
	      break;
	    }
	  }
	  input >> dos_data[i];
	  i++;
	}
	if (next) {
	  for (int np = 0; np < i; np++)
	    dos_data[np] = - dos_data[np];
	  data->add_plot(dos_data, 1, "Total beta DOS", true);
	} else {
	  data->set_grid(grid, i, true);
	  if (nproj == 2)
	    data->add_plot(dos_data, 0, "Total alpha DOS", true);
	  else
	    data->add_plot(dos_data, 0, "Total DOS", true);
	  data->set_xaxis_label("Energy (eV)");
	  data->set_xunits("eV");
	}
      }
      input.close();
    }
  }
  return data;
}

bool ONETEP::load_dos_data::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_dos *v = dynamic_cast<DLV::electron_dos *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload DOS", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), 1, false, message, mlen) != 0);
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ONETEP::load_dos_data *t,
				    const unsigned int file_version)
    {
      ::new(t)ONETEP::load_dos_data("recover");
    }

  }
}

template <class Archive>
void ONETEP::load_dos_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::load_dos_data)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)

DLV_NORMAL_EXPLICIT(ONETEP::load_dos_data)

#endif // DLV_USES_SERIALIZE
