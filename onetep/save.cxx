
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "save.hxx"

DLV::operation *ONETEP::save_structure::create(const char filename[],
					       char message[], const int_g mlen)
{
  save_structure *op = new save_structure(filename);
  if (op == 0)
    strncpy(message, "Failed to allocate ONETEP::save operation", mlen);
  else {
    message[0] = '\0';
    std::ofstream output;
    if (DLV::open_file_write(output, filename, message, mlen)) {
      op->write(output, op->get_current_model(), 0, message, mlen);
      output.close();
    }
    if (strlen(message) > 0) {
      delete op;
      op = 0;
    } else
      op->attach();
  }
  return op;
}

DLV::string ONETEP::save_structure::get_name() const
{
  return ("Save ONETEP cell file - " + get_filename());
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ONETEP::save_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)ONETEP::save_structure("recover");
    }

  }
}

template <class Archive>
void ONETEP::save_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::save_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::save_structure)

DLV_SUPPRESS_TEMPLATES(DLV::save_model_op)

DLV_NORMAL_EXPLICIT(ONETEP::save_structure)

#endif // DLV_USES_SERIALIZE
