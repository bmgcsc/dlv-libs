
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../dlv/constants.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  ifdef DLV_USES_AVS_GRAPHICS
     //#include "../avs/toolkit_obj.hxx"
#  endif // DLV_USES_AVS_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/op_data.hxx"
#include "../dlv/atom_prefs.hxx"
#include "calcs.hxx"
#include "dos.hxx" // props read
#include "grid3d.hxx" // props read
#include "scf.hxx"
#include "scf_impl.hxx"

DLV::string ONETEP::scf_base::serial_binary = "onetep";
DLV::string ONETEP::scf_base::parallel_binary = "onetep";

#ifndef NO_DLV_CALCS

#  include "../graphics/calculations.hxx"

DLV::string ONETEP::scf_base::get_path() const
{
  return DLV::get_code_path("ONETEP");
}

#endif // !NO_DLV_CALCS

DLV::string ONETEP::scf_base::get_serial_executable() const
{
  static bool binset = false;
  if (!binset) {
#ifdef WIN32
    serial_binary += ".exe";
#else
    char *machine = getenv("MACHINE");
    // binary seems to override linux_64 to linux_64_el5 
    if (strncmp(machine, "linux_64", 8) == 0)
      serial_binary += ".x86_64";
    else {
      DLV::string path = get_path();
      DLV::string name = path + serial_binary + ".i686";
      if (DLV::file_exists(name.c_str()))
	serial_binary += ".i686";
      else {
	DLV::string name = path + serial_binary + ".i586";
	if (DLV::file_exists(name.c_str()))
	  serial_binary += ".i586";
	else
	  serial_binary += ".i386";
      }
    }
#endif // !WIN32
    binset = true;
  }
  return serial_binary;
}

inline DLV::string ONETEP::scf_base::get_parallel_executable() const
{
  return parallel_binary;
}

ONETEP::Hamiltonian::Hamiltonian(const bool u, const int f, const int_g d,
				 const bool s, const int_g sp)
  : unrestricted(u), functional(dft_capz), dispersion(d), set_spin(s), spin(sp)
{
  switch (f) {
  case 0:
    functional = dft_capz;
    break;
  case 1:
    functional = dft_vwn;
    break;
  case 2:
    functional = dft_pw91;
    break;
  case 3:
    functional = dft_pbe;
    break;
  case 4:
    functional = dft_revpbe;
    break;
  case 5:
    functional = dft_rpbe;
    break;
  case 6:
    functional = dft_blyp;
    break;
  case 7:
    functional = dft_xlyp;
    break;
  default:
    // error
    break;
  }
}

ONETEP::scf_data::~scf_data()
{
}

DLV::operation *ONETEP::scf_base::create(const int_g t,
					 char message[], const int_g mlen)
{
  scf_base *op = 0;
  message[0] = '\0';
  switch (t) {
  case 0:
    op = new scf_calc();
    break;
  case 1:
    op = new opt_calc();
    break;
    //case 2:
    //op = new md_calc();
    //break;
  case 3:
    op = new prop_calc();
    break;
  default:
    strncpy(message, "BUG: ONETEP SCF task not supported", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate ONETEP::scf operation", mlen);
  } else {
    message[0] = '\0';
    if (t == 1) { // opt
      op->attach_pending_copy_model("ONETEP opt");
      // otherwise toggle atom flags isn't available
      //op->render_r(message, mlen);
    } else // scf, props
      op->attach_pending();
  }
  return op;
}

bool ONETEP::scf_base::no_errors(char message[], const int_g len)
{
  // Todo?
  return true;
}

bool ONETEP::scf_base::add_pseudo(const int_g ngwfs, const real_g radius,
				  const char filename[], const bool set_default,
				  bool &complete, char message[],
				  const int_g mlen)
{
  DLV::operation *base = get_editing();
  scf_base *op = dynamic_cast<scf_base *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not a SCF calculation", mlen);
    return false;
  }
  if (!op->show_basis) {
#ifdef ENABLE_DLV_GRAPHICS
    op->toggle_atom_flags("ONETEP pseudopotentials");
#endif // ENABLE_DLV_GRAPHICS
    op->show_basis = true;
  }
  message[0] = '\0';
  bool ok = true;
  int_g *indices = 0;
  int_g value = -1;
  ok = op->add_pseudo_pot(ngwfs, radius, filename, set_default,
			  indices, value, message, mlen);
#ifdef ENABLE_DLV_GRAPHICS
  if (ok) {
    ok = op->set_atom_index_and_flags(indices, value, set_default,
				      message, mlen);
    if (ok)
      //op->set_selected_atom_flags(true);
      (void) op->deselect_atoms(message, mlen);
  }
#endif // ENABLE_DLV_GRAPHICS
  complete = op->check_pseudos();
  return ok;
}

bool ONETEP::scf_calc::add_pseudo_pot(const int_g nfns, const real_g radius,
				      const char filename[],
				      const bool set_default,
				      int_g * &indices, int_g &value,
				      char message[], const int_g mlen)
{
  return scf_data::add_pseudo(nfns, radius, filename, set_default,
			      get_model(), indices, value, message, mlen);
}

bool ONETEP::opt_calc::add_pseudo_pot(const int_g nfns, const real_g radius,
				      const char filename[],
				      const bool set_default,
				      int_g * &indices, int_g &value,
				      char message[], const int_g mlen)
{
  return scf_data::add_pseudo(nfns, radius, filename, set_default,
			      get_model(), indices, value, message, mlen);
}

bool ONETEP::prop_calc::add_pseudo_pot(const int_g nfns, const real_g radius,
				       const char filename[],
				       const bool set_default,
				       int_g * &indices, int_g &value,
				       char message[], const int_g mlen)
{
  strncpy(message, "BUG: adding pseudo to ONETEP properties", mlen);
  return false;
}

bool ONETEP::scf_calc::check_pseudos() const
{
  return scf_data::check_pseudos(get_model());
}

bool ONETEP::opt_calc::check_pseudos() const
{
  return scf_data::check_pseudos(get_model());
}

bool ONETEP::prop_calc::check_pseudos() const
{
  return true;
}

bool ONETEP::scf_data::check_pseudos(const DLV::model *const m) const
{
  // test that we have enough for a calc.
  int count = 0;
  for (int j = 0; j < natoms; j++)
    if (basis_indices[j] != -1)
      count++;
  if (count < natoms) {
    int_g *atn = new_local_array1(int_g, natoms);
    m->get_asym_atom_types(atn, natoms);
    for (int j = 0; j < natoms; j++) {
      if (basis_indices[j] == -1) {
	for (int k = 0; k < natoms; k++) {
	  if (atn[k] == atn[j] and basis_indices[k] != -1) {
	    count++;
	    break;
	  }
	}
      }
    }
    delete_local_array(atn);
  }
  return (count == natoms);
}

bool ONETEP::scf_data::add_pseudo(const int_g nfns, const real_g radius,
				  const char filename[], const bool set_default,
				  const DLV::model *const m,
				  int_g * &indices, int_g &value,
				  char message[], const int_g mlen)
{
  // Todo - set_default seems to be unused.
  if (basis_indices == 0) {
    natoms = m->get_number_of_asym_atoms();
    basis_indices = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      basis_indices[i] = -1;
  }
  pseudo_info data(nfns, radius, filename);
  value = (int)atom_bases.size();
  atom_bases.push_back(data);
  indices = basis_indices;
  return true;
}

bool ONETEP::scf_base::calculate(const Tolerances &tol, const Hamiltonian &h,
				 const Properties &props, const Optimise &opt,
				 const DLV::job_setup_data &job,
				 const bool extern_job,
				 const char extern_dir[],
				 char message[], const int_g mlen)
{
  // I could do this via a virtual in DLV::operation, but I don't want to
  // have to alter CCP3core to add CCP3calc options.
  DLV::operation *base = get_editing();
  scf_base *op = dynamic_cast<scf_base *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not an SCF calculation", mlen);
    return false;
  }
  message[0] = '\0';
  op->set_params(tol, h, props, opt);
  (void) accept_pending();
  if (op->find_restart(message, mlen)) {
    if (op->create_files(job.is_parallel(), job.is_local(), message, mlen)) {
      if (op->make_directory(message, mlen)) {  
	op->write(op->get_infile_name(0), op->get_model(), message, mlen);
      }
    }
  }
  if (strlen(message) == 0) {
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(), op->get_base_name(), job, true,
		extern_job, extern_dir, message, mlen);
    return true;
  } else
    return false;
}

DLV::string ONETEP::scf_calc::get_name() const
{
  return ("ONETEP SCF calculation");
}

DLV::string ONETEP::prop_calc::get_name() const
{
  return ("ONETEP Properties calculation");
}

DLV::string ONETEP::opt_calc::get_name() const
{
  return ("ONETEP structure optimisation calculation");
}

bool ONETEP::scf_calc::find_restart(char message[], const int_g mlen)
{
  // Todo - restart do something else
  return true;
}

bool ONETEP::opt_calc::find_restart(char message[], const int_g mlen)
{
  // Todo - restart do something else
  return true;
}

bool ONETEP::prop_calc::find_restart(char message[], const int_g mlen)
{
  DLV::operation *op = find_parent(dkernel_label, program_name);
  scf = dynamic_cast<scf_calc *>(op);
  if (scf == 0) {
    strncpy(message, "Failed to find ONETEP SCF calculation", mlen);
    return false;
  } else
    return true;
}

void ONETEP::scf_calc::get_scf_files(DLV::string &dkn, DLV::string &ngwfs) const
{
  dkn = DLV::add_full_path(get_outfile_name(0));
  ngwfs = DLV::add_full_path(get_outfile_name(1));
}

bool ONETEP::scf_base::common_files(const bool is_local, const char tag[])
{
  add_input_file(0, tag, "dat", is_local, false);
  //add_restart_file(tag, is_local);
  //add_opt_restart_file(tag, is_local);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "job", is_local, true);
  return true;
}

DLV::string ONETEP::scf_calc::get_base_name() const
{
  DLV::string base = make_name("scf", "");
  // strip '.'
  return base.substr(0, base.length() - 1);
}

DLV::string ONETEP::opt_calc::get_base_name() const
{
  DLV::string base = make_name("opt", "");
  // strip '.'
  return base.substr(0, base.length() - 1);
}

DLV::string ONETEP::prop_calc::get_base_name() const
{
  DLV::string base = make_name("props", "");
  // strip '.'
  return base.substr(0, base.length() - 1);
}

bool ONETEP::scf_calc::create_files(const bool is_parallel, const bool is_local,
				    char message[], const int_g mlen)
{
  static char tag[] = "scf";
  bool ok = false;
  if (common_files(is_local, tag)) {
    if (add_restart(is_local))
      ok = add_output(tag, is_local, message, mlen);
  }
  return ok;
}

bool ONETEP::opt_calc::create_files(const bool is_parallel, const bool is_local,
				    char message[], const int_g mlen)
{
  static char tag[] = "opt";
  bool ok = false;
  if (common_files(is_local, tag)) {
    if (add_restart(is_local))
      ok = add_output(tag, is_local, message, mlen);
  }
  return ok;
}

bool ONETEP::prop_calc::create_files(const bool is_parallel,
				     const bool is_local,
				     char message[], const int_g mlen)
{
  static char tag[] = "props";
  bool ok = false;
  if (common_files(is_local, tag)) {
    if (add_restart(is_local))
      ok = add_output(tag, is_local, message, mlen);
  }
  return ok;
}

bool ONETEP::scf_calc::add_restart(const bool is_local)
{
  // Todo - if restart then should do something
  return true;
}

bool ONETEP::opt_calc::add_restart(const bool is_local)
{
  // Todo - if restart then should do something
  return true;
}

bool ONETEP::prop_calc::add_restart(const bool is_local)
{
  DLV::string dknfile;
  DLV::string ngwfsfile;
  scf->get_scf_files(dknfile, ngwfsfile);
  DLV::string name = get_base_name();
  add_data_file(1, dknfile, name + ".dkn", is_local, true);
  add_data_file(2, ngwfsfile, name + ".tightbox_ngwfs", is_local, true);
  return true;
}

bool ONETEP::scf_calc::add_output(const char tag[], const bool is_local,
				  char message[], const int_g mlen)
{
  add_output_file(0, tag, "dkn", is_local, true);
  add_output_file(1, tag, "tightbox_ngwfs", is_local, true);
  add_properties(get_props(), is_local, tag, has_spin(), 2);
  return true;
}

bool ONETEP::opt_calc::add_output(const char tag[], const bool is_local,
				  char message[], const int_g mlen)
{
  add_output_file(0, tag, "geom", is_local, true);
  add_output_file(1, tag, "continuation", is_local, true);
  return true;
}

bool ONETEP::prop_calc::add_output(const char tag[], const bool is_local,
				   char message[], const int_g mlen)
{
  add_properties(get_props(), is_local, tag, has_spin(), 0);
  return true;
}

void ONETEP::scf_base::add_properties(const Properties &props,
				      const bool is_local, const char tag[],
				      const bool spin, const int_g n)
{
  // Todo spin
  DLV::string name = get_base_name();
  DLV::string main_tag = tag;
  int count = n;
  if (props.num_homo_plots > 0) {
    DLV::string homo_tag = main_tag + "_HOMO";
    if (spin) {
      DLV::string spin_tag = "_UP";
      add_output_file(count, homo_tag + spin_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_homo_plots; i++) {
	char text[8];
	snprintf(text, 8, "-%1d", i);
	DLV::string next_tag = homo_tag + text;
	add_output_file(count, next_tag + spin_tag, "cube", is_local);
	count++;
      }
      spin_tag = "_DN";
      add_output_file(count, homo_tag + spin_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_homo_plots; i++) {
	char text[8];
	snprintf(text, 8, "-%1d", i);
	DLV::string next_tag = homo_tag + text;
	add_output_file(count, next_tag + spin_tag, "cube", is_local);
	count++;
      }
    } else {
      add_output_file(count, homo_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_homo_plots; i++) {
	char text[8];
	snprintf(text, 8, "-%1d", i);
	DLV::string next_tag = homo_tag + text;
	add_output_file(count, next_tag, "cube", is_local);
	count++;
      }
    }
  }
  if (props.num_lumo_plots > 0) {
    DLV::string lumo_tag = main_tag + "_LUMO";
    if (spin) {
      DLV::string spin_tag = "_UP";
      add_output_file(count, lumo_tag + spin_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_lumo_plots; i++) {
	char text[8];
	snprintf(text, 8, "+%1d", i);
	DLV::string next_tag = lumo_tag + text;
	add_output_file(count, next_tag + spin_tag, "cube", is_local);
	count++;
      }
      spin_tag = "_DN";
      add_output_file(count, lumo_tag + spin_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_lumo_plots; i++) {
	char text[8];
	snprintf(text, 8, "+%1d", i);
	DLV::string next_tag = lumo_tag + text;
	add_output_file(count, next_tag + spin_tag, "cube", is_local);
	count++;
      }
    } else {
      add_output_file(count, lumo_tag, "cube", is_local);
      count++;
      for (int_g i = 1; i < props.num_lumo_plots; i++) {
	char text[8];
	snprintf(text, 8, "+%1d", i);
	DLV::string next_tag = lumo_tag + text;
	add_output_file(count, next_tag, "cube", is_local);
	count++;
      }
    }
  }
  if (props.dos_calc) {
    if (spin) {
      DLV::string dos_tag = main_tag + "_DOS_UP";
      add_output_file(count, dos_tag, "txt", is_local);
      count++;
      dos_tag = main_tag + "_DOS_DN";
      add_output_file(count, dos_tag, "txt", is_local);
      count++;
    } else {
      DLV::string dos_tag = main_tag + "_DOS";
      add_output_file(count, dos_tag, "txt", is_local);
      count++;
    }
  }
  if (props.calc_density) {
    DLV::string pd_tag = main_tag + "_density";
    add_output_file(count, pd_tag, "cube", is_local);
    count++;
    if (spin) {
      pd_tag = main_tag + "_spindensity";
      add_output_file(count, pd_tag, "cube", is_local);
      count++;
    }
    pd_tag = main_tag + "_potential";
    add_output_file(count, pd_tag, "cube", is_local);
    count++;
  }
  // No file for mulliken apparently
  //if (props.mulliken_calc) {
}

void ONETEP::scf_data::set_params(const Tolerances &tol, const Hamiltonian &ham)
{
  tolerances = tol;
  hamiltonian = ham;
}

void ONETEP::prop_data::set_params(const Properties &props)
{
  properties = props;
}

void ONETEP::opt_data::set_params(const Optimise &opt)
{
  optimise = opt;
}

void ONETEP::scf_calc::set_params(const Tolerances &tol, const Hamiltonian &ham,
				  const Properties &p, const Optimise &)
{
  scf_data::set_params(tol, ham);
  prop_data::set_params(p);
}

void ONETEP::opt_calc::set_params(const Tolerances &tol, const Hamiltonian &ham,
				  const Properties &, const Optimise &opt)
{
  scf_data::set_params(tol, ham);
  opt_data::set_params(opt);
}

void ONETEP::prop_calc::set_params(const Tolerances &, const Hamiltonian &,
				   const Properties &p, const Optimise &)
{
  prop_data::set_params(p);
}

bool ONETEP::scf_calc::write(const DLV::string filename,
			     const DLV::model *const structure,
			     char message[], const int_g mlen)
{
  return scf_data::write_input(single_point, filename, structure,
			       message, mlen);
}

bool ONETEP::opt_calc::write(const DLV::string filename,
			     const DLV::model *const structure,
			     char message[], const int_g mlen)
{
  return scf_data::write_input(optimisation_calc, filename, structure,
			       message, mlen);
}

bool ONETEP::scf_data::write_scf(std::ofstream &output,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  cell_file::write(output, structure, species, message, mlen);
  bool ok = write_pseudos(output, structure, message, mlen);
  output << '\n';
  write_tols(output);
  output << '\n';
  write_hamiltonian(output, false);
  return ok;
}

bool ONETEP::scf_calc::write_scf_data(std::ofstream &output,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  return scf_data::write_scf(output, structure, message, mlen);
}

bool ONETEP::prop_calc::write(const DLV::string filename,
			      const DLV::model *const structure,
			      char message[], const int_g mlen)
{
  std::ofstream output;
  bool ok = true;
  if ((ok = DLV::open_file_write(output, filename.c_str(), message, mlen))) {
    output << "# DLV generated properties file";
    output << "\ntask PROPERTIES\n\n";
    ok = scf->write_scf_data(output, structure, message, mlen);
    if (ok) {
      output << '\n';
      output << "write_denskern F\n";
      output << "write_tightbox_ngwfs F\n";
      output << "read_denskern T\n";
      output << "read_tightbox_ngwfs T\n";
      ok = prop_data::write_input(output, false, message, mlen);
    }
    output.close();
    return ok;
  } else
    return false;
}

bool ONETEP::scf_data::setup_species(const DLV::model *const structure)
{
  if (basis_indices == 0) {
    natoms = structure->get_number_of_asym_atoms();
    basis_indices = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      basis_indices[i] = -1;
  }
  species = new int_g[natoms];
  int_g index[100];
  for (int_g i = 0; i < 100; i++)
    index[i] = 1;
  int_g *atn = new_local_array1(int_g, natoms);
  structure->get_asym_atom_types(atn, natoms);
  if (atom_bases.size() > 0) {
    std::list<pseudo_info>::iterator ptr;
    int_g p = 0;
    for (ptr = atom_bases.begin(); ptr != atom_bases.end(); ++ptr ) {
      int_g j;
      for (j = 0; j < natoms; j++)
	if (basis_indices[j] == p)
	  break;
      if (j < natoms) {
	species[j] = index[atn[j]];
	index[atn[j]]++;
	ptr->index = species[j];
	for (int_g k = 0; k < natoms; k++) {
	  if (j != k) {
	    if (atn[k] == atn[j]) {
	      if (basis_indices[k] == p or basis_indices[k] == -1)
		species[k] = species[j];
	    }
	  }
	}
      }
      p++;
    }
  }
  delete_local_array(atn);
  return true;
}

bool ONETEP::scf_data::write_input(const task_type task,
				   const DLV::string filename,
				   const DLV::model *const structure,
				   char message[], const int_g mlen)
{
  if (setup_species(structure)) {
    std::ofstream output;
    bool ok;
    if ((ok = DLV::open_file_write(output, filename.c_str(), message, mlen))) {
      output << "# DLV generated input file for ";
      output << structure->get_model_name().c_str() << '\n';
      switch (task) {
      case single_point:
	output << "\ntask SINGLEPOINT\n\n";
	break;
      case optimisation_calc:
	output << "\ntask GEOMETRYOPTIMIZATION\n\n";
	break;
      case molecular_dynamics:
	output << "\ntask MOLECULARDYNAMICS\n\n";
	break;
      case properties:
	// Oops - shouldn't be here
	output << "\ntask PROPERTIES\n\n";
	break;
      case transition_state:
	output << "\ntask TRANSITIONSTATESEARCH\n\n";
	break;
      };
      cell_file::write(output, structure, species, message, mlen);
      if (write_pseudos(output, structure, message, mlen)) {
      } else
	ok = false;
      if (ok)
	ok = write_controls(output, message, mlen);
      output.close();
    }
    return ok;
  } else
    return false;
}

bool ONETEP::scf_data::write_pseudos(std::ofstream &output,
				     const DLV::model *const structure,
				     char message[], const int_g mlen) const
{
  int_g *atn = new_local_array1(int_g, natoms);
  structure->get_asym_atom_types(atn, natoms);
  //int_g *charges = new_local_array1(int_g, natoms);
  //structure->get_asym_atom_charges(charges, natoms);
  //bool *use_charge = new_local_array1(bool, natoms);
  //for (int_g i = 0; i < natoms; i++)
  //  use_charge[i] = true;
  if (atom_bases.size() > 0) {
    output << "\n%block species\n";
    std::list<pseudo_info>::const_iterator ptr;
    int_g p = 0;
    for (ptr = atom_bases.begin(); ptr != atom_bases.end(); ++ptr ) {
      int_g j;
      for (j = 0; j < natoms; j++)
	if (basis_indices[j] == p)
	  break;
      if (j < natoms) {
	output << DLV::atomic_prefs::get_symbol(atn[j]);
	output << species[j] << ' ';
	output << DLV::atomic_prefs::get_symbol(atn[j]) << ' ';
	output << atn[j] << ' ';
	output << ptr->n_functions << ' ';
	output << ptr->radius << '\n';
      }
      p++;
    }
    output << "%endblock species\n" << "\n%block species_pot\n";
    p = 0;
    for (ptr = atom_bases.begin(); ptr != atom_bases.end(); ++ptr ) {
      int_g j;
      for (j = 0; j < natoms; j++)
	if (basis_indices[j] == p)
	  break;
      if (j < natoms) {
	output << DLV::atomic_prefs::get_symbol(atn[j]);
	output << species[j] << " \"";
	output << ptr->name << "\"\n";
      }
      p++;
    }
    output << "%endblock species_pot\n" << '\n';
  }
  delete_local_array(atn);
  return true;
}

bool ONETEP::scf_data::write_controls(std::ofstream &output,
				      char message[], const int_g mlen) const
{
  output << '\n';
  write_tols(output);
  output << '\n';
  write_hamiltonian(output, true);
  write_task(output);
  return write_props(output, message, mlen);
}

void ONETEP::scf_data::write_tols(std::ofstream &output) const
{
  output << "cutoff_energy " << tolerances.energy_cutoff << " eV\n";
  output << "kernel_cutoff " << tolerances.kernel_cutoff << '\n';
  output << "fine_grid_scale " << tolerances.fine_grid << '\n';
}

void ONETEP::scf_data::write_hamiltonian(std::ofstream &output,
					 const bool is_scf) const
{
  if (hamiltonian.unrestricted) {
    output << "spin_polarized T\n";
    if (hamiltonian.set_spin and is_scf)
      output << "spin " << hamiltonian.spin << '\n';
  }
  output << "xc_functional ";
  switch (hamiltonian.functional) {
  case Hamiltonian::dft_capz:
    output << "CAPZ\n";
    break;
  case Hamiltonian::dft_vwn:
    output << "VWN\n";
    break;
  case Hamiltonian::dft_pw91:
    output << "PW91\n";
    break;
  case Hamiltonian::dft_pbe:
    output << "PBE\n";
    break;
  case Hamiltonian::dft_revpbe:
    output << "REVPBE\n";
    break;
  case Hamiltonian::dft_rpbe:
    output << "RPBE\n";
    break;
  case Hamiltonian::dft_blyp:
    output << "BLYP\n";
    break;
  case Hamiltonian::dft_xlyp:
    output << "XLYP\n";
    break;
  default:
    break;
  }
  if (hamiltonian.dispersion > 0)
    output << "dispersion " << hamiltonian.dispersion << '\n';
}

void ONETEP::scf_data::write_task(std::ofstream &output) const
{
}

void ONETEP::opt_data::write_task(std::ofstream &output) const
{
  output << '\n';
  output << "geom_method ";
  if (optimise.method == 1)
    output << "DELOCALIZED\n";
  else
    output << "CARTESIAN\n";
  output << "geom_max_iter " << optimise.max_iterations << '\n';
  output << "geom_energy_tol " << optimise.energy_tol << " hartree\n";
  output << "geom_force_tol " << optimise.force_tol  << " \"ha/bohr\"\n";
  output << "geom_disp_tol " << optimise.displacement_tol  << " bohr\n";
}

bool ONETEP::scf_data::write_props(std::ofstream &output,
				   char message[], const int_g mlen) const
{
  return true;
}

bool ONETEP::scf_calc::write_props(std::ofstream &output,
				   char message[], const int_g mlen) const
{
  return prop_data::write_input(output, true, message, mlen);
}

bool ONETEP::prop_data::write_input(std::ofstream &output, const bool is_scf,
				    char message[], const int_g mlen) const
{
  if (properties.num_homo_plots == 0 and properties.num_lumo_plots == 0 and
      properties.num_eigenvalues == 0 and !properties.dos_calc and
      !properties.calc_density and !properties.mulliken_calc) {
    if (is_scf)
      return true;
    else {
      strncpy(message, "No properties selected for Properties calc", mlen);
      return false;
    }
  }
  output << '\n';
  if (is_scf)
    output << "do_properties T\n";
  output << "cube_format T\n";
  output << "grd_format F\n";
  output << "homo_dens_plot " << properties.num_homo_plots - 1 << '\n';
  output << "lumo_dens_plot " << properties.num_lumo_plots - 1 << '\n';
  output << "num_eigenvalues " << properties.num_eigenvalues << '\n';
  if (properties.dos_calc)
    output << "dos_smear " << properties.dos_smearing << " eV\n";
  else
    output << "dos_smear -0.1 eV\n";
  if (properties.calc_density)
    output << "write_density_plot T\n";
  else
    output << "write_density_plot F\n";
  if (properties.mulliken_calc) {
    output << "popn_calculate T\n";
    output << "popn_bond_cutoff " << properties.mulliken_radius << " ang\n";
  } else
    output << "popn_calculate F\n";
  return true;
}

DLV::string ONETEP::prop_data::get_label(std::ifstream &input,
					 const DLV::string tag,
					 const DLV::string id)
{
  char line[128];
  input.getline(line, 128);
  input.getline(line, 128);
  // process label
  DLV::string label = line;
  DLV::string::size_type p = label.find("for:");
  if (p == DLV::string::npos)
    p = label.find("\"");
  label = label.substr(0, p - 1);
  if (tag.length() > 0) {
    label += tag;
    label += ", ";
    label += id;
  }
  return label;
}

DLV::volume_data *
ONETEP::prop_data::create_data(const DLV::string label, DLV::operation *op,
			       const int_g nx, const int_g ny, const int_g nz,
			       const real_g origin[3], const real_g astep[3],
			       const real_g bstep[3], const real_g cstep[3])
{
  return new DLV::rspace_periodic_volume("ONETEP", label, op, nx, ny, nz,
					 origin, astep, bstep, cstep);
}

void ONETEP::opt_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

bool ONETEP::scf_calc::recover(const bool no_err, const bool log_ok,
			       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "SCF output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"SCF(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    DLV::string name = DLV::add_full_path(get_outfile_name(0));
    data = new DLV::file_data(name, program_name, dkernel_label);
    attach_data(data);
    name = DLV::add_full_path(get_outfile_name(1));
    data = new DLV::file_data(name, program_name, ngwfs_label);
    attach_data(data);
    //const DLV::model *const m = get_model();
#ifdef ENABLE_DLV_GRAPHICS
    get_display_obj()->set_ONETEP_scf();
#endif // ENABLE_DLV_GRAPHICS
    return recover_props(message, len);
  }
  return true;
}

bool ONETEP::opt_calc::recover(const bool no_err, const bool log_ok,
			       char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "ONETEP optimise";
    DLV::model *m = read_geom(label, get_outfile_name(0).c_str(),
			      message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Optimisation output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimisation(failed) output file");
    attach_data(data);
  }
  /*
  if (no_err) {
    DLV::string name = DLV::add_full_path(get_outfile_name(0));
    data = new DLV::file_data(name, program_name, dkernel_label);
    attach_data(data);
    name = DLV::add_full_path(get_outfile_name(1));
    data = new DLV::file_data(name, program_name, ngwfs_label);
    attach_data(data);
    //const DLV::model *const m = get_model();
    //get_display_obj()->set_ONETEP_scf();
    //return recover_props(message, len);
  }
  */
  return true;
}

bool ONETEP::prop_calc::recover(const bool no_err, const bool log_ok,
				char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Properties output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Properties(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    return recover_props(message, len);
  }
  return true;
}

bool ONETEP::scf_calc::recover_props(char message[], const int_g len)
{
  // Note - this must match the order of add_props
  bool spin = has_spin();
  DLV::string id = get_job_id();
  int_g count = 2;
  int nplots = get_num_homo_plots();
  DLV::string tag = "";
  nhomo = nplots;
  if (spin) {
    tag = " (up)";
    nhomo *= 2;
  }
  if (nhomo > 0)
    homo_plots = new DLV::volume_data *[nhomo];
  for (int_g i = 0; i < nplots; i++) {
    homo_plots[i] =
      cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			   id, tag, message, len);
    attach_data(homo_plots[i]);
    count++;
  }
  if (spin) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      homo_plots[nplots + i] =
	cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			     id, tag, message, len);
      attach_data(homo_plots[nplots + i]);
      count++;
    }
  }
  nplots = get_num_lumo_plots();
  nlumo = nplots;
  if (spin) {
    tag = " (up)";
    nlumo *= 2;
  }
  if (nlumo > 0)
    lumo_plots = new DLV::volume_data *[nlumo];
  for (int_g i = 0; i < nplots; i++) {
    lumo_plots[i] =
      cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			   id, tag, message, len);
    attach_data(lumo_plots[i]);
    count++;
  }
  if (spin) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      lumo_plots[nplots + i] =
	cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			     id, tag, message, len);
      attach_data(lumo_plots[nplots + i]);
      count++;
    }
  }
  if (has_dos_calc()) {
    int nproj = 1;
    if (spin)
      nproj = 2;
    DLV::electron_dos *edata =
      dos_file::read_data(this, 0, get_outfile_name(count).c_str(),
			  id, nproj, false, message, len);
    dos_plot = edata;
    //attach_data(data);
    count++;
    if (spin) {
      (void) dos_file::read_data(this, edata,
				 get_outfile_name(count).c_str(),
				 id, nproj, true, message, len);
      //attach_data(data);
      count++;
    }
    attach_data(dos_plot);
  }
  if (has_density_calc()) {
    charge_dens =
      cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			   id, "", message, len);
    attach_data(charge_dens);
    count++;
    if (spin) {
      spin_dens =
	cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			     id, "", message, len);
      attach_data(spin_dens);
      count++;
    }
    pot = cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			       id, "", message, len);
    attach_data(pot);
    count++;
  }
  // No mulliken file
  return true;
}

// Todo - this is just a copy of the scf-calc one
bool ONETEP::prop_calc::recover_props(char message[], const int_g len)
{
  // Note - this must match the order of add_props
  bool spin = has_spin();
  DLV::string id = get_job_id();
  int_g count = 0;
  int nplots = get_num_homo_plots();
  nhomo = nplots;
  DLV::string tag = "";
  if (spin) {
    tag = " (up)";
    nhomo *= 2;
  }
  if (nhomo > 0)
    homo_plots = new DLV::volume_data *[nhomo];
  for (int_g i = 0; i < nplots; i++) {
    homo_plots[i] =
      cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			   id, tag, message, len);
    attach_data(homo_plots[i]);
    count++;
  }
  if (spin) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      homo_plots[nplots + i] =
	cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			     id, tag, message, len);
      attach_data(homo_plots[nplots + i]);
      count++;
    }
  }
  nplots = get_num_lumo_plots();
  nlumo = nplots;
  if (spin) {
    tag = " (up)";
    nlumo *= 2;
  }
  if (nlumo > 0)
    lumo_plots = new DLV::volume_data *[nlumo];
  for (int_g i = 0; i < nplots; i++) {
    lumo_plots[i] = cube_file::read_data(this, 0,
					 get_outfile_name(count).c_str(),
					 id, tag, message, len);
    attach_data(lumo_plots[i]);
    count++;
  }
  if (spin) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      lumo_plots[nplots + i] =
	cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			     id, tag, message, len);
      attach_data(lumo_plots[nplots + i]);
      count++;
    }
  }
  if (has_dos_calc()) {
    int nproj = 1;
    if (spin)
      nproj = 2;
    DLV::electron_dos *edata =
      dos_file::read_data(this, 0, get_outfile_name(count).c_str(),
			  id, nproj, false, message, len);
    dos_plot = edata;
    //attach_data(data);
    count++;
    if (spin) {
      (void) dos_file::read_data(this, edata,
				 get_outfile_name(count).c_str(),
				 id, nproj, true, message, len);
      //attach_data(data);
      count++;
    }
    attach_data(dos_plot);
  }
  if (has_density_calc()) {
    charge_dens = cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
				       id, "", message, len);
    attach_data(charge_dens);
    count++;
    if (spin) {
      spin_dens = cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
				       id, "", message, len);
      attach_data(spin_dens);
      count++;
    }
    pot = cube_file::read_data(this, 0, get_outfile_name(count).c_str(),
			       id, "", message, len);
    attach_data(pot);
    count++;
  }
  // No mulliken file
  return true;
}

bool ONETEP::scf_calc::reload_data(DLV::data_object *data, char message[],
				   const int_g mlen)
{
  bool spin = has_spin();
  DLV::string id = get_job_id();
  int nplots = get_num_homo_plots();
  DLV::string tag = "";
  if (spin)
    tag = " (up)";
  bool found = false;
  int_g count = 2;
  for (int_g i = 0; i < nplots; i++) {
    if (data == homo_plots[i]) {
      (void) cube_file::read_data(this, homo_plots[i],
				  get_outfile_name(count).c_str(),
				  id, tag, message, mlen);
      found = true;
      break;
    }
    count++;
  }
  if (spin and !found) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      if (data == homo_plots[nplots + i]) {
	(void) cube_file::read_data(this, homo_plots[nplots + i],
				    get_outfile_name(count).c_str(),
				    id, tag, message, mlen);
	found = true;
	break;
      }
      count++;
    }
  }
  if (!found) {
    nplots = get_num_lumo_plots();
    if (spin)
      tag = " (up)";
    for (int_g i = 0; i < nplots; i++) {
      if (data == lumo_plots[i]) {
	(void) cube_file::read_data(this, lumo_plots[i],
				    get_outfile_name(count).c_str(),
				    id, tag, message, mlen);
	found = true;
	break;
      }
      count++;
    }
    if (spin and !found) {
      tag = " (down)";
      for (int_g i = 0; i < nplots; i++) {
	if (data == lumo_plots[nplots + i]) {
	  (void) cube_file::read_data(this, lumo_plots[nplots + i],
				      get_outfile_name(count).c_str(),
				      id, tag, message, mlen);
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found and has_dos_calc()) {
    int nproj = 1;
    if (spin)
      nproj = 2;
    if (data == dos_plot) {
      DLV::electron_dos *edata = dynamic_cast<DLV::electron_dos *>(data);
      dos_file::read_data(this, edata, get_outfile_name(count).c_str(),
			  id, nproj, false, message, mlen);
      count++;
      if (spin) {
	(void) dos_file::read_data(this, edata,
				   get_outfile_name(count).c_str(),
				   id, nproj, true, message, mlen);
	count++;
      }
      found = true;
    } else {
      count++;
      if (spin)
	count++;
    }
  }
  if (!found and has_density_calc()) {
    if (data == charge_dens) {
      (void) cube_file::read_data(this, charge_dens,
				  get_outfile_name(count).c_str(),
				  id, "", message, mlen);
      found = true;
    }
    count++;
    if (!found and spin) {
      if (data == spin_dens) {
	(void) cube_file::read_data(this, spin_dens,
				    get_outfile_name(count).c_str(),
				    id, "", message, mlen);
	found = true;
      }
      count++;
    }
    if (!found) {
      (void) cube_file::read_data(this, pot, get_outfile_name(count).c_str(),
				  id, "", message, mlen);
      found = true;

    }
    count++;
  }
  // No mulliken file
  return found;
}

bool ONETEP::opt_calc::reload_data(DLV::data_object *data, char message[],
				   const int_g mlen)
{
  // nothing Todo?
  return true;
}

bool ONETEP::prop_calc::reload_data(DLV::data_object *data, char message[],
				   const int_g mlen)
{
  // Note - this must match the order of add_props / recover
  bool spin = has_spin();
  DLV::string id = get_job_id();
  int nplots = get_num_homo_plots();
  DLV::string tag = "";
  if (spin)
    tag = " (up)";
  bool found = false;
  int_g count = 0;
  for (int_g i = 0; i < nplots; i++) {
    if (data == homo_plots[i]) {
      (void) cube_file::read_data(this, homo_plots[i],
				  get_outfile_name(count).c_str(),
				  id, tag, message, mlen);
      found = true;
      break;
    }
    count++;
  }
  if (spin and !found) {
    tag = " (down)";
    for (int_g i = 0; i < nplots; i++) {
      if (data == homo_plots[nplots + i]) {
	(void) cube_file::read_data(this, homo_plots[nplots + i],
				    get_outfile_name(count).c_str(),
				    id, tag, message, mlen);
	found = true;
	break;
      }
      count++;
    }
  }
  if (!found) {
    nplots = get_num_lumo_plots();
    if (spin)
      tag = " (up)";
    for (int_g i = 0; i < nplots; i++) {
      if (data == lumo_plots[i]) {
	(void) cube_file::read_data(this, lumo_plots[i],
				    get_outfile_name(count).c_str(),
				    id, tag, message, mlen);
	found = true;
	break;
      }
      count++;
    }
    if (spin and !found) {
      tag = " (down)";
      for (int_g i = 0; i < nplots; i++) {
	if (data == lumo_plots[nplots + i]) {
	  (void) cube_file::read_data(this, lumo_plots[nplots + i],
				      get_outfile_name(count).c_str(),
				      id, tag, message, mlen);
	  found = true;
	  break;
	}
	count++;
      }
    }
  }
  if (!found and has_dos_calc()) {
    int nproj = 1;
    if (spin)
      nproj = 2;
    if (data == dos_plot) {
      DLV::electron_dos *edata = dynamic_cast<DLV::electron_dos *>(data);
      dos_file::read_data(this, edata, get_outfile_name(count).c_str(),
			  id, nproj, false, message, mlen);
      count++;
      if (spin) {
	(void) dos_file::read_data(this, edata,
				   get_outfile_name(count).c_str(),
				   id, nproj, true, message, mlen);
	count++;
      }
      found = true;
    } else {
      count++;
      if (spin)
	count++;
    }
  }
  if (!found and has_density_calc()) {
    if (data == charge_dens) {
      (void) cube_file::read_data(this, charge_dens,
				  get_outfile_name(count).c_str(),
				  id, "", message, mlen);
      found = true;
    }
    count++;
    if (!found and spin) {
      if (data == spin_dens) {
	(void) cube_file::read_data(this, spin_dens,
				    get_outfile_name(count).c_str(),
				    id, "", message, mlen);
	found = true;
      }
      count++;
    }
    if (!found) {
      (void) cube_file::read_data(this, pot, get_outfile_name(count).c_str(),
				  id, "", message, mlen);
      found = true;

    }
    count++;
  }
  // No mulliken file
  return found;
}

bool ONETEP::scf_geom::is_geometry() const
{
  return true;
}

void ONETEP::scf_geom::inherit_model()
{
  // Null op, we have a new structure
}

void ONETEP::scf_geom::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}


DLV::model *ONETEP::scf_geom::read_geom(const DLV::string name,
					const char filename[], char message[],
					const int_g mlen)
{
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      structure = DLV::model::create_atoms(name, 3);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	int_g natoms = get_model()->get_number_of_primitive_atoms();
	int_g *atom_types = new_local_array1(int_g, natoms);
	DLV::coord_type (*coords)[3] =
	  new_local_array2(DLV::coord_type, natoms, 3);
	char line[256], text[256];
	int index;
	bool first = true;
	real_l a[3];
	real_l b[3];
	real_l c[3];
	real_l energy;
	while (!input.eof()) {
	  input >> index;
	  if (!first and index == 0)
	    break;
	  first = false;
	  input >> energy;
	  input.getline(line, 256);
	  input >> a[0];
	  input >> a[1];
	  input >> a[2];
	  input.getline(line, 256);
	  input >> b[0];
	  input >> b[1];
	  input >> b[2];
	  input.getline(line, 256);
	  input >> c[0];
	  input >> c[1];
	  input >> c[2];
	  input.getline(line, 256);
	  for (int i = 0; i < natoms; i++) {
	    int dummy;
	    input.getline(line, 256);
	    if (sscanf(line, "%s %d %lf %lf %lf", text, &dummy, &coords[i][0],
		       &coords[i][1], &coords[i][2]) != 5) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      ok = DLV_ERROR;
	    } else {
	      atom_types[i] = DLV::atomic_prefs::lookup_symbol(text);
	    }
	  }
	  // forces
	  for (int i = 0; i < natoms; i++)
	    input.getline(line, 256);
	  // blank line
	  input.getline(line, 256);
	}
	// set lattice etc for last geometry read
	a[0] *= DLV::bohr_to_angstrom;
	a[1] *= DLV::bohr_to_angstrom;
	a[2] *= DLV::bohr_to_angstrom;
	b[0] *= DLV::bohr_to_angstrom;
	b[1] *= DLV::bohr_to_angstrom;
	b[2] *= DLV::bohr_to_angstrom;
	c[0] *= DLV::bohr_to_angstrom;
	c[1] *= DLV::bohr_to_angstrom;
	c[2] *= DLV::bohr_to_angstrom;
	if (!structure->set_primitive_lattice(a, b, c)) {
	  strncpy(message, "Error setting lattice vectors",
		  mlen - 1);
	  ok = DLV_ERROR;
	} else {
	  for (int i = 0; i < natoms; i++) {
	    coords[i][0] *= DLV::bohr_to_angstrom;
	    coords[i][1] *= DLV::bohr_to_angstrom;
	    coords[i][2] *= DLV::bohr_to_angstrom;
	    DLV::atom my_atom;
	    my_atom = structure->add_cartesian_atom(atom_types[i], coords[i]);
	    if (my_atom == 0) {
	      strncpy(message, "Error adding atom to model", mlen - 1);
	      ok = DLV_ERROR;
	      break;
	    }
	  }
	}
	if (ok != DLV_OK) {
	  delete structure;
	  structure = 0;
	} else
	  structure->complete();
	delete_local_array(coords);
	delete_local_array(atom_types);
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void ONETEP::Tolerances::serialize(Archive &ar, const unsigned int version)
{
  ar & energy_cutoff;
  ar & kernel_cutoff;
  ar & fine_grid;
}

template <class Archive>
void ONETEP::Hamiltonian::serialize(Archive &ar, const unsigned int version)
{
  ar & unrestricted;
  ar & functional;
  ar & dispersion;
  ar & set_spin;
  ar & spin;
}

template <class Archive>
void ONETEP::Properties::serialize(Archive &ar, const unsigned int version)
{
  ar & num_homo_plots;
  ar & num_lumo_plots;
  ar & num_eigenvalues;
  ar & dos_calc;
  ar & dos_smearing;
  ar & calc_density;
  ar & mulliken_calc;
  ar & mulliken_radius;
}

template <class Archive>
void ONETEP::Optimise::serialize(Archive &ar, const unsigned int version)
{
  ar & method;
  ar & max_iterations;
  ar & energy_tol;
  ar & force_tol;
  ar & displacement_tol;
}

template <class Archive>
void ONETEP::pseudo_info::serialize(Archive &ar, const unsigned int version)
{
  ar & name;
  ar & index;
  ar & n_functions;
  ar & radius;
}

template <class Archive>
void ONETEP::scf_data::serialize(Archive &ar, const unsigned int version)
{
  // Todo basis?
  ar & tolerances;
  ar & hamiltonian;
}

template <class Archive>
void ONETEP::prop_data::serialize(Archive &ar, const unsigned int version)
{
  ar & properties;
}

template <class Archive>
void ONETEP::opt_data::serialize(Archive &ar, const unsigned int version)
{
  ar & optimise;
}

template <class Archive>
void ONETEP::scf_base::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::batch_calc>(*this);
  ar & show_basis;
}

template <class Archive>
void ONETEP::scf_geom::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scf_base>(*this);
}

template <class Archive>
void ONETEP::scf_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<scf_base>(*this);
  ar & boost::serialization::base_object<scf_data>(*this);
  ar & boost::serialization::base_object<prop_data>(*this);
  ar & nhomo;
  ar & nlumo;
  for (int i = 0; i < nhomo; i++)
    ar & homo_plots[i];
  for (int i = 0; i < nlumo; i++)
    ar & lumo_plots[i];
  ar & dos_plot;
  ar & charge_dens;
  ar & spin_dens;
  ar & pot;
}

template <class Archive>
void ONETEP::scf_calc::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scf_base>(*this);
  ar & boost::serialization::base_object<scf_data>(*this);
  ar & boost::serialization::base_object<prop_data>(*this);
  ar & nhomo;
  ar & nlumo;
  homo_plots = new DLV::volume_data *[nhomo];
  for (int i = 0; i < nhomo; i++)
    ar & homo_plots[i];
  lumo_plots = new DLV::volume_data *[nlumo];
  for (int i = 0; i < nlumo; i++)
    ar & lumo_plots[i];
  ar & dos_plot;
  ar & charge_dens;
  ar & spin_dens;
  ar & pot;
}

template <class Archive>
void ONETEP::prop_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<scf_base>(*this);
  ar & boost::serialization::base_object<prop_data>(*this);
  ar & scf;
  ar & nhomo;
  ar & nlumo;
  for (int i = 0; i < nhomo; i++)
    ar & homo_plots[i];
  for (int i = 0; i < nlumo; i++)
    ar & lumo_plots[i];
  ar & dos_plot;
  ar & charge_dens;
  ar & spin_dens;
  ar & pot;
}

template <class Archive>
void ONETEP::prop_calc::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scf_base>(*this);
  ar & boost::serialization::base_object<prop_data>(*this);
  ar & scf;
  ar & nhomo;
  ar & nlumo;
  homo_plots = new DLV::volume_data *[nhomo];
  for (int i = 0; i < nhomo; i++)
    ar & homo_plots[i];
  lumo_plots = new DLV::volume_data *[nlumo];
  for (int i = 0; i < nlumo; i++)
    ar & lumo_plots[i];
  ar & dos_plot;
  ar & charge_dens;
  ar & spin_dens;
  ar & pot;
}

template <class Archive>
void ONETEP::opt_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scf_geom>(*this);
  ar & boost::serialization::base_object<opt_data>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::scf_base)
BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::scf_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::prop_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::opt_calc)

DLV_SUPPRESS_TEMPLATES(DLV::batch_calc)
DLV_SUPPRESS_TEMPLATES(DLV::dos_plot)

DLV_NORMAL_EXPLICIT(ONETEP::scf_base)
DLV_NORMAL_EXPLICIT(ONETEP::scf_calc)
DLV_NORMAL_EXPLICIT(ONETEP::prop_calc)
DLV_NORMAL_EXPLICIT(ONETEP::opt_calc)

#endif // DLV_USES_SERIALIZE
