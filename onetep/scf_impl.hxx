
#ifndef ONETEP_SCF_IMPL
#define ONETEP_SCF_IMPL

namespace ONETEP {

  enum task_type {
    single_point = 0, optimisation_calc = 1, molecular_dynamics = 2,
    properties = 3, transition_state = 4
  };
		   
  const DLV::string program_name = "ONETEP";
  const DLV::string dkernel_label = "Density Kernel";
  const DLV::string ngwfs_label = "NGWFS from SCF";

  class pseudo_info {
  public:
    pseudo_info(const int_g n, const real_g r, const DLV::string file);

    DLV::string name;
    int_g index;
    int_g n_functions;
    real_g radius;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_data : public cell_file {
  public:
    scf_data();
    bool add_pseudo(const int_g nfns, const real_g radius,
		    const char filename[], const bool set_default,
		    const DLV::model *const m, int_g * &indices, int_g &value,
		    char message[], const int_g mlen);
    bool check_pseudos(const DLV::model *const m) const;
    bool has_spin() const;
    virtual ~scf_data();

  protected:
    bool setup_species(const DLV::model *const structure);
    void set_params(const Tolerances &tol, const Hamiltonian &ham);
    bool write_input(const task_type task, const DLV::string filename,
		     const DLV::model *const structure,
		     char message[], const int_g mlen);
    bool write_pseudos(std::ofstream &output, const DLV::model *const structure,
		       char message[], const int_g mlen) const;
    bool write_scf(std::ofstream &output, const DLV::model *const structure,
		   char message[], const int_g mlen);
    bool write_controls(std::ofstream &output, char message[],
			const int_g mlen) const;
    virtual void write_task(std::ofstream &output) const;
    virtual bool write_props(std::ofstream &output, char message[],
			     const int_g mlen) const;

  private:
    std::list<pseudo_info> atom_bases;
    int_g *basis_indices;
    int_g *species;
    int_g natoms;
    Tolerances tolerances;
    Hamiltonian hamiltonian;

    void write_tols(std::ofstream &output) const;
    void write_hamiltonian(std::ofstream &output, const bool is_scf) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class prop_data : public dos_file, public DLV::cube_file {
  public:
    prop_data();

  protected:
    void set_params(const Properties &props);
    bool write_input(std::ofstream &output, const bool is_scf,
		     char message[], const int_g mlen) const;
    const Properties &get_props() const;
    int_g get_num_homo_plots() const;
    int_g get_num_lumo_plots() const;
    bool has_dos_calc() const;
    bool has_density_calc() const;
    bool has_mulliken_calc() const;
    DLV::string get_label(std::ifstream &input, const DLV::string tag,
			  const DLV::string id);
    DLV::volume_data *create_data(const DLV::string label, DLV::operation *op,
				  const int_g nx, const int_g ny,
				  const int_g nz, const real_g origin[3],
				  const real_g astep[3], const real_g bstep[3],
				  const real_g cstep[3]);

  private:
    Properties properties;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class opt_data : public scf_data {
  protected:
    void set_params(const Optimise &opt);
    void write_task(std::ofstream &output) const;

  private:
    Optimise optimise;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_geom : public scf_base {
  public:
    bool is_geometry() const;

  protected:
    void inherit_model();
    void inherit_data();
    DLV::model *read_geom(const DLV::string name, const char filename[],
			  char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc : public scf_base, public scf_data, public prop_data {
  public:
    scf_calc();

    bool write_scf_data(std::ofstream &output,
			const DLV::model *const structure,
			char message[], const int_g mlen);
    void get_scf_files(DLV::string &dkn, DLV::string &ngwfs) const;

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    DLV::string get_base_name() const;
    void set_params(const Tolerances &tol, const Hamiltonian &h,
		    const Properties &p, const Optimise &opt);
    bool add_pseudo_pot(const int_g nfns, const real_g radius,
			const char filename[], const bool set_default,
			int_g * &indices, int_g &value,
			char message[], const int_g mlen);
    bool check_pseudos() const;
    bool find_restart(char message[], const int_g mlen);
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool add_restart(const bool is_local);
    bool add_output(const char tag[], const bool is_local,
		    char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    bool write_props(std::ofstream &output, char message[],
		     const int_g mlen) const;

  private:
    int_g nhomo;
    int_g nlumo;
    DLV::volume_data **homo_plots;
    DLV::volume_data **lumo_plots;
    DLV::dos_plot *dos_plot;
    DLV::volume_data *charge_dens;
    DLV::volume_data *spin_dens;
    DLV::volume_data *pot;
    
    bool recover_props(char message[], const int_g len);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class prop_calc : public scf_base, public prop_data {
  public:
    prop_calc();

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    DLV::string get_base_name() const;
    void set_params(const Tolerances &tol, const Hamiltonian &ham,
		    const Properties &p, const Optimise &opt);
    bool add_pseudo_pot(const int_g nfns, const real_g radius,
			const char filename[], const bool set_default,
			int_g * &indices, int_g &value,
			char message[], const int_g mlen);
    bool check_pseudos() const;
    bool find_restart(char message[], const int_g mlen);
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool add_restart(const bool is_local);
    bool add_output(const char tag[], const bool is_local,
		    char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    bool write_props(std::ofstream &output, char message[],
		     const int_g mlen) const;
    bool has_spin() const;

  private:
    scf_calc *scf;
    int_g nhomo;
    int_g nlumo;
    DLV::volume_data **homo_plots;
    DLV::volume_data **lumo_plots;
    DLV::dos_plot *dos_plot;
    DLV::volume_data *charge_dens;
    DLV::volume_data *spin_dens;
    DLV::volume_data *pot;

    bool recover_props(char message[], const int_g len);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc : public scf_geom, public opt_data {
  public:
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    DLV::string get_base_name() const;
    void set_params(const Tolerances &tol, const Hamiltonian &h,
		    const Properties &p, const Optimise &opt);
    bool add_pseudo_pot(const int_g nfns, const real_g radius,
			const char filename[], const bool set_default,
			int_g * &indices, int_g &value,
			char message[], const int_g mlen);
    bool check_pseudos() const;
    bool find_restart(char message[], const int_g mlen);
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool add_restart(const bool is_local);
    bool add_output(const char tag[], const bool is_local,
		    char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(ONETEP::scf_geom)
BOOST_CLASS_EXPORT_KEY(ONETEP::scf_calc)
BOOST_CLASS_EXPORT_KEY(ONETEP::prop_calc)
BOOST_CLASS_EXPORT_KEY(ONETEP::opt_calc)
#endif // DLV_USES_SERIALIZE

inline ONETEP::pseudo_info::pseudo_info(const int_g n, const real_g r,
					const DLV::string file)
  : name(file), index(-1), n_functions(n), radius(r)
{
}

inline ONETEP::scf_data::scf_data()
  : basis_indices(0), species(0), natoms(0)
{
}

inline bool ONETEP::scf_data::has_spin() const
{
  return hamiltonian.unrestricted;
}

inline ONETEP::prop_data::prop_data()
{
}

inline const ONETEP::Properties &ONETEP::prop_data::get_props() const
{
  return properties;
}

inline ONETEP::int_g ONETEP::prop_data::get_num_homo_plots() const
{
  return properties.num_homo_plots;
}

inline ONETEP::int_g ONETEP::prop_data::get_num_lumo_plots() const
{
  return properties.num_lumo_plots;
}

inline bool ONETEP::prop_data::has_dos_calc() const
{
  return properties.dos_calc;
}

inline bool ONETEP::prop_data::has_density_calc() const
{
  return properties.calc_density;
}

inline bool ONETEP::prop_data::has_mulliken_calc() const
{
  return properties.mulliken_calc;
}

inline ONETEP::scf_calc::scf_calc()
  : nhomo(0), nlumo(0), homo_plots(0), lumo_plots(0), dos_plot(0),
    charge_dens(0), spin_dens(0), pot(0)
{
}

inline ONETEP::prop_calc::prop_calc()
  : scf(0), nhomo(0), nlumo(0), homo_plots(0), lumo_plots(0), dos_plot(0),
    charge_dens(0), spin_dens(0), pot(0)
{
}

inline bool ONETEP::prop_calc::has_spin() const
{
  return scf->has_spin();
}

#endif // ONETEP_SCF_IMPL
