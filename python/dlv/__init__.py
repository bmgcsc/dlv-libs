import os
atom_prefs_file = os.getenv("DLV_ATOM_PREFS")
import dlv.main.base
dlv.main.base.initialise(atom_prefs_file)
