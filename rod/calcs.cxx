#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "data.hxx"
#include "plot.hxx"
#include "fit.hxx"
#ifdef ENABLE_ROD
#include "rod/rod.h"

ROD::rod_calc::rod_calc(DLV::model *m,
			const real_g att, const real_g sc, const real_g sc2,
			const real_g be, const real_g sfr,
			real_g dst[], real_g d1[], real_g d2[], real_g occ[],
			const int_g ndistt, const int_g ndwt,
			const int_g ndwt2, const int_g nocct,
			const bool fit_d, const bool exp_d)
  : DLV::change_geometry(m), ndistot(ndistt),  ndwtot(ndwt),
    ndwtot2(ndwt2),  nocctot(nocct),
    atten(att), scale(sc), scale2(sc2), beta(be), sfrac(sfr),
    fit_data(fit_d), exp_data(exp_d)
{
  dist = new real_g[ndistot];
  for(int_g i=0;i<ndistot;i++)
    dist[i] = dst[i];
  dw1 = new real_g[ndwtot];
  for(int_g i=0;i<ndwtot;i++)
    dw1[i] = d1[i];
  dw2 = new real_g[ndwtot2];
  for(int_g i=0;i<ndwtot2;i++)
    dw2[i] = d2[i];
  occup = new real_g[nocctot];
  for(int_g i=0;i<nocctot;i++)
    occup[i] = occ[i];
}

ROD::int_g ROD::rod_calc::get_rod_data(real_g &scale, real_g &scale_min,
				real_g &scale_max, bool &scale_fit,
				real_g &scale2, real_g &scale2_min,
				real_g &scale2_max, bool &scale2_fit,
				real_g &beta, real_g &beta_min,
				real_g &beta_max, bool &beta_fit,
				real_g &sfrac, real_g &sfrac_min,
				real_g &sfrac_max, bool &sfrac_fit,
				int_g &ndisttot, int_g &ndwtot,
				int_g &ndwtot2, int_g &nocctot,
				int_g &maxpar,
				bool got_par_file,
				bool &show_fit_results,
				char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  DLV::operation *base = DLV::operation::get_current();
  ROD::rod_calc *op = dynamic_cast<rod_calc*> (base);
  if (op !=0 || got_par_file) {
    if(op!=0)
      ok = op->initialise_data(message, mlen); //necc?
    else{
      ndisttot = NDISTOT;
      ndwtot = NDWTOT;
      ndwtot2 = NDWTOT2;
      nocctot = NOCCTOT;
      maxpar = MAXPAR;
    }
    scale = SCALE;
    scale_min = SCALELIM[0];
    scale_max = SCALELIM[1];
    scale_fit = (FIXPAR[0]==0 ? true : false);
    scale2 = SCALE2;
    scale2_min = SCALE2LIM[0];
    scale2_max = SCALE2LIM[1];
    scale2_fit = (FIXPAR[1]==0 ? true : false);
    beta = BETA;
    beta_min = BETALIM[0];
    beta_max = BETALIM[1];
    beta_fit = (FIXPAR[2]==0 ? true : false);
    sfrac = SURFFRAC;
    sfrac_min = SURFFRACLIM[0];
    sfrac_max = SURFFRACLIM[1];
    sfrac_fit = (FIXPAR[3]==0 ? true : false);
    show_fit_results = true;
  }
  else{
    // set to default values below
    scale = 1.0;
    scale_min = 0.5;
    scale_max = 2.0;
    scale_fit = true;
    scale2 = 1.0;
    scale2_min = 0.5;
    scale2_max = 2.0;
    scale2_fit = false;
    beta = 0.0;
    beta_min = 0.0;
    beta_max = 1.0;
    beta_fit = true;
    sfrac = 1.0;
    sfrac_min = 0.25;
    sfrac_max = 1.0;
    sfrac_fit = true;
    show_fit_results = false;
    //initalise_model
    ok = ROD::data::initialise_model(message, mlen); //get NDWTOT etc
    ndisttot = NDISTOT;
    ndwtot = NDWTOT;
    ndwtot2 = NDWTOT2;
    nocctot = NOCCTOT;
    maxpar = MAXPAR;
    for (int_g i = 0; i< NDISTOT; i++)
      DISPL[i] = 0.0;
    for (int_g i = 0; i< NDWTOT; i++)
      DEBWAL[i] = 0.0;
    for (int_g i = 0; i< NDWTOT2; i++)
      DEBWAL2[i] = 0.0;
    for (int_g i = 0; i< NOCCTOT; i++)
      OCCUP[i] = 1.0;
  }
  return ok;
}

ROD::int_g ROD::rod_calc::get_rod_array_data(real_g dist[], real_g dist_min[],
				     real_g dist_max[], int_g dist_fit[],
				     real_g dw1[], real_g dw1_min[],
				     real_g dw1_max[], int_g dw1_fit[],
				     real_g dw2[], real_g dw2_min[],
				     real_g dw2_max[], int_g dw2_fit[],
				     real_g occup[], real_g occup_min[],
				     real_g occup_max[], int_g occup_fit[],
				     int_g ndisttot, int_g ndwtot,
				     int_g ndwtot2, int_g nocctot,
				      bool got_par_data,
				     char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  //maybe update to find last instance of rod_fit...
  DLV::operation *base = DLV::operation::get_current();
  ROD::rod_calc *op = dynamic_cast<rod_calc*> (base);
 if (op !=0 || got_par_data) {
   //Assumes get_rod_data was previously run to initialise_data....
   for (int_g i = 0; i<ndisttot; i++){
      dist[i] = DISPL[i];
      dist_min[i] = DISPLLIM[i][0];
      dist_max[i] = DISPLLIM[i][1];
      dist_fit[i] = (FIXPAR[NSF+i]==0 ? 1 : 0);
    }
    for (int_g i = 0; i<ndwtot; i++){
      dw1[i] = DEBWAL[i];
      dw1_min[i] = DEBWALLIM[i][0];
      dw1_max[i] = DEBWALLIM[i][1];
      dw1_fit[i] = (FIXPAR[NSF+NDISTOT+i]==0 ? 1 : 0);
    }
    for (int_g i = 0; i<ndwtot2; i++){
      dw2[i] = DEBWAL2[i];
      dw2_min[i] = DEBWAL2LIM[i][0];
      dw2_max[i] = DEBWAL2LIM[i][1];
      dw2_fit[i] = (FIXPAR[NSF+NDISTOT+NDWTOT+i]==0 ? 1 : 0);
    }
    for (int_g i = 0; i<nocctot; i++){
      occup[i] = OCCUP[i];
      occup_min[i] = OCCUPLIM[i][0];
      occup_max[i] = OCCUPLIM[i][1];
      occup_fit[i] = (FIXPAR[NSF+NDISTOT+NDWTOT+NDWTOT2+i]==0 ? 1 : 0);
    }
  }
  else{
    // set to default values below
    for (int_g i=0; i<ndisttot; i++){
      dist[i] = 0.0;
      dist_min[i] = -1.0;
      dist_max[i] = 1.0;
      dist_fit[i] = 1;
    }
    for (int_g i=0; i<ndwtot; i++){
      dw1[i] = 0.0;
      dw1_min[i] = 0.0;
      dw1_max[i] = 1.0;
      dw1_fit[i] = 1;
    }
    for (int_g i=0; i<ndwtot2; i++){
      dw2[i] = 0.0;
      dw2_min[i] = 0.0;
      dw2_max[i] = 1.0;
      dw2_fit[i] = 1;
    }
    for (int_g i=0; i<nocctot; i++){
      occup[i] = 0.0;
      occup_min[i] = 0.0;
      occup_max[i] = 1.0;
      occup_fit[i] = 1;
    }
  }
  return ok;
}




ROD::int_g ROD::rod_calc::initialise_data(char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  ok = ROD::data::initialise_model(message, mlen);
  if (ok != DLV_OK)
    return ok;
  STRUCFAC = true;
  ROUGHMODEL = APPROXBETA;
  NSURF2 = 0;
  SURFFRAC = sfrac;
  SURF2FRAC = 0;
  NLAYERS = 1;
  NDOMAIN = 1;
  SCALE = scale;  //default 1
  SCALE2 = scale2;
  BETA = beta;   /* Roughness parameter beta */
  ATTEN = atten;    //default = 0.001;
  COHERENTDOMAINS = false;
  KEAT_PLUS_CHI = false;
  POTENTIAL = VKEATING;
  if(!exp_data) {
    LBRAGG = 0;
  }
  else{
    LBRAGG = LBR[0];
  }
  for (int_g i = 0; i < MAXDOM; i++)
    {
      DOMMAT11[i] = 1.;
      DOMMAT12[i] = 0.;
      DOMMAT21[i] = 0.;
      DOMMAT22[i] = 1.;
    }
  DOMOCCUP[0] = 1.;
  ZEROFRACT = true;
  DOMEQUAL = true;
  for (int_g i = 0; i < MAXTYPES; i++)
    ATRAD[i] = 0.1;
  FSIZE = 0.05;

  NDWTOT  = 0;
  NDWTOT2 = 0;
  NOCCTOT = 0;
  NDISTOT = 0;
  for (int_g i = 0; i < NBULK; i++)
    if (NDWB[i] > NDWTOT)
      NDWTOT = NDWB[i];
  for (int_g i = 0; i < NSURF; i++) {
    if (NDWS[i] > NDWTOT)
      NDWTOT = NDWS[i];
    if (NDWS2[i] > NDWTOT2)
      NDWTOT2 = NDWS2[i];
    if (NOCCUP[i] > NOCCTOT)
      NOCCTOT = NOCCUP[i];
    if (NXDIS[i] > NDISTOT)
      NDISTOT = NXDIS[i];
    if (NX2DIS[i] > NDISTOT)
      NDISTOT = NX2DIS[i];
    if (NYDIS[i] > NDISTOT)
      NDISTOT = NYDIS[i];
    if (NY2DIS[i] > NDISTOT)
      NDISTOT = NY2DIS[i];
    if (NZDIS[i] > NDISTOT)
      NDISTOT = NZDIS[i];
  }
  if (NDWTOT > MAXPAR) {
    NDWTOT = MAXPAR;
    strncpy(message, "Error, too many  parallel Debye-Waller parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NDWTOT2 > MAXPAR) {
    NDWTOT2 = MAXPAR;
    strncpy(message, "Error, too many  perpendicular Debye-Waller parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NOCCTOT > MAXPAR) {
    NOCCTOT = MAXPAR;
    strncpy(message, "Error, too many occupancy parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NDISTOT > MAXPAR) {
    NDISTOT = MAXPAR;
    strncpy(message, "Error, too many displacement parameters ", mlen - 1);
    ok = DLV_ERROR;
  }

  //BUT ONLYJUST GT DIMs
  // think in future will already have them at this point
  // either set to maxpar etc or have a second call to init arrays
  // maybe call something to set array sizes when panel is first opened
  // (as opossed to fit panel first opened





  return ok;
}

DLV::operation *
ROD::rod_calc::run_fit(const int_g fit_method, const real_g atten, real_g scale,
		       const real_g scale_min, const real_g scale_max,
		       const bool scale_fit, real_g scale2,
		       const real_g scale2_min, const real_g scale2_max,
		       const bool scale2_fit, real_g beta,
		       const real_g beta_min, const real_g beta_max,
		       const bool beta_fit, real_g sfrac,
		       const real_g sfrac_min, const real_g sfrac_max,
		       const bool sfrac_fit, real_g dist[],
		       const real_g dist_min[], const real_g dist_max[],
		       const int_g dist_fit[], const int_g ndistot,
		       real_g dw1[], const real_g dw1_min[],
		       const real_g dw1_max[], const int_g dw1_fit[],
		       const int_g ndwtot, real_g dw2[], const real_g dw2_min[],
		       const real_g dw2_max[], const int_g dw2_fit[],
		       const int_g ndwtot2, real_g occup[],
		       const real_g occup_min[], const real_g occup_max[],
		       const int_g occup_fit[], const int_g nocctot,
		       real_g fit_results[][2], real_g *chisqr, real_g *norm,
		       real_g *quality, const bool fit_data,
		       const bool exp_data, char message[], const int_g mlen,
		       const bool attach_op)
{
  int_g ok = DLV_OK;

  //should add dw etc to model at this stage
  DLV::model *m = DLV::operation::get_current_model();
  ROD::rod_fit *op = new ROD::rod_fit(m, atten, scale, scale2,
				      beta, sfrac, fit_data, exp_data,
				      scale_min, scale_max, scale_fit,
				      scale2_min, scale2_max, scale2_fit,
				      beta_min, beta_max, beta_fit,
				      sfrac_min, sfrac_max, sfrac_fit, dist,
				      dist_min, dist_max, dist_fit, ndistot,
				      dw1, dw1_min, dw1_max, dw1_fit, ndwtot,
				      dw2, dw2_min, dw2_max, dw2_fit, ndwtot2,
				      occup, occup_min, occup_max, occup_fit,
				      nocctot, chisqr, norm, quality);
  if(attach_op)
    op->attach_no_inherit_model();
  else
    op->attach_no_current();
  ok = op->initialise_data(message, mlen);
  if (ok !=DLV_OK)
    return 0;
  return (op->run_fit(fit_method, fit_results, message, mlen, attach_op));
}

DLV::operation *
ROD::rod_calc::plot_rod(DLV::model *m, const real_g atten,
			const real_g scale, const real_g scale2,
			const real_g beta, const real_g sfrac, real_g dist[],
			real_g dw1[], real_g dw2[], real_g occup[],
			const int_g ndistot, const int_g ndwtot,
			const int_g ndwtot2, const int_g nocctot,
			const real_g hrod, const real_g krod,
			const real_g lstart, const real_g lend,
			const int_g nl, const bool plot_bulk,
			const bool plot_surf, const bool plot_both,
			const bool plot_exp, const bool fit_data,
			const bool exp_data, const bool new_view,
			char message[], const int_g mlen, const bool attach_op)
{
  int_g ok = DLV_OK;
  DLV::model *m1 = DLV::model::create_atoms(m->get_model_name(),
					    m->get_model_type());
  m1->copy_model(m);
  ROD::rod_plot *op = new ROD::rod_plot(m1, atten, scale, scale2,
					beta, sfrac,
					dist, dw1, dw2, occup,
					ndistot, ndwtot, ndwtot2, nocctot,
					fit_data, exp_data,
					hrod, krod, lstart, lend, nl,
					plot_bulk, plot_surf,
					plot_both, plot_exp,
					new_view);
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate ROD::rod_plot operation",
	      mlen);
  }
  else {
    if(attach_op)
      op->attach_no_inherit_model();
    else
      op->attach_no_current();
  }
  ok = op->initialise_data(message, mlen);
  if (ok !=DLV_OK)
    return 0;
  ok =  op->plot_rod(message, mlen);
  if (ok !=DLV_OK)
    return 0;
  save_parent(op);
  return op;
}

DLV::operation *
ROD::rod_calc::plot_ffactors(DLV::model *m, const real_g atten,
			     const real_g scale, const real_g scale2,
			     const real_g beta, const real_g sfrac,
			     real_g dist[], real_g dw1[], real_g dw2[],
			     real_g occup[], const int_g ndistot,
			     const int_g ndwtot, const int_g ndwtot2,
			     const int_g nocctot, const int_g select_fs,
			     real_g h_start, real_g k_start, const real_g h_end,
			     const real_g k_end, const real_g h_step,
			     const real_g k_step, const real_g l,
			     const real_g maxq, const bool plot_calc,
			     const bool plot_exp, const bool fit_data,
			     const bool exp_data, const bool new_view,
			     char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  if(plot_exp && select_fs !=2){
    ok = DLV_ERROR;
    strncpy(message,
	    "Can only plot experimental data for plot for all exp points ",
	    mlen - 1);
    return 0;
  }
  DLV::model *m1 = DLV::model::create_atoms(m->get_model_name(),
					    m->get_model_type());
  m1->copy_model(m);
  real_g scale_factor = -1.0; //i.e. set later
  ROD::rod_ffac *op = new ROD::rod_ffac(m1, atten, scale, scale2,
					beta, sfrac,
					dist, dw1, dw2, occup,
					ndistot, ndwtot, ndwtot2, nocctot,
					fit_data, exp_data,
					select_fs, h_start, k_start, h_end,
					k_end, h_step, k_step, l, maxq,
					scale_factor,
					plot_calc, plot_exp, new_view);
  op->attach();
  ok = op->initialise_data(message, mlen);
  if (ok !=DLV_OK)
    return 0;
  op->plot_ffactors(message, mlen);
  if (ok !=DLV_OK)
    return 0;
  save_parent(op);
  return op;
}

void ROD::structure_file::calc_rlat(real_g dlat[6], real_g rlat[6])
{
  // copied from rod src
  real_l volume;
  volume = dlat[0]*dlat[1]*dlat[2]*
    sqrt(1+2*cos(dlat[3])*cos(dlat[4])*cos(dlat[5])
	 -cos(dlat[3])*cos(dlat[3])
	 -cos(dlat[4])*cos(dlat[4])
	 -cos(dlat[5])*cos(dlat[5]));
  rlat[0] = dlat[1]*dlat[2]*sin(dlat[3])/volume;
  rlat[1] = dlat[0]*dlat[2]*sin(dlat[4])/volume;
  rlat[2] = dlat[0]*dlat[1]*sin(dlat[5])/volume;
  rlat[3] = acos((cos(dlat[4])*cos(dlat[5])-cos(dlat[3]))
		 /(sin(dlat[4])*sin(dlat[5])));
  rlat[4] = acos((cos(dlat[3])*cos(dlat[5])-cos(dlat[4]))
		 /(sin(dlat[3])*sin(dlat[5])));
  rlat[5] = acos((cos(dlat[3])*cos(dlat[4])-cos(dlat[5]))
		 /(sin(dlat[3])*sin(dlat[4])));
}

DLV::model *ROD::structure_file::read(const DLV::string name,
				      const char filename[],
				      const char filename2[],
				      const int_g file_type,
				      char message[], const int_g mlen)
{
   DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  real_l a, b, c, alpha, beta, gamma;
  const int_g len = 256;
  char line[len];
  std::ifstream input;
  int_g dim = 4; // surface
  char atomids[2];
  DLV::coord_type coords[3];
  int_g dw, dw2, nocc, nx1, nx2, ny1, ny2, nz;
  real_g xc1, xc2, yc1, yc2;
  real_l nx1_lreal_g;
  bool no_fit_file = true;
  structure = DLV::model::create_atoms(name, dim);
  if (structure == 0){
    strncpy(message, "Create model failed", mlen - 1);
    ok = DLV_ERROR;
    return structure;
  }
  if(file_type%2 ==0){
    // Read in bulk file 0=bulk, 1=surf, 2=bulk+surf, 3=fit, 4=bulk+fit
    if (DLV::check_filename(filename, message, mlen)) {
      if (DLV::open_file_read(input, filename, message, mlen)) {
	input.getline(line, len);  //title
	input.getline(line, len);   // lattice parameters
	if (sscanf(line, "%lf %lf %lf %lf %lf %lf", &a, &b, &c,
		   &alpha, &beta, &gamma) != 6) {
	  strncpy(message, "Error reading lattice vectors", mlen - 1);
	  ok = DLV_ERROR;
	}
	if (!structure->set_lattice_parameters(a, b, c, alpha, beta, gamma)) {
	  strncpy(message, "Error setting lattice parameters", mlen - 1);
	  ok = DLV_ERROR;
	}
	structure->complete(); //copies lattice params to vectors
	// Get atom positions
	int_g nbulk = 0;
	for(int_g i=0;;i++){
	  dw = 0;
	  input.getline(line, 256);
	  if(line[0]==0)
	    break;
	  if (sscanf(line, "%s %lf %lf %lf %i", &atomids[0], &coords[0],
		     &coords[1], &coords[2], &dw) < 4) {
	    strncpy(message, "Error reading atoms", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  }
	  DLV::atom my_atom;
	  if (isalpha(atomids[0])) {
	    atomids[1] = tolower(atomids[1]);
	    my_atom = structure->add_fractional_atom(atomids, coords);
	  } else {
	    int_g atn = atoi(atomids);
	    my_atom = structure->add_fractional_atom(atn, coords);
	  }
	  if (my_atom == 0) {
	    strncpy(message, "Error adding atom to model", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  }
	  else
	    my_atom->set_special_periodicity(true);
	  my_atom->set_rod_ids(dw);
	  nbulk++;
	}
	input.close();


      }
    }
  }
  if(file_type > 0){
    // Read in surface/fit file 0=bulk, 1=surf, 2=bulk+surf, 3=fit, 4=bulk+fit
    if (DLV::check_filename(filename2, message, mlen)) {
      if (DLV::open_file_read(input, filename2, message, mlen)) {
	input.getline(line, mlen);  //title
	input.getline(line, mlen);   // lattice parameters
	real_l a1, b1, c1, alpha1, beta1, gamma1;
	if (sscanf(line, "%lf %lf %lf %lf %lf %lf", &a1, &b1, &c1,
		   &alpha1, &beta1, &gamma1) != 6) {
	  strncpy(message, "Error reading lattice vectors", mlen - 1);
	  ok = DLV_ERROR;
	}
	if(file_type%2 ==1){ //surf or fit only
	  if (!structure->set_lattice_parameters(a1, b1, c1,
						 alpha1, beta1, gamma1)) {
	    strncpy(message, "Error setting lattice parameters", mlen - 1);
	    ok = DLV_ERROR;
	  }
	  structure->complete(); //copies lattice params to vectors
	}
	else{
	  if (a!=a1 || b!=b1 || c!=c1 || alpha!=alpha1 ||
	      beta!=beta1 || gamma!=gamma1){
	    strncpy(message,
		    "Error lattice vectors in bulk and surface files differ",
		    mlen - 1);
	    ok = DLV_ERROR;
	  }
	}
	// Get atom positions
	int_g nsurf = 0;
	for(int_g i=0;;i++){
	  dw = 0;
	  dw2 = 0;
	  input.getline(line, 256);
	  if(line[0]==0)
	    break;
	  if (nsurf > MAXATOMS) {
	    strncpy(message,
		    "maximum number of model atoms exceeded", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  }
	  if(file_type < 3){ //surface file
	    if (sscanf(line, "%s %lf %lf %lf %i %i", &atomids[0], &coords[0],
		       &coords[1], &coords[2], &dw, &dw2) < 4) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      ok = DLV_ERROR;
	      break;
	    }
	  }
	  else{  //fit file
	    dw = dw2 = nocc = nx1 = nx2 = ny1 = ny2 = nz = 0;
	    xc1 = xc2 = yc1 = yc2 = 0.0;
	    int_g nread;
	    real_g fdummy[8];
	    nread = sscanf(line,
	     "%s %lf %f %lf %f %i %lf %f %i %f %i %lf %f %f %f %f %f %f %f %f",
			   &atomids[0], &coords[0], &xc1, &nx1_lreal_g, &xc2,
			   &nx2, &coords[1],&yc1, &ny1, &yc2, &ny2,
			   &coords[2], &fdummy[0], &fdummy[1],&fdummy[2],
			   &fdummy[3], &fdummy[4],&fdummy[5], &fdummy[6],
			   &fdummy[7]);
	    if(nread < 7){  //sur file
	      coords[1] = xc1;
	      coords[2] = nx1_lreal_g;
	      dw = (int) xc2;
	      dw2 = nx2;
	    }
	    else if(nread > 16){
	      no_fit_file = false;
	      nx1 = (int)nx1_lreal_g;
	      nz = (int)fdummy[1];
	      dw = (int)fdummy[4];
	      dw2 = (int)fdummy[5];
	      nocc = (int)fdummy[6];
	    }
	    else if (nread ==16){
	      no_fit_file = false;
	      nx1 = (int)nx1_lreal_g;
	      nz = (int)fdummy[0];
	      dw = (int)fdummy[1];
	      dw2 = (int)fdummy[2];
	      nocc = (int)fdummy[3];
	    }
	    else{
	      strncpy(message, "Error reading atom data in fit file", mlen - 1);
	      ok = DLV_ERROR;
	    }
	  }
	  DLV::atom my_atom;
	  if (isalpha(atomids[0])) {
	    atomids[1] = tolower(atomids[1]);
	    my_atom = structure->add_fractional_atom(atomids, coords);
	  } else {
	    int_g atn = atoi(atomids);
	    my_atom = structure->add_fractional_atom(atn, coords);
	  }
	  if (my_atom == 0) {
	    strncpy(message, "Error adding atom to model", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  }
	  else
	    my_atom->set_special_periodicity(false);
	  if(no_fit_file)
	    my_atom->set_rod_ids(dw, dw2);
	  else
	    my_atom->set_rod_ids(dw, dw2, nocc, xc1, nx1, xc2, nx2,
				 yc1, ny1, yc2, ny2, nz);
	  nsurf++;
	}
	input.close();
      }
    }
  }
  if (ok == DLV_ERROR) {
    delete structure;
    structure = 0;
  }
  else {
    bool only_atoms = true;
    structure->complete(only_atoms);
    //To_Do should it break without only_atoms??
  }
  if (ok != DLV_OK){
    message[mlen - 1] = '\0';
  }
  return structure;
}

DLV::operation *ROD::rod_calc::accept_rod_pending(char message[],
						  const int_g mlen)
{
  DLV::shared_ptr<DLV::data_object> base_data =
    DLV::operation::get_current()->find_shared_data(rod_plot_label,
						    program_name.c_str());
#ifdef ENABLE_DLV_GRAPHICS
  DLV::rod1d_plot *data = dynamic_cast<DLV::rod1d_plot*> (base_data.get());
#endif // ENABLE_DLV_GRAPHICS
  ROD::rod_plot *op1 = dynamic_cast<rod_plot*> (DLV::operation::get_pending());
  ROD::rod_plot *op = 0;
  if(op1 !=0){
    DLV::model *m = DLV::operation::get_current_model();
    DLV::model *m1 = DLV::model::create_atoms(m->get_model_name(),
					      m->get_model_type());
    m1->copy_model(m);
    op = new ROD::rod_plot(m1, ATTEN,  SCALE, SCALE2,
		      BETA, SURFFRAC,
		      DISPL, DEBWAL, DEBWAL2, OCCUP,
		      NDISTOT, NDWTOT, NDWTOT2, NOCCTOT,
		      op1->get_bool_fit_data(), op1->get_exp_data(),
		      op1->get_hrod(), op1->get_krod(),
		      op1->get_lstart(), op1->get_lend(),
		      op1->get_nl(),
		      op1->get_plot_bulk(), op1->get_plot_surf(),
		      op1->get_plot_both(), op1->get_plot_exp(),
		      op1->get_new_view());
    if (op == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Failed to allocate ROD::rod_plot operation",
		mlen);
    }
    else{
      op->attach_no_inherit_model();
      save_parent(op);
      DLV::operation *editing = DLV::operation::get_editing();
#ifdef ENABLE_DLV_GRAPHICS
      op->render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
      op->attach_data(base_data); //clb test
#ifdef ENABLE_DLV_GRAPHICS
      op->transfer_and_render_data(editing, data);
#endif // ENABLE_DLV_GRAPHICS
      editing->detach_data(base_data);
    }
  }
  DLV::shared_ptr<DLV::data_object> base_data1 =
    DLV::operation::get_current()->find_shared_data(rod_ffac_label,
						    program_name.c_str());
#ifdef ENABLE_DLV_GRAPHICS
  DLV::rod2d_plot *data1 = dynamic_cast<DLV::rod2d_plot*> (base_data1.get());
#endif // ENABLE_DLV_GRAPHICS
  ROD::rod_ffac *op3 = dynamic_cast<rod_ffac*> (DLV::operation::get_pending());
  ROD::rod_ffac *op2 = 0;
  if(op3 !=0){
    DLV::model *m = DLV::operation::get_current_model();
    DLV::model *m1 = DLV::model::create_atoms(m->get_model_name(),
					      m->get_model_type());
    m1->copy_model(m);
    op2 = new ROD::rod_ffac(m1, ATTEN,  SCALE, SCALE2,
			    BETA, SURFFRAC,
			    DISPL, DEBWAL, DEBWAL2, OCCUP,
			    NDISTOT, NDWTOT, NDWTOT2, NOCCTOT,
			    op3->get_bool_fit_data(), op3->get_exp_data(),
			    op3->get_select_fs(), op3->get_h_start(),
			    op3->get_k_start(), op3->get_h_end(),
			    op3->get_k_end(), op3->get_h_step(),
			    op3->get_k_step(), op3->get_l_value(),
			    op3->get_maxq(), op3->get_scale_factor(),
			    op3->get_plot_calc(), op3->get_plot_exp(),
			    op3->get_new_view());
    if (op2 == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Failed to allocate ROD::rod_ffac operation",
		mlen);
    }
    else{
      op2->attach_no_inherit_model();
      save_parent(op2);
      DLV::operation *editing = DLV::operation::get_editing();
#ifdef ENABLE_DLV_GRAPHICS
      op2->render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
      op2->attach_data(base_data1); //clb test
#ifdef ENABLE_DLV_GRAPHICS
      op2->transfer_and_render_data(editing, data1);
#endif // ENABLE_DLV_GRAPHICS
      editing->detach_data(base_data1);
    }
  }
  DLV::operation::accept_pending_no_attach();
  return DLV::operation::get_current();
}


ROD::int_g ROD::rod_calc::update_1d_plot(DLV::operation *parent,
					 bool first_edit, bool cancel_edit,
					 char message[], const int_g mlen)
{
  ROD::rod_plot *parent_plot = 0;
  if (parent !=0)
    parent_plot = dynamic_cast<rod_plot*> (parent);
  if (parent !=0 && (parent_plot !=0 || cancel_edit)){
    DLV::operation *current = DLV::operation::get_current();
    ROD::rod_plot *op1 = dynamic_cast<rod_plot*> (parent);
    if(op1!=0 || cancel_edit){
      DLV::shared_ptr<DLV::data_object> base_data;
      if (first_edit)
	base_data = op1->find_shared_data(rod_plot_label,
					  program_name.c_str());
      else if (cancel_edit){
	base_data = parent->find_shared_data(rod_plot_label,
					     program_name.c_str());
	if (base_data ==0)
	  return DLV_OK;
      }
      else
	base_data = current->find_shared_data(rod_plot_label,
					      program_name.c_str());
      if(base_data == 0){
	strncpy(message, "NO 1D PLOT AVAILABLE", mlen - 1);
	return DLV_ERROR;
      }
      DLV::rod1d_plot *data = dynamic_cast<DLV::rod1d_plot*> (base_data.get());
      if(data ==0){
	strncpy(message, "NO 1D PLOT AVAILABLE", mlen - 1);
	return DLV_ERROR;
      }
      if(!cancel_edit){
	int_g npts = NL;
	bool plot_bulk = op1->get_plot_bulk();
	bool plot_surf = op1->get_plot_surf();
	bool plot_both = op1->get_plot_both();
	int_g nplot = -1;
	if(!plot_surf&&!plot_both)
	  NSURF = 0;
	if(!plot_bulk&&!plot_both)
	  NBULK = 0;
	for (int_g i = 0; i < npts; i++)
	  f_calc(HTH[i], KTH[i], LTH[i], ATTEN, LBRAGG,
		 &FTH[0][i],&FTH[1][i], &FTH[2][i], &PHASE[i]);
	real_g *plots = new real_g[npts];
	if(plot_bulk){
	  nplot++;
	  for (int_g i = 0; i < npts; i++)
	    plots[i] = FTH[0][i];
	  data->update_plot(plots, nplot);
	}
	if(plot_surf){
	  nplot++;
	  for (int_g i = 0; i < npts; i++)
	    plots[i] = FTH[1][i];
	  data->update_plot(plots, nplot);
	}
	if(plot_both){
	  nplot++;
	  for (int_g i = 0; i < npts; i++)
	    plots[i] = FTH[2][i];
	  data->update_plot(plots, nplot);
	}
      } //if not cancel_edit
#ifdef ENABLE_DLV_GRAPHICS
      DLV::operation::get_current()->render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
      if(first_edit){
	DLV::operation::get_current()->attach_data(base_data);
#ifdef ENABLE_DLV_GRAPHICS
	DLV::operation::get_current()->transfer_and_render_data(op1, data);
#endif // ENABLE_DLV_GRAPHICS
	op1->detach_data(base_data);
      }
      else if(cancel_edit){
	DLV::operation::get_current()->attach_data(base_data);
#ifdef ENABLE_DLV_GRAPHICS
	DLV::operation::get_current()->transfer_and_render_data(parent, data);
#endif // ENABLE_DLV_GRAPHICS
	parent->detach_data(base_data);
      }
      else{
#ifdef ENABLE_DLV_GRAPHICS
	current->transfer_and_render_data(current, data);
#endif // ENABLE_DLV_GRAPHICS
      }
    }
  }
  return DLV_OK;
}

ROD::int_g ROD::rod_calc::update_2d_plot(DLV::operation *parent,
					 bool first_edit, bool cancel_edit,
					 char message[], const int_g mlen)
{
  ROD::rod_ffac *parent_plot = 0;
  if (parent !=0)
    parent_plot = dynamic_cast<rod_ffac*> (parent);
  if (parent !=0 && (parent_plot !=0 || cancel_edit)){
    DLV::operation *current = DLV::operation::get_current();
    ROD::rod_ffac *op1 = dynamic_cast<rod_ffac*> (parent);
    if(op1!=0 || cancel_edit){
      DLV::shared_ptr<DLV::data_object> base_data;
      if (first_edit)
	base_data = op1->find_shared_data(rod_ffac_label,
					  program_name.c_str());
      else if (cancel_edit){
	base_data = parent->find_shared_data(rod_ffac_label,
					     program_name.c_str());
	if (base_data ==0)
	  return DLV_OK;
      }
      else
	base_data = current->find_shared_data(rod_ffac_label,
					      program_name.c_str());
      if(base_data == 0){
	strncpy(message, "NO 2D PLOT AVAILABLE", mlen - 1);
	return DLV_ERROR;
      }
      DLV::rod2d_plot *data = dynamic_cast<DLV::rod2d_plot*> (base_data.get());
      if(data ==0){
	strncpy(message, "NO 2D PLOT AVAILABLE", mlen - 1);
	return DLV_ERROR;
      }
      if(!cancel_edit){ //need to update this part
	int_g npts = NL;
	bool plot_calc = op1->get_plot_calc();
	real_g scale_factor = op1->get_scale_factor();
	for (int_g i = 0; i < npts; i++)
	  f_calc(HTH[i], KTH[i], LTH[i], ATTEN, LBRAGG,
		 &FTH[0][i],&FTH[1][i], &FTH[2][i], &PHASE[i]);
	real_g *plots = new real_g[npts];
	if(plot_calc){
	  for (int_g i = 0; i < npts; i++) {
	    plots[i] = FTH[2][i]*scale_factor;
	    if(plots[i] < 0.001)
	      plots[i] = 0.001;
	  }
	  data->update_plot(plots, 1);
	}
	delete_local_array(plots);
	//	data->set_xaxis_label("testing testing");
      } //if not cancel_edit
#ifdef ENABLE_DLV_GRAPHICS
      DLV::operation::get_current()->render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
      if(first_edit){
	DLV::operation::get_current()->attach_data(base_data);
#ifdef ENABLE_DLV_GRAPHICS
	DLV::operation::get_current()->transfer_and_render_data(op1, data);
#endif // ENABLE_DLV_GRAPHICS
	op1->detach_data(base_data);
      }
      else if(cancel_edit){
	DLV::operation::get_current()->attach_data(base_data);
#ifdef ENABLE_DLV_GRAPHICS
	DLV::operation::get_current()->transfer_and_render_data(parent, data);
#endif // ENABLE_DLV_GRAPHICS
	parent->detach_data(base_data);
      }
      else{
#ifdef ENABLE_DLV_GRAPHICS
	current->transfer_and_render_data(current, data);
#endif // ENABLE_DLV_GRAPHICS
      }
    }
  }
  return DLV_OK;
}

#else // ENABLE_ROD

ROD::rod_calc::rod_calc(DLV::model *m,
			const real_g att, const real_g sc, const real_g sc2,
			const real_g be, const real_g sfr,
			real_g dst[], real_g d1[], real_g d2[], real_g occ[],
			const int_g ndistt, const int_g ndwt,
			const int_g ndwt2, const int_g nocct,
			const bool fit_d, const bool exp_d)
  : DLV::change_geometry(m)
{
}

DLV::operation *
ROD::rod_calc::run_fit(const int_g fit_method, const real_g atten, real_g scale,
		       const real_g scale_min, const real_g scale_max,
		       const bool scale_fit, real_g scale2,
		       const real_g scale2_min, const real_g scale2_max,
		       const bool scale2_fit, real_g beta,
		       const real_g beta_min, const real_g beta_max,
		       const bool beta_fit, real_g sfrac,
		       const real_g sfrac_min, const real_g sfrac_max,
		       const bool sfrac_fit, real_g dist[],
		       const real_g dist_min[], const real_g dist_max[],
		       const int_g dist_fit[], const int_g ndistot,
		       real_g dw1[], const real_g dw1_min[],
		       const real_g dw1_max[], const int_g dw1_fit[],
		       const int_g ndwtot, real_g dw2[], const real_g dw2_min[],
		       const real_g dw2_max[], const int_g dw2_fit[],
		       const int_g ndwtot2, real_g occup[],
		       const real_g occup_min[], const real_g occup_max[],
		       const int_g occup_fit[], const int_g nocctot,
		       real_g fit_results[][2], real_g *chisqr, real_g *norm,
		       real_g *quality, const bool fit_data,
		       const bool exp_data, char message[], const int_g mlen,
		       const bool attach_op)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}


DLV::operation *
ROD::rod_calc::plot_rod(DLV::model *m, const real_g atten,
			const real_g scale, const real_g scale2,
			const real_g beta, const real_g sfrac, real_g dist[],
			real_g dw1[], real_g dw2[], real_g occup[],
			const int_g ndistot, const int_g ndwtot,
			const int_g ndwtot2, const int_g nocctot,
			const real_g hrod, const real_g krod,
			const real_g lstart, const real_g lend,
			const int_g nl, const bool plot_bulk,
			const bool plot_surf, const bool plot_both,
			const bool plot_exp, const bool fit_data,
			const bool exp_data, const bool new_view,
			char message[], const int_g mlen, const bool attach_op)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}

DLV::operation *
ROD::rod_calc::plot_ffactors(DLV::model *m, const real_g atten,
			     const real_g scale, const real_g scale2,
			     const real_g beta, const real_g sfrac,
			     real_g dist[], real_g dw1[], real_g dw2[],
			     real_g occup[], const int_g ndistot,
			     const int_g ndwtot, const int_g ndwtot2,
			     const int_g nocctot, const int_g select_fs,
			     real_g h_start, real_g k_start, const real_g h_end,
			     const real_g k_end, const real_g h_step,
			     const real_g k_step, const real_g l,
			     const real_g maxq, const bool plot_calc,
			     const bool plot_exp, const bool fit_data,
			     const bool exp_data, const bool new_view,
			     char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}

ROD::int_g ROD::rod_calc::update_1d_plot(DLV::operation *parent,
					 bool first_edit, bool cancel_edit,
					 char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return DLV_ERROR;
}

ROD::int_g ROD::rod_calc::update_2d_plot(DLV::operation *parent,
					 bool first_edit, bool cancel_edit,
					 char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return DLV_ERROR;
}

DLV::operation *ROD::rod_calc::accept_rod_pending(char message[],
						  const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}

ROD::int_g ROD::rod_calc::get_rod_data(real_g &scale, real_g &scale_min,
				real_g &scale_max, bool &scale_fit,
				real_g &scale2, real_g &scale2_min,
				real_g &scale2_max, bool &scale2_fit,
				real_g &beta, real_g &beta_min,
				real_g &beta_max, bool &beta_fit,
				real_g &sfrac, real_g &sfrac_min,
				real_g &sfrac_max, bool &sfrac_fit,
				int_g &ndisttot, int_g &ndwtot,
				int_g &ndwtot2, int_g &nocctot,
				int_g &maxpar,
				bool got_par_file,
				bool &show_fit_results,
				char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return DLV_ERROR;
}

ROD::int_g ROD::rod_calc::get_rod_array_data(real_g dist[], real_g dist_min[],
				     real_g dist_max[], int_g dist_fit[],
				     real_g dw1[], real_g dw1_min[],
				     real_g dw1_max[], int_g dw1_fit[],
				     real_g dw2[], real_g dw2_min[],
				     real_g dw2_max[], int_g dw2_fit[],
				     real_g occup[], real_g occup_min[],
				     real_g occup_max[], int_g occup_fit[],
				     int_g ndisttot, int_g ndwtot,
				     int_g ndwtot2, int_g nocctot,
				      bool got_par_data,
				     char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return DLV_ERROR;
}

void ROD::structure_file::calc_rlat(real_g dlat[6], real_g rlat[6])
{
}

DLV::model *ROD::structure_file::read(const DLV::string name,
				      const char filename[],
				      const char filename2[],
				      const int_g file_type,
				      char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}

#endif // ENABLE_ROD

void ROD::rod_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
  if (get_model()->get_number_of_periodic_dims() > 1) {
    data = new DLV::lattice_direction_data();
    if (data != 0)
      attach_data(data);
  }
}

bool ROD::rod_calc::reload_data(class DLV::data_object *data,
				char message[], const int_g mlen)
{
  // Null call, Nothing to reload
  return true;
}

void ROD::rod_calc::save_parent(DLV::operation *op)
{
  ROD::rod_plot *op1 = dynamic_cast<rod_plot*> (op);
  if(op1 !=0){
    op1->save_parent(op);
    return;
  }
  ROD::rod_ffac *op2 = dynamic_cast<rod_ffac*> (op);
  if(op2 !=0){
    op2->save_parent(op);
    return;
  }
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void ROD::rod_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<DLV::change_geometry>(*this);
  ar & atten;
  ar & scale;
  ar & scale2;
  ar & beta;
  ar & sfrac;
  ar & fit_data;
  ar & exp_data;
  ar & ndistot;
  ar & ndwtot;
  ar & ndwtot2;
  ar & nocctot;
  for(int i=0;i<ndistot;i++)
    ar & dist[i];
  for(int i=0;i<ndwtot;i++)
    ar & dw1[i];
  for(int i=0;i<ndwtot2;i++)
    ar & dw2[i];
  for(int i=0;i<nocctot;i++)
    ar & occup[i];
}

template <class Archive>
void ROD::rod_calc::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::change_geometry>(*this);
  ar & atten;
  ar & scale;
  ar & scale2;
  ar & beta;
  ar & sfrac;
  ar & fit_data;
  ar & exp_data;
  ar & ndistot;
  ar & ndwtot;
  ar & ndwtot2;
  ar & nocctot;
  dist = new float[ndistot];
  for(int i=0;i<ndistot;i++)
    ar & dist[i];
  dw1 = new float[ndwtot];
  for(int i=0;i<ndwtot;i++)
    ar & dw1[i];
  dw2 = new float[ndwtot2];
  for(int i=0;i<ndwtot2;i++)
    ar & dw2[i];
  occup = new float[nocctot];
  for(int i=0;i<nocctot;i++)
    ar & occup[i];
}

template void ROD::rod_calc::load<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);
template void ROD::rod_calc::save<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int) const;

#  ifdef DLV_EXPLICIT_TEMPLATES

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_calc>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_calc>;
template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_calc> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_calc> >;
template class boost::serialization::extended_type_info_typeid<ROD::rod_calc>;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::rod_calc> >;
template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_calc, DLV::change_geometry>;
template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_calc, DLV::change_geometry> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_calc, DLV::change_geometry> >;
template ROD::rod_calc* boost::serialization::factory<ROD::rod_calc, 0>(std::va_list);
template ROD::rod_calc* boost::serialization::factory<ROD::rod_calc, 1>(std::va_list);
template ROD::rod_calc* boost::serialization::factory<ROD::rod_calc, 2>(std::va_list);
template ROD::rod_calc* boost::serialization::factory<ROD::rod_calc, 3>(std::va_list);
template ROD::rod_calc* boost::serialization::factory<ROD::rod_calc, 4>(std::va_list);

template boost::serialization::detail::base_cast<DLV::change_geometry, ROD::rod_calc>::type& boost::serialization::base_object<DLV::change_geometry, ROD::rod_calc>(ROD::rod_calc&);
template boost::serialization::detail::base_cast<DLV::change_geometry, ROD::rod_calc const>::type& boost::serialization::base_object<DLV::change_geometry, ROD::rod_calc const>(ROD::rod_calc const&);
template DLV::change_geometry const* boost::serialization::smart_cast<DLV::change_geometry const*, ROD::rod_calc const*>(ROD::rod_calc const*);
template ROD::rod_calc const* boost::serialization::smart_cast<ROD::rod_calc const*, DLV::change_geometry const*>(DLV::change_geometry const*);

#  endif // DLV_EXPLICIT_TEMPLATES

#endif // DLV_USES_SERIALIZE
