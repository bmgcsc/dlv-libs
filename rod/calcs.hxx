
#ifndef ROD_CALCS
#define ROD_CALCS

namespace ROD {

  using DLV::int_g;
  using DLV::real_g;
  using DLV::real_l;

  //will need different types i think
  const DLV::string program_name = "ROD";
  const DLV::string rod_plot_label = "rod 1D plot";
  const DLV::string rod_ffac_label = "rod 2D plot";

  class structure_file {
  public:
    static void calc_rlat(real_g dlat[6], real_g rlat[6]);
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    const char filename2[], const int_g file_type,
			    char message[], const int_g mlen);
  };

  class rod_calc : public DLV::change_geometry {
  public:
    static int_g get_rod_data(real_g &scale, real_g &scale_min,
			      real_g &scale_max, bool &scale_fit,
			      real_g &scale2, real_g &scale2_min,
			      real_g &scale2_max, bool &scale2_fit,
			      real_g &beta, real_g &beta_min,
			      real_g &beta_max, bool &beta_fit,
			      real_g &sfrac, real_g &sfrac_min,
			      real_g &sfrac_max, bool &sfrac_fit,
			      int_g &ndisttot, int_g &ndwtot,
			      int_g &ndwtot2, int_g &nocctot,
			      int_g &maxpar,
			      bool got_par_file,
			      bool &show_fit_results,
			      char message[], const int_g mlen);
    static int_g get_rod_array_data(real_g dist[], real_g dist_min[],
				    real_g dist_max[], int_g dist_fit[],
				    real_g dw1[], real_g dw1_min[],
				    real_g dw1_max[], int_g dw1_fit[],
				    real_g dw2[], real_g dw2_min[],
				    real_g dw2_max[], int_g dw2_fit[],
				    real_g occup[], real_g occup_min[],
				    real_g occup_max[], int_g occup_fit[],
				    int_g ndisttot, int_g ndwtot,
				    int_g ndwtot2, int_g nocctot,
				    bool got_par_file,
				    char message[], const int_g mlen);
    rod_calc(DLV::model *m, const real_g at, const real_g sc,
	     const real_g sc2, const real_g be, const real_g sfr,
	     real_g dst[], real_g d1[], real_g d2[], real_g occ[],
	     const int_g ndistt, const int_g ndwt,
	     const int_g ndwt2, const int_g nocct,
	     const bool fit_d, const bool exp_d);
    static DLV::operation *plot_rod(DLV::model *m, const real_g atten,
				    const real_g scale, const real_g scale2,
				    const real_g beta, const real_g sfrac,
				    real_g dist[], real_g dw1[], real_g dw2[],
				    real_g occup[],
				    const int_g ndistot, const int_g ndwtot,
				    const int_g ndwtot2, const int_g nocctot,
				    const real_g hrod, const real_g krod,
				    const real_g lstart, const real_g lend,
				    const int_g nl, const bool plot_bulk,
				    const bool plot_surf,
				    const bool plot_both, const bool plot_exp,
				    const bool fit_data, const bool exp_data,
				    const bool new_view,
				    char message[], const int_g mlen,
				    const bool attach_op = true);
    static DLV::operation *plot_ffactors(DLV::model *m, const real_g atten,
					 const real_g scale,
					 const real_g scale2, const real_g beta,
					 const real_g sfrac, real_g dist[],
					 real_g dw1[], real_g dw2[],
					 real_g occup[], const int_g ndistot,
					 const int_g ndwtot,
					 const int_g ndwtot2,
					 const int_g nocctot,
					 const int_g select_fs, real_g h_start,
					 real_g k_start, const real_g h_end,
					 const real_g k_end,
					 const real_g h_step,
					 const real_g k_step,
					 const real_g l, const real_g maxh,
					 const bool plot_calc,
					 const bool plot_exp,
					 const bool fit_data,
					 const bool exp_data,
					 const bool new_view,
					 char message[], const int_g mlen);
    static DLV::operation *run_fit(const int_g fit_method,
				   const real_g atten, real_g scale,
				   const real_g scale_min,
				   const real_g scale_max,
				   const bool scale_fit, real_g scale2,
				   const real_g scale2_min,
				   const real_g scale2_max,
				   const bool scale2_fit,
				   real_g beta, const real_g beta_min,
				   const real_g beta_max, const bool beta_fit,
				   real_g sfrac, const real_g sfrac_min,
				   const real_g sfrac_max, const bool sfrac_fit,
				   real_g dist[], const real_g dist_min[],
				   const real_g dist_max[],
				   const int_g dist_fit[], const int_g ndistot,
				   real_g dw1[], const real_g dw1_min[],
				   const real_g dw1_max[],
				   const int_g dw1_fit[], const int_g ndwtot,
				   real_g dw2[], const real_g dw2_min[],
				   const real_g dw2_max[],
				   const int_g dw2_fit[], const int_g ndwtot2,
				   real_g occup[], const real_g occup_min[],
				   const real_g occup_max[],
				   const int_g occup_fit[], const int_g nocctot,
				   real_g fit_results[][2],
				   real_g *chisqr, real_g *norm,
				   real_g *quality, const bool fit_data,
				   const bool exp_data, char message[],
				   const int_g mlen,
				   const bool attach_op = true);
    static int_g update_1d_plot(DLV::operation *parent, bool first_edit,
				bool cancel_edit, char message[],
				const int_g mlen);
    static int_g update_2d_plot(DLV::operation *parent, bool first_edit,
				bool cancel_edit, char message[],
				const int_g mlen);
    static DLV::operation *accept_rod_pending(char message[], const int_g mlen);
    bool reload_data(class DLV::data_object *data, char message[],
		     const int_g mlen);
    int_g initialise_data(char message[], const int_g mlen);
    bool get_bool_fit_data();
    bool get_exp_data();
    DLV::atom_integers *get_atom_labels();
    void set_atom_labels(DLV::operation *op);
    static void save_parent(DLV::operation *op);
  protected:
    void add_standard_data_objects();
    int_g get_ndistot() const;
    int_g get_ndwtot() const;
    int_g get_ndwtot2() const;
    int_g get_nocctot() const;
    void copy_dist(real_g d[]) const;
    void copy_dw1(real_g d[]) const;
    void copy_dw2(real_g d[]) const;
    void copy_occ(real_g d[]) const;

  private:
    int_g ndistot, ndwtot, ndwtot2, nocctot;
    real_g *dist, *dw1, *dw2, *occup;
    real_g atten;
    real_g scale;
    real_g scale2;
    real_g beta;
    real_g sfrac;
    bool fit_data;
    bool exp_data;
    DLV::atom_integers *atom_labels;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(ROD::rod_calc)
#endif // DLV_USES_SERIALIZE

inline ROD::int_g ROD::rod_calc::get_ndistot() const
{
  return ndistot;
}

inline ROD::int_g ROD::rod_calc::get_ndwtot() const
{
  return ndwtot;
}

inline ROD::int_g ROD::rod_calc::get_ndwtot2() const
{
  return ndwtot2;
}

inline ROD::int_g ROD::rod_calc::get_nocctot() const
{
  return nocctot;
}

inline bool ROD::rod_calc::get_bool_fit_data()
{
  return fit_data;
}
inline bool ROD::rod_calc::get_exp_data()
{
  return exp_data;
}

inline DLV::atom_integers *ROD::rod_calc::get_atom_labels()
{
  return atom_labels;
}

inline void ROD::rod_calc::set_atom_labels(DLV::operation *op)
{
  atom_labels = new DLV::atom_integers("ROD", "labels", op,"atom labels");
}

inline void ROD::rod_calc::copy_dist(real_g d[]) const
{
  for (int_g i = 0; i < ndistot; i++)
    d[i] = dist[i];
}

inline void ROD::rod_calc::copy_dw1(real_g d[]) const
{
  for (int_g i = 0; i < ndwtot; i++)
    d[i] = dw1[i];
}

inline void ROD::rod_calc::copy_dw2(real_g d[]) const
{
  for (int_g i = 0; i < ndwtot2; i++)
    d[i] = dw2[i];
}

inline void ROD::rod_calc::copy_occ(real_g d[]) const
{
  for (int_g i = 0; i < nocctot; i++)
    d[i] = occup[i];
}

#endif // ROD_CALCS
