#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "data.hxx"
#ifdef ENABLE_ROD
#include "rod/rod.h"

ROD::int_g ROD::data::read_data_file(const char filename[], bool *use_scale2,
			      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  char line[256];
  char title[256];
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      input.getline(title, 256);
      NDAT = 0;
      for(int_g i=0;;i++){
	input.getline(line, 256);

	if(line[0]==0){
	  break;
	}
	if (sscanf(line, "%f %f %f %f %f %f", &HDAT[NDAT], &KDAT[NDAT],
		   &LDAT[NDAT], &FDAT[NDAT],&ERRDAT[NDAT],&LBR[NDAT]) >= 4){
	  NDAT++;
	  if (NDAT == MAXDATA){
	    strncpy(message,
		    "Maximum number of data points exceeded, file truncated",
		    mlen - 1);
	    ok = DLV_WARNING;
	  }
	}
	else{
	  strncpy(message, "Error reading data file", mlen - 1);
	  ok = DLV_ERROR;
	  break;
	}
      }
    }
    else{
      strncpy(message, "Can not open data file", mlen - 1);
      ok = DLV_ERROR;
    }
  }
  else{
    strncpy(message, "Empty Filename", mlen - 1);
    ok = DLV_ERROR;
  }
  LBRAGG = LBR[0];
  for (int_g i = 0; i < NDAT; i++)
    if(LBR[i] < 0){
      *use_scale2 = true;
      break;
    }
  if (NDAT == 0){
    strncpy(message, "No data in file", mlen - 1);
    ok = DLV_ERROR;
  }
  return ok;
}

ROD::int_g ROD::data::read_par_file(const char filename[],
			      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  char line[256];
  char command[20];
  DLV::string com;
  char text[20];
  //set default values
  SCALE = 1.0;
  SCALELIM[0] = 0.5;
  SCALELIM[1] = 2.0;
  FIXPAR[0] = 0;
  SCALE2 = 1.0;
  SCALE2LIM[0] = 0.5;
  SCALE2LIM[1] = 2.0;
  FIXPAR[1] = 0;
  BETA = 0.0;
  BETALIM[0] = 0.0;
  BETALIM[1] = 1.0;
  FIXPAR[2] = 0;
  SURFFRAC = 1.0;
  SURFFRACLIM[0] = 0.25;
  SURFFRACLIM[1] = 1.0;
  FIXPAR[3] = 0;
  // need to do disp etc too
  ROD::data::initialise_model(message, mlen); //get NDWTOT etc
  for (int_g i = 0; i< NDISTOT; i++){
    DISPL[i] = 0.0;
    DISPLLIM[0][i] = -1.0;
    DISPLLIM[1][i] = 1.0;
    FIXPAR[4+i] = 1;
  }
  for (int_g i = 0; i< NDWTOT; i++){
    DEBWAL[i] = 0.0;
    DEBWALLIM[0][i] = 0.0;
    DEBWALLIM[1][i] = 1.0;
    FIXPAR[4+NDISTOT+i] = 1;
  }
  for (int_g i = 0; i< NDWTOT2; i++){
    DEBWAL2[i] = 0.0;
    DEBWAL2LIM[0][i] = 0.0;
    DEBWAL2LIM[1][i] = 1.0;
    FIXPAR[4+NDISTOT+NDWTOT+i] = 1;
  }
  for (int_g i = 0; i< NOCCTOT; i++){
    OCCUP[i] = 1.0;
    OCCUPLIM[0][i] = 1.0;
    OCCUPLIM[1][i] = 1.0;
    FIXPAR[4+NDISTOT+NDWTOT+NDWTOT+2+i] = 1;
  }
  // idaally a routine should be called for the above default values
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      for(int_g i=0;; i++){
	input.getline(line, 256);
	if(line[0] !='!')
	  break;
      }
      int_g tmp;
      real_g val, min, max;
      for(int_g i=0;; i++){
	input.getline(line, 256);
	if(line[0] == 0)
	  break;
	sscanf(line, "%s", &command[0]);
	if (strcmp(command, "scale")==0) {
	  sscanf(line, "%s %f %f %f %s", &command[0], &SCALE,
		 &SCALELIM[0], &SCALELIM[1], text);
	  if(SCALELIM[0] > SCALE)
	    SCALELIM[0] = SCALE;
	  if(SCALELIM[1] < SCALE)
	    SCALELIM[1] = SCALE;
	  FIXPAR[0] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "scale2")==0) {
	  sscanf(line, "%s %f %f %f %s", &command[0], &SCALE2,
		 &SCALE2LIM[0], &SCALE2LIM[1], text);
	  if(SCALE2LIM[0] > SCALE2)
	    SCALE2LIM[0] = SCALE2;
	  if(SCALE2LIM[1] < SCALE2)
	    SCALE2LIM[1] = SCALE2;
	  FIXPAR[1] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "beta")==0) {
	  sscanf(line, "%s %f %f %f %s", &command[0], &BETA,
		 &BETALIM[0], &BETALIM[1], text);
	  if(BETALIM[0] > BETA)
	    BETALIM[0] = BETA;
	  if(BETALIM[1] < BETA)
	    BETALIM[1] = BETA;
	  FIXPAR[2] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "surffrac")==0) {
	  sscanf(line, "%s %f %f %f %s", &command[0], &SURFFRAC,
		 &SURFFRACLIM[0], &SURFFRACLIM[1], text);
	  if(SURFFRACLIM[0] > SURFFRAC)
	    SURFFRACLIM[0] = SURFFRAC;
	  if(SURFFRACLIM[1] < SURFFRAC)
	    SURFFRACLIM[1] = SURFFRAC;
	  FIXPAR[3] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "displace")==0) {
	  sscanf(line, "%s %i %f %f %f %s", &command[0], &tmp, &val,
		 &min, &max, text);
	  DISPL[tmp-1] = val;
	  DISPLLIM[tmp-1][0] = (min < val ? min : val);
 	  DISPLLIM[tmp-1][1] = (max > val ? max : val);
	  FIXPAR[3+tmp] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "b1")==0) {
	  sscanf(line, "%s %i %f %f %f %s", &command[0], &tmp, &val,
		 &min, &max, text);
	  DEBWAL[tmp-1] = val;
	  DEBWALLIM[tmp-1][0] = (min < val ? min : val);
	  DEBWALLIM[tmp-1][1] = (max > val ? max : val);
	  FIXPAR[3+NDISTOT+tmp] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "b2")==0) {
	  sscanf(line, "%s %i %f %f %f %s", &command[0], &tmp, &val,
		 &min, &max, text);
	  DEBWAL2[tmp-1] = val;
	  DEBWAL2LIM[tmp-1][0] = (min < val ? min : val);
	  DEBWAL2LIM[tmp-1][1] = (max > val ? max : val);
	  FIXPAR[3+NDISTOT+NDWTOT+tmp] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "occupancy")==0) {
	  sscanf(line, "%s %i %f %f %f %s", &command[0], &tmp, &val,
		 &min, &max, text);
	  OCCUP[tmp-1] = val;
	  OCCUPLIM[tmp-1][0] = (min < val ? min : val);
	  OCCUPLIM[tmp-1][1] = (max > val ? max : val);
	  FIXPAR[3+NDISTOT+NDWTOT+NDWTOT2+tmp] = (strcmp(text, "YES")==0 ? 0 : 1);
	}
	else if (strcmp(command, "return")==0){
	  break;
	}
	else{
	  strncpy(message, " Error command not reconised in par file", mlen - 1);
	  ok =  DLV_WARNING;
	}
      }
    }
  }
  return ok;
}

ROD::int_g ROD::data::initialise_model(char message[], const int_g mlen)
{
  //std::cout << "initialising model\n";
  DLVreturn_type ok = DLV_OK;
  DLV::model *m = DLV::operation::get_current()->get_model();
  int_g dim = m->get_model_type();
  if(dim!=4){
    strncpy(message, "INCOMPATABLE STRUCTURE - SURFACE MODEL REQUIRED", mlen - 1);
    return 0;
  }
  strcpy (INLINE,m->get_model_name().c_str());
  // Get lattice parameters
  DLV::coord_type dlat[6];
  m->get_lattice_parameters(dlat[0], dlat[1], dlat[2], dlat[3],
			    dlat[4], dlat[5]);
  for(int_g i = 0; i<6; i++)
    DLAT[i] = dlat[i];
  for(int_g i = 3; i<6; i++)
    DLAT[i] = dlat[i]/RAD;
  ROD::structure_file::calc_rlat(DLAT,RLAT);
  // Get atom positions
  int_g natoms = m->get_number_of_asym_atoms();
  if(natoms > MAXATOMS){
    strncpy(message, "TOO MANY ATOMS IN MODEL", mlen - 1);
    ok = DLV_ERROR;
    return ok;
  }
  DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,natoms,3);
  m->get_asym_atom_frac_coords(coords, natoms);
  bool *atom_periodicity = new_local_array1(bool, natoms);
  m->has_asym_atom_special_periodicity(atom_periodicity, natoms);
  NSURF = 0;
  NBULK = 0;
  for(int_g i=0; i<natoms; i++){
    if(!atom_periodicity[i]){
      XS[NSURF] = coords[i][0];
      YS[NSURF] = coords[i][1];
      ZS[NSURF] = coords[i][2]/dlat[2];  //ok for cubic stuff but otherwise??
      XSFIT[NSURF] = XS[NSURF];
      YSFIT[NSURF] = YS[NSURF];
      ZSFIT[NSURF] = ZS[NSURF];
      NSURF++;
    }
    else{
      XB[NBULK] = coords[i][0];
      YB[NBULK] = coords[i][1];
      ZB[NBULK] = coords[i][2]/dlat[2];  //ok for cubic stuff but otherwise??
      NBULK++;
   }
  }
  delete_local_array(coords);
  // Get atom types
  int_g *atom_types = new_local_array1(int_g, natoms);
  m->get_asym_atom_types(atom_types, natoms);
  // Find the different element types and store them in ELEMENT
  DLV::string tmpstr;
  tmpstr = DLV::atom_type::get_atomic_symbol(atom_types[0]);
  strcpy(ELEMENT[0], tmpstr.c_str());
  bool newtype;
  NTYPES = 1;
  for (int_g i = 0; i < natoms; i++){
    newtype = true;
    for (int_g j = 0; j < NTYPES; j++)
      if (atom_types[i] == DLV::atom_type::check_atomic_symbol(ELEMENT[j]))
	newtype = false;
    if (newtype){
      if (NTYPES == MAXTYPES){
	strncpy(message, "TOO MANY ATOMS TYPES IN MODEL", mlen - 1);
	return 0;
      }
      NTYPES++;
      tmpstr = DLV::atom_type::get_atomic_symbol(atom_types[i]);
      strcpy(ELEMENT[NTYPES-1], tmpstr.c_str());
    }
  }
  // Assign a type number to all atoms in the model
  int_g nb = 0, ns = 0;
  for (int_g i = 0; i < natoms; i++)
    for (int_g j = 0; j < NTYPES; j++)
      if (atom_types[i] == DLV::atom_type::check_atomic_symbol(ELEMENT[j])){
	if(!atom_periodicity[i]){
	  TS[ns] = j;
	  ns++;
	}
	else{
	  TB[nb] = j;
	  nb++;
	}
      }
  // Store F_COEFF for each element type
  for (int_g i = 0; i < NTYPES; i++){
    get_coeff(ELEMENT[i],F_COEFF[i], message, mlen);
    if(F_COEFF[i][0] == 0.0)
      return 0;
  }

  //SET SERIAL NUMBERS FOR DEBYE WELLAR, OCCUPANCY AND DISPLACEMENTS
  int_g *atom_dws = new_local_array1(int_g, natoms);
  int_g *atom_dws2 = new_local_array1(int_g, natoms);
  int_g *atom_nocc = new_local_array1(int_g, natoms);
  real_g *atom_xconst = new_local_array1(real_g, natoms);
  int_g *atom_nxdis = new_local_array1(int_g, natoms);
  real_g *atom_x2const = new_local_array1(real_g, natoms);
  int_g *atom_nx2dis = new_local_array1(int_g, natoms);
  real_g *atom_yconst = new_local_array1(real_g, natoms);
  int_g *atom_nydis = new_local_array1(int_g, natoms);
  real_g *atom_y2const = new_local_array1(real_g, natoms);
  int_g *atom_ny2dis = new_local_array1(int_g, natoms);
  int_g *atom_nzdis = new_local_array1(int_g, natoms);
  int_g j, k;
  // SET ALL TO ZERO INITIALLY - NECCESSARY????
  for (int_g i =1; i< NBULK; i++)
    NDWB[i] = 0;
  for (int_g i =1; i< NSURF; i++)
    NDWS[i] = 0;
  m->get_asym_rod_dw1(atom_dws, natoms);
  m->get_asym_rod_dw2(atom_dws2, natoms);
  m->get_asym_rod_noccup(atom_nocc, natoms);
  m->get_asym_rod_xconst(atom_xconst, natoms);
  m->get_asym_rod_nxdis(atom_nxdis, natoms);
  m->get_asym_rod_x2const(atom_x2const, natoms);
  m->get_asym_rod_nx2dis(atom_nx2dis, natoms);
  m->get_asym_rod_yconst(atom_yconst, natoms);
  m->get_asym_rod_nydis(atom_nydis, natoms);
  m->get_asym_rod_y2const(atom_y2const, natoms);
  m->get_asym_rod_ny2dis(atom_ny2dis, natoms);
  m->get_asym_rod_nzdis(atom_nzdis, natoms);
  j = 0;
  k = 0;
  for (int_g i=0; i<natoms; i++)
    if (!atom_periodicity[i]){
      NDWS[j] = atom_dws[i];
      NDWS2[j] = atom_dws2[i];
      NOCCUP[j] = atom_nocc[i];
      XCONST[j] = atom_xconst[i];
      NXDIS[j] = atom_nxdis[i];
      X2CONST[j] = atom_x2const[i];
      NX2DIS[j] = atom_nx2dis[i];
      YCONST[j] = atom_yconst[i];
      NYDIS[j] = atom_nydis[i];
      Y2CONST[j] = atom_y2const[i];
      NY2DIS[j] = atom_ny2dis[i];
      NZDIS[j] = atom_nzdis[i];
     j++;
    }
    else{
      NDWB[k] = atom_dws[i];
      k++;
    }
  delete_local_array(atom_types);
  delete_local_array(atom_dws);
  delete_local_array(atom_dws2);
  delete_local_array(atom_nocc);
  delete_local_array(atom_xconst);
  delete_local_array(atom_nxdis);
  delete_local_array(atom_x2const);
  delete_local_array(atom_nx2dis);
  delete_local_array(atom_yconst);
  delete_local_array(atom_nydis);
  delete_local_array(atom_y2const);
  delete_local_array(atom_ny2dis);
  delete_local_array(atom_nzdis);
  // Find highest serial number of the displacement, Debye-Waller and
  //occupancy parameters
  NDWTOT  = 0;
  NDWTOT2 = 0;
  NOCCTOT = 0;
  NDISTOT = 0;
  for (int_g i = 0; i < NBULK; i++)
    if (NDWB[i] > NDWTOT)
      NDWTOT = NDWB[i];
  for (int_g i = 0; i < NSURF; i++) {
    if (NDWS[i] > NDWTOT)
      NDWTOT = NDWS[i];
    if (NDWS2[i] > NDWTOT2)
      NDWTOT2 = NDWS2[i];
    if (NOCCUP[i] > NOCCTOT)
      NOCCTOT = NOCCUP[i];
    if (NXDIS[i] > NDISTOT)
      NDISTOT = NXDIS[i];
    if (NX2DIS[i] > NDISTOT)
      NDISTOT = NX2DIS[i];
    if (NYDIS[i] > NDISTOT)
      NDISTOT = NYDIS[i];
    if (NY2DIS[i] > NDISTOT)
      NDISTOT = NY2DIS[i];
    if (NZDIS[i] > NDISTOT)
      NDISTOT = NZDIS[i];
  }
  if (NDWTOT > MAXPAR) {
    NDWTOT = MAXPAR;
    strncpy(message, "Error, too many  parallel Debye-Waller parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NDWTOT2 > MAXPAR) {
    NDWTOT2 = MAXPAR;
    strncpy(message, "Error, too many  perpendicular Debye-Waller parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NOCCTOT > MAXPAR) {
    NOCCTOT = MAXPAR;
    strncpy(message, "Error, too many occupancy parameters",
	    mlen - 1);
    ok = DLV_ERROR;
  }
  if (NDISTOT > MAXPAR) {
    NDISTOT = MAXPAR;
    strncpy(message, "Error, too many displacement parameters ", mlen - 1);
    ok = DLV_ERROR;
  }
  return ok;
}

ROD::real_g *ROD::data::get_coeff(char el[3], real_g coeff[9],
				  char message[], const int_g mlen)
{
  /*
    Assigns the coefficients for an analytical approximation of the
    atomic scattering factor of a specificied element.
    The coefficients are taken from "International Tables for X-ray
    Crystallography, Vol IV", p. 99-101.
    Returns a pointer to the coefficients, if found, and fills coeff[]
  */
  static real_g        co[][9] =
    {
      {0.493002,10.5109,0.322912,26.1257,0.140191,3.14236,
       0.040810,57.7997,0.003038},                             /* H */
      {0,0,0,0,0,0,0,0,0},                                     /* He */
      {1.282,3.9546,0.7508,1.0524,0.6175,85.3905,
       0.4653,168.261,0.0377},                                  /* Li */
      {1.5919,43.6427,1.1278,1.8623,0.5391,103.483,
       0.7029,0.542,0.0385},                                   /* Be */
      {2.0545,23.2185,1.3326,1.021,1.0979,60.3498,
       0.7068,0.1403,-0.1932},                                 /* B */
      {2.31,20.8439,1.02,10.2075,1.5886,0.5687,
       0.865,51.6512,0.2156},                                  /* C */
      {12.2126,0.0057,3.1322,9.8933,2.0125,28.9975,
       1.1663,0.5826,-11.529},                                 /* N */
      {3.0485,13.2771,2.2868,5.7011,1.5463,0.3239,
       0.867,32.9089,0.2508},                                  /* O */
      {3.5392,10.2825,2.6412,4.2944,1.517,0.2615,
       1.0243,26.1476,0.2776},                                 /* F */
      {0,0,0,0,0,0,0,0,0},                                     /* Ne */
      {4.7626,3.285,3.1736,8.8422,1.2674,0.3136,
       1.1128,129.424,0.676},                                  /* Na */
      {5.4204,2.8275,2.1735,79.2611,1.2269,0.3808,
       2.3073,7.1937,0.8584},                                  /* Mg */
      {6.4202,3.0387,1.9002,0.7426,1.5936,31.5472,
       1.9646,85.0886,1.1151},                                 /* Al */
      {6.2915,2.4386,3.0353,32.3337,1.9891,0.6785,
       1.541,81.6937,1.1407},                                  /* Si */
      {6.4345,1.9067,4.1791,27.157,1.78,0.526,1.4908,
       68.1645,1.1149},                                        /* P */
      {6.9053,1.4679,5.2034,22.2151,1.4379,0.2536,
       1.5863,56.172,0.8669},                                  /* S */
      {11.4604,0.0104,7.1964,1.1662,6.2556,18.5194,
       1.6455,47.7784,-9.5574},                                /* Cl */
      {18.2915,0.0066,7.2084,1.1717,6.5337,19.5424,
       2.3386,60.4486,-16.378},                                 /* Cx = Cl- */
      {0,0,0,0,0,0,0,0,0},                                     /* Ar */
      {8.2186,12.7949,7.4398,0.7748,1.0519,213.187,
       0.8659,41.6841,1.4228},                                 /* K */
      {8.6266,10.4421,7.3873,0.6599,1.5899,85.7484,
       1.0211,178.437,1.3751},                                 /* Ca */
      {9.189,9.0213,7.3679,0.5729,1.6409,136.108,
       1.468,51.3531,1.3329},                                   /* Sc */
      {9.7595,7.8508,7.3558,0.5,1.6991,35.6338,
       1.9021,116.105,1.2807},                                 /* Ti */
      {10.2971,6.8657,7.3511,0.4385,2.0703,26.8938,
       2.0571,102.478,1.2199},                                 /* V */
      {10.6406,6.1038,7.3537,0.392,3.324,20.2626,
       1.4922,98.7399,1.1832},                                 /* Cr */
      {11.2819,5.3409,7.3573,0.3432,3.0193,17.8674,
       2.2441,83.7543,1.0896},                                 /* Mn */
      {11.7695, 4.7611, 7.3573, 0.3072, 3.5222, 15.3535,
       2.3045, 76.8805, 1.0369},                               /* Fe */
      {12.2841,4.2791,7.3409,0.2784,4.0034,13.5359,
       2.3488,71.1692,1.0118},                                 /* Co*/
      {12.8376,3.8785,7.292,0.2565,4.4438,12.1763,
       2.38,66.3421,1.0341},                                   /* Ni */
      {13.338,3.5828,7.1676,0.247,5.6158,11.3966,
       1.6735,64.8126,1.191},                                  /* Cu */
      {14.0743,3.2655,7.0318,0.2333,5.1652,10.3163,
       2.41,58.7097,1.3041},                                   /* Zn */
      {15.2354,3.0669,6.7006,0.2412,4.3591,10.7805,
       2.9623,61.4135,1.7189},                                 /* Ga */
      {16.0816,2.8509,6.3747,0.2516,3.7068,11.4468,
       3.683,54.7625,2.1313},                                  /* Ge */
      {16.6723,2.6345,6.0701,0.2647,3.4313,12.9479,
       4.2779,47.7972,2.531},                                  /* As */
      {17.0006,2.4098,5.8196,0.2726,3.973,15.2372,
       4.3543,43.8163,2.8409},                                 /* Se */
      {17.1789,2.1723,5.2358,16.5796,5.6377,0.2609,
       3.9851,41.4328,2.9557},                                 /* Br */
      {0,0,0,0,0,0,0,0,0},                                     /* Kr */
      {17.1784,1.7888,9.6435,17.3151,5.1399,0.2748,
       1.5292,164.934,3.4873},                                 /* Rb */
      {17.5663,1.5564,9.8184,14.0988,5.422,0.1664,
       2.6694,132.376,2.5064},                                 /* Sr */
      {17.776,1.4029,10.2946,12.8006,5.72629,0.125599,
       3.26588,104.354,1.91213},                               /* Y */
      {17.8765,1.27618,10.948,11.916,5.41732,0.117622,
       3.65721,87.6627,2.06929},                               /* Zr */
      {17.6142,1.18865,12.0144,11.766,4.04183,0.204785,
       3.53346,69.7957,3.75591},                               /* Nb */
      {3.7025, 0.2772, 17.2356, 1.0958, 12.8876, 11.0040,
       3.7429, 61.6584, 4.3875 },                              /* Mo */
      {0,0,0,0,0,0,0,0,0},                                     /* Tc */
      {19.2674,0.80852,12.9182,8.4367,4.86337,24.7997,
       1.56756,94.2928,5.37874},                               /* Ru */
      {19.2957,0.751536,14.3501,8.21758,4.73425,25.8749,
       1.28918,98.6062,5.328},                                 /* Rh */
      {19.3319,0.698655,15.5017,7.98929,5.29537,25.2052,
       0.605844,76.8986,5.26593},                              /* Pd */
      {19.2808,0.6446,16.6885,7.4726,4.8045,24.6605,
       1.0463,99.8156,5.179},                                  /* Ag */
      {19.2214,0.5946,17.6444,6.9089,4.461,24.7008,
       1.6029,87.4825,5.0694},                                 /* Cd */
      {19.1624,0.5476,18.5596,6.3776,4.2948,25.8499,
       2.0396,92.8029,4.9391},			         /* In */
      {19.1889,5.8303,19.1005,0.5031,4.4585,26.8909,
       2.4663,83.9571,4.7821},                                 /* Sn */
      {19.6418,5.3034,19.0455,0.4607,5.0371,27.9074,
       2.6827,75.2825,4.5909},                                 /* Sb */
      {19.9644,4.81742,19.0138,0.420885,6.14487,28.5284,
       2.5239,70.8403,4.352},                                  /* Te */
      {20.1472,4.347,18.9949,0.3814,7.5138,27.766,
       2.2735,66.8776,4.0712},                                 /* I */
      {0,0,0,0,0,0,0,0,0},                                     /* Xe */
      {20.3892,3.569,19.1062,0.3107,10.662,24.3879,
       1.4953,213.904,3.3352},                                 /* Cs */
      {20.3361,3.216,19.297,0.2756,10.888,20.2073,
       2.6959,167.202,2.7731},                                 /* Ba */
      {20.578,2.94817,19.599,0.244475,11.3727,18.7726,
       3.28719,133.124,2.14678},                                /* La */
      {21.1671,2.81219,19.7695,0.226836,11.8513,17.6083,
       3.33049,127.113,1.86264},                               /* Ce */
      {22.044,2.77393,19.6697,0.222087,12.3856,16.7669,
       2.82428,143.644,2.0583},                                /* Pr */
      {22.6845,2.66248,19.6847,0.210628,12.774,15.8850,
       2.85137,137.903,1.98486},                               /* Nd */
      {0,0,0,0,0,0,0,0,0},                                     /* Pm */
      {24.0042,2.47274,19.4258,0.196451,13.4396,14.3996,
       2.89604,128.007,2.20963},                               /* Sm */
      {24.6274,2.3879,19.0886,0.1942,13.7603,13.7546,
       2.9227,123.174,2.5745},                                 /* Eu */
      {25.0709, 2.2534, 19.0798, 0.1820, 13.8518, 12.9331,
       3.5455, 101.398, 2.4196},                               /* Gd */
      {0,0,0,0,0,0,0,0,0},                                     /* Tb */
      {26.507,2.1802,17.6383,0.202172,14.5596,12.1899,
       2.96577,111.874,4.29728},                                /* Dy */
      {26.9049,2.07051,17.294,0.19794,14.5583,11.4407,
       3.63837,92.6566,4.56796},                               /* Ho */
      {27.6563,2.07356,16.4285,0.223545,14.9779,               /* Er */
       11.3604,2.98233,105.703,5.92046},
      //      {26.7222, 1.84659, 19.7748, 0.137290, 12.1506,           /* Er3+ */
      //       8.36225, 5.17379, 17.8974, 1.71613},
      {0,0,0,0,0,0,0,0,0},                                     /* Tm */
      {0,0,0,0,0,0,0,0,0},                                     /* Yb */
      {0,0,0,0,0,0,0,0,0},                                     /* Lu */
      {0,0,0,0,0,0,0,0,0},                                     /* Hf */
      {29.2024,1.77333,15.2293,9.37046,14.5135,0.295977,
       4.76492,63.3644,9.24354},                               /* Ta */
      {29.0818,1.72029,15.43,9.2259,14.4327,0.321703,
       5.11982,57.056,9.8875},                                 /* W */
      {28.7621,1.67191,15.7189,9.09227,14.5564,0.3505,
       5.44174,52.0861,10.472},                                /* Re */
      {28.1894,1.62903,16.155,8.97948,14.9305,0.382661,
       5.67589,48.1647,11.0005},                               /* Os */
      {27.3049,1.59279,16.7296,8.86553,15.6115,0.417916,
       5.83377,45.0011,11.4722},                               /* Ir */
      {27.0059,1.51293,17.7639,8.81174,15.7131,0.424593,
       5.7837,38.6103,11.6883},                                /* Pt */
      {16.8819,0.4611,18.5913,8.6216,25.5582,1.4826,
       5.86,36.3956,12.0658},                                  /* Au */
      {20.6809,0.545,19.0417,8.4484,21.6575,1.5729,
       5.9676,38.3246,12.6089},                                /* Hg */
      {27.5446,0.65515,19.1584,8.70751,15.538,1.96347,
       5.52593,45.8149,13.1746},                               /* Tl */
      {31.0617,0.6902,13.0637,2.3576,18.442,8.618,
       5.9696,47.2579,13.4118},                                /* Pb */
      {33.3689,0.704,12.951,2.9238,16.5877,8.7937,
       6.4692,48.0093,13.5782},                                /* Bi */
      {0,0,0,0,0,0,0,0,0},                                     /* Po */
      {0,0,0,0,0,0,0,0,0},                                     /* At */
      {0,0,0,0,0,0,0,0,0},                                     /* Rn */
      {0,0,0,0,0,0,0,0,0},                                     /* Fr */
      {0,0,0,0,0,0,0,0,0},                                     /* Ra */
      {0,0,0,0,0,0,0,0,0},                                     /* Ac */
      {0,0,0,0,0,0,0,0,0},                                     /* Th */
      {0,0,0,0,0,0,0,0,0},                                     /* Pa */
      {0,0,0,0,0,0,0,0,0},                                     /* U */
      {0,0,0,0,0,0,0,0,0},                                     /* Np */
      {0,0,0,0,0,0,0,0,0},                                     /* Pu */
      {0,0,0,0,0,0,0,0,0},                                     /* Am */
      {0,0,0,0,0,0,0,0,0},                                     /* Cm */
      {0,0,0,0,0,0,0,0,0},                                     /* Bk */
      {0,0,0,0,0,0,0,0,0},                                     /* Cf */
      {501.68, 325.81, -699510.91, 67.447, 1379533.82 ,67.142,
       -680171.66, 66.845,0},                                  /* FU */
      {4.1916,12.8573,1.63969,4.17236,1.52673,47.0179,
       -20.307,-0.01404,21.9412},                               /* O- */
      {12.6920,2.81262,6.69883,0.22789,6.06692,6.36441,
       1.0066,14.4122,1.53545}                                  /* Gi = Ga3+*/
    };
  int_g i;
  /* Find element number */
  i = DLV::atom_type::check_atomic_symbol(el);
  //    i = get_element_number(el);
  if (i == 0) {
    strncpy(message, "ELEMENT TYPE NOT FOUND", mlen - 1);
    for (int_g j = 0; j < 9; j++) coeff[j] = 0.;
    return NULL;
  }
  else
    if(i < 18) // Due to the addtion of element CX (Cl-_ after Cl!!!
      i -= 1;  // As the index of co starts at 0 for H
  for (int_g j = 0; j < 9; j++) coeff[j] = co[i][j];
  if (coeff[0] == 0.)
    strncpy(message, "F CO_EFFICIENT FOR ELEMENT NOT FOUND IN LIST", mlen - 1);
  return co[i];
}

ROD::int_g ROD::data::show_atom_labels(const int_g ltype, char message[],
				       const int_g mlen)
{
  DLV::operation *base = DLV::operation::get_current();
  DLV::model *m = base->get_model();
  if(m->get_model_type()!=4){
    strncpy(message,
	    "INCOMPATABLE STRUCTURE - SURFACE MODEL REQUIRED", mlen - 1);
    return DLV_ERROR;
  }
  int_g natoms = m->get_number_of_asym_atoms();
  // try and find old labels
  DLV::data_object *old =  base->find_data("atom labels", "ROD");
  DLV::atom_integers *old_labels = dynamic_cast<DLV::atom_integers*>(old);
  DLV::atom_integers *labels;
  if(old_labels == 0){
    bool show_panel = false;
    labels = new DLV::atom_integers("ROD", "labels", base,
				    "atom labels", show_panel);
    DLV::coord_type (*tmpcoords)[3]
      = new_local_array2(DLV::coord_type, natoms, 3);
    m->get_asym_atom_cart_coords(tmpcoords, natoms);
    real_g (*coords)[3] = new real_g[natoms][3];
    for (int_g i = 0; i < natoms; i++)
      for (int_g j = 0; j < 3; j++)
	coords[i][j] = (real_g)tmpcoords[i][j];
    labels->set_grid(coords, natoms, true);
  }
  else{
    labels = old_labels;
    labels->reset_data_sets();
  }
  int_g *data = new int_g[natoms];
  switch (ltype){
  case 0:
#ifdef ENABLE_DLV_GRAPHICS
    labels->unset_3D_display(m);
#endif // ENABLE_DLV_GRAPHICS
    return DLV_OK;
    break;
  case 1:
    for (int_g i = 0; i < natoms; i++)
      data[i] = i;
    break;
  case 2:
    m->get_asym_rod_dw1(data, natoms);
    break;
  case 3:
    m->get_asym_rod_dw2(data, natoms);
    break;
  case 4:
    m->get_asym_rod_noccup(data, natoms);
    break;
  default:
    strncpy(message, "Invalid label type", mlen - 1);
    return DLV_ERROR;
  }
  labels->add_data(data, "labels", true);
  base->attach_data(labels);
#ifdef ENABLE_DLV_GRAPHICS
  base->render_data(labels);
  labels->set_3D_display(m);
#endif // ENABLE_DLV_GRAPHICS
  return DLV_OK;
}

ROD::int_g ROD::data::update_rod_props(const int_g new_dw1,
				       const bool use_dw1, int_g &ndwtot,
				       const int_g new_dw2, const bool use_dw2,
				       int_g &ndwtot2, const int_g new_occ,
				       const bool use_occ, int_g &nocctot,
				       const bool all,
				       char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  DLV::operation *base = DLV::operation::get_current();
  DLV::model *m = base->get_model();
  if(m->get_model_type()!=4){
    strncpy(message, "INCOMPATABLE STRUCTURE - SURFACE MODEL REQUIRED", mlen - 1);
    return DLV_ERROR;
  }

  ok = DLV::operation::update_rod_props(new_dw1, use_dw1, new_dw2, use_dw2,
					new_occ, use_occ, all, message, mlen);

  if(ok==DLV_OK){
    if(new_dw1 > ndwtot){
      ndwtot++;
      NDWTOT++;
    }
    if(new_dw2 > ndwtot2){
      ndwtot2++;
      NDWTOT2++;
    }
    if(new_occ > nocctot){
      nocctot++;
      NOCCTOT++;
    }
  }
  return ok;
}

#else // ENABLE_ROD
ROD::int_g ROD::data::initialise_model(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_ERROR;
  strncpy(message, "Compiled without ROD", mlen - 1);
  return ok;
}
ROD::real_g *ROD::data::get_coeff(char el[3], real_g coeff[9],
			    char message[], const int_g mlen)
{
return NULL;
}

ROD::int_g ROD::data::read_data_file(const char filename[], bool *use_scale2,
			      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_ERROR;
  strncpy(message, "Compiled without ROD", mlen - 1);
  return ok;
}

ROD::int_g ROD::data::show_atom_labels(const int_g ltype, char message[],
				       const int_g mlen)
{
  DLVreturn_type ok = DLV_ERROR;
  strncpy(message, "Compiled without ROD", mlen - 1);
  return ok;
}

ROD::int_g ROD::data::read_par_file(const char filename[],
			      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_ERROR;
  strncpy(message, "Compiled without ROD", mlen - 1);
  return ok;
}

ROD::int_g ROD::data::update_rod_props(const int_g new_dw1, const bool use_dw1,
				       int_g &ndwtot, const int_g new_dw2,
				       const bool use_dw2, int_g &ndwtot2,
				       const int_g new_occ, const bool use_occ,
				       int_g &nocctot, const bool all,
				       char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_ERROR;
  strncpy(message, "Compiled without ROD", mlen - 1);
  return ok;
}
#endif // ENABLE_ROD


ROD::int_g ROD::data::set_missing_rod_ids()
{
  DLV::model *m = DLV::operation::get_current()->get_model();
  int_g natoms = m->get_number_of_asym_atoms();
  int_g *atom_ids = new_local_array1(int_g, natoms);
  m->get_asym_rod_dw1(atom_ids, natoms);
  m->set_asym_rod_dw1(atom_ids, natoms);
  m->get_asym_rod_dw2(atom_ids, natoms);
  m->set_asym_rod_dw2(atom_ids, natoms);
  m->get_asym_rod_noccup(atom_ids, natoms);
  m->set_asym_rod_noccup(atom_ids, natoms);
  return 1;
}

