#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/math_fns.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "plot.hxx"  //new
#include "data.hxx"
#include "fit.hxx"

ROD::rod_fit::rod_fit(DLV::model *m, const real_g att, const real_g sc,
		      const real_g sc2, const real_g be, const real_g sfr,
		      const bool fit_d, const bool exp_d, const real_g sc_min,
		      const real_g sc_max, const bool sc_fit,
		      const real_g sc2_min, const real_g sc2_max,
		      const bool sc2_fit, const real_g be_min,
		      const real_g be_max, const bool be_fit,
		      const real_g sfr_min, const real_g sfr_max,
		      const bool sfr_fit, real_g dis[], const real_g dis_min[],
		      const real_g dis_max[], const int_g dis_fit[],
		      const int_g nditot, real_g d1[], const real_g d1_min[],
		      const real_g d1_max[], const int_g d1_fit[],
		      const int_g ndtot, real_g d2[],  const real_g d2_min[],
		      const real_g d2_max[], const int_g d2_fit[],
		      const int_g ndtot2, real_g occ[], const real_g occ_min[],
		      const real_g occ_max[], const int_g occ_fit[],
		      const int_g notot, real_g *chi, real_g *nor, real_g *qual)
  : ROD::rod_calc(m, att, sc, sc2, be, sfr, dis, d1, d2,
		  occ, nditot, ndtot, ndtot2, notot, fit_d, exp_d),
    scale_min(sc_min), scale_max(sc_max), scale_fit(sc_fit),
    scale2_min(sc2_min), scale2_max(sc2_max), scale2_fit(sc2_fit),
    beta_min(be_min), beta_max(be_max), beta_fit(be_fit),
    sfrac_min(sfr_min), sfrac_max(sfr_max), sfrac_fit(sfr_fit),
    chisqr(chi), norm(nor), quality(qual)
{
  int_g n = get_ndistot();
  dist_min = new real_g[n];
  dist_max = new real_g[n];
  dist_fit = new int_g[n];
  for(int_g i=0;i<n;i++){
    dist_min[i] = dis_min[i];
    dist_max[i] = dis_max[i];
    dist_fit[i] = dis_fit[i];
  }
  n = get_ndwtot();
  dw1_min = new real_g[n];
  dw1_max = new real_g[n];
  dw1_fit = new int_g[n];
  for(int_g i=0;i<n;i++){
    dw1_min[i] = d1_min[i];
    dw1_max[i] = d1_max[i];
    dw1_fit[i] = d1_fit[i];
  }
  n = get_ndwtot2();
  dw2_min = new real_g[n];
  dw2_max = new real_g[n];
  dw2_fit = new int_g[n];
  for(int_g i=0;i<n;i++){
    dw2_min[i] = d2_min[i];
    dw2_max[i] = d2_max[i];
    dw2_fit[i] = d2_fit[i];
  }
  n = get_nocctot();
  occup_min = new real_g[n];
  occup_max = new real_g[n];
  occup_fit = new int_g[n];
  for(int_g i=0;i<n;i++){
    occup_min[i] = occ_min[i];
    occup_max[i] = occ_max[i];
    occup_fit[i] = occ_fit[i];
  }
}

#ifdef ENABLE_ROD
#include "rod/rod.h"
#include "lsqfit.h"

ROD::int_g ROD::rod_fit::initialise_data(char message[], const int_g mlen)
{
  int_g ok;
  ok = ROD::rod_calc::initialise_data(message, mlen);
  if(ok !=DLV_OK)
    return ok;
  NDISTOT = get_ndistot();
  NDWTOT = get_ndwtot();
  NDWTOT2 = get_ndwtot2();
  NOCCTOT = get_nocctot();
  int_g ntot = NSF+NDISTOT+NDWTOT+NDWTOT2+NOCCTOT;
  for (int_g i = 0; i<ntot; i++)
    FIXPAR[i] = 1;
  if(scale_fit)
    FIXPAR[0] = 0;
  if(scale2_fit)
    FIXPAR[1] = 0;
  if(beta_fit)
    FIXPAR[2] = 0;
  if(sfrac_fit)
    FIXPAR[3] = 0;
  for(int_g i = 0; i<NDISTOT; i++)
    if(dist_fit[i]>0)
      FIXPAR[NSF+i] = 0;
  for(int_g i = 0; i<NDWTOT; i++)
    if(dw1_fit[i]>0)
      FIXPAR[NSF+NDISTOT+i] = 0;
  for(int_g i = 0; i<NDWTOT2; i++)
    if(dw2_fit[i]>0)
      FIXPAR[NSF+NDISTOT+NDWTOT+i] = 0;
 for(int_g i = 0; i<NOCCTOT; i++)
    if(occup_fit[i]>0)
      FIXPAR[NSF+NDISTOT+NDWTOT+NDWTOT2+i] = 0;
 // Set Limits
 SCALELIM[0] = scale_min;
 SCALELIM[1] = scale_max;
 //SCALEPEN = ;
 SCALE2LIM[0] = scale2_min;
 SCALE2LIM[1] = scale2_max;
 // SCALE2PEN;
 BETALIM[0] = beta_min;
 BETALIM[1] = beta_max;
 //BETAPEN;
 SURFFRACLIM[0] = sfrac_min;
 SURFFRACLIM[1] = sfrac_max;
 //SURFFRACPEN;
 for (int_g i = 0; i<NDISTOT; i++){
   DISPLLIM[i][0] = dist_min[i];
   DISPLLIM[i][1] = dist_max[i];
   //DISPLPEN[i] =
 }
 for (int_g i = 0; i<NDWTOT; i++){
   DEBWALLIM[i][0] = dw1_min[i];
   DEBWALLIM[i][1] = dw1_max[i];
 }
 for (int_g i = 0; i<NDWTOT2; i++){
   DEBWAL2LIM[i][0] = dw2_min[i];
   DEBWAL2LIM[i][1] = dw2_max[i];
 }
 for (int_g i = 0; i<NOCCTOT; i++){
   OCCUPLIM[i][0] = occup_min[i];
   OCCUPLIM[i][1] = occup_max[i];
 }
  return ok;
}

DLV::operation *ROD::rod_fit::run_fit(const int_g fit_method,
				   real_g fit_results[][2],
				      char message[], const int_g mlen,
				      const bool attach_op)
{
/*  if (NDAT < 1) {
    strncpy(message, "no experimental data read in", mlen - 1);
    return 0;
}*/
  //Update values for arrays - maybe really this should be done in initialise data???
  // it is fine here currently as this routineis always called when
  // these values are changed from the GUI
  copy_dist(DISPL);
  copy_dw1(DEBWAL);
  copy_dw2(DEBWAL2);
  copy_occ(OCCUP);
  int_g ntot = init_fitpar();  //sets fitting parameters
  if (ntot > MAXFIT){
    strncpy(message, "number of fitting parameters exceeds MAXFIT", mlen - 1);
    return 0;
  }
  // TO_DO Add fake data point when Keating energy is added in the min
  //Compute weight of data points - TO_DO??
  for (int_g i = 0; i < NDAT; i++) {
    if (ERRDAT[i] > 1e-5){
      FWEIGHT[i] = 1.0/(ERRDAT[i]*ERRDAT[i]);
    }
    else
      FWEIGHT[i] = 0.;
  }
  //Generate array with index numbers of (hkl)-points for fit x-values
  real_g index[MAXDATA];
  for (int_g i = 0; i < NDAT; i++)
    index[i] = (real_g) i;
  // Calculate and store constant values, in order to speed up fitting
#ifdef SPEEDUP
  f_calc_fit_init();
#endif /* SPEEDUP */
  // Get number of free parameters and make array with free parameter numbers
  int_g freepar[MAXFIT];
  int_g nfree = 0;
  for (int_g i = 0; i<ntot; i++)
    if(FIXPAR[i] ==0){
      freepar[nfree] = i+1;
       nfree++;
    }
  //Prepare penalty factors for lsqfit:
  // for (int_g i=0; i<ntot; i++)
  // TODO - MAKE USE OF PENALTY OPTIONS
  //  penalties[i] = (FITPEN[i] == TRUE ? LARGE_PENALTY : NO_PENALTY);
  real_g penalties[MAXFIT];
  real_g  LARGE_PENALTY = 1.0E4;
  for (int_g i = 0; i<ntot; i++)
    penalties[i] = LARGE_PENALTY;
  // set main variables here as otherwise problems occur with ASA method
  // only set the minimum amount to stop ASA from crashing
  // further investiagtion needed to whether this solution works or not
 //Do the fit
 int_g rc;
   rc = lsqfit(index, FDAT, FWEIGHT, NDAT, FITPAR, ntot,
	       freepar, nfree, FITERR, FITMIN, FITMAX,
	       penalties, chisqr, ffit,
#ifdef ASAROD
	       ffit_asa,
#endif // ASAROD
	       FITTXT[0], TITLE_LENGTH, 0, fit_method, 1);
 if(rc ==0) {
   strncpy(message, "Fitting failed", mlen - 1);
   return 0;
 }
 for (int_g i = 0; i<ntot; i++){
   fit_results[i][0] = FITPAR[i];
   fit_results[i][1] = FITERR[i];
 }
 *norm = *chisqr/(NDAT-nfree+1e-10);
 if (nfree > 2)
   *quality = gammq((nfree-2)*0.5, *chisqr*0.5);
 else
   *quality = 0.0;
 for (int_g i=0; i<NDISTOT; i++)
   DISPL[i] = FITPAR[4+i];
 for (int_g i=0; i<NDWTOT; i++)
   DEBWAL[i] = FITPAR[4+NDISTOT+i];
 for (int_g i=0; i<NDWTOT2; i++)
   DEBWAL2[i] = FITPAR[4+NDISTOT+NDWTOT+i];
 for (int_g i=0; i<NOCCTOT; i++)
   OCCUP[i] = FITPAR[4+NDISTOT+NDWTOT+NDWTOT2+i];
 //write new ROD OP with these new parameters
 DLV::model *m = get_model();
 DLV::model *m1 = DLV::model::create_atoms(m->get_model_name(), m->get_model_type());
 m1->copy_model(m);
 //Update Model - or make new model???
 int_g *do_fit = new_local_array1(int_g, ntot);
 for (int_g i=0; i<ntot; i++)
   do_fit[i] = (FIXPAR[i]==0 ? 1 : 0);
 ROD::rod_fit *op =
   new ROD::rod_fit(m1, ATTEN, FITPAR[0], FITPAR[1],
		    FITPAR[2], FITPAR[3], get_bool_fit_data(), get_exp_data(),
		    FITMIN[0], FITMAX[0], bool(do_fit[0]),
		    FITMIN[1], FITMAX[1], bool(do_fit[1]),
		    FITMIN[2], FITMAX[2], bool(do_fit[2]),
		    FITMIN[3], FITMAX[3], bool(do_fit[3]),
		    &FITPAR[4], &FITMIN[4], &FITMAX[4], &do_fit[4], NDISTOT,
		    &FITPAR[4+NDISTOT], &FITMIN[4+NDISTOT], &FITMAX[4+NDISTOT],
		    &do_fit[4+NDISTOT], NDWTOT,
		    &FITPAR[4+NDISTOT+NDWTOT], &FITMIN[4+NDISTOT+NDWTOT],
		    &FITMAX[4+NDISTOT+NDWTOT], &do_fit[4+NDISTOT+NDWTOT],
		    NDWTOT2, &FITPAR[4+NDISTOT+NDWTOT+NDWTOT2],
		    &FITMIN[4+NDISTOT+NDWTOT+NDWTOT2],
		    &FITMAX[4+NDISTOT+NDWTOT+NDWTOT2],
		    &do_fit[4+NDISTOT+NDWTOT+NDWTOT2],
		    NOCCTOT, chisqr, norm, quality);
 if(op!=0 && attach_op)
   op->attach_no_inherit_model();
 return op;
}



/* DLV::operation *base1 = rod_plot::get_parent();
 if (base1 !=0){
   ROD::rod_plot *op1 = dynamic_cast<rod_plot*> (base1);
   if(op1!=0)
     DLV::operation *op1d =
       rod_calc::plot_rod(m1, ATTEN,  FITPAR[0], FITPAR[1],
			  FITPAR[2], FITPAR[3],
			  &FITPAR[4], &FITPAR[4+NDISTOT],
			  &FITPAR[4+NDISTOT+NDWTOT],
			  &FITPAR[4+NDISTOT+NDWTOT+NDWTOT2],
			  NDISTOT, NDWTOT, NDWTOT2, NOCCTOT,
			  op1->get_hrod(), op1->get_krod(),
			  op1->get_lstart(), op1->get_lend(),
			  op1->get_nl(),
			  op1->get_plot_bulk(), op1->get_plot_surf(),
			  op1->get_plot_both(), op1->get_plot_exp(),
			  op1->get_bool_fit_data(), op1->get_exp_data(),
			  op1->get_new_view(), message, mlen);
 }
 DLV::operation *base2 =  rod_ffac::get_parent();
 if (base2 !=0){
   ROD::rod_ffac *op2 = dynamic_cast<rod_ffac*> (base2);
   if(op2!=0)
     DLV::operation *op2d =
       rod_calc::plot_ffactors(m1, ATTEN, FITPAR[0], FITPAR[1],
			       FITPAR[2], FITPAR[3],
			       &FITPAR[4], &FITPAR[4+NDISTOT],
			       &FITPAR[4+NDISTOT+NDWTOT],
			       &FITPAR[4+NDISTOT+NDWTOT+NDWTOT2],
			       NDISTOT, NDWTOT, NDWTOT2, NOCCTOT,
			       op2->get_select_fs(), op2->get_h_start(),
			       op2->get_k_start(), op2->get_h_end(),
			       op2->get_k_end(), op2->get_h_step(),
			       op2->get_k_step(),
			       op2->get_l_value(), op2->get_maxq(),
			       op2->get_plot_calc(), op2->get_plot_exp(),
			       op2->get_bool_fit_data(), op2->get_exp_data(),
			       op2->get_new_view(),
			       message, mlen);
 }
 return op;
}*/

#else // ENABLE_ROD

DLV::operation *ROD::rod_fit::run_fit(const int_g fit_method,
				      real_g fit_results[][2],
				      char message[], const int_g mlen,
				      const bool attach_op)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}

#endif // ENABLE_ROD

DLV::string ROD::rod_fit::get_name() const
{
  return ("ROD FIT");
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ROD::rod_fit *t,
				    const unsigned int file_version)
    {
      ::new(t)ROD::rod_fit(0, 0.0, 0.0, 0.0, 0.0, 0.0, false, false,
			   0.0, 0.0, false, 0.0, 0.0, false, 0.0, 0.0,
			   false, 0.0, 0.0, false, 0, 0, 0, 0, 0, 0, 0, 0,
			   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }

  }
}

template <class Archive>
void ROD::rod_fit::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<ROD::rod_calc>(*this);
  ar & scale_min;
  ar & scale_max;
  ar & scale_fit;
  ar & scale2_min;
  ar & scale2_max;
  ar & scale2_fit;
  ar & beta_min;
  ar & beta_max;
  ar & beta_fit;
  ar & sfrac_min;
  ar & sfrac_max;
  ar & sfrac_fit;
  int n = get_ndistot();
  for(int i=0;i<n;i++){
    ar & dist_min[i];
    ar & dist_max[i];
    ar & dist_fit[i];
  }
  n = get_ndwtot();
  for(int i=0;i<n;i++){
    ar & dw1_min[i];
    ar & dw1_max[i];
    ar & dw1_fit[i];
  }
  n = get_ndwtot2();
  for(int i=0;i<n;i++){
    ar & dw2_min[i];
    ar & dw2_max[i];
    ar & dw2_fit[i];
  }
  n = get_nocctot();
  for(int i=0;i<n;i++){
    ar & occup_min[i];
    ar & occup_max[i];
    ar & occup_fit[i];
  }
  // Todo - these are part of the UI, and values will change as calcs are run
  //ar & chisqr;
  //ar & *norm;
  //ar & *quality;
}

template <class Archive>
void ROD::rod_fit::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<ROD::rod_calc>(*this);
  ar & scale_min;
  ar & scale_max;
  ar & scale_fit;
  ar & scale2_min;
  ar & scale2_max;
  ar & scale2_fit;
  ar & beta_min;
  ar & beta_max;
  ar & beta_fit;
  ar & sfrac_min;
  ar & sfrac_max;
  ar & sfrac_fit;
  int n = get_ndistot();
  dist_min = new float[n];
  dist_max = new float[n];
  dist_fit = new int[n];
  for(int i=0;i<n;i++){
    ar & dist_min[i];
    ar & dist_max[i];
    ar & dist_fit[i];
  }
  n = get_ndwtot();
  dw1_min = new float[n];
  dw1_max = new float[n];
  dw1_fit = new int[n];
  for(int i=0;i<n;i++){
    ar & dw1_min[i];
    ar & dw1_max[i];
    ar & dw1_fit[i];
  }
  n = get_ndwtot2();
  dw2_min = new float[n];
  dw2_max = new float[n];
  dw2_fit = new int[n];
  for(int i=0;i<n;i++){
    ar & dw2_min[i];
    ar & dw2_max[i];
    ar & dw2_fit[i];
  }
  n = get_nocctot();
  occup_min = new float[n];
  occup_max = new float[n];
  occup_fit = new int[n];
  for(int i=0;i<n;i++){
    ar & occup_min[i];
    ar & occup_max[i];
    ar & occup_fit[i];
  }
  // Todo - these are part of the UI, need to be more independent
  // so can't allocate here to recover and reload to UI
  //ar & chisqr;
  //ar & norm;
  //ar & quality;
}

BOOST_CLASS_EXPORT_GUID(ROD::rod_fit, "ROD::rod_fit")

#  ifdef DLV_EXPLICIT_TEMPLATES

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_fit>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_fit>;
template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_fit> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_fit> >;
template class boost::serialization::extended_type_info_typeid<ROD::rod_fit>;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::rod_fit> >;
template class boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_fit>;
template class boost::serialization::singleton<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_fit> >;
template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_fit>;
template class boost::serialization::singleton<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_fit> >;
template void ROD::rod_fit::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);
template void ROD::rod_fit::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int);
template class boost::serialization::singleton<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_fit> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_fit> >;
template void boost::archive::detail::instantiate_ptr_serialization<ROD::rod_fit>(ROD::rod_fit*, int, boost::archive::detail::adl_tag);
template ROD::rod_fit* boost::serialization::factory<ROD::rod_fit, 0>(std::va_list);
template ROD::rod_fit* boost::serialization::factory<ROD::rod_fit, 1>(std::va_list);
template ROD::rod_fit* boost::serialization::factory<ROD::rod_fit, 2>(std::va_list);
template ROD::rod_fit* boost::serialization::factory<ROD::rod_fit, 3>(std::va_list);
template ROD::rod_fit* boost::serialization::factory<ROD::rod_fit, 4>(std::va_list);

template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_fit, ROD::rod_calc>;
template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_fit, ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_fit, ROD::rod_calc> >;
template boost::serialization::detail::base_cast<ROD::rod_calc, ROD::rod_fit>::type& boost::serialization::base_object<ROD::rod_calc, ROD::rod_fit>(ROD::rod_fit&);
template ROD::rod_calc const* boost::serialization::smart_cast<ROD::rod_calc const*, ROD::rod_fit const*>(ROD::rod_fit const*);
template ROD::rod_fit const* boost::serialization::smart_cast<ROD::rod_fit const*, ROD::rod_calc const*>(ROD::rod_calc const*);
template boost::serialization::detail::base_cast<ROD::rod_calc, ROD::rod_fit const>::type& boost::serialization::base_object<ROD::rod_calc, ROD::rod_fit const>(ROD::rod_fit const&);

template void ROD::rod_fit::load<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);
template void ROD::rod_fit::save<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int) const;

#  endif // DLV_EXPLICIT_TEMPLATES

#endif // DLV_USES_SERIALIZE
