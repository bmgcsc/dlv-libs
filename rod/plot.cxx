#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/math_fns.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "data.hxx"
#include "plot.hxx"

ROD::rod_plot::rod_plot(DLV::model *m, const real_g att, const real_g sc,
			const real_g sc2,
			const real_g be, const real_g sfr,
			real_g dst[], real_g d1[], real_g d2[], real_g occ[],
			const int_g ndistt, const int_g ndwt,
			const int_g ndwt2, const int_g nocct,
			const bool fit_d, const bool exp_d,
			const real_g hr, const real_g kr,
			const real_g lst, const real_g le, const int_g n,
			const bool pl_bulk, const bool pl_surf,
			const bool pl_both, const bool pl_exp,
			const bool n_view)
  : ROD::rod_calc(m, att, sc, sc2, be, sfr, dst, d1, d2,
		  occ, ndistt, ndwt, ndwt2, nocct, fit_d, exp_d),
    hrod(hr), krod(kr), lstart(lst), lend(le), nl(n),
    plot_bulk(pl_bulk), plot_surf(pl_surf), plot_both(pl_both),
    plot_exp(pl_exp), new_view(n_view)
{
}

ROD::rod_ffac::rod_ffac(DLV::model *m, const real_g att, const real_g sc,
			const real_g sc2, const real_g be, const real_g sfr,
			real_g dst[], real_g d1[], real_g d2[], real_g occ[],
			const int_g ndistt, const int_g ndwt,
			const int_g ndwt2, const int_g nocct,
			const bool fit_d, const bool exp_d,
			const int_g sel_fs, real_g h_st,
			real_g k_st, const real_g h_e,
			const real_g k_e, const real_g h_stp,
			const real_g k_stp,
			const real_g l, const real_g mxq,
			const real_g s_f,
			const bool pl_calc, const bool pl_exp,
			const bool n_view)
  : ROD::rod_calc(m, att, sc, sc2, be, sfr,  dst, d1, d2,
		  occ, ndistt, ndwt, ndwt2, nocct, fit_d, exp_d),
    select_fs(sel_fs), h_start(h_st), k_start(k_st), h_end(h_e), k_end(k_e),
    h_step(h_stp), k_step(k_stp), l_value(l), maxq(mxq), scale_factor(s_f),
    plot_calc(pl_calc),
    plot_exp(pl_exp), new_view(n_view)
{
}

#ifdef ENABLE_ROD
#include "rod/rod.h"
#include "lsqfit.h"

ROD::int_g ROD::rod_plot::initialise_data(char message[], const int_g mlen)
{
  int_g ok;
  ok = ROD::rod_calc::initialise_data(message, mlen);
  if (ok != DLV_OK)
    return ok;
  real_g lstep;
  NL = nl;
  if (NL < 2) NL = 2;
  lstep = (lend-lstart)/(NL-1);
  for (int_g i = 0; i < NL; i++){
    HTH[i] = hrod;
    KTH[i] = krod;
    LTH[i] = lstart+lstep*i;
  }
  return ok;
}
ROD::int_g ROD::rod_ffac::initialise_data(char message[], const int_g mlen)
{
  int_g ok;
  ok = ROD::rod_calc::initialise_data(message, mlen);
  if (ok != DLV_OK)
    return ok;
  int_g nhsteps, nksteps;
  real_g h, k;
  if(select_fs==0){
    nhsteps = DLV::round((h_end-h_start)/h_step)+1;
    nksteps = DLV::round((k_end-k_start)/k_step)+1;
    NL = nhsteps * nksteps;
  }
  else if(select_fs==1){
    h_start = -1*DLV::round(maxq/h_step);
    k_start = -1*DLV::round(maxq/k_step);
    nhsteps = -2*h_start+1;
    nksteps = -2*k_start+1;
    NL = 0;
    for (int_g i = 0; i < nhsteps; i++)
      for (int_g j = 0; j < nksteps; j++){
	h = (i+1)*h_step;
	k = (j+1)*k_step;
	if(sqrt(h*h + k*k) < maxq+1.0E-5)
	  NL++;
      }
    NL = 4*NL + nhsteps + nksteps - 1;
  }
  else if(select_fs==2){
    NL = 0;
    for (int_g i=0; i<NDAT; i++)
      if(LDAT[i] == l_value)
	NL++;
  }

  if(select_fs==0 || select_fs==1){
    real_g hrod = h_start;
    real_g krod = k_start;
    int_g i = 0;
    for (int_g a=0; a<nhsteps; a++){
      for (int_g b=0; b<nksteps; b++){
	if((select_fs != 1) ||
	   (sqrt(real_g(hrod)*real_g(hrod) + real_g(krod)*real_g(krod)) < maxq + 1.0E-5)){
	  HTH[i] = hrod;
	  KTH[i] = krod;
	  LTH[i] = l_value;
	  i++;
	}
	krod += k_step;
      }
	hrod += h_step;
	krod = k_start;
    }
  }
  else if(select_fs==2){
    int_g j = 0;
    for (int_g i=0; i<NDAT; i++)
      if(LDAT[i] == l_value){
	HTH[j] = HDAT[i];
	KTH[j] = KDAT[i];
	LTH[j] = LDAT[i];
	j++;
      }
  }
  return ok;
}

ROD::int_g ROD::rod_plot::plot_rod(char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  if(!plot_surf&&!plot_both)
    NSURF = 0;
  if(!plot_bulk&&!plot_both)
    NBULK = 0;
  for (int_g i = 0; i < NL; i++)
    f_calc(HTH[i], KTH[i], LTH[i], ATTEN, LBRAGG,
    	   &FTH[0][i],&FTH[1][i], &FTH[2][i], &PHASE[i]);
  DLV::data_object *data = read_rod_data(this, message, mlen);
  if (data == 0) {
    strncpy(message, "Error reading rod data", mlen - 1);
    ok = DLV_ERROR;
  }
  else {
#ifdef ENABLE_DLV_GRAPHICS
    ok = render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
    if (ok == DLV_OK) {
      if(new_view){
	attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
	render_data(data);
#endif // ENABLE_DLV_GRAPHICS
      }
      else{
	DLV::operation *op = get_parent();
#ifdef ENABLE_DLV_GRAPHICS
	if(op != 0){
	  DLV::operation::get_current()->render_r(message, 256);
	}
#endif // ENABLE_DLV_GRAPHICS
	attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
	if(op != 0)
	  transfer_and_render_data(op, data);
	else
	  render_data(data);
#endif // ENABLE_DLV_GRAPHICS
      }
    }
  }
  return ok;
}

ROD::int_g ROD::rod_ffac::plot_ffactors(char message[], const int_g mlen)
{
  int_g ok = DLV_OK;
  if(plot_exp && select_fs !=2){
    strncpy(message,
	    "Can only plot experimental data for plot for all exp points",
	    mlen - 1);
    ok = DLV_ERROR;
    return ok;
  }
  if(plot_calc){
    for (int_g i=0; i<NL; i++)
      f_calc(HTH[i], KTH[i], LTH[i], ATTEN, LBRAGG,
	     &FTH[0][i],&FTH[1][i], &FTH[2][i], &PHASE[i]);
  }
  //plot graph
  real_g radius;
  if(select_fs==0|| select_fs ==1)
    if (k_step > h_step)
      radius = k_step/2.0;
    else
      radius = h_step/2.0;
  else
    radius = 0.5;

  if(scale_factor < 0.0){  //ie it has not been set yet
    int_g npts = NL;
    real_g max = 0.0;
    if(plot_calc){
      for (int_g i = 0; i < npts; i++)
	if( FTH[2][i] > max)
	  max = FTH[2][i];
    }
    if(plot_exp){
      int_g j = 0;
      for (int_g i=0; i<NDAT; i++){
	if(LDAT[i] == LTH[0])
	  if(FDAT[j] > max)
	    max = FDAT[j];
	j++;
      }
    }
    scale_factor = radius/max;
  }

  DLV::data_object *data = read_ffactors_data(this, scale_factor, message, mlen);
  if (data == 0) {
    strncpy(message, "Error reading F factors data", mlen - 1);
    ok = DLV_ERROR;
  }
  else {
#ifdef ENABLE_DLV_GRAPHICS
    ok = render_r(message, 256);
#endif // ENABLE_DLV_GRAPHICS
    if (ok == DLV_OK) {
      if(new_view){
	attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
	render_data(data);
#endif // ENABLE_DLV_GRAPHICS
      }
      else{
	DLV::operation *op = get_parent();
	attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
	if(op!=0)
	  transfer_and_render_data(op, data);
	else
	  render_data(data);
#endif // ENABLE_DLV_GRAPHICS
      }
    }
  }
  return ok;
}

DLV::rod2d_plot *ROD::rod_ffac::read_ffactors_data(DLV::operation *op,
						   const real_g scale_factor,
						   char message[],
						   const int_g mlen)
{
  DLV::rod2d_plot *data = 0;
  int_g npts = NL;
  int_g nproj;
  if(plot_calc && plot_exp)
    nproj = 3;
  else if(plot_calc || plot_exp)
    nproj = 2;
  else{
    strncpy(message, "Error nothing to plot", mlen - 1);
    return 0;
  }
  bool show_panel = false;
  DLV::string name = DLV::operation::get_current()->get_model_name();
  data = new DLV::rod2d_plot(program_name.c_str(), rod_ffac_label,
			     op, nproj, show_panel);
  data->set_title(name.c_str());
  real_g **plots = new_local_array1(real_g *, nproj);
  for (int_g i = 0; i < nproj; i++)
    plots[i] = new real_g[npts];
  real_g *grid = new real_g[npts];
  for (int_g i = 0; i < npts; i++) {
    grid[i] = HTH[i];
    plots[0][i] = KTH[i];
  }
  data->set_grid(grid, npts, false);
  data->add_plot(plots[0], 0, "Theory", true);
  if(plot_calc){
    for (int_g i = 0; i < npts; i++) {
      plots[1][i] = FTH[2][i]*scale_factor;
      if(plots[1][i] < 0.001)
	plots[1][i] = 0.001;
    }
    data->add_plot(plots[1], 1, "Experiment", true);
  }
  if(plot_exp){
    int_g j = 0;
    int_g k = nproj-1;
    for (int_g i = 0; i < NDAT; i++) {
      if(LDAT[i] == LTH[0]){
	plots[k][j] = FDAT[i]*scale_factor;
	if(plots[k][j] < 0.001)
	  plots[k][j] = 0.001;
	j++;
      }
    }
    data->add_plot(plots[k], k, "", true);
  }
  delete_local_array(plots);
  data->set_xaxis_label("Diffraction index h");
  data->set_yaxis_label("Diffraction index k");
  return data;
}

DLV::rod1d_plot
*ROD::rod_plot::read_rod_data(DLV::operation *op,
			      char message[], const int_g mlen)
{
  DLV::rod1d_plot *data = 0;
  int_g npts = NL;
  int_g npts2 = 0;
  if (plot_exp){
    for (int_g i = 0; i < NDAT; i++)
      if(HDAT[i]== HTH[0] && KDAT[i] == KTH[0] &&
	 LDAT[i] < LTH[npts-1] &&  LDAT[i] > LTH[0])
	npts2++;
  }
  int_g nproj = int(plot_bulk) +  int(plot_surf)
    + int(plot_both) +  int(plot_exp);
  bool plot_types[4];
  plot_types[0] = plot_bulk;
  plot_types[1] = plot_surf;
  plot_types[2] = plot_both;
  plot_types[3] = plot_exp;
  DLV::string plot_names[4];
  plot_names[0] = "Bulk";
  plot_names[1] = "Surface";
  plot_names[2] = "Combined";
  plot_names[3] = "Experiment";
  DLV::string label[4];
  int_g start = 0;
  for (int_g i=0; i<nproj; i++){
    for (int_g j=start; j<4; j++){
      if(plot_types[j]){
	label[i] = plot_names[j];
	start = j+1;
	break;
      }
    }
  }
  DLV::string name = DLV::operation::get_current()->get_model_name();
  int_g ngrids = 1;
  int_g max_pts = npts;
  if(plot_exp && nproj == 1 && npts2 ==0){
    strncpy(message, "Error no experimental data to plot", mlen - 1);
    return 0;
}
  if((plot_exp) && (nproj > 1))
    ngrids = 2;
  max_pts = npts;
  if((npts2 > max_pts) && (plot_exp) )
    max_pts = npts2;
  bool show_panel = false;
  data = new DLV::rod1d_plot(program_name.c_str(),  rod_plot_label, op,
			    nproj, ngrids, max_pts, show_panel);
  if(data ==0){
    strncpy(message, "Error no data available", mlen - 1);
    return data;
  }
  data->set_title(name.c_str());
   //Add grid data
  int_g grid_count = 0;
  if (plot_bulk || plot_surf || plot_both){
    real_g *grid = new real_g[npts];
    for (int_g i = 0; i < npts; i++)
      grid[i] = LTH[i];
    data->add_grid(grid, npts, grid_count);
    grid_count++;
    delete[] grid;
  }
  if (plot_exp){
    real_g *grid = new real_g[npts2];
    int_g j = 0;
    for (int_g i = 0; i < NDAT; i++)
      if(HDAT[i]== HTH[0] && KDAT[i] == KTH[0] &&
	 LDAT[i] < LTH[npts-1] &&  LDAT[i] > LTH[0] ){
	grid[j] = LDAT[i];
	j++;
      }
    data->add_grid(grid, npts2, grid_count);
    delete[] grid;
  }
  // add plot data
 real_g **plots = new_local_array1(real_g *, nproj);
  for (int_g i = 0; i < nproj; i++)
    plots[i] = new real_g[max_pts];
  for (int_g k = 0; k < nproj; k++){
    for (int_g m = k; m < 4; m++){
      if (label[k] == plot_names[m]){
	if(m<3)
	  for (int_g i = 0; i < npts; i++)
	    plots[k][i] = FTH[m][i];
	else{
	  int_g j = 0;
	  for (int_g i = 0; i < NDAT; i++)
	    if(HDAT[i]== HTH[0] && KDAT[i] == KTH[0] &&
	       LDAT[i] < LTH[npts-1] &&  LDAT[i] > LTH[0]){
	      plots[k][j] = FDAT[i];
	      j++;
	    }
	}
	break;
      }
    }
  }
  for (int_g i = 0; i < nproj; i++) {
    grid_count = 0;
    if(label[i] == "Experiment" && nproj > 1)
      grid_count = 1;
    data->add_plot(plots[i], i, grid_count, label[i].c_str(), true);
  }
  delete_local_array(plots);
  data->set_xaxis_label("Diffraction index l");
  return data;
}

#else // ENABLE_ROD

ROD::int_g ROD::rod_plot::plot_rod(char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  int_g ok = DLV_ERROR;
  return ok;
}

ROD::int_g ROD::rod_ffac::plot_ffactors(char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  int_g ok = DLV_ERROR;
  return ok;
}

DLV::rod1d_plot
*ROD::rod_plot::read_rod_data(DLV::operation *op,
			      char message[], const int_g mlen)
{
  strncpy(message, "DLV NOT COMPILED WITH ENABLE_ROD FLAG", mlen - 1);
  return 0;
}
#endif // ENABLE_ROD

DLV::operation *ROD::rod_plot::plot1D_parent = 0;
DLV::operation *ROD::rod_ffac::plot2D_parent = 0;

DLV::string ROD::rod_plot::get_name() const
{
  return ("ROD 1D PLOT");
}
DLV::string ROD::rod_ffac::get_name() const
{
  return ("ROD 2D PLOT");
}

void ROD::rod_plot::save_parent(DLV::operation *op)
{
  plot1D_parent = op;
}

DLV::operation *ROD::rod_plot::get_parent()
{
  return plot1D_parent;
}

void ROD::rod_ffac::save_parent(DLV::operation *op)
{
  plot2D_parent = op;
}

DLV::operation *ROD::rod_ffac::get_parent()
{
  return plot2D_parent;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ROD::rod_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)ROD::rod_plot(0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0,
			    0, 0, 0, false, false, 0.0, 0.0, 0.0, 0.0,
			    0, false, false, false, false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, ROD::rod_ffac *t,
				    const unsigned int file_version)
    {
      ::new(t)ROD::rod_ffac(0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0,
			    0, 0, 0, false, false, 0, 0.0, 0.0, 0.0, 0.0,
			    0.0, 0.0, 0.0, 0.0, 0.0, false, false, false);
    }

  }
}

template <class Archive>
void ROD::rod_plot::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<ROD::rod_calc>(*this);
  ar & hrod;
  ar & krod;
  ar & lstart;
  ar & lend;
  ar & nl;
  ar & plot_bulk;
  ar & plot_surf;
  ar & plot_both;
  ar & plot_exp;
  ar & new_view;
}

template <class Archive>
void ROD::rod_ffac::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<ROD::rod_calc>(*this);
  ar & select_fs;
  ar & h_start;
  ar & k_start;
  ar & h_end;
  ar & k_end;
  ar & h_step;
  ar & k_step;
  ar & l_value;
  ar & maxq;
  ar & scale_factor;
  ar & plot_calc;
  ar & plot_exp;
  ar & new_view;
}

BOOST_CLASS_EXPORT_GUID(ROD::rod_plot, "ROD::rod_plot")
BOOST_CLASS_EXPORT_GUID(ROD::rod_ffac, "ROD::rod_ffac")

#  ifdef DLV_EXPLICIT_TEMPLATES

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_plot>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_plot>;
template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_plot> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_plot> >;
template class boost::serialization::extended_type_info_typeid<ROD::rod_plot>;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::rod_plot> >;
template class boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_plot>;
template class boost::serialization::singleton<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_plot> >;
template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_plot>;
template class boost::serialization::singleton<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_plot> >;
template void ROD::rod_plot::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);
template void ROD::rod_plot::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int);
template class boost::serialization::singleton<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_plot> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_plot> >;
template void boost::archive::detail::instantiate_ptr_serialization<ROD::rod_plot>(ROD::rod_plot*, int, boost::archive::detail::adl_tag);
template ROD::rod_plot* boost::serialization::factory<ROD::rod_plot, 0>(std::va_list);
template ROD::rod_plot* boost::serialization::factory<ROD::rod_plot, 1>(std::va_list);
template ROD::rod_plot* boost::serialization::factory<ROD::rod_plot, 2>(std::va_list);
template ROD::rod_plot* boost::serialization::factory<ROD::rod_plot, 3>(std::va_list);
template ROD::rod_plot* boost::serialization::factory<ROD::rod_plot, 4>(std::va_list);

template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_plot, ROD::rod_calc>;
template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_plot, ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_plot, ROD::rod_calc> >;
template boost::serialization::detail::base_cast<ROD::rod_calc, ROD::rod_plot>::type& boost::serialization::base_object<ROD::rod_calc, ROD::rod_plot>(ROD::rod_plot&);
template ROD::rod_calc const* boost::serialization::smart_cast<ROD::rod_calc const*, ROD::rod_plot const*>(ROD::rod_plot const*);
template ROD::rod_plot const* boost::serialization::smart_cast<ROD::rod_plot const*, ROD::rod_calc const*>(ROD::rod_calc const*);

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_ffac>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_ffac>;
template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_ffac> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::rod_ffac> >;
template class boost::serialization::extended_type_info_typeid<ROD::rod_ffac>;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::rod_ffac> >;
template class boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_ffac>;
template class boost::serialization::singleton<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::rod_ffac> >;
template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_ffac>;
template class boost::serialization::singleton<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::rod_ffac> >;
template void ROD::rod_ffac::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);
template void ROD::rod_ffac::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int);
template class boost::serialization::singleton<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_ffac> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::extra_detail::guid_initializer<ROD::rod_ffac> >;
template void boost::archive::detail::instantiate_ptr_serialization<ROD::rod_ffac>(ROD::rod_ffac*, int, boost::archive::detail::adl_tag);
template ROD::rod_ffac* boost::serialization::factory<ROD::rod_ffac, 0>(std::va_list);
template ROD::rod_ffac* boost::serialization::factory<ROD::rod_ffac, 1>(std::va_list);
template ROD::rod_ffac* boost::serialization::factory<ROD::rod_ffac, 2>(std::va_list);
template ROD::rod_ffac* boost::serialization::factory<ROD::rod_ffac, 3>(std::va_list);
template ROD::rod_ffac* boost::serialization::factory<ROD::rod_ffac, 4>(std::va_list);

template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_ffac, ROD::rod_calc>;
template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_ffac, ROD::rod_calc> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::rod_ffac, ROD::rod_calc> >;
template boost::serialization::detail::base_cast<ROD::rod_calc, ROD::rod_ffac>::type& boost::serialization::base_object<ROD::rod_calc, ROD::rod_ffac>(ROD::rod_ffac&);
template ROD::rod_calc const* boost::serialization::smart_cast<ROD::rod_calc const*, ROD::rod_ffac const*>(ROD::rod_ffac const*);
template ROD::rod_ffac const* boost::serialization::smart_cast<ROD::rod_ffac const*, ROD::rod_calc const*>(ROD::rod_calc const*);

#  endif // DLV_EXPLICIT_TEMPLATES

#endif // DLV_USES_SERIALIZE
