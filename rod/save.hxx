
#ifndef ROD_SAVE_STRUCTURE
#define ROD_SAVE_STRUCTURE

namespace ROD {

  using DLV::int_g;

  class save_structure : public DLV::save_model_op {
  public:
    static DLVreturn_type create(const char filename[], const int_g file_type,
				 char message[], const int_g mlen);
    static DLVreturn_type buffer(char buffer[][256], const int_g file_type,
				 int_g &nlines,
				 char message[], const int_g mlen);
    // public for serialization
    save_structure(const char file[]);

  protected:
    DLV::string get_name() const;
    DLVreturn_type write_bul(const char filename[],
			     char message[], const int_g mlen);
    DLVreturn_type write_sur(const char filename[],
			     char message[], const int_g mlen);
    DLVreturn_type write_fit(const char filename[],
			     char message[], const int_g mlen);
    DLVreturn_type write_par(const char filename[],
			     char message[], const int_g mlen);
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

inline ROD::save_structure::save_structure(const char file[])
  : save_model_op(file)
{
}

#endif // ROD_SAVE_STRUCTURE
