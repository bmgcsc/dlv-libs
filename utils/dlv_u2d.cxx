
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
  // Todo - improve error handling and messages.
  if (argc != 2) {
    fprintf(stderr, "Usage: dlv_d2u filename\n");
    exit(1);
  }
  struct _stat filedata;
  if (_stat(argv[1], &filedata) == -1)
    exit(1);
  int length = filedata.st_size;
  if (length > 0) {
    FILE *input = fopen(argv[1], "rb");
    if (input != NULL) {
      char *buff = new char[length];
      if (fread(buff, 1, length, input) != length)
        exit(1);
      fclose(input);
      FILE *output = fopen(argv[1], "wb");
      if (output != NULL) {
        for (int i = 0; i < length; i++) {
          if (buff[i] == 10)
            fputc(13, output);
          fputc(buff[i], output);
	}
        fclose(output);
      }
      delete [] buff;
    }
  }
  return 0;
}
