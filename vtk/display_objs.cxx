
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"
#include "../graphics/display_objs.hxx"

DLV::display_obj::~display_obj()
{
}

void DLV::display_obj::transform_point(float p[3], const float mat[4][4],
				       const float xlate[3],
				       const float centre[3])
{
  // Best guess based on AVS V experiments.
  float v[3];
  v[0] = (((mat[0][0] * (p[0] - centre[0]) + mat[1][0] * (p[1] - centre[1])
	    + mat[2][0] * (p[2] - centre[2])) * mat[3][3]) + xlate[0]);
  v[1] = (((mat[0][1] * (p[0] - centre[0]) + mat[1][1] * (p[1] - centre[1])
	    + mat[2][1] * (p[2] - centre[2])) * mat[3][3]) + xlate[1]);
  v[2] = (((mat[0][2] * (p[0] - centre[0]) + mat[1][2] * (p[1] - centre[1])
	    + mat[2][2] * (p[2] - centre[2])) * mat[3][3]) + xlate[2]);
  p[0] = v[0];
  p[1] = v[1];
  p[2] = v[2];
}

void DLV::display_obj::display_3D()
{
// dummy function
}

void DLV::display_obj::undisplay_3D()
{
// dummy function
}

void DLV::display_obj::update_display_list(const render_parent *parent)
{
}

void DLV::display_obj::update_display_list(const render_parent *parent,
					   const string name)
{
}

DLV::display_obj *
DLV::display_obj::create_text_display(const render_parent *parent,
				      const class drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_real_display(const render_parent *parent,
				      const class drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_file_view(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_text_view(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_bond_text(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_atom_text(const render_parent *parent,
				   const class drawable_obj *draw,
				   const int component, const string name,
				   const int index, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_atom_vectors(const render_parent *parent,
				      const class drawable_obj *draw,
				      const int component, const string name,
				      const int index, char message[],
				      const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_phonon_anim(const render_parent *parent,
				     const class drawable_obj *draw,
				     const string name, const int index,
				     char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_isosurface(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_iso_wavefn(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const int ndata,
				    const string name, const int index,
				    const int sa, const int sb, const int sc,
				    char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_cells(const render_parent *parent,
			       const drawable_obj *draw,
			       const bool is_periodic, const bool kspace,
			       const int component, const string name,
			       const int index, char message[],
			       const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_spheres(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_volume(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_hedgehog(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool is_periodic, const bool kspace,
				  const int component, const string name,
				  const int index, char message[],
				  const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_streamlines(const render_parent *parent,
				     const drawable_obj *draw,
				     const bool is_periodic, const bool kspace,
				     const int component, const string name,
				     const int index, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_ag_contour(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_contour(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_pn_contour(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_solid_ct(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool is_periodic, const bool kspace,
				  const int component, const string name,
				  const int index, char message[],
				  const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_shaded_ct(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool is_periodic, const bool kspace,
				   const int component, const string name,
				   const int index, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_surface_plot(const render_parent *parent,
				      const drawable_obj *draw,
				      const bool is_periodic,
				      const bool kspace,
				      const int component, const string name,
				      const int index, char message[],
				      const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_dos_plot(const render_parent *parent,
				  const class drawable_obj *draw,
				  const string name, const int index,
				  const string xaxis, const string yaxis,
				  const int ngraphs, const int type,
				  char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_panel_plot(const render_parent *parent,
				    const class drawable_obj *draw,
				    const string name, const int index,
				    const string xaxis, const string yaxis,
				    const int ngraphs, char message[],
				    const int mlen, const bool split_spins)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_3D_region(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_sphere_obj(const render_parent *parent,
				    const drawable_obj *draw,
				    const string name, const int index,
				    const bool kspace, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_plane_obj(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_line_obj(const render_parent *parent,
				  const drawable_obj *draw,
				  const string name, const int index,
				  const bool kspace, char message[],
				  const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_point_obj(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_wulff_region(const render_parent *parent,
				      const drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  return nullptr;
}

DLV::display_obj *
DLV::display_obj::create_leed_pattern(const render_parent *parent,
				      const drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  return nullptr;
}

void DLV::display_obj::update_streamlines(const class drawable_obj *draw,
					  char message[], const int mlen)
{
  // Dummy
}

bool DLV::display_obj::attach_editor(const bool v, const toolkit_obj &t,
				     float matrix[4][4], float translate[3],
				     float centre[3], char message[],
				     const int mlen) const
{
  return false;
}

void DLV::display_obj::update_data(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, 
				   const string xaxis, const string yaxis,
				   const int ngraphs, const bool visible,
				   char message[], const int mlen)
{
}

void DLV::display_obj::turn_off_visible()
{
}

#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::display_obj::save(Archive &ar, const unsigned int version) const
{
  ar & name;
}

template <class Archive>
void DLV::display_obj::load(Archive &ar, const unsigned int version)
{
  ar & name;
  rendered = false;
}


//BOOST_CLASS_VERSION(DLV::plot_obj, 2)
//BOOST_CLASS_VERSION(DLV::ag_contours, 1)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::display_obj)

#endif // DLV_USES_SERIALIZE
