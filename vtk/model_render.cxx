
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_AVS_GRAPHICS)
#    error "This graphics interface should not be built when using the AVS/Express environment"
#  elif !defined(DLV_USES_VTK_GRAPHICS)
#    error "DLV_USES_VTK_GRAPHICS should also be defined when building this graphics interface"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS and DLV_USES_VTK_GRAPHICS should be defined when building this graphics interface"
#endif // GRAPHICS check

#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/model_render.hxx"

#include <vtkGlyph3DMapper.h>
#include <vtkPointData.h>
#include <vtkLODActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkSphereSource.h>
#include <vtkNamedColors.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkDataObject.h>
#include <vtkLine.h>
#include <vtkRenderer.h>
#include <vtkTubeFilter.h>
#include <vtkCellData.h>

#include "vtk_wrappers.hxx"
#include "model_render.hxx"
#include "render_base.hxx"

static float point_cloud = 300000.0;

DLV::model_display_obj::~model_display_obj()
{
}

template <class render_t>
DLV::rspace_model_templ<render_t>::~rspace_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::kspace_model_templ<render_t>::~kspace_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::outline_model_templ<render_t>::~outline_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::shell_model_templ<render_t>::~shell_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::rspace_model_templ<render_t>::rspace_model_templ(render_parent *p)
  : lattice_attached(false), lattice_labels(false), opaque_atoms(false),
    transparent_atoms(false), edit_atoms(false), opaque_line_bonds(false),
    transparent_line_bonds(false), edit_line_bonds(false),
    opaque_tube_bonds(false), transparent_tube_bonds(false),
    edit_tube_bonds(false), polyhedra(false)
{
  obj = new render_t();
  render_atoms *t = dynamic_cast<render_atoms *>(p);
  obj->options = t->get_atom_obj();
}

template <class render_t>
DLV::kspace_model_templ<render_t>::kspace_model_templ(render_parent *p)
  : lattice_attached(false)
{
  obj = nullptr;
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::copy_settings(const rspace_model_templ *parent)
{
}

template <class render_t>
DLV::toolkit_obj DLV::rspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
DLV::toolkit_obj DLV::kspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::deactivate_atom_flags()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::activate_atom_flags(const string label)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::toggle_atom_flags(const string label)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = obj->options->lattice.draw_lattice;
  labels = obj->options->lattice.label_lattice;
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = false; //obj->options->lattice.draw_lattice;
  labels = false; //obj->options->lattice.label_lattice;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice(float ipoints[][3],
						    float fpoints[][3],
						    const int n)
{
  if (obj->options->lat_points == nullptr)
    obj->options->lat_points = vtkPoints::New();
  obj->options->lat_points->SetNumberOfPoints(2 * n);
  for (int i = 0; i < n; i++) {
    obj->options->lat_points->SetPoint(i, ipoints[i][0], ipoints[i][1], ipoints[i][2]);
    obj->options->lat_points->SetPoint(n + i, fpoints[i][0], fpoints[i][1], fpoints[i][2]);
  }
  if (obj->options->lat_cells == nullptr)
    obj->options->lat_cells = vtkCellArray::New();
  else
    obj->options->lat_cells->Reset();
  for (int i = 0; i < n; i++) {
    vtkNew<vtkLine> line;
    line->GetPointIds()->SetId(0, i);
    line->GetPointIds()->SetId(1, n + i);
    obj->options->lat_cells->InsertNextCell(line);
  }
  if (obj->options->lat_lines == nullptr) {
    obj->options->lat_lines = vtkPolyData::New();
    obj->options->lat_lines->SetPoints(obj->options->lat_points);
    obj->options->lat_lines->SetLines(obj->options->lat_cells);
  }
  if (obj->options->latMapper == nullptr) {
    obj->options->latMapper = vtkPolyDataMapper::New();
    obj->options->latMapper->SetInputData(obj->options->lat_lines);
  }
  vtkNew<vtkNamedColors> colors;
  if (obj->options->atom_lattice == nullptr) {
    obj->options->atom_lattice = vtkActor::New();
    obj->options->atom_lattice->SetMapper(obj->options->latMapper);
    obj->options->atom_lattice->GetProperty()->SetLineWidth(obj->options->lines.line_width);
    obj->options->atom_lattice->GetProperty()->SetColor(colors->GetColor3d("Black").GetData());
  }
  if (!lattice_attached) {
    //std::cerr << "Attach the lattice\n";
    obj->options->get_r_render()->AddActor(obj->options->atom_lattice);
    lattice_attached = true;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice_labels(float points[][3],
							   const char *labels[],
							   const int n)
{
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::set_lattice(float vertices[][3],
						    const int nv,
						    long connects[],
						    const long nc)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice()
{
  if (lattice_attached) {
    obj->options->get_r_render()->RemoveActor(obj->options->atom_lattice);
    lattice_attached = false;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice_labels()
{
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::detach_lattice()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_atom_data(bool &draw, float &scale,
						      int &select,
						      bool &use_flags,
						      int &flag_type) const
{
  draw = obj->options->atoms.draw_atoms;
  scale = (float)obj->options->atoms.atom_scale_percent / 100.0;
  select = (int)obj->options->atoms.selections_type;
  // Todo?
  use_flags = false;
  flag_type = 0;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_atom_group_index() const
{
  return 0;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_selection_symmetry() const
{
  return 0;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_selection_symmetry(const int s)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
  if (nframes > 1) {
    std::cerr << "Ignoring frames " << nframes << std::endl;
  } else {
    double resolution = std::sqrt(point_cloud / natoms);
    if (resolution > 20) {
      resolution = 20;
    }
    if (resolution < 4) {
      resolution = 4;
    }
    //std::cout << "Resolution is: " << resolution << std::endl;
    if (obj->options->sphere == nullptr) {
      vtkSphereSource *sphere = vtkSphereSource::New();
      sphere->SetCenter(0, 0, 0);
      sphere->SetRadius(1);
      sphere->SetThetaResolution(static_cast<int>(resolution));
      sphere->SetPhiResolution(static_cast<int>(resolution));
      obj->options->sphere = sphere;
    }
    // This is roughly based on vtkMoleculeBase
    if (obj->options->colours == nullptr)
      obj->options->colours = vtkUnsignedCharArray::New();
    else
      obj->options->colours->Reset();
    obj->options->colours->SetNumberOfComponents(3);
    obj->options->colours->SetNumberOfTuples(natoms);
    //obj->options->colours->Allocate(3 * natoms);
    float rgb[3];
    obj->options->colours->SetName("colours");
    for (int i = 0; i < natoms; i++) {
      rgb[0] = colours[i][0] * 255;
      rgb[1] = colours[i][1] * 255;
      rgb[2] = colours[i][2] * 255;
      obj->options->colours->SetTuple(i, rgb);
    }
    float scale = float(obj->options->atoms.atom_scale_percent) / 100.0;
    if (obj->options->radii == nullptr)
      obj->options->radii = vtkFloatArray::New();
    else
      obj->options->radii->Reset();
    obj->options->radii->SetNumberOfComponents(3);
    obj->options->radii->SetNumberOfTuples(natoms);
    // if its scaling by the vector magnitude then to get a from sqrt(a^2 + a^2 + a^2)
    // requires an adjustment of sqrt(3) sqrt(a) so 3a'^2 = a
    scale = std::sqrt(scale / 3.0);
    for (int i = 0; i < natoms; i++) {
      float r[3];
      float rsqrt = std::sqrt(radii[i]) * scale;
      r[0] = rsqrt;
      r[1] = rsqrt;
      r[2] = rsqrt;
      obj->options->radii->SetTuple(i, r);
    }
    obj->options->radii->SetName("radii");
    if (obj->options->points == nullptr)
      obj->options->points = vtkPoints::New();
    obj->options->points->SetNumberOfPoints(natoms);
    double xsum = 0.0;
    double ysum = 0.0;
    double zsum = 0.0;
    for (int i = 0; i < natoms; i++) {
      obj->options->points->SetPoint(i, coords[i][0], coords[i][1], coords[i][2]);
      xsum += coords[i][0];
      ysum += coords[i][1];
      zsum += coords[i][2];
    }
    xsum /= double(natoms);
    ysum /= double(natoms);
    zsum /= double(natoms);
    //obj->options->points->SetScalars(obj->options->colours);
    //obj->options->points->SetVectors(obj->options->radii);
    if (obj->options->poly == nullptr) {
      obj->options->poly = vtkPolyData::New();
      obj->options->poly->SetPoints(obj->options->points);
      obj->options->poly->GetPointData()->SetScalars(obj->options->colours);
      obj->options->poly->GetPointData()->SetVectors(obj->options->radii);
    }
    if (obj->options->glyph == nullptr) {
      obj->options->glyph = vtkGlyph3D::New();
      obj->options->glyph->SetInputData(obj->options->poly);
      obj->options->glyph->SetOrient(1);
      //obj->options->glyph->SetInputArrayToProcess(0, 0, 0,
      //						 vtkDataObject::FIELD_ASSOCIATION_POINTS,
      //					 "colours");
      //obj->options->glyph->SetInputArrayToProcess(1, 0, 0,
      //					 vtkDataObject::FIELD_ASSOCIATION_POINTS,
      //					 "radii");
      obj->options->glyph->SetColorMode(VTK_COLOR_BY_SCALAR);
      obj->options->glyph->SetScaleMode(VTK_SCALE_BY_VECTOR);
      obj->options->glyph->SetSourceConnection(obj->options->sphere->GetOutputPort());
    }
    //obj->options->glyph->SetScaleFactor(scale);

    if (obj->options->atomMapper == nullptr) {
      obj->options->atomMapper = vtkPolyDataMapper::New();
      obj->options->atomMapper->SetInputConnection(obj->options->glyph->GetOutputPort());
      obj->options->atomMapper->UseLookupTableScalarRangeOff();
      obj->options->atomMapper->ScalarVisibilityOn();
      obj->options->atomMapper->SetScalarModeToDefault();
    } //else
    //atomMapper->Update();

    if (obj->options->opaque_atoms == nullptr) {
      obj->options->opaque_atoms = vtkLODActor::New();
      obj->options->opaque_atoms->SetMapper(obj->options->atomMapper);
      obj->options->opaque_atoms->GetProperty()->SetRepresentationToSurface();
      obj->options->opaque_atoms->GetProperty()->SetInterpolationToGouraud();
      obj->options->opaque_atoms->GetProperty()->SetAmbient(0.1);
      obj->options->opaque_atoms->GetProperty()->SetDiffuse(0.7);
      obj->options->opaque_atoms->GetProperty()->SetSpecular(0.5);
      obj->options->opaque_atoms->GetProperty()->SetSpecularPower(80);
      vtkNew<vtkNamedColors> vtkcolors;
      obj->options->opaque_atoms->GetProperty()->SetSpecularColor(vtkcolors->GetColor3d("White").GetData());
      obj->options->opaque_atoms->SetNumberOfCloudPoints(30000);
      //interactor->Initialize();
      //interactor->Start();
    }
    // don't do if don't want to reset, but doesn't centre the viewer...
    obj->options->opaque_atoms->SetOrigin(xsum, ysum, zsum);
    if (!opaque_atoms) {
      obj->options->get_r_render()->AddActor(obj->options->opaque_atoms);
      opaque_atoms = true;
    }
  }
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_atoms(float coords[][3],
							float colours[][3],
							float radii[],
							const int natoms,
							const int nframes)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_bond_data(bool &draw,
						      bool &polyhedra,
						      float &overlap,
						      bool &tubes,
						      float &radius,
						      int &subdiv) const
{
  draw = obj->options->bond_data.draw_bonds;
  polyhedra = false; //obj->options->bond_data.draw_polyhedra;
  overlap = obj->options->bond_data.bond_overlap;
  // vtk draw is based on lines
  tubes = false;//(obj->options.bond_data.bond_draw_type == CCP3::render_atoms::bond_t::bond_tubes);
  radius = obj->options->bond_data.bond_radius;
  subdiv = obj->options->bond_data.bond_subdivisions;
}

// For compatibility with new VTK generic data arrays.
#ifdef vtkGenericDataArray_h
#define InsertNextTupleValue InsertNextTypedTuple
#endif

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
  if (nframes > 1) {
    std::cerr << "Ignoring frames " << nframes << std::endl;
  } else {
    // make lines and then use as input to tube filter, unlike lattice needs colours
    int n = nbonds / 2;
    if (obj->options->bond_points == nullptr)
      obj->options->bond_points = vtkPoints::New();
    // everything is already doubled with start an end as sequential points
    obj->options->bond_points->SetNumberOfPoints(nbonds);
    for (int i = 0; i < nbonds; i++)
      obj->options->bond_points->SetPoint(i, coords[i][0], coords[i][1], coords[i][2]);
    if (obj->options->bond_cells == nullptr)
      obj->options->bond_cells = vtkCellArray::New();
    else
      obj->options->bond_cells->Reset();
    for (int i = 0; i < n; i++) {
      vtkNew<vtkLine> line;
      line->GetPointIds()->SetId(0, 2 * i);
      line->GetPointIds()->SetId(1, 2 * i + 1);
      obj->options->bond_cells->InsertNextCell(line);
    }
    if (obj->options->bond_colours == nullptr)
      obj->options->bond_colours = vtkUnsignedCharArray::New();
    else
      obj->options->bond_colours->Reset();
    obj->options->bond_colours->SetNumberOfComponents(3);
    obj->options->bond_colours->SetNumberOfTuples(n);
    float rgb[3];
    for (int i = 0; i < n; i++) {
      rgb[0] = colours[2 * i][0] * 255;
      rgb[1] = colours[2 * i][1] * 255;
      rgb[2] = colours[2 * i][2] * 255;
      obj->options->bond_colours->SetTuple(i, rgb);
    }
    if (obj->options->bond_lines == nullptr) {
      obj->options->bond_lines = vtkPolyData::New();
      obj->options->bond_lines->SetPoints(obj->options->bond_points);
      obj->options->bond_lines->SetLines(obj->options->bond_cells);
      obj->options->bond_lines->GetCellData()->SetScalars(obj->options->bond_colours);
    }
    if (obj->options->bond_data.bond_draw_type == CCP3::render_atoms::bond_t::bond_tubes) {
      if (obj->options->bond_tubes == nullptr) {
	obj->options->bond_tubes = vtkTubeFilter::New();
	obj->options->bond_tubes->SetInputData(obj->options->bond_lines);
	obj->options->bond_tubes->SetNumberOfSides(obj->options->bond_data.bond_subdivisions);
	obj->options->bond_tubes->CappingOff();
	obj->options->bond_tubes->SetRadius(obj->options->bond_data.bond_radius);
	obj->options->bond_tubes->SetVaryRadius(0);
      }
      if (obj->options->tubebondMapper == nullptr) {
	obj->options->tubebondMapper = vtkPolyDataMapper::New();
	obj->options->tubebondMapper->SetInputConnection(obj->options->bond_tubes->GetOutputPort());
      }
      vtkNew<vtkNamedColors> colors;
      if (obj->options->bond_opaque_tubes == nullptr) {
	obj->options->bond_opaque_tubes = vtkLODActor::New();
	obj->options->bond_opaque_tubes->SetMapper(obj->options->tubebondMapper);
	obj->options->bond_opaque_tubes->GetProperty()->SetRepresentationToSurface();
	obj->options->bond_opaque_tubes->GetProperty()->SetInterpolationToGouraud();
	obj->options->bond_opaque_tubes->GetProperty()->SetAmbient(0.1);
	obj->options->bond_opaque_tubes->GetProperty()->SetDiffuse(0.7);
	obj->options->bond_opaque_tubes->GetProperty()->SetSpecular(0.5);
	obj->options->bond_opaque_tubes->GetProperty()->SetSpecularPower(80);
	obj->options->bond_opaque_tubes->GetProperty()->SetSpecularColor(colors->GetColor3d("White").GetData());
      }
      if (!opaque_tube_bonds) {
	if (opaque_line_bonds)
	  detach_opaque_bonds();
	obj->options->get_r_render()->AddActor(obj->options->bond_opaque_tubes);
	opaque_tube_bonds = true;
      }
    } else {
      if (obj->options->linebondMapper == nullptr) {
	obj->options->linebondMapper = vtkPolyDataMapper::New();
	obj->options->linebondMapper->SetInputData(obj->options->bond_lines);
      }
      if (obj->options->bond_opaque_lines == nullptr) {
	obj->options->bond_opaque_lines = vtkActor::New();
	obj->options->bond_opaque_lines->SetMapper(obj->options->linebondMapper);
	obj->options->bond_opaque_lines->GetProperty()->SetLineWidth(4);
      }
      if (!opaque_line_bonds) {
	if (opaque_tube_bonds)
	  detach_opaque_bonds();
	obj->options->get_r_render()->AddActor(obj->options->bond_opaque_lines);
	opaque_line_bonds = true;
      }
    }
  }
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_bonds(float coords[][3],
							float colours[][3],
							const long nbonds,
							long connects[],
							const long nc,
							const int nframes,
							const bool tubes,
							const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_line_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_tube_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_bonds()
{
  if (opaque_line_bonds) {
    obj->options->get_r_render()->RemoveActor(obj->options->bond_opaque_lines);
    opaque_line_bonds = false;
  } else if (opaque_tube_bonds) {
    obj->options->get_r_render()->RemoveActor(obj->options->bond_opaque_tubes);
    opaque_tube_bonds = false;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_bonds()
{
  detach_opaque_bonds();
  detach_transparent_bonds();
  detach_edit_bonds();
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_polyhedra(float coords[][3],
						       float colours[][3],
						       const long nverts,
						       long connects[],
						       const long nc,
						       const int nframes,
						       const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_polyhedra()
{
}

template <class render_t>
DLV::outline_model_templ<render_t>::outline_model_templ(render_parent *p)
  : data_set(false)
{
  obj = nullptr;
}

template <class render_t>
DLV::toolkit_obj DLV::outline_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::outline_model_templ<render_t>::set_data(float vertices[][3],
						  const int nvertices,
						  int planes[],
						  const int nplanes,
						  float iradius[],
						  float oradius[],
						  float dmin[], float dmax[],
						  int orientation[],
						  const int ncylinders,
						  float radii[],
						  const int nspheres)
{
}

template <class render_t>
DLV::shell_model_templ<render_t>::shell_model_templ(render_parent *p)
  : is_drawn(false)
{
  obj = nullptr;
}

template <class render_t>
DLV::toolkit_obj DLV::shell_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::shell_model_templ<render_t>::draw(const float colour[3],
					    const int nshells,
					    const float radii[],
					    const float shell_colours[][3])
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::attach_editor(const bool v,
						      const toolkit_obj &t,
						      float matrix[4][4],
						      float translate[3],
						      float centre[3],
						      char message[],
						      const int mlen) const
{
}

template class DLV::rspace_model_templ<CCP3::model_atoms>;
template class DLV::kspace_model_templ<CCP3::model_kspace>;
template class DLV::outline_model_templ<CCP3::model_outline>;
template class DLV::shell_model_templ<CCP3::model_shells>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_k *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_k(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_outline_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_outline_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_shell_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_shell_r(DLV::render_parent::get_serialize_obj());
    }

  }
}

template <class Archive>
void CCP3::model_atoms::serialize(Archive &ar, const unsigned int version)
{
}

template <class Archive>
void CCP3::model_kspace::serialize(Archive &ar, const unsigned int version)
{
}

template <class Archive>
void CCP3::model_outline::serialize(Archive &ar, const unsigned int version)
{
}

template <class Archive>
void CCP3::model_shells::serialize(Archive &ar, const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::save(Archive &ar,
					      const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::load(Archive &ar,
					      const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::save(Archive &ar,
					    const unsigned int version) const
{
  // Todo
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::load(Archive &ar,
					   const unsigned int version)
{
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_display_r)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_display_k)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_outline_r)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_shell_r)

#endif // DLV_USES_SERIALIZE
