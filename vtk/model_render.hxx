
#ifndef CCP3_MODEL_RENDER
#define CCP3_MODEL_RENDER

#include <vector>

namespace CCP3 {

  class atom_bond_frame {
  public:
    atom_bond_frame();

    int nopaque_atoms;
    float (*opaque_atom_coords)[3];
    float (*opaque_atom_colours)[3];
    float (*opaque_atom_radii)[3];
    //int ntransparent_atoms;
    //float (*transparent_atom_coords)[3];
    //float (*transparent_atom_colours)[3];
    //float *transparent_atom_radii;
    //float *opacities;
  };
    
  class model_atoms {
  public:
    model_atoms();

    std::vector<atom_bond_frame> frames;
    int cur_frame;
    render_atoms *options;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class model_kspace {
  public:

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class model_outline {
  public:

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class model_shells {
  public:

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CCP3::model_atoms)
BOOST_CLASS_EXPORT_KEY(CCP3::model_kspace)
BOOST_CLASS_EXPORT_KEY(CCP3::model_outline)
BOOST_CLASS_EXPORT_KEY(CCP3::model_shells)
#endif // DLV_USES_SERIALIZE

inline CCP3::atom_bond_frame::atom_bond_frame() : nopaque_atoms(0)
{
}

inline CCP3::model_atoms::model_atoms()
  : frames(1), cur_frame(0), options(nullptr)
{
}

#endif // CCP3_MODEL_RENDER
