
#ifndef CCP3_RENDER_BASE
#define CCP3_RENDER_BASE

// forward declare
class vtkSphereSource;
class vtkFloatArray;
class vtkUnsignedCharArray;
class vtkPoints;
class vtkPolyData;
class vtkGlyph3D;
class vtkPolyDataMapper;
class vtkLODActor;
class vtkRenderer;
class vtkCellArray;
class vtkTubeFilter;

namespace CCP3 {

  class render_base {
  public:
    render_base();

    float background[3];
    
    void set_r_view(DLVview *v);
    DLVview *get_r_view();
    void set_k_view(DLVview *v);
    DLVview *get_k_view();
    vtkRenderer *get_r_render();
    vtkRenderer *get_k_render();

  private:
    DLVview *rview;
    DLVview *kview;
    vtkRenderer *rspace;
    vtkRenderer *kspace;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class render_atoms : public render_base {
  public:
    render_atoms();

    struct common_t {
      // panel 1, make these common_options as in AVS for render_r/k?
      int na;
      int nb;
      int nc;
      bool centred;
      enum {is_primitive_cell, is_conventional_cell} cell_type;
      bool is_asymmetric_cell;
      double cell_tolerance;

      common_t();
    } common;

    struct lattice_t {
      // panel 2
      bool draw_lattice;
      bool label_lattice;

      lattice_t();
    } lattice;

    struct line_t {
      // panel 3
      int line_width;
      bool smooth_lines;

      line_t();
    } lines;

    struct atom_t {
      // panel 4 - atoms
      bool draw_atoms;
      int atom_scale_percent;
      enum {invert_selection, transparent_selection, transparent_other} selections_type;
      bool label_selections;
      int what_to_select;
      int opacity_percent;
      int atom_subdivisions;
      int property_method; // Todo

      atom_t();
    } atoms;

    struct bond_t {
      // panel 5 - bonds
      bool draw_bonds;
      bool draw_polyhedra;
      double bond_overlap;
      enum {bond_lines, bond_tubes} bond_draw_type;
      int bond_subdivisions;
      double bond_radius;
      bool outline_polyhedra;

      bond_t();
    } bond_data;

    int symmetry_selector;

    static vtkSphereSource *sphere;
    vtkUnsignedCharArray *colours;
    vtkFloatArray *radii;
    vtkPoints *points;
    vtkPolyData *poly;
    vtkGlyph3D *glyph; // or Glyph3DMapper - for GPU!
    vtkPolyDataMapper *atomMapper;
    vtkLODActor *opaque_atoms;
    vtkPoints *lat_points;
    vtkCellArray *lat_cells;
    vtkPolyData *lat_lines;
    vtkPolyDataMapper *latMapper;
    vtkActor *atom_lattice;
    vtkPoints *bond_points;
    vtkUnsignedCharArray *bond_colours;
    vtkCellArray *bond_cells;
    vtkPolyData *bond_lines;
    vtkTubeFilter *bond_tubes;
    vtkPolyDataMapper *tubebondMapper;
    vtkPolyDataMapper *linebondMapper;
    vtkLODActor *bond_opaque_tubes;
    vtkActor *bond_opaque_lines;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class render_outline : public render_base {
  public:

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class render_shells : public render_base {
  public:

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CCP3::render_base)
BOOST_CLASS_EXPORT_KEY(CCP3::render_atoms)
BOOST_CLASS_EXPORT_KEY(CCP3::render_outline)
BOOST_CLASS_EXPORT_KEY(CCP3::render_shells)
#endif // DLV_USES_SERIALIZE

inline void CCP3::render_base::set_r_view(DLVview *v)
{
  rview = v;
}

inline DLVview *CCP3::render_base::get_r_view()
{
  return rview;
}

inline void CCP3::render_base::set_k_view(DLVview *v)
{
  kview = v;
}

inline DLVview *CCP3::render_base::get_k_view()
{
  return kview;
}

inline vtkRenderer *CCP3::render_base::get_r_render()
{
  return rspace;
}

inline vtkRenderer *CCP3::render_base::get_k_render()
{
  return kspace;
}

inline CCP3::render_atoms::common_t::common_t()
  : na(1), nb(1), nc(1), centred(false), cell_type(is_conventional_cell),
    is_asymmetric_cell(false), cell_tolerance(0.0001)
{
}

inline CCP3::render_atoms::lattice_t::lattice_t()
  : draw_lattice(false), label_lattice(false)
{
}

inline CCP3::render_atoms::line_t::line_t()
  : line_width(5), smooth_lines(false)
{
}

inline CCP3::render_atoms::atom_t::atom_t()
  : draw_atoms(true), atom_scale_percent(75), selections_type(invert_selection),
    label_selections(false), what_to_select(0), opacity_percent(100),
    atom_subdivisions(16), property_method(0)
{
}

inline CCP3::render_atoms::bond_t::bond_t()
  : draw_bonds(false), draw_polyhedra(false), bond_overlap(1.0),
    bond_draw_type(bond_tubes), bond_subdivisions(8), bond_radius(0.1),
    outline_polyhedra(false)
{
}

inline CCP3::render_atoms::render_atoms()
  : symmetry_selector(0), colours(nullptr), radii(nullptr), points(nullptr),
    poly(nullptr), glyph(nullptr), atomMapper(nullptr), opaque_atoms(nullptr),
    lat_points(nullptr), lat_cells(nullptr), lat_lines(nullptr), latMapper(nullptr),
    atom_lattice(nullptr), bond_points(nullptr), bond_colours(nullptr),
    bond_cells(nullptr), bond_lines(nullptr), bond_tubes(nullptr), tubebondMapper(nullptr),
    linebondMapper(nullptr), bond_opaque_tubes(nullptr), bond_opaque_lines(nullptr)

{
}

#endif // CCP3_RENDER_BASE
