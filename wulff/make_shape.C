/**********************************************************************
* This software was developed at the National Institute of Standards  *
* and Technology by employees of the Federal Government in the course *
* of their official duties.  Pursuant to title 17 Section 105 of the  *
* United States Code this software is not subject to copyright        *
* protection and is in the public domain.                             *
**********************************************************************/ 
#include <math.h>
#include <stdio.h>
#include "vertex.h"

namespace Wulff {

double vertex_tolerance = .001;
typedef vertex *p2vertex;

extern void make_shape(vector *, double *, int, p2vertex &, int &);

}

using namespace Wulff;

// Determines how close vertices need to be to be considered the same vertex
void Wulff::make_shape(vector *nml, double *gamma, int num_normals,
		       p2vertex &vertices, int &num_vertices)
{
  // An estimate on an upper bound for a 'typical' shape.
  // Wish I knew an exact upper bound (less than n^3, that is.)
  int num_allocated_vertices = 5 * num_normals; 
  vertices = new vertex[num_allocated_vertices];

  if(!vertices)
  {
    fprintf(stderr,"Couldn't allocate %d vertices.\n",num_allocated_vertices);
    vertices = NULL;
    num_vertices = 0;
    return;
  }

  num_vertices = 0;

  for(int i = 0; i<num_normals-2; i++)
  {
    for(int j = i+1; j<num_normals-1; j++)
    {
      for(int k = j+1; k<num_normals; k++)
      {
	double det = nml[k].X * (nml[i].Z * nml[j].Y - nml[i].Y * nml[j].Z)
	           + nml[k].Y * (nml[i].X * nml[j].Z - nml[i].Z * nml[j].X)
	           + nml[k].Z * (nml[i].Y * nml[j].X - nml[i].X * nml[j].Y);
	if(fabs(det) > vertex_tolerance)
	{
	  double px = gamma[i] * (nml[j].Z * nml[k].Y - nml[j].Y * nml[k].Z)
	            + gamma[j] * (nml[k].Z * nml[i].Y - nml[k].Y * nml[i].Z)
		    + gamma[k] * (nml[i].Z * nml[j].Y - nml[i].Y * nml[j].Z);
	  px /= det;

	  double py = gamma[i] * (nml[j].X * nml[k].Z - nml[j].Z * nml[k].X)
	            + gamma[j] * (nml[k].X * nml[i].Z - nml[k].Z * nml[i].X)
		    + gamma[k] * (nml[i].X * nml[j].Z - nml[i].Z * nml[j].X);
	  py /= det;

	  double pz = gamma[i] * (nml[j].Y * nml[k].X - nml[j].X * nml[k].Y)
	            + gamma[j] * (nml[k].Y * nml[i].X - nml[k].X * nml[i].Y)
		    + gamma[k] * (nml[i].Y * nml[j].X - nml[i].X * nml[j].Y);
	  pz /= det;

	  int newvertex = 1;
	  for(int i1 = 0; i1<num_normals; i1++)
	  {
	    if(i1 == i || i1 == j || i1 == k)
	      continue;
	    double dot = px * nml[i1].X + py * nml[i1].Y + pz * nml[i1].Z;
	    if(dot>gamma[i1]+vertex_tolerance)
	    {
	      newvertex = 0;
	      break;
	    }
	  }
	  if(newvertex)
	  {
	    for(int i1 = 0; i1<num_vertices; i1++)
	    {
	      if((fabs(vertices[i1].X - px) < vertex_tolerance) &&
		 (fabs(vertices[i1].Y - py) < vertex_tolerance) &&
		 (fabs(vertices[i1].Z - pz) < vertex_tolerance))
	      {
//		fprintf(stderr,"Duplicated vertex: %d,%d,%d\n",i,j,k);
		vertices[i1].add_plane(i);
		vertices[i1].add_plane(j);
		vertices[i1].add_plane(k);
		newvertex = 0;
		break;
	      }
	    }
	  }
	  if(newvertex)
	  {
	    if(num_vertices == num_allocated_vertices)
	    {
	      num_allocated_vertices += num_normals;
	      vertex *tempv = new vertex[num_allocated_vertices];
	      if(!tempv)
	      {
		fprintf(stderr,"Couldn't allocate %d vertices.\n",
			num_allocated_vertices);
		vertices = NULL;
		num_vertices = 0;
		return;
	      }
	      for(int vn = 0; vn < num_vertices; vn++)
		tempv[vn] = vertices[vn];
	      delete [] vertices;
	      vertices = tempv;
	    }
	    vertices[num_vertices].X = px;
	    vertices[num_vertices].Y = py;
	    vertices[num_vertices].Z = pz;
	    vertices[num_vertices].add_plane(i);
	    vertices[num_vertices].add_plane(j);
	    vertices[num_vertices].add_plane(k);
	    num_vertices++;
	  }
	}
      }
    }
  }
}
